#ifndef VKA_MY_VULKAN_APPLICATION_H
#define VKA_MY_VULKAN_APPLICATION_H

#include <vka/System.h>
#include <vka/BufferMemoryPool.h>
#include <vka/DescriptorSets.h>
#include <vka/PipelineCreateInfo.h>
#include <vka/DeviceMeshPrimitive.h>
#include <vka/TextureMemoryPool.h>
#include <vka/Primatives.h>
#include <vka/HostImage.h>
#include <vka/math/linalg.h>
// need this for
#include <vka/RenderSurfaces/RenderSurfaceBase.h>

#include <queue>
/**
 * @brief The MyVulkanApplication struct
 *
 * This is the class that will do all the rendering and hold all the
 * information with regards to vulkan objects.
 *
 * To be able to use the RenderSurface object, you need to
 *  provide the following functions:
 *
 *     void init(vka::System & System)
 *     void render(vka::WINDOWFrame &frame)
 *
 * Use init() to allocate any vulkan objects.
 *
 * use render( ) to perform any rendering. See the individual comments
 * on the methods.
 */

/**
 * @brief The MyVulkanApplication struct
 *
 * The Vulkan Application should live within this single class.
 */
struct MyVulkanApplication : public vka::VulkanApplication
{
public:
    //=========================================================================
    // Required Functions
    //=========================================================================
    int i=0;
    /**
     * @brief init
     * @param System
     *
     * This function will be called to initilize
     * the all the memory/objects you need.
     *
     * This should basically be used as your constructor.
     *
     * The swapchain may not have been created by this point
     *
     */
    void initResources() override;

    /**
     * @brief initSwapChainResources
     *
     *
     * This method is called whenever the swapchain changes its size.
     * we can use this method to allocate any offscreen render targets.
     * that might be dependent on the swapchain size.
     */
    void initSwapChainResources() override;


    /**
     * @brief releaseSwapChainResources
     *
     * This method gets called whenever the swapchain has been
     * resized. This method will be called to release any
     * memory or resources which was allocated by a previous call to
     * initSwapChainResources()
     *
     * After this method is called, another call to initSwapChainResources()
     * will automatically be called.
     */
    void releaseSwapChainResources() override;


    /**
     * @brief releaseResources
     *
     * This is called when the vulkan application is about to be shut down
     * Use this to release all vulkan resources.
     */
    void releaseResources() override;



    void preRender() override;

    /**
     * @brief render
     * @param frame
     *
     * The render() method is called at each frame and at
     * a rate determiend by the RenderSurface.
     *
     * frame contains the following information which you can use
     * : frame.
     *
     *   frame.defaultRenderPass - the default render pass
     *   frame.currentFrameBuffer - the current framebuffer in the render pass;
     *   frame.currentCommandBuffer - the command buffer to be used to draw;
     *   frame.swapChainImageSize - the extents of the swapchain;
     */
    void render(vka::WINDOWFrame &frame) override;


    void postRender() override;
    //=========================================================================


    virtual void mouseMoveEvent(const vka::EvtInputMouseMotion *e) override
    {
        std::cout << e->x << " , " << e->y << std::endl;
    }

    void keyPressEvent(vka::EvtInputKey const * e)       override
    {
      //  std::cout << "KeyCode: " << to_string(e->keycode) << "  Scan Code:" << to_string(e->scancode) << std::endl;
        //EvtInputKey M;
        //
        //M.down = true;
        //M.repeat = e->count();
        //M.scancode = static_cast<KeyScanCode>( e->nativeScanCode() );
        //
        //m_application->keyPressEvent(&M);

    }
protected:

    void createRenderTarget(uint32_t width, uint32_t height);
    void initPipelines();


    vka::BufferPool    m_BufferPool;
    vk::DescriptorPool m_DescriptorPool;
    vk::Pipeline       m_TrianglePipeline;
    vk::Pipeline       m_TexturePresentPipeline;
    vka::TexturePool   m_TexturePool;

    vk::DescriptorSet m_DescriptorSet_ScreenPresent_Current;
    std::queue<vk::DescriptorSet> m_DescriptorSet_ScreenPresent_Queue;

    vka::TexturePool   m_ColorAttachmentPool;

    vka::RenderTarget  m_RenderTarget;
    vk::ImageView      m_renderTargetOutput;

    vk::Extent2D       m_surfaceSize;

    vk::ImageView      m_texture;

};




#endif
