#include "MyVulkanApplication.h"

// need this for
#include <vka/RenderSurfaces/RenderSurfaceBase.h>

/**
 * @brief VKAMain
 * @return
 *
 * This is the main entry point.
 * The SDLVulkanWidget and the QTVulkanWidget call
 * call this method to generate your application.
 */
vka::VulkanApplication* VKAMain()
{
    return new MyVulkanApplication();
}




void MyVulkanApplication::initSwapChainResources()
{
    // This method is called whenever the swapchain changes its size.
    // we can use this method to allocate any offscreen render targets.
    // that might be dependent on the swapchain size.
    std::cout << "MyVulkanApplication::initSwapChainResources()  " << swapchainImageSize().width << ", " << swapchainImageSize().height<< std::endl;

    if( m_surfaceSize.width == 0)
    {
        m_surfaceSize.width  = swapchainImageSize().width;
        m_surfaceSize.height = swapchainImageSize().height;

        createRenderTarget(m_surfaceSize.width, m_surfaceSize.height);
    }
}

void MyVulkanApplication::releaseSwapChainResources()
{
    // This method gets called whenever the swapchain has been
    // resized. This method will be called to release any
    // memory or resources which was allocated by a previous call to
    // initSwapChainResources()
    //
    // After this method is called, another call to initSwapChainResources()
    // will automatically be called.

    std::cout << "MyVulkanApplication::releaseSwapChainResources() " << swapchainImageSize().width << ", " << swapchainImageSize().height<< std::endl;
}

void MyVulkanApplication::releaseResources()
{
    std::cout << "MyVulkanApplication::releaseResources()" << std::endl;
}

void MyVulkanApplication::initResources()
{
    auto imgSize = swapchainImageSize();
    std::cout << "MyVulkanApplication::initResources() " << std::endl;

    auto & System = getSystem();

    vk::BufferCreateInfo binfo;
    binfo.setSize(1024*1024*25);
    binfo.setUsage( vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferSrc);
    m_BufferPool = System.createBufferPool( binfo, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);


    vka::DescriptorPoolCreateInfo2 I;
    I.maxSets = 1000;
    I.poolSizes.push_back( {vk::DescriptorType::eCombinedImageSampler, 1000 });
    I.poolSizes.push_back( {vk::DescriptorType::eUniformBuffer, 1000 });
    I.poolSizes.push_back( {vk::DescriptorType::eStorageBuffer, 1000 });

    m_DescriptorPool = System.createDescriptorPool( I.create() );





    m_TexturePool =  System.createTexturePool( 50*1024*1024,
                                               vk::Format::eR8G8B8A8Unorm,
                                               vk::MemoryPropertyFlagBits::eDeviceLocal,
                                               vk::ImageUsageFlagBits::eSampled
                                               | vk::ImageUsageFlagBits::eTransferSrc
                                               | vk::ImageUsageFlagBits::eTransferDst);

    m_texture = m_TexturePool.createNewImage( vk::Format::eR8G8B8A8Unorm, vk::Extent3D(imgSize.width,imgSize.height,1), 1, 9);


    m_ColorAttachmentPool =  System.createTexturePool( 128*1024*1024,
                                                       vk::Format::eR32G32B32A32Sfloat,
                                                       vk::MemoryPropertyFlagBits::eDeviceLocal,
                                                       vk::ImageUsageFlagBits::eSampled
                                                       | vk::ImageUsageFlagBits::eColorAttachment);

}

void MyVulkanApplication::createRenderTarget(uint32_t width, uint32_t height)
{
    vka::RenderTargetCreateInfo RTI;

    m_renderTargetOutput = m_ColorAttachmentPool.createNewImage( vk::Format::eR8G8B8A8Unorm, vk::Extent3D(width,height,1), 1, 1);

    RTI.colorTargets.push_back( m_renderTargetOutput );
    //RTI.colorTargets.push_back( attachmentPool.createNewImage( vk::Format::eR32G32B32A32Sfloat, vk::Extent3D(WIDTH,HEIGHT,1), 1, 1) );
    //RTI.colorTargets.push_back( attachmentPool.createNewImage( vk::Format::eR32G32B32A32Sfloat, vk::Extent3D(WIDTH,HEIGHT,1), 1, 1) );
    //RTI.depthTarget = depthPool.createNewImage( vk::Format::eD32Sfloat, vk::Extent3D(WIDTH,HEIGHT,1), 1, 1);
    m_RenderTarget = getSystem().createRenderTarget(RTI);

    {
        vka::DescriptorSetLayoutCreateInfo2 L;
        L.bindings.push_back( vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eCombinedImageSampler, 1, vk::ShaderStageFlagBits::eFragment) );

        auto l1 = getSystem().createDescriptorSetLayout(L);

        for(uint32_t i=0; i<m_concurrentFrameCount+1; i++)
        {
            m_DescriptorSet_ScreenPresent_Queue.push(
                        getSystem().allocateDescriptorSet( m_DescriptorPool, l1 )
                        );
        }
    }

    {
        // get the next one in the queue and updated it and
        // then make it the current one.
        auto dset = m_DescriptorSet_ScreenPresent_Queue.front();
        m_DescriptorSet_ScreenPresent_Queue.pop();

        std::vector< vk::WriteDescriptorSet > W_v;

        vk::DescriptorImageInfo imageInfo;

        imageInfo.imageView   = m_renderTargetOutput;
        imageInfo.sampler     = getSystem().getDefaultSampler( m_renderTargetOutput );
        imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;

        W_v.push_back(
                    vk::WriteDescriptorSet( dset,  // set
                                            0,   // binding
                                            0,   // array index
                                            1,   // total descriptors
                                            vk::DescriptorType::eCombinedImageSampler,
                                            &imageInfo,
                                            nullptr)
                    );


        getSystem().updateDescriptorSets(W_v);
        m_DescriptorSet_ScreenPresent_Current = dset;
    }

    initPipelines();
}

void MyVulkanApplication::initPipelines()
{
    auto & System = *m_System;
    // The first Pipeline we're going to create will simply draw a triangle onto the screen
    // with no depth testing.
    //
    // We need to make sure we create a ScreenBuffer or a RenderTarget first. The ScreenBuffer
    // is automatically created in the Surface class.
    {

        vka::GraphicsPipelineCreateInfo3 P;

        P.vertexShader   = System.createShaderModuleFromGLSL( VKA_SOURCE_CODE_DIR  "/share/shaders/triangle.vert");
        P.fragmentShader = System.createShaderModuleFromGLSL( VKA_SOURCE_CODE_DIR  "/share/shaders/triangle.frag");

        P.colorOutputs.push_back(vk::Format::eR8G8B8A8Unorm);
        P.renderToTexture = true;
        P.enableDepthTest = false;

        m_TrianglePipeline = System.createPipeline(P);

    }


    {

        vka::GraphicsPipelineCreateInfo3 P;

        P.vertexShader   = System.createShaderModuleFromGLSL( VKA_SOURCE_CODE_DIR  "/share/shaders/texture_present.vert");
        P.fragmentShader = System.createShaderModuleFromGLSL( VKA_SOURCE_CODE_DIR  "/share/shaders/texture_present.frag");

        P.enableDepthTest = true;
        P.renderPass     = getDefaultRenderPass();

        m_TexturePresentPipeline = System.createPipeline(P);

    }
}

void MyVulkanApplication::preRender()
{

}

void MyVulkanApplication::postRender()
{
    auto & System = *m_System;


    if(1)
    {
        // take the currently working descriptor set and push it onto the queue
        // get the next one in the queue and updated it and
        // then make it the current one.

        m_DescriptorSet_ScreenPresent_Queue.push( m_DescriptorSet_ScreenPresent_Current);


        auto dset = m_DescriptorSet_ScreenPresent_Queue.front();
        m_DescriptorSet_ScreenPresent_Queue.pop();

        //std::cout << "updating descriptor set: " << dset << std::endl;

        std::vector< vk::WriteDescriptorSet > W_v;

        vk::DescriptorImageInfo imageInfo;

        imageInfo.imageView   = m_renderTargetOutput;
        imageInfo.sampler     = getSystem().getDefaultSampler( m_renderTargetOutput );
        imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;

        W_v.push_back(
                    vk::WriteDescriptorSet( dset,  // set
                                            0,   // binding
                                            0,   // array index
                                            1,   // total descriptors
                                            vk::DescriptorType::eCombinedImageSampler,
                                            &imageInfo,
                                            nullptr)
                    );


        System.updateDescriptorSets(W_v);
        m_DescriptorSet_ScreenPresent_Current = dset;
    }
}

void MyVulkanApplication::render(vka::WINDOWFrame &frame)
{
    auto & cb = frame.currentCommandBuffer;

    cb.waitForHostTransfers();
    // This is going to be a two pass Deferred rendering frame.
    // we will first draw to a color attachment
    // and then use the color attachmet as an input texture
    // to render a quad to the screen
    {

        auto rpbi = m_RenderTarget.getRenderPassBeginInfo();

        cb.beginRenderPass(rpbi, vk::SubpassContents::eInline);

        cb.bindPipeline( vk::PipelineBindPoint::eGraphics, m_TrianglePipeline);

        vk::Viewport vp( 0,0,
                         rpbi.renderArea.extent.width, rpbi.renderArea.extent.height,0,1);
        vk::Rect2D scissor( vk::Offset2D(0,0),
                            rpbi.renderArea.extent);

        cb.setViewport(0, vp);
        cb.setScissor(0, scissor);
        cb.draw(3,1,0,0);

        cb.endRenderPass();

    }

    {

        // wait for all the color attachments to finish being written to
        // before executing any fragment shaders which come after this barrier.
        // No need to perform any ImageTransitions since the RenderTarget
        // is automatically set up to convert the images into eShaderReadOnlyOptimal
#if 0
        cb.pipelineBarrier( vk::PipelineStageFlagBits::eColorAttachmentOutput,
                            vk::PipelineStageFlagBits::eFragmentShader,
                            vk::DependencyFlags(),
                            nullptr, nullptr, nullptr);
#else
        // does the same as above
        cb.simplePipelineBarrier(vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                 vk::PipelineStageFlagBits::eFragmentShader);
#endif
    }


    // The Second render pass. We'll use this to
    {
        VkClearColorValue clearColor = {{ 1, 0, 0, 1 }};
        VkClearDepthStencilValue clearDS = { 1, 0 };
        VkClearValue clearValues[3];
        memset(clearValues, 0, sizeof(clearValues));
        clearValues[0].color = clearValues[2].color = clearColor;
        clearValues[1].depthStencil = clearDS;

        //VkRenderPassBeginInfo rpBeginInfo;
        //memset(&rpBeginInfo, 0, sizeof(rpBeginInfo));
        vk::RenderPassBeginInfo rpBeginInfo;
        rpBeginInfo.renderPass               = frame.defaultRenderPass;//m_window->defaultRenderPass();
        rpBeginInfo.framebuffer              = frame.currentFrameBuffer;//m_window->currentFramebuffer();
        rpBeginInfo.renderArea.extent.width  = frame.swapChainImageSize.width;
        rpBeginInfo.renderArea.extent.height = frame.swapChainImageSize.height;
        rpBeginInfo.clearValueCount          = 2;//m_window->sampleCountFlagBits() > VK_SAMPLE_COUNT_1_BIT ? 3 : 2;
        rpBeginInfo.pClearValues             = reinterpret_cast<vk::ClearValue const*>(clearValues);

        cb.beginRenderPass(rpBeginInfo, vk::SubpassContents::eInline);

        cb.bindPipeline( vk::PipelineBindPoint::eGraphics, m_TexturePresentPipeline);

        cb.bindDescriptorSets( m_TexturePresentPipeline, 0, m_DescriptorSet_ScreenPresent_Current, nullptr);

        vk::Viewport vp(0,0, frame.swapChainImageSize.width,frame.swapChainImageSize.height,0,1);
        vk::Rect2D scissor( vk::Offset2D(0,0), frame.swapChainImageSize);


        struct push_t {
            glm::vec2 position   = {-0.5,-0.5};
            glm::vec2 size       = {1,1};
            glm::vec2 screenSize = {0, 0};
            glm::vec2 unused;
        } pushConsts;

        pushConsts.screenSize.x = frame.swapChainImageSize.width;
        pushConsts.screenSize.y = frame.swapChainImageSize.height;
        auto layout = getSystem().info( m_TexturePresentPipeline ).layout;
        cb.pushConstants( layout, vk::ShaderStageFlagBits::eFragment | vk::ShaderStageFlagBits::eVertex, 0, sizeof(pushConsts), &pushConsts);


        cb.setViewport(0, vp);
        cb.setScissor(0, scissor);
        cb.draw(6,1,0,0);

        // Rendering code goes here.


        cb.endRenderPass();
    }

}

