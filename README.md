# VKA

## Renderers

In vka, there are some basic renderers which a allow you to render specific
types of things. For example there is a SceneRenderer which lets you render
vka::Scene (basically a GLTF object). It contains it's own shader
and provides some basic mechanisms to draw the scene at various locations.

Renderers require you to provided memory pools or other external objects that
may be required.

PBRSceneRenderer: Draws vka::Scene

TexturePresenter: Presents a single texture to the



## TexturePresenter

## PostProcessRenderer

## Line Renderer

Used to draw simple wireframe lines and shapes.

```C++

vka::LineRenderer L;
auto ci = L.createInfo();
// fill ci
L.init(ci);

// somewhere during the frame

// start drawing using a specific model matrix
L.pushMatrices( model, view, projection);
    L.drawLineSegment( p_start, c_start, p_end, c_end); // position/colour of linesegment
    L.drawLineSegment( p2_start, c2_start, p2_end, c2_end);
    //...

// start a new shape with a different model matrix
L.pushMatrices( model2, view, projection);
    L.drawLineSegment( p_start, c_start, p_end, c_end); // position/colour of linesegment
    L.drawLineSegment( p2_start, c2_start, p2_end, c2_end);
    // ....

// During command buffer recording.
L.begin(cmdBuff);
    L.drawAll();
L.end();

```

## Phases (Future)

Phases are a more managed Renderer, they combine multiple renderers into a
single phase. For example the `gBufferPhase` is used to draw Scenes, lines,
and to a specific render target. The RenderTarget and all Color Attachments
are managed for your.

Phases manage much of their own resources: RenderPasses, RenderTargets, Output
Color Attachments, Descriptor Pools

Phases can be linked together. For example, the `gBufferPhase` can be linked to
to the `LightPassPhase` which takes the output images of the gBufferPhase and
uses them to produce a composed image with lights.



```C++

gBufferPhase G;

G.init(...);

LightPassPhase L;

L.setPositionInput( G.positionTarget );
L.setNormalInput(   G.normalTarget   );
L.setAlbedoInput(   G.albedoTarget   );

L.init();


G.begin(cmdBuff)
    G.setCameraMatrix(....);
    G.drawScene( ... );
    G.drawScene( ... );
    G.drawScene( ... );
    G.drawScene( ... );
G.end()

L.begin(cmdBuff);
    L.draw();
L.end();

```
