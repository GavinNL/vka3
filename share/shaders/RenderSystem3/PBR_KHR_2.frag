//
#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : enable



#define HAS_FRAGMENT_START
#define HAS_FRAGMENT_END

// Ignore the PBR material calculation
//#define IGNORE_PBR_MATERIAL

#include "defaultFragmentShader.h"



#if defined HAS_FRAGMENT_START
// Use this to modify the values coming from the
// vertex shader.
VertexInput FRAGMENT_START(VertexInput v_in)
{
    // these are the default values.
    v_in.position     = f_POSITION;
    v_in.normal       = f_NORMAL;
    v_in.texCoord0.xy = f_TEXCOORD_0;
    v_in.color        = f_COLOR_0;
    return v_in; // dont do anything
}
#endif


#if defined HAS_FRAGMENT_END
// this is the final function before the colour
// it oputput to the screen
// All lighting calculations have been done
vec4 FRAGMENT_END(VertexInput v_in, vec4 outputColor)
{
    return outputColor;
}
#endif
