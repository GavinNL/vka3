#ifndef RENDERSYSTEM3_PBR_FUNCTRIONS_H
#define RENDERSYSTEM3_PBR_FUNCTRIONS_H

#include "PBR_Types.h"

MULTIBUFFER(Environment_t)
MULTIBUFFER(Material_t)
MULTIBUFFER(Light_t)

const float M_PI = 3.141592653589793;
const float GAMMA = 2.2;
const float INV_GAMMA = 1.0 / GAMMA;

#define v_Position f_POSITION
#define v_Normal   f_NORMAL
#define v_Color    f_COLOR_0
#define v_UVCoord1 f_TEXCOORD_0
#define v_UVCoord2 f_TEXCOORD_0

#define s_Material                      s_Material_t.value
#define s_Lights                        s_Light_t.value


#define u_GGXLUT                 s_Environment_t.value[  U_ENVIRONMENT_0 ].brdf
#define u_LambertianEnvSampler   s_Environment_t.value[  U_ENVIRONMENT_0 ].irradiance
#define u_GGXEnvSampler          s_Environment_t.value[  U_ENVIRONMENT_0 ].radiance


#define ALPHAMODE_MASK
#define ALPHAMODE_OPAQUE


// These values should eventually be defined in the
// object system when the shder is compiled
#define HAS_BASE_COLOR_MAP
#define HAS_NORMALS
#define HAS_NORMAL_MAP
#define HAS_METALLIC_ROUGHNESS_MAP
#define HAS_EMISSIVE_MAP
#define HAS_OCCLUSION_MAP
#define HAS_VERTEX_COLOR_VEC3
#define HAS_THICKNESS_MAP
#define HAS_THICKNESS_UV_TRANSFORM


#define MATERIAL_IOR
#define MATERIAL_METALLICROUGHNESS
#define MATERIAL_TRANSMISSION
#define MATERIAL_UNLIT
#define MATERIAL_ABSORPTION
#define MATERIAL_CLEARCOAT

#define TONEMAP_ACES
#define TONEMAP_HEJLRICHARD
#define TONEMAP_UNCHARTED

#define USE_IBL
#define USE_PUNCTUAL


struct NormalInfo
{
    vec3 ng;
    vec3 n;
    vec3 t;
    vec3 b;
};


float clampedDot(vec3 x, vec3 y)
{
    return clamp(dot(x, y), 0.0, 1.0);
}

float sq(float t)
{
    return t * t;
}

vec2 sq(vec2 t)
{
    return t * t;
}

vec3 sq(vec3 t)
{
    return t * t;
}

vec4 sq(vec4 t)
{
    return t * t;
}

vec3 linearTosRGB(vec3 color)
{
    return pow(color, vec3(INV_GAMMA));
}

vec3 sRGBToLinear(vec3 srgbIn)
{
    return vec3(pow(srgbIn.xyz, vec3(GAMMA)));
}

vec4 sRGBToLinear(vec4 srgbIn)
{
    return vec4(sRGBToLinear(srgbIn.xyz), srgbIn.w);
}

vec3 toneMapUncharted2Impl(vec3 color)
{
    const float A = 0.15;
    const float B = 0.50;
    const float C = 0.10;
    const float D = 0.20;
    const float E = 0.02;
    const float F = 0.30;
    return ((color*(A*color+C*B)+D*E)/(color*(A*color+B)+D*F))-E/F;
}

vec3 toneMapUncharted(vec3 color)
{
    const float W = 11.2;
    color = toneMapUncharted2Impl(color * 2.0);
    vec3 whiteScale = 1.0 / toneMapUncharted2Impl(vec3(W));
    return linearTosRGB(color * whiteScale);
}

vec3 toneMapHejlRichard(vec3 color)
{
    color = max(vec3(0.0), color - vec3(0.004));
    return (color*(6.2*color+.5))/(color*(6.2*color+1.7)+0.06);
}

vec3 toneMapACES(vec3 color)
{
    const float A = 2.51;
    const float B = 0.03;
    const float C = 2.43;
    const float D = 0.59;
    const float E = 0.14;
    return linearTosRGB(clamp((color * (A * color + B)) / (color * (C * color + D) + E), 0.0, 1.0));
}

vec3 toneMap(vec3 color)
{

#ifdef TONEMAP_UNCHARTED
    return toneMapUncharted(color);
#endif

#ifdef TONEMAP_HEJLRICHARD
    return toneMapHejlRichard(color);
#endif

#ifdef TONEMAP_ACES
    return toneMapACES(color);
#endif

    return linearTosRGB(color);
}











vec3 transmissionAbsorption(vec3 v, vec3 n, float ior, float thickness, vec3 absorptionColor)
{
    vec3 r = refract(-v, n, 1.0 / ior);
    return exp(-absorptionColor * thickness * dot(-n, r));
}


vec3 F_None(vec3 f0, vec3 f90, float VdotH)
{
    return f0;
}

vec3 F_Schlick(vec3 f0, vec3 f90, float VdotH)
{
    return f0 + (f90 - f0) * pow(clamp(1.0 - VdotH, 0.0, 1.0), 5.0);
}

vec3 F_CookTorrance(vec3 f0, vec3 f90, float VdotH)
{
    vec3 f0_sqrt = sqrt(f0);
    vec3 ior = (1.0 + f0_sqrt) / (1.0 - f0_sqrt);
    vec3 c = vec3(VdotH);
    vec3 g = sqrt(sq(ior) + c*c - 1.0);
    return 0.5 * pow(g-c, vec3(2.0)) / pow(g+c, vec3(2.0)) * (1.0 + pow(c*(g+c) - 1.0, vec3(2.0)) / pow(c*(g-c) + 1.0, vec3(2.0)));
}



float V_GGX(float NdotL, float NdotV, float alphaRoughness)
{
    float alphaRoughnessSq = alphaRoughness * alphaRoughness;

    float GGXV = NdotL * sqrt(NdotV * NdotV * (1.0 - alphaRoughnessSq) + alphaRoughnessSq);
    float GGXL = NdotV * sqrt(NdotL * NdotL * (1.0 - alphaRoughnessSq) + alphaRoughnessSq);

    float GGX = GGXV + GGXL;
    if (GGX > 0.0)
    {
        return 0.5 / GGX;
    }
    return 0.0;
}



float V_GGX_anisotropic(float NdotL, float NdotV, float BdotV, float TdotV, float TdotL, float BdotL, float anisotropy, float at, float ab)
{
    float GGXV = NdotL * length(vec3(at * TdotV, ab * BdotV, NdotV));
    float GGXL = NdotV * length(vec3(at * TdotL, ab * BdotL, NdotL));
    float v = 0.5 / (GGXV + GGXL);
    return clamp(v, 0.0, 1.0);
}


float V_Ashikhmin(float NdotL, float NdotV)
{
    return clamp(1.0 / (4.0 * (NdotL + NdotV - NdotL * NdotV)),0.0,1.0);
}

float V_Kelemen(float LdotH)
{
        return 0.25 / (LdotH * LdotH);
}




float D_GGX(float NdotH, float alphaRoughness)
{
    float alphaRoughnessSq = alphaRoughness * alphaRoughness;
    float f = (NdotH * NdotH) * (alphaRoughnessSq - 1.0) + 1.0;
    return alphaRoughnessSq / (M_PI * f * f);
}

float D_GGX_anisotropic(float NdotH, float TdotH, float BdotH, float anisotropy, float at, float ab)
{
    float a2 = at * ab;
    vec3 f = vec3(ab * TdotH, at * BdotH, a2 * NdotH);
    float w2 = a2 / dot(f, f);
    return a2 * w2 * w2 / M_PI;
}

float D_Ashikhmin(float NdotH, float alphaRoughness)
{
        float a2 = alphaRoughness * alphaRoughness;
    float cos2h = NdotH * NdotH;
    float sin2h = 1.0 - cos2h;
    float sin4h = sin2h * sin2h;
    float cot2 = -cos2h / (a2 * sin2h);
    return 1.0 / (M_PI * (4.0 * a2 + 1.0) * sin4h) * (4.0 * exp(cot2) + sin4h);
}


float D_Charlie(float sheenRoughness, float NdotH)
{
    sheenRoughness = max(sheenRoughness, 0.000001);     float alphaG = sheenRoughness * sheenRoughness;
    float invR = 1.0 / alphaG;
    float cos2h = NdotH * NdotH;
    float sin2h = 1.0 - cos2h;
    return (2.0 + invR) * pow(sin2h, invR * 0.5) / (2.0 * M_PI);
}



vec3 BRDF_lambertian(vec3 f0, vec3 f90, vec3 diffuseColor, float VdotH)
{
        return (1.0 - F_Schlick(f0, f90, VdotH)) * (diffuseColor / M_PI);
}

vec3 BRDF_specularGGX(vec3 f0, vec3 f90, float alphaRoughness, float VdotH, float NdotL, float NdotV, float NdotH)
{
    vec3 F = F_Schlick(f0, f90, VdotH);
    float Vis = V_GGX(NdotL, NdotV, alphaRoughness);
    float D = D_GGX(NdotH, alphaRoughness);

    return F * Vis * D;
}



vec3 BRDF_specularAnisotropicGGX(vec3 f0, vec3 f90, float alphaRoughness, float VdotH, float NdotL, float NdotV, float NdotH,
    float BdotV, float TdotV, float TdotL, float BdotL, float TdotH, float BdotH, float anisotropy)
{
            float at = max(alphaRoughness * (1.0 + anisotropy), 0.00001);
    float ab = max(alphaRoughness * (1.0 - anisotropy), 0.00001);

    vec3 F = F_Schlick(f0, f90, VdotH);
    float V = V_GGX_anisotropic(NdotL, NdotV, BdotV, TdotV, TdotL, BdotL, anisotropy, at, ab);
    float D = D_GGX_anisotropic(NdotH, TdotH, BdotH, anisotropy, at, ab);

    return F * V * D;
}




#ifdef MATERIAL_SHEEN
vec3 BRDF_specularSheen(vec3 sheenColor, float sheenIntensity, float sheenRoughness, float NdotL, float NdotV, float NdotH)
{
    float sheenDistribution = D_Charlie(sheenRoughness, NdotH);
    float sheenVisibility = V_Ashikhmin(NdotL, NdotV);
    return sheenColor * sheenIntensity * sheenDistribution * sheenVisibility;
}
#endif











vec2 getNormalUV()
{
        vec3 uv = vec3(v_UVCoord1,1.0);
    #ifdef HAS_NORMAL_UV_TRANSFORM
    uv *= u_NormalUVTransform;
    #endif

    return uv.xy;
}

vec2 getEmissiveUV()
{
        vec3 uv = vec3( v_UVCoord1,1.0);
    #ifdef HAS_EMISSIVE_UV_TRANSFORM
    uv *= u_EmissiveUVTransform;
    #endif

    return uv.xy;
}

#ifdef HAS_OCCLUSION_MAP
vec2 getOcclusionUV()
{
        vec3 uv = vec3(v_UVCoord1,1.0);
    #ifdef HAS_OCCLUSION_UV_TRANSFORM
    uv *= u_OcclusionUVTransform;
    #endif

    return uv.xy;
}
#endif

vec2 getBaseColorUV()
{
        vec3 uv = vec3(v_UVCoord1,1.0);
    #ifdef HAS_BASECOLOR_UV_TRANSFORM
    uv *= u_BaseColorUVTransform;
    #endif

    return uv.xy;
}

vec2 getMetallicRoughnessUV()
{
        vec3 uv = vec3(v_UVCoord1,1.0);
    #ifdef HAS_METALLICROUGHNESS_UV_TRANSFORM
    uv *= u_MetallicRoughnessUVTransform;
    #endif

    return uv.xy;
}

#ifdef MATERIAL_SPECULARGLOSSINESS
vec2 getSpecularGlossinessUV()
{
        vec3 uv = vec3(v_UVCoord1,1.0);
    #ifdef HAS_SPECULARGLOSSINESS_UV_TRANSFORM
    uv *= u_SpecularGlossinessUVTransform;
    #endif

    return uv.xy;
}


vec2 getDiffuseUV()
{
        vec3 uv = vec3(v_UVCoord1,1.0);
    #ifdef HAS_DIFFUSE_UV_TRANSFORM
    uv *= u_DiffuseUVTransform;
    #endif

    return uv.xy;
}

#endif


#ifdef MATERIAL_CLEARCOAT

    vec2 getClearcoatUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_CLEARCOAT_UV_TRANSFORM
        uv *= u_ClearcoatUVTransform;
        #endif
        return uv.xy;
    }

    vec2 getClearcoatRoughnessUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_CLEARCOATROUGHNESS_UV_TRANSFORM
        uv *= u_ClearcoatRoughnessUVTransform;
        #endif
        return uv.xy;
    }

    vec2 getClearcoatNormalUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_CLEARCOATNORMAL_UV_TRANSFORM
        uv *= u_ClearcoatNormalUVTransform;
        #endif
        return uv.xy;
    }

#endif


#ifdef MATERIAL_SHEEN

    vec2 getSheenUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_SHEENCOLORINTENSITY_UV_TRANSFORM
        uv *= u_SheenColorIntensityUVTransform;
        #endif
        return uv.xy;
    }

#endif



#ifdef MATERIAL_METALLICROUGHNESS_SPECULAROVERRIDE
vec2 getMetallicRoughnessSpecularUV()
{
        vec3 uv = vec3(v_UVCoord1,1.0);
    #ifdef HAS_METALLICROUGHNESSSPECULAR_UV_TRANSFORM
    uv *= u_MetallicRougnessSpecularUVTransform;
    #endif
    return uv.xy;
}
#endif





#ifdef MATERIAL_SUBSURFACE

    vec2 getSubsurfaceColorUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_SUBSURFACECOLOR_UV_TRANSFORM
        uv *= u_SubsurfaceColorUVTransform;
        #endif
        return uv.xy;
    }

    vec2 getSubsurfaceThicknessUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_SUBSURFACETHICKNESS_UV_TRANSFORM
        uv *= u_SubsurfaceThicknessUVTransform;
        #endif
        return uv.xy;
    }

#endif





#ifdef MATERIAL_THIN_FILM

    vec2 getThinFilmUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_THIN_FILM_UV_TRANSFORM
        uv *= u_ThinFilmUVTransform;
        #endif

        return uv.xy;
    }

    vec2 getThinFilmThicknessUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_THIN_FILM_THICKNESS_UV_TRANSFORM
        uv *= u_ThinFilmThicknessUVTransform;
        #endif

        return uv.xy;
    }

    vec2 getThicknessUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_THICKNESS_UV_TRANSFORM
        uv *= u_ThicknessUVTransform;
        #endif

        return uv.xy;
    }

#endif



#ifdef HAS_ANISOTROPY_MAP

    vec2 getAnisotropyUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_ANISOTROPY_UV_TRANSFORM
        uv *= u_AnisotropyUVTransform;
        #endif

        return uv.xy;
    }

    vec2 getAnisotropyDirectionUV()
    {
            vec3 uv = vec3(v_UVCoord1,1.0);
        #ifdef HAS_ANISOTROPY_DIRECTION_UV_TRANSFORM
        uv *= u_AnisotropyDirectionUVTransform;
        #endif

        return uv.xy;
    }

#endif












#ifdef HAS_NORMALS
  #ifdef HAS_TANGENTS
    #else
    #endif
#endif

#ifdef HAS_VERTEX_COLOR_VEC3
#endif
#ifdef HAS_VERTEX_COLOR_VEC4
#endif


#include "Light.h"


vec4 getVertexColor()
{
   vec4 color = vec4(1.0, 1.0, 1.0, 1.0);

#ifdef HAS_VERTEX_COLOR_VEC3
    color.rgb = v_Color;
#endif
#ifdef HAS_VERTEX_COLOR_VEC4
    color = v_Color;
#endif

   return color;
}



vec2 getBRDF(vec2 samplePoint)
{
    #if 0
    vec4 v255 = texture(u_GGXLUT, samplePoint);
    v255 *= 255.f;
    return vec2( 255.0*v255.g + v255.r , 255.0*v255.a + v255.b ) / 65535.0f;
    #else
    return textureLod( u_GGXLUT, vec2(samplePoint.x, samplePoint.y), 0.f).rg;
    #endif
}



vec2 getBRDF(vec3 n, vec3 v, float perceptualRoughness, vec3 specularColor)
{
    float NdotV = clampedDot(n, v);

    float mipCount = float( textureQueryLevels( u_GGXEnvSampler ) );

    float lod = clamp(perceptualRoughness * float(mipCount), 0.0, float(mipCount));
    vec3 reflection = normalize(reflect(-v, n));

    vec2 brdfSamplePoint = clamp(vec2(NdotV, perceptualRoughness), vec2(0.0, 0.0), vec2(1.0, 1.0));
    vec2 brdf = getBRDF(brdfSamplePoint);
    return brdf;
}

vec3 getIBLRadianceGGX(vec3 n, vec3 v, float perceptualRoughness, vec3 specularColor)
{
    float NdotV = clampedDot(n, v);


    float mipCount = float( textureQueryLevels(u_GGXEnvSampler) );

    float lod = clamp(perceptualRoughness * float(mipCount), 0.0, float(mipCount));
    vec3 reflection = normalize(reflect(-v, n));

    vec2 brdfSamplePoint = clamp(vec2(NdotV, perceptualRoughness), vec2(0.0, 0.0), vec2(1.0, 1.0));
    vec2 brdf = getBRDF(brdfSamplePoint);    vec4 specularSample = textureLod(u_GGXEnvSampler, reflection, lod);

    vec3 specularLight = specularSample.rgb;

#ifndef USE_HDR
    specularLight = sRGBToLinear(specularLight);
#endif

   return specularLight * (specularColor * brdf.x + brdf.y);
}

vec3 getIBLRadianceTransmission(vec3 n, vec3 v, float perceptualRoughness, float ior, vec3 baseColor)
{
    float NdotV = clampedDot(n, v);
    vec2 brdfSamplePoint = clamp(vec2(NdotV, perceptualRoughness), vec2(0.0, 0.0), vec2(1.0, 1.0));
    vec2 brdf = getBRDF(brdfSamplePoint);
    float mipCount = float( textureQueryLevels(u_GGXEnvSampler) );
    float lod = clamp(perceptualRoughness * float(mipCount), 0.0, float(mipCount));

    vec3 r = refract(-v, n, 1.0 / ior);
    vec3 m = 2.0 * dot(-n, r) * r + n;
    vec3 rr = -refract(-r, m, ior);

    vec4 specularSample = textureLod(u_GGXEnvSampler, rr, lod);
    vec3 specularLight = specularSample.rgb;

#ifndef USE_HDR
    specularLight = sRGBToLinear(specularLight);
#endif

   return specularLight * (brdf.x + brdf.y);
}

vec3 getIBLRadianceLambertian(vec3 n, vec3 diffuseColor)
{
    vec3 diffuseLight = vec3(1,1,1);

    diffuseLight = texture(u_LambertianEnvSampler, n).rgb;

    #ifndef USE_HDR
        diffuseLight = sRGBToLinear(diffuseLight);
    #endif

    return diffuseLight * diffuseColor;
}

#ifdef MATERIAL_SHEEN
vec3 getIBLRadianceCharlie(vec3 n, vec3 v, float sheenRoughness, vec3 sheenColor, float sheenIntensity, int materialIndex)
{
    float NdotV = clampedDot(n, v);
    float mipCount = float( textureQueryLevels(s_Material[materialIndex].CharlieEnvSampler) );
    float lod = clamp(sheenRoughness * float(mipCount), 0.0, float(mipCount));
    vec3 reflection = normalize(reflect(-v, n));

    vec2 brdfSamplePoint = clamp(vec2(NdotV, sheenRoughness), vec2(0.0, 0.0), vec2(1.0, 1.0));
    float brdf = texture(u_CharlieLUT, brdfSamplePoint).b;
    vec4 sheenSample = textureLod(s_Material[materialIndex].CharlieEnvSampler, reflection, lod);

    vec3 sheenLight = sheenSample.rgb;

    #ifndef USE_HDR
    sheenLight = sRGBToLinear(sheenLight);
    #endif

    return sheenIntensity * sheenLight * sheenColor * brdf;
}
#endif

vec3 getIBLRadianceSubsurface(vec3 n, vec3 v, float scale, float distortion, float power, vec3 color, float thickness, int materialIndex)
{
    vec3 diffuseLight = vec3(1,1,1);

    diffuseLight = texture(u_LambertianEnvSampler, n).rgb;


    #ifndef USE_HDR
        diffuseLight = sRGBToLinear(diffuseLight);
    #endif

    return diffuseLight * getPunctualRadianceSubsurface(n, v, -v, scale, distortion, power, color, thickness);
}


struct MaterialInfo
{
    vec3 f0;
    float perceptualRoughness;

    vec3 albedoColor;
    float exposure;

    vec3 f90;
    float metallic;

    vec3  n;
    float ior;

    vec3 t;
    vec3 b;

    vec3 baseColor;
    float sheenIntensity;

    vec3 emissive;
    float ao;

    vec3 sheenColor;
    float sheenRoughness;

    float anisotropy;
    vec3 clearcoatF0;

    vec3 clearcoatF90;
    float clearcoatFactor;

    vec3 clearcoatNormal;
    float clearcoatRoughness;

    float subsurfaceScale;
    float subsurfaceDistortion;
    float subsurfacePower;
    float subsurfaceThickness;

    vec3  subsurfaceColor;

    float thinFilmFactor;
    float thinFilmThickness;
    float thickness;

    vec3 absorption;
    float transmission;
    float alphaCutoff;
    float alpha;
};

NormalInfo getVertexNormalInfo(vec3 v)
{
    vec2 UV = getNormalUV();
    vec3 uv_dx = dFdx(vec3(UV, 0.0));
    vec3 uv_dy = dFdy(vec3(UV, 0.0));

    vec3 t_ = (uv_dy.t * dFdx(v_Position) - uv_dx.t * dFdy(v_Position)) / (uv_dx.s * uv_dy.t - uv_dy.s * uv_dx.t);

    vec3 n, t, b, ng;

    #ifdef HAS_TANGENTS
        t = normalize(v_TBN[0]);
        b = normalize(v_TBN[1]);
        ng = normalize(v_TBN[2]);
    #else

    #ifdef HAS_NORMALS
        ng = normalize(v_Normal);
    #else
        ng = normalize(cross(dFdx(v_Position), dFdy(v_Position)));
    #endif

        t = normalize(t_ - ng * dot(ng, t_));
        b = cross(ng, t);
    #endif

    float facing = step(0.0, dot(v, ng)) * 2.0 - 1.0;
    t *= facing;
    b *= facing;
    ng *= facing;

    vec3 direction;
    direction = vec3(1.0, 0.0, 0.0);

    t = mat3(t, b, ng) * normalize(direction);
    b = normalize(cross(ng, t));

    n = ng;

    NormalInfo info;
    info.ng = ng;
    info.t = t;
    info.b = b;
    info.n = n;
    return info;
}

NormalInfo getNormalInfo(vec3 v, int materialIndex)
{
    NormalInfo info = getVertexNormalInfo(v);


    vec3 t = info.t;
    vec3 n = info.n;
    vec3 ng = info.ng;
    vec3 b = info.b;

    vec2 UV = getNormalUV();

    vec3 direction;
    #ifdef MATERIAL_ANISOTROPY
        #ifdef HAS_ANISOTROPY_DIRECTION_MAP
            direction = texture(s_Material[materialIndex].AnisotropyDirectionSampler, getAnisotropyDirectionUV()).xyz * 2.0 - vec3(1.0);
        #else
            direction = s_Material[materialIndex].AnisotropyDirection;
        #endif
        t = mat3(t, b, ng) * normalize(direction);
        b = normalize(cross(ng, t));
    #endif

    if( valid( s_Material[u_MaterialIndex].NormalSampler ) )
    {
        n = texture(s_Material[u_MaterialIndex].NormalSampler, UV).rgb * 2.0 - vec3(1.0);
        n *= vec3(s_Material[materialIndex].NormalScale, s_Material[materialIndex].NormalScale, 1.0);
        n = mat3(t, b, ng) * normalize(n);
    }
    else
    {
        n = ng;
    }

    info.ng = ng;
    info.t = t;
    info.b = b;
    info.n = n;
    return info;
}

vec4 getBaseColor(int materialIndex)
{
    vec4 baseColor = vec4(1, 1, 1, 1);

    #if defined(MATERIAL_SPECULARGLOSSINESS)
        baseColor = s_Material[materialIndex].DiffuseFactor;
    #elif defined(MATERIAL_METALLICROUGHNESS)
        baseColor = s_Material[materialIndex].BaseColorFactor;
    #endif

    #if defined(MATERIAL_SPECULARGLOSSINESS) && defined(HAS_DIFFUSE_MAP)
        baseColor *= sRGBToLinear(texture(s_Material[materialIndex].DiffuseSampler, getDiffuseUV()));
    #elif defined(MATERIAL_METALLICROUGHNESS)
        if( valid(s_Material[materialIndex].BaseColorSampler) )
        {
          baseColor *= sRGBToLinear(texture(s_Material[materialIndex].BaseColorSampler, getBaseColorUV()));
        }
    #endif

    return baseColor * getVertexColor();
}

#ifdef MATERIAL_SPECULARGLOSSINESS

MaterialInfo getSpecularGlossinessInfo(MaterialInfo info, int materialIndex)
{
    info.f0 = s_Material[materialIndex].SpecularFactor;
    info.perceptualRoughness = s_Material[materialIndex].GlossinessFactor;

#ifdef HAS_SPECULAR_GLOSSINESS_MAP
    vec4 sgSample = sRGBToLinear(texture(s_Material[materialIndex].SpecularGlossinessSampler, getSpecularGlossinessUV()));
    info.perceptualRoughness *= sgSample.a ;
    info.f0 *= sgSample.rgb;
#endif

      info.perceptualRoughness = 1.0 - info.perceptualRoughness;
    info.albedoColor = info.baseColor.rgb * (1.0 - max(max(info.f0.r, info.f0.g), info.f0.b));

    return info;
}
#endif

#ifdef MATERIAL_METALLICROUGHNESS_SPECULAROVERRIDE
float getMetallicRoughnessSpecularFactor(int materialIndex)
{
    #ifdef HAS_METALLICROUGHNESS_SPECULAROVERRIDE_MAP
    vec4 specSampler =  texture(s_Material[materialIndex].MetallicRoughnessSpecularSampler, getMetallicRoughnessSpecularUV());
    return 0.08 * s_Material[materialIndex].MetallicRoughnessSpecularFactor * specSampler.a;
#endif
    return  0.08 * s_Material[materialIndex].MetallicRoughnessSpecularFactor;
}
#endif

MaterialInfo getMetallicRoughnessInfo(MaterialInfo info, float f0_ior, int materialIndex)
{
    info.metallic            = s_Material[materialIndex].MetallicFactor;
    info.perceptualRoughness = s_Material[materialIndex].RoughnessFactor;

    if( valid(s_Material[materialIndex].MetallicRoughnessSampler) )
    {
        vec4 mrSample = texture(s_Material[materialIndex].MetallicRoughnessSampler, getMetallicRoughnessUV());

        info.perceptualRoughness *= mrSample.g;
        info.metallic            *= mrSample.b;
    }

#ifdef MATERIAL_METALLICROUGHNESS_SPECULAROVERRIDE
    vec3 f0 = vec3(getMetallicRoughnessSpecularFactor());
#else
    vec3 f0 = vec3(f0_ior);
#endif

    info.albedoColor = mix(info.baseColor.rgb * (vec3(1.0) - f0),  vec3(0), info.metallic);
    info.f0 = mix(f0, info.baseColor.rgb, info.metallic);

    return info;
}

MaterialInfo getSheenInfo(MaterialInfo info, int materialIndex)
{
    info.sheenColor     = s_Material[materialIndex].SheenColorFactor;
    info.sheenIntensity = s_Material[materialIndex].SheenIntensityFactor;
    info.sheenRoughness = s_Material[materialIndex].SheenRoughness;

#ifdef HAS_SHEEN_COLOR_INTENSITY_MAP
        vec4 sheenSample = texture(s_Material[materialIndex].SheenColorIntensitySampler, getSheenUV());
        info.sheenColor *= sheenSample.xyz;
        info.sheenIntensity *= sheenSample.w;
#endif

    return info;
}

#ifdef MATERIAL_SUBSURFACE
MaterialInfo getSubsurfaceInfo(MaterialInfo info, int materialIndex)
{
    info.subsurfaceScale = s_Material[materialIndex].SubsurfaceScale;
    info.subsurfaceDistortion = s_Material[materialIndex].SubsurfaceDistortion;
    info.subsurfacePower = s_Material[materialIndex].SubsurfacePower;
    info.subsurfaceColor = s_Material[materialIndex].SubsurfaceColorFactor;
    info.subsurfaceThickness = s_Material[materialIndex].SubsurfaceThicknessFactor;

    #ifdef HAS_SUBSURFACE_COLOR_MAP
        info.subsurfaceColor *= texture(s_Material[materialIndex].SubsurfaceColorSampler, getSubsurfaceColorUV()).rgb;
    #endif

    #ifdef HAS_SUBSURFACE_THICKNESS_MAP
        info.subsurfaceThickness *= texture(s_Material[materialIndex].SubsurfaceThicknessSampler, getSubsurfaceThicknessUV()).r;
    #endif

    return info;
}
#endif

#ifdef MATERIAL_THIN_FILM

vec3 getThinFilmF0(vec3 f0, vec3 f90, float NdotV, float thinFilmFactor, float thinFilmThickness, int materialIndex)
{
    if (thinFilmFactor == 0.0)
    {
                return f0;
    }

    vec3 lutSample = texture(s_Material[materialIndex].ThinFilmLUT, vec2(thinFilmThickness, NdotV)).rgb - 0.5;
    vec3 intensity = thinFilmFactor * 4.0 * f0 * (1.0 - f0);
    return clamp(intensity * lutSample, 0.0, 1.0);
}


MaterialInfo getThinFilmInfo(MaterialInfo info, int materialIndex)
{
    info.thinFilmFactor = s_Material[materialIndex].ThinFilmFactor;
    info.thinFilmThickness = s_Material[materialIndex].ThinFilmThicknessMaximum / 1200.0;

    #ifdef HAS_THIN_FILM_MAP
        info.thinFilmFactor *= texture(s_Material[materialIndex].ThinFilmSampler, getThinFilmUV()).r;
    #endif

    #ifdef HAS_THIN_FILM_THICKNESS_MAP
        float thicknessSampled = texture(s_Material[materialIndex].ThinFilmThicknessSampler, getThinFilmThicknessUV()).g;
        float thickness = mix(s_Material[materialIndex].ThinFilmThicknessMinimum / 1200.0, s_Material[materialIndex].ThinFilmThicknessMaximum / 1200.0, thicknessSampled);
        info.thinFilmThickness = thickness;
    #endif

    return info;
}
#endif

MaterialInfo getTransmissionInfo(MaterialInfo info, int materialIndex)
{
    info.transmission = s_Material[materialIndex].Transmission;
    return info;
}

MaterialInfo getThicknessInfo(MaterialInfo info, int materialIndex)
{
    info.thickness = 1.0;

    #ifdef MATERIAL_THICKNESS
    info.thickness = s_Material[materialIndex].Thickness;

    #ifdef HAS_THICKNESS_MAP
    info.thickness *= texture(s_Material[materialIndex].ThicknessSampler, getThicknessUV()).r;
    #endif

    #endif

    return info;
}

MaterialInfo getAbsorptionInfo(MaterialInfo info, int materialIndex)
{
    info.absorption = vec3(0.0);

    #ifdef MATERIAL_ABSORPTION
    info.absorption = s_Material[materialIndex].AbsorptionColor;
    #endif

    return info;
}

MaterialInfo getAnisotropyInfo(MaterialInfo info, int materialIndex)
{
    info.anisotropy = s_Material[materialIndex].Anisotropy;

#ifdef HAS_ANISOTROPY_MAP
    info.anisotropy *= texture(s_Material[materialIndex].AnisotropySampler, getAnisotropyUV()).r * 2.0 - 1.0;
#endif

    return info;
}

MaterialInfo getClearCoatInfo(MaterialInfo info, NormalInfo normalInfo, int materialIndex)
{
    info.clearcoatFactor    = s_Material[materialIndex].ClearcoatFactor;
    info.clearcoatRoughness = s_Material[materialIndex].ClearcoatRoughnessFactor;
    info.clearcoatF0 = vec3(0.04);
    info.clearcoatF90 = vec3(clamp(info.clearcoatF0 * 50.0, 0.0, 1.0));

    #ifdef HAS_CLEARCOAT_TEXTURE_MAP
        vec4 ccSample = texture(s_Material[materialIndex].ClearcoatSampler, getClearcoatUV());
        info.clearcoatFactor *= ccSample.r;
    #endif

    #ifdef HAS_CLEARCOAT_ROUGHNESS_MAP
        vec4 ccSampleRough = texture(s_Material[materialIndex].ClearcoatRoughnessSampler, getClearcoatRoughnessUV());
        info.clearcoatRoughness *= ccSampleRough.g;
    #endif

    #ifdef HAS_CLEARCOAT_NORMAL_MAP
        vec4 ccSampleNor = texture(s_Material[materialIndex].ClearcoatNormalSampler, getClearcoatNormalUV());
        info.clearcoatNormal = normalize(ccSampleNor.xyz);
    #else
        info.clearcoatNormal = normalInfo.ng;
    #endif

    info.clearcoatRoughness = clamp(info.clearcoatRoughness, 0.0, 1.0);

    return info;
}



vec4 calculate_PBR_OutputColor(MaterialInfo materialInfo)
{
  vec3 v = normalize(iCamera - v_Position);

  vec3 n = materialInfo.n;
  vec3 t = materialInfo.t;
  vec3 b = materialInfo.b;

  vec3 f_specular = vec3(0.0);
  vec3 f_diffuse = vec3(0.0);
  vec3 f_emissive = vec3(0.0);
  vec3 f_clearcoat = vec3(0.0);
  vec3 f_sheen = vec3(0.0);
  vec3 f_subsurface = vec3(0.0);
  vec3 f_transmission = vec3(0.0);

  vec4 g_finalColor;


  #ifdef USE_IBL
  f_specular += getIBLRadianceGGX(n, v, materialInfo.perceptualRoughness, materialInfo.f0);
  f_diffuse  += getIBLRadianceLambertian(n, materialInfo.albedoColor);

  #ifdef MATERIAL_CLEARCOAT
      f_clearcoat += getIBLRadianceGGX(materialInfo.clearcoatNormal, v, materialInfo.clearcoatRoughness, materialInfo.clearcoatF0);
  #endif

  #ifdef MATERIAL_SHEEN
      f_sheen += getIBLRadianceCharlie(n, v, materialInfo.sheenRoughness, materialInfo.sheenColor, materialInfo.sheenIntensity);
  #endif

  #ifdef MATERIAL_SUBSURFACE
      f_subsurface += getIBLRadianceSubsurface(n, v, materialInfo.subsurfaceScale, materialInfo.subsurfaceDistortion, materialInfo.subsurfacePower, materialInfo.subsurfaceColor, materialInfo.subsurfaceThickness);
  #endif

  #ifdef MATERIAL_TRANSMISSION
      f_transmission += getIBLRadianceTransmission(n, v, materialInfo.perceptualRoughness, materialInfo.ior, materialInfo.baseColor);
  #endif
#endif


float clearcoatFactor = 0.0;
vec3 clearcoatFresnel = vec3(0.0);

#ifdef MATERIAL_CLEARCOAT
    clearcoatFactor = materialInfo.clearcoatFactor;
    clearcoatFresnel = F_Schlick(materialInfo.clearcoatF0, materialInfo.clearcoatF90, clampedDot(materialInfo.clearcoatNormal, v));
#endif

#ifdef USE_PUNCTUAL

  float alphaRoughness = materialInfo.perceptualRoughness * materialInfo.perceptualRoughness;


  int lc = U_LIGHT_COUNT;
  for(int i = 0; i < lc; ++i)
  {

      Light_t light = s_Lights[ U_LIGHT_INDEX + i ];// s_Lights[pushConsts.fragmentStorageIndex0+i];

      vec3 pointToLight = -light.direction;
      float rangeAttenuation = 1.0;
      float spotAttenuation = 1.0;

      if(light.type != LightType_Directional)
      {
          pointToLight = light.position - v_Position;
      }

      if (light.type != LightType_Directional)
      {
          rangeAttenuation = getRangeAttenuation(light.range, length(pointToLight));
      }
      if (light.type == LightType_Spot)
      {
          spotAttenuation = getSpotAttenuation(pointToLight, light.direction, light.outerConeCos, light.innerConeCos);
      }

      vec3 intensity = rangeAttenuation * spotAttenuation * light.intensity * light.color;

      vec3 l = normalize(pointToLight);         vec3 h = normalize(l + v);                float NdotL = clampedDot(n, l);
      float NdotV = clampedDot(n, v);
      float NdotH = clampedDot(n, h);
      float LdotH = clampedDot(l, h);
      float VdotH = clampedDot(v, h);


      if (NdotL > 0.0 || NdotV > 0.0)
      {
           f_diffuse += intensity * NdotL *  BRDF_lambertian(materialInfo.f0, materialInfo.f90, materialInfo.albedoColor, VdotH);

          #ifdef MATERIAL_ANISOTROPY
            vec3 h = normalize(l + v);
            float TdotL = dot(t, l);
            float BdotL = dot(b, l);
            float TdotH = dot(t, h);
            float BdotH = dot(b, h);
            f_specular += intensity * NdotL * BRDF_specularAnisotropicGGX(materialInfo.f0, materialInfo.f90, alphaRoughness,
                VdotH, NdotL, NdotV, NdotH,
                BdotV, TdotV, TdotL, BdotL, TdotH, BdotH, materialInfo.anisotropy);
          #else
            f_specular += intensity * NdotL * BRDF_specularGGX(materialInfo.f0, materialInfo.f90, alphaRoughness, VdotH, NdotL, NdotV, NdotH);
          #endif

          #ifdef MATERIAL_SHEEN
              f_sheen += intensity * getPunctualRadianceSheen(materialInfo.sheenColor, materialInfo.sheenIntensity, materialInfo.sheenRoughness,
                  NdotL, NdotV, NdotH);
          #endif

          #ifdef MATERIAL_CLEARCOAT
              f_clearcoat += intensity * getPunctualRadianceClearCoat(materialInfo.clearcoatNormal, v, l,
                  h, VdotH,
                  materialInfo.clearcoatF0, materialInfo.clearcoatF90, materialInfo.clearcoatRoughness);
          #endif
      }

      #ifdef MATERIAL_SUBSURFACE
          f_subsurface += intensity * getPunctualRadianceSubsurface(n, v, l,
              materialInfo.subsurfaceScale, materialInfo.subsurfaceDistortion, materialInfo.subsurfacePower,
              materialInfo.subsurfaceColor, materialInfo.subsurfaceThickness);
      #endif

      #ifdef MATERIAL_TRANSMISSION
          f_transmission += intensity * getPunctualRadianceTransmission(n, v, l, alphaRoughness, materialInfo.ior, materialInfo.f0);
      #endif
  }
#endif
  f_emissive = materialInfo.emissive;

  vec3 color = vec3(0);




  #ifdef MATERIAL_ABSORPTION
      f_transmission *= transmissionAbsorption(v, n, materialInfo.ior, materialInfo.thickness, materialInfo.absorption);
  #endif

  #ifdef MATERIAL_TRANSMISSION
  vec3 diffuse = mix(f_diffuse, f_transmission, materialInfo.transmission);
  #else
  vec3 diffuse = f_diffuse;
  #endif

  float reflectance = max(max(materialInfo.f0.r, materialInfo.f0.g), materialInfo.f0.b);
  color = (f_emissive + diffuse + f_specular + f_subsurface + (1.0 - reflectance) * f_sheen) * (1.0 - clearcoatFactor * clearcoatFresnel) + f_clearcoat * clearcoatFactor;

  float ao = 1.0;
    #ifdef HAS_OCCLUSION_MAP
            color = color * materialInfo.ao;
        #endif

#ifndef DEBUG_OUTPUT
#ifdef ALPHAMODE_MASK
    if(materialInfo.alpha < materialInfo.alphaCutoff)
    {
        discard;
    }
    #endif

    g_finalColor = vec4( toneMap(color * materialInfo.exposure), materialInfo.alpha);

  switch( s_Material[u_MaterialIndex].DebugOutput )
  {
      case 1:
        g_finalColor.rgb = linearTosRGB(materialInfo.baseColor);
        break;
      case 2:
        g_finalColor.rgb = vec3(materialInfo.metallic);
        break;
      case 3:
        g_finalColor.rgb = vec3(materialInfo.perceptualRoughness);
        break;
      case 4:
        g_finalColor.rgb = materialInfo.n;
        break;
      case 5:
        g_finalColor.rgb = normalize(v_Normal)*0.5f + vec3(0.5);        break;
      case 6:
        g_finalColor.rgb = t * 0.5 + vec3(0.5);         break;
      case 7:
        g_finalColor.rgb = b * 0.5 + vec3(0.5);         break;
      case 8:
        g_finalColor.rgb = vec3(materialInfo.ao);         break;
      case 9:
        g_finalColor.rgb = materialInfo.f0;
        break;
      case 10:
        g_finalColor.rgb = f_emissive;
        break;
      case 11:
        g_finalColor.rgb = f_specular;
        break;
      case 12:
        g_finalColor.rgb = f_diffuse;
        break;
      case 13:
        g_finalColor.rgb = vec3(materialInfo.thickness);
        break;
      case 14:
        g_finalColor.rgb = f_clearcoat;
        break;
      case 15:
        g_finalColor.rgb = f_sheen;
        break;
      case 16:
        g_finalColor.rgb = vec3(materialInfo.alpha);
        break;
      case 17:
        g_finalColor.rgb = f_subsurface;
        break;
      case 18:
        g_finalColor.rgb = linearTosRGB(f_transmission);
        break;
      case 19:
        g_finalColor.rgb = texture(u_LambertianEnvSampler, f_NORMAL).rgb;
        break;
      case 20:
        g_finalColor.rgb = texture(u_GGXEnvSampler, f_NORMAL).rgb;
        break;
      case 21:
        g_finalColor.rg = getBRDF(n, v, materialInfo.perceptualRoughness, materialInfo.f0);
        g_finalColor.b=0.;
        break;
  }

  return g_finalColor;

#endif

}


MaterialInfo getMaterialInfo(int materialIndex)
{
    MaterialInfo materialInfo;
    vec4 g_finalColor;
    vec4 baseColor = getBaseColor(materialIndex);

#ifdef ALPHAMODE_OPAQUE
    baseColor.a = 1.0;
#endif
    materialInfo.exposure = s_Material[materialIndex].Exposure;

    vec3 v = normalize( iCamera - v_Position);
    NormalInfo normalInfo = getNormalInfo(v, materialIndex);
    vec3 n = normalInfo.n;
    vec3 t = normalInfo.t;
    vec3 b = normalInfo.b;

    materialInfo.baseColor = baseColor.rgb;
    #ifdef ALPHAMODE_OPAQUE
        materialInfo.alpha = 1.0;
    #endif

#ifdef MATERIAL_IOR
    float ior = s_Material[materialIndex].IOR_and_f0.x;
    float f0_ior = s_Material[materialIndex].IOR_and_f0.y;
#else
        float ior = 1.5;
    float f0_ior = 0.04;
#endif
    materialInfo.ior = ior;


#ifdef MATERIAL_SPECULARGLOSSINESS
    materialInfo = getSpecularGlossinessInfo(materialInfo, materialIndex);
#endif

#ifdef MATERIAL_METALLICROUGHNESS
    materialInfo = getMetallicRoughnessInfo(materialInfo, f0_ior, materialIndex);
#endif

#ifdef MATERIAL_SHEEN
    materialInfo = getSheenInfo(materialInfo, materialIndex);
#endif

#ifdef MATERIAL_SUBSURFACE
    materialInfo = getSubsurfaceInfo(materialInfo, materialIndex);
#endif

#ifdef MATERIAL_THIN_FILM
    materialInfo = getThinFilmInfo(materialInfo, materialIndex);
#endif

#ifdef MATERIAL_CLEARCOAT
    materialInfo = getClearCoatInfo(materialInfo, normalInfo, materialIndex);
#endif

#ifdef MATERIAL_TRANSMISSION
    materialInfo = getTransmissionInfo(materialInfo, materialIndex);
#endif

#ifdef MATERIAL_ANISOTROPY
    materialInfo = getAnisotropyInfo(materialInfo, materialIndex);
#endif

    materialInfo = getThicknessInfo(materialInfo, materialIndex);
    materialInfo = getAbsorptionInfo(materialInfo, materialIndex);

    materialInfo.perceptualRoughness = clamp(materialInfo.perceptualRoughness, 0.0, 1.0);
    materialInfo.metallic = clamp(materialInfo.metallic, 0.0, 1.0);

        float reflectance = max(max(materialInfo.f0.r, materialInfo.f0.g), materialInfo.f0.b);

        materialInfo.f90 = vec3(clamp(reflectance * 50.0, 0.0, 1.0));

    materialInfo.n = n;
    materialInfo.t = t;
    materialInfo.b = b;

#ifdef MATERIAL_THIN_FILM
    materialInfo.f0 = getThinFilmF0(materialInfo.f0, materialInfo.f90, clampedDot(n, v),
        materialInfo.thinFilmFactor, materialInfo.thinFilmThickness, materialIndex);
#endif

    materialInfo.emissive = s_Material[materialIndex].EmissiveFactor;

#ifdef HAS_EMISSIVE_MAP
    if( valid(s_Material[materialIndex].EmissiveSampler))
    {
        materialInfo.emissive *= sRGBToLinear(texture(s_Material[materialIndex].EmissiveSampler, getEmissiveUV())).rgb;
    }
#endif

  materialInfo.ao = 1.0;
  #ifdef HAS_OCCLUSION_MAP
  if( valid(s_Material[materialIndex].OcclusionSampler))
  {
      materialInfo.ao = texture( s_Material[materialIndex].OcclusionSampler,  getOcclusionUV()).r;
  }
#endif


    return materialInfo;
}






#endif
