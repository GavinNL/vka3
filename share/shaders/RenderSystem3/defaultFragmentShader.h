#ifndef VKA_ECS_DEFAULT_FRAGMENT_SHADER_H
#define VKA_ECS_DEFAULT_FRAGMENT_SHADER_H

//====================================================
// thi sneeds to be included to have textures
#include "textureArray.h"
#include "pushConstants.h"
#include "GLOBAL_UNIFORM.h"
#include "multiBuffer.h"
//====================================================

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec3 f_NORMAL;
layout(location = 2) in vec2 f_TEXCOORD_0;
layout(location = 3) in vec3 f_COLOR_0;

layout(location = 0) out vec4 out_COLOR;

#define u_MaterialIndex                  pushConsts.materialIndex0
#define u_MaterialIndex0                 pushConsts.materialIndex0
#define u_MaterialIndex1                 pushConsts.materialIndex1

#include "PBR.h"

struct VertexInput
{
    vec3 position;
    vec3 normal;
    vec3 texCoord0;
    vec3 texCoord1;
    vec3 color;
};

MULTIBUFFER_S(Environment_t, Environment)

#if defined HAS_FRAGMENT_START
VertexInput FRAGMENT_START(VertexInput v_in);
#endif

#if defined HAS_FRAGMENT_END
vec4 FRAGMENT_END(VertexInput v_in, vec4 outputColor);
#endif

struct PBR_out
{
    vec3  albedoColor;
    vec3  normal;
    vec3  emission;
    float metallic;
    float roughness;
    float occlusion;
    float alpha;
    float alpha_cutoff;
};

vec4 f_out = vec4(1,1,1,1);

// Default PBR Shader
void main()
{
    VertexInput v_in;
    v_in.position     = f_POSITION;
    v_in.normal       = f_NORMAL;
    v_in.texCoord0.xy = f_TEXCOORD_0;
    v_in.color        = f_COLOR_0;

    PBR_out f_out;
    f_out.albedoColor  = v_in.color;
    f_out.normal       = v_in.normal;
    f_out.emission     = vec3(0,0,0);
    f_out.occlusion    = 1.0f;
    f_out.metallic     = 0.0f;
    f_out.roughness    = 1.0f;
    f_out.alpha        = 1.0f;
    f_out.alpha_cutoff = 0.5f;

    #if defined HAS_FRAGMENT_START
      v_in = FRAGMENT_START(v_in);
    #endif

    vec4 outputColor = vec4(v_in.color, 1.0);

    #if defined IGNORE_PBR_MATERIAL
    #else

        if( s_Material[u_MaterialIndex].unlit == 1)
        {
            outputColor = getBaseColor(u_MaterialIndex);
            out_COLOR   = vec4(outputColor.rgb,1.0f);
            return;
        }

        // We need to make sure that we fill in the MaterialInfo struct
        //
        MaterialInfo materialInfo1 = getMaterialInfo(u_MaterialIndex);

        // pass the PBR Material Information into the following method
        // to calculate the final output colour
        outputColor = calculate_PBR_OutputColor(materialInfo1);

    #endif


    #if defined HAS_FRAGMENT_END
      outputColor = FRAGMENT_END(v_in, outputColor);
    #endif

    out_COLOR = vec4(outputColor.rgb,1.0f);
}

#endif
