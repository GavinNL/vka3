layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec3 f_NORMAL;
layout(location = 2) in vec2 f_TEXCOORD_0;
layout(location = 3) in vec3 f_COLOR_0;


FSOut MAIN()
{
    FSOut OUT;

    // pixel output color
    OUT.albedo   = f_COLOR_0.rgb;

    // world space position
    OUT.position = f_POSITION.rgb;

    // world space normal
    OUT.normal   = f_NORMAL.rgb;

    OUT.metallic = 1.0f;

    return OUT;
}
