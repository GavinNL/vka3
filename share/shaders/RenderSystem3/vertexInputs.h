#ifndef RENDERSYSTEM3_VERTEX_INPUTS
#define RENDERSYSTEM3_VERTEX_INPUTS


layout( location = 0) in vec3  in_POSITION;
layout( location = 1) in vec3  in_NORMAL;
layout( location = 2) in vec3  in_TANGENT;
layout( location = 3) in vec2  in_TEXCOORD_0;
layout( location = 4) in vec2  in_TEXCOORD_1;
layout( location = 5) in vec4  in_COLOR_0;
layout( location = 6) in uvec4 in_JOINTS_0;
layout( location = 7) in vec4  in_WEIGHTS_0;

#define v_POSITION   in_POSITION
#define v_NORMAL     in_NORMAL
#define v_TANGENT    in_TANGENT
#define v_TEXCOORD_0 in_TEXCOORD_0
#define v_TEXCOORD_1 in_TEXCOORD_1
#define v_COLOR_0    in_COLOR_0
#define v_JOINTS_0   in_JOINTS_0
#define v_WEIGHTS_0  in_WEIGHTS_0

#endif
