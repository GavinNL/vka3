#ifndef RENDERSYSTEM3_TEXTUREARRAY_INF
#define RENDERSYSTEM3_TEXTUREARRAY_INF

//
#ifndef MAX_TEXTURES_BOUND
  #define MAX_TEXTURES_BOUND 0
#endif

#ifndef MAX_CUBES_BOUND
  #define MAX_CUBES_BOUND 0
#endif

#ifndef TEXTURE_ARRAY_SET
    #define TEXTURE_ARRAY_SET 1
#endif

#ifndef TEXTURE_ARRAY_BINDING
    #define TEXTURE_ARRAY_BINDING 0
#endif

#ifndef TEXTURECUBE_ARRAY_BINDING
    #define TEXTURECUBE_ARRAY_BINDING 1
#endif


#if MAX_TEXTURES_BOUND > 0
  layout(set = TEXTURE_ARRAY_SET, binding = TEXTURE_ARRAY_BINDING)     uniform sampler2D   _INPUT_TEXTURE_ARRAY[MAX_TEXTURES_BOUND];
#endif

#if MAX_CUBES_BOUND > 0
  layout(set = TEXTURE_ARRAY_SET, binding = TEXTURECUBE_ARRAY_BINDING) uniform samplerCube _INPUT_CUBE_ARRAY[MAX_CUBES_BOUND];
#endif


struct sampler2D_id
{
    int index;
};

struct samplerCube_id
{
    int index;
};


bool valid( sampler2D_id d)
{
  return !( d.index<0 || d.index >= MAX_TEXTURES_BOUND);
}
bool valid( samplerCube_id d)
{
  return !( d.index<0 || d.index >= MAX_CUBES_BOUND);
}



vec4 texture( samplerCube_id s, vec3 uv)
{
#if MAX_CUBES_BOUND > 0
     if( !valid(s) ) return vec4(1,1,1,1);
     return texture( _INPUT_CUBE_ARRAY[ s.index ], uv );
#else
     return vec4(1,1,1,1);
#endif
}

vec4 textureLod( samplerCube_id s, vec3 uv, float lod)
{
#if MAX_CUBES_BOUND > 0
      if( !valid(s) ) return vec4(1,1,1,1);
      return textureLod( _INPUT_CUBE_ARRAY[s.index], uv, lod );
#else
    return vec4(1,1,1,1);
#endif

}

float textureQueryLevels(samplerCube_id s)
{
#if MAX_CUBES_BOUND > 0
    if( !valid(s) ) return 0.f;
    return textureQueryLevels( _INPUT_CUBE_ARRAY[s.index] );
#else
     return vec4(1,1,1,1);
#endif
}


vec4 texture( sampler2D_id s, vec2 uv)
{
#if MAX_TEXTURES_BOUND > 0
     if( !valid(s) ) 
        return vec4(1,1,1,1);
     return texture( _INPUT_TEXTURE_ARRAY[ s.index ], uv );
#else
        return vec4(1,1,1,1);
#endif
}

vec4 textureLod( sampler2D_id s, vec2 uv, float lod)
{
#if MAX_TEXTURES_BOUND > 0
     if( !valid(s) ) 
        return vec4(1,1,1,1);
  return textureLod( _INPUT_TEXTURE_ARRAY[s.index], uv, lod );
#else
   return vec4(1,1,1,1);
#endif
}

float textureQueryLevels(sampler2D_id s)
{
#if MAX_TEXTURES_BOUND > 0
    if( !valid(s) ) 
        return 1.f;
    return textureQueryLevels( _INPUT_TEXTURE_ARRAY[s.index] );
#else
   return 1.0f;
#endif
}



#endif

