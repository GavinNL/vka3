#ifndef RENDERSYSTEM3_PUSHCONSTANTS
#define RENDERSYSTEM3_PUSHCONSTANTS

layout(push_constant) uniform PushConsts
{
    int    projMatrixIndex;
    int    viewMatrixIndex;
    int    transformIndex;
    int    projViewMatrixIndex;  // index into the lights uniform buffer

    int    materialIndex0; // index into the materials uniform buffer
    int    materialIndex1;
    int    userStorageIdex;
    int    attributeFlags;

    float  lod; // index into the materials uniform buffer
    int    environmentIndex1;
    int    environmentIndex2;
    int    unused1;

    int    lightIndex;
    int    lightCount;
    int    unused4[2];
} pushConsts;

#define VKA_ATTRIBUTES_FLAGS         pushConsts.attributeFlags
#define VKA_HAS_POSITION    (( VKA_ATTRIBUTES_FLAGS & 0x01) == 0x01)
#define VKA_HAS_NORMAL      (( VKA_ATTRIBUTES_FLAGS & 0x02) == 0x02)
#define VKA_HAS_TANGENT     (( VKA_ATTRIBUTES_FLAGS & 0x04) == 0x04)
#define VKA_HAS_TEXCOORD_0  (( VKA_ATTRIBUTES_FLAGS & 0x08) == 0x08)
#define VKA_HAS_TEXCOORD_1  (( VKA_ATTRIBUTES_FLAGS & 0x10) == 0x10)
#define VKA_HAS_COLOR_0     (( VKA_ATTRIBUTES_FLAGS & 0x20) == 0x20)
#define VKA_HAS_JOINTS_0    (( VKA_ATTRIBUTES_FLAGS & 0x40) == 0x40)
#define VKA_HAS_WEIGHTS_0   (( VKA_ATTRIBUTES_FLAGS & 0x80) == 0x80)

#define U_LIGHT_COUNT pushConsts.lightCount
#define U_LIGHT_INDEX pushConsts.lightIndex

#define U_ENVIRONMENT_0 pushConsts.environmentIndex1
#define U_ENVIRONMENT_1 pushConsts.environmentIndex2

#endif
