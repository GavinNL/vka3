#ifndef RENDERSYSTEM3_MULTIBUFFER_H
#define RENDERSYSTEM3_MULTIBUFFER_H

#define MULTIBUFFER(structType) \
layout(set=0, binding=1) buffer readonly STORAGE_s_ ## structType \
{\
   structType value[];\
} s_ ## structType;

#define MULTIBUFFER_S(structType, label) \
layout(set=0, binding=1) buffer readonly STORAGE_s_ ## label \
{\
   structType value[];\
} s_ ## label;\
structType get ## label(int index)\
{\
    return s_ ## label.value[index];\
}

#endif
