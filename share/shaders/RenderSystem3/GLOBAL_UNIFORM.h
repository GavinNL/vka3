#ifndef RENDERSYSTEM3_GLOBAL_UNIFORM_INL
#define RENDERSYSTEM3_GLOBAL_UNIFORM_INL


layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
{
    vec2  WINDOW;
    float TIME_DELTA;
    float UNUSED0;

    vec3  MOUSE;
    int   FRAME_NUMBER;

    vec3 MOUSE_NEAR;
    float TIME_INT;

    vec3 MOUSE_FAR;
    float TIME_FRAC;

    vec3 CAMERA_POSITION;
    float UNUSED4;

    mat4 VIEW;
    mat4 PROJECTION;
    mat4 PROJECTION_VIEW;
} GLOBAL_UNIFORM;


#define GLOBAL GLOBAL_UNIFORM

#define G_PROJECTION_VIEW GLOBAL_UNIFORM.PROJECTION_VIEW
#define G_VIEW GLOBAL_UNIFORM.VIEW
#define G_PROJECTION GLOBAL_UNIFORM.PROJECTION

#define iFrame         GLOBAL.FRAME_NUMBER
#define iTimeDelta     GLOBAL.TIME_DELTA
#define iResolution    GLOBAL.WINDOW
#define iScreenWidth   GLOBAL.WINDOW.x
#define iScreenHeight  GLOBAL.WINDOW.x
#define iWindow        GLOBAL.WINDOW
#define iTime         (GLOBAL.TIME_INT+GLOBAL.TIME_FRAC)
#define iCamera        GLOBAL.CAMERA_POSITION

#endif
