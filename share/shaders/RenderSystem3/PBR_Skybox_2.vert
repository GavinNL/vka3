//
#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_GOOGLE_include_directive : enable

//===================================================================
// These definitions must be enabled or disabled
// before the call to #include "defaultVertexShader.h"
//===================================================================
//#define HAS_VERTEX_START       // requires VertexOutput VERTEX_START(VertexOutput v_in) {  } ;
//#define HAS_VERTEX_END         // requires VertexOutput VERTEX_END(VertexOutput v_in) { };

// Uncomment this to ignore bone calculations
#define IGNORE_BONES
// uncomment this to ignore model transformations
//#define IGNORE_MODEL_TRANSFORM
//===================================================================

#include "defaultVertexShader.h"


#if defined HAS_VERTEX_START
/**
* This function is called at the beginning of the vertex shader stage,
**/
VertexOutput VERTEX_START(VertexOutput v_in)
{
    return v_in; // dont do anything
}
#endif


#if defined HAS_VERTEX_END
/**
* This function at the end of the vertex stage. After all Bone/model transforms
* have been applied, but before the view and projection matrix
* has been applied
**/
VertexOutput VERTEX_END(VertexOutput v_in)
{
    return v_in; // dont do anything
}
#endif
