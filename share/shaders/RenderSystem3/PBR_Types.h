#ifndef RENDERSYSTEM3_PBR_TYPES_H
#define RENDERSYSTEM3_PBR_TYPES_H

struct Light_t
{
    vec3 direction;
    float range;

    vec3 color;
    float intensity;

    vec3 position;
    float innerConeCos;

    float   outerConeCos;
    int type;
    int unused1;
    int unused2;
};

struct Environment_t
{
    samplerCube_id skybox;
    samplerCube_id radiance;
    samplerCube_id irradiance;
    sampler2D_id   brdf;
};

struct Material_t
{
    int   LightCount;
    float Exposure;
    float AlphaCutoff;
    int   unlit;

    int   NormalUVSet;
    float NormalScale;
    float MetallicFactor ;
    float RoughnessFactor;

    vec4  BaseColorFactor;

    int   BaseColorUVSet;
    int   MetallicRoughnessUVSet;
    float Thickness;
    float Transmission ;

    vec3  SheenColorFactor;
    float SheenIntensityFactor;

    vec3  EmissiveFactor;
    int   EmissiveUVSet;

    float SheenRoughness;
    float ClearcoatFactor;
    float ClearcoatRoughnessFactor;
    float Anisotropy;

    vec2  IOR_and_f0;
    float vertexOffsetDistance;
    float secondsSinceLoaded;

    vec3  AbsorptionColor;
    int   DebugOutput;

    sampler2D_id BaseColorSampler;
    sampler2D_id NormalSampler;
    sampler2D_id MetallicRoughnessSampler;
    sampler2D_id EmissiveSampler;

    sampler2D_id  OcclusionSampler;
    sampler2D_id  ExtraSampler1;
    sampler2D_id  ExtraSampler2;
    sampler2D_id  ExtraSampler3;
};

#endif
