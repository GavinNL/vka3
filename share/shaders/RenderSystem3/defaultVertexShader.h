#ifndef VKA_ECS_DEFAULT_VERTEX_SHADER_H
#define VKA_ECS_DEFAULT_VERTEX_SHADER_H

#include "vertexInputs.h"
#include "GLOBAL_UNIFORM.h"
#include "pushConstants.h"
#include "multiBuffer.h"

layout(location = 0) out vec3 f_POSITION;
layout(location = 1) out vec3 f_NORMAL;
layout(location = 2) out vec2 f_TEXCOORD_0;
layout(location = 3) out vec3 f_COLOR_0;

struct VertexOutput
{
    vec3 position;
    vec3 normal;
    vec4 color;
    vec3 texCoord0;
    vec3 texCoord1;
};

out gl_PerVertex
{
     vec4 gl_Position;
     float gl_PointSize;
};

//=============================================
// Multibuffer used to store all the matrices
// that we might need
MULTIBUFFER_S(mat4, Transform)
//=============================================

mat4 getModelMatrix()
{
    return getTransform( pushConsts.transformIndex );//s_Transforms.value[ u_TransformIndex];
}
mat4 getProjectionViewMatrix()
{
    return getTransform(pushConsts.projViewMatrixIndex);//s_Transforms.value[ u_TransformIndex];
}
mat4 getProjectionMatrix()
{
    return getTransform(pushConsts.projMatrixIndex);//s_Transforms.value[ u_TransformIndex];
}
mat4 getViewMatrix()
{
    return getTransform(pushConsts.viewMatrixIndex);//s_Transforms.value[ u_TransformIndex];
}

mat4 getSkinMatrix()
{
    #if defined VKA_HAS_BONES
    if( VKA_HAS_JOINTS_0 )
    {
        int boneIndexBase = pushConsts.transformIndex + 1;
        return in_WEIGHTS_0.x * s_Transform.value[ boneIndexBase + int(in_JOINTS_0.x) ] +
               in_WEIGHTS_0.y * s_Transform.value[ boneIndexBase + int(in_JOINTS_0.y) ] +
               in_WEIGHTS_0.z * s_Transform.value[ boneIndexBase + int(in_JOINTS_0.z) ] +
               in_WEIGHTS_0.w * s_Transform.value[ boneIndexBase + int(in_JOINTS_0.w) ];
    }
    return mat4(1.0);
    #else
    return mat4(1.0);
    #endif
}

#if defined HAS_VERTEX_START
  VertexOutput VERTEX_START(VertexOutput v_in);
#endif

#if defined HAS_VERTEX_END
  VertexOutput VERTEX_END(VertexOutput v_in);
#endif

void main()
{
    VertexOutput v_out;

    v_out.position     = v_POSITION;
    v_out.normal       = v_NORMAL;
    v_out.color        = v_COLOR_0.xyzw;
    v_out.texCoord0    = vec3(v_TEXCOORD_0, 0);

    float t = float( VKA_HAS_COLOR_0 );
    v_out.color = mix( vec4(1,1,1,1), in_COLOR_0.rgba, t);

    #if defined HAS_VERTEX_START
        v_out = VERTEX_START(v_out);
    #endif

#if defined IGNORE_BONES

        #if defined IGNORE_MODEL_TRANSFORM
            // dont do anything
        #else
            mat4 skinMat    = getModelMatrix();
            v_out.normal    = normalize(transpose(inverse(mat3( skinMat ))) * v_out.normal);
            v_out.position  = (skinMat * vec4( v_out.position, 1.0)).xyz;
        #endif

#else

      #if defined IGNORE_MODEL_TRANSFORM
          // still need to do skin transform
          mat4 skinMat    = getSkinMatrix();
          v_out.normal    = normalize(transpose(inverse(mat3( skinMat ))) * v_out.normal);
          v_out.position  = (skinMat * vec4( v_out.position, 1.0)).xyz;
      #else
          mat4 skinMat    =  getModelMatrix() * getSkinMatrix();
          v_out.normal    = normalize(transpose(inverse(mat3( skinMat ))) * v_out.normal);
          v_out.position  = (skinMat * vec4( v_out.position, 1.0)).xyz;
      #endif

#endif


    float offsetDist = 0.0f;
    v_out.position += offsetDist*v_out.normal;

#if defined HAS_VERTEX_END
        v_out = VERTEX_END(v_out);
#endif

    gl_Position  = getProjectionViewMatrix() * vec4(v_out.position,1.0);

    f_POSITION   = v_out.position.xyz;
    f_NORMAL     = v_out.normal;
    f_TEXCOORD_0 = v_out.texCoord0.xy;
    f_COLOR_0    = v_out.color.rgb;
}

#endif
