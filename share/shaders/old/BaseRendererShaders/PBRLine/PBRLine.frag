#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec4 f_COLOR;

//============================================================
// Output Framebuffer data.
// These output values will go to a separate texture
//============================================================
layout (location = 0) out vec4 out_ALBEDO;
layout (location = 1) out vec4 out_POSITION;
layout (location = 2) out vec4 out_NORMAL;

struct PBROut
{
    vec3 position;
    vec3 normal;
    vec3 albedo;
    float metallic;
    float roughness;
};

PBROut defaultMain()
{
    PBROut OUT;

    OUT.albedo = f_COLOR.rgb;
    OUT.normal = vec3(0,0,0);
    return OUT;
}

// Do not remove this. This will be replaced
__VKA_MAIN_FRAG__

void main()
{
    PBROut OUT = __VKA_FRAG_ENTRY_POINT__;

    //  ================= DO NOT MODIFY =======================
    out_ALBEDO.rgba   = vec4( OUT.albedo.rgb, OUT.metallic);
    out_POSITION.rgba = vec4( OUT.position, 0);
    out_NORMAL.rgba   = vec4( OUT.normal, OUT.roughness);

}
