/**

This is a template of ridgid body pipeline with no lights

*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 in_POSITION;
layout(location = 1) in uint in_COLOR_0;

//============================================================
//
//============================================================
layout(location = 0) out vec3 f_POSITION;
layout(location = 1) out vec4 f_COLOR;

layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
{
    float  SCREEN_WIDTH   ;
    float  SCREEN_HEIGHT  ;
    float  MOUSE_X        ;
    float  MOUSE_Y        ;
    float  MOUSE_Z        ;
    float  TIME_INT       ;
    float  TIME_FRAC      ;
    float  UNUSED1        ;
    vec3   MOUSE_NEAR     ;
    float  UNUSED2        ;
    vec3   MOUSE_FAR      ;
    float  UNUSED3        ;
    vec3   CAMERA_POSITION;
    float  UNUSED4        ;
    mat4   VIEW           ;
    mat4   PROJECTION     ;
    mat4   PROJECTION_VIEW;

} GLOBAL_UNIFORM;

/**
The push constant block is where all the vertex information
is stored for all 3 vertices in the triangle.

Total size of block: 112 bytes
mvp matrix = 64 bytes
vertex = 3 * ( 12 + 4 ) = 48

**/
layout(push_constant) uniform PushConsts
{
    mat4 modelMatrix;
} PUSHCONSTS;

out gl_PerVertex
{
    vec4 gl_Position;
};

struct VertexOut
{
  vec3 position;
  vec3 normal;
  vec3 color;
  vec2 texCoord_0;
};

VertexOut defaultMain()
{
    VertexOut OUT;

    vec4 col
             = vec4(  in_COLOR_0         & 0xFF,
                      (in_COLOR_0 >> 8)   & 0xFF,
                      (in_COLOR_0 >> 16)  & 0xFF,
                      (in_COLOR_0 >> 24)  & 0xFF);
    col   /= 255.0;

    vec4 locPos = PUSHCONSTS.modelMatrix * vec4(in_POSITION,1);

    OUT.position   = locPos.xyz;
    OUT.normal     = vec3(0,0,0);
    OUT.texCoord_0 = vec2(0,0);;
    OUT.color      = col.rgb;

    return OUT;
}

void main()
{
    VertexOut OUT = defaultMain();

    //====================== DO NOT EDIT =====================
    f_POSITION   = OUT.position;//outWorldPos;
    //f_NORMAL     = OUT.normal;//outNormal;
    //f_TEXCOORD_0 = OUT.texCoord_0;//in_TEXCOORD_0;
    f_COLOR      = vec4(OUT.color, 1.0);

    gl_Position  =  GLOBAL_UNIFORM.PROJECTION_VIEW *  vec4( OUT.position, 1.0);
}
