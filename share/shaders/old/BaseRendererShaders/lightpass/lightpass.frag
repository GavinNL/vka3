/**

This Shader is meant to be used with the vka::PBRRenderer
It cannot be compiled on its own, it must be compiled through the PBRRenderer
class as it adds extra data into the shader prior to compilation

*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

#ifndef VKA_MAX_TEXTURES
#define VKA_MAX_TEXTURES 4
#endif

layout(set = 1, binding = 0) uniform sampler2D VKA_INPUT_TEXTURE[VKA_MAX_TEXTURES];

layout(set = 2, binding = 0) uniform samplerCube irradianceTexture;
layout(set = 2, binding = 1) uniform samplerCube prefilteredMap;
layout(set = 2, binding = 2) uniform sampler2D   specularBRDF_LUT;
layout(set = 2, binding = 3) uniform samplerCube   skybox;

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec3 f_NORMAL;
layout(location = 2) in vec2 f_TEXCOORD_0;
layout(location = 3) in vec3 f_COLOR_0;


//============================================================
// Output Framebuffer data.
// These output values will go to a separate texture
//============================================================
layout (location = 0) out vec4 out_ALBEDO;

layout(push_constant) uniform PushConsts
{
    // 16 bytes
    vec2 position;
    vec2 size;

    // 32 bytes
    vec2 screenSize;
    int totalLights;
    int fragmentUniformIndex;

    // 48
    vec3 cameraPosition;
    float skyboxLOD;


    mat4 inverseViewMatrix;
} pushConsts;

#define u_cameraPosition pushConsts.cameraPosition
#define u_screenSize pushConsts.screenSize

struct Light_t
{
      vec4  position;
      vec4  radiance;
};

layout(set = 0, binding = 1) buffer readonly STORAGE_LIGHT_t
{

    Light_t light[];

} STORAGE_LIGHT;


struct PBROut
{
    vec3 position;
    vec3 normal;
    vec3 albedo;
};


vec3 getEyePosition()
{
    return pushConsts.cameraPosition;
}

// Find the normal for this fragment, pulling either from a predefined normal map
// or from the interpolated mesh normal and tangent attributes.
vec3 getNormal()
{
    return texture( VKA_INPUT_TEXTURE[2], f_TEXCOORD_0).rgb;
}
vec3 getNormal(vec2 du)
{
    return texture( VKA_INPUT_TEXTURE[2], f_TEXCOORD_0+du).rgb;
}

vec3 getAlbedo()
{
    return texture( VKA_INPUT_TEXTURE[0], f_TEXCOORD_0).rgb;
}
vec3 getAlbedo(vec2 du)
{
    return texture( VKA_INPUT_TEXTURE[0], f_TEXCOORD_0+du).rgb;
}

vec4 getFragmentPosition()
{
    return texture( VKA_INPUT_TEXTURE[1], f_TEXCOORD_0).rgba;
}
vec4 getFragmentPosition(vec2 du)
{
    return texture( VKA_INPUT_TEXTURE[1], f_TEXCOORD_0+du).rgba;
}

float getDepth()
{
    return texture( VKA_INPUT_TEXTURE[3], f_TEXCOORD_0).r;
}

float getDepth(vec2 du)
{
    return texture( VKA_INPUT_TEXTURE[3], f_TEXCOORD_0+du).r;
}

float getMetallic()
{
    return texture( VKA_INPUT_TEXTURE[0], f_TEXCOORD_0).a;
    return 0.5;
}

float getRoughness()
{
    return texture( VKA_INPUT_TEXTURE[2], f_TEXCOORD_0).a;
    return 0.0;
}

/**
This function should return the colour of the 3d Cube
map
**/
vec3 getIrradiance(vec3 dir)
{
    #if 0
        // Create a very simple horizon reflection
        vec3 sky    = vec3(0.8,0.8,1.0);
        vec3 ground = vec3(0.1,0.1,0.1);

        return mix(ground, sky, smoothstep(-0.03f,0.03f,dir.y));
    #else
    //return vec3(0.8,0.8,1.0);
    // Sample diffuse irradiance at normal direction.
    vec3 irradiance = texture(irradianceTexture, dir).rgb;
    return irradiance;
    #endif

}

vec3 getSpecularIrradiance(vec3 dir, float roughness)
{
    float specularTextureLevels = float( textureQueryLevels(prefilteredMap) );

    vec3 specularIrradiance = textureLod(prefilteredMap, dir, roughness * specularTextureLevels ).rgb;

    return specularIrradiance;
}

vec2 getSpecularBRDF(float cosLo, float roughness)
{
    vec4 v255 = texture(specularBRDF_LUT, vec2(cosLo, roughness));
#if 0
    return v255.rg;
#else
    return vec2( 255.0*v255.g + v255.r , 255.0*v255.a + v255.b ) / 255.0f;
#endif
}

int getNumLights()
{
    return pushConsts.totalLights;
}

//=========================

const float M_PI = 3.141592653589793;
const float c_MinRoughness = 0.04;

const float PBR_WORKFLOW_METALLIC_ROUGHNESS = 0.0;
const float PBR_WORKFLOW_SPECULAR_GLOSINESS = 1.0f;

#define MANUAL_SRGB 1

// Encapsulate the various inputs used by the various functions in the shading equation
// We store values in this struct to simplify the integration of alternative implementations
// of the shading terms, outlined in the Readme.MD Appendix.
struct PBRInfo
{
	float NdotL;                  // cos angle between normal and light direction
	float NdotV;                  // cos angle between normal and view direction
	float NdotH;                  // cos angle between normal and half vector
	float LdotH;                  // cos angle between light direction and half vector
	float VdotH;                  // cos angle between view direction and half vector
	float perceptualRoughness;    // roughness value, as authored by the model creator (input to shader)
	float metalness;              // metallic value at the surface
	vec3 reflectance0;            // full reflectance color (normal incidence angle)
	vec3 reflectance90;           // reflectance color at grazing angle
	float alphaRoughness;         // roughness mapped to a more linear change in the roughness (proposed by [2])
	vec3 diffuseColor;            // color contribution from diffuse lighting
	vec3 specularColor;           // color contribution from specular lighting
};

vec3 Uncharted2Tonemap(vec3 color)
{
	float A = 0.15;
	float B = 0.50;
	float C = 0.10;
	float D = 0.20;
	float E = 0.02;
	float F = 0.30;
	float W = 11.2;
	return ((color*(A*color+C*B)+D*E)/(color*(A*color+B)+D*F))-E/F;
}

//vec4 tonemap(vec4 color, float exposure, float gamma)
//{
//	vec3 outcol = Uncharted2Tonemap(color.rgb * exposure);
//	outcol = outcol * (1.0f / Uncharted2Tonemap(vec3(11.2f)));
//	return vec4(pow(outcol, vec3(1.0f / gamma)), color.a);
//}
vec4 tonemap(vec4 color)
{
    return color;
}


// Physically Based Rendering
// Copyright (c) 2017-2018 Michał Siejak

// Physically Based shading model: Lambetrtian diffuse BRDF + Cook-Torrance microfacet specular BRDF + IBL for ambient.

// This implementation is based on "Real Shading in Unreal Engine 4" SIGGRAPH 2013 course notes by Epic Games.
// See: http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf

const float PI = 3.141592;
const float Epsilon = 0.00001;

// Constant normal incidence Fresnel factor for all dielectrics.
const vec3 Fdielectric = vec3(0.04);

// GGX/Towbridge-Reitz normal distribution function.
// Uses Disney's reparametrization of alpha = roughness^2.
float ndfGGX(float cosLh, float roughness)
{
	float alpha   = roughness * roughness;
	float alphaSq = alpha * alpha;

	float denom = (cosLh * cosLh) * (alphaSq - 1.0) + 1.0;
	return alphaSq / (PI * denom * denom);
}

// Single term for separable Schlick-GGX below.
float gaSchlickG1(float cosTheta, float k)
{
	return cosTheta / (cosTheta * (1.0 - k) + k);
}

// Schlick-GGX approximation of geometric attenuation function using Smith's method.
float gaSchlickGGX(float cosLi, float cosLo, float roughness)
{
	float r = roughness + 1.0;
	float k = (r * r) / 8.0; // Epic suggests using this roughness remapping for analytic lights.
	return gaSchlickG1(cosLi, k) * gaSchlickG1(cosLo, k);
}

// Shlick's approximation of the Fresnel factor.
vec3 fresnelSchlick(vec3 F0, float cosTheta)
{
	return F0 + (vec3(1.0) - F0) * pow(1.0 - cosTheta, 5.0);
}



//==============================================================================


float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

PBROut defaultMain()
{
    PBROut OUT;
    //OUT.albedo = vec3( texture(specularBRDF_LUT, f_TEXCOORD_0).rg, 0);
    //return OUT;
    vec4 fragOut = vec4(0,0,0,1);

	// Sample input textures to get shading model params.
	vec3  albedo    = getAlbedo();//texture(albedoTexture, vin.texcoord).rgb;
	float metalness = getMetallic();//texture(metalnessTexture, vin.texcoord).r;
	float roughness = getRoughness();//texture(roughnessTexture, vin.texcoord).r;
    float depth     = getDepth();

    vec3 eyePosition      = getEyePosition();
    vec4 fragmentPosition4 = getFragmentPosition();
    vec3 fragmentPosition  = fragmentPosition4.rgb;


	// Outgoing light direction (vector from world-space fragment position to the "eye").
	vec3 Lo = normalize( eyePosition - fragmentPosition );

	// Get current fragment's normal and transform to world space.
	vec3 surfaceNormal = getNormal();//normalize(2.0 * texture(normalTexture, vin.texcoord).rgb - 1.0);

    if( fragmentPosition4.a < 0.0001)
    {
        OUT.albedo = albedo;
        return OUT;
    }


    if( depth > 0.99999f )
    {
        OUT.albedo = textureLod(skybox, f_NORMAL, 0.0f).rgb;
        return OUT;
    }

	// Angle between surface normal and outgoing light direction.
	float cosLo = max(0.0, dot(surfaceNormal, Lo));

	// Specular reflection vector.
	vec3 Lr = 2.0 * cosLo * surfaceNormal - Lo;

	// Fresnel reflectance at normal incidence (for metals use albedo color).
	vec3 F0 = mix(Fdielectric, albedo, metalness);

	// Direct lighting calculation for analytical lights.
	vec3 directLighting = vec3(0);

    int NumLights = getNumLights();

	for(int i=0; i<NumLights; ++i)
	{
        int lightIndex = pushConsts.fragmentUniformIndex+i;

        vec3 lightPosition = STORAGE_LIGHT.light[lightIndex].position.xyz;
		vec3 Lradiance     = STORAGE_LIGHT.light[lightIndex].radiance.rgb;

        float dist        = length(lightPosition - fragmentPosition);
        float attenuation = 1.0 / (dist * dist);

        Lradiance *= attenuation;
        // incoming light direction.
		vec3 Li        = normalize(lightPosition - fragmentPosition );//-UNIFORM.LIGHT[i].direction;


		// Half-vector between Li and Lo.
		vec3 Lh = normalize(Li + Lo);

		// Calculate angles between surface normal and various light vectors.
		float cosLi = max(0.0, dot(surfaceNormal, Li));
		float cosLh = max(0.0, dot(surfaceNormal, Lh));

		// Calculate Fresnel term for direct lighting.
		vec3 F  = fresnelSchlick(F0, max(0.0, dot(Lh, Lo)));
		// Calculate normal distribution for specular BRDF.
		float D = ndfGGX(cosLh, roughness);
		// Calculate geometric attenuation for specular BRDF.
		float G = gaSchlickGGX(cosLi, cosLo, roughness);

		// Diffuse scattering happens due to light being refracted multiple times by a dielectric medium.
		// Metals on the other hand either reflect or absorb energy, so diffuse contribution is always zero.
		// To be energy conserving we must scale diffuse BRDF contribution based on Fresnel factor & metalness.
		vec3 kd = mix(vec3(1.0) - F, vec3(0.0), metalness);

		// Lambert diffuse BRDF.
		// We don't scale by 1/PI for lighting & material units to be more convenient.
		// See: https://seblagarde.wordpress.com/2012/01/08/pi-or-not-to-pi-in-game-lighting-equation/
		vec3 diffuseBRDF = kd * albedo;

		// Cook-Torrance specular microfacet BRDF.
		vec3 specularBRDF = (F * D * G) / max(Epsilon, 4.0 * cosLi * cosLo);

		// Total contribution for this light.
		directLighting += (diffuseBRDF + specularBRDF) * Lradiance * cosLi;
	}



	// Ambient lighting (IBL).
	vec3 ambientLighting = vec3(1.0,1.00,1.0);
#if 1
	{
		// Sample diffuse irradiance at normal direction.
		vec3 irradiance = getIrradiance(surfaceNormal);//texture(irradianceTexture, surfaceNormal).rgb;

		// Calculate Fresnel term for ambient lighting.
		// Since we use pre-filtered cubemap(s) and irradiance is coming from many directions
		// use cosLo instead of angle with light's half-vector (cosLh above).
		// See: https://seblagarde.wordpress.com/2011/08/17/hello-world/
		vec3 F = fresnelSchlick(F0, cosLo);

		// Get diffuse contribution factor (as with direct lighting).
		vec3 kd = mix( vec3(1.0) - F, vec3(0.0), metalness);

		// Irradiance map contains exitant radiance assuming Lambertian BRDF, no need to scale by 1/PI here either.
		vec3 diffuseIBL = kd * albedo * irradiance;

		// Sample pre-filtered specular reflection environment at correct mipmap level.
		vec3 specularIrradiance   = getSpecularIrradiance(Lr, roughness);

		// Split-sum approximation factors for Cook-Torrance specular BRDF.
        vec2 specularBRDF = getSpecularBRDF(cosLo, roughness);//texture(specularBRDF_LUT, vec2(cosLo, roughness)).rg;


		// Total specular IBL contribution.
		vec3 specularIBL = (F0 * specularBRDF.x + specularBRDF.y) * specularIrradiance;

		// Total ambient lighting contribution.
		ambientLighting = diffuseIBL + specularIBL;

//        vec3 reflection    = -normalize(reflect(Lo, surfaceNormal));
//        vec3 specularColor = mix(F0, albedo.rgb, metalness);
	}

#endif
	// Final fragment color.
	fragOut = vec4(directLighting + ambientLighting, 1.0);
    OUT.albedo = fragOut.rgb;


    #define DEPTH_B 0.9999f
    if( depth > DEPTH_B )
    {
        vec3 skyColor = textureLod(skybox, f_NORMAL, 0.0f).rgb;
        float    t =  smoothstep( DEPTH_B, 1.0, depth);
        OUT.albedo = mix(OUT.albedo, skyColor, t);
    }


    return OUT;
}


__VKA_MAIN_FRAG__

void main()
{
    PBROut OUT = __VKA_FRAG_ENTRY_POINT__;

    //  ================= DO NOT MODIFY =======================
    out_ALBEDO.rgba   = vec4( OUT.albedo.rgb, 1.0);
    //out_POSITION.rgba = vec4( OUT.position, 1);
    //out_NORMAL.rgba   = vec4( OUT.normal, 1);

}
