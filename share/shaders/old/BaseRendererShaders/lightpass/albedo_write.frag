float map(float value, float min1, float max1, float min2, float max2) {
  return min2 + (value - min1) * (max2 - min2) / (max1 - min1);
}

PBROut fragmentOut()
{
    #define MIN_FOG 45.0f
    #define MAX_FOG 50.0f

    PBROut ot;
    float d = getDepth();

    if( d > 0.999999)
    {

        ot.albedo = textureLod(skybox, f_NORMAL, 0.0f).rgb;
        return  ot;
    }
    ot.albedo   = getAlbedo().xyz;
    vec3 pos = getFragmentPosition().xyz;
    float dist = length(pos - u_cameraPosition);

    //=====================
    #if 0
    float du = 1.0f/u_screenSize.x;
    float dv = 1.0f/u_screenSize.y;

    float dpx = getFragmentPosition( vec2(du,0.f)  ).r - getFragmentPosition( vec2(-du, 0.0f) ).r;
    float dpy = getFragmentPosition( vec2(0.f, dv) ).g - getFragmentPosition( vec2(0.f, -dv)  ).g;
    float dp = length( vec2(dpx,dpy));

    if( dp > 0.1)
    {
      ot.albedo = vec3(1.f,1.f,0.f);
      return ot;
    }
    #endif
    //=====================


    if( dist > MIN_FOG)
    {
        vec3 skybox = textureLod(skybox, f_NORMAL, 0.0f).rgb;
        dist = map( dist, MIN_FOG, MAX_FOG, 0.0f, 1.0f);
        dist = clamp(dist, 0,1);
        ot.albedo = mix(ot.albedo, skybox, dist);
    }
    else if( dist > MAX_FOG)
    {
      vec3 skybox = textureLod(prefilteredMap, f_NORMAL, 0.0f).rgb;
      ot.albedo = skybox;
    }

    //ot.position = getFragmentPosition().xyz;
    //ot.normal   = getNormal().xyz;


    return ot;
}
