//
// This fragment shader defines a reference implementation for Physically Based Shading of
// a microfacet surface material defined by a glTF model.
//
// References:
// [1] Real Shading in Unreal Engine 4
//     http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf
// [2] Physically Based Shading at Disney
//     http://blog.selfshadow.com/publications/s2012-shading-course/burley/s2012_pbs_disney_brdf_notes_v3.pdf
// [3] README.md - Environment Maps
//     https://github.com/KhronosGroup/glTF-WebGL-PBR/#environment-maps
// [4] "An Inexpensive BRDF Model for Physically based Rendering" by Christophe Schlick
//     https://www.cs.virginia.edu/~jdl/bib/appearance/analytic%20models/schlick94b.pdf
// [5] "KHR_materials_clearcoat"
//     https://github.com/ux3d/glTF/tree/KHR_materials_pbrClearcoat/extensions/2.0/Khronos/KHR_materials_clearcoat
// [6] "KHR_materials_specular"
//     https://github.com/ux3d/glTF/tree/KHR_materials_pbrClearcoat/extensions/2.0/Khronos/KHR_materials_specular
// [7] "KHR_materials_subsurface"
//     https://github.com/KhronosGroup/glTF/pull/1766
// [8] "KHR_materials_thinfilm"
//     https://github.com/ux3d/glTF/tree/extensions/KHR_materials_thinfilm/extensions/2.0/Khronos/KHR_materials_thinfilm

#define v_UVCoord1 f_TEXCOORD_0
#define v_UVCoord2 f_TEXCOORD_0
#define v_Position f_POSITION
#define v_Normal   f_NORMAL
#define v_Color    f_COLOR_0

#define ALPHAMODE_MASK
#define ALPHAMODE_OPAQUE
//#define DEBUG_ALPHA
//#define DEBUG_BASECOLOR
//#define DEBUG_BITANGENT
//#define DEBUG_F0
//#define DEBUG_FCLEARCOAT
//#define DEBUG_FDIFFUSE
//#define DEBUG_FEMISSIVE
//#define DEBUG_FSHEEN
//#define DEBUG_FSPECULAR
//#define DEBUG_FSUBSURFACE
//#define DEBUG_FTRANSMISSION
//#define DEBUG_METALLIC
//#define DEBUG_NORMAL
//#define DEBUG_OCCLUSION
//#define DEBUG_ROUGHNESS
//#define DEBUG_TANGENT
//#define DEBUG_THICKNESS



#define HAS_BASE_COLOR_MAP

#define HAS_NORMALS
#define HAS_NORMAL_MAP
//#define HAS_NORMAL_UV_TRANSFORM

#define HAS_METALLIC_ROUGHNESS_MAP
//#define HAS_METALLICROUGHNESS_UV_TRANSFORM

#define HAS_EMISSIVE_MAP
//#define HAS_EMISSIVE_UV_TRANSFORM

//#define HAS_OCCLUSION_MAP
//#define HAS_OCCLUSION_UV_TRANSFORM


#define HAS_VERTEX_COLOR_VEC3

#define MATERIAL_IOR

#define MATERIAL_METALLICROUGHNESS
//#define HAS_BASECOLOR_UV_TRANSFORM

#define MATERIAL_TRANSMISSION

#define MATERIAL_UNLIT

#define MATERIAL_ABSORPTION




//#define MATERIAL_ANISOTROPY
//#define HAS_ANISOTROPY_DIRECTION_MAP
//#define HAS_ANISOTROPY_DIRECTION_UV_TRANSFORM
//#define HAS_ANISOTROPY_MAP
//#define HAS_ANISOTROPY_UV_TRANSFORM

//#define MATERIAL_CLEARCOAT
//#define HAS_CLEARCOAT_NORMAL_MAP
//#define HAS_CLEARCOATNORMAL_UV_TRANSFORM
//#define HAS_CLEARCOAT_ROUGHNESS_MAP
//#define HAS_CLEARCOATROUGHNESS_UV_TRANSFORM
//#define HAS_CLEARCOAT_TEXTURE_MAP
//#define HAS_CLEARCOAT_UV_TRANSFORM

//#define MATERIAL_METALLICROUGHNESS_SPECULAROVERRIDE
//#define HAS_METALLICROUGHNESS_SPECULAROVERRIDE_MAP
//#define HAS_METALLICROUGHNESSSPECULAR_UV_TRANSFORM

//#define MATERIAL_SHEEN
//#define HAS_SHEEN_COLOR_INTENSITY_MAP
//#define HAS_SHEENCOLORINTENSITY_UV_TRANSFORM

//#define MATERIAL_SPECULARGLOSSINESS
//#define HAS_SPECULAR_GLOSSINESS_MAP
//#define HAS_SPECULARGLOSSINESS_UV_TRANSFORM
//#define HAS_DIFFUSE_UV_TRANSFORM

//#define MATERIAL_SUBSURFACE
//#define HAS_SUBSURFACE_COLOR_MAP
//#define HAS_SUBSURFACECOLOR_UV_TRANSFORM
//#define HAS_SUBSURFACE_THICKNESS_MAP
//#define HAS_SUBSURFACETHICKNESS_UV_TRANSFORM

//#define MATERIAL_THICKNESS
#define HAS_THICKNESS_MAP
#define HAS_THICKNESS_UV_TRANSFORM


//#define MATERIAL_THIN_FILM
//#define HAS_THIN_FILM_MAP
//#define HAS_THIN_FILM_THICKNESS_MAP
//#define HAS_THIN_FILM_THICKNESS_UV_TRANSFORM
//#define HAS_THIN_FILM_UV_TRANSFORM

#define TONEMAP_ACES
#define TONEMAP_HEJLRICHARD
#define TONEMAP_UNCHARTED
#define USE_IBL
#define USE_PUNCTUAL

//#define HAS_TANGENTS
//#define HAS_VERTEX_COLOR_VEC4

struct Light
{
    vec3 direction;
    float range;

    vec3 color;
    float intensity;

    vec3 position;
    float innerConeCos;

    float outerConeCos;
    int type;

    vec2 padding;
};

#define u_Camera GLOBAL.CAMERA_POSITION

// Each of these uniforms are grouped by alignment
uniform int   u_LightCount;
uniform float u_Exposure;
uniform float u_AlphaCutoff;
uniform int   u_unlit;

uniform int   u_NormalUVSet;
uniform float u_NormalScale;
uniform float u_MetallicFactor;
uniform float u_RoughnessFactor;

uniform vec4  u_BaseColorFactor;

uniform int    u_BaseColorUVSet;
uniform int    u_MetallicRoughnessUVSet;
uniform float  u_Thickness;
uniform float  u_Transmission;

uniform vec3   u_SheenColorFactor;
uniform float  u_SheenIntensityFactor;

uniform vec3   u_EmissiveFactor;
uniform int    u_EmissiveUVSet;


uniform float u_SheenRoughness;
uniform float u_ClearcoatFactor;
uniform float u_ClearcoatRoughnessFactor;
uniform float u_Anisotropy;



uniform vec2  u_IOR_and_f0;
uniform vec2  u_unused2;

uniform vec3  u_AbsorptionColor;
uniform int   u_DebugOutput;


uniform sampler2D_id u_BaseColorSampler;
uniform sampler2D_id u_NormalSampler;
uniform sampler2D_id u_MetallicRoughnessSampler;
uniform sampler2D_id u_EmissiveSampler;

uniform int            u_MipCount;
uniform samplerCube_id u_LambertianEnvSampler;
uniform samplerCube_id u_GGXEnvSampler;
uniform sampler2D_id   u_GGXLUT;

storageArray Light s_Lights;

//unused_uniform int    u_OcclusionUVSet;
//unused_uniform float  u_OcclusionStrength;
//unused_uniform vec3  u_SpecularFactor;
//unused_uniform vec4  u_DiffuseFactor;
//unused_uniform float u_GlossinessFactor;
//unused_uniform float u_MetallicRoughnessSpecularFactor;
//unused_uniform vec3  u_AnisotropyDirection;
//unused_uniform float u_SubsurfaceScale;
//unused_uniform float u_SubsurfaceDistortion;
//unused_uniform float u_SubsurfacePower;
//unused_uniform vec3  u_SubsurfaceColorFactor;
//unused_uniform float u_SubsurfaceThicknessFactor;
//unused_uniform float u_ThinFilmFactor;
//unused_uniform float u_ThinFilmThicknessMinimum;
//unused_uniform float u_ThinFilmThicknessMaximum;

//unused_uniform mat3   u_NormalUVTransform;
//unused_uniform mat3   u_EmissiveUVTransform;
//unused_uniform mat3   u_OcclusionUVTransform;
//unused_uniform mat3   u_BaseColorUVTransform;
//unused_uniform mat3   u_MetallicRoughnessUVTransform;
//unused_uniform int    u_DiffuseUVSet;
//unused_uniform mat3   u_DiffuseUVTransform;
//unused_uniform int    u_SpecularGlossinessUVSet;
//unused_uniform mat3   u_SpecularGlossinessUVTransform;
//unused_uniform int    u_ClearcoatUVSet;
//unused_uniform mat3   u_ClearcoatUVTransform;
//unused_uniform int    u_ClearcoatRoughnessUVSet;
//unused_uniform mat3   u_ClearcoatRoughnessUVTransform;
//unused_uniform int    u_ClearcoatNormalUVSet;
//unused_uniform mat3   u_ClearcoatNormalUVTransform;
//unused_uniform int    u_SheenColorIntensityUVSet;
//unused_uniform mat3   u_SheenColorIntensityUVTransform;
//unused_uniform int    u_MetallicRougnessSpecularTextureUVSet;
//unused_uniform mat3   u_MetallicRougnessSpecularUVTransform;
//unused_uniform int    u_SubsurfaceColorUVSet;
//unused_uniform mat3   u_SubsurfaceColorUVTransform;
//unused_uniform int    u_SubsurfaceThicknessUVSet;
//unused_uniform mat3   u_SubsurfaceThicknessUVTransform;
//unused_uniform int    u_ThinFilmUVSet;
//unused_uniform mat3   u_ThinFilmUVTransform;
//unused_uniform int    u_ThinFilmThicknessUVSet;
//unused_uniform mat3   u_ThinFilmThicknessUVTransform;
//unused_uniform int    u_ThicknessUVSet;
//unused_uniform mat3   u_ThicknessUVTransform;
//unused_uniform int    u_AnisotropyUVSet;
//unused_uniform mat3   u_AnisotropyUVTransform;
//unused_uniform int    u_AnisotropyDirectionUVSet;
//unused_uniform mat3   u_AnisotropyDirectionUVTransform;

//unused_uniform sampler2D_id u_OcclusionSampler;
//unused_uniform sampler2D_id u_DiffuseSampler;
//unused_uniform sampler2D_id u_SpecularGlossinessSampler;
//unused_uniform sampler2D_id u_ClearcoatSampler;
//unused_uniform sampler2D_id u_ClearcoatRoughnessSampler;
//unused_uniform sampler2D_id u_ClearcoatNormalSampler;
//unused_uniform sampler2D_id u_SheenColorIntensitySampler;
//unused_uniform sampler2D_id u_MetallicRoughnessSpecularSampler;
//unused_uniform sampler2D_id u_SubsurfaceColorSampler;
//unused_uniform sampler2D_id u_SubsurfaceThicknessSampler;
//unused_uniform sampler2D_id u_ThinFilmLUT;
//unused_uniform sampler2D_id u_ThinFilmSampler;
//unused_uniform sampler2D_id u_ThinFilmThicknessSampler;
//unused_uniform sampler2D_id u_ThicknessSampler;
//unused_uniform sampler2D_id u_AnisotropySampler;
//unused_uniform sampler2D_id u_AnisotropyDirectionSampler;

//unused_uniform samplerCube_id u_CharlieEnvSampler;
//unused_uniform sampler2D_id   u_CharlieLUT;



//===========================================================
//#include <tonemapping.glsl>
//===========================================================

const float GAMMA = 2.2;
const float INV_GAMMA = 1.0 / GAMMA;

// linear to sRGB approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 linearTosRGB(vec3 color)
{
    return pow(color, vec3(INV_GAMMA));
}

// sRGB to linear approximation
// see http://chilliant.blogspot.com/2012/08/srgb-approximations-for-hlsl.html
vec3 sRGBToLinear(vec3 srgbIn)
{
    return vec3(pow(srgbIn.xyz, vec3(GAMMA)));
}

vec4 sRGBToLinear(vec4 srgbIn)
{
    return vec4(sRGBToLinear(srgbIn.xyz), srgbIn.w);
}

// Uncharted 2 tone map
// see: http://filmicworlds.com/blog/filmic-tonemapping-operators/
vec3 toneMapUncharted2Impl(vec3 color)
{
    const float A = 0.15;
    const float B = 0.50;
    const float C = 0.10;
    const float D = 0.20;
    const float E = 0.02;
    const float F = 0.30;
    return ((color*(A*color+C*B)+D*E)/(color*(A*color+B)+D*F))-E/F;
}

vec3 toneMapUncharted(vec3 color)
{
    const float W = 11.2;
    color = toneMapUncharted2Impl(color * 2.0);
    vec3 whiteScale = 1.0 / toneMapUncharted2Impl(vec3(W));
    return linearTosRGB(color * whiteScale);
}

// Hejl Richard tone map
// see: http://filmicworlds.com/blog/filmic-tonemapping-operators/
vec3 toneMapHejlRichard(vec3 color)
{
    color = max(vec3(0.0), color - vec3(0.004));
    return (color*(6.2*color+.5))/(color*(6.2*color+1.7)+0.06);
}

// ACES tone map
// see: https://knarkowicz.wordpress.com/2016/01/06/aces-filmic-tone-mapping-curve/
vec3 toneMapACES(vec3 color)
{
    const float A = 2.51;
    const float B = 0.03;
    const float C = 2.43;
    const float D = 0.59;
    const float E = 0.14;
    return linearTosRGB(clamp((color * (A * color + B)) / (color * (C * color + D) + E), 0.0, 1.0));
}

vec3 toneMap(vec3 color)
{
    color *= u_Exposure;

#ifdef TONEMAP_UNCHARTED
    return toneMapUncharted(color);
#endif

#ifdef TONEMAP_HEJLRICHARD
    return toneMapHejlRichard(color);
#endif

#ifdef TONEMAP_ACES
    return toneMapACES(color);
#endif

    return linearTosRGB(color);
}
//===========================================================




//===========================================================
//#include <textures.glsl>
//===========================================================

vec2 getNormalUV()
{
    vec3 uv = vec3(u_NormalUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_NORMAL_UV_TRANSFORM
    uv *= u_NormalUVTransform;
    #endif

    return uv.xy;
}

vec2 getEmissiveUV()
{
    vec3 uv = vec3(u_EmissiveUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_EMISSIVE_UV_TRANSFORM
    uv *= u_EmissiveUVTransform;
    #endif

    return uv.xy;
}

#ifdef HAS_OCCLUSION_MAP
vec2 getOcclusionUV()
{
    vec3 uv = vec3(u_OcclusionUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_OCCLUSION_UV_TRANSFORM
    uv *= u_OcclusionUVTransform;
    #endif

    return uv.xy;
}
#endif

vec2 getBaseColorUV()
{
    vec3 uv = vec3(u_BaseColorUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_BASECOLOR_UV_TRANSFORM
    uv *= u_BaseColorUVTransform;
    #endif

    return uv.xy;
}

vec2 getMetallicRoughnessUV()
{
    vec3 uv = vec3(u_MetallicRoughnessUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_METALLICROUGHNESS_UV_TRANSFORM
    uv *= u_MetallicRoughnessUVTransform;
    #endif

    return uv.xy;
}

#ifdef MATERIAL_SPECULARGLOSSINESS
vec2 getSpecularGlossinessUV()
{
    vec3 uv = vec3(u_SpecularGlossinessUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_SPECULARGLOSSINESS_UV_TRANSFORM
    uv *= u_SpecularGlossinessUVTransform;
    #endif

    return uv.xy;
}


vec2 getDiffuseUV()
{
    vec3 uv = vec3(u_DiffuseUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_DIFFUSE_UV_TRANSFORM
    uv *= u_DiffuseUVTransform;
    #endif

    return uv.xy;
}

#endif


#ifdef MATERIAL_CLEARCOAT
vec2 getClearcoatUV()
{
    vec3 uv = vec3(u_ClearcoatUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);
    #ifdef HAS_CLEARCOAT_UV_TRANSFORM
    uv *= u_ClearcoatUVTransform;
    #endif
    return uv.xy;
}

vec2 getClearcoatRoughnessUV()
{
    vec3 uv = vec3(u_ClearcoatRoughnessUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);
    #ifdef HAS_CLEARCOATROUGHNESS_UV_TRANSFORM
    uv *= u_ClearcoatRoughnessUVTransform;
    #endif
    return uv.xy;
}

vec2 getClearcoatNormalUV()
{
    vec3 uv = vec3(u_ClearcoatNormalUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);
    #ifdef HAS_CLEARCOATNORMAL_UV_TRANSFORM
    uv *= u_ClearcoatNormalUVTransform;
    #endif
    return uv.xy;
}
#endif

#ifdef MATERIAL_SHEEN
vec2 getSheenUV()
{
    vec3 uv = vec3(u_SheenColorIntensityUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);
    #ifdef HAS_SHEENCOLORINTENSITY_UV_TRANSFORM
    uv *= u_SheenColorIntensityUVTransform;
    #endif
    return uv.xy;
}
#endif


#ifdef MATERIAL_METALLICROUGHNESS_SPECULAROVERRIDE
vec2 getMetallicRoughnessSpecularUV()
{
    vec3 uv = vec3(u_MetallicRougnessSpecularTextureUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);
    #ifdef HAS_METALLICROUGHNESSSPECULAR_UV_TRANSFORM
    uv *= u_MetallicRougnessSpecularUVTransform;
    #endif
    return uv.xy;
}
#endif

#ifdef MATERIAL_SUBSURFACE
vec2 getSubsurfaceColorUV()
{
    vec3 uv = vec3(u_SubsurfaceColorUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);
    #ifdef HAS_SUBSURFACECOLOR_UV_TRANSFORM
    uv *= u_SubsurfaceColorUVTransform;
    #endif
    return uv.xy;
}

vec2 getSubsurfaceThicknessUV()
{
    vec3 uv = vec3(u_SubsurfaceThicknessUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);
    #ifdef HAS_SUBSURFACETHICKNESS_UV_TRANSFORM
    uv *= u_SubsurfaceThicknessUVTransform;
    #endif
    return uv.xy;
}
#endif

#ifdef MATERIAL_THIN_FILM
vec2 getThinFilmUV()
{
    vec3 uv = vec3(u_ThinFilmUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_THIN_FILM_UV_TRANSFORM
    uv *= u_ThinFilmUVTransform;
    #endif

    return uv.xy;
}

vec2 getThinFilmThicknessUV()
{
    vec3 uv = vec3(u_ThinFilmThicknessUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_THIN_FILM_THICKNESS_UV_TRANSFORM
    uv *= u_ThinFilmThicknessUVTransform;
    #endif

    return uv.xy;
}

vec2 getThicknessUV()
{
    vec3 uv = vec3(u_ThicknessUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_THICKNESS_UV_TRANSFORM
    uv *= u_ThicknessUVTransform;
    #endif

    return uv.xy;
}
#endif

#ifdef HAS_ANISOTROPY_MAP

vec2 getAnisotropyUV()
{
    vec3 uv = vec3(u_AnisotropyUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_ANISOTROPY_UV_TRANSFORM
    uv *= u_AnisotropyUVTransform;
    #endif

    return uv.xy;
}



vec2 getAnisotropyDirectionUV()
{
    vec3 uv = vec3(u_AnisotropyDirectionUVSet < 1 ? v_UVCoord1 : v_UVCoord2, 1.0);

    #ifdef HAS_ANISOTROPY_DIRECTION_UV_TRANSFORM
    uv *= u_AnisotropyDirectionUVTransform;
    #endif

    return uv.xy;
}

#endif

//===========================================================







//===========================================================
//#include <functions.glsl>
//===========================================================
// textures.glsl needs to be included

const float M_PI = 3.141592653589793;

#ifdef HAS_NORMALS
  #ifdef HAS_TANGENTS
  //  in mat3 v_TBN;
  #else
  //  in vec3 v_Normal;
  #endif
#endif

#ifdef HAS_VERTEX_COLOR_VEC3
//in vec3 v_Color;
#endif
#ifdef HAS_VERTEX_COLOR_VEC4
//in vec4 v_Color;
#endif

vec4 getVertexColor()
{
   vec4 color = vec4(1.0, 1.0, 1.0, 1.0);

#ifdef HAS_VERTEX_COLOR_VEC3
    color.rgb = v_Color;
#endif
#ifdef HAS_VERTEX_COLOR_VEC4
    color = v_Color;
#endif

   return color;
}

struct NormalInfo {
    vec3 ng;   // Geometric normal
    vec3 n;    // Pertubed normal
    vec3 t;    // Pertubed tangent
    vec3 b;    // Pertubed bitangent
};

float clampedDot(vec3 x, vec3 y)
{
    return clamp(dot(x, y), 0.0, 1.0);
}

float sq(float t)
{
    return t * t;
}

vec2 sq(vec2 t)
{
    return t * t;
}

vec3 sq(vec3 t)
{
    return t * t;
}

vec4 sq(vec4 t)
{
    return t * t;
}

vec3 transmissionAbsorption(vec3 v, vec3 n, float ior, float thickness, vec3 absorptionColor)
{
    vec3 r = refract(-v, n, 1.0 / ior);
    return exp(-absorptionColor * thickness * dot(-n, r));
}

//===========================================================

//===========================================================
//#include <brdf.glsl>
//===========================================================
//
// Fresnel
//
// http://graphicrants.blogspot.com/2013/08/specular-brdf-reference.html
// https://github.com/wdas/brdf/tree/master/src/brdfs
// https://google.github.io/filament/Filament.md.html
//

vec3 F_None(vec3 f0, vec3 f90, float VdotH)
{
    return f0;
}

// The following equation models the Fresnel reflectance term of the spec equation (aka F())
// Implementation of fresnel from [4], Equation 15
vec3 F_Schlick(vec3 f0, vec3 f90, float VdotH)
{
    return f0 + (f90 - f0) * pow(clamp(1.0 - VdotH, 0.0, 1.0), 5.0);
}

vec3 F_CookTorrance(vec3 f0, vec3 f90, float VdotH)
{
    vec3 f0_sqrt = sqrt(f0);
    vec3 ior = (1.0 + f0_sqrt) / (1.0 - f0_sqrt);
    vec3 c = vec3(VdotH);
    vec3 g = sqrt(sq(ior) + c*c - 1.0);
    return 0.5 * pow(g-c, vec3(2.0)) / pow(g+c, vec3(2.0)) * (1.0 + pow(c*(g+c) - 1.0, vec3(2.0)) / pow(c*(g-c) + 1.0, vec3(2.0)));
}

// Smith Joint GGX
// Note: Vis = G / (4 * NdotL * NdotV)
// see Eric Heitz. 2014. Understanding the Masking-Shadowing Function in Microfacet-Based BRDFs. Journal of Computer Graphics Techniques, 3
// see Real-Time Rendering. Page 331 to 336.
// see https://google.github.io/filament/Filament.md.html#materialsystem/specularbrdf/geometricshadowing(specularg)
float V_GGX(float NdotL, float NdotV, float alphaRoughness)
{
    float alphaRoughnessSq = alphaRoughness * alphaRoughness;

    float GGXV = NdotL * sqrt(NdotV * NdotV * (1.0 - alphaRoughnessSq) + alphaRoughnessSq);
    float GGXL = NdotV * sqrt(NdotL * NdotL * (1.0 - alphaRoughnessSq) + alphaRoughnessSq);

    float GGX = GGXV + GGXL;
    if (GGX > 0.0)
    {
        return 0.5 / GGX;
    }
    return 0.0;
}

// Anisotropic GGX visibility function, with height correlation.
// T: Tanget, B: Bi-tanget
float V_GGX_anisotropic(float NdotL, float NdotV, float BdotV, float TdotV, float TdotL, float BdotL, float anisotropy, float at, float ab)
{
    float GGXV = NdotL * length(vec3(at * TdotV, ab * BdotV, NdotV));
    float GGXL = NdotV * length(vec3(at * TdotL, ab * BdotL, NdotL));
    float v = 0.5 / (GGXV + GGXL);
    return clamp(v, 0.0, 1.0);
}

// https://github.com/google/filament/blob/master/shaders/src/brdf.fs#L136
// https://github.com/google/filament/blob/master/libs/ibl/src/CubemapIBL.cpp#L179
// Note: Google call it V_Ashikhmin and V_Neubelt
float V_Ashikhmin(float NdotL, float NdotV)
{
    return clamp(1.0 / (4.0 * (NdotL + NdotV - NdotL * NdotV)),0.0,1.0);
}

// https://github.com/google/filament/blob/master/shaders/src/brdf.fs#L131
float V_Kelemen(float LdotH)
{
    // Kelemen 2001, "A Microfacet Based Coupled Specular-Matte BRDF Model with Importance Sampling"
    return 0.25 / (LdotH * LdotH);
}

// The following equation(s) model the distribution of microfacet normals across the area being drawn (aka D())
// Implementation from "Average Irregularity Representation of a Roughened Surface for Ray Reflection" by T. S. Trowbridge, and K. P. Reitz
// Follows the distribution function recommended in the SIGGRAPH 2013 course notes from EPIC Games [1], Equation 3.
float D_GGX(float NdotH, float alphaRoughness)
{
    float alphaRoughnessSq = alphaRoughness * alphaRoughness;
    float f = (NdotH * NdotH) * (alphaRoughnessSq - 1.0) + 1.0;
    return alphaRoughnessSq / (M_PI * f * f);
}

// Anisotropic GGX NDF with a single anisotropy parameter controlling the normal orientation.
// See https://google.github.io/filament/Filament.html#materialsystem/anisotropicmodel
// T: Tanget, B: Bi-tanget
float D_GGX_anisotropic(float NdotH, float TdotH, float BdotH, float anisotropy, float at, float ab)
{
    float a2 = at * ab;
    vec3 f = vec3(ab * TdotH, at * BdotH, a2 * NdotH);
    float w2 = a2 / dot(f, f);
    return a2 * w2 * w2 / M_PI;
}

float D_Ashikhmin(float NdotH, float alphaRoughness)
{
    // Ashikhmin 2007, "Distribution-based BRDFs"
    float a2 = alphaRoughness * alphaRoughness;
    float cos2h = NdotH * NdotH;
    float sin2h = 1.0 - cos2h;
    float sin4h = sin2h * sin2h;
    float cot2 = -cos2h / (a2 * sin2h);
    return 1.0 / (M_PI * (4.0 * a2 + 1.0) * sin4h) * (4.0 * exp(cot2) + sin4h);
}

//Sheen implementation-------------------------------------------------------------------------------------
// See  https://github.com/sebavan/glTF/tree/KHR_materials_sheen/extensions/2.0/Khronos/KHR_materials_sheen

// Estevez and Kulla http://www.aconty.com/pdf/s2017_pbs_imageworks_sheen.pdf
float D_Charlie(float sheenRoughness, float NdotH)
{
    sheenRoughness = max(sheenRoughness, 0.000001); //clamp (0,1]
    float alphaG = sheenRoughness * sheenRoughness;
    float invR = 1.0 / alphaG;
    float cos2h = NdotH * NdotH;
    float sin2h = 1.0 - cos2h;
    return (2.0 + invR) * pow(sin2h, invR * 0.5) / (2.0 * M_PI);
}

//https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#acknowledgments AppendixB
vec3 BRDF_lambertian(vec3 f0, vec3 f90, vec3 diffuseColor, float VdotH)
{
    // see https://seblagarde.wordpress.com/2012/01/08/pi-or-not-to-pi-in-game-lighting-equation/
    return (1.0 - F_Schlick(f0, f90, VdotH)) * (diffuseColor / M_PI);
}

//  https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#acknowledgments AppendixB
vec3 BRDF_specularGGX(vec3 f0, vec3 f90, float alphaRoughness, float VdotH, float NdotL, float NdotV, float NdotH)
{
    vec3 F = F_Schlick(f0, f90, VdotH);
    float Vis = V_GGX(NdotL, NdotV, alphaRoughness);
    float D = D_GGX(NdotH, alphaRoughness);

    return F * Vis * D;
}

vec3 BRDF_specularAnisotropicGGX(vec3 f0, vec3 f90, float alphaRoughness, float VdotH, float NdotL, float NdotV, float NdotH,
    float BdotV, float TdotV, float TdotL, float BdotL, float TdotH, float BdotH, float anisotropy)
{
    // Roughness along tangent and bitangent.
    // Christopher Kulla and Alejandro Conty. 2017. Revisiting Physically Based Shading at Imageworks
    float at = max(alphaRoughness * (1.0 + anisotropy), 0.00001);
    float ab = max(alphaRoughness * (1.0 - anisotropy), 0.00001);

    vec3 F = F_Schlick(f0, f90, VdotH);
    float V = V_GGX_anisotropic(NdotL, NdotV, BdotV, TdotV, TdotL, BdotL, anisotropy, at, ab);
    float D = D_GGX_anisotropic(NdotH, TdotH, BdotH, anisotropy, at, ab);

    return F * V * D;
}

// f_sheen
#ifdef MATERIAL_SHEEN
vec3 BRDF_specularSheen(vec3 sheenColor, float sheenIntensity, float sheenRoughness, float NdotL, float NdotV, float NdotH)
{
    float sheenDistribution = D_Charlie(sheenRoughness, NdotH);
    float sheenVisibility = V_Ashikhmin(NdotL, NdotV);
    return sheenColor * sheenIntensity * sheenDistribution * sheenVisibility;
}
#endif

//===========================================================

//===========================================================
//#include <punctual.glsl>
//===========================================================
// KHR_lights_punctual extension.
// see https://github.com/KhronosGroup/glTF/tree/master/extensions/2.0/Khronos/KHR_lights_punctual


const int LightType_Directional = 0;
const int LightType_Point = 1;
const int LightType_Spot = 2;

// https://github.com/KhronosGroup/glTF/blob/master/extensions/2.0/Khronos/KHR_lights_punctual/README.md#range-property
float getRangeAttenuation(float range, float distance)
{
    if (range <= 0.0)
    {
        // negative range means unlimited
        return 1.0;
    }
    return max(min(1.0 - pow(distance / range, 4.0), 1.0), 0.0) / pow(distance, 2.0);
}

// https://github.com/KhronosGroup/glTF/blob/master/extensions/2.0/Khronos/KHR_lights_punctual/README.md#inner-and-outer-cone-angles
float getSpotAttenuation(vec3 pointToLight, vec3 spotDirection, float outerConeCos, float innerConeCos)
{
    float actualCos = dot(normalize(spotDirection), normalize(-pointToLight));
    if (actualCos > outerConeCos)
    {
        if (actualCos < innerConeCos)
        {
            return smoothstep(outerConeCos, innerConeCos, actualCos);
        }
        return 1.0;
    }
    return 0.0;
}

vec3 getPunctualRadianceSubsurface(vec3 n, vec3 v, vec3 l, float scale, float distortion, float power, vec3 color, float thickness)
{
    vec3 distortedHalfway = l + n * distortion;
    float backIntensity = max(0.0, dot(v, -distortedHalfway));
    float reverseDiffuse = pow(clamp(0.0, 1.0, backIntensity), power) * scale;
    return (reverseDiffuse + color) * (1.0 - thickness);
}

vec3 getPunctualRadianceTransmission(vec3 n, vec3 v, vec3 l, float alphaRoughness, float ior, vec3 f0)
{
    vec3 r = refract(-v, n, 1.0 / ior);
    vec3 h = normalize(l - r);
    float NdotL = clampedDot(-n, l);
    float NdotV = clampedDot(n, -r);

    float Vis = V_GGX(clampedDot(-n, l), NdotV, alphaRoughness);
    float D = D_GGX(clampedDot(r, l), alphaRoughness);

    return NdotL * f0 * Vis * D;
}

vec3 getPunctualRadianceClearCoat(vec3 clearcoatNormal, vec3 v, vec3 l, vec3 h, float VdotH, vec3 f0, vec3 f90, float clearcoatRoughness)
{
    float NdotL = clampedDot(clearcoatNormal, l);
    float NdotV = clampedDot(clearcoatNormal, v);
    float NdotH = clampedDot(clearcoatNormal, h);
    return NdotL * BRDF_specularGGX(f0, f90, clearcoatRoughness * clearcoatRoughness, VdotH, NdotL, NdotV, NdotH);
}

#ifdef MATERIAL_SHEEN

vec3 getPunctualRadianceSheen(vec3 sheenColor, float sheenIntensity, float sheenRoughness, float NdotL, float NdotV, float NdotH)
{
    return NdotL * BRDF_specularSheen(sheenColor, sheenIntensity, sheenRoughness, NdotL, NdotV, NdotH);
}

#endif

//===========================================================

//===========================================================
//#include <ibl.glsl>
//===========================================================
vec2 getBRDF(vec2 samplePoint)
{
    #if 0
    vec4 v255 = texture(u_GGXLUT, samplePoint);
    v255 *= 255.f;
    return vec2( 255.0*v255.g + v255.r , 255.0*v255.a + v255.b ) / 65535.0f;
    #else
    return textureLod(u_GGXLUT, vec2(samplePoint.x, 1.f-samplePoint.y), 0.f).rg;
    #endif
}

vec2 getBRDF(vec3 n, vec3 v, float perceptualRoughness, vec3 specularColor)
{
    float NdotV = clampedDot(n, v);

    float mipCount = float( textureQueryLevels(u_GGXEnvSampler) );
    //float mipCount = u_MipCount;

    float lod = clamp(perceptualRoughness * float(mipCount), 0.0, float(mipCount));
    vec3 reflection = normalize(reflect(-v, n));

    vec2 brdfSamplePoint = clamp(vec2(NdotV, perceptualRoughness), vec2(0.0, 0.0), vec2(1.0, 1.0));
    vec2 brdf = getBRDF(brdfSamplePoint);//texture(u_GGXLUT, brdfSamplePoint).rg;
    return brdf;
}

vec3 getIBLRadianceGGX(vec3 n, vec3 v, float perceptualRoughness, vec3 specularColor)
{
    float NdotV = clampedDot(n, v);


    float mipCount = float( textureQueryLevels(u_GGXEnvSampler) );
    //float mipCount = u_MipCount;

    float lod = clamp(perceptualRoughness * float(mipCount), 0.0, float(mipCount));
    vec3 reflection = normalize(reflect(-v, n));

    vec2 brdfSamplePoint = clamp(vec2(NdotV, perceptualRoughness), vec2(0.0, 0.0), vec2(1.0, 1.0));
    vec2 brdf = getBRDF(brdfSamplePoint);//texture(u_GGXLUT, brdfSamplePoint).rg;
    vec4 specularSample = textureLod(u_GGXEnvSampler, reflection, lod);

    vec3 specularLight = specularSample.rgb;

#ifndef USE_HDR
    specularLight = sRGBToLinear(specularLight);
#endif

   return specularLight * (specularColor * brdf.x + brdf.y);
}

vec3 getIBLRadianceTransmission(vec3 n, vec3 v, float perceptualRoughness, float ior, vec3 baseColor)
{
    // Sample GGX LUT.
    float NdotV = clampedDot(n, v);
    vec2 brdfSamplePoint = clamp(vec2(NdotV, perceptualRoughness), vec2(0.0, 0.0), vec2(1.0, 1.0));
    vec2 brdf = getBRDF(brdfSamplePoint);//texture(u_GGXLUT, brdfSamplePoint).rg;

    // Sample GGX environment map.
    float mipCount = float( textureQueryLevels(u_GGXEnvSampler) );
    //float mipCount = u_MipCount;
    float lod = clamp(perceptualRoughness * float(mipCount), 0.0, float(mipCount));

    // Approximate double refraction by assuming a solid sphere beneath the point.
    vec3 r = refract(-v, n, 1.0 / ior);
    vec3 m = 2.0 * dot(-n, r) * r + n;
    vec3 rr = -refract(-r, m, ior);

    vec4 specularSample = textureLod(u_GGXEnvSampler, rr, lod);
    vec3 specularLight = specularSample.rgb;

#ifndef USE_HDR
    specularLight = sRGBToLinear(specularLight);
#endif

   return specularLight * (brdf.x + brdf.y);
}

vec3 getIBLRadianceLambertian(vec3 n, vec3 diffuseColor)
{
    vec3 diffuseLight = vec3(1,1,1);

    if( valid(u_LambertianEnvSampler))
      diffuseLight = texture(u_LambertianEnvSampler, n).rgb;

    #ifndef USE_HDR
        diffuseLight = sRGBToLinear(diffuseLight);
    #endif

    return diffuseLight * diffuseColor;
}

#ifdef MATERIAL_SHEEN
vec3 getIBLRadianceCharlie(vec3 n, vec3 v, float sheenRoughness, vec3 sheenColor, float sheenIntensity)
{
    float NdotV = clampedDot(n, v);
    float mipCount = float( textureQueryLevels(u_CharlieEnvSampler) );
    //float mipCount = u_MipCount;
    float lod = clamp(sheenRoughness * float(mipCount), 0.0, float(mipCount));
    vec3 reflection = normalize(reflect(-v, n));

    vec2 brdfSamplePoint = clamp(vec2(NdotV, sheenRoughness), vec2(0.0, 0.0), vec2(1.0, 1.0));
    float brdf = texture(u_CharlieLUT, brdfSamplePoint).b;
    vec4 sheenSample = textureLod(u_CharlieEnvSampler, reflection, lod);

    vec3 sheenLight = sheenSample.rgb;

    #ifndef USE_HDR
    sheenLight = sRGBToLinear(sheenLight);
    #endif

    return sheenIntensity * sheenLight * sheenColor * brdf;
}
#endif

vec3 getIBLRadianceSubsurface(vec3 n, vec3 v, float scale, float distortion, float power, vec3 color, float thickness)
{
    vec3 diffuseLight = vec3(1,1,1);//texture(u_LambertianEnvSampler, n).rgb;
    if( valid( u_LambertianEnvSampler ) )
       diffuseLight = texture(u_LambertianEnvSampler, n).rgb;

    #ifndef USE_HDR
        diffuseLight = sRGBToLinear(diffuseLight);
    #endif

    return diffuseLight * getPunctualRadianceSubsurface(n, v, -v, scale, distortion, power, color, thickness);
}
//===========================================================


struct MaterialInfo
{
    float perceptualRoughness;      // roughness value, as authored by the model creator (input to shader)
    vec3 f0;                        // full reflectance color (n incidence angle)

    float alphaRoughness;           // roughness mapped to a more linear change in the roughness (proposed by [2])
    vec3 albedoColor;

    vec3 f90;                       // reflectance color at grazing angle
    float metallic;

    vec3 n;
    vec3 baseColor; // getBaseColor()

    float sheenIntensity;
    vec3 sheenColor;
    float sheenRoughness;

    float anisotropy;

    vec3 clearcoatF0;
    vec3 clearcoatF90;
    float clearcoatFactor;
    vec3 clearcoatNormal;
    float clearcoatRoughness;

    float subsurfaceScale;
    float subsurfaceDistortion;
    float subsurfacePower;
    vec3 subsurfaceColor;
    float subsurfaceThickness;

    float thinFilmFactor;
    float thinFilmThickness;

    float thickness;

    vec3 absorption;

    float transmission;
};

// Get normal, tangent and bitangent vectors.
NormalInfo getNormalInfo(vec3 v)
{
    vec2 UV = getNormalUV();
    vec3 uv_dx = dFdx(vec3(UV, 0.0));
    vec3 uv_dy = dFdy(vec3(UV, 0.0));

    vec3 t_ = (uv_dy.t * dFdx(v_Position) - uv_dx.t * dFdy(v_Position)) /
        (uv_dx.s * uv_dy.t - uv_dy.s * uv_dx.t);

    vec3 n, t, b, ng;

    // Compute geometrical TBN:
    #ifdef HAS_TANGENTS
        // Trivial TBN computation, present as vertex attribute.
        // Normalize eigenvectors as matrix is linearly interpolated.
        t = normalize(v_TBN[0]);
        b = normalize(v_TBN[1]);
        ng = normalize(v_TBN[2]);
    #else
        // Normals are either present as vertex attributes or approximated.
        #ifdef HAS_NORMALS
            ng = normalize(v_Normal);
        #else
            ng = normalize(cross(dFdx(v_Position), dFdy(v_Position)));
        #endif

        t = normalize(t_ - ng * dot(ng, t_));
        b = cross(ng, t);
    #endif

    // For a back-facing surface, the tangential basis vectors are negated.
    float facing = step(0.0, dot(v, ng)) * 2.0 - 1.0;
    t *= facing;
    b *= facing;
    ng *= facing;

    // Due to anisoptry, the tangent can be further rotated around the geometric normal.
    vec3 direction;
    #ifdef MATERIAL_ANISOTROPY
        #ifdef HAS_ANISOTROPY_DIRECTION_MAP
            direction = texture(u_AnisotropyDirectionSampler, getAnisotropyDirectionUV()).xyz * 2.0 - vec3(1.0);
        #else
            direction = u_AnisotropyDirection;
        #endif
    #else
        direction = vec3(1.0, 0.0, 0.0);
    #endif
    t = mat3(t, b, ng) * normalize(direction);
    b = normalize(cross(ng, t));

    // Compute pertubed normals:
    //#ifdef HAS_NORMAL_MAP
    if( valid(u_NormalSampler) )
    {
        n = texture(u_NormalSampler, UV).rgb * 2.0 - vec3(1.0);
        n *= vec3(u_NormalScale, u_NormalScale, 1.0);
        n = mat3(t, b, ng) * normalize(n);
    }
    else
    {
        n = ng;
    }

    NormalInfo info;
    info.ng = ng;
    info.t = t;
    info.b = b;
    info.n = n;
    return info;
}

vec4 getBaseColor()
{
    vec4 baseColor = vec4(1, 1, 1, 1);

    #if defined(MATERIAL_SPECULARGLOSSINESS)
        baseColor = u_DiffuseFactor;
    #elif defined(MATERIAL_METALLICROUGHNESS)
        baseColor = u_BaseColorFactor;
    #endif

    #if defined(MATERIAL_SPECULARGLOSSINESS) && defined(HAS_DIFFUSE_MAP)
        baseColor *= sRGBToLinear(texture(u_DiffuseSampler, getDiffuseUV()));
    #elif defined(MATERIAL_METALLICROUGHNESS)
        if( valid(u_BaseColorSampler) )
        {
          baseColor *= sRGBToLinear(texture(u_BaseColorSampler, getBaseColorUV()));
        }
    #endif

    return baseColor * getVertexColor();
}

#ifdef MATERIAL_SPECULARGLOSSINESS

MaterialInfo getSpecularGlossinessInfo(MaterialInfo info)
{
    info.f0 = u_SpecularFactor;
    info.perceptualRoughness = u_GlossinessFactor;

#ifdef HAS_SPECULAR_GLOSSINESS_MAP
    vec4 sgSample = sRGBToLinear(texture(u_SpecularGlossinessSampler, getSpecularGlossinessUV()));
    info.perceptualRoughness *= sgSample.a ;
    info.f0 *= sgSample.rgb;
#endif

  // 1 - glossiness
    info.perceptualRoughness = 1.0 - info.perceptualRoughness;
    info.albedoColor = info.baseColor.rgb * (1.0 - max(max(info.f0.r, info.f0.g), info.f0.b));

    return info;
}
#endif

// KHR_extension_specular alters f0 on metallic materials based on the specular factor specified in the extention
#ifdef MATERIAL_METALLICROUGHNESS_SPECULAROVERRIDE
float getMetallicRoughnessSpecularFactor()
{
    //F0 = 0.08 * specularFactor * specularTexture
#ifdef HAS_METALLICROUGHNESS_SPECULAROVERRIDE_MAP
    vec4 specSampler =  texture(u_MetallicRoughnessSpecularSampler, getMetallicRoughnessSpecularUV());
    return 0.08 * u_MetallicRoughnessSpecularFactor * specSampler.a;
#endif
    return  0.08 * u_MetallicRoughnessSpecularFactor;
}
#endif

MaterialInfo getMetallicRoughnessInfo(MaterialInfo info, float f0_ior)
{
    info.metallic = u_MetallicFactor;
    info.perceptualRoughness = u_RoughnessFactor;


    if( valid(u_MetallicRoughnessSampler) )
    {
        // Roughness is stored in the 'g' channel, metallic is stored in the 'b' channel.
        // This layout intentionally reserves the 'r' channel for (optional) occlusion map data
        vec4 mrSample = texture(u_MetallicRoughnessSampler, getMetallicRoughnessUV());

        // The roughness is sampled from teh green channel and
        // the metallic is sampled from the blue channel.
        // this is defined in the GLTF spec
        info.perceptualRoughness *= mrSample.g;
        info.metallic            *= mrSample.b;
    }

#ifdef MATERIAL_METALLICROUGHNESS_SPECULAROVERRIDE
    // Overriding the f0 creates unrealistic materials if the IOR does not match up.
    vec3 f0 = vec3(getMetallicRoughnessSpecularFactor());
#else
    // Achromatic f0 based on IOR.
    vec3 f0 = vec3(f0_ior);
#endif

    info.albedoColor = mix(info.baseColor.rgb * (vec3(1.0) - f0),  vec3(0), info.metallic);
    info.f0 = mix(f0, info.baseColor.rgb, info.metallic);

    return info;
}

MaterialInfo getSheenInfo(MaterialInfo info)
{
    info.sheenColor     = u_SheenColorFactor;
    info.sheenIntensity = u_SheenIntensityFactor;
    info.sheenRoughness = u_SheenRoughness;

    #ifdef HAS_SHEEN_COLOR_INTENSITY_MAP
        vec4 sheenSample = texture(u_SheenColorIntensitySampler, getSheenUV());
        info.sheenColor *= sheenSample.xyz;
        info.sheenIntensity *= sheenSample.w;
    #endif

    return info;
}

#ifdef MATERIAL_SUBSURFACE
MaterialInfo getSubsurfaceInfo(MaterialInfo info)
{
    info.subsurfaceScale = u_SubsurfaceScale;
    info.subsurfaceDistortion = u_SubsurfaceDistortion;
    info.subsurfacePower = u_SubsurfacePower;
    info.subsurfaceColor = u_SubsurfaceColorFactor;
    info.subsurfaceThickness = u_SubsurfaceThicknessFactor;

    #ifdef HAS_SUBSURFACE_COLOR_MAP
        info.subsurfaceColor *= texture(u_SubsurfaceColorSampler, getSubsurfaceColorUV()).rgb;
    #endif

    #ifdef HAS_SUBSURFACE_THICKNESS_MAP
        info.subsurfaceThickness *= texture(u_SubsurfaceThicknessSampler, getSubsurfaceThicknessUV()).r;
    #endif

    return info;
}
#endif

#ifdef MATERIAL_THIN_FILM

vec3 getThinFilmF0(vec3 f0, vec3 f90, float NdotV, float thinFilmFactor, float thinFilmThickness)
{
    if (thinFilmFactor == 0.0)
    {
        // No thin film applied.
        return f0;
    }

    vec3 lutSample = texture(u_ThinFilmLUT, vec2(thinFilmThickness, NdotV)).rgb - 0.5;
    vec3 intensity = thinFilmFactor * 4.0 * f0 * (1.0 - f0);
    return clamp(intensity * lutSample, 0.0, 1.0);
}


MaterialInfo getThinFilmInfo(MaterialInfo info)
{
    info.thinFilmFactor = u_ThinFilmFactor;
    info.thinFilmThickness = u_ThinFilmThicknessMaximum / 1200.0;

    #ifdef HAS_THIN_FILM_MAP
        info.thinFilmFactor *= texture(u_ThinFilmSampler, getThinFilmUV()).r;
    #endif

    #ifdef HAS_THIN_FILM_THICKNESS_MAP
        float thicknessSampled = texture(u_ThinFilmThicknessSampler, getThinFilmThicknessUV()).g;
        float thickness = mix(u_ThinFilmThicknessMinimum / 1200.0, u_ThinFilmThicknessMaximum / 1200.0, thicknessSampled);
        info.thinFilmThickness = thickness;
    #endif

    return info;
}
#endif

MaterialInfo getTransmissionInfo(MaterialInfo info)
{
    info.transmission = u_Transmission;
    return info;
}

MaterialInfo getThicknessInfo(MaterialInfo info)
{
    info.thickness = 1.0;

    #ifdef MATERIAL_THICKNESS
    info.thickness = u_Thickness;

    #ifdef HAS_THICKNESS_MAP
    info.thickness *= texture(u_ThicknessSampler, getThicknessUV()).r;
    #endif

    #endif

    return info;
}

MaterialInfo getAbsorptionInfo(MaterialInfo info)
{
    info.absorption = vec3(0.0);

    #ifdef MATERIAL_ABSORPTION
    info.absorption = u_AbsorptionColor;
    #endif

    return info;
}

MaterialInfo getAnisotropyInfo(MaterialInfo info)
{
    info.anisotropy = u_Anisotropy;

#ifdef HAS_ANISOTROPY_MAP
    info.anisotropy *= texture(u_AnisotropySampler, getAnisotropyUV()).r * 2.0 - 1.0;
#endif

    return info;
}

MaterialInfo getClearCoatInfo(MaterialInfo info, NormalInfo normalInfo)
{
    info.clearcoatFactor    = u_ClearcoatFactor;
    info.clearcoatRoughness = u_ClearcoatRoughnessFactor;
    info.clearcoatF0 = vec3(0.04);
    info.clearcoatF90 = vec3(clamp(info.clearcoatF0 * 50.0, 0.0, 1.0));

    #ifdef HAS_CLEARCOAT_TEXTURE_MAP
        vec4 ccSample = texture(u_ClearcoatSampler, getClearcoatUV());
        info.clearcoatFactor *= ccSample.r;
    #endif

    #ifdef HAS_CLEARCOAT_ROUGHNESS_MAP
        vec4 ccSampleRough = texture(u_ClearcoatRoughnessSampler, getClearcoatRoughnessUV());
        info.clearcoatRoughness *= ccSampleRough.g;
    #endif

    #ifdef HAS_CLEARCOAT_NORMAL_MAP
        vec4 ccSampleNor = texture(u_ClearcoatNormalSampler, getClearcoatNormalUV());
        info.clearcoatNormal = normalize(ccSampleNor.xyz);
    #else
        info.clearcoatNormal = normalInfo.ng;
    #endif

    info.clearcoatRoughness = clamp(info.clearcoatRoughness, 0.0, 1.0);

    return info;
}


FSOut MAIN()
{
    FSOut OUT;

    OUT.normal   = f_NORMAL.rgb;
    OUT.position = f_POSITION.rgb;

    vec4 g_finalColor;
    vec4 baseColor = getBaseColor();

#ifdef ALPHAMODE_OPAQUE
    baseColor.a = 1.0;
#endif


    if(u_unlit == 1)
    {
        g_finalColor = (vec4(linearTosRGB(baseColor.rgb), baseColor.a));
        OUT.albedo = g_finalColor.rgb;
        return OUT;
    }

    vec3 v = normalize(u_Camera - v_Position);
    NormalInfo normalInfo = getNormalInfo(v);
    vec3 n = normalInfo.n;
    vec3 t = normalInfo.t;
    vec3 b = normalInfo.b;

    float NdotV = clampedDot(n, v);
    float TdotV = clampedDot(t, v);
    float BdotV = clampedDot(b, v);

    MaterialInfo materialInfo;
    materialInfo.baseColor = baseColor.rgb;

#ifdef MATERIAL_IOR
    float ior = u_IOR_and_f0.x;
    float f0_ior = u_IOR_and_f0.y;
#else
    // The default index of refraction of 1.5 yields a dielectric normal incidence reflectance of 0.04.
    float ior = 1.5;
    float f0_ior = 0.04;
#endif

#ifdef MATERIAL_SPECULARGLOSSINESS
    materialInfo = getSpecularGlossinessInfo(materialInfo);
#endif

#ifdef MATERIAL_METALLICROUGHNESS
    materialInfo = getMetallicRoughnessInfo(materialInfo, f0_ior);
#endif

#ifdef MATERIAL_SHEEN
    materialInfo = getSheenInfo(materialInfo);
#endif

#ifdef MATERIAL_SUBSURFACE
    materialInfo = getSubsurfaceInfo(materialInfo);
#endif

#ifdef MATERIAL_THIN_FILM
    materialInfo = getThinFilmInfo(materialInfo);
#endif

#ifdef MATERIAL_CLEARCOAT
    materialInfo = getClearCoatInfo(materialInfo, normalInfo);
#endif

#ifdef MATERIAL_TRANSMISSION
    materialInfo = getTransmissionInfo(materialInfo);
#endif

#ifdef MATERIAL_ANISOTROPY
    materialInfo = getAnisotropyInfo(materialInfo);
#endif

    materialInfo = getThicknessInfo(materialInfo);
    materialInfo = getAbsorptionInfo(materialInfo);

    materialInfo.perceptualRoughness = clamp(materialInfo.perceptualRoughness, 0.0, 1.0);
    materialInfo.metallic = clamp(materialInfo.metallic, 0.0, 1.0);

    // Roughness is authored as perceptual roughness; as is convention,
    // convert to material roughness by squaring the perceptual roughness.
    materialInfo.alphaRoughness = materialInfo.perceptualRoughness * materialInfo.perceptualRoughness;

    // Compute reflectance.
    float reflectance = max(max(materialInfo.f0.r, materialInfo.f0.g), materialInfo.f0.b);

    // Anything less than 2% is physically impossible and is instead considered to be shadowing. Compare to "Real-Time-Rendering" 4th editon on page 325.
    materialInfo.f90 = vec3(clamp(reflectance * 50.0, 0.0, 1.0));

    materialInfo.n = n;

#ifdef MATERIAL_THIN_FILM
    materialInfo.f0 = getThinFilmF0(materialInfo.f0, materialInfo.f90, clampedDot(n, v),
        materialInfo.thinFilmFactor, materialInfo.thinFilmThickness);
#endif

    // LIGHTING
    vec3 f_specular = vec3(0.0);
    vec3 f_diffuse = vec3(0.0);
    vec3 f_emissive = vec3(0.0);
    vec3 f_clearcoat = vec3(0.0);
    vec3 f_sheen = vec3(0.0);
    vec3 f_subsurface = vec3(0.0);
    vec3 f_transmission = vec3(0.0);



    // Calculate lighting contribution from image based lighting source (IBL)
#ifdef USE_IBL
    f_specular += getIBLRadianceGGX(n, v, materialInfo.perceptualRoughness, materialInfo.f0);
    f_diffuse += getIBLRadianceLambertian(n, materialInfo.albedoColor);

    #ifdef MATERIAL_CLEARCOAT
        f_clearcoat += getIBLRadianceGGX(materialInfo.clearcoatNormal, v, materialInfo.clearcoatRoughness, materialInfo.clearcoatF0);
    #endif

    #ifdef MATERIAL_SHEEN
        f_sheen += getIBLRadianceCharlie(n, v, materialInfo.sheenRoughness, materialInfo.sheenColor, materialInfo.sheenIntensity);
    #endif

    #ifdef MATERIAL_SUBSURFACE
        f_subsurface += getIBLRadianceSubsurface(n, v, materialInfo.subsurfaceScale, materialInfo.subsurfaceDistortion, materialInfo.subsurfacePower, materialInfo.subsurfaceColor, materialInfo.subsurfaceThickness);
    #endif

    #ifdef MATERIAL_TRANSMISSION
        f_transmission += getIBLRadianceTransmission(n, v, materialInfo.perceptualRoughness, ior, materialInfo.baseColor);
    #endif
#endif


#ifdef USE_PUNCTUAL

    for (int i = 0; i < u_LightCount; ++i)
    {
        Light light = s_Lights[i];

        vec3 pointToLight = -light.direction;
        float rangeAttenuation = 1.0;
        float spotAttenuation = 1.0;

        if(light.type != LightType_Directional)
        {
            pointToLight = light.position - v_Position;
        }

        // Compute range and spot light attenuation.
        if (light.type != LightType_Directional)
        {
            rangeAttenuation = getRangeAttenuation(light.range, length(pointToLight));
        }
        if (light.type == LightType_Spot)
        {
            spotAttenuation = getSpotAttenuation(pointToLight, light.direction, light.outerConeCos, light.innerConeCos);
        }

        vec3 intensity = rangeAttenuation * spotAttenuation * light.intensity * light.color;

        vec3 l = normalize(pointToLight);   // Direction from surface point to light
        vec3 h = normalize(l + v);          // Direction of the vector between l and v, called halfway vector
        float NdotL = clampedDot(n, l);
        float NdotV = clampedDot(n, v);
        float NdotH = clampedDot(n, h);
        float LdotH = clampedDot(l, h);
        float VdotH = clampedDot(v, h);


        if (NdotL > 0.0 || NdotV > 0.0)
        {
            // Calculation of analytical light
            //https://github.com/KhronosGroup/glTF/tree/master/specification/2.0#acknowledgments AppendixB
            f_diffuse += intensity * NdotL *  BRDF_lambertian(materialInfo.f0, materialInfo.f90, materialInfo.albedoColor, VdotH);

            #ifdef MATERIAL_ANISOTROPY
              vec3 h = normalize(l + v);
              float TdotL = dot(t, l);
              float BdotL = dot(b, l);
              float TdotH = dot(t, h);
              float BdotH = dot(b, h);
              f_specular += intensity * NdotL * BRDF_specularAnisotropicGGX(materialInfo.f0, materialInfo.f90, materialInfo.alphaRoughness,
                  VdotH, NdotL, NdotV, NdotH,
                  BdotV, TdotV, TdotL, BdotL, TdotH, BdotH, materialInfo.anisotropy);
            #else
              f_specular += intensity * NdotL * BRDF_specularGGX(materialInfo.f0, materialInfo.f90, materialInfo.alphaRoughness, VdotH, NdotL, NdotV, NdotH);
            #endif

            #ifdef MATERIAL_SHEEN
                f_sheen += intensity * getPunctualRadianceSheen(materialInfo.sheenColor, materialInfo.sheenIntensity, materialInfo.sheenRoughness,
                    NdotL, NdotV, NdotH);
            #endif

            #ifdef MATERIAL_CLEARCOAT
                f_clearcoat += intensity * getPunctualRadianceClearCoat(materialInfo.clearcoatNormal, v, l,
                    h, VdotH,
                    materialInfo.clearcoatF0, materialInfo.clearcoatF90, materialInfo.clearcoatRoughness);
            #endif
        }

        #ifdef MATERIAL_SUBSURFACE
            f_subsurface += intensity * getPunctualRadianceSubsurface(n, v, l,
                materialInfo.subsurfaceScale, materialInfo.subsurfaceDistortion, materialInfo.subsurfacePower,
                materialInfo.subsurfaceColor, materialInfo.subsurfaceThickness);
        #endif

        #ifdef MATERIAL_TRANSMISSION
            f_transmission += intensity * getPunctualRadianceTransmission(n, v, l, materialInfo.alphaRoughness, ior, materialInfo.f0);
        #endif
    }
#endif // !USE_PUNCTUAL

    f_emissive = u_EmissiveFactor;

    if( valid(u_EmissiveSampler))
    {
      f_emissive *= sRGBToLinear(texture(u_EmissiveSampler, getEmissiveUV())).rgb;
    }

    vec3 color = vec3(0);

///
/// Layer blending
///

    float clearcoatFactor = 0.0;
    vec3 clearcoatFresnel = vec3(0.0);

    #ifdef MATERIAL_CLEARCOAT
        clearcoatFactor = materialInfo.clearcoatFactor;
        clearcoatFresnel = F_Schlick(materialInfo.clearcoatF0, materialInfo.clearcoatF90, clampedDot(materialInfo.clearcoatNormal, v));
    #endif

    #ifdef MATERIAL_ABSORPTION
        f_transmission *= transmissionAbsorption(v, n, ior, materialInfo.thickness, materialInfo.absorption);
    #endif

    #ifdef MATERIAL_TRANSMISSION
    vec3 diffuse = mix(f_diffuse, f_transmission, materialInfo.transmission);
    #else
    vec3 diffuse = f_diffuse;
    #endif

    color = (f_emissive + diffuse + f_specular + f_subsurface + (1.0 - reflectance) * f_sheen) * (1.0 - clearcoatFactor * clearcoatFresnel) + f_clearcoat * clearcoatFactor;

    float ao = 1.0;
    // Apply optional PBR terms for additional (optional) shading
    #ifdef HAS_OCCLUSION_MAP
        ao = texture(u_OcclusionSampler,  getOcclusionUV()).r;
        color = mix(color, color * ao, u_OcclusionStrength);
    #endif

#ifndef DEBUG_OUTPUT // no debug

  #ifdef ALPHAMODE_MASK
      // Late discard to avaoid samplig artifacts. See https://github.com/KhronosGroup/glTF-Sample-Viewer/issues/267
      if(baseColor.a < u_AlphaCutoff)
      {
          discard;
      }
      baseColor.a = 1.0;
  #endif

    // regular shading
    g_finalColor = vec4(toneMap(color), baseColor.a);

    if(u_DebugOutput==0)
    {
        g_finalColor.a = 1.0;
        OUT.albedo = g_finalColor.rgb;
        return OUT;
    }
#endif

    switch( u_DebugOutput)
    {
        case 1:
          g_finalColor.rgb = linearTosRGB(materialInfo.baseColor);
          break;
        case 2:
          g_finalColor.rgb = vec3(materialInfo.metallic);
          break;
        case 3:
          g_finalColor.rgb = vec3(materialInfo.perceptualRoughness);
          break;
        case 4:
          g_finalColor.rgb = texture(u_NormalSampler, getNormalUV()).rgb;
          break;
        case 5:
          g_finalColor.rgb = normalize(v_Normal)*0.5f + vec3(0.5);//vec3(0.5, 0.5, 1.0);
          break;
        case 6:
          g_finalColor.rgb = t * 0.5 + vec3(0.5); // tangent?
          break;
        case 7:
          g_finalColor.rgb = b * 0.5 + vec3(0.5); // bitangent?
          break;
        case 8:
          g_finalColor.rgb = vec3(ao); // occlusion
          break;
        case 9:
          g_finalColor.rgb = materialInfo.f0;
          break;
        case 10:
          g_finalColor.rgb = f_emissive;
          break;
        case 11:
          g_finalColor.rgb = f_specular;
          break;
        case 12:
          g_finalColor.rgb = f_diffuse;
          break;
        case 13:
          g_finalColor.rgb = vec3(materialInfo.thickness);
          break;
        case 14:
          g_finalColor.rgb = f_clearcoat;
          break;
        case 15:
          g_finalColor.rgb = f_sheen;
          break;
        case 16:
          g_finalColor.rgb = vec3(baseColor.a);
          break;
        case 17:
          g_finalColor.rgb = f_subsurface;
          break;
        case 18:
          g_finalColor.rgb = linearTosRGB(f_transmission);
          break;
        case 19:
          g_finalColor.rgb = texture(u_LambertianEnvSampler, f_NORMAL).rgb;
          break;
        case 20:
          g_finalColor.rgb = texture(u_GGXEnvSampler, f_NORMAL).rgb;
          break;
        case 21:
          g_finalColor.rg = getBRDF(n, v, materialInfo.perceptualRoughness, materialInfo.f0);
          g_finalColor.b=0.;
          break;


    }
    g_finalColor.a = 1.0;

    OUT.albedo   = g_finalColor.rgb;

    return OUT;
}

