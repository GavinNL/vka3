//=========================================================================
// These share the same set/binding because it is backed by a single
// storage buffer. This essentially creates 2 different views into
// the buffer..
//
// Uniforms should obey the proper alignment, that is they must align
// to vec2 or to vec4
//
// All uniforms must start with u_
//=========================================================================
uniform mat4 u_modelMatrix;
uniform int  u_useSkin;
uniform int  u_unused1;
uniform int  u_unused2;
uniform int  u_unused3;

//=========================================================================
// Storage arrays are used to store multiple objects of the same type.
// There is a maximum of 4 storageArrays per shader stage.
// To access the storage data, you must access it as follows:
//=========================================================================
storageArray mat4 s_boneMatrices;
//=========================================================================

mat4 getBoneTransformMatrix(vec4 inWeight0, uvec4 inJoint0)
{
	// Mesh is skinned
	mat4 skinMat =
		inWeight0.x * s_boneMatrices[ int(inJoint0.x) ] +
		inWeight0.y * s_boneMatrices[ int(inJoint0.y) ] +
		inWeight0.z * s_boneMatrices[ int(inJoint0.z) ] +
		inWeight0.w * s_boneMatrices[ int(inJoint0.w) ];

    return skinMat;
}

VSOut MAIN()
{
    VSOut OUT;

    vec4 locPos;
    mat4 skinMat = u_modelMatrix;

    if( VKA_HAS_JOINTS_0)
    {
      skinMat = u_modelMatrix *  getBoneTransformMatrix( in_WEIGHTS_0, in_JOINTS_0);
    }

    locPos = skinMat * vec4( in_POSITION, 1.0);

    OUT.glPosition = GLOBAL.PROJECTION_VIEW * locPos;
    OUT.position   = locPos.xyz;
    OUT.normal     = normalize(transpose(inverse(mat3( skinMat ))) * in_NORMAL);
    OUT.texCoord_0 = in_TEXCOORD_0;
    
    float t = float(VKA_HAS_COLOR_0);
    OUT.color      = mix( vec3(1,1,1), in_COLOR_0.rgb, t);



    return OUT;
}

