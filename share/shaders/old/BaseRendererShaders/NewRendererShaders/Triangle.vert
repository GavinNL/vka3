//=========================================================================
// The following are defined:
//
//
//
//        VKA_ATTRIBUTES_FLAGS
//        VKA_HAS_POSITION    
//        VKA_HAS_NORMAL      
//        VKA_HAS_TANGENT     
//        VKA_HAS_TEXCOORD_0  
//        VKA_HAS_TEXCOORD_1  
//        VKA_HAS_COLOR_0     
//        VKA_HAS_JOINTS_0    
//        VKA_HAS_WEIGHTS_0   
//=========================================================================
layout(location = 0) out vec3 f_POSITION;
layout(location = 1) out vec3 f_NORMAL;
layout(location = 2) out vec2 f_TEXCOORD_0;
layout(location = 3) out vec3 f_COLOR_0;

//=========================================================================
// These share the same set/binding because it is backed by a single
// storage buffer. This essentially creates 2 different views into
// the buffer..
//
// Uniforms should obey the proper alignment, that is they must align
// to vec2 or to vec4
//
// All uniforms must start with u_
//=========================================================================
uniform mat4 u_modelMatrix;
uniform int  u_useSkin;
uniform int  u_unused1;
uniform int  u_unused2;
uniform int  u_unused3;

//=========================================================================
// Storage arrays are used to store multiple objects of the same type.
// There is a maximum of 4 storageArrays per shader stage.
// To access the storage data, you must access it as follows:
//=========================================================================
storageArray mat4 s_boneMatrices;
//=========================================================================

mat4 getBoneTransformMatrix(vec4 inWeight0, uvec4 inJoint0)
{
	// Mesh is skinned
	mat4 skinMat =
		inWeight0.x * s_boneMatrices[ int(inJoint0.x) ] +
		inWeight0.y * s_boneMatrices[ int(inJoint0.y) ] +
		inWeight0.z * s_boneMatrices[ int(inJoint0.z) ] +
		inWeight0.w * s_boneMatrices[ int(inJoint0.w) ];

    return skinMat;
}


const vec3 MyArray[3]=vec3[3](
	    vec3( 0.0 ,-0.5 , 0), // top middle
	    vec3(-0.5 , 0.5 , 0), // Bottom left corner
	    vec3( 0.5 , 0.5 , 0)  // bottom right corner
    );

    const vec3 MyCol[3]=vec3[3](
	    vec3(1,0,0),  // red
	    vec3(0,1,0),  // green
	    vec3(0,0,1)   // blue
    );

void MAIN( )
{
    
    gl_Position  = vec4(MyArray[ gl_VertexIndex%3 ], 1.0);
    f_POSITION   = MyArray[ gl_VertexIndex%3 ];
    f_NORMAL     = vec3(0,0,1);
    f_TEXCOORD_0 = vec2(0,0);
    f_COLOR_0    = MyCol[ gl_VertexIndex%3 ];

}

