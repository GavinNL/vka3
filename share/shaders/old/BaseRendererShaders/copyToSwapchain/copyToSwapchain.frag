/**

This Shader is meant to be used with the vka::PBRRenderer
It cannot be compiled on its own, it must be compiled through the PBRRenderer
class as it adds extra data into the shader prior to compilation

*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform sampler2D   VKA_INPUT_TEXTURE;

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec3 f_NORMAL;
layout(location = 2) in vec2 f_TEXCOORD_0;
layout(location = 3) in vec3 f_COLOR_0;


#define VKA_TEXTURE_0 VKA_INPUT_TEXTURE

//============================================================
// Output Framebuffer data.
// These output values will go to a separate texture
//============================================================
layout (location = 0) out vec4 out_ALBEDO;

layout(push_constant) uniform PushConsts
{
    vec2 position;
    vec2 size;

    vec2 screenSize;
    int totalLights;
    int fragmentUniformIndex;

    vec3 cameraPosition;
    float unused;

    mat4 inverseViewMatrix;
} pushConsts;

struct PBROut
{
    vec3 position;
    vec3 normal;
    vec3 albedo;
};

PBROut defaultMain()
{
    PBROut OUT;

    OUT.albedo = texture( VKA_TEXTURE_0, f_TEXCOORD_0).rgb;

    return OUT;
}


__VKA_MAIN_FRAG__

void main()
{
    PBROut OUT = __VKA_FRAG_ENTRY_POINT__;

    //  ================= DO NOT MODIFY =======================
    out_ALBEDO.rgba   = vec4( OUT.albedo.rgb, 1.0);
    //out_POSITION.rgba = vec4( OUT.position, 1);
    //out_NORMAL.rgba   = vec4( OUT.normal, 1);

}
