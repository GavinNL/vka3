//=========================================================================
// These share the same set/binding because it is backed by a single
// storage buffer. This essentially creates 2 different views into
// the buffer..
//
// Uniforms should obey the proper alignment, that is they must align
// to vec2 or to vec4
//
// All uniforms must start with u_
//=========================================================================
uniform vec4   u_baseColorFactor          ;
uniform float  u_metallicFactor           ;
uniform float  u_roughnessFactor          ;
uniform sampler2D_id    u_baseColorTexture         ;
uniform sampler2D_id    u_metallicRoughnessTexture ;
uniform sampler2D_id    u_normalTexture            ;
uniform sampler2D_id    u_occlusionTexture         ;
uniform sampler2D_id    u_emissiveTexture          ;
uniform sampler2D_id    u_unused                   ;


//=========================================================================
// Storage arrays are used to store multiple objects of the same type.
// There is a maximum of 4 storageArrays per shader stage.
// To access the storage data, you must access it as follows:
//=========================================================================
//=========================================================================

FSOut MAIN()
{
    FSOut OUT;

    OUT.position  = f_POSITION;
    OUT.normal    = f_NORMAL;
    OUT.albedo    = u_baseColorFactor.rgb;
    OUT.metallic  = u_metallicFactor;
    OUT.roughness = u_roughnessFactor;

    if( valid(u_baseColorTexture) )
      OUT.albedo *= texture(u_baseColorTexture, f_TEXCOORD_0).rgb;

    return OUT;
}
