/*

This Shader is meant to be used with the vka::PBRRenderer
It cannot be compiled on its own, it must be compiled through the PBRRenderer
class as it adds extra data into the shader prior to compilation

DO NOT EDIT THIS!
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable


//============================================================
// Input Vertex Attributes
//
// All vertex attributes defined by the GLTF spec must be
// used as an input. The render system will bind
// an "undefined" buffer to any input attributes that
// do not exist in the mesh.
//============================================================
layout(location = 0)  in vec3  in_POSITION;
layout(location = 1)  in vec3  in_NORMAL;
layout(location = 2)  in vec3  in_TANGENT;
layout(location = 3)  in vec2  in_TEXCOORD_0;
layout(location = 4)  in vec2  in_TEXCOORD_1;
layout(location = 5)  in vec3  in_COLOR_0;
layout(location = 6)  in uvec4 in_JOINTS_0;
layout(location = 7)  in vec4  in_WEIGHTS_0;

//============================================================
//
//============================================================
layout(location = 0) out vec3 f_POSITION;
layout(location = 1) out vec3 f_NORMAL;
layout(location = 2) out vec2 f_TEXCOORD_0;
layout(location = 3) out vec3 f_COLOR_0;


//------------------------------------------------------------
//
//------------------------------------------------------------

layout(push_constant) uniform PushConsts {
    int vertexUniformIndex  ;
    int fragmentUniformIndex;
    int attributeFlags;
    int unused;
} pushConsts;


// MUST be set0 binding 2 if this uniform exists
layout(set=0, binding=2) uniform GLOBAL_UNIFORM_t
{
    float  SCREEN_WIDTH   ;
    float  SCREEN_HEIGHT  ;
    float  MOUSE_X        ;
    float  MOUSE_Y        ;
    float  MOUSE_Z        ;
    float  TIME_INT       ;
    float  TIME_FRAC      ;
    float  UNUSED1        ;
    vec3   MOUSE_NEAR     ;
    float  UNUSED2        ;
    vec3   MOUSE_FAR      ;
    float  UNUSED3        ;
    vec3   CAMERA_POSITION;
    float  UNUSED4        ;
    mat4   VIEW           ;
    mat4   PROJECTION     ;
    mat4   PROJECTION_VIEW;

} GLOBAL_UNIFORM;


/**
This uniform is for the per-frame information

Set 0 binding 0 should always be the Global Per frame shader.

Note that alignment is extremely important, everything must
be aligned to multiples of 2 floats...
**/

struct VertexUniform_t
{
    int    worldMatrixIndex;
    int    nodeMatrixIndex;
};

//=========================================================================
// These share the same set/binding because it is backed by a single
// storage buffer. This essentially creates 2 different views into
// the buffer.
//=========================================================================
layout(set = 0, binding = 0) buffer readonly STORAGE_RENDER_INFO_t
{
    VertexUniform_t info[];
} STORAGE_RENDER_INFO;

layout(set = 0, binding = 0) buffer readonly STORAGE_MATRIX_t
{
    mat4 transform[];
} STORAGE_MATRIX;
//=========================================================================

out gl_PerVertex
{
  vec4 gl_Position;
};

struct VertexOut
{
  vec3 position;
  vec3 normal;
  vec3 color;
  vec2 texCoord_0;

};

#define VKA_MATRIX                   STORAGE_MATRIX.transform

#define VKA_RENDER_INFO              STORAGE_RENDER_INFO.info[ pushConsts.vertexUniformIndex ]

#define VKA_NODE_MATRIX_INDEX        VKA_RENDER_INFO.nodeMatrixIndex
#define VKA_MODEL_MATRIX_INDEX       VKA_RENDER_INFO.worldMatrixIndex

#define VKA_ATTRIBUTES_FLAGS         pushConsts.attributeFlags

#define VKA_PROJECTION_MATRIX       GLOBAL_UNIFORM.PROJECTION
#define VKA_VIEW_MATRIX             GLOBAL_UNIFORM.VIEW
#define VKA_PROJECTION_VIEW_MATRIX  GLOBAL_UNIFORM.PROJECTION_VIEW
#define VKA_MODEL_MATRIX            VKA_MATRIX[ VKA_MODEL_MATRIX_INDEX ]
#define VKA_NODE_MATRIX             VKA_MATRIX[ VKA_NODE_MATRIX_INDEX  ]


#define VKA_HAS_POSITION    (( VKA_ATTRIBUTES_FLAGS & 0x01)==0x01)
#define VKA_HAS_NORMAL      (( VKA_ATTRIBUTES_FLAGS & 0x02)==0x02)
#define VKA_HAS_TANGENT     (( VKA_ATTRIBUTES_FLAGS & 0x04)==0x04)
#define VKA_HAS_TEXCOORD_0  (( VKA_ATTRIBUTES_FLAGS & 0x08)==0x08)
#define VKA_HAS_TEXCOORD_1  (( VKA_ATTRIBUTES_FLAGS & 0x10)==0x10)
#define VKA_HAS_COLOR_0     (( VKA_ATTRIBUTES_FLAGS & 0x20)==0x20)
#define VKA_HAS_JOINTS_0    (( VKA_ATTRIBUTES_FLAGS & 0x40)==0x40)
#define VKA_HAS_WEIGHTS_0   (( VKA_ATTRIBUTES_FLAGS & 0x80)==0x80)

mat4 getBoneTransformMatrix(vec4 inWeight0, uvec4 inJoint0)
{
    int skinTransformIndexOffset = VKA_NODE_MATRIX_INDEX+1;

	// Mesh is skinned
	mat4 skinMat =
		inWeight0.x * VKA_MATRIX[ skinTransformIndexOffset + int(inJoint0.x) ] +
		inWeight0.y * VKA_MATRIX[ skinTransformIndexOffset + int(inJoint0.y) ] +
		inWeight0.z * VKA_MATRIX[ skinTransformIndexOffset + int(inJoint0.z) ] +
		inWeight0.w * VKA_MATRIX[ skinTransformIndexOffset + int(inJoint0.w) ];

    return skinMat;
}

VertexOut defaultMain()
{
  VertexOut OUT;

  vec4 locPos = vec4(0,0,0,0);
  vec3 outWorldPos;
  vec3 outNormal;

  if( VKA_HAS_JOINTS_0)
  {
      mat4 skinMat = VKA_MODEL_MATRIX * VKA_NODE_MATRIX * getBoneTransformMatrix( in_WEIGHTS_0, in_JOINTS_0);
      locPos       =  skinMat * vec4(in_POSITION, 1.0);

      outNormal = normalize(transpose(inverse(mat3( skinMat))) * in_NORMAL);
  }
  else
  {
      mat4 skinMat = VKA_MODEL_MATRIX * VKA_NODE_MATRIX;
      locPos  = skinMat * vec4( in_POSITION , 1.0);

      outNormal = normalize(transpose(inverse(mat3( skinMat ))) * in_NORMAL);
  }

  outWorldPos = locPos.xyz / locPos.w;

  OUT.position   = locPos.xyz;
  OUT.normal     = outNormal;
  OUT.texCoord_0 = in_TEXCOORD_0;
  OUT.color      = vec3(1,1,1);

  return OUT;
}

__VKA_MAIN_VERT__


void main()
{

  VertexOut OUT = __VKA_VERT_ENTRY_POINT__;


  //====================== DO NOT EDIT =====================
  f_POSITION   = OUT.position;//outWorldPos;
  f_NORMAL     = OUT.normal;//outNormal;
  f_TEXCOORD_0 = OUT.texCoord_0;//in_TEXCOORD_0;
  f_COLOR_0    = OUT.color;

  gl_Position  =  VKA_PROJECTION_MATRIX * (VKA_VIEW_MATRIX * vec4( OUT.position, 1.0) );
  // No need to do this since we're going to be modifying the projection
  // matrix
  //gl_Position.y = -gl_Position.y;
}
