PBROut fragmentOut()
{
    PBROut ot = defaultMain();
    ot.albedo.xyz = vec3(ot.albedo.x + ot.albedo.y + ot.albedo.z) * 0.3333;
    return ot;
}
