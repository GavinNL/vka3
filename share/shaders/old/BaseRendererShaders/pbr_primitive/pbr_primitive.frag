/**

This Shader is meant to be used with the vka::PBRRenderer
It cannot be compiled on its own, it must be compiled through the PBRRenderer
class as it adds extra data into the shader prior to compilation

*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

#ifndef VKA_MAX_TEXTURES
#define VKA_MAX_TEXTURES 32
#endif

layout(set = 1, binding = 0) uniform sampler2D VKA_INPUT_TEXTURE[VKA_MAX_TEXTURES];

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec3 f_NORMAL;
layout(location = 2) in vec2 f_TEXCOORD_0;
layout(location = 3) in vec3 f_COLOR_0;


//============================================================
// Output Framebuffer data.
// These output values will go to a separate texture
//============================================================
layout (location = 0) out vec4 out_ALBEDO;
layout (location = 1) out vec4 out_POSITION;
layout (location = 2) out vec4 out_NORMAL;


layout (push_constant) uniform PushConsts
{
    int vertexUniformIndex  ;
    int fragmentUniformIndex;
} pushConsts;


struct FragmentUniform_t
{
    vec4   baseColorFactor          ;

    float  metallicFactor           ;
    float  roughnessFactor          ;
    int    baseColorTexture         ;
    int    metallicRoughnessTexture ;

    int    normalTexture            ;
    int    occlusionTexture         ;
    int    emissiveTexture          ;
    int    unused                   ;
};

layout(set = 0, binding = 1) buffer readonly STORAGE_MATERIAL_t
{

    FragmentUniform_t material[];

} STORAGE_MATERIAL;


struct PBROut
{
    vec3 position;
    vec3 normal;
    vec3 albedo;
    float metallic;
    float roughness;
};

#define VKA_UNIFORM  STORAGE_MATERIAL.material[pushConsts.fragmentUniformIndex]


/*------------------------------------------------------------------------------
struct PBROut
{
vec3 position;
vec3 normal;
vec3 albedo;
};

The F
------------------------------------------------------------------------------*/

#if 1
PBROut defaultMain()
{
    PBROut OUT;

    // index to look up in the STORAGE_MATERIAL
    int baseColorTexture = VKA_UNIFORM.baseColorTexture;
    int metallicRoughnessTexture = VKA_UNIFORM.metallicRoughnessTexture;

    OUT.metallic  = VKA_UNIFORM.metallicFactor;
    OUT.roughness = VKA_UNIFORM.roughnessFactor;

    if( metallicRoughnessTexture == -1)
    {
        OUT.albedo    = VKA_UNIFORM.baseColorFactor.rgb;
    }
    else
    {
          //OUT.albedo = VKA_UNIFORM.baseColorFactor.rgb;
        OUT.albedo   = texture(  VKA_INPUT_TEXTURE[ baseColorTexture ], f_TEXCOORD_0.xy   ).rgb;
    }

    if( metallicRoughnessTexture != -1)
    {
        //vec3 mrt = texture(  VKA_INPUT_TEXTURE[ metallicRoughnessTexture ], f_TEXCOORD_0.xy   ).rgb;
        //OUT.albedo = mrt;
        //OUT.metallic  = mrt.g;
        //OUT.roughness = 0.5;//mrt.g;
    }

    OUT.position = f_POSITION.rgb;
    OUT.normal   = f_NORMAL.rgb;

    return OUT;
}
#else
PBROut defaultMain()
{
    PBROut OUT;

    // index to look up in the STORAGE_MATERIAL
    int MATERIAL_INDEX   = f_materialIndex.x;
    int baseColorTexture = VKA_MATERIAL[MATERIAL_INDEX].baseColorTexture;

    //OUT.albedo = VKA_MATERIAL[MATERIAL_INDEX].baseColorFactor.rgb;
    OUT.albedo = vec3(1,0,0);
    OUT.position = f_POSITION.rgb;
    OUT.normal   = f_NORMAL.rgb;

    return OUT;
}
#endif

__VKA_MAIN_FRAG__

void main()
{
    PBROut OUT = __VKA_FRAG_ENTRY_POINT__;

    //  ================= DO NOT MODIFY =======================
    out_ALBEDO.rgba   = vec4( OUT.albedo.rgb, OUT.metallic);
    out_POSITION.rgba = vec4( OUT.position, 1);
    out_NORMAL.rgba   = vec4( OUT.normal, OUT.roughness);

}
