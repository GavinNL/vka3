/**
*
* This is a very simple shader for testing. It is a constant shader
* that always produces a coloured triangle. It doesn't require any
* vertex attributes, or uniform buffers.
*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_Normal;
layout(location = 2) in vec2 in_TexCoord_0;

layout(location = 0) out vec3 f_Position;
layout(location = 1) out vec3 f_Normal;
layout(location = 2) out vec2 f_TexCoord_0;
layout(location = 3) out vec3 f_TexCoord_1;

layout(set=0, binding=0) uniform UniformBufferObject {
    mat4 model;
    mat4 proj;
} cameraData;


out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{

    f_Position   = (cameraData.model * vec4( in_Position, 1.0)).xyz;
    f_Normal     = in_Normal;
    f_TexCoord_0 = in_TexCoord_0;
    f_TexCoord_1 = normalize(in_Position);
    gl_Position  =  cameraData.proj * vec4( f_Position, 1.0);
}
