/*------------------------------------------------------------------------------
This is the Default PBR Fragment shader for the PBRPrimitiveRenderer Class.

This is the VertexOut class. You must return this type with the values filled out.

struct VertexOut
{
    vec3 position;
    vec3 normal;
    vec3 color;
    vec2 texCoord_0;

};

--------------------------------------------------------------------------------
Inputs:
The following input data is available for all vertex shaders that use.
Note: Only certain input attributes may contain valid data. Check the
VKA_HAS_<PROPERTY>  constant to determine if this instance has the valid attributes
--------------------------------------------------------------------------------
in vec3  in_POSITION;
in vec3  in_NORMAL;
in vec3  in_TANGENT;
in vec2  in_TEXCOORD_0;
in vec2  in_TEXCOORD_1;
in vec3  in_COLOR_0;
in uvec4 in_JOINTS_0;
in vec4  in_WEIGHTS_0;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
Uniforms.
These values are available to be used inside the shader.
Note: When actually using the PBRPrimitiveRenderer class in code, you must set
these values in the C++ code before they can be accessed by the shader.
--------------------------------------------------------------------------------
float GLOBAL_UNIFORM.SCREEN_WIDTH
float GLOBAL_UNIFORM.SCREEN_HEIGHT
float GLOBAL_UNIFORM.MOUSE_X
float GLOBAL_UNIFORM.MOUSE_Y
float GLOBAL_UNIFORM.MOUSE_Z
float GLOBAL_UNIFORM.TIME_INT
float GLOBAL_UNIFORM.TIME_FRAC
vec3  GLOBAL_UNIFORM.MOUSE_NEAR
vec3  GLOBAL_UNIFORM.MOUSE_FAR
vec3  GLOBAL_UNIFORM.CAMERA_POSITION
mat4  GLOBAL_UNIFORM.VIEW
mat4  GLOBAL_UNIFORM.PROJECTION
mat4  GLOBAL_UNIFORM.PROJECTION_VIEW
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
Textures:
--------------------------------------------------------------------------------
uniform sampler2D VKA_INPUT_TEXTURE[VKA_MAX_TEXTURES];
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
Constants:
These constants/flags are accessble from this shader:
--------------------------------------------------------------------------------
VKA_NODE_MATRIX_INDEX
VKA_MODEL_MATRIX_INDEX
VKA_FIRST_BONE_MATRIX_INDEX
VKA_PROJECTION_VIEW_MATRIX
VKA_PROJECTION_MATRIX
VKA_VIEW_MATRIX
VKA_MODEL_MATRIX
VKA_NODE_MATRIX
VKA_HAS_POSITION
VKA_HAS_NORMAL
VKA_HAS_TANGENT
VKA_HAS_TEXCOORD_0
VKA_HAS_TEXCOORD_1
VKA_HAS_COLOR_0
VKA_HAS_JOINTS_0
VKA_HAS_WEIGHTS_0
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
Fucntions:
These functions are avaialble in the shader to be used
--------------------------------------------------------------------------------
// returns a matrix which is the full transformation of the bone's influence
mat4 getBoneTransformMatrix(vec4 inWeight0, uvec4 inJoint0)
--------------------------------------------------------------------------------


------------------------------------------------------------------------------*/

VertexOut vertexOut()
{
    VertexOut OUT;

    f_materialIndex = pushConsts.materialIndex + gl_InstanceIndex;
    vec4 locPos = vec4(0,0,0,0);
    vec3 outWorldPos;
    vec3 outNormal;

    float t = GLOBAL_UNIFORM.TIME_INT  + GLOBAL_UNIFORM.TIME_FRAC;
          t = t * 2.0f*3.14159265359;

    float k = 2.0f*3.1141592653594159/2.0f;

    float r = length(in_POSITION.xyz);
    vec3 inPosition = in_POSITION * (1.0f + 0.5f*sin( k*r - 0.5f*t ) );

    if ( VKA_HAS_JOINTS_0 )
    {
            mat4 skinMat   = VKA_MODEL_MATRIX * VKA_NODE_MATRIX * getBoneTransformMatrix( in_WEIGHTS_0, in_JOINTS_0);

            locPos    =  skinMat * vec4(inPosition, 1.0);

            outNormal = normalize(transpose(inverse(mat3( skinMat))) * in_NORMAL);
    }
    else
    {
        mat4 skinMat = VKA_MODEL_MATRIX * VKA_NODE_MATRIX;
        locPos  = skinMat * vec4( inPosition , 1.0);

        outNormal = normalize(transpose(inverse(mat3( skinMat ))) * in_NORMAL);
    }

    outWorldPos = locPos.xyz / locPos.w;

    OUT.position   =  outWorldPos;
    OUT.normal     =  outNormal;
    OUT.texCoord_0 =  in_TEXCOORD_0;
    OUT.color      = vec3(1,1,1);

    return OUT;
}
