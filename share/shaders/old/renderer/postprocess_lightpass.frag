//==============================================================================
// This shader is meant to be used with the vka::PostProcessRenderer2 class.
//
//==============================================================================
#define MAX_LIGHTS 10

struct Light_t
{
  vec4  position;
  vec4  radiance;
};

VKA_POSTPROCESS_UNIFORM
{

    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
    float MOUSE_X;
    float MOUSE_Y;

    int   NUM_LIGHTS;
    int   UNUSED_0;
    int   UNUSED_1;
    int   UNUSED_2;

    vec4    CAMERA_POSITION;

    Light_t LIGHT[MAX_LIGHTS];

} UNIFORM;


//===
vec3 getEyePosition()
{
    return UNIFORM.CAMERA_POSITION.xyz;
}

// Find the normal for this fragment, pulling either from a predefined normal map
// or from the interpolated mesh normal and tangent attributes.
vec3 getNormal()
{
    return texture( TEXTURE_2, f_TEXCOORD_0).rgb;
}

vec3 getAlbedo()
{
    return texture( TEXTURE_0, f_TEXCOORD_0).rgb;
}

vec3 getFragmentPosition()
{
    return texture( TEXTURE_1, f_TEXCOORD_0).rgb;
}
float getMetallic()
{
    return 0.5;
}

float getRoughness()
{
    return 0.5;
}

// Physically Based Rendering
// Copyright (c) 2017-2018 Michał Siejak

// Physically Based shading model: Lambetrtian diffuse BRDF + Cook-Torrance microfacet specular BRDF + IBL for ambient.

// This implementation is based on "Real Shading in Unreal Engine 4" SIGGRAPH 2013 course notes by Epic Games.
// See: http://blog.selfshadow.com/publications/s2013-shading-course/karis/s2013_pbs_epic_notes_v2.pdf

const float PI = 3.141592;
const float Epsilon = 0.00001;

// Constant normal incidence Fresnel factor for all dielectrics.
const vec3 Fdielectric = vec3(0.04);

// GGX/Towbridge-Reitz normal distribution function.
// Uses Disney's reparametrization of alpha = roughness^2.
float ndfGGX(float cosLh, float roughness)
{
	float alpha   = roughness * roughness;
	float alphaSq = alpha * alpha;

	float denom = (cosLh * cosLh) * (alphaSq - 1.0) + 1.0;
	return alphaSq / (PI * denom * denom);
}

// Single term for separable Schlick-GGX below.
float gaSchlickG1(float cosTheta, float k)
{
	return cosTheta / (cosTheta * (1.0 - k) + k);
}

// Schlick-GGX approximation of geometric attenuation function using Smith's method.
float gaSchlickGGX(float cosLi, float cosLo, float roughness)
{
	float r = roughness + 1.0;
	float k = (r * r) / 8.0; // Epic suggests using this roughness remapping for analytic lights.
	return gaSchlickG1(cosLi, k) * gaSchlickG1(cosLo, k);
}

// Shlick's approximation of the Fresnel factor.
vec3 fresnelSchlick(vec3 F0, float cosTheta)
{
	return F0 + (vec3(1.0) - F0) * pow(1.0 - cosTheta, 5.0);
}


/**
This function should return the colour of the 3d Cube
map
**/
vec3 getIrradiance(vec3 dir)
{
    #if 1
    return vec3(1.5,1.5,1.5);
    #else
    // Sample diffuse irradiance at normal direction.
    vec3 irradiance = texture(irradianceTexture, dir).rgb;
    return irradiance;
    #endif

}

vec3 getSpecularIrradiance(vec3 dir)
{
    #if 1
    return vec3(0.5,0.5,0.5);
    #else
    // Sample pre-filtered specular reflection environment at correct mipmap level.
    int specularTextureLevels = textureQueryLevels(specularTexture);
    vec3 specularIrradiance   = textureLod(specularTexture, dir, roughness * specularTextureLevels).rgb;
    return specularIrradiance;
    #endif

}

int getNumLights()
{
    return UNIFORM.NUM_LIGHTS;
    return 1;
}
















float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec4 fragmentOut()
{
    vec4 fragOut = vec4(0,0,0,1);
    //fragOut = pbrMain();
    //return;
	// Sample input textures to get shading model params.
	vec3  albedo    = getAlbedo();//texture(albedoTexture, vin.texcoord).rgb;
	float metalness = getMetallic();//texture(metalnessTexture, vin.texcoord).r;
	float roughness = getRoughness();//texture(roughnessTexture, vin.texcoord).r;

    vec3 eyePosition      = getEyePosition();
    vec3 fragmentPosition = getFragmentPosition();

	// Outgoing light direction (vector from world-space fragment position to the "eye").
	vec3 Lo = normalize( eyePosition - fragmentPosition );

	// Get current fragment's normal and transform to world space.
	vec3 surfaceNormal = getNormal();//normalize(2.0 * texture(normalTexture, vin.texcoord).rgb - 1.0);

    if( length(surfaceNormal.rgb) < 0.001)
    {
        fragOut = vec4(albedo.rgb, 1.0);
        return fragOut;
    }

	// Angle between surface normal and outgoing light direction.
	float cosLo = max(0.0, dot(surfaceNormal, Lo));

	// Specular reflection vector.
	vec3 Lr = 2.0 * cosLo * surfaceNormal - Lo;

	// Fresnel reflectance at normal incidence (for metals use albedo color).
	vec3 F0 = mix(Fdielectric, albedo, metalness);

	// Direct lighting calculation for analytical lights.
	vec3 directLighting = vec3(0);

    int NumLights = getNumLights();

	for(int i=0; i<NumLights; ++i)
	{
        vec3 lightPosition = UNIFORM.LIGHT[i].position.xyz;

        float dist    = length(lightPosition - fragmentPosition);
        float attenuation = 1.0 / (dist * dist);

        // incoming light direction.
		vec3 Li        = normalize(lightPosition - fragmentPosition );//-UNIFORM.LIGHT[i].direction;
		vec3 Lradiance =  UNIFORM.LIGHT[i].radiance.rgb * attenuation;

		// Half-vector between Li and Lo.
		vec3 Lh = normalize(Li + Lo);

		// Calculate angles between surface normal and various light vectors.
		float cosLi = max(0.0, dot(surfaceNormal, Li));
		float cosLh = max(0.0, dot(surfaceNormal, Lh));

		// Calculate Fresnel term for direct lighting.
		vec3 F  = fresnelSchlick(F0, max(0.0, dot(Lh, Lo)));
		// Calculate normal distribution for specular BRDF.
		float D = ndfGGX(cosLh, roughness);
		// Calculate geometric attenuation for specular BRDF.
		float G = gaSchlickGGX(cosLi, cosLo, roughness);

		// Diffuse scattering happens due to light being refracted multiple times by a dielectric medium.
		// Metals on the other hand either reflect or absorb energy, so diffuse contribution is always zero.
		// To be energy conserving we must scale diffuse BRDF contribution based on Fresnel factor & metalness.
		vec3 kd = mix(vec3(1.0) - F, vec3(0.0), metalness);

		// Lambert diffuse BRDF.
		// We don't scale by 1/PI for lighting & material units to be more convenient.
		// See: https://seblagarde.wordpress.com/2012/01/08/pi-or-not-to-pi-in-game-lighting-equation/
		vec3 diffuseBRDF = kd * albedo;

		// Cook-Torrance specular microfacet BRDF.
		vec3 specularBRDF = (F * D * G) / max(Epsilon, 4.0 * cosLi * cosLo);

		// Total contribution for this light.
		directLighting += (diffuseBRDF + specularBRDF) * Lradiance * cosLi;
	}



	// Ambient lighting (IBL).
	vec3 ambientLighting = vec3(1.0,1.0,1.0);
#if 1
	{
		// Sample diffuse irradiance at normal direction.
		vec3 irradiance = getIrradiance(surfaceNormal);//texture(irradianceTexture, surfaceNormal).rgb;

		// Calculate Fresnel term for ambient lighting.
		// Since we use pre-filtered cubemap(s) and irradiance is coming from many directions
		// use cosLo instead of angle with light's half-vector (cosLh above).
		// See: https://seblagarde.wordpress.com/2011/08/17/hello-world/
		vec3 F = fresnelSchlick(F0, cosLo);

		// Get diffuse contribution factor (as with direct lighting).
		vec3 kd = mix( vec3(1.0) - F, vec3(0.0), metalness);

		// Irradiance map contains exitant radiance assuming Lambertian BRDF, no need to scale by 1/PI here either.
		vec3 diffuseIBL = kd * albedo * irradiance;

		// Sample pre-filtered specular reflection environment at correct mipmap level.
		vec3 specularIrradiance   = getSpecularIrradiance(Lr);

		// Split-sum approximation factors for Cook-Torrance specular BRDF.
		vec2 specularBRDF = vec2(0.0);//texture(specularBRDF_LUT, vec2(cosLo, roughness)).rg;

		// Total specular IBL contribution.
		vec3 specularIBL = (F0 * specularBRDF.x + specularBRDF.y) * specularIrradiance;

		// Total ambient lighting contribution.
		ambientLighting = diffuseIBL + specularIBL;
	}
#endif
	// Final fragment color.
	fragOut = vec4(directLighting + ambientLighting, 1.0);
    return fragOut;
}
