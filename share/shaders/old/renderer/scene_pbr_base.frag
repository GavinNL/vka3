/**

This Shader is meant to be used with the vka::PBRRenderer
It cannot be compiled on its own, it must be compiled through the PBRRenderer
class as it adds extra data into the shader prior to compilation

*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

#ifndef VKA_MAX_TEXTURES
    #define VKA_MAX_TEXTURES 32
#endif

layout(set = 1, binding = 0) uniform sampler2D VKA_INPUT_TEXTURE[VKA_MAX_TEXTURES];

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec3 f_NORMAL;
layout(location = 2) in vec2 f_TEXCOORD_0;
layout(location = 3) in vec3 f_COLOR_0;


layout(location = 4) flat in int f_materialIndex;

//============================================================
// Output Framebuffer data.
// These output values will go to a separate texture
//============================================================
layout (location = 0) out vec4 out_ALBEDO;

#ifdef VKA_WRITE_POSITION_OUTPUT
    layout (location = 1) out vec4 out_POSITION;
#endif

#ifdef VKA_WRITE_NORMAL_OUTPUT
    layout (location = 2) out vec4 out_NORMAL;
#endif


//============================================================
// Global Uniform Buffer.
//
// This structure must be standardized
//============================================================
layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
{

    VKA_PBR_GLOBAL_UNIFORM_VARS

} GLOBAL_UNIFORM;


struct Material_t
{
    vec4      baseColorFactor;
    float     metallicFactor;
    float     roughnessFactor;
    int       baseColorTexture;
    int       metallicRoughnessTexture;
    int       normalTexture;
    int       occlusionTexture;
    int       emissiveTexture;
    int       unused;
};

layout(set = 0, binding = 2) buffer STORAGE_MATERIAL_t
{

    Material_t material[];

} STORAGE_MATERIAL;


layout(push_constant) uniform PushConsts
{

VKA_PBR_RENDERER_PUSHCONSTS_VARS

} pushConsts;


Material_t getMaterial(int i)
{
    return STORAGE_MATERIAL.material[i];
}


struct PBROut
{
    vec3 position;
    vec3 normal;
    vec3 albedo;

};


/*------------------------------------------------------------------------------
struct PBROut
{
    vec3 position;
    vec3 normal;
    vec3 albedo;
};

The F
------------------------------------------------------------------------------*/
PBROut defaultMain()
{
    PBROut OUT;

    // index to look up in the STORAGE_MATERIAL
    int MATERIAL_INDEX   = f_materialIndex;
    int baseColorTexture = vka_Material.baseColorTexture;

    if( baseColorTexture == -1)
    {
        OUT.albedo = vka_Material.baseColorFactor.rgb;
    }
    else
    {
        OUT.albedo   = texture(  VKA_INPUT_TEXTURE[ baseColorTexture ], f_TEXCOORD_0.xy   ).rgb;
    }

    OUT.position = f_POSITION.rgb;
    OUT.normal   = f_NORMAL.rgb;

    return OUT;
}

__VKA_MAIN_FRAG__

void main()
{
    PBROut OUT = __VKA_FRAG_ENTRY_POINT__;

    //  ================= DO NOT MODIFY =======================
    out_ALBEDO.rgba = vec4(OUT.albedo.rgb, 1.0);
    #ifdef VKA_WRITE_POSITION_OUTPUT
        out_POSITION.rgba = vec4( OUT.position, 1);
    #endif

    #ifdef VKA_WRITE_NORMAL_OUTPUT
        out_NORMAL.rgba   = vec4( OUT.normal, 1);
    #endif

}
