//==============================================================================
// This shader is meant to be used with the vka::PostProcessRenderer2 class.
//
//==============================================================================
VKA_POSTPROCESS_UNIFORM
{

    float TIME_INT;
    float TIME_FRAC;
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;

} UNIFORM;


vec4 fragmentOut()
{
    vec2 r = vec2( cos( (UNIFORM.TIME_INT + UNIFORM.TIME_FRAC) ), sin( (UNIFORM.TIME_INT + UNIFORM.TIME_FRAC)) );

    vec3 norm = (texture( TEXTURE_0 , f_TEXCOORD_0.xy + 0.005*r  ).rgb +
                 texture( TEXTURE_0 , f_TEXCOORD_0.xy + vec2(0,0)     ).rgb +
                 texture( TEXTURE_0 , f_TEXCOORD_0.xy - 0.005*r ).rgb)*0.3333;

    return vec4(norm, 1.0);

}
