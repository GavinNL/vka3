#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec4 f_COLOR;

//============================================================
// Output Framebuffer data.
// These output values will go to a separate texture
//============================================================
layout (location = 0) out vec4 out_ALBEDO;

#ifdef VKA_WRITE_POSITION_OUTPUT
    layout (location = 1) out vec4 out_POSITION;
#endif

#ifdef VKA_WRITE_NORMAL_OUTPUT
    layout (location = 2) out vec4 out_NORMAL;
#endif

layout(set=0, binding=0) uniform UNIFORM_t
{
    VKA_RENDERER_UNIFORM_VARS
} UNIFORM;

layout(push_constant) uniform PushConsts
{

    VKA_RENDERER_PUSHCONSTS_VARS

} PUSHCONSTS;

void main()
{
    out_ALBEDO   = vec4(f_COLOR.rgb, 1.0);

    #ifdef VKA_WRITE_POSITION_OUTPUT
        out_POSITION = vec4(f_POSITION,1.0);
    #endif

    #ifdef VKA_WRITE_NORMAL_OUTPUT
        out_NORMAL   = vec4(0,0,0,1.0);
    #endif
}
