/**

This is a template of ridgid body pipeline with no lights

*/

#version 450
#extension GL_ARB_separate_shader_objects : enable


layout(location = 0) in vec3 in_POSITION;
layout(location = 1) in uint in_COLOR_0;

//============================================================
//
//============================================================
layout(location = 0) out vec3 f_POSITION;
layout(location = 1) out vec4 f_COLOR;

layout(set=0, binding=0) uniform UNIFORM_t
{
    mat4  PROJECTION;
    mat4  VIEW;
    mat4  PROJECTION_VIEW;
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
} UNIFORM;


/**
The push constant block is where all the vertex information
is stored for all 3 vertices in the triangle.

Total size of block: 112 bytes
mvp matrix = 64 bytes
vertex = 3 * ( 12 + 4 ) = 48

**/
layout(push_constant) uniform PushConsts
{

    VKA_RENDERER_PUSHCONSTS_VARS

} PUSHCONSTS;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    f_COLOR    = vec4( in_COLOR_0       & 0xFF,
                       (in_COLOR_0 >> 8)  & 0xFF,
                       (in_COLOR_0 >> 16) & 0xFF,
                       (in_COLOR_0 >> 21)  & 0xFF);
    f_COLOR   /= 255.0;

#if 1
    vec4 outWorldPos = VKA_MODEL_MATRIX * vec4( in_POSITION, 1);

    f_POSITION  = outWorldPos.xyz;
    gl_Position = VKA_PROJECTION_VIEW_MATRIX * outWorldPos;
#else
    vec4 outWorldPos = VKA_MODEL_MATRIX * vec4( in_POSITION, 1);

    f_POSITION  = outWorldPos.xyz;
    gl_Position = VKA_VIEW_MATRIX * outWorldPos;
#endif


}
