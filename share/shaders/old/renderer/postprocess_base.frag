//==============================================================================
// This the base fragment shader which is meant to be used with the vka::PostProcessRenderer2 class.
//
//
/*
    vka::PostProcessRenderer2 m_postProcessRenderer;
    auto PBRcreate = m_postProcessRenderer.createInfo2();

    // add the base shaders
    PBRcreate.baseVertexShaderPath     = getPath("postprocess_base.vert");
    PBRcreate.baseFragmentShaderPath   = getPath("postprocess_base.frag");

    // the acutal shader for post procesing
    PBRcreate.postProcessShaderPath    = getPath("postprocess_blur.frag");

    // number of frames in the swapchain
    PBRcreate.concurrentFrames         = concurrentFrameCount();

    // which descirptor pool should we allocate descriptors from
    PBRcreate.descriptorPool           = m_descriptorPool;

    // which buffer pool shoudl we allocate the uniform buffer from
    PBRcreate.uniformStorageBufferPool = m_bufferPoolUniform;

    // size of the swapchain image
    PBRcreate.screenSize               = swapchainImageSize();

    // maximum number of input textures into the post process shader
    PBRcreate.maxTextures           = 8;

    // Enable writing to the position and normal colour target
    PBRcreate.renderPass            = m_PostProcessingTarget1.getDefaultRenderPass();
    PBRcreate.albedoOutput          = m_ImageFinal2;

    m_postProcessRenderer.init(PBRcreate);
*/
//==============================================================================
#version 450
#extension GL_ARB_separate_shader_objects : enable

#ifndef VKA_MAX_TEXTURES
    #define VKA_MAX_TEXTURES 32
#endif

layout(set = 0, binding = 1) uniform sampler2D VKA_INPUT_TEXTURE[VKA_MAX_TEXTURES];

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec3 f_NORMAL;
layout(location = 2) in vec2 f_TEXCOORD_0;
layout(location = 3) in vec3 f_COLOR_0;


layout(location = 4) flat in int f_materialIndex;

//============================================================
// Output Framebuffer data.
// These output values will go to a separate texture
//============================================================
layout (location = 0) out vec4 out_ALBEDO;

#ifdef VKA_WRITE_POSITION_OUTPUT
    layout (location = 1) out vec4 out_POSITION;
#endif

#ifdef VKA_WRITE_NORMAL_OUTPUT
    layout (location = 2) out vec4 out_NORMAL;
#endif

#define TEXTURE_0 VKA_INPUT_TEXTURE[ pushConsts.inputs[0] ]
#define TEXTURE_1 VKA_INPUT_TEXTURE[ pushConsts.inputs[1] ]
#define TEXTURE_2 VKA_INPUT_TEXTURE[ pushConsts.inputs[2] ]
#define TEXTURE_3 VKA_INPUT_TEXTURE[ pushConsts.inputs[3] ]
#define TEXTURE_4 VKA_INPUT_TEXTURE[ pushConsts.inputs[4] ]
#define TEXTURE_5 VKA_INPUT_TEXTURE[ pushConsts.inputs[5] ]
#define TEXTURE_6 VKA_INPUT_TEXTURE[ pushConsts.inputs[6] ]
#define TEXTURE_7 VKA_INPUT_TEXTURE[ pushConsts.inputs[7] ]
//============================================================
// Global Uniform Buffer.
//
// This structure must be standardized
//============================================================

layout(push_constant) uniform PushConsts
{

VKA_PBR_RENDERER_PUSHCONSTS_VARS

} pushConsts;

struct PBROut
{
    vec3 position;
    vec3 normal;
    vec4 albedo;

};


vec4 defaultMain()
{
    vec3 norm = texture( VKA_INPUT_TEXTURE[ pushConsts.inputs[0] ] , f_TEXCOORD_0.xy  ).bgr;
    return vec4(norm, 1.0);
}

__VKA_MAIN_FRAG__

void main()
{
    PBROut OUT;
    OUT.albedo = __VKA_FRAG_ENTRY_POINT__;

    //  ================= DO NOT MODIFY =======================
    out_ALBEDO.rgba = vec4(OUT.albedo.rgb, 1.0);
    #ifdef VKA_WRITE_POSITION_OUTPUT
        out_POSITION.rgba = vec4( OUT.position, 1);
    #endif

    #ifdef VKA_WRITE_NORMAL_OUTPUT
        out_NORMAL.rgba   = vec4( OUT.normal, 1);
    #endif

}
