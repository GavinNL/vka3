/**

This Shader is meant to be used with the vka::PBRRenderer
It cannot be compiled on its own, it must be compiled through the PBRRenderer
class as it adds extra data into the shader prior to compilation

DO NOT EDIT THIS!
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable


//============================================================
// Input Vertex Attributes
//
// All vertex attributes defined by the GLTF spec must be
// used as an input. The render system will bind
// an "undefined" buffer to any input attributes that
// do not exist in the mesh.
//============================================================


//============================================================
//
//============================================================
layout(location = 0) out vec3 f_POSITION;
layout(location = 1) out vec3 f_NORMAL;
layout(location = 2) out vec2 f_TEXCOORD_0;
layout(location = 3) out vec3 f_COLOR_0;

//------------------------------------------------------------
//
//------------------------------------------------------------
layout(location = 4) flat out int f_materialIndex;  // used to tell the fragment shader what index to look at in the
                                                    // material storage buffer.



/**
This uniform is for the per-frame information

Set 0 binding 0 should always be the Global Per frame shader.

Note that alignment is extremely important, everything must
be aligned to multiples of 2 floats...
**/
// layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
// {
//
// VKA_PBR_GLOBAL_UNIFORM_VARS
//
// } GLOBAL_UNIFORM;


layout(push_constant) uniform PushConsts
{
    VKA_PBR_RENDERER_PUSHCONSTS_VARS

} pushConsts;

out gl_PerVertex
{
    vec4 gl_Position;
};

struct VertexOut
{
    vec3 position;
    vec3 normal;
    vec3 color;
    vec2 texCoord_0;

};

const vec2 MyArray[6]=vec2[6](
        vec2( 0.0 , 0.0), // topleft
        vec2( 0.0 , 1.0), // bottom left
        vec2( 1.0 , 1.0),  // bottom right corner
        vec2( 0.0 , 0.0), // topleft
        vec2( 1.0 , 1.0),  // bottom right corner
        vec2( 1.0 , 0.0) // top right
    );

    const vec2 TexCoord[6]=vec2[6](
        vec2(0,0),  // red
        vec2(0,1),  // green
        vec2(1,1),  // blue
      vec2(0,0),  // red
        vec2(1,1),  // green
        vec2(1,0)   // blue
    );

VertexOut defaultMain()
{
    VertexOut OUT;

    vec2 p = MyArray[gl_VertexIndex] * pushConsts.size + pushConsts.position;

    OUT.position   =  vec3( p.xy, 0.0);
    OUT.normal     =  vec3(0,0,1.0);
    OUT.texCoord_0 =  TexCoord[gl_VertexIndex];
    OUT.color      =  vec3(1,1,1);

    return OUT;
}

__VKA_MAIN_VERT__

void main()
{
    VertexOut OUT = __VKA_VERT_ENTRY_POINT__;

    //====================== DO NOT EDIT =====================
    f_POSITION   = OUT.position;//outWorldPos;
    f_NORMAL     = OUT.normal;//outNormal;
    f_TEXCOORD_0 = OUT.texCoord_0;//in_TEXCOORD_0;
    f_COLOR_0    = OUT.color;

    gl_Position  =  vec4( OUT.position, 1.0);
    // No need to do this since we're going to be modifying the projection
    // matrix
    //gl_Position.y = -gl_Position.y;
}
