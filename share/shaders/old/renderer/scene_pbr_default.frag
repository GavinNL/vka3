/*------------------------------------------------------------------------------
This is the Default PBR Fragment shader for the PBRPrimitiveRenderer Class.

This is the PBROut class. You must return this type with the values filled out.

struct PBROut
{
    vec3 position;
    vec3 normal;
    vec3 albedo;
};

--------------------------------------------------------------------------------
Inputs:
--------------------------------------------------------------------------------
    in vec3 f_POSITION;
    in vec3 f_NORMAL;
    in vec2 f_TEXCOORD_0;
    in vec3 f_COLOR_0;
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
Uniforms.
These values are available to be used inside the shader.
Note: When actually using the PBRPrimitiveRenderer class in code, you must set
these values in the C++ code before they can be accessed by the shader.
--------------------------------------------------------------------------------
float UNIFORM.SCREEN_WIDTH
float UNIFORM.SCREEN_HEIGHT
float UNIFORM.MOUSE_X
float UNIFORM.MOUSE_Y
float UNIFORM.MOUSE_Z
float UNIFORM.TIME_INT
float UNIFORM.TIME_FRAC
vec3  UNIFORM.MOUSE_NEAR
vec3  UNIFORM.MOUSE_FAR
vec3  UNIFORM.CAMERA_POSITION
mat4  UNIFORM.VIEW
mat4  UNIFORM.PROJECTION
mat4  UNIFORM.PROJECTION_VIEW
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
Textures:
--------------------------------------------------------------------------------
uniform sampler2D VKA_INPUT_TEXTURE[VKA_MAX_TEXTURES];
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
Constants:
These constants/flags are accessble from this shader:
--------------------------------------------------------------------------------
VKA_NODE_MATRIX_INDEX
VKA_MODEL_MATRIX_INDEX
VKA_FIRST_BONE_MATRIX_INDEX
VKA_PROJECTION_VIEW_MATRIX
VKA_PROJECTION_MATRIX
VKA_VIEW_MATRIX
VKA_MODEL_MATRIX
VKA_NODE_MATRIX
VKA_HAS_POSITION
VKA_HAS_NORMAL
VKA_HAS_TANGENT
VKA_HAS_TEXCOORD_0
VKA_HAS_TEXCOORD_1
VKA_HAS_COLOR_0
VKA_HAS_JOINTS_0
VKA_HAS_WEIGHTS_0
--------------------------------------------------------------------------------

------------------------------------------------------------------------------*/
PBROut fragmentOut()
{
    //================================
    PBROut OUT;
    //================================
    // Must set the following:
    //  vec3 OUT.position
    //  vec3 OUT.normal
    //  vec3 OUT.albedo
    //================================

    // index to look up in the STORAGE_MATERIAL
    int MATERIAL_INDEX   = f_materialIndex;
    int baseColorTexture = vka_Material.baseColorTexture;

    OUT.albedo   = baseColorTexture == -1 ?
                          vka_Material.baseColorFactor.rgb
                        : texture(  VKA_INPUT_TEXTURE[ baseColorTexture ], f_TEXCOORD_0.xy   ).rgb;
    OUT.position = f_POSITION.rgb;
    OUT.normal   = f_NORMAL.rgb;

    return OUT;
}
