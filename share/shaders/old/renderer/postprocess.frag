// You may modify below here.

// <-----------------   DO NOT MODIFY ----------------------------------->
layout(set=0, binding=0) uniform UNIFORM_t
{
// <-----------------   DO NOT MODIFY ----------------------------------->
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
    float MOUSE_X;
    float MOUSE_Y;
    float MOUSE_Z;
    float TIME_INT;
    float TIME_FRAC;
    float UNUSED_0;
// <-----------------   DO NOT MODIFY ----------------------------------->
} UNIFORM;
// <-----------------   DO NOT MODIFY ----------------------------------->

vec4 albedoPresent()
{
    vec2 r = vec2( cos( (UNIFORM.TIME_INT + UNIFORM.TIME_FRAC) ), sin( (UNIFORM.TIME_INT + UNIFORM.TIME_FRAC)) );

    vec3 norm = (texture( TEXTURE[0] , f_TEXCOORD_0.xy + 0.005*r  ).rgb +
                 texture( TEXTURE[0] , f_TEXCOORD_0.xy + vec2(0,0)     ).rgb +
                 texture( TEXTURE[0] , f_TEXCOORD_0.xy - 0.005*r ).rgb)*0.3333;

    return vec4(norm, 1.0);
}

void main()
{
    out_Color = albedoPresent();
}
