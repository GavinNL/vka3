#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 3) uniform sampler2D TEXTURE[1024];

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec3 f_NORMAL;
layout(location = 2) in vec2 f_TEXCOORD_0;
layout(location = 3) in vec3 f_COLOR_0;


layout(location = 4) flat in int f_materialIndex;



//============================================================
// Output Framebuffer data.
// These output values will go to a separate texture
//============================================================
layout (location = 0) out vec4 out_ALBEDO;
layout (location = 1) out vec4 out_POSITION;
layout (location = 2) out vec4 out_NORMAL;




//============================================================
// Global Uniform Buffer.
//
// This structure must be standardized
//============================================================
layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
{
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
    float MOUSE_X;
    float MOUSE_Y;
    float MOUSE_Z;
    float TIME_INT;
    float TIME_FRAC;
    float UNUSED1;
    vec3  MOUSE_NEAR;
    float UNUSED2;
    vec3  MOUSE_FAR;
    float UNUSED3;
    vec3  CAMERA_POSITION;
    float UNUSED4;
    mat4  VIEW;
    mat4  PROJECTION;
    mat4  PROJECTION_VIEW;

} GLOBAL_UNIFORM;


struct Material_t
{
    vec4      baseColorFactor;
    float     metallicFactor;
    float     roughnessFactor;
    int       baseColorTexture;
    int       metallicRoughnessTexture;
};

layout(set = 0, binding = 2) buffer STORAGE_MATERIAL_t
{

    Material_t material[];

} STORAGE_MATERIAL;


layout(push_constant) uniform PushConsts
{
    int  matrixIndex;
    int  materialIndex;
    int  hasBones;
} pushConsts;

void main()
{
  out_POSITION = vec4( f_POSITION, 1.0);
  out_NORMAL   = vec4( f_NORMAL  , 1.0);

  // index to look up in the STORAGE_MATERIAL
  int index = f_materialIndex;

  int baseColorTexture = STORAGE_MATERIAL.material[ index ].baseColorTexture;
  out_ALBEDO.a = 1.0;

  out_ALBEDO.rgb = STORAGE_MATERIAL.material[ index ].baseColorFactor.rgb;

  if( baseColorTexture == -1)
  {
     out_ALBEDO.rgb = STORAGE_MATERIAL.material[ index ].baseColorFactor.rgb;
  }
  else
  {
    out_ALBEDO.rgb   = texture(  TEXTURE[ baseColorTexture ], f_TEXCOORD_0.xy   ).rgb;
  }
  //out_ALBEDO.rgb   = texture(  TEXTURE[ 0 ], f_TEXCOORD_0.xy   ).rgb;

  // we need to figure out what to do with th
  out_POSITION.a = STORAGE_MATERIAL.material[ index ].metallicFactor;
  out_NORMAL.a   = STORAGE_MATERIAL.material[ index ].roughnessFactor;
  // out_NORMAL.g += STORAGE_MATERIAL.material[ index ].roughnessFactor;
}
