#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 2) uniform sampler2D TEXTURE[32];

//========================================================================
// Input attributes.
//========================================================================
layout(location = 0) in vec4 f_POSITION;
layout(location = 1) in vec2 f_TEXCOORD_0;


//========================================================================
// Output Color
//========================================================================
layout(location = 0) out vec4 out_Color;


//========================================================================
// Push constants. This should be in all stages.
//========================================================================
layout(push_constant) uniform PushConsts {
        vec2 position;
        vec2 size;
        vec2 screenSize;
        int  type;
        int  unused;
} pushConsts;


layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
{
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
    float MOUSE_X;
    float MOUSE_Y;
    float MOUSE_Z;
    float TIME_INT;
    float TIME_FRAC;
    float UNUSED1;
    vec3  MOUSE_NEAR;
    float UNUSED2;
    vec3  MOUSE_FAR;
    float UNUSED3;
    vec3  CAMERA_POSITION;
    float UNUSED4;
    mat4  VIEW;
    mat4  PROJECTION;
    mat4  PROJECTION_VIEW;

} GLOBAL_UNIFORM;


//==============================================================================


layout(set=0, binding=1) uniform UNIFORM_t
{
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
    float MOUSE_X;
    float MOUSE_Y;
    float MOUSE_Z;
    float TIME_INT;
    float TIME_FRAC;
    float UNUSED1;
    vec3  MOUSE_NEAR;
    float UNUSED2;
    vec3  MOUSE_FAR;
    float UNUSED3;
    vec3  CAMERA_POSITION;
    float UNUSED4;
    mat4  VIEW;
    mat4  PROJECTION;
    mat4  PROJECTION_VIEW;

} UNIFORM;


const float PI = 3.14159265359;

vec4 normalPresent()
{
    vec3 norm = texture( TEXTURE[ 2], f_TEXCOORD_0.xy ).rgb;
    return vec4(norm,1.0);
}

vec4 positionPresent()
{
    vec3 norm = texture( TEXTURE[ 1], f_TEXCOORD_0.xy ).rgb;
    return vec4(norm,1.0);
}

// vec4 horizontalBlur()
// {
//     float screenScale = 1.0f / GLOBAL_UNIFORM.SCREEN_WIDTH;
//     vec3 norm = texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(-2 * screenScale, 0)) .rgb +
//                 texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(-1 * screenScale, 0)) .rgb +
//                 texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2( 0 * screenScale, 0)) .rgb +
//                 texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2( 1 * screenScale, 0) ) .rgb +
//                 texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2( 2 * screenScale, 0) ) .rgb;
//
//     return vec4(norm*0.2,1.0);
// }

vec4 horizontalBlur()
{
    //float screenScale = 1.0f / pushConsts.screenSize.y;
    float screenScale = 1.0f / GLOBAL_UNIFORM.SCREEN_HEIGHT;

/*
const float coefs[11 ]= float[11](
    0.09, //0.000003	,
    0.09, //0.000229	,
    0.09, //0.005977	,
    0.09, //0.060598	,
    0.09, //0.24173	,
    0.09, //0.382925	,
    0.09, //0.24173	,
    0.09, //0.060598	,
    0.09, //0.005977	,
    0.09, //0.000229	,
    0.09); //0.000003);
*/
    const float coefs[11 ]= float[11](
        0.000003	,
        0.000229	,
        0.005977	,
        0.060598	,
        0.24173	,
        0.382925	,
        0.24173	,
        0.060598	,
        0.005977	,
        0.000229	,
        0.000003);

    vec3 norm = vec3(0.0f);
    for(int i=0;i<11;i++)
    {
       norm += coefs[i] * texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2( (i-5) * screenScale, 0 ) ) .rgb;
    }
    //vec3 norm = texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0, -2 * screenScale ) ) .rgb +
    //            texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0, -1 * screenScale ) ) .rgb +
    //            texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0,  0 * screenScale ) ) .rgb +
    //            texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0,  1 * screenScale ) ) .rgb +
    //            texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0,  2 * screenScale ) ) .rgb;

    return vec4(norm,1.0);
}

vec4 verticalBlur()
{
    //float screenScale = 1.0f / pushConsts.screenSize.y;
    float screenScale = 1.0f / GLOBAL_UNIFORM.SCREEN_HEIGHT;

/*
const float coefs[11 ]= float[11](
    0.09, //0.000003	,
    0.09, //0.000229	,
    0.09, //0.005977	,
    0.09, //0.060598	,
    0.09, //0.24173	,
    0.09, //0.382925	,
    0.09, //0.24173	,
    0.09, //0.060598	,
    0.09, //0.005977	,
    0.09, //0.000229	,
    0.09); //0.000003);
*/
    const float coefs[11 ]= float[11](
        0.000003	,
        0.000229	,
        0.005977	,
        0.060598	,
        0.24173	,
        0.382925	,
        0.24173	,
        0.060598	,
        0.005977	,
        0.000229	,
        0.000003);

    vec3 norm = vec3(0.0f);
    for(int i=0;i<11;i++)
    {
       norm += coefs[i] * texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0, (i-5) * screenScale ) ) .rgb;
    }
    //vec3 norm = texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0, -2 * screenScale ) ) .rgb +
    //            texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0, -1 * screenScale ) ) .rgb +
    //            texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0,  0 * screenScale ) ) .rgb +
    //            texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0,  1 * screenScale ) ) .rgb +
    //            texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(0,  2 * screenScale ) ) .rgb;

    return vec4(norm,1.0);
}

vec4 laplacian()
{
  vec2 screenScale = vec2(1.0f / GLOBAL_UNIFORM.SCREEN_WIDTH, 1.0f / GLOBAL_UNIFORM.SCREEN_HEIGHT);
  vec3 norm = -texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2( 1,  0 )*screenScale ) .rgb +
              -texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2(-1,  0 )*screenScale ) .rgb +
              4*texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2( 0,  0 )*screenScale ) .rgb +
              -texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2( 0, -1 )*screenScale ) .rgb +
              -texture( TEXTURE[ 0 ], f_TEXCOORD_0.xy + vec2( 0, -1 )*screenScale ) .rgb;

  return vec4(norm,1.0);
}

vec4 depthPresent()
{
    float d = texture( TEXTURE[0], f_TEXCOORD_0.xy ).r;
    return vec4(d,d,d,1.0);
}


vec4 albedoPresent()
{
    vec3 norm = texture( TEXTURE[0] , f_TEXCOORD_0.xy ) .rgb;

    return vec4( norm, 1.0);
}

void main()
{
  //vec2 AA = vec2( GLOBAL_UNIFORM.MOUSE_X / GLOBAL_UNIFORM.SCREEN_WIDTH,
  //                GLOBAL_UNIFORM.MOUSE_Y / GLOBAL_UNIFORM.SCREEN_HEIGHT);
  //out_Color = albedoPresent();
  //out_Color = laplacian();
  switch( pushConsts.type )
  {
      case 0: out_Color = horizontalBlur(); break;
      case 1: out_Color = verticalBlur(); break;
  }

  //out_Color = vec4( f_TEXCOORD_0.xy,0,1);
}
