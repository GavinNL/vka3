/**

This is a template of ridgid body pipeline with no lights

*/

#version 450
#extension GL_ARB_separate_shader_objects : enable



//============================================================
//
//============================================================
layout(location = 0) out vec3 f_POSITION;
layout(location = 1) out vec4 f_COLOR;
//layout(location = 1) out vec3 f_NORMAL;
//layout(location = 2) out vec2 f_TEXCOORD_0;


/**
This uniform is for the per-frame information

Set 0 binding 0 should always be the Global Per frame shader.

Note that alignment is extremely important, everything must
be aligned to multiples of 2 floats...
**/
layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
{
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
    float MOUSE_X;
    float MOUSE_Y;
    float MOUSE_Z;
    float TIME_INT;
    float TIME_FRAC;
    float UNUSED1;
    vec3  MOUSE_NEAR;
    float UNUSED2;
    vec3  MOUSE_FAR;
    float UNUSED3;
    vec3  CAMERA_POSITION;
    float UNUSED4;
    mat4  VIEW;
    mat4  PROJECTION;
    mat4  PROJECTION_VIEW;

} GLOBAL_UNIFORM;



struct vertex_t
{
  vec3 p; // position
  int  c; // color
};



/**
The push constant block is where all the vertex information
is stored for all 3 vertices in the triangle.

Total size of block: 112 bytes
mvp matrix = 64 bytes
vertex = 3 * ( 12 + 4 ) = 48

**/
layout(push_constant) uniform PushConsts
{
    mat4 model;
    vertex_t v[2];  // 3*16 bytes = 48
    int type; //0 - lines, 1-ellipse, 2-box
} pushConsts;

out gl_PerVertex
{
    vec4 gl_Position;
};


void drawLines()
{
  vec4 vertexModelPosition = vec4( pushConsts.v[gl_VertexIndex].p, 1.0);
  vec4 worldPosition = pushConsts.model * vertexModelPosition;

  gl_Position  =  GLOBAL_UNIFORM.PROJECTION_VIEW  * worldPosition;

  f_COLOR = vec4(
                   pushConsts.v[gl_VertexIndex].c & 0xFF,
                  (pushConsts.v[gl_VertexIndex].c >> 8) & 0xFF,
                  (pushConsts.v[gl_VertexIndex].c >> 16) & 0xFF,
                  0xFF);

  f_COLOR /= 255.0;

  f_POSITION   = worldPosition.xyz;

  gl_Position.y = -gl_Position.y;
}

void drawEllipse()
{
  float ax = pushConsts.v[0].p.x;
  float ay = pushConsts.v[0].p.y;
  float ds = pushConsts.v[0].p.z;

  float t = gl_VertexIndex/2 + gl_VertexIndex%2;

  float x = ax * cos( ds * t );
  float y = ay * sin( ds * t );

  vec4 vertexModelPosition = pushConsts.model * vec4( x,y,0, 1.0);
  vec4 worldPosition = vertexModelPosition;

  gl_Position  =  GLOBAL_UNIFORM.PROJECTION_VIEW  * worldPosition;


  f_COLOR = vec4(
                   pushConsts.v[0].c & 0xFF,
                  (pushConsts.v[0].c >> 8) & 0xFF,
                  (pushConsts.v[0].c >> 16) & 0xFF,
                  0xFF);

  f_COLOR /= 255.0;

  f_POSITION   = worldPosition.xyz;

  gl_Position.y = -gl_Position.y;
}

void drawBox()
{
  int i = gl_VertexIndex/8;

  const int i1[8] = int[8](0,1,1,2,2,3,3,0);
  const int i3[8] = int[8](0,4,1,5,2,6,3,7);

  const vec3 boxVertex[8]=vec3[8](
  	    vec3( 1 ,  -1  , 1), // top middle
        vec3( 1 ,  -1  ,-1), // top middle
        vec3(-1 ,  -1  ,-1), // top middle
        vec3(-1 ,  -1  , 1), // top middle

        vec3( 1 ,  1  , 1), // top middle
        vec3( 1 ,  1  ,-1), // top middle
        vec3(-1 ,  1  ,-1), // top middle
        vec3(-1 ,  1  , 1) // top middle
      );

 vec3 v = i%3==2 ? boxVertex[ i3[gl_VertexIndex%8] ] : boxVertex[ i1[gl_VertexIndex%8]   + (i%3==1 ? 4:0)     ];

  vec4 worldPosition = pushConsts.model * vec4( v, 1.0);
  gl_Position  =  GLOBAL_UNIFORM.PROJECTION_VIEW  * worldPosition;

  f_COLOR = vec4(
                   pushConsts.v[0].c & 0xFF,
                  (pushConsts.v[0].c >> 8) & 0xFF,
                  (pushConsts.v[0].c >> 16) & 0xFF,
                  0xFF);

  f_COLOR /= 255.0;

  f_POSITION   = worldPosition.xyz;
//  f_POSITION   = gl_Position.xyz;

  gl_Position.y = -gl_Position.y;
}

void drawBasic()
{
  const vec3 boxVertex[2]=vec3[2](
        vec3( -1 ,  -1  , 0 ), // top middle
        vec3(  1 ,   1  , 0 ) // top middle
      );
      const vec3 boxCol[2]=vec3[2](
            vec3(  1 ,   0  , 0 ), // top middle
            vec3(  0 ,   1  , 0 ) // top middle
          );

      gl_Position  =  vec4(boxVertex[ gl_VertexIndex%2 ], 1);
      f_COLOR = vec4( boxCol[ gl_VertexIndex%2], 1);
      f_POSITION = gl_Position.xyz;

      gl_Position.y = -gl_Position.y;
}

void main()
{

    switch( pushConsts.type )
    {
        case 0: drawLines();   return;
        case 1: drawEllipse(); return;
        case 2: drawBox(); return;;
        default:
          drawBasic();
          break;
    }
    return;

    gl_Position  =  GLOBAL_UNIFORM.PROJECTION_VIEW  * pushConsts.model * vec4( pushConsts.v[gl_VertexIndex].p, 1.0);

    f_COLOR = vec4(
                     pushConsts.v[gl_VertexIndex].c & 0xFF,
                    (pushConsts.v[gl_VertexIndex].c >> 8) & 0xFF,
                    (pushConsts.v[gl_VertexIndex].c >> 16) & 0xFF,
                    0xFF);

    f_COLOR /= 255.0;

    f_POSITION   = gl_Position.xyz;

    gl_Position.y = -gl_Position.y;
}
