#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 f_POSITION;
layout(location = 1) in vec4 f_COLOR;

//============================================================
// Output Framebuffer data.
// These output values will go to a separate texture
//============================================================
layout (location = 0) out vec4 out_ALBEDO;
layout (location = 1) out vec4 out_POSITION;
layout (location = 2) out vec4 out_NORMAL;


//============================================================
// Global Uniform Buffer.
//
// This structure must be standardized
//============================================================
layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
{
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
    float MOUSE_X;
    float MOUSE_Y;
    float MOUSE_Z;
    float TIME_INT;
    float TIME_FRAC;
    float UNUSED1;
    vec3  MOUSE_NEAR;
    float UNUSED2;
    vec3  MOUSE_FAR;
    float UNUSED3;
    vec3  CAMERA_POSITION;
    float UNUSED4;
    mat4  VIEW;
    mat4  PROJECTION;
    mat4  PROJECTION_VIEW;

} GLOBAL_UNIFORM;


void main()
{
  out_POSITION = vec4(f_POSITION,1.0);
  out_NORMAL   = vec4(0,0,0,1.0);
  out_ALBEDO   = vec4(f_COLOR.rgb, 1.0);
}
