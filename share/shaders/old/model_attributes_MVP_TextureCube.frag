#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 1) uniform samplerCube texSampler;

layout(location = 0) in vec3 f_Position;
layout(location = 1) in vec3 f_Normal;
layout(location = 2) in vec2 f_TexCoord_0;
layout(location = 3) in vec3 f_TexCoord_1;

layout(location = 0) out vec4 outColor;

void main()
{
    //outColor = f_Position.xyz;
    outColor = texture( texSampler, f_TexCoord_1   ).rgba;
}



