/**
*
* This is a very simple shader for testing. It is a constant shader
* that always produces a coloured triangle. It doesn't require any
* vertex attributes, or uniform buffers.
*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec3 f_Color;
layout(location = 1) out vec3 f_Position;
layout(location = 2) out vec3 f_Normal;

out gl_PerVertex 
{
    vec4 gl_Position;
};

const vec3 MyArray[3]=vec3[3](
	    vec3( 0.0 ,-0.5 , 0), // top middle
	    vec3(-0.5 , 0.5 , 0), // Bottom left corner
	    vec3( 0.5 , 0.5 , 0)  // bottom right corner
    );

    const vec3 MyCol[3]=vec3[3](
	    vec3(1,0,0),  // red
	    vec3(0,1,0),  // green
	    vec3(0,0,1)   // blue
    );

void main() 
{
    gl_Position  = vec4( MyArray[gl_VertexIndex], 1.0);

    f_Color = MyCol[gl_VertexIndex];
    f_Position = gl_Position.xyz;
    f_Normal = vec3(0,0,1);
}
