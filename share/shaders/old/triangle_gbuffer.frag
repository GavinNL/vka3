#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 f_Color;
layout(location = 1) in vec3 f_Position;
layout(location = 2) in vec3 f_Normal;

layout(location = 0) out vec4 outAlbedo;
layout(location = 1) out vec4 outPosition;
layout(location = 2) out vec4 outNormal;

void main() 
{
    outAlbedo   = vec4(f_Color,1);
    outPosition = vec4(f_Position,1);
    outNormal   = vec4(f_Normal,1);
}
