/**
*
* This is a very simple shader for testing. It is a constant shader
* that always produces a coloured triangle. It doesn't require any
* vertex attributes, or uniform buffers.
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_Color;

layout(location = 0) out vec4 f_Color;

layout(push_constant) uniform PushConsts {
        mat4 mvp;
} pushConsts;

out gl_PerVertex 
{
    vec4 gl_Position;
};

void main() 
{
    gl_Position  = pushConsts.mvp * vec4( in_Position, 1.0);
    f_Color      = vec4(in_Color,1.0);
}
