#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 2) uniform sampler2D TEXTURE[32];

//========================================================================
// Input attributes.
//========================================================================
layout(location = 0) in vec4 f_POSITION;
layout(location = 1) in vec2 f_TEXCOORD_0;


//========================================================================
// Output Color
//========================================================================
layout(location = 0) out vec4 out_Color;


//========================================================================
// Push constants. This should be in all stages.
//========================================================================
layout(push_constant) uniform PushConsts {
        vec2 position;
        vec2 size;
        vec2 screenSize;
        int  type;
        int  unused;
} pushConsts;


layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
{
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
    float MOUSE_X;
    float MOUSE_Y;
    float MOUSE_Z;
    float TIME_INT;
    float TIME_FRAC;
    float UNUSED1;
    vec3  MOUSE_NEAR;
    float UNUSED2;
    vec3  MOUSE_FAR;
    float UNUSED3;
    vec3  CAMERA_POSITION;
    float UNUSED4;
    mat4  VIEW;
    mat4  PROJECTION;
    mat4  PROJECTION_VIEW;

} GLOBAL_UNIFORM;


//==============================================================================

const float PI = 3.14159265359;

struct Light_t
{
  vec4 ambientColour;
  vec4 diffuseColour;
  vec4 specularColour;
  vec4 position;
  int   type;
  float constant, linear, quadratic;
};

//
layout(set=0, binding=1) uniform UNIFORM_t
{
    vec3    unused;
    int     lightCount;
    Light_t light[10];
} UNIFORM;


//==============================================================================
// The following section ius taken from: https://learnopengl.com/PBR/Lighting
//==============================================================================
vec3 CalcPointLight(Light_t light,
                    vec3 normal, vec3 fragPos, vec3 viewDir,
                    vec4 materialColour,
                    vec4 specularShiney)
{
    vec3 lightDir = normalize(light.position.xyz - fragPos);

    // diffuse shading: The Light Colour that reflects off the object and
    // hits the camera.
    float diff    = max(dot(normal, lightDir), 0.0);
    vec3 diffuse  = light.diffuseColour.rgb  * diff * materialColour.rgb;


    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), specularShiney.a);
    vec3 specular = light.specularColour.rgb * spec * specularShiney.rgb;


    // attenuation
    float distance    = length(light.position.xyz - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance +
  			     light.quadratic * (distance * distance));

    // combine results
    vec3 ambient  = light.ambientColour.rgb         * materialColour.rgb;



    ambient  *= attenuation;
    diffuse  *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
    //return (ambient + diffuse );
}


float DistributionGGX(vec3 N, vec3 H, float roughness)
{
    float a      = roughness*roughness;
    float a2     = a*a;
    float NdotH  = max(dot(N, H), 0.0);
    float NdotH2 = NdotH*NdotH;

    float num   = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return num / denom;
}

float GeometrySchlickGGX(float NdotV, float roughness)
{
    float r = (roughness + 1.0);
    float k = (r*r) / 8.0;

    float num   = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return num / denom;
}

float GeometrySmith(vec3 N, vec3 V, vec3 L, float roughness)
{
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx2  = GeometrySchlickGGX(NdotV, roughness);
    float ggx1  = GeometrySchlickGGX(NdotL, roughness);

    return ggx1 * ggx2;
}

vec3 fresnelSchlick(float cosTheta, vec3 F0)
{
    return F0 + (1.0 - F0) * pow(1.0 - cosTheta, 5.0);
}

vec4 pbrMain()
{
    vec4 albedo    = texture( TEXTURE[ 0], f_TEXCOORD_0.xy ).rgba;
    vec4 worldPos4 = texture( TEXTURE[ 1], f_TEXCOORD_0.xy ).rgba;
    vec4 norm4     = texture( TEXTURE[ 2], f_TEXCOORD_0.xy ).rgba;
    float depth    = texture( TEXTURE[ 3], f_TEXCOORD_0.xy ).r;

    vec3 camPos = GLOBAL_UNIFORM.CAMERA_POSITION.xyz;

    if( length(norm4.rgb) < 0.001)
    {
        return vec4(albedo.rgb, 1.0);
        //return vec4(albedo.rgb, 1.0) * (depth > 0.999999 ? 0 : 1 ) ;
    }
    vec3 norm     = normalize(norm4.rgb);
    vec3 worldPos = worldPos4.rgb;

    vec3 viewDir  = normalize( camPos - worldPos);
    //==========================================================================
    // TO DO:
    //==========================================================================
    float ao        = 0.1; // this should be retrieved from the gBuffer

    //float roughness = 0.5; // this should be retrieved from the gBuffer
    //float metallic  = 1.0;//* albedo.a;

    float roughness = norm4.a; // this should be retrieved from the gBuffer
    float metallic  = worldPos4.a;
    //==========================================================================

    vec3 N = normalize(norm);
    vec3 V = normalize(camPos - worldPos);




    vec3 F0 = vec3(0.04);
    F0 = mix(F0, albedo.xyz, metallic);

    // reflectance equation
    vec3 Lo = vec3(0.0);
    for(int i = 0; i < UNIFORM.lightCount; ++i)
    {
      vec3 lightPosition = UNIFORM.light[i].position.xyz;
      vec3 lightColour   = UNIFORM.light[i].diffuseColour.xyz;

      // calculate per-light radiance
      vec3 L = normalize( lightPosition - worldPos);
      vec3 H = normalize(V + L);
      float distance    = length(lightPosition - worldPos);

      float attenuation = 1.0 / (distance * distance);
      vec3 radiance     = lightColour * attenuation;

      // cook-torrance brdf
      float NDF = DistributionGGX(N, H, roughness);
      float G   = GeometrySmith(N, V, L, roughness);
      vec3 F    = fresnelSchlick(max(dot(H, V), 0.0), F0);

      vec3 kS = F;
      vec3 kD = vec3(1.0) - kS;
      kD *= 1.0 - metallic;

      vec3 numerator    = NDF * G * F;
      float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0);
      vec3 specular     = numerator / max(denominator, 0.001);

      // add to outgoing radiance Lo
      float NdotL = max(dot(N, L), 0.0);
      Lo += (kD * albedo.rgb / PI + specular) * radiance * NdotL;
    }

    vec3 ambient = vec3(0.1) * albedo.rgb * ao;
    vec3 color = ambient.rgb + Lo;

    color = color / (color + vec3(1.0));
    color = pow(color, vec3(1.0/2.2));

    if( depth > 0.99999)
    {
        return vec4(albedo.rgb,1.0);
    }
    else
    {
        return vec4(color,1.0);
    }
    //return vec4(color, 1.0) * (depth > 0.999999 ? 0 : 1 ) ;
}
//==============================================================================
// END: https://learnopengl.com/PBR/Lighting
//==============================================================================


void main()
{
    //out_Color = vec4(f_TEXCOORD_0.xy,1,1);
    out_Color = pbrMain();
}
