/**

This is a template of ridgid body pipeline with no lights

*/

#version 450
#extension GL_ARB_separate_shader_objects : enable


//============================================================
// Input Vertex Attributes
//
// All vertex attributes defined by the GLTF spec must be
// used as an input. The render system will bind
// an "undefined" buffer to any input attributes that
// do not exist in the mesh.
//============================================================
layout(location =  0)  in vec3  in_POSITION;
layout(location =  1)  in vec3  in_NORMAL;
layout(location =  2)  in vec3  in_TANGENT;
layout(location =  3)  in vec2  in_TEXCOORD_0;
layout(location =  4)  in vec2  in_TEXCOORD_1;
layout(location =  5)  in vec3  in_COLOR_0;
layout(location =  6)  in uvec4 in_JOINTS_0;
layout(location =  7)  in vec4  in_WEIGHTS_0;

//============================================================
//
//============================================================
layout(location = 0) out vec3 f_POSITION;
layout(location = 1) out vec3 f_NORMAL;
layout(location = 2) out vec2 f_TEXCOORD_0;
layout(location = 3) out vec3 f_COLOR_0;


//------------------------------------------------------------
//
//------------------------------------------------------------
layout(location = 4) flat out int f_materialIndex;  // used to tell the fragment shader what index to look at in the
                                                    // material storage buffer.



/**
This uniform is for the per-frame information

Set 0 binding 0 should always be the Global Per frame shader.

Note that alignment is extremely important, everything must
be aligned to multiples of 2 floats...
**/
layout(set=0, binding=0) uniform GLOBAL_UNIFORM_t
{
    float SCREEN_WIDTH;
    float SCREEN_HEIGHT;
    float MOUSE_X;
    float MOUSE_Y;
    float MOUSE_Z;
    float TIME_INT;
    float TIME_FRAC;
    float UNUSED1;
    vec3  MOUSE_NEAR;
    float UNUSED2;
    vec3  MOUSE_FAR;
    float UNUSED3;
    vec3  CAMERA_POSITION;
    float UNUSED4;
    mat4  VIEW;
    mat4  PROJECTION;
    mat4  PROJECTION_VIEW;

} GLOBAL_UNIFORM;





layout(set = 0, binding = 1) buffer STORAGE_MATRIX_t
{

    mat4 transform[];

} STORAGE_MATRIX;


// struct Material_t
// {
//   vec4      baseColorFactor;
//   float     metallicFactor;
//   float     roughnessFactor;
//   int       baseColorTexture;
//   int       metallicRoughnessTexture;
// };
// layout(set = 0, binding = 2) buffer STORAGE_MATERIAL_t
// {
//
//     Material_t material[];
//
// } STORAGE_MATERIAL;


/**
The push constant block is where all the vertex information
is stored for all 3 vertices in the triangle.

Total size of block: 112 bytes
mvp matrix = 64 bytes
vertex = 3 * ( 12 + 4 ) = 48

**/
layout(push_constant) uniform PushConsts
{
        int  matrixIndex;
        int  materialIndex;
        int  hasBones;
} pushConsts;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    int matrixIndex = pushConsts.matrixIndex;

    f_materialIndex = pushConsts.materialIndex;


    if(  pushConsts.hasBones == 1)
    {
        vec4 totalLocalPosition = vec4(0,0,0,0);
        vec3 totalNormal = vec3(0,0,0);

        mat4 modelMatrix = STORAGE_MATRIX.transform[   matrixIndex   ];

        // the weights dont always add up to 1
        vec4 weights = in_WEIGHTS_0 / (in_WEIGHTS_0.x + in_WEIGHTS_0.y + in_WEIGHTS_0.z + in_WEIGHTS_0.w);

        for(int i=0; i < 4; i++)
        {
            mat4 jointTransform = STORAGE_MATRIX.transform[ 1 + matrixIndex + in_JOINTS_0[i] ];
            vec4 posePosition  = jointTransform * vec4(in_POSITION, 1.0);

            vec3 worldNormal   =  (jointTransform * vec4(in_NORMAL, 0)).xyz;

            //vec3 worldNormal = mat3(inverse(transpose(modelMatrix * jointTransform))) * in_NORMAL;

            totalLocalPosition += posePosition * weights[i];
            totalNormal        += worldNormal  * weights[i];
        }

        vec4 worldPosition = modelMatrix * totalLocalPosition;

        f_POSITION = worldPosition.xyz;

        gl_Position   =  GLOBAL_UNIFORM.PROJECTION_VIEW  * worldPosition;
        gl_Position.y = -gl_Position.y;

        f_NORMAL     = ( modelMatrix * vec4(totalNormal,0) ).xyz;
    }
    else
    {
        mat4 modelMatrix = STORAGE_MATRIX.transform[   matrixIndex   ];

        f_COLOR_0 = vec3(1,1,1);

        vec4 worldPosition = modelMatrix * vec4(in_POSITION,1) ;

        f_POSITION = worldPosition.xyz;

        gl_Position   =  GLOBAL_UNIFORM.PROJECTION_VIEW  * worldPosition;
        gl_Position.y = -gl_Position.y;

        f_NORMAL      = ( modelMatrix * vec4(in_NORMAL, 0.0) ).xyz;
    }

    f_TEXCOORD_0 = in_TEXCOORD_0;

}
