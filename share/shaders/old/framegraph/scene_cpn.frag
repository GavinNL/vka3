#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 f_Position;
layout(location = 1) in vec3 f_Normal;
layout(location = 2) in vec2 f_TexCoord_0;
layout(location = 3) in vec3 f_TexCoord_1;

layout (location = 0) out vec4 out_ALBEDO;
layout (location = 1) out vec4 out_POSITION;
layout (location = 2) out vec4 out_NORMAL;

layout(set = 0, binding = 2) uniform sampler2D TEXTURE_0;


void main()
{
    out_POSITION = vec4(f_Position,1.0);
    out_NORMAL   = vec4(f_Normal, 1.0);
    out_ALBEDO   = vec4( texture(  TEXTURE_0, f_TexCoord_0.xy   ).rgb,1);
}
