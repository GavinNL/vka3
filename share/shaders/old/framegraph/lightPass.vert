/**
*
* This is a very simple shader for testing. It is a constant shader
* that always produces a coloured triangle. It doesn't require any
* vertex attributes, or uniform buffers.
*/

#version 450
#extension GL_ARB_separate_shader_objects : enable



//========================================================================
// Output attributes.
//========================================================================
layout(location = 0) out vec4 f_POSITION;
layout(location = 1) out vec2 f_TEXCOORD_0;


layout(push_constant) uniform PushConsts {
        vec2 position;
        vec2 size;
        vec2 screenSize;
        vec2 unused;
} pushConsts;

out gl_PerVertex
{
    vec4 gl_Position;
};

const vec2 MyArray[6]=vec2[6](
	    vec2( 0.0 , 0.0), // topleft
	    vec2( 0.0 , 1.0), // bottom left
	    vec2( 1.0 , 1.0),  // bottom right corner
        vec2( 0.0 , 0.0), // topleft
	    vec2( 1.0 , 1.0),  // bottom right corner
	    vec2( 1.0 , 0.0) // top right
    );

    const vec2 TexCoord[6]=vec2[6](
	    vec2(0,0),  // red
	    vec2(0,1),  // green
	    vec2(1,1),  // blue
        vec2(0,0),  // red
	    vec2(1,1),  // green
	    vec2(1,0)   // blue
    );

void main()
{
    vec2 p = MyArray[gl_VertexIndex] * pushConsts.size + pushConsts.position;
    gl_Position       = vec4( p.xy, 0.0, 1.0);
    f_POSITION        = gl_Position;
    f_TEXCOORD_0      = TexCoord[gl_VertexIndex];
}
