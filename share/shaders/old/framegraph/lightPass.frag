#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(set = 0, binding = 0) uniform sampler2D TEXTURE[4];

//========================================================================
// Input attributes.
//========================================================================
layout(location = 0) in vec4 f_POSITION;
layout(location = 1) in vec2 f_TEXCOORD_0;


//========================================================================
// Output Color
//========================================================================
layout(location = 0) out vec4 out_Color;


//========================================================================
// Push constants. This should be in all stages.
//========================================================================
layout(push_constant) uniform PushConsts {
        vec2 position;
        vec2 size;
        vec2 screenSize;
        vec2 unused;
} pushConsts;

vec4 albedoPresent()
{
    vec3 norm = texture( TEXTURE[0] , f_TEXCOORD_0.xy ) .rgb;

    return vec4(norm, 1.0).brga;
}

void main()
{
  //vec2 AA = vec2( GLOBAL_UNIFORM.MOUSE_X / GLOBAL_UNIFORM.SCREEN_WIDTH,
  //                GLOBAL_UNIFORM.MOUSE_Y / GLOBAL_UNIFORM.SCREEN_HEIGHT);
  out_Color = albedoPresent();
  //out_Color = vec4( f_TEXCOORD_0.xy,0,1);
}
