/**
*
* This is a very simple shader for testing. It is a constant shader
* that always produces a coloured triangle. It doesn't require any
* vertex attributes, or uniform buffers.
*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 in_Position;
layout(location = 1) in vec3 in_Normal;
layout(location = 2) in vec2 in_TexCoord_0;

layout(location = 0) out vec3 f_Position;
layout(location = 1) out vec3 f_Normal;
layout(location = 2) out vec2 f_TexCoord_0;
layout(location = 3) out vec3 f_TexCoord_1;

layout(set = 0, binding = 0) buffer STORAGE_MATRIX_t
{
    mat4 transform[];
} STORAGE_MATRIX;

struct RenderInfo_t
{
    int viewMatrixIndex;
    int projMatrixIndex;
    int worldMatrixIndex;
    int nodeMatrixIndex;
};

layout(set = 0, binding = 1) buffer RENDER_INFO_t
{
    RenderInfo_t info[];

} RENDER_INFO;

out gl_PerVertex
{
    vec4 gl_Position;
};

void main()
{
    int infoIndex = gl_InstanceIndex;

    int viewMatrixIndex  = RENDER_INFO.info[infoIndex].viewMatrixIndex;
    int projMatrixIndex  = RENDER_INFO.info[infoIndex].projMatrixIndex;
    int worldMatrixIndex = RENDER_INFO.info[infoIndex].worldMatrixIndex;
    int nodeMatrixIndex  = RENDER_INFO.info[infoIndex].nodeMatrixIndex;

    mat4 modelMatrx = STORAGE_MATRIX.transform[ worldMatrixIndex ] * STORAGE_MATRIX.transform[ nodeMatrixIndex ];

    f_Position   = ( modelMatrx * vec4( in_Position, 1.0)).xyz;
    f_Normal     = ( modelMatrx * vec4(in_Normal, 0.0) ).xyz;

    f_TexCoord_0 = in_TexCoord_0;

    f_TexCoord_1 = normalize(in_Position);

    gl_Position  =  STORAGE_MATRIX.transform[projMatrixIndex] * STORAGE_MATRIX.transform[viewMatrixIndex] * vec4( f_Position, 1.0);
}
