-- This Name must be unqiue. Only one script
-- of this particlar name can be attached to any one entity.
NAME = "playerInput"

--[[
The ENTITY table contains all information about the entity
and it's components,

For example, you can access the transform component by
ENTITY.TRANFORM:setPosition( )

The ENTITY table can also be used to store variables between
scripts. All scripts which are attached to the entity will
share the same ENTITY table.

--]]

local oldX=0
local oldY=0

LOOK_SENSITIVITY_H = 1
LOOK_SENSITIVITY_V = 1

FORWARD_SENSITIVITY = 1
SIDE_SENSITIVITY    = 1


function MouseMove(x, y)
    ENTITY.lookH = ENTITY.lookH - INPUT.MOUSE.XREL * LOOK_SENSITIVITY_H
    ENTITY.lookV = ENTITY.lookV + INPUT.MOUSE.YREL * LOOK_SENSITIVITY_V
end

--[[
The key callback function which will be used
to translate keypresses into Control input values,
eg: "move forward"
--]]
function Key(k , st)
    ENTITY.forward = 0
    ENTITY.side    = 0
    ENTITY.up      = 0
    ENTITY.slow    = 0

    if INPUT.KEY.LSHIFT then
        ENTITY.slow = 1
        return
    end

    if INPUT.KEY.w then
        ENTITY.forward = ENTITY.forward + 1
    end
    if INPUT.KEY.s then
        ENTITY.forward = ENTITY.forward - 1
    end

    ENTITY.forward = ENTITY.forward * FORWARD_SENSITIVITY
    ENTITY.side    = ENTITY.side * SIDE_SENSITIVITY

    if INPUT.KEY.a then
        ENTITY.side = ENTITY.side + 1
    end
    if INPUT.KEY.d then
        ENTITY.side = ENTITY.side - 1
    end

end

function MouseButton(b , st)
    ENTITY.lookEnabled = INPUT.MOUSE.RIGHT
--    print("hello from onButton  " .. b)
--    print(st)
--    ENTITY.look = st
end


--[[
The Start method is called when the
script is first loaded. You can use
this method to register any additional events
you want to call.
--]]
function Start()
    INPUT.XREL = 0
    INPUT.YREL = 0

    ENTITY.lookH = 0
    ENTITY.lookV = 0
    ENTITY.forward = 0
    ENTITY.side    = 0
    ENTITY.up      = 0
    ENTITY.slow    = 0
end


--[[
This function is called when the script has been
shutdown. Use this to unregister any methods
--]]
function Exit()


end
