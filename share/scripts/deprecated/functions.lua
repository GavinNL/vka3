-- find an entity named CameraEntity
ent = ENGINE.findEntity("CameraEntity")

entnew = ENGINE.createEntity("CameraEntity2")
ENGINE.CPosition.set(ent, v)
ENGINE.CRotation.set(ent, v)
ENGINE.CScale.set(ent, v)
ENGINE.CRender.set(ent,  ent)
ENGINE.CAnimation.setState(ent, 3)

-- ENGINE functions

-- finds the entity which has that name, returns -1
-- if the entity does not exist
ENGINE.findEntity("name")

-- Creates an entity with a name. returns -1 if a
-- entiy with that name already exists
ENGINE.createEntity("name")

-- Enables or disables the mouse
ENGINE.lockCursor(true)



-- CPosition functions

-- Sets the position of the entity
ENGINE.CPosition.set(entityID, vec3 )

-- gets the position of the entity as a vec3
ENGINE.CPosition.get(entityID




-- CRotation functions

-- Sets the rotation of the entity
ENGINE.CRotation.set(entityID, quat )

-- gets the rotation of the entity as a quat
ENGINE.CRotation.get(entityID)

-- Sets the rotation of the entity using euler angles
ENGINE.CRotation.setEuler(entityID, vec3)



-- CAnimation functions

-- checks whether the entity has an animation component
ENGINE.CAnimation.exists(entityID )

-- Changes the animation state
ENGINE.CAnimation.changeState(entityID, stateIndex, transitionLength)

-- returns the current animation state index
ENGINE.CAnimation.getCurrentState(entityID)

-- get the next animation state index
ENGINE.CAnimation.getNextState(entityID)




-- CScene functions

-- checks whether the entity has an CScene component
ENGINE.CScene.exists(entityID )

-- returns the node index with the given name,
-- returns -1 if no node can be found
ENGINE.CScene.findNode(entityID, nodeName)





-- CRender functions

-- checks whether the entity has an CRender component
ENGINE.CRender.exists(entityID )

-- Finds the node index with the given name
-- returns -1 if no node can be found
ENGINE.CRender.findNode(entityID, nodeName)


-- Sets the sceneEnity
ENGINE.CRender.setSceneEntity(entityID, newEntity)

-- Gets the sceneEntity
ENGINE.CRender.getSceneEntity(entityID)





-- CPose functions

-- checks whether the entity has an CPose component
ENGINE.CPose.exists(entityID )

-- Sets the transform of a parcular node index
-- using a transform:new()
ENGINE.CPose.setTransform(entityID, nodeIndex, transform)





-- CPhysics functions

-- checks whether the entity has an CPhysics component
ENGINE.CPhysics.exists(entityID )


ENGINE.CPose.getLinearVelocity(entityID)
ENGINE.CPose.getAngularVelocity(entityID)
ENGINE.CPose.setLinearVelocity(entityID , vec3)
ENGINE.CPose.setAngularVelocity(entityID, vec3)

ENGINE.CPose.setLinearFactor(entityID , vec3)
ENGINE.CPose.setAngularFactor(entityID, vec3)



-- The update() method is called every frame
function update()


end

function close(T)

   print("Mouse X  =  " .. MOUSE.X)

end
