-- there should not be a any global variables --

-- Code in teh global scope is only called once when the script is loaded.
-- use this to initalize anything you need.

sensitivity = 1

target_Name = "PlayerEntity"
target_ID = ENGINE.findEntity( target_Name )

R = 6

-- The delta arch relative
deltaEuler  = vec3:new(0,0,0)
cameraOffset= vec3:new(0,2,0)
targetOffset= vec3:new(0,0,0)

cameraPosition = vec3:new(0,0,0)


function update()

   if( not ENGINE.CPhysics.exists(ENTITY_ID)  ) then

      targetOrientation = ENGINE.CRotation.get(target_ID)
      targetPosition    = ENGINE.CPosition.get(target_ID) + targetOffset

      if( MOUSE.MIDDLE ) then
          deltaEuler.y = deltaEuler.y - MOUSE.DX*sensitivity*FRAME.deltaTime
      end

      deltaEuler.x = deltaEuler.x - MOUSE.DY*sensitivity*FRAME.deltaTime

      q = quat.fromEuler(deltaEuler)

       -- The initial camera position is behind the target (hence the -R)
       x = quat.rotateVector(q, vec3:new(0,0,-R))

       --
       desiredCameraPosition = quat.rotateVector( targetOrientation, x )


       -- the final camera position
       desiredCameraPosition = targetPosition + desiredCameraPosition + cameraOffset

       cameraPosition = vec3.mix(cameraPosition, desiredCameraPosition, 0.1)

       -- now that we ahve the camera's final position, we can force the
       -- camera entity to always look at the target
       orientation = quat.lookAt( targetPosition-cameraPosition, vec3:new(0,1,0))

       ENGINE.CPosition.set(ENTITY_ID, cameraPosition)
       ENGINE.CRotation.set(ENTITY_ID, orientation)
   else

   end


end

function close(T)

   print("Mouse X  =  " .. MOUSE.X)

end
