-- there should not be a any global variables --

-- Code in teh global scope is only called once when the script is loaded.
-- use this to initalize anything you need.
roll_pitch_yaw = vec3.new(0,0,0)
speed = vec3.new(0,0,0)

forwardSpeed = 10
sensitivity = 0.005

-- The update() method is called every frame
function update()

   if( not ENGINE.CPhysics.exists(ENTITY_ID)  ) then

      desired = vec3.new(0,0,0)

      if( MOUSE.RIGHT ) then
          desired.y = roll_pitch_yaw.y - MOUSE.DX*sensitivity
          desired.x = roll_pitch_yaw.x + MOUSE.DY*sensitivity

          roll_pitch_yaw = vec3.mix(roll_pitch_yaw, desired, 0.5)

          ENGINE.CRotation.setEuler(ENTITY_ID, roll_pitch_yaw)
      end
        --  print(roll_pitch_yaw.y)

       desiredSpeed = vec3.new(0,0,0)
       if( KEY.w ) then
          desiredSpeed.z = -forwardSpeed
       elseif( KEY.s ) then
         desiredSpeed.z =  forwardSpeed
       end
       if( KEY.a ) then
          desiredSpeed.x = -forwardSpeed
       elseif( KEY.d ) then
         desiredSpeed.x =  forwardSpeed
       end

       orientation = ENGINE.CRotation.get(ENTITY_ID)

       desiredSpeed = quat.rotateVector( orientation, desiredSpeed)

       speed = vec3.mix( speed, desiredSpeed, 0.05)

       currentPos = ENGINE.CPosition.get(ENTITY_ID)

       currentPos.x = currentPos.x + speed.x * FRAME.deltaTime
       currentPos.y = currentPos.y + speed.y * FRAME.deltaTime
       currentPos.z = currentPos.z + speed.z * FRAME.deltaTime

       ENGINE.CPosition.set(ENTITY_ID, currentPos)
   else

   end


end

function close(T)

   print("Mouse X  =  " .. MOUSE.X)

end
