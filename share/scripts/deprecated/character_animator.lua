
function init()
    idleState = AnimationStateInput:new(0)
    idleState:setRootNodeMotionMask( vec3:new(1,1,0)) -- Do not use z translation for the root node

    runningState = AnimationStateInput:new(4) --.newAnimationStateInput(2)
    runningState:setRootNodeMotionMask( vec3:new(1,1,0)) -- Do not use z translation for the root node

    in3 = AnimationVariable:new() --.newAnimationStateInput(2)

    idleToRunningMix = AnimationStateMix:new(idleState, runningState, in3)

    ENGINE.CAnimation.setAnimationFlow(ENTITY_ID, idleToRunningMix:toNode())

    T0=0

    print("Initialised!")
end


init()


function locomotionState()
    -- Increment the time for each of the AnimationStateInputs.
    -- This value is in normalized time!
    -- if incrementTime(1) is called, it is as if the
    -- animation looped once.
    idleState:incrementTime(FRAME.deltaTime)
    runningState:incrementTime(FRAME.deltaTime)

    T0 = T0 + MOUSE.WHEEL * 0.01

    speed = vec2:new(VARS.vel.x, VARS.vel.z)
    t = vec2.norm(speed)/7

    in3:setValue( t )
end

-- The update() method is called every frame
function update()

    locomotionState()

    if( KEY.RELEASED.F2)
    then
        -- Predefined variable. If set to true, the script will be reloaded
        -- during the next update.
        _RELOAD_SCRIPT=true
    end
end

function close()
   print("Mouse X  =  " .. MOUSE.X)
end
