-- Code in teh global scope is only called once when the script is loaded.
-- use this to initalize anything you need.
angfac = vec3.new()
angfac.y = 1
jumptimer = 0

if( ENGINE.CPhysics.exists(ENTITY_ID)  ) then
    ENGINE.CPhysics.setAngularFactor(ENTITY_ID, angfac )
end

mouseSensitivity = 120
forwardSpeed = 5

-- ENGINE.lockCursor(true)

lockCursor = true


-- The update() method is called every frame
function update()

   v = vec3:new(0,0,0)

   -- If the keys are down, change the velocity
   if( KEY.w) then
      v.z = v.z + forwardSpeed
   end
   if( KEY.a) then
      v.x = v.x + forwardSpeed
   end
   if( KEY.d) then
      v.x = v.x - forwardSpeed
   end

   currentVelocity = ENGINE.CPhysics.getLinearVelocity(ENTITY_ID)


   if( KEY.RELEASED.F1)
   then
       ENGINE.lockCursor(lockCursor)
       lockCursor = not lockCursor
   end


   -- Do the jumping action. Only jump if the jumptimer is greater than some
   -- value.
   if( KEY.SPACE and jumptimer > 6) then
      currentVelocity.y = 5
      jumptimer = 0

   end

   -- If we are not moving in the y direction, increase the jump timer
   if( currentVelocity.y < 0.001 and currentVelocity.y > -0.001) then
      jumptimer = jumptimer+1
      if( jumptimer > 6) then
         VARS.jumping=false
      end
   end

   currentOrientation    = ENGINE.CRotation.get(ENTITY_ID)

   desiredLinearVelocity = quat.rotateVector( currentOrientation, v )
   desiredVelocity       = vec3.mix( currentVelocity, desiredLinearVelocity, 0.1)
  -- desiredVelocity       = desiredLinearVelocity
   desiredVelocity.y     = currentVelocity.y
   ENGINE.CPhysics.setLinearVelocity(ENTITY_ID, desiredVelocity )

   desiredAngularVelocity   = vec3:new()
   desiredAngularVelocity.y = -MOUSE.DX * mouseSensitivity * FRAME.deltaTime

   currentAngularVelocity = ENGINE.CPhysics.getAngularVelocity(ENTITY_ID)

   --desiredAngularVelocity = vec3.mix( currentAngularVelocity, desiredAngularVelocity, 0.02)
   --desiredAngularVelocity = vec3.mix( currentAngularVelocity, desiredAngularVelocity, 0.02)

   if( MOUSE.MIDDLE ) then
       desiredAngularVelocity.y = 0
   end

   ENGINE.CPhysics.setAngularVelocity(ENTITY_ID, desiredAngularVelocity)

   VARS.vel = desiredVelocity
   VARS.jumping = true

end

function close(T)

   print("Mouse X  =  " .. MOUSE.X)

end
