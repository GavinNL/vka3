-- This Name must be unqiue. Only one script
-- of this particlar name can be attached to any one entity.
NAME = "playerController"

--[[
The ENTITY table contains all information about the entity
and it's components,

For example, you can access the transform component by
ENTITY.TRANFORM:setPosition( )

The ENTITY table can also be used to store variables between
scripts. All scripts which are attached to the entity will
share the same ENTITY table.

--]]

-- This is a Local script varaible
-- it can be modified from C++
DECAY_HALFLIFE = 0.03

FORWARD_ACCELERATION = 1
SIDE_ACCELERATION    = 1

local hSpeed = 0
local vSpeed = 0

local es = 0.01

local speed = vec3:new(0,0,0)

-- We will hold a reference to all
-- the components we will be using.
-- see Start() for initialization
local Transform = nil

--[[
This method will be called every frame once it is registered
in the Start( ) funciton
--]]
function Update()

    if ENTITY.lookH == nil then
        return
    end
    if( Transform == nil) then
        return
    end
    dt = FRAME.DELTA_TIME

    hSpeed = ENTITY.lookH * es;
    vSpeed = ENTITY.lookV * es;

    speed.x = speed.x + ENTITY.side    *  FORWARD_ACCELERATION*dt;
    speed.z = speed.z + ENTITY.forward *  SIDE_ACCELERATION*dt;

    decay_rate = math.exp( -dt * 0.30102999566 / DECAY_HALFLIFE)

    Transform:translateLocal(  speed )

    if ENTITY.lookEnabled == true then
        Transform:rotateGlobal( vec3:new(0,1,0), hSpeed )
        Transform:rotateLocal(  vec3:new(1,0,0), vSpeed )
    end

    ENTITY.lookH = 0
    ENTITY.lookV = 0

    speed.x = speed.x * decay_rate;
    speed.z = speed.z * decay_rate;

    hSpeed = hSpeed * decay_rate
    vSpeed = vSpeed * decay_rate

end


--[[
The Start function is called when the
script is first loaded. You can use
this method to register any additional events
you want to call.
--]]
function Start()

    if ENTITY.lookH == nil  then
        ENTITY.lookH = 0
    end
    if ENTITY.lookV == nil  then
        ENTITY.lookV = 0
    end

    -- We need to get a reference to the components
    -- we are planning on using
    if ENGINE.hasComponent(ENTITY.ID, "TRANSFORM") then
        Transform = ENGINE.getTransformComponent(ENTITY.ID)
    end

end

--[[
    Called win a componenet has been added to this entity
]]--
function ComponentAdded(component)
    if component == "TRANSFORM" then
        Transform = ENGINE.getTransformComponent(ENTITY.ID)
    end
end

--[[
    Called when a component has been removed from this entity
]]--

function ComponentRemoved(component)
    if component == "TRANSFORM" then
        Transform = nil
    end
end

--[[
This function is called when the script has been
shutdown.

Execute quit() anywhere in the script to tell the
scripting system to shutdown the script
--]]
function Exit()
    print("PlayerController Exit() called")
end
