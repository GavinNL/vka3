require 'Script'


--[[
Name:       playerController.lua

the playerController.lua script will read in the "control" variables which
were created by the playerInput.lua script (or some other script) and
modify the entity's transform component appropriately.

The script creates a basic FPS free-form camera control.

Hold the Right Mouse button to enable free looking and use the WSAD keys
to move the camera.

--]]
playerController = class('playerController', Script)

--[[
Name:       Start()
Required:   YES
Parameters: NONE

Called when the script is added to the entity. It is used to
initialize any local variables within the class instance.
Essentially this acts like the constructor.
--]]
function playerController:Start()

    self.hSpeed = 0
    self.vSpeed = 0

    -- This is a Local script varaible
    -- it can be modified from C++
    self.DECAY_HALFLIFE = 0.03

    self.FORWARD_ACCELERATION = 1
    self.SIDE_ACCELERATION    = 1

    self.es = 0.01

    self.speed = vec3:new(0,0,0)

    -- We will hold a reference to all
    -- the components we will be using.
    -- see Start() for initialization
    --self.Transform = nil
    self.Transform = nil

    if ENGINE.hasComponent(ENTITY.ID, "TRANSFORM") then
        self.Transform = ENGINE.getTransformComponent( ENTITY.ID )
    end


end


--[[
Name:       Update()
Required:   YES
Parameters: NONE


Called whenever there has been a change to the Entity. Either the components
have changed, or the script has requested an update. You should use this
method to make sure that the entity has all the components required
to run the script properly.

--]]
function playerController:Update()

    if ENGINE.hasComponent(ENTITY.ID, "TRANSFORM") then
        self.Transform = ENGINE.getTransformComponent( ENTITY.ID )
    else
        self.Transform = nil
    end

end


function playerController:Loop()

    if( self.Transform == nil) then
        return
    end

    if ENTITY.lookH == nil then
        return
    end

    dt = FRAME.DELTA_TIME

    self.hSpeed = ENTITY.lookH * self.es;
    self.vSpeed = ENTITY.lookV * self.es;

    scaleV = 1.0
    if ENTITY.slow==1 then
        scaleV = 0.2
    end

    self.speed.x = self.speed.x + ENTITY.side    *  scaleV*self.FORWARD_ACCELERATION*dt;
    self.speed.z = self.speed.z + ENTITY.forward *  scaleV*self.SIDE_ACCELERATION*dt;

    decay_rate = math.exp( -dt * 0.30102999566 / self.DECAY_HALFLIFE)

    self.Transform:translateLocal(  self.speed )

    if ENTITY.lookEnabled == true then
        self.Transform:rotateGlobal( vec3:new(0,1,0), self.hSpeed )
        self.Transform:rotateLocal(  vec3:new(1,0,0), self.vSpeed )
    end

    ENTITY.lookH = 0
    ENTITY.lookV = 0

    self.speed.x = self.speed.x * decay_rate;
    self.speed.z = self.speed.z * decay_rate;

    self.hSpeed = self.hSpeed * decay_rate
    self.vSpeed = self.vSpeed * decay_rate

end
--]]


--[[
Name:       Stop()
Required:   NO
Parameters: NONE

This is the desctructor. It is called when the script ends.

Note: DO NOT call self.Stop() anywhere within the class.

--]]
function playerController:Stop()

end
