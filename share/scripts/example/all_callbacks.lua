-- This Name must be unqiue. Only one script
-- of this particlar name can be attached to any one entity.
NAME = "PlayerInput"

--[[
The ENTITY table contains all information about the entity
and it's components, 

For example, you can access the transform component by
ENTITY.TRANFORM:setPosition( )

The ENTITY table can also be used to store variables between
scripts. All scripts which are attached to the entity will
share the same ENTITY table.

The List of all callback functions are listed after the comments

List of all variables:

FRAME.DELTA_TIME

ENTITY.ID

ENGINE.hasComponent( component_str) - component_str must be one of: "CAMERA", "RIGIDBODY", "TRANSFORM", "RENDER", "SCRIPT"


ENGINE.getTransformComponent(ENTITY.ID)
ENGINE.getRenderComponent(ENTITY.ID)
ENGINE.getRigidBodyComponent(ENTITY.ID)
ENGINE.getCameraComponent(ENTITY.ID)
ENGINE.getScriptComponent(ENTITY.ID)

INPUT.MOUSE.XREL
INPUT.MOUSE.YREL
INPUT.MOUSE.X
INPUT.MOUSE.Y

INPUT.BUTTON.LEFT
INPUT.BUTTON.RIGHT
INPUT.BUTTON.MIDDLE
INPUT.BUTTON.X1
INPUT.BUTTON.X2

INPUT.KEY.UNKNOWN
INPUT.KEY.RETURN
INPUT.KEY.ESCAPE
INPUT.KEY.BACKSPACE
INPUT.KEY.TAB
INPUT.KEY.SPACE
INPUT.KEY.EXCLAIM
INPUT.KEY.QUOTEDBL
INPUT.KEY.HASH
INPUT.KEY.PERCENT
INPUT.KEY.DOLLAR
INPUT.KEY.AMPERSAND
INPUT.KEY.QUOTE
INPUT.KEY.LEFTPAREN
INPUT.KEY.RIGHTPAREN
INPUT.KEY.ASTERISK
INPUT.KEY.PLUS
INPUT.KEY.COMMA
INPUT.KEY.MINUS
INPUT.KEY.PERIOD
INPUT.KEY.SLASH
INPUT.KEY.NUM_0
INPUT.KEY.NUM_1
INPUT.KEY.NUM_2
INPUT.KEY.NUM_3
INPUT.KEY.NUM_4
INPUT.KEY.NUM_5
INPUT.KEY.NUM_6
INPUT.KEY.NUM_7
INPUT.KEY.NUM_8
INPUT.KEY.NUM_9
INPUT.KEY.COLON
INPUT.KEY.SEMICOLON
INPUT.KEY.LESS
INPUT.KEY.EQUALS
INPUT.KEY.GREATER
INPUT.KEY.QUESTION
INPUT.KEY.AT
INPUT.KEY.LEFTBRACKET
INPUT.KEY.BACKSLASH
INPUT.KEY.RIGHTBRACKET
INPUT.KEY.CARET
INPUT.KEY.UNDERSCORE
INPUT.KEY.BACKQUOTE
INPUT.KEY.a
INPUT.KEY.b
INPUT.KEY.c
INPUT.KEY.d
INPUT.KEY.e
INPUT.KEY.f
INPUT.KEY.g
INPUT.KEY.h
INPUT.KEY.i
INPUT.KEY.j
INPUT.KEY.k
INPUT.KEY.l
INPUT.KEY.m
INPUT.KEY.n
INPUT.KEY.o
INPUT.KEY.p
INPUT.KEY.q
INPUT.KEY.r
INPUT.KEY.s
INPUT.KEY.t
INPUT.KEY.u
INPUT.KEY.v
INPUT.KEY.w
INPUT.KEY.x
INPUT.KEY.y
INPUT.KEY.z
INPUT.KEY.CAPSLOCK
INPUT.KEY.F1
INPUT.KEY.F2
INPUT.KEY.F3
INPUT.KEY.F4
INPUT.KEY.F5
INPUT.KEY.F6
INPUT.KEY.F7
INPUT.KEY.F8
INPUT.KEY.F9
INPUT.KEY.F10
INPUT.KEY.F11
INPUT.KEY.F12
INPUT.KEY.PRINTSCREEN
INPUT.KEY.SCROLLLOCK
INPUT.KEY.PAUSE
INPUT.KEY.INSERT
INPUT.KEY.HOME
INPUT.KEY.PAGEUP
INPUT.KEY.DELETE
INPUT.KEY.END
INPUT.KEY.PAGEDOWN
INPUT.KEY.RIGHT
INPUT.KEY.LEFT
INPUT.KEY.DOWN
INPUT.KEY.UP
INPUT.KEY.NUMLOCKCLEAR
INPUT.KEY.KP_DIVIDE
INPUT.KEY.KP_MULTIPLY
INPUT.KEY.KP_MINUS
INPUT.KEY.KP_PLUS
INPUT.KEY.KP_ENTER
INPUT.KEY.KP_1
INPUT.KEY.KP_2
INPUT.KEY.KP_3
INPUT.KEY.KP_4
INPUT.KEY.KP_5
INPUT.KEY.KP_6
INPUT.KEY.KP_7
INPUT.KEY.KP_8
INPUT.KEY.KP_9
INPUT.KEY.KP_0
INPUT.KEY.KP_PERIOD
INPUT.KEY.APPLICATION
INPUT.KEY.POWER
INPUT.KEY.KP_EQUALS
INPUT.KEY.F13
INPUT.KEY.F14
INPUT.KEY.F15
INPUT.KEY.F16
INPUT.KEY.F17
INPUT.KEY.F18
INPUT.KEY.F19
INPUT.KEY.F20
INPUT.KEY.F21
INPUT.KEY.F22
INPUT.KEY.F23
INPUT.KEY.F24
INPUT.KEY.EXECUTE
INPUT.KEY.HELP
INPUT.KEY.MENU
INPUT.KEY.SELECT
INPUT.KEY.STOP
INPUT.KEY.AGAIN
INPUT.KEY.UNDO
INPUT.KEY.CUT
INPUT.KEY.COPY
INPUT.KEY.PASTE
INPUT.KEY.FIND
INPUT.KEY.MUTE
INPUT.KEY.VOLUMEUP
INPUT.KEY.VOLUMEDOWN
INPUT.KEY.KP_COMMA
INPUT.KEY.KP_EQUALSAS400
INPUT.KEY.ALTERASE
INPUT.KEY.SYSREQ
INPUT.KEY.CANCEL
INPUT.KEY.CLEAR
INPUT.KEY.PRIOR
INPUT.KEY.RETURN2
INPUT.KEY.SEPARATOR
INPUT.KEY.OUT
INPUT.KEY.OPER
INPUT.KEY.CLEARAGAIN
INPUT.KEY.CRSEL
INPUT.KEY.EXSEL
INPUT.KEY.KP_00
INPUT.KEY.KP_000
INPUT.KEY.THOUSANDSSEPARATOR
INPUT.KEY.DECIMALSEPARATOR
INPUT.KEY.CURRENCYUNIT
INPUT.KEY.CURRENCYSUBUNIT
INPUT.KEY.KP_LEFTPAREN
INPUT.KEY.KP_RIGHTPAREN
INPUT.KEY.KP_LEFTBRACE
INPUT.KEY.KP_RIGHTBRACE
INPUT.KEY.KP_TAB
INPUT.KEY.KP_BACKSPACE
INPUT.KEY.KP_A
INPUT.KEY.KP_B
INPUT.KEY.KP_C
INPUT.KEY.KP_D
INPUT.KEY.KP_E
INPUT.KEY.KP_F
INPUT.KEY.KP_XOR
INPUT.KEY.KP_POWER
INPUT.KEY.KP_PERCENT
INPUT.KEY.KP_LESS
INPUT.KEY.KP_GREATER
INPUT.KEY.KP_AMPERSAND
INPUT.KEY.KP_DBLAMPERSAND
INPUT.KEY.KP_VERTICALBAR
INPUT.KEY.KP_DBLVERTICALBAR
INPUT.KEY.KP_COLON
INPUT.KEY.KP_HASH
INPUT.KEY.KP_SPACE
INPUT.KEY.KP_AT
INPUT.KEY.KP_EXCLAM
INPUT.KEY.KP_MEMSTORE
INPUT.KEY.KP_MEMRECALL
INPUT.KEY.KP_MEMCLEAR
INPUT.KEY.KP_MEMADD
INPUT.KEY.KP_MEMSUBTRACT
INPUT.KEY.KP_MEMMULTIPLY
INPUT.KEY.KP_MEMDIVIDE
INPUT.KEY.KP_PLUSMINUS
INPUT.KEY.KP_CLEAR
INPUT.KEY.KP_CLEARENTRY
INPUT.KEY.KP_BINARY
INPUT.KEY.KP_OCTAL
INPUT.KEY.KP_DECIMAL
INPUT.KEY.KP_HEXADECIMAL
INPUT.KEY.LCTRL
INPUT.KEY.LSHIFT
INPUT.KEY.LALT
INPUT.KEY.LGUI
INPUT.KEY.RCTRL
INPUT.KEY.RSHIFT
INPUT.KEY.RALT
INPUT.KEY.RGUI
INPUT.KEY.MODE
INPUT.KEY.AUDIONEXT
INPUT.KEY.AUDIOPREV
INPUT.KEY.AUDIOSTOP
INPUT.KEY.AUDIOPLAY
INPUT.KEY.AUDIOMUTE
INPUT.KEY.MEDIASELECT
INPUT.KEY.WWW
INPUT.KEY.MAIL
INPUT.KEY.CALCULATOR
INPUT.KEY.COMPUTER
INPUT.KEY.AC_SEARCH
INPUT.KEY.AC_HOME
INPUT.KEY.AC_BACK
INPUT.KEY.AC_FORWARD
INPUT.KEY.AC_STOP
INPUT.KEY.AC_REFRESH
INPUT.KEY.AC_BOOKMARKS
INPUT.KEY.BRIGHTNESSDOWN
INPUT.KEY.BRIGHTNESSUP
INPUT.KEY.DISPLAYSWITCH
INPUT.KEY.KBDILLUMTOGGLE
INPUT.KEY.KBDILLUMDOWN
INPUT.KEY.KBDILLUMUP
INPUT.KEY.EJECT
INPUT.KEY.SLEEP
INPUT.KEY.APP1
INPUT.KEY.APP2
INPUT.KEY.AUDIOREWIND
INPUT.KEY.AUDIOFASTFORWARD
--]]




--[[
(required) The Start method is called when the
script is first loaded. You can use
this method to register any additional events
you want to call.
--]]
function Start()

end


--[[
(required) This function is called when the script has been
shutdown. Use this to unregister any methods
--]]
function Exit()

end


--[[
(optional) Called once per frame
]]--
function Update()

end



--[[
(optional) Called whenever the mouse is moved.
]]--
function MouseMove(x , y)

end

--[[
(optional) Called whenever a key is pressed
]]--
function Key(keystring, state)

end

--[[
(optional) Called whenever mouse button is pressed
]]--
function MouseButton(buttonString, state)

end

--[[
(optional) Called whenever mouse wheel is changed
]]--
function MouseWheel(dy)

end

--[[
    Called win a componenet has been added to this entity
]]--
function ComponentAdded(component)

end

--[[
    Called when a component has been removed from this entity
]]--

function ComponentRemoved(component)

end



