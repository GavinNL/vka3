require 'Script'


--[[
Name:       cameraOrbitController.lua

The cameraOrbitController.lua script will read in the "control" variables which
were created by the playerInput.lua script (or some other script) and
modify the entity's transform component appropriately.

The script creates a basic FPS free-form camera control.

Hold the Right Mouse button to enable free looking and use the WSAD keys
to move the camera.

--]]
cameraOrbitController = class('cameraOrbitController', Script)

--[[
Name:       Start()
Required:   YES
Parameters: NONE

Called when the script is added to the entity. It is used to
initialize any local variables within the class instance.
Essentially this acts like the constructor.
--]]
function cameraOrbitController:Start()
    self.myTransform = Transform:new()
    self.myTarget    = vec3:new(0,0,0)

    self.hSpeed = 0
    self.vSpeed = 0

    -- This is a Local script variable
    -- it can be modified from C++
    self.DECAY_HALFLIFE = 0.03

    self.FORWARD_ACCELERATION = 1
    self.SIDE_ACCELERATION    = 1

    self.es = 0.01

    self.speed = vec3:new(0,0,0)

    if self.targetEntity ~= nil then
      self.targetEntityId = ENGINE.findEntityByName(self.targetEntity)
      if self.targetEntityId ~= nil  then
          self.targetTransform = ENGINE.getTransformComponent( self.targetEntityId )
      end
    end

    -- We will hold a reference to all
    -- the components we will be using.
    -- see Start() for initialization
    -- self.TransformComponent = nil
    self.TransformComponent = nil

    if self.targetOffset == nil then
        self.targetOffset = vec3:new(0,0,0)
    end

    if ENGINE.hasComponent(ENTITY.ID, "TRANSFORM") then
        self.TransformComponent = ENGINE.getTransformComponent( ENTITY.ID )
    end


end


--[[
Name:       Update()
Required:   YES
Parameters: NONE


Called whenever there has been a change to the Entity. Either the components
have changed, or the script has requested an update. You should use this
method to make sure that the entity has all the components required
to run the script properly.

--]]
function cameraOrbitController:Update()

    if ENGINE.hasComponent(ENTITY.ID, "TRANSFORM") then
        self.TransformComponent   = ENGINE.getTransformComponent( ENTITY.ID )
        self.myTransform.position = self.TransformComponent:getPosition()
        self.myTransform.rotation = self.TransformComponent:getRotation()
    else
        self.TransformComponent = nil
    end

end


function cameraOrbitController:Loop()

    if( self.TransformComponent == nil) then
        return
    end

    if ENTITY.lookH == nil then
        return
    end

    if self.targetTransform ~= nil then
        self.myTarget = self.targetTransform:getPosition()
    end

    dt = FRAME.DELTA_TIME

    self.hSpeed = ENTITY.lookH * self.es;
    self.vSpeed = ENTITY.lookV * self.es;

    decay_rate = math.exp( -dt * 0.30102999566 / self.DECAY_HALFLIFE)


    if ENTITY.lookEnabled == true then
        self.myTransform:rotateGlobal( vec3:new(0,1,0), self.hSpeed )
        self.myTransform:rotateLocal(  vec3:new(1,0,0), self.vSpeed )
    end

    T = Transform:new()
    T.position = self.targetOffset + self.myTarget + self.myTransform:forward() * 5
    T:lookAt( self.myTarget, vec3:new(0,1,0)  )

    --self.myTransform.position = self.myTarget;
    --self.myTransform:translateLocal( vec3:new(0,0,-5) )

    -- Set the actual transform for the entity.
    self.TransformComponent:set(  T.position, T.rotation )

    ENTITY.lookH = 0
    ENTITY.lookV = 0


end
--]]


--[[
Name:       Stop()
Required:   NO
Parameters: NONE

This is the desctructor. It is called when the script ends.

Note: DO NOT call self.Stop() anywhere within the class.

--]]
function cameraOrbitController:Stop()

end
