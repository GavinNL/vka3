require 'Script'

--[[
Name:       OneShot.lua

This script reads the Keyboard/Mouse input events and translates them
into "control" variables, such as "move forward" or "move back", etc.

The "control" variables are stored in the ENTITY table which can be read
by other scripts to actually perform the movement.

--]]
OneShot = class('OneShot', Script)

--[[
Name:       Start()
Required:   YES
Parameters: NONE

Called when the script is added to the entity. It is used to
initialize any local variables within the class instance.
Essentially this acts like the constructor.
--]]
function OneShot:Start()

    -- create a new entity
    newEntityId     = ENGINE.createEntity()

    -- get a reference to the entity so we can create
    -- components for it.
    newEntityObject = ENGINE.getEntityObject(newEntityId)

    -- create the render component and return a reference
    RRef = newEntityObject:createRenderComponent()
    RRef:setResource("rc:/gizmo")

    -- create the transform component and return a reference
    TRef = newEntityObject:createTransformComponent()
    TRef:setPosition( vec3:new(0,3,0) )

    -- tell the system we want to destroy the
    -- entity that is running this script.
    -- when it is destroyed the Stop() method will
    -- be called
    ENGINE.scheduleDestroy(entityId)
end


--[[
Name:       Stop()
Required:   NO
Parameters: NONE

This is the desctructor. It is called when the script ends.

Note: DO NOT call self.Stop() anywhere within the class.

--]]
function OneShot:Stop()
    print("OneShot script Stopped")
end
