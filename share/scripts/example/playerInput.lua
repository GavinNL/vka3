require 'Script'

--[[
Name:       playerInput.lua

This script reads the Keyboard/Mouse input events and translates them
into "control" variables, such as "move forward" or "move back", etc.

The "control" variables are stored in the ENTITY table which can be read
by other scripts to actually perform the movement.

--]]
playerInput = class('playerInput', Script)

--[[
Name:       Start()
Required:   YES
Parameters: NONE

Called when the script is added to the entity. It is used to
initialize any local variables within the class instance.
Essentially this acts like the constructor.
--]]
function playerInput:Start()

    -- Any variables stored in the ENTITY. table will be
    -- available to other scripts running on the same entity.
    -- we can use this to pass information from the playerInput
    -- to the characterController.
    INPUT.XREL = 0
    INPUT.YREL = 0

    ENTITY.lookH   = 0
    ENTITY.lookV   = 0
    ENTITY.forward = 0
    ENTITY.side    = 0
    ENTITY.up      = 0
    ENTITY.slow    = 0

    self.LOOK_SENSITIVITY_H  = 1
    self.LOOK_SENSITIVITY_V  = 1
    self.FORWARD_SENSITIVITY = 1
    self.SIDE_SENSITIVITY    = 1

end


--[[
Name:       Stop()
Required:   NO
Parameters: NONE

This is the desctructor. It is called when the script ends.

Note: DO NOT call self.Stop() anywhere within the class.

--]]
function playerInput:Stop()

end


--[[
Name:       MouseMove()
Required:   NO
Parameters: x(float), y(float)

If defined, this method is called whenever the mouse moves on the screen.
the values of x and y are mouse coordinates. You can also
obtain the mouse coordinates from:

 INPUT.MOUSE.X, INPUT.MOUSE.Y, INPUT.MOUSE.YREL, INPUT.MOUSE.YREL

--]]
function playerInput:MouseMotion()
    -- translate the mouse's X/Y relative speeds into horizontal and vertical
    -- look directions
    ENTITY.lookH = ENTITY.lookH - INPUT.MOUSE.XREL * self.LOOK_SENSITIVITY_H
    ENTITY.lookV = ENTITY.lookV + INPUT.MOUSE.YREL * self.LOOK_SENSITIVITY_V
end

--[[

Name:       Key()
Required:   NO
Parameters: k(string), st (bool)

This method is called whenever a key is pressed on the keyboard

 The value of k is a string representing the key, and st is the state of the
 key as a boolean. 1 for down, 0 for up.

 THe key state can also be obtained from INPUT.KEY.KEYNAME

--]]
function playerInput:Key()
    ENTITY.forward = 0
    ENTITY.side    = 0
    ENTITY.up      = 0
    ENTITY.slow    = 0

    if INPUT.KEY.LSHIFT then
        ENTITY.slow = 1
    end

    if INPUT.KEY.w then
        ENTITY.forward = ENTITY.forward + 1
    end
    if INPUT.KEY.s then
        ENTITY.forward = ENTITY.forward - 1
    end

    if INPUT.KEY.a then
        ENTITY.side = ENTITY.side + 1
    end
    if INPUT.KEY.d then
        ENTITY.side = ENTITY.side - 1
    end

    ENTITY.forward = ENTITY.forward * self.FORWARD_SENSITIVITY
    ENTITY.side    = ENTITY.side    * self.SIDE_SENSITIVITY

end

--[[

Name:       MouseButton()
Required:   NO
Parameters: b (string), st(boolean)

This method is called whenever a mouse button is pressed

--]]
function playerInput:MouseButton()
    ENTITY.lookEnabled = INPUT.MOUSE.RIGHT
end
