require 'Script'


--[[
Name:       cameraMoveController.lua

the cameraMoveController.lua script will read in the "control" variables which
were created by the playerInput.lua script (or some other script) and
modify the entity's Position component appropriately.

The script creates a basic FPS free-form camera control.

Use the WSAD keys to move the camera

--]]
cameraMoveController = class('cameraMoveController', Script)

--[[
Name:       Start()
Required:   YES
Parameters: NONE

Called when the script is added to the entity. It is used to
initialize any local variables within the class instance.
Essentially this acts like the constructor.
--]]
function cameraMoveController:Start()

    -- This is a Local script varaible
    -- it can be modified from C++
    self.DECAY_HALFLIFE = 0.03

    self.FORWARD_ACCELERATION = 1
    self.SIDE_ACCELERATION    = 1

    self.es = 0.01

    self.speed = vec3:new(0,0,0)

end


--[[
Name:       Update()
Required:   YES
Parameters: NONE


Called whenever there has been a change to the Entity. Either the components
have changed, or the script has requested an update. You should use this
method to make sure that the entity has all the components required
to run the script properly.

--]]
function cameraMoveController:Update()

    if ENGINE.hasComponent(ENTITY.ID, "POSITION") then
        self.Position = ENGINE.getTransformComponent( ENTITY.ID )
    else
        self.Position = nil
    end

end


function cameraMoveController:Loop()

    dt = FRAME.DELTA_TIME

    scaleV = 1.0
    if ENTITY.slow==1 then
        scaleV = 0.2
    end

    self.speed.x = self.speed.x + ENTITY.side    *  scaleV*self.FORWARD_ACCELERATION*dt;
    self.speed.z = self.speed.z + ENTITY.forward *  scaleV*self.SIDE_ACCELERATION*dt;

    direction = quat:new(1,0,0,0)

    if ENGINE.hasComponent(ENTITY.ID, "ROTATION") then

        rotC = ENGINE.getRotationComponent( ENTITY.ID )
        direction = rotC:getRotation()
    end

    if ENGINE.hasComponent(ENTITY.ID, "POSITION") then
        posC = ENGINE.getPositionComponent( ENTITY.ID )
        posC:translate( direction * self.speed )
    end

    decay_rate = math.exp( -dt * 0.30102999566 / self.DECAY_HALFLIFE)

    self.speed.x = self.speed.x * decay_rate;
    self.speed.z = self.speed.z * decay_rate;

end
--]]


--[[
Name:       Stop()
Required:   NO
Parameters: NONE

This is the desctructor. It is called when the script ends.

Note: DO NOT call self.Stop() anywhere within the class.

--]]
function cameraMoveController:Stop()

end
