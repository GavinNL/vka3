require 'Script'

--[[
Name:       OneShot.lua

This script reads the Keyboard/Mouse input events and translates them
into "control" variables, such as "move forward" or "move back", etc.

The "control" variables are stored in the ENTITY table which can be read
by other scripts to actually perform the movement.

--]]
OneShot2 = class('OneShot2', Script)

--[[
Name:       Start()
Required:   YES
Parameters: NONE

Called when the script is added to the entity. It is used to
initialize any local variables within the class instance.
Essentially this acts like the constructor.
--]]
function OneShot2:Start()

    print("OneShot2!")

    -- tell the system we want to destroy the
    -- entity that is running this script.
    -- when it is destroyed the Stop() method will
    -- be called
-- ENGINE.scheduleDestroy(entityId)
end

function OneShot2:MouseMotion()

    --print("MouseMotion: " .. INPUT.MOUSE.X)

end

--[[
Name:       Stop()
Required:   NO
Parameters: NONE

This is the desctructor. It is called when the script ends.

Note: DO NOT call self.Stop() anywhere within the class.

--]]
function OneShot2:Stop()
    print("OneShot2 script Stopped")
end
