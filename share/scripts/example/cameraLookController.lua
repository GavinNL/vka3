require 'Script'


--[[
Name:       cameraLookController.lua

the cameraLookController.lua script will read in the "control" variables which
were created by the playerInput.lua script (or some other script) and
modify the entity's rotation component appropriately.

The script creates a basic FPS free-form camera control. It does not
provide any translation movement.

Hold the Right Mouse button to enable free looking 

--]]
cameraLookController = class('cameraLookController', Script)

--[[
Name:       Start()
Required:   YES
Parameters: NONE

Called when the script is added to the entity. It is used to
initialize any local variables within the class instance.
Essentially this acts like the constructor.
--]]
function cameraLookController:Start()

    self.hSpeed = 0
    self.vSpeed = 0

    self.es = 0.01

    -- We will hold a reference to all
    -- the components we will be using.
    -- see Start() for initialization
    --self.Rotation = nil
    self.Rotation = nil

    if ENGINE.hasComponent(ENTITY.ID, "ROTATION") then
        self.Rotation = ENGINE.getRotationComponent( ENTITY.ID )
    end

end


--[[
Name:       Update()
Required:   YES
Parameters: NONE


Called whenever there has been a change to the Entity. Either the components
have changed, or the script has requested an update. You should use this
method to make sure that the entity has all the components required
to run the script properly.

--]]
function cameraLookController:Update()

    if ENGINE.hasComponent(ENTITY.ID, "ROTATION") then
        self.Rotation = ENGINE.getRotationComponent( ENTITY.ID )
    else
        self.Rotation = nil
    end

end


function cameraLookController:Loop()

    if( self.Rotation == nil) then
        return
    end

    if ENTITY.lookH == nil then
        return
    end

    dt = FRAME.DELTA_TIME

    self.hSpeed = ENTITY.lookH * self.es;
    self.vSpeed = ENTITY.lookV * self.es;

    if ENTITY.lookEnabled == true then
        self.Rotation:rotateGlobal( vec3:new(0,1,0), self.hSpeed )
        self.Rotation:rotateLocal(  vec3:new(1,0,0), self.vSpeed )
    end

    ENTITY.lookH = 0
    ENTITY.lookV = 0

end
--]]


--[[
Name:       Stop()
Required:   NO
Parameters: NONE

This is the desctructor. It is called when the script ends.

Note: DO NOT call self.Stop() anywhere within the class.

--]]
function cameraLookController:Stop()

end
