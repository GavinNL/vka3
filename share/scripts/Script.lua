class = require 'middleclass'
Script = class('Script')

function Script:initialize()

    self._isScript=true
    self._isStopped=false
    self._isStarted=false

end

function Script:Stop()
    self._isStopped = true
end
