#include <vka/HostMeshPrimitive.h>
#include <vka/Primatives.h>

#include "catch.hpp"


SCENARIO("HostMeshPrimitiveAttribute")
{
    vka::HostMeshPrimitiveAttribute A;

    struct TestCase_t
    {
        vka::AccessorType  a;
        vka::ComponentType c;
        vk::Format          f;
        size_t              s;
    };

    TestCase_t TestCases[] = {

        {vka::AccessorType::eVec1, vka::ComponentType::eFloat, vk::Format::eR32Sfloat         , 4},
        {vka::AccessorType::eVec2, vka::ComponentType::eFloat, vk::Format::eR32G32Sfloat      , 8},
        {vka::AccessorType::eVec3, vka::ComponentType::eFloat, vk::Format::eR32G32B32Sfloat   , 12},
        {vka::AccessorType::eVec4, vka::ComponentType::eFloat, vk::Format::eR32G32B32A32Sfloat, 16},

        {vka::AccessorType::eVec1, vka::ComponentType::eInt, vk::Format::eR32Sint         , 4},
        {vka::AccessorType::eVec2, vka::ComponentType::eInt, vk::Format::eR32G32Sint      , 8},
        {vka::AccessorType::eVec3, vka::ComponentType::eInt, vk::Format::eR32G32B32Sint   , 12},
        {vka::AccessorType::eVec4, vka::ComponentType::eInt, vk::Format::eR32G32B32A32Sint, 16},

        {vka::AccessorType::eVec1, vka::ComponentType::eUnsignedInt, vk::Format::eR32Uint         , 4},
        {vka::AccessorType::eVec2, vka::ComponentType::eUnsignedInt, vk::Format::eR32G32Uint      , 8},
        {vka::AccessorType::eVec3, vka::ComponentType::eUnsignedInt, vk::Format::eR32G32B32Uint   , 12},
        {vka::AccessorType::eVec4, vka::ComponentType::eUnsignedInt, vk::Format::eR32G32B32A32Uint, 16},

        {vka::AccessorType::eVec1, vka::ComponentType::eUnsignedShort, vk::Format::eR16Uint         , 2},
        {vka::AccessorType::eVec2, vka::ComponentType::eUnsignedShort, vk::Format::eR16G16Uint      , 4},
        {vka::AccessorType::eVec3, vka::ComponentType::eUnsignedShort, vk::Format::eR16G16B16Uint   , 6},
        {vka::AccessorType::eVec4, vka::ComponentType::eUnsignedShort, vk::Format::eR16G16B16A16Uint, 8},

        {vka::AccessorType::eVec1, vka::ComponentType::eShort, vk::Format::eR16Sint         , 2},
        {vka::AccessorType::eVec2, vka::ComponentType::eShort, vk::Format::eR16G16Sint      , 4},
        {vka::AccessorType::eVec3, vka::ComponentType::eShort, vk::Format::eR16G16B16Sint   , 6},
        {vka::AccessorType::eVec4, vka::ComponentType::eShort, vk::Format::eR16G16B16A16Sint, 8},

        {vka::AccessorType::eVec1, vka::ComponentType::eByte, vk::Format::eR8Sint      , 1},
        {vka::AccessorType::eVec2, vka::ComponentType::eByte, vk::Format::eR8G8Sint    , 2},
        {vka::AccessorType::eVec3, vka::ComponentType::eByte, vk::Format::eR8G8B8Sint  , 3},
        {vka::AccessorType::eVec4, vka::ComponentType::eByte, vk::Format::eR8G8B8A8Sint, 4},

        {vka::AccessorType::eVec1, vka::ComponentType::eUnsignedByte, vk::Format::eR8Uint      , 1},
        {vka::AccessorType::eVec2, vka::ComponentType::eUnsignedByte, vk::Format::eR8G8Uint    , 2},
        {vka::AccessorType::eVec3, vka::ComponentType::eUnsignedByte, vk::Format::eR8G8B8Uint  , 3},
        {vka::AccessorType::eVec4, vka::ComponentType::eUnsignedByte, vk::Format::eR8G8B8A8Uint, 4}
    };

    for(auto & TC : TestCases)
    {
        A.init( TC.a, TC.c);

        REQUIRE( A.attributeSize() == TC.s);
        REQUIRE( A.format()        == TC.f);
    }

}

SCENARIO("Merge HostMeshPrimitiveAttribute")
{
    vka::HostMeshPrimitiveAttribute A1;

    A1.init( vka::AccessorType::eVec3, vka::ComponentType::eFloat);
    A1.allocate( 100 );

    REQUIRE( A1.count() == 100 );
    REQUIRE( A1.byteSize()  == 100 * 3 * sizeof(float));


    vka::HostMeshPrimitiveAttribute A2;

    A2.init( vka::AccessorType::eVec3, vka::ComponentType::eFloat);
    A2.allocate( 100 );

    REQUIRE( A2.count() == 100 );
    REQUIRE( A2.byteSize()  == 100 * 3 * sizeof(float));



    vka::HostMeshPrimitiveAttribute A3;

    A3.init( vka::AccessorType::eVec2, vka::ComponentType::eFloat);
    A3.allocate( 100 );

    REQUIRE( A3.count() == 100 );
    REQUIRE( A3.byteSize()  == 100 * 2 * sizeof(float));


    WHEN("We merge A1 and A2")
    {
        auto indexOfA2 = A1.append( A2 );

        THEN(" A1 now has more vertex attributes")
        {
            REQUIRE( A1.count() == 200);
            REQUIRE( indexOfA2  == 100 );
        }
    }

    WHEN("We merge A1 and A3")
    {
        REQUIRE_THROWS(A1.append(A3));
    }
}


SCENARIO("Merge HostMeshPrimitive")
{


    GIVEN("Two exact same mesh primitives")
    {
        vka::HostMeshPrimitive M1 = vka::boxPrimitive(1,1,1);
        vka::HostMeshPrimitive M2 = vka::boxPrimitive(1,1,1);

        auto ICOUNT = M1.indexCount();
        auto VCOUNT = M1.vertexCount();

        THEN("THey are similar since they have the same attributes and formats")
        {
            REQUIRE( M1.isSimilar(M2) == true );

            REQUIRE(M1.drawCalls.size() == 1);
            REQUIRE(M2.drawCalls.size() == 1);


            REQUIRE( M1.drawCalls[0].indexCount  == 36);
            REQUIRE( M1.drawCalls[0].vertexCount == 36);
            REQUIRE( M1.drawCalls[0].vertexOffset == 0);
            REQUIRE( M1.drawCalls[0].firstIndex == 0);

            REQUIRE( M2.drawCalls[0].indexCount  == 36);
            REQUIRE( M2.drawCalls[0].vertexCount == 36);
            REQUIRE( M2.drawCalls[0].vertexOffset == 0);
            REQUIRE( M2.drawCalls[0].firstIndex == 0);

            WHEN("We merge the two")
            {
                auto c = M1.append(M2);

                REQUIRE( c == ICOUNT );

                REQUIRE( M1.indexCount()  == ICOUNT*2);
                REQUIRE( M1.vertexCount() == VCOUNT*2);

                REQUIRE(M1.drawCalls.size() == 2);

                REQUIRE( M1.drawCalls[1].indexCount  == 36);
                REQUIRE( M1.drawCalls[1].vertexCount == 36);
                REQUIRE( M1.drawCalls[1].vertexOffset == 36);
                REQUIRE( M1.drawCalls[1].firstIndex == 72);

            }
        }
    }
}


SCENARIO("mesh")
{
    vka::HostMeshPrimitive M = vka::boxPrimitive(1,1,1);


    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::POSITION   ).count() > 0 );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::NORMAL	 ).count() > 0 );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::TEXCOORD_0 ).count() > 0 );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::TANGENT	 ).count() == 0 );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::TEXCOORD_1 ).count() == 0 );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::COLOR_0	 ).count() == 0 );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::JOINTS_0   ).count() == 0 );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::WEIGHTS_0  ).count() == 0 );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::POSITION   ).format()  == vk::Format::eR32G32B32Sfloat);
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::NORMAL	 ).format()  == vk::Format::eR32G32B32Sfloat);
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::TEXCOORD_0 ).format()  == vk::Format::eR32G32Sfloat);
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::TANGENT	 ).format()  == vk::Format::eUndefined);
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::TEXCOORD_1 ).format()  == vk::Format::eUndefined);
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::COLOR_0	 ).format()  == vk::Format::eUndefined);
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::JOINTS_0   ).format()  == vk::Format::eUndefined);
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::WEIGHTS_0  ).format()  == vk::Format::eUndefined);


    auto c = M.getAttribute( vka::PrimitiveAttribute::POSITION  ).count();
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::POSITION  ).count() == c );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::NORMAL	).count() == c );
    REQUIRE( M.getAttribute( vka::PrimitiveAttribute::TEXCOORD_0).count() == c );


}




SCENARIO("Test HostMeshPrimitive::get( )")
{

    GIVEN("An attribute of type vec3:unsigned int")
    {
        using _t  = uint32_t;
        auto comp = vka::ComponentType::eUnsignedInt;
        auto typ  = vka::AccessorType::eVec3;

        vka::HostMeshPrimitiveAttribute A1;

        A1.init( typ, comp);

        REQUIRE( A1.type            == typ);
        //REQUIRE( A1.attributeSize() == 12 );
        REQUIRE( vka::componentSize(A1.componentType) == sizeof(_t));

        A1.push_back( std::array<_t,3>{1,2,3}  );
        A1.push_back( std::array<_t,3>{4,5,6}  );

        REQUIRE( A1.get(0,0) == Approx(1) );
        REQUIRE( A1.get(0,1) == Approx(2) );
        REQUIRE( A1.get(0,2) == Approx(3) );

        REQUIRE( A1.get(1,0) == Approx(4) );
        REQUIRE( A1.get(1,1) == Approx(5) );
        REQUIRE( A1.get(1,2) == Approx(6) );

    }
    GIVEN("An attribute of type vec3:unsigned int")
    {
        using _t  = float;
        auto comp = vka::ComponentType::eFloat;
        auto typ  = vka::AccessorType::eVec3;

        vka::HostMeshPrimitiveAttribute A1;

        A1.init( typ, comp);

        REQUIRE( A1.type            == typ);
        //REQUIRE( A1.attributeSize() == 12 );
        REQUIRE( vka::componentSize(A1.componentType) == sizeof(_t));

        A1.push_back( std::array<_t,3>{1,2,3}  );
        A1.push_back( std::array<_t,3>{4,5,6}  );

        REQUIRE( A1.get(0,0) == Approx(1) );
        REQUIRE( A1.get(0,1) == Approx(2) );
        REQUIRE( A1.get(0,2) == Approx(3) );

        REQUIRE( A1.get(1,0) == Approx(4) );
        REQUIRE( A1.get(1,1) == Approx(5) );
        REQUIRE( A1.get(1,2) == Approx(6) );

    }
    GIVEN("An attribute of type vec3:unsigned int")
    {
        using _t  = int32_t;
        auto comp = vka::ComponentType::eInt;
        auto typ  = vka::AccessorType::eVec3;

        vka::HostMeshPrimitiveAttribute A1;

        A1.init( typ, comp);

        REQUIRE( A1.type            == typ);
        //REQUIRE( A1.attributeSize() == 12 );
        REQUIRE( vka::componentSize(A1.componentType) == sizeof(_t));

        A1.push_back( std::array<_t,3>{1,2,3}  );
        A1.push_back( std::array<_t,3>{4,5,6}  );

        REQUIRE( A1.get(0,0) == Approx(1) );
        REQUIRE( A1.get(0,1) == Approx(2) );
        REQUIRE( A1.get(0,2) == Approx(3) );

        REQUIRE( A1.get(1,0) == Approx(4) );
        REQUIRE( A1.get(1,1) == Approx(5) );
        REQUIRE( A1.get(1,2) == Approx(6) );

    }
}


