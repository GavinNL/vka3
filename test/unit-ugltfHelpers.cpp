#include <vka/uGLTF_Helpers.h>

#include "catch.hpp"

#include <fstream>

#include <mgo/SceneComponent.h>



/**
 * @brief convertUGLTFMesh
 * @param M
 * @param mergedPrimitives
 * @return
 *
 * Takes the mesh M and tries to merge it with one of the meshes in mergedPrimitives
 * it returns the primitive draw information.
 */
mgo::Primitive mergeUGLTFPrimitive( uGLTF::Primitive const & P, std::vector<vka::HostMeshPrimitive> & mergedPrimitives)
{
    // loop through all the primitives and check if they are
    // all similar. If they are, concatenate them into a single
    // mesh primitive;

    auto hostPrimitive = vka::convertUGLTFPrimitive(P);

    assert(hostPrimitive.drawCalls.size() > 0);

    // look through all the mergedPrimitives and see
    // which one it can be merged in
    for(auto & mp : mergedPrimitives)
    {
        if(mp.isSimilar(hostPrimitive) )
        {
            mp.append(hostPrimitive);

            mgo::Primitive outP;
            outP.primitiveIndex        = 0;
            outP.materialIndex         = P.material;
            outP.drawInfo              = mp.drawCalls.back();

            return outP;
        }
    }

    mergedPrimitives.push_back(hostPrimitive);

    mgo::Primitive outP;
    outP.primitiveIndex        = 0;
    outP.materialIndex         = P.material;
    outP.drawInfo.firstIndex   = 0;
    outP.drawInfo.indexCount   = hostPrimitive.indexCount();
    outP.drawInfo.vertexOffset = 0;
    outP.drawInfo.vertexCount  = hostPrimitive.vertexCount();

    return outP;
}



SCENARIO("HostMeshPrimitiveAttribute")
{
    std::string path = "/home/globo/Other_Projects/vka2/share/vka2/models/BoomBox.glb";

    uGLTF::GLTFModel M;

    std::ifstream in( path );

    M.load(in);

    auto HM = vka::convertUGLTFPrimitive( M.meshes.at(0).primitives.at(0));

    REQUIRE( HM.hasAttribute( vka::PrimitiveAttribute::POSITION ) );
    REQUIRE( HM.hasAttribute( vka::PrimitiveAttribute::NORMAL ) );



    auto I = vka::convertUGLTFImage( M.images.at(0));

    REQUIRE( I.width() > 0 );
    REQUIRE( I.height() > 0 );
}




