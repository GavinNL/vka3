#include <vka/uGLTF_Helpers.h>

#include "catch.hpp"

#include <mgo/common_functions.h>

SCENARIO("Test parseURI")
{
    {
        auto f = mgo::parseURI("resource://hello.txt");

        REQUIRE( f.first  == "resource");
        REQUIRE( f.second == "hello.txt");
    }
    {
        auto f = mgo::parseURI("file:///home/gavin/hello.txt");

        REQUIRE( f.first  == "file");
        REQUIRE( f.second == "/home/gavin/hello.txt");
    }
    {
        auto f = mgo::parseURI("/home/gavin/hello.txt");

        REQUIRE( f.first  == "");
        REQUIRE( f.second == "/home/gavin/hello.txt");
    }
    {
        auto f = mgo::parseURI("hello.txt");

        REQUIRE( f.first  == "");
        REQUIRE( f.second == "hello.txt");
    }
}




