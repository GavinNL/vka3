#include <vka/uGLTF_Helpers.h>

#include "catch.hpp"

#include <mgo/common_functions.h>



SCENARIO("Test parseURI")
{
    std::map<std::string, uint32_t> _map;
    std::vector<std::string> _vector;

    uint32_t i=0;
    _vector.resize(67);
    _vector[i++] = "mixamorig:Hips";
    _vector[i++] = "mixamorig:Spine";
    _vector[i++] = "mixamorig:Spine1";
    _vector[i++] = "mixamorig:Spine2";
    _vector[i++] = "mixamorig:Neck";
    _vector[i++] = "mixamorig:Head";
    _vector[i++] = "mixamorig:HeadTop_End";
    _vector[i++] = "mixamorig:LeftEye";
    _vector[i++] = "mixamorig:RightEye";
    _vector[i++] = "mixamorig:LeftShoulder";
    _vector[i++] = "mixamorig:LeftArm";
    _vector[i++] = "mixamorig:LeftForeArm";
    _vector[i++] = "mixamorig:LeftHand";
    _vector[i++] = "mixamorig:LeftHandMiddle1";
    _vector[i++] = "mixamorig:LeftHandMiddle2";
    _vector[i++] = "mixamorig:LeftHandMiddle3";
    _vector[i++] = "mixamorig:LeftHandMiddle4";
    _vector[i++] = "mixamorig:LeftHandThumb1";
    _vector[i++] = "mixamorig:LeftHandThumb2";
    _vector[i++] = "mixamorig:LeftHandThumb3";
    _vector[i++] = "mixamorig:LeftHandThumb4";
    _vector[i++] = "mixamorig:LeftHandIndex1";
    _vector[i++] = "mixamorig:LeftHandIndex2";
    _vector[i++] = "mixamorig:LeftHandIndex3";
    _vector[i++] = "mixamorig:LeftHandIndex4";
    _vector[i++] = "mixamorig:LeftHandRing1";
    _vector[i++] = "mixamorig:LeftHandRing2";
    _vector[i++] = "mixamorig:LeftHandRing3";
    _vector[i++] = "mixamorig:LeftHandRing4";
    _vector[i++] = "mixamorig:LeftHandPinky1";
    _vector[i++] = "mixamorig:LeftHandPinky2";
    _vector[i++] = "mixamorig:LeftHandPinky3";
    _vector[i++] = "mixamorig:LeftHandPinky4";
    _vector[i++] = "mixamorig:RightShoulder";
    _vector[i++] = "mixamorig:RightArm";
    _vector[i++] = "mixamorig:RightForeArm";
    _vector[i++] = "mixamorig:RightHand";
    _vector[i++] = "mixamorig:RightHandMiddle1";
    _vector[i++] = "mixamorig:RightHandMiddle2";
    _vector[i++] = "mixamorig:RightHandMiddle3";
    _vector[i++] = "mixamorig:RightHandMiddle4";
    _vector[i++] = "mixamorig:RightHandThumb1";
    _vector[i++] = "mixamorig:RightHandThumb2";
    _vector[i++] = "mixamorig:RightHandThumb3";
    _vector[i++] = "mixamorig:RightHandThumb4";
    _vector[i++] = "mixamorig:RightHandIndex1";
    _vector[i++] = "mixamorig:RightHandIndex2";
    _vector[i++] = "mixamorig:RightHandIndex3";
    _vector[i++] = "mixamorig:RightHandIndex4";
    _vector[i++] = "mixamorig:RightHandRing1";
    _vector[i++] = "mixamorig:RightHandRing2";
    _vector[i++] = "mixamorig:RightHandRing3";
    _vector[i++] = "mixamorig:RightHandRing4";
    _vector[i++] = "mixamorig:RightHandPinky1";
    _vector[i++] = "mixamorig:RightHandPinky2";
    _vector[i++] = "mixamorig:RightHandPinky3";
    _vector[i++] = "mixamorig:RightHandPinky4";
    _vector[i++] = "mixamorig:RightUpLeg";
    _vector[i++] = "mixamorig:RightLeg";
    _vector[i++] = "mixamorig:RightFoot";
    _vector[i++] = "mixamorig:RightToeBase";
    _vector[i++] = "mixamorig:RightToe_End";
    _vector[i++] = "mixamorig:LeftUpLeg";
    _vector[i++] = "mixamorig:LeftLeg";
    _vector[i++] = "mixamorig:LeftFoot";
    _vector[i++] = "mixamorig:LeftToeBase";
    _vector[i++] = "mixamorig:LeftToe_End";

    i=0;
    for(auto & v : _vector)
    {
        _map[ v ] = i++;
    }


    BENCHMARK("Map: find RightHandThumb1")
    {
        const std::string key = "mixamorig:RightHandThumb1";
        auto it = _map.find(key);
        if( it!=_map.end()) return it->second;
        return 0u;
    };

    BENCHMARK("Vector: find RightHandThumb1")
    {
        const std::string key = "mixamorig:RightHandThumb1";
        int32_t i=0;
        for(auto & v : _vector)
        {
            if(v==key)
                return i;
            ++i;
        }
        return 0;
    };

    BENCHMARK("Vector: find RightHandThumb1 find_if")
    {
        const std::string key = "mixamorig:RightHandThumb1";
        auto it = std::find_if( _vector.begin(), _vector.end(),
                      [&](auto & v)
        {
            return v==key;
        });
        if(it==_vector.end()) return 0l;
        return std::distance(_vector.begin(), it);
    };

}




SCENARIO("Test parseURI, without prepended name")
{
    std::map<std::string, uint32_t> _map;
    std::vector<std::string> _vector;

    uint32_t i=0;
    _vector.resize(67);
    _vector[i++] = "Hips";
    _vector[i++] = "Spine";
    _vector[i++] = "Spine1";
    _vector[i++] = "Spine2";
    _vector[i++] = "Neck";
    _vector[i++] = "Head";
    _vector[i++] = "HeadTop_End";
    _vector[i++] = "LeftEye";
    _vector[i++] = "RightEye";
    _vector[i++] = "LeftShoulder";
    _vector[i++] = "LeftArm";
    _vector[i++] = "LeftForeArm";
    _vector[i++] = "LeftHand";
    _vector[i++] = "LeftHandMiddle1";
    _vector[i++] = "LeftHandMiddle2";
    _vector[i++] = "LeftHandMiddle3";
    _vector[i++] = "LeftHandMiddle4";
    _vector[i++] = "LeftHandThumb1";
    _vector[i++] = "LeftHandThumb2";
    _vector[i++] = "LeftHandThumb3";
    _vector[i++] = "LeftHandThumb4";
    _vector[i++] = "LeftHandIndex1";
    _vector[i++] = "LeftHandIndex2";
    _vector[i++] = "LeftHandIndex3";
    _vector[i++] = "LeftHandIndex4";
    _vector[i++] = "LeftHandRing1";
    _vector[i++] = "LeftHandRing2";
    _vector[i++] = "LeftHandRing3";
    _vector[i++] = "LeftHandRing4";
    _vector[i++] = "LeftHandPinky1";
    _vector[i++] = "LeftHandPinky2";
    _vector[i++] = "LeftHandPinky3";
    _vector[i++] = "LeftHandPinky4";
    _vector[i++] = "RightShoulder";
    _vector[i++] = "RightArm";
    _vector[i++] = "RightForeArm";
    _vector[i++] = "RightHand";
    _vector[i++] = "RightHandMiddle1";
    _vector[i++] = "RightHandMiddle2";
    _vector[i++] = "RightHandMiddle3";
    _vector[i++] = "RightHandMiddle4";
    _vector[i++] = "RightHandThumb1";
    _vector[i++] = "RightHandThumb2";
    _vector[i++] = "RightHandThumb3";
    _vector[i++] = "RightHandThumb4";
    _vector[i++] = "RightHandIndex1";
    _vector[i++] = "RightHandIndex2";
    _vector[i++] = "RightHandIndex3";
    _vector[i++] = "RightHandIndex4";
    _vector[i++] = "RightHandRing1";
    _vector[i++] = "RightHandRing2";
    _vector[i++] = "RightHandRing3";
    _vector[i++] = "RightHandRing4";
    _vector[i++] = "RightHandPinky1";
    _vector[i++] = "RightHandPinky2";
    _vector[i++] = "RightHandPinky3";
    _vector[i++] = "RightHandPinky4";
    _vector[i++] = "RightUpLeg";
    _vector[i++] = "RightLeg";
    _vector[i++] = "RightFoot";
    _vector[i++] = "RightToeBase";
    _vector[i++] = "RightToe_End";
    _vector[i++] = "LeftUpLeg";
    _vector[i++] = "LeftLeg";
    _vector[i++] = "LeftFoot";
    _vector[i++] = "LeftToeBase";
    _vector[i++] = "LeftToe_End";

    i=0;
    for(auto & v : _vector)
    {
        _map[ v ] = i++;
    }


    BENCHMARK("Map: find RightHandThumb1")
    {
        const std::string key = "RightHandThumb1";
        auto it = _map.find(key);
        if( it!=_map.end()) return it->second;
        return 0u;
    };

    BENCHMARK("Vector: find RightHandThumb1")
    {
        const std::string key = "RightHandThumb1";
        int32_t i=0;
        for(auto & v : _vector)
        {
            if(v==key)
                return i;
            ++i;
        }
        return 0;
    };

    BENCHMARK("Vector: find RightHandThumb1 find_if")
    {
        const std::string key = "RightHandThumb1";
        auto it = std::find_if( _vector.begin(), _vector.end(),
                      [&](auto & v)
        {
            return v==key;
        });
        if(it==_vector.end()) return 0l;
        return std::distance(_vector.begin(), it);
    };



    BENCHMARK("Map: find LeftToe_End")
    {
        const std::string key = "LeftToe_End";
        auto it = _map.find(key);
        if( it!=_map.end()) return it->second;
        return 0u;
    };

    BENCHMARK("Vector: find LeftToe_End")
    {
        const std::string key = "LeftToe_End";
        int32_t i=0;
        for(auto & v : _vector)
        {
            if(v==key)
                return i;
            ++i;
        }
        return 0;
    };

    BENCHMARK("Vector: find LeftToe_End find_if")
    {
        const std::string key = "LeftToe_End";
        auto it = std::find_if( _vector.begin(), _vector.end(),
                      [&](auto & v)
        {
            return v==key;
        });
        if(it==_vector.end()) return 0l;
        return std::distance(_vector.begin(), it);
    };

}
