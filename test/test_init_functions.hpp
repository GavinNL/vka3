#ifndef TEST_INIT_FUNCTIONS_H
#define TEST_INIT_FUNCTIONS_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vka/vka.h>
#include <vka/core/System.h>
#include <vka/core/vka_vulkan.h>

#include <vka/logging.h>


SDL_Window               * m_window = nullptr;
vk::DebugReportCallbackEXT m_callback;
vka::SystemCreateInfo2     m_SysCreateInfo;


static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData)
{
    (void)location;
    (void)flags;
    (void)code;
    (void)userData;
    (void)obj;
    (void)objType;
    VKA_WARN("**Validation** [{:s}]: {:s}",layerPrefix, msg);

    return VK_FALSE;
}

static SDL_Window* initWindow()
{
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS );

    if(SDL_Vulkan_LoadLibrary(nullptr) == -1)
    {
        VKA_CRIT("Error loading vulkan");
        exit(1);
    }
    atexit(SDL_Quit);

    auto window = SDL_CreateWindow("APPLICATION_NAME",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        1024,
        768,
        SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);

    if(window == nullptr)
    {
        VKA_CRIT("Couldn\'t set video mode: {}", SDL_GetError());
        exit(1);
    }
    return window;
}

static std::vector<std::string> SDL_GetInstanceExtensions(SDL_Window * window)
{
    std::vector<std::string> ext;

    // We only need to populate the .requiredExtensions array
    // with the values returned by SDL
    unsigned int count = 0;
    SDL_Vulkan_GetInstanceExtensions(window, &count, nullptr);

    const char **names = new const char *[count];
    SDL_Vulkan_GetInstanceExtensions(window, &count, names);

    for(uint32_t i=0; i < count ; i++)
    {
        ext.push_back( names[i] );
    }
    return ext;
}

static vk::SurfaceKHR SDL_CreateVulkanSurface(vk::Instance instance, SDL_Window * window)
{
    // Create a surface using the SDL function
    vk::SurfaceKHR             surface;
    if( !SDL_Vulkan_CreateSurface( window, instance, reinterpret_cast<VkSurfaceKHR*>(&surface)  ) )
    {
        throw std::runtime_error( "Failed to create surface" );
    }
    return surface;
}

static void init()
{

    m_window = initWindow(); // create a SDL window
    //=======================================================================
    // BOILER PLATE CODE TO GET THE INSTANCE and DEVICE SETUP
    //=======================================================================
    // 1. Create an instance using teh vka::InstanceCreateInfo2 struct
    vka::InstanceCreateInfo2 instanceCreateInfo;
    instanceCreateInfo.requiredExtensions  = SDL_GetInstanceExtensions(m_window);
    instanceCreateInfo.requiredExtensions.push_back( VK_EXT_DEBUG_REPORT_EXTENSION_NAME); // need this for debug callback
    instanceCreateInfo.validationLayers    = {"VK_LAYER_LUNARG_standard_validation"};

    auto instance    = vka::createInstance(instanceCreateInfo);


    m_callback = vka::createDebugCallback( instance, debugCallback );

    // 2. Create a surface using the SDL function. We need this first to
    //    figure out if we can draw to this surface.
    vk::SurfaceKHR   surface = SDL_CreateVulkanSurface(instance, m_window);

    // 3. Create a physical device using the physicalDeviceCreateInfo2 struct
    vka::PhysicalDeviceCreateInfo2 physicalDeviceCreateInfo;
    physicalDeviceCreateInfo.instance = instance;
    physicalDeviceCreateInfo.surface  = surface;
    physicalDeviceCreateInfo.deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME}; // no need for this
                                                                                   // it's automatically set as the default.
    auto physicalDevice = vka::createPhysicalDevice(physicalDeviceCreateInfo);
    assert(physicalDevice);

    // 4. create a logical device using the deviceCreateInfo2 struct
    vka::DeviceCreateInfo2 deviceCreateInfo;
    deviceCreateInfo.physicalDevice = physicalDevice;
    deviceCreateInfo.requiredDeviceExtensions = physicalDeviceCreateInfo.deviceExtensions; //
    deviceCreateInfo.requiredValidationLayers = instanceCreateInfo.validationLayers;
    // 4.1 find appropriate queues that can present graphics to the surface.
    deviceCreateInfo.findAppropriateQueues(surface);
    assert( deviceCreateInfo.graphicsQueueFamily < std::numeric_limits<uint32_t>::max() );
    assert( deviceCreateInfo.presentQueueFamily  < std::numeric_limits<uint32_t>::max() );

    auto device = vka::createDevice(deviceCreateInfo);

    assert(device);

    // Create all the vulkan objects
    // You do not need to use this function.
    // You can create all the objects yourself
    vka::SystemCreateInfo2 sysCreateInfo;
    sysCreateInfo.device              = device;
    sysCreateInfo.instance            = instance;
    sysCreateInfo.surface             = surface;
    sysCreateInfo.physicalDevice      = physicalDevice;
    sysCreateInfo.queuePresentFamily  = deviceCreateInfo.presentQueueFamily;
    sysCreateInfo.queueGraphicsFamily = deviceCreateInfo.graphicsQueueFamily;
    sysCreateInfo.queuePresent        = device.getQueue( deviceCreateInfo.presentQueueFamily  , 0u );
    sysCreateInfo.queueGraphics       = device.getQueue( deviceCreateInfo.graphicsQueueFamily , 0u );


    m_SysCreateInfo = sysCreateInfo;
    // create the global system object
    vka::System::createSystem( sysCreateInfo );
}

static void destroy()
{
    vka::System::get().destroy();
    vka::destroyDebugCallback( m_SysCreateInfo.instance, m_callback);
    m_SysCreateInfo.instance.destroySurfaceKHR(m_SysCreateInfo.surface);
    m_SysCreateInfo.device.destroy();
    m_SysCreateInfo.instance.destroy();
    SDL_DestroyWindow(m_window);
}

#endif
