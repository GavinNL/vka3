#include <vka/math/geometry/bvhTree.h>

#include "catch.hpp"
#include <iostream>


SCENARIO("asdfaf")
{
    glm::u8vec2 y = {1, 2};
    glm::u8vec2 z = {2, 1};

    auto m = glm::min(y,z);
    auto M = glm::max(y,z);

    REQUIRE( m.x == 1 );
    REQUIRE( m.y == 1 );
    REQUIRE( M.x == 2 );
    REQUIRE( M.x == 2 );

    auto b = glm::lessThan(y,z);
    REQUIRE( b.x == true);
    REQUIRE( b.y == false);
}

SCENARIO("Expands")
{

    vka::AABB a1;
    a1.lowerBound = {0,0,0};
    a1.upperBound = {10,10,10};

    a1.expand({5,5,5});
    REQUIRE( a1.lowerBound.x == Approx(0) );
    REQUIRE( a1.lowerBound.y == Approx(0) );
    REQUIRE( a1.lowerBound.z == Approx(0) );

    REQUIRE( a1.upperBound.x == Approx(10) );
    REQUIRE( a1.upperBound.y == Approx(10) );
    REQUIRE( a1.upperBound.z == Approx(10) );

    a1.expand({20,20,20});
    REQUIRE( a1.lowerBound.x == Approx(0) );
    REQUIRE( a1.lowerBound.y == Approx(0) );
    REQUIRE( a1.lowerBound.z == Approx(0) );

    REQUIRE( a1.upperBound.x == Approx(20) );
    REQUIRE( a1.upperBound.y == Approx(20) );
    REQUIRE( a1.upperBound.z == Approx(20) );

    a1.expand({-5,-5,-5});
    REQUIRE( a1.lowerBound.x == Approx(-5) );
    REQUIRE( a1.lowerBound.y == Approx(-5) );
    REQUIRE( a1.lowerBound.z == Approx(-5) );

    REQUIRE( a1.upperBound.x == Approx(20) );
    REQUIRE( a1.upperBound.y == Approx(20) );
    REQUIRE( a1.upperBound.z == Approx(20) );

    a1.expand({20,40,20});
    REQUIRE( a1.lowerBound.x == Approx(-5) );
    REQUIRE( a1.lowerBound.y == Approx(-5) );
    REQUIRE( a1.lowerBound.z == Approx(-5) );

    REQUIRE( a1.upperBound.x == Approx(20) );
    REQUIRE( a1.upperBound.y == Approx(40) );
    REQUIRE( a1.upperBound.z == Approx(20) );
}

SCENARIO("Expands")
{

    vka::AABB a1;
    a1.lowerBound = {-10,-10,-10};
    a1.upperBound = {10,10,10};


    vka::AABB a2;
    a2.lowerBound = {-5,-20,-5};
    a2.upperBound = { 5, 20, 5};


    a1.expand(a2);
    REQUIRE( a1.lowerBound.x == Approx(-10) );
    REQUIRE( a1.lowerBound.y == Approx(-20) );
    REQUIRE( a1.lowerBound.z == Approx(-10) );

    REQUIRE( a1.upperBound.x == Approx(10) );
    REQUIRE( a1.upperBound.y == Approx(20) );
    REQUIRE( a1.upperBound.z == Approx(10) );

}

SCENARIO("Test Custom Shape")
{
    std::vector<double> x;
    std::vector<double> y;
    std::vector<double> z;

    for(uint32_t i=0;i<100000;i++)
    {
        x.push_back(rand() % 100);
        y.push_back(rand() % 100);
        z.push_back(rand() % 100);
    }

    THEN("Array/Double")
    {
        using value_type = float;
        using Tree_t     = vka::BVHTree<value_type, 3>;
        using vec_type   = Tree_t::vec_type;
        using aabb_type  = typename Tree_t::aabb_type;

        // Fattening factor.
        double fatten = 0.1;

        // Size of the simulation box.
        vec_type boxSize({100, 100, 100});

        // Number of large discs.
        unsigned int nLarge = 50000;


        Tree_t treeLarge(fatten, {false, false, false}, boxSize, nLarge, true);

        treeLarge.insertParticle(1, {10,10,10}, 3);

        treeLarge.insertParticle(2, {500,500,500}, 5);
        struct Sphere
        {
            vec_type center;
            float R;
        };

        Sphere F{ {0,0,0}, 10};
        std::vector<uint32_t> particles;
        treeLarge.query(
                     0xFFFFFFFF,
                     F,
                     // lambda which returns true if F and aabb intersect
                     [](Sphere const & F, aabb_type const & aabb)
                     {
                            auto r = glm::length(aabb.upperBound-aabb.lowerBound);
                            return glm::length(F.center - aabb.centre) < (r + F.R);
                     },
                     // callback funciton.
                     [&](uint32_t particle)
                     {
                            particles.push_back(particle);
                     }
                    );

        REQUIRE( particles.size() == 1);
    }

}



SCENARIO("Test Vector")
{
    std::vector<double> x;
    std::vector<double> y;
    std::vector<double> z;

    for(uint32_t i=0;i<100000;i++)
    {
        x.push_back(rand() % 100);
        y.push_back(rand() % 100);
        z.push_back(rand() % 100);
    }

    THEN("Array/Double")
    {
        using value_type = float;
        using Tree_t     = vka::BVHTree<value_type, 3>;
        using vec_type   = Tree_t::vec_type;
        using aabb_type  = typename Tree_t::aabb_type;

        // Fattening factor.
        double fatten = 0.1;

        // Size of the simulation box.
        vec_type boxSize({100, 100, 100});

        // Number of large discs.
        unsigned int nLarge = 50000;


        Tree_t treeLarge(fatten, {false, false, false}, boxSize, nLarge, true);

        for(uint32_t i=0;i<5000;i++)
        {
            auto _x = static_cast<value_type>( x[i] );
            auto _y = static_cast<value_type>( y[i] );
            auto _z = static_cast<value_type>( z[i] );
            treeLarge.insertParticle(i++, { _x,_y,_z }, 3.0);
        }


        BENCHMARK("Query")
        {
            return treeLarge.query(
                        aabb_type( {0,0,0}, {10,10,10} )
                        );

        };


        struct Sphere
        {
            vec_type center;
            float R;
        };

        Sphere F{ {0,0,0}, 10};

        BENCHMARK("Query Sphere")
        {
            std::vector<uint32_t> particles;
            treeLarge.query(
                         0xFFFFFFFF,
                         F,
                         // lambda which returns true if F and aabb intersect
                         [](Sphere const & F, aabb_type const & aabb)
                         {
                                const auto dr = (aabb.upperBound-aabb.lowerBound) / 2.f;
                                auto r = glm::length(dr) ;
                                return glm::length(F.center - aabb.centre) < (r + F.R);
                         },
                         // callback funciton.
                         [&](uint32_t particle)
                         {
                                particles.push_back(particle);
                         }
                        );
        };
    }

}







