#include <vulkan/vulkan.hpp>


// WINDOW_MANAGER is a definition which is defined in the CMakeLists.txt file
// if WINDOW_MANAGER==0 it will use a qt window
// if WINDOW_MANGER==1 it will use an SDL window.

#if WINDOW_MANAGER == 0

//#define USE_MAIN_WINDOW

#if defined USE_MAIN_WINDOW
#include "qt_src/qt_mainwindow.h"
#endif
#include <vka/RenderSurfaces/QtVulkanWidget.h>
#include <QApplication>
#include <QPlainTextEdit>
#include <QVulkanInstance>
#include <QLibraryInfo>
#include <QLoggingCategory>
#include <QPointer>

Q_LOGGING_CATEGORY(lcVk, "qt.vulkan")


static QPointer<QPlainTextEdit> messageLogWidget;
static QtMessageHandler oldMessageHandler = nullptr;

static void messageHandler(QtMsgType msgType, const QMessageLogContext &logContext, const QString &text)
{
    if (!messageLogWidget.isNull())
        messageLogWidget->appendPlainText(text);
    if (oldMessageHandler)
        oldMessageHandler(msgType, logContext, text);
}

#else

#include <vka/RenderSurfaces/SDLVulkanWidget.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData)
{
    spdlog::warn("**Validation** [{:s}]: {:s}",layerPrefix, msg);

    return VK_FALSE;
}

#endif


int main(int argc, char *argv[])
{

#if WINDOW_MANAGER == 0
    QApplication app(argc, argv);

    messageLogWidget = new QPlainTextEdit(QLatin1String(QLibraryInfo::build()) + QLatin1Char('\n'));
    messageLogWidget->setReadOnly(true);

    oldMessageHandler = qInstallMessageHandler(messageHandler);

    QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));

    QVulkanInstance inst;

    #ifndef Q_OS_ANDROID
        inst.setLayers(QByteArrayList() << "VK_LAYER_LUNARG_standard_validation");
    #else
        inst.setLayers(QByteArrayList()
                       << "VK_LAYER_GOOGLE_threading"
                       << "VK_LAYER_LUNARG_parameter_validation"
                       << "VK_LAYER_LUNARG_object_tracker"
                       << "VK_LAYER_LUNARG_core_validation"
                       << "VK_LAYER_LUNARG_image"
                       << "VK_LAYER_LUNARG_swapchain"
                       << "VK_LAYER_GOOGLE_unique_objects");
    #endif

    if (!inst.create())
        qFatal("Failed to create Vulkan instance: %d", inst.errorCode());

#else

#endif


    #if WINDOW_MANAGER == 0
     using VulkanWidget = vka::QtVulkanWidget;
    #else
    using VulkanWidget = vka::SDLVulkanWidget;
    #endif

    // This class provides all the draw functions
    // it can be used with the QTRenderSurface
    // as well as the SDLRenderSurface

    //=============================================================
    // This the main VulkanWidget
    //=============================================================
    // This is the widget that can be added to a QtWindow or any other
    // widget. The actual  Applicatioof the application
    VulkanWidget *vulkanWindow = new VulkanWidget();

#if WINDOW_MANAGER == 0
    vulkanWindow->setVulkanInstance(&inst);

    #if defined USE_MAIN_WINDOW
        // or we can add it to another widget;
        MainWindow mainWindow(vulkanWindow, messageLogWidget.data());

        mainWindow.resize(1024, 768);
        mainWindow.show();
    #else
    // we can just show the window on its own
    vulkanWindow->resize(1024, 768);
    vulkanWindow->show();
    #endif

#else
    vulkanWindow->resize(1024, 768);
    vulkanWindow->show();
#endif

    //=============================================================

    // calling show() will create the instance of the VulkanApplication




#if WINDOW_MANAGER == 0
    return app.exec();
#else
    return vulkanWindow->exec();
#endif


}
