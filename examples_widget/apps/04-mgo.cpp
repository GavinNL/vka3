#define WINDOW_MANAGER 1



#if WINDOW_MANAGER == 0

#include "qt_src/qt_mainwindow.h"
#include <vka/RenderSurfaces/QtVulkanWidget.h>
#include <QApplication>
#include <QPlainTextEdit>
#include <QVulkanInstance>
#include <QLibraryInfo>
#include <QLoggingCategory>
#include <QPointer>

Q_LOGGING_CATEGORY(lcVk, "qt.vulkan")


static QPointer<QPlainTextEdit> messageLogWidget;
static QtMessageHandler oldMessageHandler = nullptr;

static void messageHandler(QtMsgType msgType, const QMessageLogContext &logContext, const QString &text)
{
    if (!messageLogWidget.isNull())
        messageLogWidget->appendPlainText(text);
    if (oldMessageHandler)
        oldMessageHandler(msgType, logContext, text);
}

#else

#include <vka/RenderSurfaces/SDLVulkanWidget.h>
#include <spdlog/spdlog.h>
#include <spdlog/sinks/stdout_color_sinks.h>

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData)
{
    spdlog::warn("**Validation** [{:s}]: {:s}",layerPrefix, msg);

    return VK_FALSE;
}

#endif

#include <mgo/SystemBase.h>
#include <mgo/SDLInputSystem.h>
#include <mgo/RenderSystem.h>
#include <mgo/ControlSystem.h>
#include <mgo/PhysicsSystem.h>
#include <mgo/AnimationSystem.h>
#include <mgo/TransformSystem.h>

#include <mgo/Commands.h>
#include <mgo/ResourcePath.h>

#include <mgo/Engine.h>
#include <iostream>

#include <iostream>

#include <vka/vka_vulkan.h>

#include <vka/System.h>
#include <vka/BufferMemoryPool.h>
#include <vka/DescriptorSets.h>
#include <vka/PipelineCreateInfo.h>
#include <vka/DeviceMeshPrimitive.h>
#include <vka/TextureMemoryPool.h>
#include <vka/Primatives.h>
#include <vka/HostImage.h>
#include <vka/linear_algebra.h>

#include <mgo/CollisionComponent.h>
#include <mgo/TransformComponent.h>
#include <mgo/LightComponent.h>
#include <mgo/ScriptComponent.h>



#define WIDTH  1920
#define HEIGHT 1080

struct MGO4  : public vka::VulkanApplication
{
public:
    void initResources() override
    {
        m_engine = std::make_shared<mgo::Engine>();

        m_engine->addResourcePath( "/home/gavin" );
        m_engine->addResourcePath( "/home/gavin/Other_Projects/vka3/share" );
        m_engine->addResourcePath( "/home/gavin/Other_Projects/" );
        m_engine->addResourcePath( VKA_SOURCE_DIR "/share" );
        m_engine->addResourcePath( "/home/globo/Projects" );

        m_engine->init();

        m_engine->m_RenderSystem->m_System = m_System;
        m_engine->m_RenderSystem->initResources();

        {
            auto   e = m_engine->createEntity2("CameraEntity")
                             .create<mgo::TransformComponent>( mgo::vec3(0,0,5) )
                             .create<mgo::CameraComponent>()
                             .create<mgo::LightComponent>()
                           //  .create<mgo::ScriptComponent>("controller", "resource://scripts/3rd_person_camera.lua")
                             .create<mgo::ScriptComponent>("controller", "resource://scripts/fps_free_camera.lua")
                    ;

            auto & L = m_engine->get<mgo::LightComponent>(e);
            L.diffuseColour.r = 100;
            L.diffuseColour.g = 100;
            L.diffuseColour.b = 100;
        }

        {
            auto boxPrimEntity    = m_engine->quickCreate<mgo::PrimitiveComponent>("primitive::box",    vka::boxPrimitive(1,1,1) );
            // Create two SceneComponent entities which we will use
            auto boxSceneEntity    = m_engine->createEntity2("ent::Box")
                                           .create<mgo::SceneComponent>(boxPrimEntity)
                                           .getID();

            {
                auto e2 = m_engine->createEntity2();

                vka::transform rootTransform;
                rootTransform.set_scale( {40,1.0,40} );

                e2.create<mgo::TransformComponent>(  )
                  .create<mgo::RigidBodyComponent>( 0.0f, 0)
                  .create<mgo::CollisionComponent>(btBoxShape( btVector3(20,.5,20)) )
                  .create<mgo::RenderComponent   >( boxSceneEntity, rootTransform );

            }
        }
    }

    void releaseResources() override
    {
        m_engine->m_RenderSystem->releaseResources();
    }

    void initSwapChainResources() override
    {
        m_engine->m_RenderSystem->m_swapChainColorFormat = colorFormat();
        m_engine->m_RenderSystem->m_swapChainDepthFormat = depthStencilFormat();

        m_engine->m_RenderSystem->initSwapChainResources();
    }

    void releaseSwapChainResources() override
    {
        m_engine->m_RenderSystem->releaseSwapChainResources();
    }

    void mouseMoveEvent(vka::EvtInputMouseMotion const * e) override
    {
        m_engine->trigger( *e);
    }
    void mousePressEvent(vka::EvtInputMouseButton const * e) override
    {
        m_engine->trigger( *e);
    }
    void mouseReleaseEvent(vka::EvtInputMouseButton const * e) override
    {
        m_engine->trigger( *e);
    }
    void keyPressEvent(vka::EvtInputKey const * e) override
    {
        m_engine->trigger( *e);
    }
    void keyReleaseEvent(vka::EvtInputKey const * e) override
    {
        m_engine->trigger( *e);
    }

    void preRender() override
    {
        m_engine->m_RenderSystem->preRender();
    }


    void postRender() override
    {
        m_engine->m_RenderSystem->prePostRender();
    }

    void render(vka::WINDOWFrame &frame) override
    {

      //  mgo::CmdRenderDrawLines lines;
      //  lines.colors = {
      //      glm::u8vec4(255,0,0,255),
      //      glm::u8vec4(255,0,0,255),
      //      glm::u8vec4( 0,255,0,255),
      //      glm::u8vec4( 0,255,0,255),
      //      glm::u8vec4( 0,0,255,255),
      //      glm::u8vec4( 0,0,255,255)
      //  };
      //
      //  lines.positions = {
      //      glm::vec3( 0,0,0),
      //      glm::vec3( 255,0,0),
      //      glm::vec3( 0,0,0),
      //      glm::vec3( 0,255,0),
      //      glm::vec3( 0,0,0),
      //      glm::vec3( 0,0,255)
      //  };
      //
      //  m_engine->trigger( lines );
      //  m_engine->trigger( mgo::CmdMainLoop() );

        m_engine->trigger( mgo::CmdMainLoop() );
        m_engine->m_RenderSystem->render(frame);
    }

public:
    std::shared_ptr<mgo::Engine> m_engine;
};


vka::VulkanApplication* VKAMain()
{
    return new MGO4();
}
