#ifndef VKA_BASIS_CONVERTER_BASE_H
#define VKA_BASIS_CONVERTER_BASE_H

#include <vector>
#include <cstring>

#include <vulkan/vulkan.hpp>

namespace vka
{


/**
 *
 *  To Compress an image to BC3, use the following:
 *
 *  vka::HostImage img;
 *
 *  // use 4 threads, only need to initiate this once
 *  // it can be reused
 *  vka::BasisConverter converter(4);
 *
 *  // Push an image layer into the converter
 *  converter.pushImageLayer(img);
 *
 *  // Start the conversion proces
 *  //   true - generate mipmaps
 *  auto B = converter.process(true);
 *
 *  // Compress the image to BC3 format
 *  auto BC3 = B.transcodeImage(0,0, vk::Format::eBc3UnormBlock,1);
 *
 *  // use the raw data
 *  BC3.m_data
 */



class CompressedImage
{
public:
    vk::Format           m_format;
    std::vector<uint8_t> m_data;

};

/**
 * @brief The BasisImage class
 *
 * Pmpl class for BasisImage
 */
class BasisImage
{
public:
    BasisImage(){};
    ~BasisImage(){};


    /**
     * @brief transcodeImage
     * @param image_index
     * @param mip_level_index
     * @param format
     * @param unused
     * @param get_alpha_for_opaque_formats
     * @return
     *
     * Return a single compressed image of a parctular image_index and mip_level
     */
    CompressedImage transcodeImage( uint32_t image_index,
                                    uint32_t mip_level_index,
                                    vk::Format vkformat,
                                    uint32_t get_alpha_for_opaque_formats);

    size_t size() const
    {
        return m_data.size();
    }
private:
    std::vector<uint8_t> m_data;
    uint32_t width;
    uint32_t height;
    uint32_t layers;
    friend class BasisConverter;
};


class BasisConverterImpl;

/**
 * @brief The BasisConverter class
 *
 * The BasisConverter class is used to convert HostImages
 * into to Basis format. The Basis format can then be
 * converted into compressed textures.
 *
 * You shoudl only have one BasisConverter instantiated because
 * it keeps an internal threadpool
 */
class BasisConverter
{
    std::unique_ptr<BasisConverterImpl> m_impl;
public:
    BasisConverter(uint32_t threadPoolThreadCount=1);
    ~BasisConverter();

    /**
     * @brief imageCount
     * @return
     * Returns the number of image layers in the converter
     */
    size_t imageCount() const;

    /**
     * @brief clear
     *
     * Clear all images
     */
    void clear();

    /**
     * @brief pushImageLayer
     * @param I
     *
     * Push an image into the converter
     */
    void pushImageLayer(void const * data, size_t size, uint32_t width, uint32_t height);

    /**
     * @brief process
     * @param I
     * @param genMipMaps
     * @return
     *
     * Process all the layers and return a BasisImage
     * which can then be converted into various compressed
     * texture formats.
     *
     * Make sure to call clear() after you generate the images
     */
    BasisImage process( bool genMipMaps);

};







}

#endif 


