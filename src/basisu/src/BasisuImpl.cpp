#include "basisu/basisu_comp.h"
#include "basisu/transcoder/basisu_transcoder.h"
#include <vka/BasisConverter/BasisConverterBase.h>
#include <cstring>

namespace vka
{

class BasisConverterImpl
{
public:
    BasisConverterImpl(uint32_t threadPoolThreadCount=1) : m_jobPool(threadPoolThreadCount)
    {
        m_params.m_read_source_images = false;
        m_params.m_debug              = true;
        m_params.m_uastc              = true;
        m_params.m_pJob_pool          = &m_jobPool;

        m_params.m_mip_gen            = true;
    }

    basisu::job_pool m_jobPool;
    basisu::basis_compressor_params m_params;
};



















BasisConverter::BasisConverter(uint32_t threadPoolThreadCount)
{
    basisu::basisu_encoder_init();
    basist::basisu_transcoder_init();

    m_impl = std::make_unique<BasisConverterImpl>(threadPoolThreadCount);

    m_impl->m_params.m_read_source_images = false;
    m_impl->m_params.m_debug              = true;
    m_impl->m_params.m_uastc              = true;
    m_impl->m_params.m_pJob_pool          = &m_impl->m_jobPool;

    m_impl->m_params.m_mip_gen            = true;

}

BasisConverter::~BasisConverter()
{
    m_impl.reset();
}

size_t BasisConverter::imageCount() const
{
    return m_impl->m_params.m_source_images.size();
}


void BasisConverter::pushImageLayer(void const * data, size_t bytes, uint32_t width, uint32_t height)
{
    m_impl->m_params.m_source_images.emplace_back();
    basisu::image & i = m_impl->m_params.m_source_images.back();

    i.resize( width, height );

    assert( i.get_pixels().size() == width*height );

    std::memcpy( i.get_pixels().data(), data, bytes );
}

BasisImage BasisConverter::process(bool genMipMaps)
{
    basisu::basis_compressor comp;

    m_impl->m_params.m_read_source_images = false;
    m_impl->m_params.m_debug              = true;
    m_impl->m_params.m_uastc              = true;
    m_impl->m_params.m_mip_gen            = genMipMaps;

    comp.init(m_impl->m_params);

    auto err = comp.process();

    BasisImage B;
    if( err == basisu::basis_compressor::cECSuccess)
    {
        B.m_data = comp.get_output_basis_file();
    }

    return B;

}

void BasisConverter::clear()
{
    m_impl->m_params.m_source_images.clear();
}










static std::shared_ptr< basist::etc1_global_selector_codebook> get_Global_codebook()
{
    static auto s = std::make_shared<basist::etc1_global_selector_codebook>(basist::g_global_selector_cb_size, basist::g_global_selector_cb);
    return s;
}

CompressedImage BasisImage::transcodeImage(uint32_t image_index,
                                           uint32_t mip_level_index,
                                           vk::Format vkformat,
                                           uint32_t get_alpha_for_opaque_formats)
{

    basist::transcoder_texture_format format;
    if( vkformat == vk::Format::eBc3SrgbBlock ||
            vkformat == vk::Format::eBc3UnormBlock )
    {
        format = basist::transcoder_texture_format::cTFBC3;
    }
    else
    {
        throw std::runtime_error("other formats not implemented yet");
    }

    CompressedImage cmp;

    //  (void)unused;
#define MAGIC 0xDEADBEE1

    // assert(m_magic == MAGIC);
    // if (m_magic != MAGIC)
    //     return 0;
    basist::basisu_transcoder m_transcoder( get_Global_codebook().get() );

    m_transcoder.start_transcoding( m_data.data(), m_data.size() );

    const basist::transcoder_texture_format transcoder_format = static_cast<basist::transcoder_texture_format>(format);

    uint32_t orig_width, orig_height, total_blocks;
    if (!m_transcoder.get_image_level_desc(m_data.data(), m_data.size(), image_index, mip_level_index, orig_width, orig_height, total_blocks))
    {
        throw std::runtime_error("Could not get the parcular image level description");
    }


    std::vector<uint8_t> dst_data;

    uint32_t flags = get_alpha_for_opaque_formats ? basist::cDecodeFlagsTranscodeAlphaDataToOpaqueFormats : 0;

    uint32_t status;

    if (basis_transcoder_format_is_uncompressed(transcoder_format))
    {
        const uint32_t bytes_per_pixel = basis_get_uncompressed_bytes_per_pixel(transcoder_format);
        const uint32_t bytes_per_line = orig_width * bytes_per_pixel;
        const uint32_t bytes_per_slice = bytes_per_line * orig_height;

        dst_data.resize(bytes_per_slice);

        status = m_transcoder.transcode_image_level(
                    m_data.data(), m_data.size(), image_index, mip_level_index,
                    dst_data.data(), orig_width * orig_height,
                    transcoder_format,
                    flags,
                    orig_width,
                    nullptr,
                    orig_height);
    }
    else
    {
        uint32_t bytes_per_block = basis_get_bytes_per_block_or_pixel(transcoder_format);

        uint32_t required_size = total_blocks * bytes_per_block;

        if (transcoder_format == basist::transcoder_texture_format::cTFPVRTC1_4_RGB || transcoder_format == basist::transcoder_texture_format::cTFPVRTC1_4_RGBA)
        {
            // For PVRTC1, Basis only writes (or requires) total_blocks * bytes_per_block. But GL requires extra padding for very small textures:
            // https://www.khronos.org/registry/OpenGL/extensions/IMG/IMG_texture_compression_pvrtc.txt
            // The transcoder will clear the extra bytes followed the used blocks to 0.
            const uint32_t width = (orig_width + 3) & ~3;
            const uint32_t height = (orig_height + 3) & ~3;
            required_size = (std::max(8U, width) * std::max(8U, height) * 4 + 7) / 8;
            assert(required_size >= total_blocks * bytes_per_block);
        }

        dst_data.resize(required_size);

        status = m_transcoder.transcode_image_level(
                    m_data.data(), m_data.size(), image_index, mip_level_index,
                    dst_data.data(), dst_data.size() / bytes_per_block,
                    static_cast<basist::transcoder_texture_format>(format),
                    flags);
    }

    //      emscripten::val memory = emscripten::val::module_property("HEAP8")["buffer"];
    //      emscripten::val memoryView = emscripten::val::global("Uint8Array").new_(memory, reinterpret_cast<uintptr_t>(dst_data.data()), dst_data.size());

    //      dst.call<void>("set", memoryView);

    cmp.m_data = std::move(dst_data);
    cmp.m_format = vkformat;
    m_transcoder.stop_transcoding();
    return cmp;
}



}
