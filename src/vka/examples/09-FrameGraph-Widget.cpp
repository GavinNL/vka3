#include <iostream>



#include <vka/logging.h>


#include <vka/math.h>

#if !defined VKA_NO_SDL_WIDGET
#include <vka/widgets/SDLWidget.h>
#endif

#include <vka/widgets/Application.h>
#include <vka/core/ShaderModuleCreateInfo.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/System.h>
#include <vka/utils/GLSLCompiler.h>

#include <framegraph/framegraph2.h>

#define WIDTH  1024
#define HEIGHT 768


VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;

    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}


struct myExecutor : public vka::FrameGraphExecutor
{
    // FrameGraphExecutor interface
    vk::Pipeline m_trianglePipeline;
    vk::RenderPass m_swapchainRenderPass;

    bool m_initialized=false;
public:

    void initialize()
    {
        if(m_initialized) return;
        // use this to render to the swapchain
        vka::GraphicsPipelineCreateInfo4 P;

        P.vertexShader   = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/triangle.vert", EShLangVertex);
        P.fragmentShader = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/triangle.frag", EShLangFragment);
        P.renderPass     = m_swapchainRenderPass;
        //P.vertexInputFormats = {
        //                            vk::Format::eR32G32B32Sfloat, // these aren't actually used in the shader
        //                            vk::Format::eR32G32B32Sfloat, // these aren't actually used in the shader
        //                            vk::Format::eR32G32B32Sfloat, // these aren't actually used in the shader
        //                       };
        m_trianglePipeline = vka::System::get().createGraphicsPipeline(P);

        m_initialized=true;
    }

    vk::ShaderModule compileShaderModule(const std::string & path, EShLanguage language)
    {
        vka::GLSLCompiler compiler;
        auto spv   = compiler.compileFile(path, language  );

        vka::ShaderModuleCreateInfo2 createInfo;
        createInfo.code = spv;
        return vka::System::get().createShaderModule(createInfo);
    };

    //======================================================================================
    // FrameGraph methods
    //======================================================================================
    void init(RenderPassInit &I) override
    {
        (void)I;
        if( I.renderPassName == "swapchainPass")
        {
            // This section is executed when the resize() method is called
            m_swapchainRenderPass = I.renderPass;
            initialize();
        }
    }
    void destroy(RenderPassDestroy &D) override
    {
        (void)D;
    }
    void write(RenderPassWrite &C) override
    {
        (void)C;
        if( C.renderPassName == "swapchainPass")
        {
            // Set the clear color value to something other than the default.
            // the renderpassBeginInfo is currently pointing to this
            C.clearValues[0].setColor(  vk::ClearColorValue( std::array<float,4>( {1.0f,1.0f,0.0f,1.0f} ) )  );

            C.cmd.beginRenderPass( C.renderPassBeginInfo, vk::SubpassContents::eInline );


            C.cmd.bindPipeline( vk::PipelineBindPoint::eGraphics, m_trianglePipeline);

                vk::Viewport vp(0,0, static_cast<float>(C.frameBufferExtent.width), static_cast<float>(C.frameBufferExtent.height),0,1);
                vk::Rect2D scissor( vk::Offset2D(0,0),  C.frameBufferExtent);
                C.cmd.setViewport(0, vp);
                C.cmd.setScissor(0, scissor);

                    C.cmd.draw(3,1,0,0);

            C.cmd.endRenderPass();

        }
    }
};

class MyApp : public vka::Application
{
    // Application interface
public:
    vka::FrameGraph2 m_framegraph;
    myExecutor       m_exe;

    /**
     * @brief initResources
     *
     * This will be called right after system gets created.
     * It can be used to initialize any vulkan related resources
     * that are not related to the swapchain.
     */
    void initResources() override
    {
        glslang::InitializeProcess();

        // Load the framegraph
        {
            std::ifstream i( VKA_CMAKE_SOURCE_DIR "/src/vka/third_party/vulkanframegraphflow/samples/simpleSwapchain.flow" );
            if( !i )
            {
                throw std::runtime_error("Failed to open flow file");
            }
            nlohmann::json J;
            i >> J;

            m_framegraph.fromJson( m_framegraph.fromJsonFlow2(J) );
        }

    }
    void releaseResources() override
    {
        glslang::FinalizeProcess();
        std::cout << "release non-graphics resources" << std::endl;

        // free everything from the framegraph
        m_framegraph.free();
    }


    /**
     * @brief initSwapChainResources
     *
     * This is called right after the swapchain is created.
     * You can use this to initialize any swapchain related resources,
     * for example, render targets (because they change with swapchain size)
     */
    void initSwapChainResources() override
    {
        this->startThreadPolling();
        std::cout << "init swapchain releated resources" << std::endl;
        auto e = swapchainImageSize();
        vka::FrameGraphResizeInfo r_info;
        r_info.width  = e.width;
        r_info.height = e.height;
        m_framegraph.resize( r_info );
    }
    void releaseSwapChainResources() override
    {
        this->stopThreadPolling();
        std::cout << "release swapchain releated resources" << std::endl;
    }

    void renderFrameGraph(vka::ApplicationFrame & frame)
    {
        vka::FrameGraphExecuteInfo eInfo;
        eInfo.commandBuffer        = frame.currentCommandBuffer;

        // information about the swapchain
        eInfo.swapchainFrameIndex  = frame.currentSwaphainIndex;
        eInfo.swapchainRenderPass  = frame.defaultRenderPass;
        eInfo.swapchainImageView   = frame.currentSwapchainImageView;
        eInfo.swapchainImage       = frame.currentSwapchainImage;
        eInfo.swapchainFramebuffer = frame.currentFrameBuffer;
        eInfo.swapchainExtent      = frame.swapchainImageSize;
        eInfo.swapchainFormat      = frame.swapchainColorFormat;
        eInfo.swapchainClearValues = { { vk::ClearColorValue( std::array<float,4>{1.f,0.f,0.f,1.0f})  } };
        //===========================================================================

        // execute the framegraph
        m_framegraph.execute( m_exe, eInfo);
    }
    /**
     * @brief render
     * @param frame
     *
     * This is the widget's main vulkan render function.
     * use this to actually record your command buffers.
     * submission of commandbuffers are automatically done
     * at the end
     */
    void render(vka::ApplicationFrame & frame) override
    {
        renderFrameGraph(frame);
    }

    void mouseMoveEvent(const vka::EvtInputMouseMotion *e) override
    {
        (void)e;
    }
    void mousePressEvent(const vka::EvtInputMouseButton *e) override
    {
        (void)e;
    }
    void mouseReleaseEvent(const vka::EvtInputMouseButton *e) override
    {
        (void)e;
    }
    void keyPressEvent(const vka::EvtInputKey *e) override
    {
        (void)e;
    }
    void keyReleaseEvent(const vka::EvtInputKey *e) override
    {
        (void)e;
    }
};

#if !defined VKA_NO_SDL_WIDGET
int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;
    // The basic usage of this

    // create a vulkan window widget
    vka::SDLVulkanWidget2 vulkanWindow;

    // create the window and initialize
    // the vka::System
    vulkanWindow.create(1024, 768, debugCallback);


    // instantiate your application
    MyApp app;


    vulkanWindow.exec( &app );


    return 0;
}

#endif
