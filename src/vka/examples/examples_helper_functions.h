#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/GLSLCompiler/GLSLCompiler.h>

#include <vka/core.h>


#include <vka/logging.h>


#include <vka/math.h>

#define WIDTH  1024
#define HEIGHT 768



inline SDL_Window* initWindow()
{
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS );

    if(SDL_Vulkan_LoadLibrary(nullptr) == -1)
    {
        VKA_CRIT("Error loading vulkan");
        exit(1);
    }
    atexit(SDL_Quit);

    auto window = SDL_CreateWindow("APPLICATION_NAME",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        WIDTH,
        HEIGHT,
        SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);

    if(window == nullptr)
    {
        VKA_CRIT("Couldn\'t set video mode: {}", SDL_GetError());
        exit(1);
    }
    return window;
}


VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;
    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}

inline vk::Extent2D getWindowExtent(SDL_Window * window)
{
    int w,h;
    SDL_GetWindowSize(window,&w,&h);
    return vk::Extent2D( static_cast<uint32_t>(w) , static_cast<uint32_t>(h) );
}

inline std::vector<std::string> SDL_GetInstanceExtensions(SDL_Window * window)
{
    std::vector<std::string> ext;

    // We only need to populate the .requiredExtensions array
    // with the values returned by SDL
    unsigned int count = 0;
    SDL_Vulkan_GetInstanceExtensions(window, &count, nullptr);

    const char **names = new const char *[count];
    SDL_Vulkan_GetInstanceExtensions(window, &count, names);

    for(uint32_t i=0; i < count ; i++)
    {
        ext.push_back( names[i] );
    }
    return ext;
}

inline vk::SurfaceKHR SDL_CreateVulkanSurface(vk::Instance instance, SDL_Window * window)
{
    // Create a surface using the SDL function
    vk::SurfaceKHR             surface;
    if( !SDL_Vulkan_CreateSurface( window, instance, reinterpret_cast<VkSurfaceKHR*>(&surface)  ) )
    {
        throw std::runtime_error( "Failed to create surface" );
    }
    return surface;
}

vka::SystemCreateInfo2 _systemCreateInfo;
vk::DebugReportCallbackEXT _callbackObj;
SDL_Window * _window;
vk::SurfaceKHR _surface;

SDL_Window* exampleGetWindow()
{
    return _window;
}

inline void initSystem()
{


    //=======================================================================
    // BOILER PLATE CODE TO GET THE INSTANCE and DEVICE SETUP
    //=======================================================================
    auto window = initWindow(); // create a SDL window
    _window = window;
    //=======================================================================

    //=======================================================================
    // BOILER PLATE CODE TO GET THE INSTANCE and DEVICE SETUP
    //=======================================================================
    // 1. Create an instance using teh vka::InstanceCreateInfo2 struct
    vka::InstanceCreateInfo2 instanceCreateInfo;
    instanceCreateInfo.requiredExtensions  = SDL_GetInstanceExtensions(window);
    instanceCreateInfo.requiredExtensions.push_back( VK_EXT_DEBUG_REPORT_EXTENSION_NAME); // need this for debug callback
    instanceCreateInfo.validationLayers    = {"VK_LAYER_LUNARG_standard_validation"};

    auto instance    = vka::createInstance(instanceCreateInfo);


    _callbackObj = vka::createDebugCallback( instance, debugCallback );

    // 2. Create a surface using the SDL function. We need this first to
    //    figure out if we can draw to this surface.
    vk::SurfaceKHR   surface = SDL_CreateVulkanSurface(instance, window);


    // 3. Create a physical device using the physicalDeviceCreateInfo2 struct
    vka::PhysicalDeviceCreateInfo2 physicalDeviceCreateInfo;
    physicalDeviceCreateInfo.instance = instance;
    physicalDeviceCreateInfo.surface  = surface;
    physicalDeviceCreateInfo.deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME}; // no need for this
                                                                                   // it's automatically set as the default.
    auto physicalDevice = vka::createPhysicalDevice(physicalDeviceCreateInfo);
    assert(physicalDevice);

    // 4. create a logical device using the deviceCreateInfo2 struct
    vka::DeviceCreateInfo2 deviceCreateInfo;
    deviceCreateInfo.physicalDevice = physicalDevice;
    deviceCreateInfo.requiredDeviceExtensions = physicalDeviceCreateInfo.deviceExtensions; //
    deviceCreateInfo.requiredValidationLayers = instanceCreateInfo.validationLayers;
    // 4.1 find appropriate queues that can present graphics to the surface.
    deviceCreateInfo.findAppropriateQueues(surface);
    assert( deviceCreateInfo.graphicsQueueFamily != std::numeric_limits<uint32_t>::max() );
    assert( deviceCreateInfo.presentQueueFamily  != std::numeric_limits<uint32_t>::max() );

    auto device = vka::createDevice(deviceCreateInfo);
    assert(device);

    //=======================================================================
    // END
    //=======================================================================




    // Create all the vulkan objects
    // You do not need to use this function.
    // You can create all the objects yourself
    vka::SystemCreateInfo2 sysCreateInfo;
    sysCreateInfo.device              = device;
    sysCreateInfo.instance            = instance;
    sysCreateInfo.surface             = surface;
    sysCreateInfo.physicalDevice      = physicalDevice;
    sysCreateInfo.queuePresentFamily  = deviceCreateInfo.presentQueueFamily;
    sysCreateInfo.queueGraphicsFamily = deviceCreateInfo.graphicsQueueFamily;
    sysCreateInfo.queuePresent        = device.getQueue( static_cast<uint32_t>(deviceCreateInfo.presentQueueFamily) , 0u );
    sysCreateInfo.queueGraphics       = device.getQueue( static_cast<uint32_t>(deviceCreateInfo.graphicsQueueFamily) , 0u );

    _systemCreateInfo = sysCreateInfo;

    // create the global system object
    vka::System::createSystem( sysCreateInfo );
}

inline void destroySystem()
{
    vka::System::get().destroy();

    // Destroy any left over objects we didn't free
    vka::destroyDebugCallback( _systemCreateInfo.instance, _callbackObj );
    _systemCreateInfo.instance.destroySurfaceKHR(_systemCreateInfo.surface);
    _systemCreateInfo.device.destroy();
    _systemCreateInfo.instance.destroy();
}


