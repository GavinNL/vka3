#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>
#include <thread>
#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/TextureMemoryPool.h>
#include <vka/core/Primatives.h>
#include <vka/utils/HostImage.h>


#include <vka/utils/GLSLCompiler.h>
#include <vka/logging.h>


#define WIDTH  1024
#define HEIGHT 768



SDL_Window* initWindow()
{
    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS );

    if(SDL_Vulkan_LoadLibrary(nullptr) == -1)
    {
        VKA_CRIT("Error loading vulkan");
        exit(1);
    }
    atexit(SDL_Quit);

    auto window = SDL_CreateWindow("APPLICATION_NAME",
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        WIDTH,
        HEIGHT,
        SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);

    if(window == nullptr)
    {
        VKA_CRIT("Couldn\'t set video mode: {}", SDL_GetError());
        exit(1);
    }
    return window;
}


//static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType, uint64_t obj, size_t location, int32_t code, const char* layerPrefix, const char* msg, void* userData)
//{
//    VKA_WARN("**Validation** [{:s}]: {:s}",layerPrefix, msg);

//    return VK_FALSE;
//}

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;
    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}

vk::Extent2D getWindowExtent(SDL_Window * window)
{
    int w,h;
    SDL_GetWindowSize(window,&w,&h);
    return vk::Extent2D( static_cast<uint32_t>(w) , static_cast<uint32_t>(h) );
}

std::vector<std::string> SDL_GetInstanceExtensions(SDL_Window * window)
{
    std::vector<std::string> ext;

    // We only need to populate the .requiredExtensions array
    // with the values returned by SDL
    unsigned int count = 0;
    SDL_Vulkan_GetInstanceExtensions(window, &count, nullptr);

    const char **names = new const char *[count];
    SDL_Vulkan_GetInstanceExtensions(window, &count, names);

    for(uint32_t i=0; i < count ; i++)
    {
        ext.push_back( names[i] );
    }
    return ext;
}

vk::SurfaceKHR SDL_CreateVulkanSurface(vk::Instance instance, SDL_Window * window)
{
    // Create a surface using the SDL function
    vk::SurfaceKHR             surface;
    if( !SDL_Vulkan_CreateSurface( window, instance, reinterpret_cast<VkSurfaceKHR*>(&surface)  ) )
    {
        throw std::runtime_error( "Failed to create surface" );
    }
    return surface;
}

int main(int argc, char ** argv)
{
    (void)argc;
    (void)argv;
    
    glslang::InitializeProcess();

    //=======================================================================
    // BOILER PLATE CODE TO GET THE INSTANCE and DEVICE SETUP
    //=======================================================================
    auto window = initWindow(); // create a SDL window
    //=======================================================================

    //=======================================================================
    // BOILER PLATE CODE TO GET THE INSTANCE and DEVICE SETUP
    //=======================================================================
    // 1. Create an instance using teh vka::InstanceCreateInfo2 struct
    vka::InstanceCreateInfo2 instanceCreateInfo;
    instanceCreateInfo.requiredExtensions  = SDL_GetInstanceExtensions(window);
    instanceCreateInfo.requiredExtensions.push_back( VK_EXT_DEBUG_REPORT_EXTENSION_NAME); // need this for debug callback
    instanceCreateInfo.validationLayers    = {"VK_LAYER_LUNARG_standard_validation"};

    auto instance    = vka::createInstance(instanceCreateInfo);


    auto callbackObj = vka::createDebugCallback( instance, debugCallback );

    // 2. Create a surface using the SDL function. We need this first to
    //    figure out if we can draw to this surface.
    vk::SurfaceKHR   surface = SDL_CreateVulkanSurface(instance, window);


    // 3. Create a physical device using the physicalDeviceCreateInfo2 struct
    vka::PhysicalDeviceCreateInfo2 physicalDeviceCreateInfo;
    physicalDeviceCreateInfo.instance = instance;
    physicalDeviceCreateInfo.surface  = surface;
    physicalDeviceCreateInfo.deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME}; // no need for this
                                                                                   // it's automatically set as the default.
    auto physicalDevice = vka::createPhysicalDevice(physicalDeviceCreateInfo);
    assert(physicalDevice);

    // 4. create a logical device using the deviceCreateInfo2 struct
    vka::DeviceCreateInfo2 deviceCreateInfo;
    deviceCreateInfo.physicalDevice = physicalDevice;
    deviceCreateInfo.requiredDeviceExtensions = physicalDeviceCreateInfo.deviceExtensions; //
    deviceCreateInfo.requiredValidationLayers = instanceCreateInfo.validationLayers;
    // 4.1 find appropriate queues that can present graphics to the surface.
    deviceCreateInfo.findAppropriateQueues(surface);
    assert( deviceCreateInfo.graphicsQueueFamily != std::numeric_limits<uint32_t>::max());
    assert( deviceCreateInfo.presentQueueFamily  != std::numeric_limits<uint32_t>::max());

    auto device = vka::createDevice(deviceCreateInfo);
    assert(device);

    //=======================================================================
    // END
    //=======================================================================




    // Create all the vulkan objects
    // You do not need to use this function.
    // You can create all the objects yourself
    vka::SystemCreateInfo2 sysCreateInfo;
    sysCreateInfo.device              = device;
    sysCreateInfo.instance            = instance;
    sysCreateInfo.surface             = surface;
    sysCreateInfo.physicalDevice      = physicalDevice;
    sysCreateInfo.queuePresentFamily  = deviceCreateInfo.presentQueueFamily;
    sysCreateInfo.queueGraphicsFamily = deviceCreateInfo.graphicsQueueFamily;
    sysCreateInfo.queuePresent        = device.getQueue( static_cast<uint32_t>(deviceCreateInfo.presentQueueFamily) , 0u );
    sysCreateInfo.queueGraphics       = device.getQueue( static_cast<uint32_t>(deviceCreateInfo.graphicsQueueFamily) , 0u );

    // create the global system object
    vka::System::createSystem( sysCreateInfo );

    // get a refernce to the system
    auto & System = vka::System::get();

    auto currentWindowExtent = getWindowExtent(window);
    //=============================================================================
    // The screen buffer encapsulates the surface/swapchain/framebuffers/imageviews
    // and depth textures into a single class for convenience.
    //
    //
    vka::ScreenBufferCreateInfo createInfo;
    createInfo.surface              = surface;
    createInfo.extent               = currentWindowExtent;
    createInfo.verticalSync         = true;
    createInfo.extraSwapchainImages = 1; // request 1 extra swapchain image
    createInfo.maxSwapchainImages   = 3; // up to a max of 3

    auto ScreenB = System.createScreenBuffer( createInfo );
    //=============================================================================


    // create a default command pool
    auto CP = System.createCommandPool();


    auto cb = System.allocateCommandBuffer(vk::CommandBufferLevel::ePrimary, CP);

    auto imageAvailableSemaphore = System.createSemaphore();
    auto renderCompleteSemaphore = System.createSemaphore();



    vk::BufferCreateInfo binfo;
    binfo.setSize(1024*1024*25);
    binfo.setUsage( vk::BufferUsageFlagBits::eVertexBuffer | vk::BufferUsageFlagBits::eIndexBuffer | vk::BufferUsageFlagBits::eTransferSrc);


    auto bPool = System.createBufferPool( binfo, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent);


        // create a box primitive
        auto B = vka::HostTriPrimitive::Box(1,1,1);

        vka::DeviceMeshPrimitive deviceBox = bPool.allocateDeviceMeshPrimitive( B.requiredByteSize(16) );

        // copy the data from the HostMeshPrimitive to the DeviceMeshPrimitive;
        deviceBox.copyFrom(B);



        vka::DescriptorPoolCreateInfo2 I;
        I.maxSets = 1000;
        I.poolSizes.push_back( {vk::DescriptorType::eCombinedImageSampler, 1000 });
        I.poolSizes.push_back( {vk::DescriptorType::eUniformBuffer, 1000 });
        I.poolSizes.push_back( {vk::DescriptorType::eStorageBuffer, 1000 });

        auto dPool = System.createDescriptorPool( I );


        vka::DescriptorSetLayoutCreateInfo2 L;
        L.bindings.push_back( vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eSampler, 1, vk::ShaderStageFlagBits::eFragment | vk::ShaderStageFlagBits::eVertex) );

        vka::DescriptorSetLayoutCreateInfo2 L2;
        L2.bindings.push_back( vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eSampler, 1, vk::ShaderStageFlagBits::eFragment | vk::ShaderStageFlagBits::eVertex) );

        auto l1 = System.createDescriptorSetLayout(L);
        auto l2 = System.createDescriptorSetLayout(L2);

        assert(l1 == l2);

        auto s1 = System.allocateDescriptorSet( dPool, l1 );
        auto s2 = System.allocateDescriptorSet( dPool, l2 );

        (void)s1;
        (void)s2;

        vk::Pipeline trianglePipeline;


        {
            // use this to render to the swapchain
            vka::GraphicsPipelineCreateInfo4 P;


            auto compileShaderModule = [&](const std::string & path, EShLanguage language)
            {
                vka::GLSLCompiler compiler;
                auto spv   = compiler.compileFile(path, language  );

                vka::ShaderModuleCreateInfo2 _createInfo;
                _createInfo.code = spv;
                return System.createShaderModule(_createInfo);
            };

            P.vertexShader   = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/triangle.vert", EShLangVertex);
            P.fragmentShader = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/triangle.frag", EShLangFragment);
            P.renderPass     = ScreenB.getDefaultRenderPass();
            trianglePipeline = System.createGraphicsPipeline(P);

        }


   auto TP =  System.createTexturePool( 50*1024*1024,
                              vk::Format::eR8G8B8A8Unorm,
                              vk::MemoryPropertyFlagBits::eDeviceLocal,
                              vk::ImageUsageFlagBits::eSampled
                              | vk::ImageUsageFlagBits::eTransferSrc
                              | vk::ImageUsageFlagBits::eTransferDst);

   auto nI = TP.createNewImage2D( vk::Format::eR8G8B8A8Unorm, vk::Extent2D(1024,1024), 1, 9);



   {
       vka::HostImage img;
       img.resize(1024, 1024);

       // Generate an image by applying a function to each channel
       img.r = [](float u, float v){ return 0.5f+0.5f*std::cos( 3.0f*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
       img.b = [](float u, float v){ return 0.5f+0.5f*std::sin( 3.0f*2*3.14159f*std::hypot(u,v-1)) ;}; // full red texture;
       img.a = 255; // full red texture;



       auto imgSubBuffer = bPool.allocate( img.size() );
       void * m = imgSubBuffer.getMappedMemory();

       std::memcpy( m,  img.data(), img.size() );



       auto cb1 = System.allocateCommandBuffer(vk::CommandBufferLevel::ePrimary, CP);


       cb1.begin( vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse) );

       vka::BufferSubImageCopyToImage D;
       D.srcBufferRegion.offset = vk::Offset2D(0,0);
       D.srcBufferRegion.extent = vk::Extent2D(1024,1024);
       D.srcBufferImageExtent   = vk::Extent2D(1024,1024);

       D.dstImageOffset = vk::Offset2D(0,0);
       D.mip = 0;
       D.layer = 0;

       cb1.copyBufferToImageAndConvert(imgSubBuffer, nI, D);


        cb1.generateMipMaps(nI, 0);

       cb1.end();

       {
           vk::SubmitInfo info;
           info.waitSemaphoreCount   = 0;
           info.pWaitSemaphores      = &imageAvailableSemaphore;
           info.signalSemaphoreCount = 0;
           info.pSignalSemaphores    = &renderCompleteSemaphore;
           info.commandBufferCount   = 1;
           info.pCommandBuffers      = &cb1;

           vk::PipelineStageFlags wait_stage = vk::PipelineStageFlagBits::eHost;
           info.pWaitDstStageMask = &wait_stage;

           System.submitCommandBuffers(info);
       }
   }


    SDL_Event event;
    bool quit = false;
    while(!quit)
    {
        std::this_thread::sleep_for( std::chrono::milliseconds(1));

        while (SDL_PollEvent(&event))
        {
            if( event.type == SDL_QUIT ) // User pressed the x button.
            {
                quit = true;
            }
            if (event.type == SDL_MOUSEBUTTONDOWN)
            {
                // we have pressed a mouse button
            }
            if (event.type == SDL_MOUSEMOTION)
            {
                // we have moved the mouse cursor
            }
        }

        cb.reset(vk::CommandBufferResetFlagBits::eReleaseResources);
        cb.begin( vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse) );

            auto frameBufferIndex = ScreenB.getNextFrameIndex(imageAvailableSemaphore);


            // set the screens clear color
            ScreenB.setClearColor( 1,1,0,1 );

            // do not "copy" this RenderPassBeginInfo, since it has
            // pointers to data that is stored in the ScreenB.
            vk::RenderPassBeginInfo RPBI = ScreenB.getRenderPassBeginInfo();

            cb.beginRenderPass(RPBI, vk::SubpassContents::eInline);

                cb.bindPipeline( vk::PipelineBindPoint::eGraphics, trianglePipeline);

                cb.bindDeviceMeshPrimitive(deviceBox, true);
                vk::Viewport vp(0,0, static_cast<float>(currentWindowExtent.width), static_cast<float>(currentWindowExtent.height) ,0,1);
                vk::Rect2D scissor( vk::Offset2D(0,0), currentWindowExtent);
                cb.setViewport(0, vp);
                cb.setScissor(0, scissor);
                cb.draw(3,1,0,0);

                // Rendering code goes here.


           cb.endRenderPass();

        cb.end();

        {
            vk::SubmitInfo info;
            info.waitSemaphoreCount   = 1;
            info.pWaitSemaphores      = &imageAvailableSemaphore;
            info.signalSemaphoreCount = 1;
            info.pSignalSemaphores    = &renderCompleteSemaphore;
            info.commandBufferCount   = 1;
            info.pCommandBuffers      = &cb;

            vk::PipelineStageFlags wait_stage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
            info.pWaitDstStageMask = &wait_stage;

            System.submitCommandBuffers(info);
        }

        try {

            ScreenB.presentFrameIndex(frameBufferIndex, renderCompleteSemaphore);

        }
        catch (...)
        {
            currentWindowExtent = getWindowExtent(window);

            vka::ScreenBufferCreateInfo _createInfo;
            _createInfo.surface              = surface;
            _createInfo.extent               = currentWindowExtent;
            _createInfo.verticalSync         = true;
            _createInfo.extraSwapchainImages = 1; // request 1 extra swapchain image
            _createInfo.maxSwapchainImages   = 3; // up to a max of 3

            ScreenB = System.createScreenBuffer(_createInfo, &ScreenB);

        }

    }

    System.destroyBufferPool(bPool);
    System.destroyTexturePool( TP);

    // destroy the screen buffer
    System.destroyScreenBuffer(ScreenB);

    // Destroy any left over objects we didn't free
    System.destroy();

    vka::destroyDebugCallback( instance, callbackObj );
    instance.destroySurfaceKHR(surface);
    device.destroy();
    instance.destroy();

    glslang::FinalizeProcess();
    return 0;
}


