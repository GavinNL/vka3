cmake_minimum_required(VERSION 3.10)




set(CMAKE_INCLUDE_CURRENT_DIR ON)

set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)


find_package(Qt5 COMPONENTS Widgets REQUIRED)

add_executable(qt_test main.cpp qt_src/qt_mainwindow.cpp)

target_link_libraries( qt_test
                            PUBLIC
                                vka::vka
                                vka::basisu
                                Qt5::Widgets
                                vka_project_warnings)


target_compile_definitions( qt_test
                          PUBLIC
                          WINDOW_MANAGER=0)

return()

