#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/GLSLCompiler/GLSLCompiler.h>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/TextureMemoryPool.h>
#include <vka/core/HostTriMesh.h>
#include <vka/core/HostImage.h>

#include <vka/logging.h>


#include <vka/math.h>

#include "examples_helper_functions.h"

#include <vka/Renderers/TexturePresentRenderer.h>
#include <vka/Renderers/PBRModelRenderer.h>

#define WIDTH  1024
#define HEIGHT 768


int main(int argc, char ** argv)
{
    (void)argc;
    (void)argv;
    glslang::InitializeProcess();

    initSystem();

    // get a refernce to the system
    auto & System = vka::System::get();



    //==========================================================================================
    // Initialize all the resource pools we want to use
    //==========================================================================================
    // create a default command pool
    auto commandPool = System.createCommandPool();


    // Staging Pool and Unifforms
    auto bufferPoolStaging = System.createBufferPool( 25*1024*1024,
                                                      vk::BufferUsageFlagBits::eVertexBuffer
                                                         | vk::BufferUsageFlagBits::eIndexBuffer
                                                         | vk::BufferUsageFlagBits::eUniformBuffer
                                                         | vk::BufferUsageFlagBits::eStorageBuffer
                                                         | vk::BufferUsageFlagBits::eTransferSrc,
                                                      vk::MemoryPropertyFlagBits::eHostVisible
                                                         | vk::MemoryPropertyFlagBits::eHostCoherent);

    auto bufferPoolAttributes = System.createBufferPool( 25*1024*1024,
                                                      vk::BufferUsageFlagBits::eVertexBuffer
                                                         | vk::BufferUsageFlagBits::eIndexBuffer
                                                         | vk::BufferUsageFlagBits::eTransferDst,
                                                      vk::MemoryPropertyFlagBits::eDeviceLocal );

    auto texturePool =  System.createTexturePool( 50*1024*1024,
                               vk::Format::eR8G8B8A8Unorm,
                               vk::MemoryPropertyFlagBits::eDeviceLocal,
                               vk::ImageUsageFlagBits::eSampled
                               | vk::ImageUsageFlagBits::eTransferSrc
                               | vk::ImageUsageFlagBits::eTransferDst);

    auto depthPool =  System.createTexturePool( 50*1024*1024,
                               vk::Format::eD32Sfloat,
                               vk::MemoryPropertyFlagBits::eDeviceLocal,
                               vk::ImageUsageFlagBits::eSampled |vk::ImageUsageFlagBits::eDepthStencilAttachment);


    size_t
    colorAttachmentSize = WIDTH*HEIGHT*
                           4*4 // 4 components, 4 bytes per pixel;
                           *3; // 3 color attachments

    auto attachmentPool  =  System.createTexturePool( colorAttachmentSize,
                                                      vk::Format::eR32G32B32A32Sfloat,
                                                      vk::MemoryPropertyFlagBits::eDeviceLocal,
                                                      vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eColorAttachment);


    vka::DescriptorPoolCreateInfo2 I;
    I.maxSets = 1000;
    I.poolSizes.push_back( {vk::DescriptorType::eCombinedImageSampler, 1000 });
    I.poolSizes.push_back( {vk::DescriptorType::eUniformBuffer, 1000 });
    I.poolSizes.push_back( {vk::DescriptorType::eStorageBuffer, 1000 });

    auto descriptorPool = System.createDescriptorPool( I );



    //===============================================================================
    // Initialize the RenderTargets
    //===============================================================================
    auto depthTexture = depthPool.createNewImage2D( vk::Format::eD32Sfloat, {WIDTH,HEIGHT}, 1, 1);

    vka::ScreenBufferCreateInfo createInfo;
    createInfo.surface                    = System.getCreateInfo().surface;
    createInfo.extent                     = getWindowExtent( exampleGetWindow() );
    createInfo.verticalSync               = true;
    createInfo.extraSwapchainImages       = 1; // request 1 extra swapchain image
    createInfo.maxSwapchainImages         = 3; // up to a max of 3
    //createInfo.depthStencilFormatOrImage  = depthTexture;

    auto ScreenB = System.createScreenBuffer( createInfo );


    auto positionAttachment = attachmentPool.createNewImage2D( vk::Format::eR32G32B32A32Sfloat, {WIDTH, HEIGHT}, 1, 1);
    auto normalAttachment   = attachmentPool.createNewImage2D( vk::Format::eR32G32B32A32Sfloat, {WIDTH, HEIGHT}, 1, 1);
    auto albedoAttachment   = attachmentPool.createNewImage2D( vk::Format::eR8G8B8A8Unorm,      {WIDTH, HEIGHT}, 1, 1);
    vka::RenderTargetCreateInfo rtCI;
    rtCI.colorTargets = {albedoAttachment, positionAttachment, normalAttachment };
    rtCI.depthTarget  = depthTexture;

    auto gBufferTarget = System.createRenderTarget(rtCI);
    //===============================================================================



    //==========================================================================================
    // LAMBDA function to upload a HostMeshPrimitive directly to the
    // attribute buffer
    //==========================================================================================
    auto uploadMesh = [&]( vka::HostTriPrimitive const & B)
    {
        uint32_t alignment=16;

        // create a box primitive
        //auto B = vka::boxPrimitive(1,1,1);

        // allocate a a MeshPrimitive from the staging pool.
        vka::DeviceMeshPrimitive stagingBox = bufferPoolStaging.allocateDeviceMeshPrimitive( B.requiredByteSize(alignment) );

        // copy the data from the HostMeshPrimitive to the DeviceMeshPrimitive;
        stagingBox.copyFrom(B,alignment);



        // allocate a mesh primitive from the device local pool
        auto devBox  = bufferPoolAttributes.allocateDeviceMeshPrimitive( B.requiredByteSize(alignment) );


        auto cpyBuff = System.allocateCommandBuffer(vk::CommandBufferLevel::ePrimary, commandPool);

        cpyBuff.reset(vk::CommandBufferResetFlagBits::eReleaseResources);
        cpyBuff.begin( vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse) );

        cpyBuff.copyDeviceMeshPrimitive(stagingBox, devBox);

        cpyBuff.end();

        System.submitCommandBuffers( cpyBuff, nullptr, nullptr, nullptr);
        bufferPoolStaging.free(stagingBox);

        return devBox;
    };

    auto uploadImage = [&]( vka::HostImage const & _img)
    {
        auto imgSubBuffer = bufferPoolStaging.allocate( _img.size() );

        imgSubBuffer.copyData( _img.data(), _img.size());

        auto w = _img.width();
        auto h = _img.height();
        auto m =  uint32_t( std::log2( std::min(w,h) ) );

        auto outImg = texturePool.createNewImage2D( vk::Format::eR8G8B8A8Unorm, vk::Extent2D(w,h), 1, m);

        auto cb1 = System.allocateCommandBuffer(vk::CommandBufferLevel::ePrimary, commandPool);


        cb1.begin( vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eOneTimeSubmit) );

            cb1.waitForHostTransfers();

            vka::BufferSubImageCopyToImage D;
            D.srcBufferRegion.offset = vk::Offset2D(0,0);
            D.srcBufferRegion.extent = vk::Extent2D(w,h);
            D.srcBufferImageExtent   = vk::Extent2D(w,h);

            D.dstImageOffset = vk::Offset2D(0,0);
            D.mip = 0;
            D.layer = 0;

            cb1.copyBufferToImageAndConvert(imgSubBuffer, outImg, D);


            cb1.generateMipMaps(outImg, 0);

        cb1.end();

        System.submitCommandBuffers(cb1, nullptr, nullptr, nullptr );

        bufferPoolStaging.free(imgSubBuffer);

        return outImg;
    };
    //==========================================================================================


    //==========================================================================================
    // create some semaphores
    //==========================================================================================
    auto imageAvailableSemaphore = System.createSemaphore();
    auto renderCompleteSemaphore = System.createSemaphore();


    //vka::DeviceMeshPrimitive deviceBox = uploadMesh( vka::HostTriPrimitive::Box(1,1,1));
    vka::DeviceMeshPrimitive deviceBox = uploadMesh( vka::HostTriPrimitive::TestSphere(0.5));


    vka::HostImage img;
    img.resize(1024, 1024);

    //// Generate an image by applying a function to each channel
    img.r = [](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
    img.g = 0;//[](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
    img.b = [](float u, float v){ return 0.5f+0.5f*std::sin( 3*2*3.14159f*std::hypot(u,v-1)) ;}; // full red texture;
    img.a = 255; // full red texture;

    auto nI = uploadImage(img);





    auto matrixBuffer   = bufferPoolStaging.allocate(1024*1024);
    auto materialBuffer = bufferPoolStaging.allocate(1024*1024);
    auto uniformBuffer  = bufferPoolStaging.allocate(1024);


    //======================================================================
    // Creating the PBR renderer.
    //
    // The PBR renderer works as follows
    // 1 Host Visible Uniform Buffer to be used as GLOBAL data
    //    - this global data stores things like the window size, mouse positions
    //      camera view and projection matrices.
    //
    // 2 Host Visible Storage Buffers, one for matrices and one for materials
    //
    // 1 array of textures of size 1024.
    //
    //
    // Prior to recording any commands using the PBR renderer, you have to
    // push all the textures you are going to use for this frame
    // into the renderer. This must be done BEFORE you call beginCommandBuffer
    // Once that is done, you can call the updateDescriptors() method.
    // this will add all the textures you are going to use
    // into the shader's descriptor.
    //======================================================================
    vka::PBRPrimitiveRenderer PBR;

    {
        vka::StorageDescriptor_t<glm::mat4> sd_mat;
        vka::StorageDescriptor_t<vka::PBRPrimitiveRenderer::Material_t> sd_material;


        sd_mat.init(      matrixBuffer,   0 , static_cast<uint32_t>(matrixBuffer.getSize()) );
        sd_material.init( materialBuffer, 0 , static_cast<uint32_t>(matrixBuffer.getSize()) );



        vka::PBRPrimitiveRenderer::CreateInfo PBRcreate;

        PBRcreate.baseVertexShaderPath    = VKA_CMAKE_SOURCE_DIR "/share/shaders/renderer/scene_pbr_base.vert";
        PBRcreate.baseFragmentShaderPath  = VKA_CMAKE_SOURCE_DIR "/share/shaders/renderer/scene_pbr_base.frag";
        PBRcreate.concurrentFrames      = static_cast<uint32_t>(ScreenB.m_frameBuffers.size());
        PBRcreate.descriptorPool        = descriptorPool;
        PBRcreate.screenSize            = getWindowExtent(_window);
        PBRcreate.matrixStorage         = sd_mat;   // Storage Buffer Used for storing matrice
        PBRcreate.materialStorage       = sd_material; // Storage Buffer used for storing materials
        PBRcreate.globalUniformBuffer   = uniformBuffer;  // Uniform Buffer used for storing global
        PBRcreate.nullImage             = nI; // a null image which will be used
        PBRcreate.maxTextures           = 64;

        // Enable writing to the position and normal colour target
        PBRcreate.renderPass            = gBufferTarget.getDefaultRenderPass();
        PBRcreate.albedoOutput          = albedoAttachment;
        PBRcreate.positionOutput        = positionAttachment;
        PBRcreate.normalOutput          = normalAttachment;
        // if there are no textures. Must have at least 1 textures

        PBR.init(PBRcreate);
    }
    //======================================================================



    //======================================================================
    //======================================================================
    TexturePresentRenderer TPR;
    {
        TexturePresentRenderer::CreateInfo TPRcreate;
        TPRcreate.concurrentFrames  = static_cast<uint32_t>(ScreenB.m_frameBuffers.size());
        TPRcreate.renderPass     = ScreenB.getDefaultRenderPass();
        TPRcreate.descriptorPool = descriptorPool;
        TPRcreate.screenSize     = getWindowExtent(_window);
        TPR.init(TPRcreate);
        TPR.setImageSize(2,2);
        TPR.setImagePosition(-1,-1);
        TPR.setImage( albedoAttachment ); // present the albdo image
    }
    //======================================================================



    std::vector<vka::transform>                   allTransforms;
    for(int i=0;i<5;i++)
    {
        vka::transform T;
        T.m_position.x =  static_cast<float>(i);

        for(int j=0;j<5;j++)
        {
            T.m_position.y =  static_cast<float>(j);
            allTransforms.push_back(T);
        }
    }


    const float AR = WIDTH / ( float )HEIGHT;
    auto cameraProjectionMatrix = glm::perspective( glm::radians(45.0f), AR, 0.1f, 30.0f);
    cameraProjectionMatrix[1][1] *= -1;

    auto cameraViewMatrix       = glm::mat4();


    vka::DescriptorWriter dWriter;
    SDL_Event event;
    bool quit = false;

    float t=0.0f;
    auto cb = System.allocateCommandBuffer(vk::CommandBufferLevel::ePrimary, commandPool);
    while(!quit)
    {
        // Before we begin writing any command buffers
        // we have to add all the textures we plan to use for this
        // frame.
        {
            PBR.clearFrameTextures();
            PBR.addFrameTexture( nI );
        }

        // Update all the descriptors for this frame
        // This is where all the textures will be boudn
        // to the descriptor set. After this happens,
        // you cannot update the sets
        {

            PBR.updateDescriptorSets(dWriter);
            TPR.updateDescriptorSets(dWriter);

            if( dWriter.writeDescriptorSet.size() )
                System.updateDescriptorSets( dWriter.writeDescriptorSet );

            dWriter.clear();
        }

        t+= 0.025f;


        while (SDL_PollEvent(&event))
        {
            if( event.type == SDL_QUIT ) // User pressed the x button.
            {
                quit = true;
            }
            if (event.type == SDL_MOUSEBUTTONDOWN)
            {
                // we have pressed a mouse button
            }
            if (event.type == SDL_MOUSEMOTION)
            {
                // we have moved the mouse cursor
            }
        }


        cb.reset(vk::CommandBufferResetFlagBits::eReleaseResources);
        cb.begin( vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse) );

            auto frameBufferIndex = ScreenB.getNextFrameIndex(imageAvailableSemaphore);

            // set the screens clear color
            ScreenB.setClearColor( 1,0,0,1 );



            // do not "copy" this RenderPassBeginInfo, since it has
            // pointers to data that is stored in the ScreenB.
            vk::RenderPassBeginInfo RPBI = gBufferTarget.getRenderPassBeginInfo();
            cb.beginRenderPass(RPBI, vk::SubpassContents::eInline);

            {

                //======================================================================
                // Using the PBR renderer.
                //
                //======================================================================
                // These two calls reset the matrices and materials
                // storage buffer, so that any new materials/materials
                // will now be pushed into the 0th index.
                // If you do not want to keep uploading the matrices/materials each frame
                // you can upload them once, and keep track of the index
                //
                // You can either call PBR.resetXXXXX() or
                // you can reset the individual StorageDescriptors manually.
                // Reset the storage descriptors manually if you plan on
                // sharing the storage descriptors with other renderers
                PBR.resetMatrices();
                PBR.resetMaterials();

                // Since we have called PBR.resetMatrices() we will
                // need to upload the projection and view matrices
                // we only need to do this once.
                // since all models will be rendered with the same matrix
                PBR.setCameraMatrices( cameraViewMatrix, cameraProjectionMatrix);

                // call begin to bind pipeline and any descriptor sets that it might be
                // using
                PBR.begin(cb);


                // bind the mesh primitive to the command buffer
                PBR.bindDeviceMeshPrimitive( deviceBox );

                // We're going to create a vector of materials,
                // one for each object instance we are going to draw
                std::vector<vka::PBRPrimitiveRenderer::Material_t> materials;
                int32_t i=0;
                for(auto & T : allTransforms)
                {
                    vka::PBRPrimitiveRenderer::Material_t material2;
                    material2.baseColorFactor  = {T.m_position.x/5.0f,
                                                  T.m_position.y/5.0f,
                                                  1,
                                                  1};

                    if( i%2 == 0)
                        material2.baseColorTexture = PBR.findFrameTextureIndex(nI);

                    materials.push_back(material2);
                    i++;
                    T.rotate( {0,1,0}, 0.025f);
                }


                // Push all the transforms into the storage buffer
                // it will return the index of the first inserted
                // matrix. This will be used to tell the
                // PBR renderer where to look for the matrices when
                // we draw multiple instances.
                std::vector<glm::mat4> allMatrices;
                for(auto & tr : allTransforms)
                    allMatrices.push_back(tr.get_matrix());

                //======================================================================
                // There are two types of "model matrices" that the PBR renderer uses
                // the modelMatrix and the nodeMatrix.
                // the nodeMatrix is what you should use if you want to draw instances
                // The final vertex output is equal to:
                //
                //  v_out = PROJ * VIEW * MODEL * NODE * v_in
                //
                //======================================================================
                vka::transform modelTransform;
                modelTransform.m_position = {-2.5,-2.5,-10.0f};
                int32_t modelMatrixIndex   = static_cast<int32_t>( PBR.pushMatrix( modelTransform.get_matrix() ) );
                //======================================================================

                //======================================================================
                // Push all the matrices for each node onto the storage buffer
                //======================================================================
                int32_t firstNodeMatrixIndex = PBR.pushMatrices(allMatrices);
                //======================================================================

                //======================================================================
                // Push all the materials for the objects int he the storage buffer
                //======================================================================
                int32_t firstMaterial        = static_cast<int32_t>( PBR.pushMaterials(materials) );
                //======================================================================

                // Set the model index. We will use this to
                // transform all instances of the nodes
                PBR.setModelMatrixIndex(modelMatrixIndex);

                // draw the same primitive twice, using
                // consequitively pushed matrices/materials
                // use this if you have multiple
                PBR.drawPrimitiveInstanced(firstNodeMatrixIndex, 1, firstMaterial, static_cast<uint32_t>(allTransforms.size()) );

                PBR.end();

            }

            cb.endRenderPass();

            // wait for all the PBR's colour attachments to be written
            // before we continue with any using of the fragment shaders
            cb.simplePipelineBarrier(vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                     vk::PipelineStageFlagBits::eFragmentShader);

            {
                vk::RenderPassBeginInfo _RPBI = ScreenB.getRenderPassBeginInfo();
                cb.beginRenderPass(_RPBI, vk::SubpassContents::eInline);

                TPR.write(cb);

                cb.endRenderPass();
            }



        cb.end();

        System.submitCommandBuffers( {cb},
                                     {imageAvailableSemaphore},
                                     {vk::PipelineStageFlagBits::eColorAttachmentOutput},
                                     {renderCompleteSemaphore} );

        ScreenB.presentFrameIndex(frameBufferIndex, renderCompleteSemaphore);
    }

    PBR.destroy();
    TPR.destroy();

    System.destroyBufferPool(bufferPoolStaging);
    System.destroyBufferPool(bufferPoolAttributes);

    // destroy the screen buffer. This needs to be done
    // before the texture pools
    System.destroyScreenBuffer(ScreenB);
    System.destroyRenderTarget(gBufferTarget);

    System.destroyTexturePool(texturePool);
    System.destroyTexturePool(depthPool);


    destroySystem();


    glslang::FinalizeProcess();
    return 0;
}


