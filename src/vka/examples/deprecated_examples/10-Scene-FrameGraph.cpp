#include <iostream>
#include <queue>
#include <tuple>

#include <vka/core.h>

#include <vka/utils/SceneLoader.h>

#include <future>

#include <vka/logging.h>


#include <vka/math.h>

#include <vka/GLSLCompiler/GLSLCompiler.h>

#include <vka/Renderers/PBRSceneRenderer.h>
#include <vka/Renderers/TexturePresentRenderer.h>
#include <vka/Renderers/PostProcessRenderer.h>
#include <vka/Renderers/PBRLightPass.h>
#include <vka/Renderers/SimpleRenderer.h>
#include <vka/Renderers/Uploaders.h>

#include <vka/utils/Camera.h>
#include <vka/Controllers/FPSController.h>
#include <vka/Controllers/OrbitController.h>
#include <vka/Controllers/GizmoController.h>

#include <vka/utils/FrameGraph.h>
#include <vka/utils/SpecialPipeline.h>
#include <vka/Renderers/StorageDescriptor.h>

#define WIDTH  1024
#define HEIGHT 768


#ifndef VKA_NO_SDL_WIDGET
#include <vka/widgets/SDLWidget.h>
#endif

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;
    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}


class MyApp : public vka::Application
{
    // Application interface
public:

    //==========================================================================
    // Resources Pools
    //==========================================================================
    vka::BufferPool   m_bufferPoolStaging;
    vka::BufferPool   m_bufferPoolUniform;
    vka::BufferPool   m_bufferPoolAttributes;
    vka::BufferPool   m_bufferPoolAttributesHostVisible;

    vka::TexturePool  m_texturePool;

    vk::DescriptorPool m_descriptorPool;

    //==========================================================================
    // Resources
    //==========================================================================
    vk::ImageView     m_nullImage;

    //==========================================================================

#define SCENE_RENDERER_ALLOCATE_OWN_RESOURCES



    //==========================================================================
    // Renderers
    //==========================================================================
    // resources needed for the SceneRenderer
#if !defined SCENE_RENDERER_ALLOCATE_OWN_RESOURCES
    vka::SubBuffer                                             m_pbr_matrixStorageBuffer;
    vka::SubBuffer                                             m_pbr_materialStorageBuffer;
    vka::SubBuffer                                             m_pbr_uniformBuffer;
    StorageDescriptor_t<glm::mat4>                             m_pbr_matrixStorage;
    StorageDescriptor_t<vka::PBRPrimitiveRenderer::Material_t> m_pbr_materialStorage;
#endif

    //==========================================================================

    vka::FrameGraph m_frameGraph;

    vka::StorageDescriptor_t<glm::mat4> m_matrixStorage;

    vka::SpecialPipeline     m_gbufferPipeline;
    vka::SpecialPipeline     m_lightPipeline;
    //==========================================================================
    // Helper Classes
    //==========================================================================
    vka::PrimitiveUploader m_primtiveUploader;
    vka::ImageUploader     m_imageUploader;
    //==========================================================================


    //==========================================================================
    //
    //==========================================================================


    struct PrimitiveObject
    {
        vka::HostTriPrimitive                   host;      // the acutal mesh data which will be stored on the host

        std::optional<vka::DeviceMeshPrimitive> device;    // the device object to use once the data has been copieed.
    };

    std::vector<PrimitiveObject> m_primitives; // a list of all primitives

    struct Entity
    {
        uint32_t       primIndex;  // which primitive in the m_primitives to use?
        vka::transform transform;  // where should we draw the entity
    };


    std::vector<Entity> m_entities; // a list of all entities that we will be drawing


    // This structure will be used as indices into the Matrix Storage buffer
    // each instance of a PrimitiveObject that will be drawn will have a
    // unique PrimRenderInfo.  In the shader, we will index into the storage
    // buffer using the gl_InstanceIndex variable.
    // once we have the PrimRenderInfo, we will use the indices listed
    // in it to index into the Matrix Storage Buffer to get all the individual
    // matrices.
    struct PrimRenderInfo
    {
        int32_t viewMatrixIndex  = 0;
        int32_t projMatrixIndex  = 0;
        int32_t worldMatrixIndex = 0;
        int32_t nodeMatrixIndex  = 0;
    };
    //==========================================================================


    /**
     * @brief initResources
     *
     * This will be called right after system gets created.
     * It can be used to initialize any vulkan related resources
     * that are not related to the swapchain.
     */
    void initResources() override
    {


        addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");

        glslang::InitializeProcess();


        //m_lightTransform.lookat( { 1, 1, 1}, {0,1,0});
        //m_lightTransform.m_position = {-3,5,-3};
        m_camera.transform.m_position = {5,5,5};
        m_camera.transform.lookat({0,0,0},{0,1,0});

        {
            auto & System = vka::System::get();
            // Staging Pool and Unifforms
             m_bufferPoolStaging = System.createBufferPool( 25*1024*1024,
                                                              vk::BufferUsageFlagBits::eTransferSrc,
                                                              vk::MemoryPropertyFlagBits::eHostVisible
                                                                 | vk::MemoryPropertyFlagBits::eHostCoherent);

             m_bufferPoolUniform = System.createBufferPool( 10*1024*1024,
                                                              vk::BufferUsageFlagBits::eUniformBuffer
                                                                 | vk::BufferUsageFlagBits::eStorageBuffer
                                                                 | vk::BufferUsageFlagBits::eTransferSrc,
                                                              vk::MemoryPropertyFlagBits::eHostVisible
                                                                 | vk::MemoryPropertyFlagBits::eHostCoherent);

             m_bufferPoolAttributesHostVisible = System.createBufferPool( 5*1024*1024,
                                                                          vk::BufferUsageFlagBits::eVertexBuffer
                                                                              | vk::BufferUsageFlagBits::eIndexBuffer
                                                                              | vk::BufferUsageFlagBits::eTransferDst,
                                                                          vk::MemoryPropertyFlagBits::eHostVisible
                                                                             | vk::MemoryPropertyFlagBits::eHostCoherent);

            m_bufferPoolAttributes = System.createBufferPool( 25*1024*1024,
                                                             vk::BufferUsageFlagBits::eVertexBuffer
                                                                 | vk::BufferUsageFlagBits::eIndexBuffer
                                                                 | vk::BufferUsageFlagBits::eTransferDst,
                                                              vk::MemoryPropertyFlagBits::eDeviceLocal );

            m_texturePool =  System.createTexturePool( 50*1024*1024,
                                       vk::Format::eR8G8B8A8Unorm,
                                       vk::MemoryPropertyFlagBits::eDeviceLocal,
                                       vk::ImageUsageFlagBits::eSampled
                                       | vk::ImageUsageFlagBits::eTransferSrc
                                       | vk::ImageUsageFlagBits::eTransferDst);



            vka::DescriptorPoolCreateInfo2 I;
            I.maxSets = 1000;
            I.poolSizes.push_back( {vk::DescriptorType::eCombinedImageSampler, 1000 });
            I.poolSizes.push_back( {vk::DescriptorType::eUniformBuffer, 1000 });
            I.poolSizes.push_back( {vk::DescriptorType::eStorageBuffer, 1000 });

            m_descriptorPool = System.createDescriptorPool( I );
        }


       vka::HostImage img;
       img.resize(1024, 1024);

       //// Generate an image by applying a function to each channel
       img.r = [](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
       img.g = 0;//[](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
       img.b = [](float u, float v){ return 0.5f+0.5f*std::sin( 3*2*3.14159f*std::hypot(u,v-1)) ;}; // full red texture;
       img.a = 255; // full red texture;


       m_nullImage  = m_imageUploader.uploadImage(img, m_texturePool);


       buildRenderGraph();

       m_primitives.resize(2);
       m_primitives[0].host = vka::HostTriPrimitive::Box( {1,1,1} );

       Entity e;
       e.primIndex = 0;
       e.transform.m_position = {0,0,0};
       m_entities.push_back(e);

       e.transform.m_position = {0,2,0};
       m_entities.push_back(e);
    }




    void buildRenderGraph()
    {
        auto & graph = m_frameGraph;

        // Add a HostTask. A HostTask is meant to be used to perform
        // a process on the host. For example, finding all entities that
        // intersect the frustum.
        // The HostTask will produce a "visibleEntities" resource
        // that will exist on the Host.
        graph.addTask("frustumCulling")
              // input host resource
             .addHostResourceInput("frameTime")   // no tasks create this. must be created outside the graph.
              // output host resource
             .addHostResourceOutput("entityList") // list of entities to draw
             .setCommandBufferWrite(
                          [&](vka::RenderTaskVar & c)
                          {
                             (void)c;

                             // The frustrumCulling task performs the following operations:
                             //  - determines which drawable objects are are within the view frstum
                             //  - determine which drawable objects are NOT loaded into the GPU
                             //     -
                             auto & entityList         = c.emplaceResource<  std::vector<uint32_t>      >("entityList");
                             entityList.clear();

                             // Using variables;
                             //  ResourceOutputs MUST be created or emplaced by the task
                             //  that listed it as an output resource

                             //  Other varaibles
                             //  These can be set outside of the main graph
                             //  loop, but they need to be created
                             if( c.graph->isHostResourceCreated("frameTime"))
                             {
                                 auto & dt = c.getHostResource<float>("frameTime");
                                 (void)dt;
                             }


                             // loop through all entities
                             uint32_t e=0;
                             for(auto & E : m_entities)
                             {
                                 auto & pr = m_primitives[ E.primIndex ];

                                 if( !pr.device )
                                 {
                                      // the host object is not loaded into the GPU, so schedule it for load
                                      pr.device = m_primtiveUploader.upload( pr.host, m_bufferPoolAttributes);
                                 }
                                 else
                                 {
                                     entityList.push_back( e );
                                 }
                                 ++e;
                             }

                             // this part should really be in a separate place
                             // probably somewhere in the "rendering"

                          });


        //=====================================================================
        // This is the bufferTransfer task. It's job is to
        // transfer data from the host to vertex/uniform buffers
        // the HostTask will be
        //=====================================================================
        graph.addTask("bufferTransfer")
             // input hostResource
             .addHostResourceInput("entityList")               // list of entities to draw, only used as a dependency
             // output host resource
             .addHostResourceOutput("allocatedStagingBuffers") // list of staging buffers that ahve been allocated.
             .setCommandBufferWrite(
                         [&](vka::RenderTaskVar & C)
                         {
                              auto cmd = C.commandBuffer;
                              auto & allocatedBuffers = C.getOrEmplaceResource< std::vector<vka::SubBuffer> >("allocatedStagingBuffers");
                              for(auto & b : allocatedBuffers)
                                  m_bufferPoolStaging.free(b);

                              m_primtiveUploader.processTransfers(cmd, m_bufferPoolStaging, allocatedBuffers);
                              m_imageUploader.processTransfers(   cmd, m_bufferPoolStaging, allocatedBuffers);

                              // memory barrier to prevent running vertex shaders
                              // until all staging->device transfers are complete
                              cmd.waitForTransfersBeforeRunningVertexShaders();
                         });
        //======================================================================



        graph.addTask("gbuffer")
                // Color outputs
             .addColorOutput("albedo"  , vk::Format::eR8G8B8A8Unorm      ) // color outputs
             .addColorOutput("position", vk::Format::eR32G32B32A32Sfloat ) // color outputs
             .addColorOutput("normal"  , vk::Format::eR32G32B32A32Sfloat ) // color outputs
             .addColorOutput("depth"   , vk::Format::eD32Sfloat          ) // depth outputs
                // input Host Resources from other tasks
             .addHostResourceInput("allocatedStagingBuffers")  // not used in task, only to ensure it runs after the bufferTransfer task
             .addHostResourceInput("entityList")               // which entities to draw (from frustumCulling)
             .setInitCallback([&]( vka::RenderTaskVar & C)
                                 {
                                    // gbuffer_descriptorSet is not set to be an input/output resource
                                    // we are going to use this as a local variable
                                    if( !C.localResourceHasValue("storageBuffer") )
                                    {
                                        //=================================
                                        // Create the StorageDescriptor
                                        //=================================
                                        auto & storageBuffer = C.getOrEmplaceLocalResource<vka::SubBuffer>("storageBuffer") = m_bufferPoolUniform.allocate(2*1024*1024);

                                        auto & matrixStorage = C.getOrEmplaceLocalResource<vka::StorageDescriptor_t<glm::mat4> >("matrixStorage");
                                        matrixStorage.init( storageBuffer, 0, 1024*1024 );

                                        auto & renderInfoStorage = C.getOrEmplaceLocalResource<vka::StorageDescriptor_t<PrimRenderInfo> >("renderInfoStorage");
                                        renderInfoStorage.init( storageBuffer, 1024*1024, 1024*1024 );


                                        // Allocate a DescriptorSet
                                        vka::DescriptorSetLayoutCreateInfo2 L;
                   /*matrixStorage*/    L.bindings.push_back( vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eStorageBuffer , 1, vk::ShaderStageFlagBits::eVertex) );
                   /*renderInfoStorage*/L.bindings.push_back( vk::DescriptorSetLayoutBinding(1, vk::DescriptorType::eStorageBuffer , 1, vk::ShaderStageFlagBits::eVertex) );
                   /*renderInfoStorage*/L.bindings.push_back( vk::DescriptorSetLayoutBinding(2, vk::DescriptorType::eCombinedImageSampler , 1, vk::ShaderStageFlagBits::eFragment) );
                                        auto descriptorSet = vka::System::get().allocateDescriptorSet( m_descriptorPool, L );
                                        C.getOrEmplaceLocalResource<vk::DescriptorSet>("descriptorSet") = descriptorSet;
                                        //=================================

                                        vk::Sampler sampler = vka::System::get().getDefaultSampler(m_nullImage);;

                                        vka::DescriptorWriter dWriter;
                                        dWriter.attachBufferDescriptor( descriptorSet, 0, vk::DescriptorType::eStorageBuffer, storageBuffer, 0);
                                        dWriter.attachBufferDescriptor( descriptorSet, 1, vk::DescriptorType::eStorageBuffer, storageBuffer, 1024*1024);
                                        dWriter.attachTextureDescriptor(descriptorSet, 2, vk::DescriptorType::eCombinedImageSampler, m_nullImage, sampler);
                                        vka::System::get().updateDescriptorSets( dWriter.writeDescriptorSet );
                                    }
                                 })
            .setDestroyCallback([&]( vka::RenderTaskVar & C)
                                {
                                   vk::DescriptorSet d = C.getOrEmplaceLocalResource<vk::DescriptorSet>("descriptorSet");
                                   vka::SubBuffer    s = C.getOrEmplaceLocalResource<vka::SubBuffer>("uniformBuffer");

                                   if( d )
                                   {
                                       m_bufferPoolStaging.free(s);
                                       vka::System::get().destroyDescriptorSet(d);
                                   }
                                })
             .setCommandBufferWrite(
                            [&]( vka::RenderTaskVar & c)
                            {
                                // from frustumCulling
                                auto & entityList         = c.getHostResource<      std::vector<uint32_t>       >("entityList");

                                // local data
                                auto & matrixStorage      = c.getLocalResource<vka::StorageDescriptor_t<glm::mat4>      >("matrixStorage");
                                auto & renderInfoStorage  = c.getLocalResource<vka::StorageDescriptor_t<PrimRenderInfo> >("renderInfoStorage");
                                auto & descriptorSet      = c.getLocalResource<vk::DescriptorSet                   >("descriptorSet");

                                auto dt = c.getHostResource<float>("frameTime");
                                static float t= 0;
                                t += dt;
                                matrixStorage.reset();
                                renderInfoStorage.reset();
                                //===========================================================
                                // Copy the uniform buffer
                                //===========================================================
                                auto screenSize = c.getHostResource<glm::vec2>("screenSize");


                                m_camera.setPerspective( screenSize, 45.0f );
                                m_camera.transform.m_position = { 5.f*std::cos(t),5,5.f*std::sin(t)};
                                m_camera.transform.lookat( {0,0,0}, {0,1,0});

                                auto viewMatrix = m_camera.transform.get_view_matrix();
                                auto projMatrix = m_camera.projMatrix;
                                auto eyeMatrix  = glm::mat4(1.0f);

                                PrimRenderInfo rInfo;
                                rInfo.projMatrixIndex = static_cast<int32_t>( matrixStorage.push( projMatrix ) );
                                rInfo.viewMatrixIndex = static_cast<int32_t>( matrixStorage.push( viewMatrix ) );
                                rInfo.nodeMatrixIndex = static_cast<int32_t>( matrixStorage.push( eyeMatrix  ) );

                                //===========================================================

                                auto C = c.commandBuffer;

                                C.beginRenderPass( *c.renderPassBeginInfo, vk::SubpassContents::eInline);

                                    auto pipeline = m_gbufferPipeline.getCurrentPipeline();
                                    C.bindPipeline(vk::PipelineBindPoint::eGraphics, pipeline);
                                    C.bindDescriptorSets(pipeline, 0, descriptorSet, nullptr);

                                    vk::Viewport vp(0,0, static_cast<float>(c.imageExtent.width) ,static_cast<float>(c.imageExtent.height),0,1);
                                    vk::Rect2D scissor( vk::Offset2D(0,0), c.imageExtent);
                                    C.setViewport(0, vp);
                                    C.setScissor(0, scissor);

                                    for(auto & e : entityList)
                                    {
                                        auto & E  = m_entities[e];
                                        auto & pr = m_primitives[ E.primIndex ];

                                         rInfo.worldMatrixIndex = static_cast<int32_t>( matrixStorage.push( E.transform.get_matrix() ) );

                                         auto instanceIndex = renderInfoStorage.push(rInfo);

                                         auto drawCall = pr.device->drawCall();
                                         C.bindDeviceMeshPrimitive(*pr.device, true);
                                         C.drawIndexed( drawCall.indexCount, 1, drawCall.firstIndex, drawCall.vertexOffset, instanceIndex );
                                    }

                                C.endRenderPass();
                            })
                .setClearColorCallback(
                    [](uint32_t index, vk::Format format) -> vk::ClearColorValue
                    {
                        return vk::ClearColorValue( std::array<float,4>( {1,0,0,1}));
                        (void)index;
                        (void)format;
                        return vk::ClearColorValue();
                        })
                    .setClearDepthCallback(
                        [](vk::Format format) -> vk::ClearDepthStencilValue
                        {
                            return vk::ClearDepthStencilValue(1.0,0);
                            (void)format;
                            return vk::ClearDepthStencilValue();
                        })
                      ;

            graph.addTask("lightPass")
                 .addAttachmentInput("albedo"  )   // Input attachments are essentially textures
                 .addAttachmentInput("position")   // which have been written to by a prevoius render pass
                 .addAttachmentInput("normal")     // eg: Not static textures which have been loaded by an external .jpg file
                 .addAttachmentInput("depth")     // eg: Not static textures which have been loaded by an external .jpg file
                 .addColorOutput("full", vk::Format::eR8G8B8A8Unorm)
                  .setCommandBufferWrite(
                        [&]( vka::RenderTaskVar & c)
                        {
                            auto cb = c.commandBuffer;

                            cb.beginRenderPass( *c.renderPassBeginInfo, vk::SubpassContents::eInline);


                            auto p = m_lightPipeline.getCurrentPipeline();
                            cb.bindPipeline( vk::PipelineBindPoint::eGraphics, p);
                            cb.bindDescriptorSets(p, 0, c.inputAttachmentDescriptorSet, nullptr);

                            vk::Viewport vp(0,0, static_cast<float>(c.imageExtent.width), static_cast<float>(c.imageExtent.height),0,1);
                            vk::Rect2D scissor( vk::Offset2D(0,0), c.imageExtent);
                            cb.setViewport(0, vp);
                            cb.setScissor(0, scissor);

                            struct  {
                                 glm::vec2 position;
                                 glm::vec2 size;
                                 glm::vec2 screenSize;
                                 glm::vec2 unused;
                            } pushConsts;
                            pushConsts.position = {-1,-1};
                            pushConsts.size     = {2,2};

                            auto layout = vka::System::get().info( p ).createInfo.layout;
                            cb.pushConstants( layout, vk::ShaderStageFlagBits::eFragment | vk::ShaderStageFlagBits::eVertex, 0, sizeof(pushConsts), &pushConsts);

                            cb.draw(6,1,0,0);

                            cb.endRenderPass();
                        });

            graph.addTask("present")
                 .addAttachmentInput("full")   // Input attachments are essentially textures
                 .addHostResourceOutput("renderComplete")   // Input attachments are essentially textures
                 .setCommandBufferWrite(
                       [&]( vka::RenderTaskVar & c)
                        {
                            assert( c.inputAttachmentImage.size() == 1);
                            auto currentSwapchainImage = c.getHostResource<vk::Image>("currentSwapchainImage");
                            auto swapchainColorFormat  = c.getHostResource<vk::Format>("swapchainColorFormat");
                            auto swapChainImageSize    = c.getHostResource<vk::Extent2D>("swapChainImageSize");

                            auto cb = c.commandBuffer;
                            auto outputImg             = c.inputAttachmentImage.front();// c.graph->getTask("lightPass").getOutputResource(imgName)._image;
                            auto outputImgExtents      = c.inputAttachmentImageExtents.front();///.graph->getTask("lightPass").getOutputResource(imgName)._extents;

                            bool useBlit=false;
                            // Check if the device supports blitting from optimal images (the swapchain images are in optimal format)
                            auto p = vka::System::get().physicalDevice().getFormatProperties(swapchainColorFormat);
                            if( p.optimalTilingFeatures & vk::FormatFeatureFlagBits::eBlitDst)
                            {
                                // we can blit directly to swapchain
                                useBlit=true;
                            }

                            // convert the output image we want to display into TransferSrcOptimal
                            cb.convertShaderImageToTransferSrc(outputImg);
                            // convert the swapchain image into TransferDstOptimal
                            cb.convertSwaphainImageToTransferDst(currentSwapchainImage );

                            { // Do the actual copying from one image to the other.

                                vk::ImageCopy region;
                                region.setExtent( vk::Extent3D(outputImgExtents.width, outputImgExtents.height,1) );
                                region.srcSubresource.layerCount      = 1;
                                region.srcSubresource.baseArrayLayer  = 0;
                                region.srcSubresource.mipLevel        = 0;
                                region.srcSubresource.aspectMask      = vk::ImageAspectFlagBits::eColor;

                                region.dstSubresource.layerCount      = 1;
                                region.dstSubresource.baseArrayLayer  = 0;
                                region.dstSubresource.mipLevel        = 0;
                                region.dstSubresource.aspectMask      = vk::ImageAspectFlagBits::eColor;

                                if( !useBlit )  // if we dont have access to bliting to swapchains then use copyImage
                                {
                                    cb.copyImage( outputImg,
                                                  vk::ImageLayout::eTransferSrcOptimal,
                                                  currentSwapchainImage,
                                                  vk::ImageLayout::eTransferDstOptimal,
                                                  region);
                                }
                                else
                                {
                                    vk::ImageBlit imageBlitRegion;

                                    imageBlitRegion.srcOffsets[0].x = 0;
                                    imageBlitRegion.srcOffsets[0].y = 0;
                                    imageBlitRegion.srcOffsets[0].z = 0;
                                    imageBlitRegion.srcOffsets[1].x = static_cast<int32_t>(outputImgExtents.width);
                                    imageBlitRegion.srcOffsets[1].y = static_cast<int32_t>(outputImgExtents.height);
                                    imageBlitRegion.srcOffsets[1].z = 1;

                                    imageBlitRegion.dstOffsets[0].x = 0;
                                    imageBlitRegion.dstOffsets[0].y = 0;
                                    imageBlitRegion.dstOffsets[0].z = 0;
                                    imageBlitRegion.dstOffsets[1].x = static_cast<int32_t>( swapChainImageSize.width);
                                    imageBlitRegion.dstOffsets[1].y = static_cast<int32_t>( swapChainImageSize.height);
                                    imageBlitRegion.dstOffsets[1].z = 1;

                                    imageBlitRegion.srcSubresource = region.srcSubresource;
                                    imageBlitRegion.dstSubresource = region.dstSubresource;


                                    // Issue the blit command
                                    cb.blitImage( outputImg, vk::ImageLayout::eTransferSrcOptimal,
                                                  currentSwapchainImage, vk::ImageLayout::eTransferDstOptimal,
                                                  imageBlitRegion, vk::Filter::eNearest
                                                  );
                                }
                            }

                            // convert the swapchain image into back to presentKHR so that it can be presented
                            cb.convertSwapchainImageFromTransferDstToPresentSrc(currentSwapchainImage);

                        })
            ;

            graph.setFinalOutput("renderComplete");
            graph.compile();
            graph.dotGraph(std::cout);

            graph.init();
    }

    /**
     * @brief uploadScene
     * @param M
     * @return
     *
     * Upload a scene to to the
     */
    vka::Scene uploadGLTF(uGLTF::GLTFModel const & M)
    {
        auto hostScene = vka::fromUGLTF( M );

        return uploadScene(hostScene);
    }

    vka::Scene uploadScene(vka::HostScene const & S)
    {
        vka::Scene scene;

        scene.nodes      = S.nodes;
        scene.meshes     = S.meshes;
        scene.materials  = S.materials;
        scene.scenes     = S.scenes;
        scene.animations = S.animations;
        scene.primitives = S.primitives;
        scene.skins      = S.skins;

        for(auto & p : S.rawPrimitives)
        {
             scene.rawPrimitives.push_back( m_primtiveUploader.upload( p , m_bufferPoolAttributes) );
        }

        for(auto & p : S.images)
        {
             scene.images.push_back( m_imageUploader.uploadImage( p , m_texturePool) );
        }

        return scene;
    }

    /**
     * @brief releaseResources
     *
     * This method must destroy all
     * non-swapchain related resources that were
     *
     */
    void releaseResources() override
    {
        auto & System = vka::System::get();

        // we should call m_frameGraph.freeResources() before
        // calling destroy. But this gets called in releaseSwapchainResources()
        m_frameGraph.destroy();

        m_gbufferPipeline.destroy();
        m_lightPipeline.destroy();

        System.destroyBufferPool(m_bufferPoolStaging);
        System.destroyBufferPool(m_bufferPoolAttributes);
        System.destroyBufferPool(m_bufferPoolAttributesHostVisible);
        System.destroyBufferPool(m_bufferPoolUniform);

        System.destroyTexturePool(m_texturePool);

        System.destroyDescriptorPool(m_descriptorPool);



        glslang::FinalizeProcess();
        std::cout << "release non-graphics resources" << std::endl;
    }


    auto compileShaderModule (const std::string & path, EShLanguage language)
    {
        vka::GLSLCompiler compiler;

        try {
            auto spv   = compiler.compileFile(path, language  );
            vka::ShaderModuleCreateInfo2 _createInfo;
            _createInfo.code = spv;
            return vka::System::get().createShaderModule(_createInfo);
        } catch (std::exception & e) {

            std::cout << compiler.getLog() << std::endl;
            throw  e;
        }
    };


    /**
     * @brief initSwapChainResources
     *
     * This is called right after the swapchain is created.
     * You can use this to initialize any swapchain related resources,
     * for example, render targets (because they change with swapchain size)
     */
    void initSwapChainResources() override
    {
        std::cout << "init swapchain releated resources" << std::endl;

        auto width    = swapchainImageSize().width;
        auto height   = swapchainImageSize().height;


        // Set the swapchain size for the framegraph
        // We will need to call initializeResources()
        // to regenerate all the internal framebuffers
        // because of the new size
        m_frameGraph.setSwapchainExtent( vk::Extent2D(width,height));

        // Initialize the s
        m_frameGraph.checkReinitalizeResources();

        {
            static bool once=true;

            if( once )
            {
                {
                    once=false;
                    // use this to render to the swapchain
                    vka::GraphicsPipelineCreateInfo4 P;


                    P.vertexShader   = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/framegraph/scene_cpn.vert", EShLangVertex);
                    P.fragmentShader = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/framegraph/scene_cpn.frag", EShLangFragment);
                    P.renderPass     = m_frameGraph.getTask("gbuffer").getRenderPass();
                    P.vertexInputFormats = {
                        vk::Format::eR32G32B32Sfloat,
                        vk::Format::eR32G32B32Sfloat,
                        vk::Format::eR32G32Sfloat
                    };

                    P.enableDepthTest  = true;
                    P.enableDepthWrite = true;

                    P.colorOutputs.emplace_back().format = vk::Format::eR8G8B8A8Unorm;
                    P.colorOutputs.emplace_back().format = vk::Format::eR32G32B32A32Sfloat;
                    P.colorOutputs.emplace_back().format = vk::Format::eR32G32B32A32Sfloat;

                    m_gbufferPipeline.init(P);
                }
                {
                    once=false;
                    // use this to render to the swapchain
                    vka::GraphicsPipelineCreateInfo4 P;


                    P.vertexShader   = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/framegraph/lightPass.vert", EShLangVertex);
                    P.fragmentShader = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/framegraph/lightPass.frag", EShLangFragment);
                    P.renderPass     = m_frameGraph.getTask("lightPass").getRenderPass();
                    P.vertexInputFormats = {
                    };

                    P.enableDepthTest  = false;
                    P.enableDepthWrite = false;

                    P.colorOutputs.emplace_back().format = vk::Format::eR8G8B8A8Unorm;

                    m_lightPipeline.init(P);
                }
            }
        }


        updateDescriptorSets();

        startThreadPolling();
    }

    void releaseSwapChainResources() override
    {
        // the swapchain has changed size
        // so we will need to free the internal framebuffers
        //m_frameGraph.freeResources();
        //std::cout << "release swapchain releated resources" << std::endl;
        stopThreadPolling();
    }

    void updateDescriptorSets()
    {
        vka::DescriptorWriter dWriter;

        if( dWriter.writeDescriptorSet.size() )
            vka::System::get().updateDescriptorSets( dWriter.writeDescriptorSet );

        dWriter.clear();

    }



    vka::Camera m_camera;

    /**
     * @brief render
     * @param frame
     *
     * This is the widget's main vulkan render function.
     * use this to actually record your command buffers.
     * submission of commandbuffers are automatically done
     * at the end
     */
    void render(vka::ApplicationFrame & frame) override
    {
        updateDescriptorSets();
        auto cb = vka::CommandBuffer(frame.currentCommandBuffer);

        tick( std::chrono::duration<double>(frame.frameTime - frame.lastFrameTime).count() );
        float dt = std::chrono::duration<float>(frame.frameTime - frame.lastFrameTime).count();

        glm::vec2 screenSize( frame.swapchainImageSize.width , frame.swapchainImageSize.height );
        m_camera.setPerspective( screenSize, 45.0f );
        m_camera.transform.m_position = {0,0,-5};


        // Execute the Frame Graph.
        // These resources are required by some of the defined tasks, but are
        // not created by any of the tasks, so we need to emplace them
        // before the task executes.
        m_frameGraph.getOrEmplaceHostResource<float>("frameTime")                 = dt;
        m_frameGraph.getOrEmplaceHostResource<glm::vec2>("screenSize")            = glm::vec2( frame.swapchainImageSize.width , frame.swapchainImageSize.height );
        m_frameGraph.getOrEmplaceHostResource<vk::Image>("currentSwapchainImage") = frame.currentSwapchainImage;
        m_frameGraph.getOrEmplaceHostResource<vk::Format>("swapchainColorFormat") = frame.swapchainColorFormat;
        m_frameGraph.getOrEmplaceHostResource<vk::Extent2D>("swapChainImageSize") = frame.swapchainImageSize;
        m_frameGraph.execute(cb);

        if(0)
        {
            const std::string imgName = "full";
            auto outputImg        = m_frameGraph.getTask("lightPass").getOutputResource(imgName)._image;
            auto outputImgExtents = m_frameGraph.getTask("lightPass").getOutputResource(imgName)._extents;


            bool useBlit=false;
            // Check if the device supports blitting from optimal images (the swapchain images are in optimal format)
            auto p = vka::System::get().physicalDevice().getFormatProperties(frame.swapchainColorFormat);
            if( p.optimalTilingFeatures & vk::FormatFeatureFlagBits::eBlitDst)
            {
                // we can blit directly to swapchain
                useBlit=true;
            }
          //  useBlit=false;


            // convert the output image we want to display into TransferSrcOptimal
            cb.convertShaderImageToTransferSrc(outputImg);
            // convert the swapchain image into TransferDstOptimal
            cb.convertSwaphainImageToTransferDst(frame.currentSwapchainImage);

            { // Do the actual copying from one image to the other.

                vk::ImageCopy region;
                region.setExtent( vk::Extent3D(outputImgExtents.width, outputImgExtents.height,1) );
                region.srcSubresource.layerCount      = 1;
                region.srcSubresource.baseArrayLayer  = 0;
                region.srcSubresource.mipLevel        = 0;
                region.srcSubresource.aspectMask      = vk::ImageAspectFlagBits::eColor;

                region.dstSubresource.layerCount      = 1;
                region.dstSubresource.baseArrayLayer  = 0;
                region.dstSubresource.mipLevel        = 0;
                region.dstSubresource.aspectMask      = vk::ImageAspectFlagBits::eColor;

                if( !useBlit )  // if we dont have access to bliting to swapchains then use copyImage
                {

                    cb.copyImage( outputImg,
                                  vk::ImageLayout::eTransferSrcOptimal,
                                  frame.currentSwapchainImage,
                                  vk::ImageLayout::eTransferDstOptimal,
                                  region);
                }
                else
                {
                    vk::ImageBlit imageBlitRegion;

                    imageBlitRegion.srcOffsets[0].x = 0;
                    imageBlitRegion.srcOffsets[0].y = 0;
                    imageBlitRegion.srcOffsets[0].z = 0;
                    imageBlitRegion.srcOffsets[1].x = static_cast<int32_t>(outputImgExtents.width);
                    imageBlitRegion.srcOffsets[1].y = static_cast<int32_t>(outputImgExtents.height);
                    imageBlitRegion.srcOffsets[1].z = 1;

                    imageBlitRegion.dstOffsets[0].x = 0;
                    imageBlitRegion.dstOffsets[0].y = 0;
                    imageBlitRegion.dstOffsets[0].z = 0;
                    imageBlitRegion.dstOffsets[1].x = static_cast<int32_t>( frame.swapchainImageSize.width);
                    imageBlitRegion.dstOffsets[1].y = static_cast<int32_t>( frame.swapchainImageSize.height);
                    imageBlitRegion.dstOffsets[1].z = 1;

                    imageBlitRegion.srcSubresource = region.srcSubresource;
                    imageBlitRegion.dstSubresource = region.dstSubresource;


                    // Issue the blit command
                    cb.blitImage( outputImg, vk::ImageLayout::eTransferSrcOptimal,
                                  frame.currentSwapchainImage, vk::ImageLayout::eTransferDstOptimal,
                                  imageBlitRegion, vk::Filter::eNearest
                                  );
                }
            }

            // convert the swapchain image into back to presentKHR so that it can be presented
            cb.convertSwapchainImageFromTransferDstToPresentSrc(frame.currentSwapchainImage);

        }
    }


    void tick(double dt)
    {
        static double t=0;
        t += dt;
    }

    void mouseMoveEvent(const vka::EvtInputMouseMotion *e) override
    {
        (void)e;
    }
    void mousePressEvent(const vka::EvtInputMouseButton *e) override
    {
        if( e->button == vka::MouseButton::LEFT)
        {
            m_mouse.left = true;
        }
        if( e->button == vka::MouseButton::RIGHT)
            m_mouse.right = true;
        if( e->button == vka::MouseButton::MIDDLE)
        {
            m_mouse.middle = true;
        }
    }
    void mouseReleaseEvent(const vka::EvtInputMouseButton *e) override
    {
        if( e->button == vka::MouseButton::LEFT)
            m_mouse.left = false;
        if( e->button == vka::MouseButton::RIGHT)
            m_mouse.right = false;
        if( e->button == vka::MouseButton::MIDDLE)
        {
            m_mouse.middle = false;
            m_mouseProjection = m_camera.projectMouseRay( glm::vec2(e->x, e->y) );
        }
    }
    void keyPressEvent(const vka::EvtInputKey *e) override
    {
        (void)e;
       // std::cout << "Key Press: " << to_string(e->keycode) << std::endl;;
    }
    void keyReleaseEvent(const vka::EvtInputKey *e) override
    {
        (void)e;
    }

    struct
    {
        bool forward=false;
    } control;
    struct
    {
        bool left  = false;
        bool middle=false;
        bool right = false;
    } m_mouse;
    //=======================

    vka::line3 m_mouseProjection;
};

#ifndef VKA_NO_SDL_WIDGET

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;
    // The basic usage of this

    // create a vulkan window widget
    vka::SDLVulkanWidget2 vulkanWindow;

    // create the window and initialize
    // the vka::System
    vulkanWindow.create(1024, 768, debugCallback);

    // instantiate your application
    MyApp app;

    vulkanWindow.exec( &app );


    return 0;
}

#endif

