#include <iostream>
#include <queue>
#include <tuple>

#include <vka/core.h>

#include <vka/utils/SceneLoader.h>

#include <future>

#include <vka/logging.h>


#include <vka/math.h>

#include <vka/GLSLCompiler/GLSLCompiler.h>

#include <vka/Renderers/PBRSceneRenderer.h>
#include <vka/Renderers/TexturePresentRenderer.h>
#include <vka/Renderers/PostProcessRenderer.h>
#include <vka/Renderers/PBRLightPass.h>
#include <vka/Renderers/SimpleRenderer.h>
#include <vka/Renderers/Uploaders.h>
#include <vka/Renderers/PostProcessRenderer2.h>

#include <vka/utils/Camera.h>
#include <vka/Controllers/FPSController.h>
#include <vka/Controllers/OrbitController.h>
#include <vka/Controllers/GizmoController.h>

#define WIDTH  1024
#define HEIGHT 768


#ifndef VKA_NO_SDL_WIDGET
#include <vka/widgets/SDLWidget.h>
#endif

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;
    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}


class MyApp : public vka::Application
{
    // Application interface
public:

    //==========================================================================
    // Resources Pools
    //==========================================================================
    vka::BufferPool   m_bufferPoolStaging;
    vka::BufferPool   m_bufferPoolUniform;
    vka::BufferPool   m_bufferPoolAttributes;
    vka::BufferPool   m_bufferPoolAttributesHostVisible;

    vka::TexturePool  m_texturePool;
    vka::TexturePool  m_texturePoolColorAttachments;
    vka::TexturePool  m_texturePoolDepth;

    vk::DescriptorPool m_descriptorPool;

    //==========================================================================
    // Resources
    //==========================================================================
    vk::ImageView     m_nullImage;
    vk::ImageView     m_attachmentPosition;
    vk::ImageView     m_attachmentNormal;
    vk::ImageView     m_attachmentAlbedo;
    vk::ImageView     m_attachmentDepth;
    vka::RenderTarget m_targetGbuffer;



    vk::ImageView     m_ImageFinal1;
    vk::ImageView     m_ImageFinal2;
    vka::RenderTarget m_PostProcessingTarget1;
    vka::RenderTarget m_PostProcessingTarget2;
    //==========================================================================

#define SCENE_RENDERER_ALLOCATE_OWN_RESOURCES



    //==========================================================================
    // Renderers
    //==========================================================================
    // resources needed for the SceneRenderer
#if !defined SCENE_RENDERER_ALLOCATE_OWN_RESOURCES
    vka::SubBuffer                                             m_pbr_matrixStorageBuffer;
    vka::SubBuffer                                             m_pbr_materialStorageBuffer;
    vka::SubBuffer                                             m_pbr_uniformBuffer;
    StorageDescriptor_t<glm::mat4>                             m_pbr_matrixStorage;
    StorageDescriptor_t<vka::PBRPrimitiveRenderer::Material_t> m_pbr_materialStorage;
#endif

    // scene renderer to render vka::Scene objects
    vka::PBRSceneRenderer                                      m_sceneRenderer;

    // Simple postprocess renderer
    vka::PostProcessRenderer2                                  m_lightPass;
    vka::PostProcessRenderer2                                  m_postProcessRenderer;

    // Renderer for presenting a single texture to the swapchain image
    TexturePresentRenderer                                     m_texturePresentRenderer;


    vka::SimpleRenderer                                          m_lineRenderer;
    //==========================================================================




    //==========================================================================
    // Helper Classes
    //==========================================================================
    vka::PrimitiveUploader m_primtiveUploader;
    vka::ImageUploader     m_imageUploader;
    //==========================================================================


    vka::Scene                          m_scene1;
    vka::Scene                          m_gizmoScene;

    vka::transform                      m_rootSceneTransform;
    vka::transform                      m_lightTransform;
    /**
     * @brief initResources
     *
     * This will be called right after system gets created.
     * It can be used to initialize any vulkan related resources
     * that are not related to the swapchain.
     */
    void initResources() override
    {
        addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");

        glslang::InitializeProcess();


        //m_lightTransform.lookat( { 1, 1, 1}, {0,1,0});
        //m_lightTransform.m_position = {-3,5,-3};
        m_camera.transform.m_position = {5,5,5};
        m_camera.transform.lookat({0,0,0},{0,1,0});

        {
            auto & System = vka::System::get();
            // Staging Pool and Unifforms
             m_bufferPoolStaging = System.createBufferPool( 25*1024*1024,
                                                              vk::BufferUsageFlagBits::eTransferSrc,
                                                              vk::MemoryPropertyFlagBits::eHostVisible
                                                                 | vk::MemoryPropertyFlagBits::eHostCoherent);

             m_bufferPoolUniform = System.createBufferPool( 10*1024*1024,
                                                              vk::BufferUsageFlagBits::eUniformBuffer
                                                                 | vk::BufferUsageFlagBits::eStorageBuffer
                                                                 | vk::BufferUsageFlagBits::eTransferSrc,
                                                              vk::MemoryPropertyFlagBits::eHostVisible
                                                                 | vk::MemoryPropertyFlagBits::eHostCoherent);

             m_bufferPoolAttributesHostVisible = System.createBufferPool( 5*1024*1024,
                                                                          vk::BufferUsageFlagBits::eVertexBuffer
                                                                              | vk::BufferUsageFlagBits::eIndexBuffer
                                                                              | vk::BufferUsageFlagBits::eTransferDst,
                                                                          vk::MemoryPropertyFlagBits::eHostVisible
                                                                             | vk::MemoryPropertyFlagBits::eHostCoherent);

            m_bufferPoolAttributes = System.createBufferPool( 25*1024*1024,
                                                             vk::BufferUsageFlagBits::eVertexBuffer
                                                                 | vk::BufferUsageFlagBits::eIndexBuffer
                                                                 | vk::BufferUsageFlagBits::eTransferDst,
                                                              vk::MemoryPropertyFlagBits::eDeviceLocal );

            m_texturePool =  System.createTexturePool( 50*1024*1024,
                                       vk::Format::eR8G8B8A8Unorm,
                                       vk::MemoryPropertyFlagBits::eDeviceLocal,
                                       vk::ImageUsageFlagBits::eSampled
                                       | vk::ImageUsageFlagBits::eTransferSrc
                                       | vk::ImageUsageFlagBits::eTransferDst);



            vka::DescriptorPoolCreateInfo2 I;
            I.maxSets = 1000;
            I.poolSizes.push_back( {vk::DescriptorType::eCombinedImageSampler, 1000 });
            I.poolSizes.push_back( {vk::DescriptorType::eUniformBuffer, 1000 });
            I.poolSizes.push_back( {vk::DescriptorType::eUniformBufferDynamic, 1000 });
            I.poolSizes.push_back( {vk::DescriptorType::eStorageBuffer, 1000 });

            m_descriptorPool = System.createDescriptorPool( I );

        }

       #if !defined SCENE_RENDERER_ALLOCATE_OWN_RESOURCES
        m_pbr_matrixStorageBuffer   = m_bufferPoolUniform.allocate(1024*1024);
        m_pbr_materialStorageBuffer = m_bufferPoolUniform.allocate(1024*1024);
        m_pbr_uniformBuffer         = m_bufferPoolUniform.allocate(1024);

        m_pbr_matrixStorage.init(   m_pbr_matrixStorageBuffer  , 0 , m_pbr_matrixStorageBuffer.getSize()   );
        m_pbr_materialStorage.init( m_pbr_materialStorageBuffer, 0 , m_pbr_materialStorageBuffer.getSize() );
       #endif


       vka::HostImage img;
       img.resize(1024, 1024);

       //// Generate an image by applying a function to each channel
       img.r = [](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
       img.g = 0;//[](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
       img.b = [](float u, float v){ return 0.5f+0.5f*std::sin( 3*2*3.14159f*std::hypot(u,v-1)) ;}; // full red texture;
       img.a = 255; // full red texture;


       m_nullImage  = m_imageUploader.uploadImage(img, m_texturePool);

       {

           {
               std::ifstream in(VKA_CMAKE_SOURCE_DIR "/share/models/BoxTextured.gltf");
               //std::ifstream in(VKA_CMAKE_SOURCE_DIR "/share/models/BoxAnimated.gltf");
               //std::ifstream in(VKA_CMAKE_SOURCE_DIR "/share/models/InterpolationTest.glb");
               //std::ifstream in(VKA_CMAKE_SOURCE_DIR "/share/models/ybot_full.glb");
               uGLTF::GLTFModel M;
               M.load(in);
               m_scene1 = uploadGLTF(M);


               //m_scene1 = uploadScene( m_sceneRenderer.createTestSphere() );
           }

           {
               m_gizmoScene = uploadScene( vka::HostScene::createGizmoScene() );
           }
       }
    }


    /**
     * @brief uploadScene
     * @param M
     * @return
     *
     * Upload a scene to to the
     */
    vka::Scene uploadGLTF(uGLTF::GLTFModel const & M)
    {
        //std::vector<vka::HostTriPrimitive>  scenePrimitivesHost;
        auto hostScene = vka::fromUGLTF( M );

        return uploadScene(hostScene);
    }

    vka::Scene uploadScene(vka::HostScene const & S)
    {
        vka::Scene scene;

        scene.nodes      = S.nodes;
        scene.meshes     = S.meshes;
        scene.materials  = S.materials;
        scene.scenes     = S.scenes;
        scene.animations = S.animations;
        scene.primitives = S.primitives;
        scene.skins      = S.skins;

        for(auto & p : S.rawPrimitives)
        {
             scene.rawPrimitives.push_back( m_primtiveUploader.upload( p , m_bufferPoolAttributes) );
        }

        for(auto & p : S.images)
        {
             scene.images.push_back( m_imageUploader.uploadImage( p , m_texturePool) );
        }

        return scene;
    }

    /**
     * @brief releaseResources
     *
     * This method must destroy all
     * non-swapchain related resources that were
     *
     */
    void releaseResources() override
    {
        auto & System = vka::System::get();

        m_sceneRenderer.destroy();
        m_texturePresentRenderer.destroy();
        m_lightPass.destroy();
        m_postProcessRenderer.destroy();
        m_lineRenderer.destroy();

        System.destroyBufferPool(m_bufferPoolStaging);
        System.destroyBufferPool(m_bufferPoolAttributes);
        System.destroyBufferPool(m_bufferPoolAttributesHostVisible);
        System.destroyBufferPool(m_bufferPoolUniform);

        System.destroyTexturePool(m_texturePool);

        System.destroyDescriptorPool(m_descriptorPool);

        glslang::FinalizeProcess();
        std::cout << "release non-graphics resources" << std::endl;
    }


    /**
     * @brief initSwapChainResources
     *
     * This is called right after the swapchain is created.
     * You can use this to initialize any swapchain related resources,
     * for example, render targets (because they change with swapchain size)
     */
    void initSwapChainResources() override
    {
        std::cout << "init swapchain releated resources" << std::endl;

        auto & System = vka::System::get();
        auto width    = swapchainImageSize().width;
        auto height   = swapchainImageSize().height;

        //===============================================================================================================================
        // Create the texture pools which will be used for color attachments
        // and depth attachments.
        //
        // we will have to creat these everytime the swapchain is recreated because we
        // will need different amounts of memory depending on the size of the swapchain.
        //
        // One way to optimize this is to only free the memory/images if the
        // swapchain is larger than the previous size.
        // if it is smaller, we can reuse the images, but then use the viewport to
        // only render to a subset of the image.
        //===============================================================================================================================
        {
            m_texturePoolDepth =  System.createTexturePool( width*height*4,
                                       vk::Format::eD32Sfloat,
                                       vk::MemoryPropertyFlagBits::eDeviceLocal,
                                       vk::ImageUsageFlagBits::eSampled |vk::ImageUsageFlagBits::eDepthStencilAttachment);
            size_t
            colorAttachmentSize =  2 * width*height*vka::formatSize(vk::Format::eR32G32B32A32Sfloat)
                                 + 3 * width*height*vka::formatSize(vk::Format::eR8G8B8A8Unorm);


            m_texturePoolColorAttachments  =  System.createTexturePool( colorAttachmentSize,
                                                              vk::Format::eR32G32B32A32Sfloat,
                                                              vk::MemoryPropertyFlagBits::eDeviceLocal,
                                                              vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eColorAttachment);
        }
        //===============================================================================================================================





        //===============================================================================================================================
        // Create the GBuffer target and initialize the primitive renderer
        //===============================================================================================================================
        {
            vka::RenderTargetCreateInfo rtCI;

            m_attachmentDepth     = m_texturePoolDepth.createNewImage2D( vk::Format::eD32Sfloat, {width,height}, 1, 1);
            m_attachmentPosition = m_texturePoolColorAttachments.createNewImage2D( vk::Format::eR32G32B32A32Sfloat, {width, height}, 1, 1);
            m_attachmentNormal   = m_texturePoolColorAttachments.createNewImage2D( vk::Format::eR32G32B32A32Sfloat, {width, height}, 1, 1);
            m_attachmentAlbedo   = m_texturePoolColorAttachments.createNewImage2D( vk::Format::eR8G8B8A8Unorm,      {width, height}, 1, 1);

            rtCI.colorTargets = {m_attachmentAlbedo, m_attachmentPosition, m_attachmentNormal };
            rtCI.depthTarget  = m_attachmentDepth;
            m_targetGbuffer   = System.createRenderTarget(rtCI);

            #if !defined SCENE_RENDERER_ALLOCATE_OWN_RESOURCES
            #else
            #endif

            if(!m_sceneRenderer)
            {
                #if !defined SCENE_RENDERER_ALLOCATE_OWN_RESOURCES
                    auto PBRcreate = vka::PBRPrimitiveRenderer::createInfo();
                #else
                    auto PBRcreate = vka::PBRPrimitiveRenderer::CreateInfo2();
                #endif

                PBRcreate.baseVertexShaderPath      = getPath("scene_pbr_base.vert");
                PBRcreate.baseFragmentShaderPath    = getPath("scene_pbr_base.frag");

                PBRcreate.concurrentFrames      = concurrentFrameCount();
                PBRcreate.descriptorPool        = m_descriptorPool;
                PBRcreate.screenSize            = swapchainImageSize();


                #if !defined SCENE_RENDERER_ALLOCATE_OWN_RESOURCES
                    PBRcreate.matrixStorage         = m_pbr_matrixStorage;   // Storage Buffer Used for storing matrice
                    PBRcreate.materialStorage       = m_pbr_materialStorage; // Storage Buffer used for storing materials
                    PBRcreate.globalUniformBuffer   = m_pbr_uniformBuffer;   // Uniform Buffer used for storing global
                #else
                    PBRcreate.uniformStorageBufferPool = m_bufferPoolUniform;
                #endif

                PBRcreate.nullImage             = m_nullImage; // a null image which will be used
                PBRcreate.maxTextures           = 64;
                // Enable writing to the position and normal colour target
                PBRcreate.renderPass            = m_targetGbuffer.getDefaultRenderPass();
                PBRcreate.albedoOutput          = m_attachmentAlbedo;
                PBRcreate.positionOutput        = m_attachmentPosition;
                PBRcreate.normalOutput          = m_attachmentNormal;

                m_sceneRenderer.init(PBRcreate);


                //===============================================================================================================================
                // Create a Custom Shader for the PBRPrimitiveRenderer.
                // This shader will simply scale the vertices by a certain amount.
                //===============================================================================================================================
                {
                    std::ifstream t( VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer/scene_pbr_default.frag");
                    std::string shaderSrc((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

                    m_sceneRenderer.newPipelinePBR( vka::uri("file:" VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer/scene_pbr_breathe.vert"),
                                                    vka::uri("file:" VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer/scene_pbr_default.frag") );

                }
            }

            m_sceneRenderer.setViewport( vk::Viewport( 0,0, static_cast<float>(width), static_cast<float>(height), 0, 1) );
            m_sceneRenderer.setScissor( vk::Rect2D( vk::Offset2D(0,0), swapchainImageSize()));
        }
        //===============================================================================================================================


        m_ImageFinal1   = m_texturePoolColorAttachments.createNewImage2D( vk::Format::eR8G8B8A8Unorm, {width, height}, 1, 1);
        m_ImageFinal2   = m_texturePoolColorAttachments.createNewImage2D( vk::Format::eR8G8B8A8Unorm, {width, height}, 1, 1);

        vka::RenderTargetCreateInfo rtCI;
        rtCI.colorTargets = {m_ImageFinal1};
        m_PostProcessingTarget1 = System.createRenderTarget(rtCI);

        rtCI.colorTargets = {m_ImageFinal2};
        m_PostProcessingTarget2 = System.createRenderTarget(rtCI);

        //===============================================================================================================================
        // Create the post processing render target and set up the postprocessing renderer
        //===============================================================================================================================
        {
            if( !m_lightPass)
            {
                auto PBRcreate = m_lightPass.createInfo2();

                PBRcreate.baseVertexShaderPath     = getPath("postprocess_base.vert");
                PBRcreate.baseFragmentShaderPath   = getPath("postprocess_base.frag");
                PBRcreate.postProcessShaderPath    = getPath("postprocess_lightpass.frag");

                PBRcreate.concurrentFrames         = concurrentFrameCount();
                PBRcreate.descriptorPool           = m_descriptorPool;
                PBRcreate.screenSize               = swapchainImageSize();
                PBRcreate.uniformStorageBufferPool = m_bufferPoolUniform;

                PBRcreate.maxTextures           = 8;
                // Enable writing to the position and normal colour target
                PBRcreate.renderPass            = m_PostProcessingTarget1.getDefaultRenderPass();
                PBRcreate.albedoOutput          = m_ImageFinal1;

                m_lightPass.init(PBRcreate);
            }
            m_lightPass.setViewport( vk::Viewport( 0,0, static_cast<float>(width), static_cast<float>(height), 0, 1) );
            m_lightPass.setScissor( vk::Rect2D( vk::Offset2D(0,0), swapchainImageSize()));
            m_lightPass.clearFrameTextures();
            m_lightPass.addFrameTexture( m_attachmentAlbedo );
            m_lightPass.addFrameTexture( m_attachmentPosition );
            m_lightPass.addFrameTexture( m_attachmentNormal );
            m_lightPass.setInputTexture( 0, m_attachmentAlbedo);
            m_lightPass.setInputTexture( 1, m_attachmentPosition);
            m_lightPass.setInputTexture( 2, m_attachmentNormal);
        }


        if( !m_postProcessRenderer)
        {
            auto PBRcreate = m_postProcessRenderer.createInfo2();

            PBRcreate.baseVertexShaderPath     = getPath("postprocess_base.vert");
            PBRcreate.baseFragmentShaderPath   = getPath("postprocess_base.frag");
            PBRcreate.postProcessShaderPath    = getPath("postprocess_blur.frag");

            PBRcreate.concurrentFrames         = concurrentFrameCount();
            PBRcreate.descriptorPool           = m_descriptorPool;
            PBRcreate.screenSize               = swapchainImageSize();
            PBRcreate.uniformStorageBufferPool = m_bufferPoolUniform;

            PBRcreate.maxTextures           = 8;
            // Enable writing to the position and normal colour target
            PBRcreate.renderPass            = m_PostProcessingTarget1.getDefaultRenderPass();
            PBRcreate.albedoOutput          = m_ImageFinal2;

            m_postProcessRenderer.init(PBRcreate);
        }
        m_postProcessRenderer.setViewport( vk::Viewport( 0,0, static_cast<float>(width), static_cast<float>(height), 0, 1) );
        m_postProcessRenderer.setScissor( vk::Rect2D( vk::Offset2D(0,0), swapchainImageSize()));
        m_postProcessRenderer.clearFrameTextures();
        m_postProcessRenderer.addFrameTexture( m_ImageFinal1 );
        m_postProcessRenderer.setInputTexture( 0, m_ImageFinal1);

        //===============================================================================================================================
        // Set up the Line Renderer presenter
        //===============================================================================================================================
        {
            if(!m_lineRenderer )
            {
                auto createInfo           = m_lineRenderer.createInfo();

                createInfo.hostVisibleUniformBufferPool = m_bufferPoolUniform;
                createInfo.hostVisibleVertexBuffer      = m_bufferPoolAttributesHostVisible.allocate(2*1024*1024);

                createInfo.descriptorPool = m_descriptorPool;
                createInfo.albedoOutput   = m_attachmentAlbedo;
                createInfo.positionOutput = m_attachmentPosition;
                createInfo.normalOutput   = m_attachmentNormal;

                createInfo.renderPass     = m_targetGbuffer.getDefaultRenderPass();
                createInfo.screenSize     = swapchainImageSize();
                m_lineRenderer.init(createInfo);
            }

            m_lineRenderer.setViewport( vk::Viewport( 0,0, static_cast<float>(width), static_cast<float>(height), 0, 1) );
            m_lineRenderer.setScissor(  vk::Rect2D( vk::Offset2D(0,0), swapchainImageSize()));
        }
        //===============================================================================================================================




        //===============================================================================================================================
        // Set up the texture presenter
        //===============================================================================================================================
        {
            if(!m_texturePresentRenderer )
            {
                TexturePresentRenderer::CreateInfo TPRcreate;
                TPRcreate.renderPass       = getDefaultRenderPass();
                TPRcreate.descriptorPool   = m_descriptorPool;
                TPRcreate.screenSize       = swapchainImageSize();
                TPRcreate.concurrentFrames = concurrentFrameCount();
                m_texturePresentRenderer.init(TPRcreate);
                m_texturePresentRenderer.setImageSize(2,2);
                m_texturePresentRenderer.setImagePosition(-1.0,-1.0);
                m_texturePresentRenderer.setImage( m_ImageFinal1 ); // present the albdo image
            }


            m_texturePresentRenderer.setViewport( vk::Viewport( 0,0, static_cast<float>(width), static_cast<float>(height), 0, 1) );
            m_texturePresentRenderer.setScissor( vk::Rect2D( vk::Offset2D(0,0), swapchainImageSize()));
        }
        //===============================================================================================================================



        m_sceneRenderer.addFrameTexture( m_nullImage);


        m_texturePresentRenderer.setImage( m_ImageFinal2 ); // present the albdo image

        updateDescriptorSets();
    }

    void updateDescriptorSets()
    {
        vka::DescriptorWriter dWriter;

        m_sceneRenderer.updateSets(dWriter);

        m_lineRenderer.updateSets(dWriter);

        m_lightPass.updateSets(dWriter);

        m_postProcessRenderer.updateSets(dWriter);

        m_texturePresentRenderer.updateSets(dWriter);

        if( dWriter.writeDescriptorSet.size() )
            vka::System::get().updateDescriptorSets( dWriter.writeDescriptorSet );

        dWriter.clear();

    }
    void releaseSwapChainResources() override
    {
        auto & System = vka::System::get();


        //===============================================================================================================================
        // Destroy the gbuffer
        //===============================================================================================================================
        System.destroyRenderTarget(m_targetGbuffer);
        m_texturePoolColorAttachments.destroyImage(m_attachmentPosition);
        m_texturePoolColorAttachments.destroyImage(m_attachmentNormal);
        m_texturePoolColorAttachments.destroyImage(m_attachmentAlbedo);
        m_texturePoolDepth.destroyImage(m_attachmentDepth);
        //===============================================================================================================================


        //===============================================================================================================================
        // Destroy the post process renderer
        //===============================================================================================================================
        System.destroyRenderTarget(m_PostProcessingTarget1);
        System.destroyRenderTarget(m_PostProcessingTarget2);

        m_texturePoolColorAttachments.destroyImage(m_ImageFinal1);
        //===============================================================================================================================

        {
            System.destroyTexturePool(m_texturePoolColorAttachments);
            System.destroyTexturePool(m_texturePoolDepth);
        }
        std::cout << "release swapchain releated resources" << std::endl;
    }


    vka::Camera m_camera;

    /**
     * @brief render
     * @param frame
     *
     * This is the widget's main vulkan render function.
     * use this to actually record your command buffers.
     * submission of commandbuffers are automatically done
     * at the end
     */
    void render(vka::ApplicationFrame & frame) override
    {
        m_sceneRenderer.prepSceneTextures(m_scene1);
        updateDescriptorSets();
        auto cb = vka::CommandBuffer(frame.currentCommandBuffer);

        tick( std::chrono::duration<double>(frame.frameTime - frame.lastFrameTime).count() );

        // we need to store the allocated subbuffers
        // until the commandbuffer has been submitted
        std::vector<vka::SubBuffer> allocatedBuffers;

        // upload any meshs that need to be uploaded
        m_primtiveUploader.processTransfers(cb, m_bufferPoolStaging, allocatedBuffers);
        m_imageUploader.processTransfers(   cb, m_bufferPoolStaging, allocatedBuffers);

        cb.waitForHostTransfers();

        glm::vec2 screenSize( frame.swapchainImageSize.width , frame.swapchainImageSize.height );
        m_camera.setPerspective( screenSize, 45.0f );

        {
            auto & G = m_sceneRenderer.getGlobalUniform();

            static double t = 0;
            t += std::chrono::duration<double>( frame.frameTime - frame.lastFrameTime ).count();

            G.TIME_INT      = floor(  static_cast<float>(t) );
            G.TIME_FRAC     = static_cast<float>(t)-G.TIME_INT;
            G.SCREEN_WIDTH  = static_cast<float>( frame.swapchainImageSize.width  );
            G.SCREEN_HEIGHT = static_cast<float>( frame.swapchainImageSize.height );
        }


        // ============= BEGIN RENDERING ========================
        // do not "copy" this RenderPassBeginInfo, since it has
        // pointers to data that is stored in the ScreenB.
        vk::RenderPassBeginInfo RPBI = m_targetGbuffer.getRenderPassBeginInfo();
        cb.beginRenderPass(RPBI, vk::SubpassContents::eInline);

        m_sceneRenderer.resetMatrices();
        m_sceneRenderer.resetMaterials();

        auto camViewMatrix = m_camera.calculateViewMatrix();
        m_sceneRenderer.setCameraMatrices( camViewMatrix, m_camera.projMatrix);
        m_lineRenderer.setCameraMatrices(  camViewMatrix, m_camera.projMatrix);

        m_sceneRenderer.begin(cb);



            if( 1 )
            { // do animations

                // get the current pose of the scene,modelSpaceNodeTransforms
                // which is a vector of node transformations.
                auto pose = m_scene1.getNodeSpaceTransforms();

                static float t=0.0f;
                //t += 0.01;
                t = std::fmod(t, 2.0f);
                m_scene1.calculateAnimationPose(pose, 2, t);

                //m_lightTransform.m_position =  glm::vec3(0.5,1.0,0.5) + glm::vec3( 10.5f*std::cos(t), 0.0f, 10.5f*std::sin(t) );

                // convert the local nodepsace transformas into a
                // model space transforms so that the scene renderer can render
                // it
                auto modelSpaceMatrices = m_scene1.getModelSpaceMatrices(pose);

                m_sceneRenderer.bindPipeline(0);
                m_sceneRenderer.setFrontFace(vk::FrontFace::eCounterClockwise);

                m_sceneRenderer.drawScene( m_scene1,
                                               m_rootSceneTransform,
                                               modelSpaceMatrices);
            }

            {
                m_sceneRenderer.bindPipeline(1);
                m_sceneRenderer.setPolygonMode( vk::PolygonMode::eFill);

                // get the current pose of the scene,modelSpaceNodeTransforms
                // which is a vector of node transformations.
                auto pose = m_gizmoScene.getNodeSpaceTransforms();

                // convert the local nodepsace transformas into a
                // model space transforms so that the scene renderer can render
                // it. This doesn't need to be done every frame
                // only when the nodes have changed position.
                auto modelSpaceMatrices = m_gizmoScene.getModelSpaceMatrices(pose);

                m_sceneRenderer.drawScene( m_gizmoScene,
                                           //0,
                                           m_lightTransform.get_matrix(),
                                           modelSpaceMatrices);

                m_sceneRenderer.bindPipeline(0);
                m_sceneRenderer.drawScene( m_gizmoScene,
                                           1,
                                           m_rootSceneTransform.get_matrix(),
                                           modelSpaceMatrices

                                           );

                m_sceneRenderer.drawScene( m_gizmoScene,
                                           0,
                                           m_rootSceneTransform.get_matrix(),
                                           modelSpaceMatrices);
            }

            m_sceneRenderer.end();


            //================================================================================
            // Do some line rendering.
            // Since the line renderer uses the same output images, wecan use the same render
            // pass as the scene renderer.
            //================================================================================
            m_lineRenderer.beginTopology(vk::PrimitiveTopology::eLineList, m_rootSceneTransform.get_matrix() );

            glm::u8vec4 grey = {0x33, 0x33, 0x33, 255};
            glm::u8vec4 white= {255,255,255,255};

            for(int i = -10 ; i <= 10 ; i++)
            {
                auto c = i%5==0 ? white : grey;
                m_lineRenderer.pushVertex({i  ,0 ,-10}, c);
                m_lineRenderer.pushVertex( {i ,0,10}, c);

                m_lineRenderer.pushVertex( {-10,0 ,  i}, c);
                m_lineRenderer.pushVertex( {10 ,0 , i},  c);
            }

            // m_lineRenderer.pushLineSegment
            // (
            // m_mouseProjection.p.asVec(),
            // glm::u8vec4(255,0,0,255),
            // m_mouseProjection.p.asVec() + m_mouseProjection.v,
            // glm::u8vec4(255,0,0,255));


            m_lineRenderer.begin(cb);
                m_lineRenderer.drawAll();
            m_lineRenderer.end();

            //================================================================================
        cb.endRenderPass();

        // wait for all the PBR's colour attachments to be written
        // before we continue with any using of the fragment shaders
        cb.simplePipelineBarrier(vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                 vk::PipelineStageFlagBits::eFragmentShader);

        //================================================================================
        // Post Process Pass
        // The general idea for the post process stage is to update the uniform
        //
        //================================================================================
        if(1)
        {
            {
                static double _t = 0;
                _t += std::chrono::duration<double>( frame.frameTime - frame.lastFrameTime ).count();
                float t = static_cast<float>(_t);
                struct Light_t
                {
                  glm::vec4  position;
                  glm::vec4  radiance;
                };

                struct uni
                {

                    float SCREEN_WIDTH;
                    float SCREEN_HEIGHT;
                    float MOUSE_X;
                    float MOUSE_Y;

                    int   NUM_LIGHTS;
                    int   UNUSED_0;
                    int   UNUSED_1;
                    int   UNUSED_2;

                    glm::vec4    CAMERA_POSITION;

                    Light_t LIGHT[10];

                };
                uni G;
                //G.TIME_INT      = floor(t);
                //G.TIME_FRAC     = t-floor(t);
                G.SCREEN_WIDTH      = static_cast<float>( frame.swapchainImageSize.width );
                G.SCREEN_HEIGHT     = static_cast<float>( frame.swapchainImageSize.height);
                G.LIGHT[0].position = glm::vec4(m_lightTransform.m_position, 1.0);//glm::vec4(0.5,1.0,0.5,0) + glm::vec4( 3.5f*std::cos(_t), 0.0f, 3.5f*std::sin(_t), 1.0f);
                G.LIGHT[0].radiance = glm::vec4(4,4,4,1);
                G.LIGHT[1].position = glm::vec4(0.5,1.0,0.5,0) + glm::vec4( 3.5f*std::cos(t), 0.0f, 3.5f*std::sin(t), 1.0f);
                G.LIGHT[1].radiance = glm::vec4(4,3,4,1);
                G.CAMERA_POSITION   = glm::vec4(  m_camera.transform.m_position, 1);
                G.NUM_LIGHTS = 2;

                m_lightPass.copyUniformBuffer(&G, sizeof(G));
            }

            //================================================================================
            // After the end of the pevious renderpass, then output ColorAttachments were
            // converted into ShaderReadOnlyOptimal (albedo, position, normal)
            // so that they could be read in by the postprocessing step as
            // Textures.
            //
            // This render pass takes in the ColorAttacments from the previous stage:
            //  albedo, position, normal
            //================================================================================
            vk::RenderPassBeginInfo _RPBI = m_PostProcessingTarget1.getRenderPassBeginInfo();
            cb.beginRenderPass(_RPBI, vk::SubpassContents::eInline);

                m_lightPass.begin(cb);

                    m_lightPass.bindPipeline(0);
                    m_lightPass.bindDescriptorSets();
                    m_lightPass.drawQuad( {-1,-1}, {2,2});


                m_lightPass.end();

            cb.endRenderPass();
            //================================================================================
            // wait for all the PBR's colour attachments to be written
            // before we continue with any using of the fragment shaders
            cb.simplePipelineBarrier(vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                     vk::PipelineStageFlagBits::eFragmentShader);
        }
        //================================================================================


        {
            {
                static double _t = 0;
                _t += std::chrono::duration<double>( frame.frameTime - frame.lastFrameTime ).count();
                float t = static_cast<float>(_t);

                struct uni
                {

                    float TIME_INT;
                    float TIME_FRAC;
                    float SCREEN_WIDTH;
                    float SCREEN_HEIGHT;

                };
                uni G;
                G.TIME_INT      = floor(t);
                G.TIME_FRAC     = t-floor(t);
                G.SCREEN_WIDTH  = static_cast<float>(frame.swapchainImageSize.width);
                G.SCREEN_HEIGHT = static_cast<float>(frame.swapchainImageSize.height);
                m_postProcessRenderer.copyUniformBuffer(&G, sizeof(G));
            }

            //================================================================================
            // After the end of the pevious renderpass, then output ColorAttachments were
            // converted into ShaderReadOnlyOptimal (albedo, position, normal)
            // so that they could be read in by the postprocessing step as
            // Textures.
            //
            // This render pass takes in the ColorAttacments from the previous stage:
            //  albedo, position, normal
            //================================================================================
            vk::RenderPassBeginInfo _RPBI = m_PostProcessingTarget2.getRenderPassBeginInfo();
            cb.beginRenderPass(_RPBI, vk::SubpassContents::eInline);

                m_postProcessRenderer.begin(cb);

                    m_postProcessRenderer.bindPipeline(0);
                    m_postProcessRenderer.bindDescriptorSets();
                    m_postProcessRenderer.drawQuad( {-1,-1}, {2,2});


                m_postProcessRenderer.end();

            cb.endRenderPass();
            //================================================================================
            // wait for all the PBR's colour attachments to be written
            // before we continue with any using of the fragment shaders
            cb.simplePipelineBarrier(vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                     vk::PipelineStageFlagBits::eFragmentShader);
        }
        //================================================================================


        {
            if( depthStencilFormat() != vk::Format::eUndefined)
            {

            }
            VkClearColorValue clearColor = {{ 1, 0, 0, 1 }};
            VkClearDepthStencilValue clearDS = { 1, 0 };
            VkClearValue clearValues[3];
            memset(clearValues, 0, sizeof(clearValues));
            clearValues[0].color        = clearValues[2].color = clearColor;
            clearValues[1].depthStencil = clearDS;

            vk::RenderPassBeginInfo rpBeginInfo;
            rpBeginInfo.renderPass               = frame.defaultRenderPass;
            rpBeginInfo.framebuffer              = frame.currentFrameBuffer;
            rpBeginInfo.renderArea.extent.width  = frame.swapchainImageSize.width;
            rpBeginInfo.renderArea.extent.height = frame.swapchainImageSize.height;

            rpBeginInfo.clearValueCount          = 1;//m_window->sampleCountFlagBits() > VK_SAMPLE_COUNT_1_BIT ? 3 : 2;
            rpBeginInfo.clearValueCount          = depthStencilFormat() == vk::Format::eUndefined ? 1 : 2;
            rpBeginInfo.pClearValues             = reinterpret_cast<vk::ClearValue const*>(clearValues);

            cb.beginRenderPass(rpBeginInfo, vk::SubpassContents::eInline);
                m_texturePresentRenderer.write(cb);
            cb.endRenderPass();
        }



        // After this method returns, the command buffer
        // will be submitted, so we do not need the allocated
        // subbuffers anymore
        for(auto & b : allocatedBuffers)
            m_bufferPoolStaging.free(b);
    }

    vka::FPS_Controller m_camControl;
    vka::OrbitController m_camControlOrbit;
   vka::GizmoController m_GizmoController;

    void tick(double dt)
    {

        m_camControl.step(      &m_camera.transform ,                   static_cast<float>(dt) );


    }

    void mouseMoveEvent(const vka::EvtInputMouseMotion *e) override
    {
        //m_rootSceneTransform.position().z = -2.5;
        if(1)
        {
            if( m_mouse.right)
            {
                m_camControl.lookH( -static_cast<float>(e->xrel) * 0.01f );
                m_camControl.lookV(  static_cast<float>(e->yrel) * 0.01f );
            }

        }

        if( 1)
        {
            if( m_mouse.left)
            {
                m_camControlOrbit.lookH( -static_cast<float>(e->xrel) * 0.01f );
                m_camControlOrbit.lookV(  static_cast<float>(e->yrel) * 0.01f );
            }

        }

        if(m_mouse.middle)
        {
            auto mouseProjectionRay = m_camera.projectMouseRay( glm::vec2(e->x, e->y) );

            m_GizmoController.step( &m_lightTransform, mouseProjectionRay);
        }
    }
    void mousePressEvent(const vka::EvtInputMouseButton *e) override
    {
        if( e->button == vka::MouseButton::LEFT)
        {
            m_mouse.left = true;
        }
        if( e->button == vka::MouseButton::RIGHT)
            m_mouse.right = true;
        if( e->button == vka::MouseButton::MIDDLE)
        {
            m_mouse.middle = true;
        }
    }
    void mouseReleaseEvent(const vka::EvtInputMouseButton *e) override
    {
        if( e->button == vka::MouseButton::LEFT)
            m_mouse.left = false;
        if( e->button == vka::MouseButton::RIGHT)
            m_mouse.right = false;
        if( e->button == vka::MouseButton::MIDDLE)
        {
            m_GizmoController.m_tranform = nullptr;
            m_mouse.middle = false;
            m_mouseProjection = m_camera.projectMouseRay( glm::vec2(e->x, e->y) );
        }
    }
    void keyPressEvent(const vka::EvtInputKey *e) override
    {
        switch(e->keycode)
        {
            case vka::KeyCode::w:
                m_camControl.moveFoward(3);
                break;
            case vka::KeyCode::s:
                m_camControl.moveFoward(-3);
                break;
            case vka::KeyCode::a:
                m_camControl.moveSide(3);
                break;
            case vka::KeyCode::d:
                m_camControl.moveSide(-3);
                break;
            default: break;
        }
        std::cout << "Key Press: " << to_string(e->keycode) << std::endl;;
    }
    void keyReleaseEvent(const vka::EvtInputKey *e) override
    {
        switch(e->keycode)
        {
            case vka::KeyCode::w:
                m_camControl.moveFoward(0);
                break;
            case vka::KeyCode::s:
                m_camControl.moveFoward(0);
                break;
            case vka::KeyCode::a:
                m_camControl.moveSide(0);
                break;
            case vka::KeyCode::d:
                m_camControl.moveSide(0);
                break;
            case vka::KeyCode::o:
                m_texturePresentRenderer.setImage( m_attachmentNormal );
                break;
            default: break;
        }
    }

    struct
    {
        bool forward=false;
    } control;
    struct
    {
        bool left  = false;
        bool middle=false;
        bool right = false;
    } m_mouse;
    //=======================

    vka::line3 m_mouseProjection;
};

#ifndef VKA_NO_SDL_WIDGET

int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;

    // The basic usage of this

    // create a vulkan window widget
    vka::SDLVulkanWidget2 vulkanWindow;

    // create the window and initialize
    // the vka::System
    vulkanWindow.create(1024, 768, debugCallback);

    // instantiate your application
    MyApp app;

    vulkanWindow.exec( &app );


    return 0;
}

#endif
