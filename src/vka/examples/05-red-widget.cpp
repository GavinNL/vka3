#include <iostream>



#include <vka/logging.h>


#include <vka/math.h>

#if !defined VKA_NO_SDL_WIDGET
#include <vka/widgets/SDLWidget.h>
#endif

#include <vka/widgets/Application.h>
#include <vka/core/ShaderModuleCreateInfo.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/System.h>
#include <vka/utils/GLSLCompiler.h>

#define WIDTH  1024
#define HEIGHT 768


VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;

    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}


class MyApp : public vka::Application
{
    // Application interface
public:
    vk::Pipeline m_trianglePipeline;

    /**
     * @brief initResources
     *
     * This will be called right after system gets created.
     * It can be used to initialize any vulkan related resources
     * that are not related to the swapchain.
     */
    void initResources() override
    {
        glslang::InitializeProcess();
        // use this to render to the swapchain
        vka::GraphicsPipelineCreateInfo4 P;

        auto compileShaderModule = [&](const std::string & path, EShLanguage language)
        {
            vka::GLSLCompiler compiler;
            auto spv   = compiler.compileFile(path, language  );

            vka::ShaderModuleCreateInfo2 createInfo;
            createInfo.code = spv;
            return vka::System::get().createShaderModule(createInfo);
        };

        P.vertexShader   = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/triangle.vert", EShLangVertex);
        P.fragmentShader = compileShaderModule(VKA_CMAKE_SOURCE_DIR  "/share/shaders/triangle.frag", EShLangFragment);
        P.renderPass     = getDefaultRenderPass();
        //P.vertexInputFormats = {
        //                            vk::Format::eR32G32B32Sfloat, // these aren't actually used in the shader
        //                            vk::Format::eR32G32B32Sfloat, // these aren't actually used in the shader
        //                            vk::Format::eR32G32B32Sfloat, // these aren't actually used in the shader
        //                       };
        m_trianglePipeline = vka::System::get().createGraphicsPipeline(P);

    }
    void releaseResources() override
    {
        // destroy the pipeline and the shadermodules if they are not
        // being used
        vka::System::get().destroyPipeline(m_trianglePipeline, true);

        glslang::FinalizeProcess();
        std::cout << "release non-graphics resources" << std::endl;
    }


    /**
     * @brief initSwapChainResources
     *
     * This is called right after the swapchain is created.
     * You can use this to initialize any swapchain related resources,
     * for example, render targets (because they change with swapchain size)
     */
    void initSwapChainResources() override
    {
        std::cout << "init swapchain releated resources" << std::endl;
    }
    void releaseSwapChainResources() override
    {
        std::cout << "release swapchain releated resources" << std::endl;
    }

    /**
     * @brief render
     * @param frame
     *
     * This is the widget's main vulkan render function.
     * use this to actually record your command buffers.
     * submission of commandbuffers are automatically done
     * at the end
     */
    void render(vka::ApplicationFrame & frame) override
    {
        auto & cb = frame.currentCommandBuffer;

        VkClearColorValue clearColor = {{ 1, 0, 0, 1 }};
        VkClearDepthStencilValue clearDS = { 1, 0 };
        VkClearValue clearValues[3];
        memset(clearValues, 0, sizeof(clearValues));
        clearValues[0].color = clearValues[2].color = clearColor;
        clearValues[1].depthStencil = clearDS;

        vk::RenderPassBeginInfo rpBeginInfo;
        rpBeginInfo.renderPass               = frame.defaultRenderPass;//m_window->defaultRenderPass();
        rpBeginInfo.framebuffer              = frame.currentFrameBuffer;//m_window->currentFramebuffer();
        rpBeginInfo.renderArea.extent.width  = frame.swapchainImageSize.width;
        rpBeginInfo.renderArea.extent.height = frame.swapchainImageSize.height;
        rpBeginInfo.clearValueCount          = 1;//m_window->sampleCountFlagBits() > VK_SAMPLE_COUNT_1_BIT ? 3 : 2;
        rpBeginInfo.pClearValues             = reinterpret_cast<vk::ClearValue const*>(clearValues);

        cb.beginRenderPass(rpBeginInfo, vk::SubpassContents::eInline);

        cb.bindPipeline( vk::PipelineBindPoint::eGraphics, m_trianglePipeline);

            vk::Viewport vp(0,0, static_cast<float>(frame.swapchainImageSize.width), static_cast<float>(frame.swapchainImageSize.height),0,1);
            vk::Rect2D scissor( vk::Offset2D(0,0),  frame.swapchainImageSize);
            cb.setViewport(0, vp);
            cb.setScissor(0, scissor);

                cb.draw(3,1,0,0);


        cb.endRenderPass();

    }

    void mouseMoveEvent(const vka::EvtInputMouseMotion *e) override
    {
        (void)e;
    }
    void mousePressEvent(const vka::EvtInputMouseButton *e) override
    {
        (void)e;
    }
    void mouseReleaseEvent(const vka::EvtInputMouseButton *e) override
    {
        (void)e;
    }
    void keyPressEvent(const vka::EvtInputKey *e) override
    {
        (void)e;
    }
    void keyReleaseEvent(const vka::EvtInputKey *e) override
    {
        (void)e;
    }
};

#if !defined VKA_NO_SDL_WIDGET
int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;
    // The basic usage of this

    // create a vulkan window widget
    vka::SDLVulkanWidget2 vulkanWindow;

    // create the window and initialize
    // the vka::System
    vulkanWindow.create(1024, 768, debugCallback);


    // instantiate your application
    MyApp app;


    vulkanWindow.exec( &app );


    return 0;
}

#endif
