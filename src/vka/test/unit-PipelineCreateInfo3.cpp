#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/TextureMemoryPool.h>
#include <vka/core/Primatives.h>
#include <vka/utils/HostImage.h>
#include <vka/core/RenderPassCreateInfo.h>

#include <vka/utils/GLSLCompiler.h>


#define WIDTH  1024
#define HEIGHT 768

#include "test_init_functions.hpp"


SCENARIO( "Creating Pipelines from GLSL code" )
{
    glslang::InitializeProcess();

    init();
    auto & System = vka::System::get();

    // Lambda to create shadermodules from glsl code
    auto compileShaderModule = [&](const std::string & path, EShLanguage language)
    {
        vka::GLSLCompiler compiler;
        auto spv   = compiler.compileFile(path, language  );

        vka::ShaderModuleCreateInfo2 createInfo;
        createInfo.code = spv;
        return System.createShaderModule(createInfo);
    };

    {
        // First compile each of the shaders into a Spir-V module
        auto vert_m = compileShaderModule( VKA_CMAKE_SOURCE_DIR "/share/shaders/simple_line_shader.vert", EShLangVertex);
        auto frag_m = compileShaderModule( VKA_CMAKE_SOURCE_DIR "/share/shaders/simple_line_shader.frag", EShLangFragment);

        // Next create a RenderPass which will render directly to
        // the swapchain and not have a depth buffer
        vka::RenderPassCreateInfo2 rp;
        rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::ePresentSrcKHR);

        auto renderPass = System.createRenderPass(rp);

        REQUIRE( renderPass);

        vka::GraphicsPipelineCreateInfo4 p;

        p.vertexShader       = vert_m;
        p.fragmentShader     = frag_m;
        p.renderPass         = renderPass;
        p.vertexInputFormats = { vk::Format::eR32G32B32Sfloat,
                                 vk::Format::eR32G32B32Sfloat};

        auto pipeline = System.createGraphicsPipeline(p);

        REQUIRE( pipeline);

        //VULKAN.device.destroyPipeline(pipeline);
        //VULKAN.device.destroyRenderPass(renderPass);
        //VULKAN.device.destroyShaderModule(vert_m);
        //VULKAN.device.destroyShaderModule(vert_m);

        System.destroy();
    }


    destroy();
    glslang::FinalizeProcess();
}
