#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
//#include <vka/BufferMemoryPool.h>
//#include <vka/DescriptorSets.h>
//#include <vka/PipelineCreateInfo.h>
//#include <vka/DeviceMeshPrimitive.h>
//#include <vka/TextureMemoryPool.h>
//#include <vka/Primatives.h>
//#include <vka/HostImage.h>

// if using this header, you will need to link with the following
// libraries provided by glslang:
// SPIRV glslang HLSL OGLCompiler OSDependent
#include <vka/utils/GLSLCompiler.h>

#include <vka/core/RenderPassCreateInfo.h>
#include <vka/core/DescriptorSetLayoutCreateInfo.h>
#include <vka/core/PipelineLayoutCreateInfo.h>

#include "test_init_functions.hpp"

SCENARIO( "Compiling a shader twice and creating a ShaderModule only creates it once" )
{
    // Required to be able to compile glsl->spirv
    glslang::InitializeProcess();

    init();
    auto & System = vka::System::get();



    vka::GLSLCompiler compiler;


    vk::ShaderModule shader1;
    vk::ShaderModule shader2;


    {
        auto vert_spv = compiler.compileFile( VKA_CMAKE_SOURCE_DIR "/share/shaders/scene_cpn.vert", EShLangVertex);


        vka::ShaderModuleCreateInfo2 createInfo;
        createInfo.code = vert_spv;

        shader1 = System.createShaderModule(createInfo);

        REQUIRE( shader1 );
    }

    {
        auto vert_spv = compiler.compileFile( VKA_CMAKE_SOURCE_DIR "/share/shaders/scene_cpn.vert", EShLangVertex);


        vka::ShaderModuleCreateInfo2 createInfo;
        createInfo.code = vert_spv;

        shader2 = System.createShaderModule(createInfo);

        REQUIRE( shader2 );
    }

    // same source code does not recompile into a new shader module
    REQUIRE( shader1 == shader2 );



    destroy();

    glslang::FinalizeProcess();
}
