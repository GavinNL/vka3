#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/TextureMemoryPool.h>
#include <vka/core/Primatives.h>
#include <vka/utils/HostImage.h>
#include <vka/core/RenderPassCreateInfo.h>

#include <vka/utils/GLSLCompiler.h>

#include <vka/utils/SpecialPipeline.h>

#define WIDTH  1024
#define HEIGHT 768

#include "test_init_functions.hpp"


SCENARIO( "Creating Pipelines from GLSL code" )
{
    glslang::InitializeProcess();

    init();
    auto & System = vka::System::get();

    // Lambda to create shadermodules from glsl code
    auto compileShaderModule = [&](const std::string & path, EShLanguage language)
    {
        vka::GLSLCompiler compiler;
        auto spv   = compiler.compileFile(path, language  );

        vka::ShaderModuleCreateInfo2 createInfo;
        createInfo.code = spv;
        return System.createShaderModule(createInfo);
    };

    {
        // First compile each of the shaders into a Spir-V module
        auto vert_m = compileShaderModule( VKA_CMAKE_SOURCE_DIR "/share/shaders/simple_line_shader.vert", EShLangVertex);
        auto frag_m = compileShaderModule( VKA_CMAKE_SOURCE_DIR "/share/shaders/simple_line_shader.frag", EShLangFragment);

        // Next create a RenderPass which will render directly to
        // the swapchain and not have a depth buffer
        vka::RenderPassCreateInfo2 rp;
        rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::ePresentSrcKHR);

        auto renderPass = System.createRenderPass(rp);

        REQUIRE( renderPass);

        vka::GraphicsPipelineCreateInfo4 p;

        p.vertexShader       = vert_m;
        p.fragmentShader     = frag_m;
        p.renderPass         = renderPass;
        p.enableBlending     = false;
        p.enableDepthTest    = false;
        p.enableDepthWrite   = false;
        p.vertexInputFormats = { vk::Format::eR32G32B32Sfloat,
                                 vk::Format::eR32G32B32Sfloat};



        //========================================================================
        // MAIN TEST
        //========================================================================

        vka::SpecialPipeline S;

        // Initialize the pipeline with the
        // initial state
        S.init(p);
        REQUIRE( S.numPipelines() == 0);

        // getting the current pipeline shouldn't
        // recompile a new shader
        auto p1 = S.getCurrentPipeline();
        REQUIRE( p1 );

        REQUIRE( S.numPipelines() == 1 );


        {
            S.enableDepthTest(true);

            auto p2 = S.getCurrentPipeline();

            // pipelines are different
            REQUIRE( p1 != p2 );

            // new pipeline was created
            REQUIRE( S.numPipelines() == 2);

            S.enableDepthTest(false);

            auto p3 = S.getCurrentPipeline();
            REQUIRE( S.numPipelines() == 2);

            // same as original pipeline
            REQUIRE( p3 == p1);

        }

        {
            S.enableDepthWrite(true);

            auto p2 = S.getCurrentPipeline();

            // pipelines are different
            REQUIRE( p1 != p2 );

            // new pipeline was created
            REQUIRE( S.numPipelines() == 3);

            S.enableDepthWrite(false);

            auto p3 = S.getCurrentPipeline();
            REQUIRE( S.numPipelines() == 3);

            // same as original pipeline
            REQUIRE( p3 == p1);

        }

        {
            S.enableBlending(true);

            auto p2 = S.getCurrentPipeline();

            // pipelines are different
            REQUIRE( p1 != p2 );

            // new pipeline was created
            REQUIRE( S.numPipelines() == 4);

            S.enableBlending(false);

            auto p3 = S.getCurrentPipeline();
            REQUIRE( S.numPipelines() == 4);

            // same as original pipeline
            REQUIRE( p3 == p1);

        }

        {
            S.setTopology(vk::PrimitiveTopology::eTriangleFan);

            auto p2 = S.getCurrentPipeline();

            // pipelines are different
            REQUIRE( p1 != p2 );

            // new pipeline was created
            REQUIRE( S.numPipelines() == 5);

            S.setTopology(vk::PrimitiveTopology::eTriangleList);

            auto p3 = S.getCurrentPipeline();
            REQUIRE( S.numPipelines() == 5);

            // same as original pipeline
            REQUIRE( p3 == p1);

        }

        {
            S.setFrontFace(vk::FrontFace::eClockwise);

            auto p2 = S.getCurrentPipeline();

            // pipelines are different
            REQUIRE( p1 != p2 );

            // new pipeline was created
            REQUIRE( S.numPipelines() == 6);

            S.setFrontFace(vk::FrontFace::eCounterClockwise);

            auto p3 = S.getCurrentPipeline();
            REQUIRE( S.numPipelines() == 6);

            // same as original pipeline
            REQUIRE( p3 == p1);

        }

        {
            S.setPolygonMode(vk::PolygonMode::eLine);

            auto p2 = S.getCurrentPipeline();

            // pipelines are different
            REQUIRE( p1 != p2 );

            // new pipeline was created
            REQUIRE( S.numPipelines() == 7);

            S.setPolygonMode(vk::PolygonMode::eFill);

            auto p3 = S.getCurrentPipeline();
            REQUIRE( S.numPipelines() == 7);

            // same as original pipeline
            REQUIRE( p3 == p1);

        }
        //========================================================================



        System.destroy();
    }


    destroy();
    glslang::FinalizeProcess();
}
