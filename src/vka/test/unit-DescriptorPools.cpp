#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/DescriptorSets.h>

#include <vka/core/RenderPassCreateInfo.h>
#include <vka/core/DescriptorSetLayoutCreateInfo.h>

#include "test_init_functions.hpp"


SCENARIO( "Creating DescriptorPools" )
{
    init();
    auto & System = vka::System::get();

    vka::DescriptorPoolCreateInfo2 I;
    I.maxSets = 1000;
    I.poolSizes.push_back( {vk::DescriptorType::eCombinedImageSampler, 1000 });
    I.poolSizes.push_back( {vk::DescriptorType::eSampledImage, 1000 });
    I.poolSizes.push_back( {vk::DescriptorType::eUniformBuffer, 1000 });
    I.poolSizes.push_back( {vk::DescriptorType::eStorageBuffer, 1000 });

    auto dPool = System.createDescriptorPool( I );

    auto & dPoolInfo = System.info(dPool);

    REQUIRE(dPool);

    {
        // create a set using a layout
        vka::DescriptorSetLayoutCreateInfo2 layoutCreateInfo;
        layoutCreateInfo.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);
        layoutCreateInfo.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);

        auto layout = System.createDescriptorSetLayout(layoutCreateInfo);

        auto & layoutInfo = System.info(layout);

        // allocate a set from the pool using the layout.
        auto set = System.allocateDescriptorSet( dPool, layout);

        REQUIRE( set );

        // the layout is now linked to the descriptor set that was created
        REQUIRE( layoutInfo.descriptorSets.size() == 1);
        REQUIRE( layoutInfo.descriptorSets.count(set) == 1 );

        // the descriptor pool is keeping track of which sets it allocated
        REQUIRE( dPoolInfo.sets.count(set) == 1 );

        // allocate a descritor set using a DescriptorSetLayoutCreateInfo2
        {
            vka::DescriptorSetLayoutCreateInfo2 layoutCreateInfo2;
            layoutCreateInfo2.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);
            layoutCreateInfo2.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);

            auto set1 = System.allocateDescriptorSet( dPool, layoutCreateInfo2);

            // since the layout is the same, it should NOT have created a new layout
            REQUIRE( layoutInfo.descriptorSets.size() == 2);
            REQUIRE( layoutInfo.descriptorSets.count(set1) == 1 );

        }
    }

    destroy();
}
