#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
//#include <vka/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
//#include <vka/PipelineCreateInfo.h>
//#include <vka/DeviceMeshPrimitive.h>
//#include <vka/TextureMemoryPool.h>
//#include <vka/Primatives.h>
//#include <vka/HostImage.h>

#include <vka/core/RenderPassCreateInfo.h>
#include <vka/core/DescriptorSetLayoutCreateInfo.h>


#include "test_init_functions.hpp"


SCENARIO("Adding the same binding doesn't increase the binding count.")
{
    vka::DescriptorSetLayoutCreateInfo2 ci;

    ci.addDescriptor(0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);
    ci.addDescriptor(0, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eFragment);

    REQUIRE( ci.bindings.size() == 1);

    vk::ShaderStageFlags f = vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment;
    REQUIRE( ci.bindings[0].stageFlags ==  f);
    REQUIRE( ci.bindings[0].binding    ==  0);
}

SCENARIO("Adding multiple bindings always keeps the vector sorted")
{
    vka::DescriptorSetLayoutCreateInfo2 ci;

    ci.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);
    ci.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);

    REQUIRE( ci.bindings.size() == 2);

    REQUIRE( ci.bindings[0].binding         ==  0);
    REQUIRE( ci.bindings[0].stageFlags      ==  vk::ShaderStageFlagBits::eFragment);
    REQUIRE( ci.bindings[0].descriptorType  ==  vk::DescriptorType::eSampledImage);
    REQUIRE( ci.bindings[0].descriptorCount ==  2);

    REQUIRE( ci.bindings[1].binding         ==  1);
    REQUIRE( ci.bindings[1].stageFlags      ==  vk::ShaderStageFlagBits::eVertex);
    REQUIRE( ci.bindings[1].descriptorType  ==  vk::DescriptorType::eUniformBuffer);
    REQUIRE( ci.bindings[1].descriptorCount ==  1);

}

SCENARIO("Hash are same if the layout is the same")
{
    vka::DescriptorSetLayoutCreateInfo2 CI1;
    vka::DescriptorSetLayoutCreateInfo2 CI2;

    CI1.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);
    CI1.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);

    CI2.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);
    CI2.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);

    std::hash<vka::DescriptorSetLayoutCreateInfo2> H;
    REQUIRE( H(CI1) == H(CI2));
}

SCENARIO("Hash are different if the layout is different")
{
    vka::DescriptorSetLayoutCreateInfo2 CI1;
    vka::DescriptorSetLayoutCreateInfo2 CI2;

    CI1.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);
    CI1.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);

    CI2.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);
    CI2.addDescriptor(1, vk::DescriptorType::eSampledImage, 1, vk::ShaderStageFlagBits::eVertex);

    std::hash<vka::DescriptorSetLayoutCreateInfo2> H;
    REQUIRE( H(CI1) != H(CI2));
}

SCENARIO( "CreatingDescriptor Set Layouts using the same info does not create multiple layouts." )
{
    init();
    auto & System = vka::System::get();

    {
        vka::DescriptorSetLayoutCreateInfo2 CI1;
        vka::DescriptorSetLayoutCreateInfo2 CI2;

        CI1.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);
        CI1.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);

        CI2.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);
        CI2.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);

        auto l1 = System.createDescriptorSetLayout(CI1);
        auto l2 = System.createDescriptorSetLayout(CI2);

        REQUIRE( l1 );
        REQUIRE( l1 == l2 );
    }


    destroy();
}
