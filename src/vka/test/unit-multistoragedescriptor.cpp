#include "catch.hpp"

#include <iostream>

#if defined(__clang__)
#pragma clang diagnostic ignored "-Wkeyword-macro"
#elif defined(__GNUC__) || defined(__GNUG__)
#pragma GCC diagnostic push
#elif defined(_MSC_VER)

#endif



#define private public
#include <vka/Renderers/MultiStorageDescriptor.h>
#undef private

SCENARIO("regex test 1")
{
    GIVEN("A raw buffer which is used as the data for the MultiStorageData")
    {

        vka::MultiStorageDescriptor_t D;
        std::vector<uint8_t> rawData(128);

        D.m_info = std::make_shared<vka::MultiStorageDescriptor_t::Info_t>() ;
        D.m_info->m_begin   = rawData.data();
        D.m_info->m_end     = 0;
        D.m_info->m_maxSize = 32;

        // float array indices:   0       1       2       3       4       5        6      7
        //                       +---------------+---------------+---------------+---------------+
        //                       | float |       | vec2          | float | float | float | float |
        //                       +---------------+---------------+---------------+---------------+
        // vec2  array indices:   0               1               2               3

        WHEN("We push a float, vec2, float,float,float,float into the buffer")
        {
            REQUIRE( D.push( float(3.2f) ) == 0); // returns the index as if the raw buffer was an array of floats
            REQUIRE( D.push( glm::vec2() ) == 1); // returns the index as if the raw buffer was an array of vec2
            REQUIRE( D.push( float(3.2f) ) == 4);
            REQUIRE( D.push( float(3.2f) ) == 5);
            REQUIRE( D.push( float(3.2f) ) == 6);


            REQUIRE( D.push( glm::vec2() ) == 0);
            REQUIRE( D.push( float(3.2f) ) == 2); // looped back
            REQUIRE( D.push( float(3.2f) ) == 3); // looped back
            REQUIRE( D.push( glm::vec2() ) == 2);
        }
    }


}
