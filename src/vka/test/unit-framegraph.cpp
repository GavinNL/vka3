#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/TextureMemoryPool.h>
#include <vka/core/Primatives.h>
#include <vka/utils/HostImage.h>
#include <vka/core/RenderPassCreateInfo.h>

#include "test_init_functions.hpp"

#define protected public
#include <vka/utils/FrameGraph.h>
#undef protected

using namespace vka;

SCENARIO("")
{

}


SCENARIO( "Creating two render passes with the same input will result in the same renderpass being returned" )
{
    init();
    auto & System = vka::System::get();

    {

        vka::RenderPassCreateInfo2 rp;

        rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::ePresentSrcKHR);

        auto rpci = rp.generateRenderPassCreateInfo();

        auto renderPass = System.device().createRenderPass(rpci);

        REQUIRE( renderPass);

        System.device().destroyRenderPass(renderPass);
    }

    {
        vka::RenderPassCreateInfo2 rp;

        rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::ePresentSrcKHR);

        auto rpci = rp.generateRenderPassCreateInfo();

        auto renderPass = System.device().createRenderPass(rpci);

        REQUIRE( renderPass);

        System.device().destroyRenderPass(renderPass);
    }

    {
        vka::RenderPassCreateInfo2 rp;

        rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm,         vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addColorAttachment( vk::Format::eR32G32B32A32Sfloat,    vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addColorAttachment( vk::Format::eR32G32B32A32Sfloat,    vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addColorAttachment( vk::Format::eR32G32B32A32Sfloat,    vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addColorAttachment( vk::Format::eR32G32B32A32Sfloat,    vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addDepthStenchilAttachment(vk::Format::eD24UnormS8Uint, vk::ImageLayout::eShaderReadOnlyOptimal);

        auto rpci = rp.generateRenderPassCreateInfo();

        auto renderPass = System.device().createRenderPass(rpci);

        REQUIRE( renderPass);

        System.device().destroyRenderPass(renderPass);
    }


    {
        vk::RenderPass renderPass;
        {

            vka::RenderPassCreateInfo2 rp;

            rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm , vk::ImageLayout::ePresentSrcKHR );

            renderPass = System.createRenderPass(rp);

            REQUIRE( renderPass);
        }
        {

            vka::RenderPassCreateInfo2 rp;

            rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm , vk::ImageLayout::ePresentSrcKHR );

            auto renderPass2 = System.createRenderPass(rp);

            REQUIRE( renderPass2);
            REQUIRE( renderPass2 == renderPass);
        }
    }


    {
        FrameGraph graph;


        graph.addTask("gbuffer")
                .addColorOutput("albedo"  , vk::Format::eR8G8B8A8Unorm      )
                .addColorOutput("position", vk::Format::eR32G32B32A32Sfloat )
                .addColorOutput("normal"  , vk::Format::eR32G32B32A32Sfloat )
                .addColorOutput("depth"   , vk::Format::eD32Sfloat          )
                .setClearColorCallback(
                    [](uint32_t index, vk::Format format) -> vk::ClearColorValue
                    {
                        (void)index;
                        (void)format;
                        return vk::ClearColorValue();
                    })
                .setClearDepthCallback(
                    [](vk::Format format) -> vk::ClearDepthStencilValue
                    {
                        (void)format;
                        return vk::ClearDepthStencilValue();
                    })
                .setCommandBufferWrite(
                    [](vka::RenderTaskVar & c)
                    {
                        (void)c;
                        std::cout << "Writing gbuffer commands" << std::endl;
                    }
                    );

        graph.addTask("lightPass")
             .addAttachmentInput("albedo"  )   // Input attachments are essentially textures
             .addAttachmentInput("position")   // which have been written to by a prevoius render pass
             .addAttachmentInput("normal")     // eg: Not static textures which have been loaded by an external .jpg file
             .addAttachmentInput("shadowMap1") //
             .addAttachmentInput("shadowMap2") //
             .addAttachmentInput("shadowMap3") //
             .addColorOutput("full", vk::Format::eR8G8B8A8Unorm, vk::Extent2D(1024,768))
             .setClearColorCallback(
                 [](uint32_t index, vk::Format format) -> vk::ClearColorValue
                 {
                    (void)index;
                    (void)format;
                     return vk::ClearColorValue();
                 })
             .setClearDepthCallback(
                 [](vk::Format format) -> vk::ClearDepthStencilValue
                 {
                    (void)format;
                     return vk::ClearDepthStencilValue();
                 })
             .setCommandBufferWrite(
                 []( vka::RenderTaskVar & c)
                 {
                     (void)c;
                     std::cout << "Writing lightPass commands" << std::endl;
                 }
                );

        graph.addTask("shadowPass")
                .addColorOutput("shadowMap1"  , vk::Format::eR32Sfloat, vk::Extent2D(1024,768) )
                .addColorOutput("shadowMap2"  , vk::Format::eR32Sfloat, vk::Extent2D(1024,768) )
                .addColorOutput("shadowMap3"  , vk::Format::eR32Sfloat, vk::Extent2D(1024,768) )
                .setClearColorCallback(
                    [](uint32_t index, vk::Format format) -> vk::ClearColorValue
                    {
                        (void)index;
                        (void)format;
                        return vk::ClearColorValue();
                    })
                .setClearDepthCallback(
                    [](vk::Format format) -> vk::ClearDepthStencilValue
                    {
                        (void)format;
                        return vk::ClearDepthStencilValue();
                    })
                .setCommandBufferWrite(
                []( vka::RenderTaskVar & c)
                {
                    (void)c;
                    std::cout << "Writing shadowPass commands" << std::endl;
                }
               );
        graph.setFinalOutput("full");

        graph.setSwapchainExtent( vk::Extent2D(1024,768));



        // compiles the graph and
        // creates all the RenderPasses and FrameBuffers.


        graph.compile();

        std::cout << "=======================================\n";
        graph.dotGraph(std::cout);
        std::cout << "=======================================\n";



        graph.checkReinitalizeResources();


        // create a default command pool
        auto CP = vka::System::get().createCommandPool();
        auto cmd = vka::System::get().allocateCommandBuffer( vk::CommandBufferLevel::ePrimary, CP);


        cmd.reset(vk::CommandBufferResetFlagBits::eReleaseResources);

        // Execute all RenderPasses on a single command buffer.
        cmd.begin( vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse) );

            graph.execute(cmd);

        cmd.end();


        {
            vk::SubmitInfo info;
            info.waitSemaphoreCount   = 0;
            //info.pWaitSemaphores      = &imageAvailableSemaphore;
            info.signalSemaphoreCount = 0;
            //info.pSignalSemaphores    = &renderCompleteSemaphore;
            info.commandBufferCount   = 1;
            info.pCommandBuffers      = &cmd;

            vk::PipelineStageFlags wait_stage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
            info.pWaitDstStageMask = &wait_stage;

            vka::System::get().submitCommandBuffers(info);
            vka::System::get().device().waitIdle();
        }






        graph.setSwapchainExtent( vk::Extent2D(1920,1080) );
        graph.checkReinitalizeResources();


        graph.destroy();
        std::cout << "---------------------------------------------" << std::endl;

    }
    destroy();
}




