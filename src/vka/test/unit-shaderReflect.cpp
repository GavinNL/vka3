#include "catch.hpp"
#include <vka/core/System.h>
#include <vka/core/ShaderReflect.h>
#include <vka/utils/GLSLCompiler.h>

SCENARIO( "1" )
{
    glslang::InitializeProcess();

    vka::GLSLCompiler compiler;
    auto srcCode = compiler.compileFile( VKA_CMAKE_SOURCE_DIR "/share/shaders/triangle.vert", EShLangVertex);


    vka::ShaderReflect R;
    R.parseSPIRV(srcCode);


    REQUIRE( R.inputs.size() == 0 );
    //REQUIRE( R.inputs[0].format == vk::Format::eR32G32B32Sfloat);
    //REQUIRE( R.inputs[0].name   == "f_Color");

    REQUIRE( R.outputs.size()      == 3 );

    REQUIRE( R.outputs[0].format   == vk::Format::eR32G32B32Sfloat);
    REQUIRE( R.outputs[0].name     == "f_Color");
    REQUIRE( R.outputs[0].location == 0);

    REQUIRE( R.outputs[1].format   == vk::Format::eR32G32B32Sfloat);
    REQUIRE( R.outputs[1].name     == "f_Position");
    REQUIRE( R.outputs[1].location == 1);

    REQUIRE( R.outputs[2].format   == vk::Format::eR32G32B32Sfloat);
    REQUIRE( R.outputs[2].name     == "f_Normal");
    REQUIRE( R.outputs[2].location == 2);
}


SCENARIO( "2" )
{
    vka::GLSLCompiler compiler;
    auto srcCode = compiler.compileFile( VKA_CMAKE_SOURCE_DIR "/share/shaders/simple_line_shader.vert", EShLangVertex);

    vka::ShaderReflect R;
    R.parseSPIRV(srcCode);


    REQUIRE( R.inputs.size()    == 2 );
    REQUIRE( R.inputs[0].format == vk::Format::eR32G32B32Sfloat);
    REQUIRE( R.inputs[0].name   == "in_Position");

    REQUIRE( R.inputs[1].format == vk::Format::eR32G32B32Sfloat);
    REQUIRE( R.inputs[1].name   == "in_Color");


    REQUIRE( R.outputs.size()      == 1 );
    REQUIRE( R.outputs[0].format   == vk::Format::eR32G32B32A32Sfloat);
    REQUIRE( R.outputs[0].name     == "f_Color");
    REQUIRE( R.outputs[0].location == 0);


    REQUIRE( R.pushConstants.name  == "pushConsts");
    REQUIRE( R.pushConstants.size  == 16*sizeof(float));

    glslang::FinalizeProcess();
}


