#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
//#include <vka/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
//#include <vka/PipelineCreateInfo.h>
//#include <vka/DeviceMeshPrimitive.h>
//#include <vka/TextureMemoryPool.h>
//#include <vka/Primatives.h>
//#include <vka/HostImage.h>

#include <vka/core/RenderPassCreateInfo.h>
#include <vka/core/DescriptorSetLayoutCreateInfo.h>
#include <vka/core/PipelineLayoutCreateInfo.h>


#include "test_init_functions.hpp"


SCENARIO( "Creating two pipeline layouts with the same input will result in the same layout being returned" )
{
    init();
    auto & System = vka::System::get();

    vk::PipelineLayout layout1;
    vk::PipelineLayout layout2;


    {
        vka::DescriptorSetLayoutCreateInfo2 CI1;
        vka::DescriptorSetLayoutCreateInfo2 CI2;

        CI1.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);
        CI1.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);

        CI2.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eGeometry);
        CI2.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);

        auto l1 = System.createDescriptorSetLayout(CI1);
        REQUIRE( l1 );

        auto l2 = System.createDescriptorSetLayout(CI2);
        REQUIRE( l2 );

        REQUIRE( l1 != l2 );


        {
            vka::PipelineLayoutCreateInfo2 pCI;

            pCI.SetLayouts.push_back(l1);
            pCI.SetLayouts.push_back(l2);

            layout1 = System.createPipelineLayout(pCI);

            REQUIRE(layout1);
        }
    }


    {
        vka::DescriptorSetLayoutCreateInfo2 CI1;
        vka::DescriptorSetLayoutCreateInfo2 CI2;

        CI1.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eFragment);
        CI1.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);

        CI2.addDescriptor(0, vk::DescriptorType::eSampledImage , 2, vk::ShaderStageFlagBits::eGeometry);
        CI2.addDescriptor(1, vk::DescriptorType::eUniformBuffer, 1, vk::ShaderStageFlagBits::eVertex);

        auto l1 = System.createDescriptorSetLayout(CI1);
        REQUIRE( l1 );

        auto l2 = System.createDescriptorSetLayout(CI2);
        REQUIRE( l2 );

        REQUIRE( l1 != l2 );


        {
            vka::PipelineLayoutCreateInfo2 pCI;

            pCI.SetLayouts.push_back(l1);
            pCI.SetLayouts.push_back(l2);

            layout2 = System.createPipelineLayout(pCI);

            REQUIRE(layout2);
        }
    }

    // since they were created with the same DescriptorSetLayouts
    REQUIRE(layout1 == layout2);

    destroy();
}
