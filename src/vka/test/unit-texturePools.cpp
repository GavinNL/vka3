#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
//#include <vka/BufferMemoryPool.h>
//#include <vka/DescriptorSets.h>
//#include <vka/PipelineCreateInfo.h>
//#include <vka/DeviceMeshPrimitive.h>
#include <vka/core/TextureMemoryPool.h>
//#include <vka/Primatives.h>
//#include <vka/HostImage.h>

// if using this header, you will need to link with the following
// libraries provided by glslang:
// SPIRV glslang HLSL OGLCompiler OSDependent
#include <vka/utils/GLSLCompiler.h>

#include <vka/core/RenderPassCreateInfo.h>
#include <vka/core/DescriptorSetLayoutCreateInfo.h>
#include <vka/core/PipelineLayoutCreateInfo.h>


#include "test_init_functions.hpp"

SCENARIO( "Creating texture pools" )
{
    init();
    auto & System = vka::System::get();

    // A texture pool with a base texture of RGBA8 will be able
    // to allocate textures formats of R8, RG8, and RGBA8
    // it WONT be able to do   RGB8
    auto texturePool =  System.createTexturePool( 50*1024*1024,
                               vk::Format::eR8G8B8A8Unorm,
                               vk::MemoryPropertyFlagBits::eDeviceLocal,
                               vk::ImageUsageFlagBits::eSampled
                               | vk::ImageUsageFlagBits::eTransferSrc
                               | vk::ImageUsageFlagBits::eTransferDst);

    auto outImg = texturePool.createNewImage2D( vk::Format::eR8G8B8A8Unorm, vk::Extent2D(1024,1024), 1, 1);


    REQUIRE(outImg);

    texturePool.destroyImage(outImg);


    auto outImageCube = texturePool.createNewImageCube( vk::Format::eR8G8B8A8Unorm, vk::Extent2D(256,256), 1);
    REQUIRE(outImageCube);



    auto outImage1 = texturePool.createNewImage2D( vk::Format::eR8Unorm, vk::Extent2D(256,256), 1, 1);
    REQUIRE(outImage1);

    auto outImage2 = texturePool.createNewImage2D( vk::Format::eR8G8Unorm, vk::Extent2D(256,256), 1, 1);
    REQUIRE(outImage2);


    texturePool.destroyImage(outImageCube);


    System.destroyTexturePool(texturePool);



    destroy();

}
