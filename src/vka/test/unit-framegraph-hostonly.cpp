#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/TextureMemoryPool.h>
#include <vka/core/Primatives.h>
#include <vka/utils/HostImage.h>
#include <vka/core/RenderPassCreateInfo.h>

#include "test_init_functions.hpp"

#if defined(__clang__)
#pragma clang diagnostic ignored "-Wkeyword-macro"
#elif defined(__GNUC__) || defined(__GNUG__)
#pragma GCC diagnostic push
#elif defined(_MSC_VER)

#endif

#define protected public
#include <vka/utils/FrameGraph.h>
#undef protected

using namespace vka;

SCENARIO("Create a host-only frame graph (no rendering) ")
{
    FrameGraph G;

    G.addRenderTask("task1",
                  []( vka::RenderTaskVar & c)
                  {
                      (void)c;
                  })
                .addHostResourceOutput("v1")
    ;
    G.addRenderTask("task2",
                  []( vka::RenderTaskVar & c)
                  {
                      (void)c;
                  })
                .addHostResourceInput("v1")
                .addHostResourceOutput("v2")
            ;


    G.setFinalOutput("v2");

    G.compile();

    auto task_order = G.calculateTaskOrder();

    REQUIRE( task_order.size() == 2);
    REQUIRE( task_order[0] == "task1");
    REQUIRE( task_order[1] == "task2");

    THEN("Executing the graph will throw an error because v1 is not produced by task1")
    {
        REQUIRE_THROWS( G.execute( vk::CommandBuffer() ) );
    }

    G.getTask("task1").setCommandBufferWrite([]( vka::RenderTaskVar & c)
    {
        c.emplaceResource<int>("v1");
        c.getHostResource<int>("v1") = 3;
    });
    G.getTask("task1").setCommandBufferWrite([]( vka::RenderTaskVar & c)
    {
        REQUIRE( c.getHostResource<int>("v1") == 3);
    });


    WHEN("When we add a new task to the graph which only produces an output")
    {
        G.addRenderTask("task3",
                      []( vka::RenderTaskVar & c)
                          {
                            (void)c;
                          })
        .addHostResourceOutput("v3");

        THEN("THe graph does not need to be recompiled because the ouput is not being used by any other nodes")
        {
            REQUIRE( G.needRecompile() == false);
        }

        WHEN("We add v3 as a resource input for task2")
        {
            G.getTask("task2").addHostResourceInput("v3");

            THEN("THe graph needs to be recompiled")
            {
                REQUIRE( G.needRecompile() == true);

                WHEN("We recompile")
                {
                    G.compile();

                    auto task_order_new = G.calculateTaskOrder();

                    REQUIRE( task_order_new.size() == 3);
                    //REQUIRE( task_order_new[0] == "task1");
                    //REQUIRE( task_order_new[1] == "task2"
                }
            }
        }
    }
    (void)destroy;
    (void)init;
}

