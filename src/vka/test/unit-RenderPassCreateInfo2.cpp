#include "catch.hpp"

#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/TextureMemoryPool.h>
#include <vka/core/Primatives.h>
#include <vka/utils/HostImage.h>
#include <vka/core/RenderPassCreateInfo.h>

#include "test_init_functions.hpp"


SCENARIO( "Creating two render passes with the same input will result in the same renderpass being returned" )
{
    init();
    auto & System = vka::System::get();

    {

        vka::RenderPassCreateInfo2 rp;

        rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::ePresentSrcKHR);

        auto rpci = rp.generateRenderPassCreateInfo();

        auto renderPass = System.device().createRenderPass(rpci);

        REQUIRE( renderPass);

        System.device().destroyRenderPass(renderPass);
    }

    {
        vka::RenderPassCreateInfo2 rp;

        rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::ePresentSrcKHR);

        auto rpci = rp.generateRenderPassCreateInfo();

        auto renderPass = System.device().createRenderPass(rpci);

        REQUIRE( renderPass);

        System.device().destroyRenderPass(renderPass);
    }

    {
        vka::RenderPassCreateInfo2 rp;

        rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm,         vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addColorAttachment( vk::Format::eR32G32B32A32Sfloat,    vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addColorAttachment( vk::Format::eR32G32B32A32Sfloat,    vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addColorAttachment( vk::Format::eR32G32B32A32Sfloat,    vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addColorAttachment( vk::Format::eR32G32B32A32Sfloat,    vk::ImageLayout::eShaderReadOnlyOptimal);
        rp.addDepthStenchilAttachment(vk::Format::eD24UnormS8Uint, vk::ImageLayout::eShaderReadOnlyOptimal);

        auto rpci = rp.generateRenderPassCreateInfo();

        auto renderPass = System.device().createRenderPass(rpci);

        REQUIRE( renderPass);

        System.device().destroyRenderPass(renderPass);
    }


    {
        vk::RenderPass renderPass;
        {

            vka::RenderPassCreateInfo2 rp;

            rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm , vk::ImageLayout::ePresentSrcKHR );

            renderPass = System.createRenderPass(rp);

            REQUIRE( renderPass);
        }
        {

            vka::RenderPassCreateInfo2 rp;

            rp.addColorAttachment( vk::Format::eR8G8B8A8Unorm , vk::ImageLayout::ePresentSrcKHR );

            auto renderPass2 = System.createRenderPass(rp);

            REQUIRE( renderPass2);
            REQUIRE( renderPass2 == renderPass);
        }
    }

    destroy();
}
