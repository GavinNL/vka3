#include <vka/core/TextureMemoryPool.h>
#include <vka/core/System.h>
#include <vka/core/Global.h>

#include <vka/logging.h>

namespace vka
{

vk::Image   TexturePool::_allocateImage( vk::Format format,
                                vk::ImageType type,
                                vk::Extent3D extent,
                                uint32_t arrayLayers,
                                uint32_t mipLevels)
{
    auto device = vka::System::device();

    vk::ImageCreateInfo info = GLOBAL.info(m_image).createInfo;
    auto & TPInfo =  m_info;

    info.imageType     = type;// VK_IMAGE_TYPE_2D;

    //create_info.format        = vk::Format::eR8G8B8A8Unorm;
    //create_info.tiling        = vk::ImageTiling::eOptimal;
    info.initialLayout = vk::ImageLayout::eUndefined;
    //create_info.usage         = vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled;// VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    info.samples       = vk::SampleCountFlagBits::e1; // VK_SAMPLE_COUNT_1_BIT;
//    create_info.sharingMode   = vk::SharingMode::eExclusive; // VK_SHARING_MODE_EXCLUSIVE;

    info.extent              = extent;
    info.arrayLayers         = arrayLayers;
    info.format              = format;
    info.mipLevels           = mipLevels;

    if( info.arrayLayers >=6 && type == vk::ImageType::e2D)
    {
        info.flags |= vk::ImageCreateFlagBits::eCubeCompatible;
    }

    auto img = GLOBAL.system->createImage(info);

    if( img )
    {
        vk::MemoryRequirements req = device.getImageMemoryRequirements(img);
        auto             alignment = req.alignment;

        vk::DeviceSize size = req.size;
        size = size%alignment==0 ? size : ((size/alignment + 1)*alignment);



        // find the offset into the memory where we can store
        // data for this image.
        auto offset = TPInfo.allocator.allocate(size);


        if(offset == std::numeric_limits<size_t>::max() )
        {
            GLOBAL.system->destroyImage(img);
            throw std::runtime_error("Failed allocate enough memory");
        }

        VKA_DEBUG("Image Allocated, {} bytes, at Memory Offset {}", size, offset);

        GLOBAL.system->bindImageMemory(img, TPInfo.memory, offset);

        return img;
    }
    throw std::runtime_error("Failed to create vk::Image");
}

vk::ImageView   TexturePool::createNewImage( vk::Format format,
                                             vk::Extent3D extent,
                                             uint32_t arrayLayers,
                                             uint32_t mipLevels)
{
    auto img = _allocateImage(format, vk::ImageType::e2D, extent, arrayLayers, mipLevels);

    auto v = _createDefaultView( img, vk::ImageViewType::e2D );

    if( v )
    {
        return v;
    }

    throw std::runtime_error("Failed to create vk::Image");
}

vk::ImageView   TexturePool::createNewImage2D( vk::Format format,
                                vk::Extent2D extent,
                                uint32_t arrayLayers,
                                uint32_t mipLevels)
{
    auto img = _allocateImage(format, vk::ImageType::e2D, vk::Extent3D(extent.width,extent.height,1), arrayLayers, mipLevels);

    auto v = _createDefaultView( img, vk::ImageViewType::e2D);

    if( v )
    {
        return v;
    }

    throw std::runtime_error("Failed to create vk::Image");
}

vk::ImageView   TexturePool::createNewImageCube( vk::Format format,
                                vk::Extent2D extent,
                                uint32_t mipLevels)
{
    auto img = _allocateImage(format,vk::ImageType::e2D,  vk::Extent3D(extent.width,extent.height,1), 6, mipLevels);

    auto v = _createDefaultView( img, vk::ImageViewType::eCube);

    if( v )
    {
        return v;
    }

    throw std::runtime_error("Failed to create vk::Image");
}

void TexturePool::destroyAll()
{
    auto & TPInfo = m_info;

    auto views = TPInfo.imageViews;

    VKA_DEBUG("TexturePool::destroyAll() - Destroying all ImageViews. ImageViews currently allocated {} ", TPInfo.imageViews.size() );

    for(auto v: views)
    {
        destroyImage(v);
    }

    auto memorySize = allocationSize();

    TPInfo.allocator.setSize( memorySize  );
    assert( TPInfo.imageViews.size() == 0);
}

vk::DeviceSize TexturePool::allocationSize() const
{
    auto & TPInfo = m_info;
    return TPInfo.memoryRequirements.size;
}


void TexturePool::_destroyImage(vk::Image i)
{
    auto & TPInfo = m_info;


    {
        // get the image from the view
        auto img = i;


        // Info for the vk::Images
        auto & Iinfo = GLOBAL.imagesContainer.info(img);


        {
            // unallocate the memory from our allocator
            auto offset = Iinfo.offset;

            TPInfo.allocator.free( offset );

            // erase the image and the view from the TextureMemoryPool's
            // data
            TPInfo.images.erase(img);
        }


        GLOBAL.system->destroyImage(img);

    }
}

void TexturePool::_destroyImageView(vk::ImageView v)
{

    auto & TPInfo = m_info;

    TPInfo.imageViews.erase(v);

    // Destroy the ImageView, since that is what we requested
    GLOBAL.system->destroyImageView(v); //IVinfo is invalidated here

}

void TexturePool::destroyImage(vk::ImageView v)
{




    {
        // info for the vk::ImageView (The main ID)
        auto & IVinfo = GLOBAL.imageViewsContainer.info(v);

        // get the image from the view
        auto img = IVinfo.createInfo.image;

        _destroyImageView(v);
        _destroyImage(img);
    }
}

vk::ImageView TexturePool::_createDefaultView(vk::Image img, vk::ImageViewType type)
{
    vk::ImageViewCreateInfo C;
    C.image                           = img;

    auto & imgInfo = GLOBAL.imagesContainer.info(img);

    auto imgType    = imgInfo.createInfo.imageType;
    auto layerCount = imgInfo.createInfo.arrayLayers;
    auto format     = imgInfo.createInfo.format;
    auto mipLevels  = imgInfo.createInfo.mipLevels;

    switch( imgType )
    {
        case vk::ImageType::e1D:
            C.viewType = vk::ImageViewType::e1D;
            break;
        case vk::ImageType::e2D:
            C.viewType = vk::ImageViewType::e2D;
            if( layerCount > 1)
                C.viewType = vk::ImageViewType::e2DArray;
            break;
        case vk::ImageType::e3D:
            C.viewType = vk::ImageViewType::e3D;
            break;

    }
    C.viewType = type;

    C.format                          = format;

    C.subresourceRange.aspectMask     = vk::ImageAspectFlagBits::eColor;
    switch(C.format)
    {
        case vk::Format::eD16Unorm:
        case vk::Format::eD32Sfloat:
        case vk::Format::eD16UnormS8Uint:
        case vk::Format::eD24UnormS8Uint:
        case vk::Format::eD32SfloatS8Uint:
            C.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
            break;
        default:
            C.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
        break;
    }

    C.subresourceRange.baseMipLevel   = 0;
    C.subresourceRange.levelCount     = mipLevels;
    C.subresourceRange.baseArrayLayer = 0;
    C.subresourceRange.layerCount     = layerCount;

    auto v = GLOBAL.system->createImageView(C);
    if( v )
    {
        auto & TPInfo = m_info;
        TPInfo.imageViews.insert(v);
    }
    return v;


}


}
