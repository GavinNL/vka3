#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/Global.h>
#include <vka/core/System.h>

#include <vka/logging.h>


namespace vka
{

bool DeviceMeshPrimitive::copyFrom(HostTriPrimitive const &P, bool copyToMapped)
{
    uint32_t alignment=16;
    auto requiredBytes = P.requiredByteSize(alignment);

    // maximum size of the subbuffer
    auto sizeOfSubBuffer = m_subBuffer.getSize();

    if(requiredBytes > sizeOfSubBuffer)
    {
        return false;
    }

    auto drawCall = P.getDrawCall();

    m_drawInfo.indexCount   = drawCall.indexCount  ;
    m_drawInfo.firstIndex   = drawCall.firstIndex  ;
    m_drawInfo.vertexOffset = drawCall.vertexOffset;
    m_drawInfo.vertexCount  = drawCall.vertexCount ;

    auto offsets = P.getOffets(alignment);

    if(copyToMapped)
    {
        auto * data = static_cast<unsigned char*>( m_subBuffer.getMappedMemory() ) ;

        // VKA_TRACE("Copying HostTriPrimitive to MAPPED DeviceMeshPrimitive: Bytes Available: {} ", sizeOfSubBuffer);
        P.copyData(data, offsets);
    }

    // clear all the offsets
    setOffset(PrimitiveAttribute::POSITION   , {});
    setOffset(PrimitiveAttribute::NORMAL	 , {});
    setOffset(PrimitiveAttribute::TANGENT	 , {});
    setOffset(PrimitiveAttribute::TEXCOORD_0 , {});
    setOffset(PrimitiveAttribute::TEXCOORD_1 , {});
    setOffset(PrimitiveAttribute::COLOR_0	 , {});
    setOffset(PrimitiveAttribute::JOINTS_0   , {});
    setOffset(PrimitiveAttribute::WEIGHTS_0  , {});
    setOffset(PrimitiveAttribute::__LAST     , {});// index

    for(auto a : {PrimitiveAttribute::POSITION
                  , PrimitiveAttribute::NORMAL
                  , PrimitiveAttribute::TANGENT
                  , PrimitiveAttribute::TEXCOORD_0
                  , PrimitiveAttribute::TEXCOORD_1
                  , PrimitiveAttribute::COLOR_0
                  , PrimitiveAttribute::JOINTS_0
                  , PrimitiveAttribute::WEIGHTS_0
                  , PrimitiveAttribute::__LAST} /*__LAST == index*/ )
    {
        auto i = static_cast<size_t>(a);
        if( offsets[i] != std::numeric_limits<vk::DeviceSize>::max())
        {
            setOffset( a, offsets[i]);
        }
        else
        {
            setOffset( a, {});
        }
    }

    if( P.INDEX.attributeSize() == 1)
    {
        throw std::runtime_error("INDICES are using [UNSIGNED] BYTE, which is not allowed right now.");
    }
    m_indexType   = P.INDEX.attributeSize() == 2 ? vk::IndexType::eUint16 : vk::IndexType::eUint32;

    //===============================================================
    // Check for overlap
    //===============================================================
    auto checkOverlap = [&](uint32_t i, uint32_t size)
    {
        uint32_t j = i+1;
        while(m_attributeOffsets[j]!=std::numeric_limits<size_t>::max() )
        {
            j++;
        }

        if( m_attributeOffsets[i]+size > m_attributeOffsets[j] )
        {
            throw std::runtime_error("Attributes overlap");
        }
    };

    if( m_attributeOffsets[0] != std::numeric_limits<size_t>::max() ) checkOverlap(0, static_cast<uint32_t>(P.POSITION  .byteLength()   ) );
    if( m_attributeOffsets[1] != std::numeric_limits<size_t>::max() ) checkOverlap(0, static_cast<uint32_t>(P.NORMAL	.byteLength()   ) );
    if( m_attributeOffsets[2] != std::numeric_limits<size_t>::max() ) checkOverlap(0, static_cast<uint32_t>(P.TANGENT	.byteLength()   ) );
    if( m_attributeOffsets[3] != std::numeric_limits<size_t>::max() ) checkOverlap(0, static_cast<uint32_t>(P.TEXCOORD_0.byteLength()   ) );
    if( m_attributeOffsets[4] != std::numeric_limits<size_t>::max() ) checkOverlap(0, static_cast<uint32_t>(P.TEXCOORD_1.byteLength()   ) );
    if( m_attributeOffsets[5] != std::numeric_limits<size_t>::max() ) checkOverlap(0, static_cast<uint32_t>(P.COLOR_0	.byteLength()   ) );
    if( m_attributeOffsets[6] != std::numeric_limits<size_t>::max() ) checkOverlap(0, static_cast<uint32_t>(P.JOINTS_0  .byteLength()   ) );
    //if( m_attributeOffsets[7] != std::numeric_limits<size_t>::max() ) checkOverlap(0, P.WEIGHTS_0 .byteLength() );


    return true;
}

}
