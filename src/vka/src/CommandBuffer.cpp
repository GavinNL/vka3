#include <vka/core/CommandBuffer.h>
#include <vka/core/Global.h>
#include <vka/core/DeviceMeshPrimitive.h>

#include <vka/logging.h>

namespace vka
{

vk::BufferCopy CommandBuffer::get_BufferCopy( vka::SubBuffer const & src, vka::SubBuffer const & dst) const
{
    vk::BufferCopy bc;

    bc.srcOffset = src.getOffset();
    bc.dstOffset = dst.getOffset();
    bc.size      = std::min( dst.getSize(), src.getSize() );

    return bc;
}

void CommandBuffer::copySubBuffer( vka::SubBuffer const & src, vka::SubBuffer const & dst)
{
    vk::BufferCopy bc = get_BufferCopy(src, dst);

    copyBuffer(src.getParentBuffer(), dst.getParentBuffer(), bc);
}

void CommandBuffer::copySubBufferToImage(vka::SubBuffer const &src,
                                         vk::Image img,
                                         vk::ImageLayout imageLayout,
                                         const vk::BufferImageCopy &C) const
{
    vk::BufferImageCopy lC = C;

    lC.setBufferOffset( src.getOffset() + C.bufferOffset );

    vk::CommandBuffer::copyBufferToImage(
                src.getParentBuffer(),
                img,
                imageLayout,
                lC
                );
}

void CommandBuffer::copyDeviceMeshPrimitive(DeviceMeshPrimitive &src, DeviceMeshPrimitive &dst)
{
    copySubBuffer( src.m_subBuffer, dst.m_subBuffer);

    dst.m_attributeOffsets = src.m_attributeOffsets;
    dst.m_indexType        = src.m_indexType;
    dst.m_drawInfo         = src.m_drawInfo;
}

void CommandBuffer::bindIndexSubBuffer(vka::SubBuffer const &src,  vk::DeviceSize offset, vk::IndexType indexType)
{
    bindIndexBuffer( src.getParentBuffer(),
                     src.getOffset() + offset, indexType);
}

void CommandBuffer::bindVertexSubBuffer(uint32_t firstBinding,
                                      vka::SubBuffer const & src,
                                      vk::DeviceSize subBufferOffset)
{
    vk::Buffer b = src.getParentBuffer();
    subBufferOffset       = src.getOffset()+subBufferOffset;
    bindVertexBuffers(firstBinding, b, subBufferOffset);
}

void CommandBuffer::bindDeviceMeshPrimitive(DeviceMeshPrimitive const & src, bool bindAvailable)
{
    std::array<vk::Buffer    , static_cast<size_t>(PrimitiveAttribute::__LAST)> buffers;
    std::array<vk::DeviceSize, static_cast<size_t>(PrimitiveAttribute::__LAST)> offsets = {0};

    static_assert( sizeof(offsets)+1*sizeof(vk::DeviceSize) == sizeof(src.m_attributeOffsets) , "");

    uint32_t totalAttributes=0;

    if( bindAvailable )
    {
        for(uint32_t i=0; i < buffers.size() ; i++)
        {
            if( src.m_attributeOffsets[i] != std::numeric_limits<vk::DeviceSize>::max() )
            {
                buffers[totalAttributes] = src.m_subBuffer.getParentBuffer();
                offsets[totalAttributes] = src.m_subBuffer.getOffset();
                offsets[totalAttributes] = src.m_subBuffer.getOffset() + src.m_attributeOffsets[i];
                totalAttributes++;
            }
        }
    }
    else
    {
        for(uint32_t i=0; i < buffers.size() ; i++)
        {
            buffers[i] = src.m_subBuffer.getParentBuffer();
            offsets[i] = src.m_subBuffer.getOffset();
            if( src.m_attributeOffsets[i] != std::numeric_limits<vk::DeviceSize>::max() )
            {
                offsets[i] += src.m_attributeOffsets[i];
            }
            totalAttributes++;
        }
    }

    bindVertexBuffers(0, totalAttributes, buffers.data(), offsets.data());

    if( src.indexCount()!=0 )
    {
        bindIndexSubBuffer( src.m_subBuffer, src.getIndexByteOffset(), src.m_indexType);
    }
}


void CommandBuffer::setImageLayout(vk::Image img,
                                   vk::ImageAspectFlags aspects,
                                   vk::ImageLayout oldLayout,
                                   vk::ImageLayout newLayout,
                                   vk::AccessFlags srcAccess,
                                   vk::AccessFlags dstAccess,
                                   vk::PipelineStageFlags srcStageFlags,
                                   vk::PipelineStageFlags dstStageFlags)

{
    auto & info = GLOBAL.imagesContainer.info( img );

    vk::ImageMemoryBarrier imageBarrier;

    //imageBarrier.pNext = NULL;
    imageBarrier.oldLayout = oldLayout;
    imageBarrier.newLayout = newLayout;
    imageBarrier.image     = img;

    vk::ImageSubresourceRange range;
    range.aspectMask = aspects;
    range.baseArrayLayer = 0;
    range.baseMipLevel   = 0;
    range.levelCount     = info.createInfo.mipLevels;
    range.layerCount     = info.createInfo.arrayLayers;

    imageBarrier.srcAccessMask = srcAccess;
    imageBarrier.dstAccessMask = dstAccess;

    setImageLayout( img, range, oldLayout, newLayout, srcAccess, dstAccess, srcStageFlags, dstStageFlags);

}


void CommandBuffer::setImageLayout(vk::Image img,
                                   vk::ImageSubresourceRange const & range,
                                   vk::ImageLayout _oldLayout,
                                   vk::ImageLayout _newLayout,
                                   vk::AccessFlags srcAccess,
                                   vk::AccessFlags dstAccess,
                                   vk::PipelineStageFlags srcStageFlags,
                                   vk::PipelineStageFlags dstStageFlags)
{
    auto & info = GLOBAL.imagesContainer.info( img );

    // check that all layers in the range are the same layout
    {
        for(uint32_t l = range.baseArrayLayer; l < range.layerCount; l++)
        {
            for(uint32_t m=range.baseMipLevel; m<range.levelCount; m++)
            {
                if( info.m_layouts[l][m] != _oldLayout )
                {
                    VKA_TRACE("CommandBuffer::setImageLayout.. Layer {} Mip {} is not the correct layout {}, (required {}) ", l,m,to_string(info.m_layouts[l][m]), to_string(_oldLayout) );
                }
            }
        }
    }

    vk::ImageMemoryBarrier imageBarrier;

    imageBarrier.oldLayout = _oldLayout;
    imageBarrier.newLayout = _newLayout;
    imageBarrier.image     = img;
    imageBarrier.subresourceRange = range;

    imageBarrier.srcAccessMask = srcAccess;
    imageBarrier.dstAccessMask = dstAccess;

    pipelineBarrier( srcStageFlags, dstStageFlags, vk::DependencyFlags(), nullptr,nullptr,imageBarrier);



    for(uint32_t l = range.baseArrayLayer; l < (range.baseArrayLayer+range.layerCount); l++)
    {
        for(uint32_t m=range.baseMipLevel; m<(range.baseMipLevel+range.levelCount); m++)
        {
            VKA_TRACE("CommandBuffer::setImageLayout.. Layer {} Mip {}    {} ---> {} ", l,m,to_string(info.m_layouts[l][m]), to_string(_newLayout) );
            info.m_layouts[l][m] = _newLayout;
        }
    }
}


#if 1
void vka::CommandBuffer::copyBufferToImageWithBorderAndConvert(
                                            vka::SubBuffer srcBuffer,
                                            vk::ImageView dstImageView,
                                            uint32_t borderSize,
                                            vka::BufferSubImageCopyToImage subImageCopies)
{
    auto w = subImageCopies.srcBufferImageExtent.width;
    auto h = subImageCopies.srcBufferImageExtent.height;

    auto wi = static_cast<int32_t>(subImageCopies.srcBufferImageExtent.width);
    auto hi = static_cast<int32_t>(subImageCopies.srcBufferImageExtent.height);

    auto x = subImageCopies.srcBufferRegion.offset.x;
    auto y = subImageCopies.srcBufferRegion.offset.y;

    int32_t bs  = static_cast<int32_t>(borderSize);
    uint32_t bsu = borderSize;

    std::vector<vka::BufferSubImageCopyToImage> bc;

    // middle main block
    if(1){
        vka::BufferSubImageCopyToImage tl;
        tl.srcBufferImageExtent = subImageCopies.srcBufferImageExtent;
        tl.srcBufferRegion.offset = vk::Offset2D{x ,y };
        tl.srcBufferRegion.extent = vk::Extent2D{w, h};
        tl.dstImageOffset       = vk::Offset2D{ bs,bs};
        tl.mip = 0;
        tl.layer = 0;
        bc.push_back(tl);
    }
    if(1){ // bottom left block
        vka::BufferSubImageCopyToImage tl;
        tl.srcBufferImageExtent = subImageCopies.srcBufferImageExtent;
        tl.srcBufferRegion.offset = vk::Offset2D{ x+wi-bs , y+hi-bs };
        tl.srcBufferRegion.extent = vk::Extent2D{bsu, bsu};
        tl.dstImageOffset       = vk::Offset2D{ 0, 0};
        tl.mip = 0;
        tl.layer = 0;
        bc.push_back(tl);
        //tl.dstImageOffset       = vk::Offset2D{ static_cast<int32_t>(w), static_cast<int32_t>(h) };

    }
    if(1){ // top right block
        vka::BufferSubImageCopyToImage tl;
        tl.srcBufferImageExtent = subImageCopies.srcBufferImageExtent;
        tl.srcBufferRegion.offset = vk::Offset2D{ x , y };
        tl.srcBufferRegion.extent = vk::Extent2D{bsu, bsu};
        tl.dstImageOffset       = vk::Offset2D{bs+wi, bs+hi};
        tl.mip = 0;
        tl.layer = 0;
        bc.push_back(tl);
        //tl.dstImageOffset       = vk::Offset2D{ static_cast<int32_t>(w), static_cast<int32_t>(h) };

    }
    if(1){ // bottom right block
        vka::BufferSubImageCopyToImage tl;
        tl.srcBufferImageExtent = subImageCopies.srcBufferImageExtent;
        tl.srcBufferRegion.offset = vk::Offset2D{x , y+hi-bs };
        tl.srcBufferRegion.extent = vk::Extent2D{bsu, bsu};
        tl.dstImageOffset       = vk::Offset2D{bs+wi, 0};
        tl.mip = 0;
        tl.layer = 0;
        bc.push_back(tl);
        //tl.dstImageOffset       = vk::Offset2D{ static_cast<int32_t>(w), static_cast<int32_t>(h) };

    }
    if(1){ // top leftblock
        vka::BufferSubImageCopyToImage tl;
        tl.srcBufferImageExtent = subImageCopies.srcBufferImageExtent;
        tl.srcBufferRegion.offset = vk::Offset2D{x-wi-bs , y };
        tl.srcBufferRegion.extent = vk::Extent2D{bsu, bsu};
        tl.dstImageOffset       = vk::Offset2D{0, bs+hi};
        tl.mip = 0;
        tl.layer = 0;
        bc.push_back(tl);
        //tl.dstImageOffset       = vk::Offset2D{ static_cast<int32_t>(w), static_cast<int32_t>(h) };

    }
    if(1){ // top
        vka::BufferSubImageCopyToImage tl;
        tl.srcBufferImageExtent = subImageCopies.srcBufferImageExtent;
        tl.srcBufferRegion.offset = vk::Offset2D{x , y };
        tl.srcBufferRegion.extent = vk::Extent2D{w, bsu};
        tl.dstImageOffset       = vk::Offset2D{bs, bs+hi};
        tl.mip = 0;
        tl.layer = 0;
        bc.push_back(tl);
        //tl.dstImageOffset       = vk::Offset2D{ static_cast<int32_t>(w), static_cast<int32_t>(h) };

    }
    if(1){ // bottom
        vka::BufferSubImageCopyToImage tl;
        tl.srcBufferImageExtent = subImageCopies.srcBufferImageExtent;
        tl.srcBufferRegion.offset = vk::Offset2D{x , y+hi-bs };
        tl.srcBufferRegion.extent = vk::Extent2D{w, bsu};
        tl.dstImageOffset       = vk::Offset2D{bs, 0};
        tl.mip = 0;
        tl.layer = 0;
        bc.push_back(tl);
        //tl.dstImageOffset       = vk::Offset2D{ static_cast<int32_t>(w), static_cast<int32_t>(h) };

    }
    if(1){ // left
        vka::BufferSubImageCopyToImage tl;
        tl.srcBufferImageExtent = subImageCopies.srcBufferImageExtent;
        tl.srcBufferRegion.offset = vk::Offset2D{x+wi-bs , y };
        tl.srcBufferRegion.extent = vk::Extent2D{bsu, h};
        tl.dstImageOffset       = vk::Offset2D{0, bs};
        tl.mip = 0;
        tl.layer = 0;
        bc.push_back(tl);
        //tl.dstImageOffset       = vk::Offset2D{ static_cast<int32_t>(w), static_cast<int32_t>(h) };
    }
    if(1){ // right
        vka::BufferSubImageCopyToImage tl;
        tl.srcBufferImageExtent = subImageCopies.srcBufferImageExtent;
        tl.srcBufferRegion.offset = vk::Offset2D{x , y };
        tl.srcBufferRegion.extent = vk::Extent2D{bsu, h};
        tl.dstImageOffset       = vk::Offset2D{wi+bs, bs};
        tl.mip = 0;
        tl.layer = 0;
        bc.push_back(tl);
        //tl.dstImageOffset       = vk::Offset2D{ static_cast<int32_t>(w), static_cast<int32_t>(h) };

    }
    copyBufferToImageAndConvert(srcBuffer ,dstImageView, bc);
};
#endif

void vka::CommandBuffer::copyBufferToImageAndConvert (vka::SubBuffer srcBuffer,
                                                      vk::ImageView dstImageView,
                                                      vk::ArrayProxy<BufferSubImageCopyToImage const > subImageCopies)
{
    auto & info  = GLOBAL.imageViewsContainer.info(dstImageView);
    auto img     = info.createInfo.image;
    auto & infoI = GLOBAL.imagesContainer.info(img);
    auto format  = infoI.createInfo.format;
    auto formatByteSize = vka::formatSize(format);
    // Size of the image
    uint32_t imgWidth  = infoI.createInfo.extent.width;
    uint32_t imgHeight = infoI.createInfo.extent.height;


    // figure out which layers/mips need to be converted into
    // TransferDstOptimal
    std::set< std::pair<uint32_t, uint32_t> > layerAndMip;
    for(auto & s : subImageCopies)
    {
        layerAndMip.insert( std::make_pair(s.layer, s.mip) );
    }

    //===============================================================================
    // Convert all the layers/mip levels into a layout that allows transfering to
    //===============================================================================
    for(auto & s : layerAndMip)
    {
        vk::ImageSubresourceRange mipSubRange;// = {};
        mipSubRange.aspectMask   = vk::ImageAspectFlagBits::eColor;//
        mipSubRange.baseMipLevel = s.second;
        mipSubRange.levelCount   = 1;

        mipSubRange.baseArrayLayer = s.first;
        mipSubRange.layerCount     = 1;

        // convert the image layer into TransferDstOptimal
        setImageLayout(
                    img, mipSubRange,
                    vk::ImageLayout::eUndefined,
                    vk::ImageLayout::eTransferDstOptimal,
                    vk::AccessFlags(),
                    vk::AccessFlagBits::eTransferWrite,
                    vk::PipelineStageFlagBits::eHost,
                    vk::PipelineStageFlagBits::eTransfer);
    }
    //===============================================================================



    std::vector<vk::BufferImageCopy> BICv;
    BICv.reserve(subImageCopies.size());
    for(auto & s : subImageCopies)
    {
        // Want to Copy this sub image which is contained in the buffer
        vk::Offset3D srcBufferSubImageOffset(  s.srcBufferRegion.offset.x,      s.srcBufferRegion.offset.y,       0);
       // vk::Extent3D srcBufferSubImageExtent(  s.srcBufferImageExtent.width,  s.srcBufferImageExtent.height,  1);

        auto & BIC = BICv.emplace_back();

        uint32_t byteOffsetOfFirstPixelInSubBufferImage = ( static_cast<uint32_t>( srcBufferSubImageOffset.x) + static_cast<uint32_t>(srcBufferSubImageOffset.y) * s.srcBufferImageExtent.width ) * formatByteSize;
        (void)byteOffsetOfFirstPixelInSubBufferImage;

        if( vka::formatIsCompressedBC(format) )
        {
            imgWidth=0;
            imgHeight=0;
        }

        //  https://arm-software.github.io/vulkan-sdk/mipmapping.html

        BIC.bufferOffset      = byteOffsetOfFirstPixelInSubBufferImage;
        BIC.bufferRowLength   = s.srcBufferImageExtent.width;
        BIC.bufferImageHeight = s.srcBufferImageExtent.height;
        BIC.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
        BIC.imageSubresource.mipLevel = s.mip;
        BIC.imageSubresource.baseArrayLayer = s.layer;
        BIC.imageSubresource.layerCount = 1;
        BIC.imageOffset        = vk::Offset3D(s.dstImageOffset.x, s.dstImageOffset.y, 0);
        BIC.imageExtent.width  = s.srcBufferRegion.extent.width;
        BIC.imageExtent.height = s.srcBufferRegion.extent.height;
        BIC.imageExtent.depth = 1;

        BIC.setBufferOffset( srcBuffer.getOffset() + BIC.bufferOffset );
    }

    copyBufferToImage(
                srcBuffer.getParentBuffer(),
                img,
                vk::ImageLayout::eTransferDstOptimal,
                BICv
                );


    //==============================================================
    // Convert back into shaderReadOnlyOptimal
    //==============================================================
    for(auto & s : layerAndMip)
    {
        vk::ImageSubresourceRange mipSubRange;// = {};
        mipSubRange.aspectMask   = vk::ImageAspectFlagBits::eColor;//
        mipSubRange.baseMipLevel = s.second;
        mipSubRange.levelCount   = 1;

        mipSubRange.baseArrayLayer = s.first;
        mipSubRange.layerCount     = 1;

        // convert the image layer into TransferDstOptimal
        setImageLayout(
                    img, mipSubRange,
                    vk::ImageLayout::eTransferDstOptimal,
                    vk::ImageLayout::eShaderReadOnlyOptimal,
                    vk::AccessFlagBits::eTransferWrite,
                    vk::AccessFlagBits::eShaderRead,
                    vk::PipelineStageFlagBits::eTransfer,
                    vk::PipelineStageFlagBits::eFragmentShader);
    }
    //==============================================================
};


void CommandBuffer::generateMipMaps(vk::ImageView img, uint32_t layer)
{
    auto & info = GLOBAL.imageViewsContainer.info(img);
    generateMipMaps(info.createInfo.image, layer);
}

void CommandBuffer::generateMipMaps(vk::Image img, uint32_t layer)
{
    auto & info = GLOBAL.imagesContainer.info(img);

    auto extent = info.createInfo.extent;

    vk::ImageSubresourceRange range;
    range.setAspectMask( vk::ImageAspectFlagBits::eColor);
    range.layerCount     = 1;
    range.baseArrayLayer = layer;
    range.baseMipLevel   = 0;
    range.levelCount     = 1;

    auto width  = extent.width;
    auto height = extent.height;
    //auto depth  = extent.depth;

    // Convert the first MipMap level to TransferSrc
    // so that it can be copied to the next level.
    setImageLayout(
                img, range,
                vk::ImageLayout::eShaderReadOnlyOptimal,
                vk::ImageLayout::eTransferSrcOptimal,
                vk::AccessFlagBits::eShaderRead,
                vk::AccessFlagBits::eTransferRead,
                vk::PipelineStageFlagBits::eAllCommands,
                vk::PipelineStageFlagBits::eAllCommands);

    auto mipLevels = info.createInfo.mipLevels;

    for (uint32_t i = 1; i < mipLevels; i++)
    {
        vk::ImageBlit imageBlit;

        // Source
        imageBlit.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;//VK_IMAGE_ASPECT_COLOR_BIT;
        imageBlit.srcSubresource.layerCount = 1;
        imageBlit.srcSubresource.baseArrayLayer = layer;
        imageBlit.srcSubresource.mipLevel = i-1;
        imageBlit.srcOffsets[1].x = int32_t( width >> (i - 1));
        imageBlit.srcOffsets[1].y = int32_t( height >> (i - 1));
        imageBlit.srcOffsets[1].z = 1;

        // Destination
        imageBlit.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
        imageBlit.dstSubresource.layerCount = 1;
        imageBlit.dstSubresource.mipLevel = i;
        imageBlit.dstSubresource.baseArrayLayer = layer;
        imageBlit.dstOffsets[1].x = int32_t(width >> i);
        imageBlit.dstOffsets[1].y = int32_t(height >> i);
        imageBlit.dstOffsets[1].z = 1;

        vk::ImageSubresourceRange mipSubRange;// = {};
        mipSubRange.aspectMask   = vk::ImageAspectFlagBits::eColor;//
        mipSubRange.baseMipLevel = i;
        mipSubRange.levelCount   = 1;
        mipSubRange.layerCount   = 1;
        mipSubRange.baseArrayLayer = layer;

        // Transiton current mip level to transfer dest
        setImageLayout(
                    img, mipSubRange,
                    info.m_layouts[layer][i],  //vk::ImageLayout::eUndefined,
                    vk::ImageLayout::eTransferDstOptimal,
                    vk::AccessFlags(),
                    vk::AccessFlagBits::eTransferWrite,
                    vk::PipelineStageFlagBits::eAllCommands,
                    vk::PipelineStageFlagBits::eAllCommands);


        // Blit from previous level
        blitImage( img, vk::ImageLayout::eTransferSrcOptimal,
                                     img, vk::ImageLayout::eTransferDstOptimal,
                                     imageBlit, vk::Filter::eLinear);

        setImageLayout(
                    img, mipSubRange,
                    vk::ImageLayout::eTransferDstOptimal,
                    vk::ImageLayout::eTransferSrcOptimal,
                    vk::AccessFlags(),
                    vk::AccessFlagBits::eTransferWrite,
                    vk::PipelineStageFlagBits::eHost,
                    vk::PipelineStageFlagBits::eTransfer);

    }
    range.levelCount =  mipLevels;
    setImageLayout(
                img, range,
                vk::ImageLayout::eTransferSrcOptimal,
                vk::ImageLayout::eShaderReadOnlyOptimal,
                vk::AccessFlagBits::eHostWrite | vk::AccessFlagBits::eTransferWrite,
                vk::AccessFlagBits::eShaderRead,
                vk::PipelineStageFlagBits::eAllCommands,
                vk::PipelineStageFlagBits::eAllCommands);

}


void CommandBuffer::bindDescriptorSets( vk::Pipeline pipeline, uint32_t firstSet, vk::ArrayProxy<const vk::DescriptorSet> descriptorSets, vk::ArrayProxy<const uint32_t> dynamicOffsets) const
{
    auto & p = GLOBAL.info(pipeline);

    vk::CommandBuffer::bindDescriptorSets(vk::PipelineBindPoint::eGraphics, p.createInfo.layout, firstSet, descriptorSets, dynamicOffsets
                                          );
}


}
