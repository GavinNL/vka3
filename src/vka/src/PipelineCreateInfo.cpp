#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/Global.h>
#include <vka/core/System.h>
#include <vka/utils/hash.h>

namespace vka
{

size_t PipelineLayoutCreateInfo2::getHash() const
{
    std::hash<uint32_t> H;

    size_t hash=0;
    for(auto s : PushConstantRanges)
    {
        hash = hashCombine(hash, H( static_cast<uint32_t>(s.stageFlags) ) );
        hash = hashCombine(hash, H( static_cast<uint32_t>(s.size) ) );
        hash = hashCombine(hash, H( static_cast<uint32_t>(s.offset) ) );
    }

    for(auto s : SetLayouts)
    {
        hash = hashCombine(hash, GLOBAL.info(s).hash);
    }

    return hash;
}








}
