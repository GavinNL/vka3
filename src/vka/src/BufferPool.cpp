#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/System.h>
#include <vka/core/Global.h>


namespace vka
{

SubBuffer BufferPool::allocate(vk::DeviceSize size)
{
    auto & info = GLOBAL.bufferPools.at(m_buffer);

    auto alignment = info.memoryRequirements.alignment;



    size_t actualSize = size < alignment ? alignment : (( size/alignment + 1 ) * alignment);

    assert( actualSize % alignment == 0);

    auto offset = info.allocator.allocate(actualSize);

    if( std::numeric_limits<AllocatorHelper::offset_type>::max() == offset)
    {
        // not enough space
        throw std::runtime_error("Not enough space in the buffer pool");
    }

    SubBuffer p;
    p.bufferPool = m_buffer;
    p.byteOffset = offset;


    return p;
}

DeviceMeshPrimitive BufferPool::allocateDeviceMeshPrimitive(vk::DeviceSize bytes)
{
    auto sb = allocate(bytes);

    DeviceMeshPrimitive dmp;
    dmp.m_subBuffer = sb;

    return dmp;
}

bool BufferPool::canAllocate(vk::DeviceSize size) const
{
    auto & info = GLOBAL.bufferPools.at(m_buffer);

    return info.allocator.canAllocate(size);
}


void BufferPool::free(DeviceMeshPrimitive & S)
{
    free( S.m_subBuffer );
}

void BufferPool::free(SubBuffer & s)
{
    if( s.bufferPool )
    {

        auto & info = GLOBAL.bufferPools.at(m_buffer);

        info.allocator.free(s.byteOffset);

        s.byteOffset = 0;
        s.bufferPool = vk::Buffer();

    }
}


void SubBuffer::free()
{
    if( bufferPool )
    {
        auto & info = GLOBAL.bufferPools.at(bufferPool);
        info.allocator.free(byteOffset);

        byteOffset = 0;
        bufferPool = vk::Buffer();
    }
}

vk::DeviceSize SubBuffer::getSize() const
{
    auto & info = GLOBAL.bufferPools.at(bufferPool);

    return info.allocator.getAllocationSize( byteOffset);
    //return GLOBAL.memory.at(info.memory).createInfo.allocationSize;
}

void* SubBuffer::getMappedMemory()
{
    auto & info = GLOBAL.bufferPoolInfo(bufferPool);


    void * m = GLOBAL.system->mapMemory( info.memory, byteOffset);

    return m;
}

void SubBuffer::copyData(const void* src, vk::DeviceSize size, vk::DeviceSize offset)
{
    std::memcpy( static_cast<unsigned char*>( getMappedMemory() ) + offset, src, size);
}

}
