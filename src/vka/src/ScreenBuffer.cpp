#include <vka/core/Global.h>
#include <vka/core/ScreenBuffer.h>
#include <vka/core/System.h>

namespace vka
{



uint32_t ScreenBuffer::getNextFrameIndex(vk::Semaphore signalSemaphore)
{
    m_next_frame_index  = vka::System::device().acquireNextImageKHR( m_SwapChain,
                                          std::numeric_limits<uint64_t>::max(),
                                          signalSemaphore,
                                          vk::Fence()).value;
    return m_next_frame_index;
}

void ScreenBuffer::setClearColor( float r, float g, float b, float a )
{
    m_clearColors.at(0) = vk::ClearColorValue( std::array<float,4>{r,g,b,a} );
}


void ScreenBuffer::setDepthClear(float depth, uint32_t stencil )
{
    m_clearColors.at(1) = vk::ClearDepthStencilValue(depth , stencil);
}



vk::RenderPassBeginInfo ScreenBuffer::getRenderPassBeginInfo()
{
    vk::RenderPassBeginInfo RPBI;

    vk::ClearValue cv( vk::ClearColorValue( std::array<float,4>{1.0f,0.0,0.0,1.0} ));
    RPBI.renderPass        = m_renderPass;
    RPBI.framebuffer       = m_frameBuffers[m_next_frame_index];
    RPBI.clearValueCount   = static_cast<uint32_t>(m_clearColors.size());
    RPBI.pClearValues      = m_clearColors.data();
    RPBI.renderArea.extent = m_extent;

    return RPBI;
}

void ScreenBuffer::presentFrameIndex(uint32_t frameIndex, const vk::ArrayProxy<const vk::Semaphore> wait_semaphore)
{
    vk::PresentInfoKHR presentInfo;

    presentInfo.waitSemaphoreCount  = wait_semaphore.size();
    presentInfo.pWaitSemaphores     = wait_semaphore.data();

    vk::SwapchainKHR swapChains[] = { m_SwapChain };
    presentInfo.swapchainCount    = 1;
    presentInfo.pSwapchains       = swapChains;
    presentInfo.pImageIndices     = &frameIndex;
    presentInfo.pResults = nullptr;

    System::presentQueue().presentKHR( presentInfo );
}


}
