#include <vka/core/ShaderReflect.h>
#include <vka/core/Global.h>
#include <vka/logging.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wtype-limits"

#include <spirv_cross/spirv_glsl.hpp>


#pragma GCC diagnostic pop


namespace vka
{

VertexInput_t _getStageInputs(spirv_cross::SPIRType const & T)
{
    VertexInput_t D;

    const vk::Format fmap[][4] =
    {
        { vk::Format::eR32Sfloat, vk::Format::eR32G32Sfloat, vk::Format::eR32G32B32Sfloat, vk::Format::eR32G32B32A32Sfloat},
        { vk::Format::eR32Sint, vk::Format::eR32G32Sint, vk::Format::eR32G32B32Sint, vk::Format::eR32G32B32A32Sint},
        { vk::Format::eR32Uint, vk::Format::eR32G32Uint, vk::Format::eR32G32B32Uint, vk::Format::eR32G32B32A32Uint},
        { vk::Format::eR16Sint, vk::Format::eR16G16Sint, vk::Format::eR16G16B16Sint, vk::Format::eR16G16B16A16Sint},
        { vk::Format::eR16Uint, vk::Format::eR16G16Uint, vk::Format::eR16G16B16Uint, vk::Format::eR16G16B16A16Uint}
    };

    switch(T.basetype)
    {
        case spirv_cross::SPIRType::Float:
            D.format = fmap[0][T.vecsize-1];
            break;
        case spirv_cross::SPIRType::Int:
            D.format = fmap[1][T.vecsize-1];
            break;
        case spirv_cross::SPIRType::UInt:
            D.format = fmap[2][T.vecsize-1];
            break;
        case spirv_cross::SPIRType::Short:
            D.format = fmap[3][T.vecsize-1];
            break;
        case spirv_cross::SPIRType::UShort:
            D.format = fmap[4][T.vecsize-1];
            break;
        default:
            throw std::runtime_error("Input not handled by reflection");
    }

    return D;
}


void _update_reflection(const std::vector<uint32_t> &spirv, ShaderReflect & R)
{

    spirv_cross::CompilerGLSL glsl( spirv.data(), spirv.size() );

    // The SPIR-V is now parsed, and we can perform reflection on it.
    spirv_cross::ShaderResources resources = glsl.get_shader_resources();


    for(auto & push_consts : resources.push_constant_buffers)
    {
        //auto & x = glsl.get_type( glsl.get_type( push_consts.type_id ).self );

        R.pushConstants.name = push_consts.name;

        auto  & t = glsl.get_type(push_consts.base_type_id);
        R.pushConstants.size = glsl.get_declared_struct_size(t);

        VKA_DEBUG("Push Constants Buffer Found, Name: {},    Size: {}", R.pushConstants.name, R.pushConstants.size);
    }


    for(auto & stage_input : resources.stage_inputs)
    {
        //glsl.get_type(get_type(type_id).self)
        auto & x = glsl.get_type( glsl.get_type( stage_input.type_id ).self );

        unsigned location         = glsl.get_decoration(stage_input.id, spv::DecorationLocation);
        unsigned attachment_index = glsl.get_decoration(stage_input.id, spv::DecorationInputAttachmentIndex);
        unsigned index            = glsl.get_decoration(stage_input.id, spv::DecorationIndex);

        R.inputs.push_back( _getStageInputs(x) );
        R.inputs.back().name     = stage_input.name;
        R.inputs.back().location = location;


        VKA_DEBUG("Stage input: {}  id: {} type_id: {} base_type_id: {},  Format: {}   attachment_index: {},  index: {}", stage_input.name,
                                     stage_input.id,
                                     stage_input.type_id,
                                     stage_input.base_type_id,
                                     vk::to_string( R.inputs.back().format),
                                      attachment_index, index);
    }

    std::sort( R.inputs.begin(),
               R.inputs.end(),
               [](VertexInput_t const & l, VertexInput_t const & r)
    {
        return l.location < r.location;
    });

    for(auto & stage_output : resources.stage_outputs)
    {
        //glsl.get_type(get_type(type_id).self)
        auto & x = glsl.get_type( glsl.get_type( stage_output.type_id ).self );

        unsigned location         = glsl.get_decoration(stage_output.id, spv::DecorationLocation);
        unsigned attachment_index = glsl.get_decoration(stage_output.id, spv::DecorationInputAttachmentIndex);
        unsigned index            = glsl.get_decoration(stage_output.id, spv::DecorationIndex);

        R.outputs.push_back( _getStageInputs(x) );
        R.outputs.back().name = stage_output.name;
        R.outputs.back().location = location;

        VKA_DEBUG("Stage Outputs: {}  id: {} type_id: {} base_type_id: {},   Format: {}   attachment_index: {},  index: {}", stage_output.name,
                                     stage_output.id,
                                     stage_output.type_id,
                                     stage_output.base_type_id,
                                     vk::to_string( R.outputs.back().format ),
                                     attachment_index, index);
    }

    std::sort( R.outputs.begin(),
               R.outputs.end(),
               [](VertexInput_t const & l, VertexInput_t const & r)
    {
        return l.location < r.location;
    });


    // Get all sampled images in the shader.
    for (auto &resource : resources.sampled_images)
    {
        unsigned set     = glsl.get_decoration(resource.id, spv::DecorationDescriptorSet);
        unsigned binding = glsl.get_decoration(resource.id, spv::DecorationBinding);



        auto & D  = R.descriptors[resource.name];
        D.name    = resource.name;
        D.set     = set;
        D.binding = binding;
        D.type    = vk::DescriptorType::eCombinedImageSampler;
        D.size    = 0;

        D.arraySize = 0;
        {
            auto & t = glsl.get_type(resource.type_id);

            if( t.array.size() == 1)
            {
                D.arraySize = t.array[0];
            }
        }

        VKA_DEBUG("SampledImage ({}): set {}   binding {}     array size: {}", resource.name, D.set, D.binding, D.arraySize);
    }


    for(auto & cis : glsl.get_combined_image_samplers())
    {
        VKA_DEBUG("Combined Image Sampler: combined_id: {}", cis.combined_id);
        VKA_DEBUG("Combined Image Sampler: image_id: {}", cis.image_id);
        VKA_DEBUG("Combined Image Sampler: sampler_id: {}", cis.sampler_id);
    }

    for (auto &resource : resources.uniform_buffers)
    {
        unsigned set     = glsl.get_decoration(resource.id, spv::DecorationDescriptorSet);
        unsigned binding = glsl.get_decoration(resource.id, spv::DecorationBinding);

        auto & D  = R.descriptors[resource.name];
        D.name    = resource.name;
        D.set     = set;
        D.binding = binding;
        D.type    = vk::DescriptorType::eUniformBuffer;
        auto  & base_type = glsl.get_type(resource.base_type_id);
        D.size    = glsl.get_declared_struct_size( base_type );

        D.arraySize = 0;
        {
            auto & t = glsl.get_type(resource.type_id);

            if( t.array.size() == 1)
            {
                D.arraySize = t.array[0];
            }
        }

        VKA_DEBUG("UniformBuffer ({}): set {}   binding {}   arraySize: {}", resource.name, D.set, D.binding, D.arraySize);
    }


    for (auto &resource : resources.storage_buffers)
    {
        unsigned set     = glsl.get_decoration(resource.id, spv::DecorationDescriptorSet);
        unsigned binding = glsl.get_decoration(resource.id, spv::DecorationBinding);

        auto & D  = R.descriptors[resource.name];
        D.name    = resource.name;
        D.set     = set;
        D.binding = binding;
        D.type    = vk::DescriptorType::eStorageBuffer;
        auto  & base_type = glsl.get_type(resource.base_type_id);
        D.size    = glsl.get_declared_struct_size(base_type);

        D.arraySize = 0;
        {
            auto & t = glsl.get_type(resource.type_id);

            if( t.array.size() == 1)
            {
                D.arraySize = t.array[0];
            }
        }

        VKA_DEBUG("StorageBuffer ({}): set {}   binding {}   arraySize: {}", resource.name, D.set, D.binding, D.arraySize);
    }

}

void ShaderReflect::parseSPIRV(const std::vector<uint32_t> & src)
{
    _update_reflection(src, *this);
}

}
