#include <vka/utils/VulkanHelperFunctions.h>

#include <vka/core/System.h>
#include <vka/core/Global.h>
#include <vka/core/ScreenBuffer.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/ShaderReflect.h>
#include <vka/core/TextureMemoryPool.h>
#include <vka/core/RenderPassCreateInfo.h>


#include <vka/vka.h>
#include <vka/logging.h>

#include <fstream>
#include <set>
#include <chrono>

namespace vka
{

GLOBAL_OBJECTS_t GLOBAL;
Logger vkalogger;

vk::Instance System::createInstance(SystemCreateInfo const & createInfo)
{
    bool m_enable_validation_layers = true;

    if(m_enable_validation_layers && !checkValidationLayerSupport(createInfo.requiredValidationLayers) )
    {
        throw std::runtime_error("Validation layers required but not available");
    }

    std::vector<char const*>  required_extensions;
    std::vector<char const*>  validationLayers;

    for(auto & v : createInfo.requiredExtensions)
    {
        VKA_DEBUG("Extension: {}", v );
        required_extensions.push_back( &v[0] );
    }
    for(auto & v : createInfo.requiredValidationLayers)
    {
        VKA_DEBUG("Validation Layer: {}", v );
        validationLayers.push_back( &v[0] );
    }

    vk::ApplicationInfo appInfo;
    appInfo.setPApplicationName( createInfo.applicationName.c_str() )
           .setApplicationVersion( VK_MAKE_VERSION(createInfo.applicationVersionMajor, createInfo.applicationVersionMinor, createInfo.applicationVersionPatch) )
           .setPEngineName("No Engine")
           .setEngineVersion(VK_MAKE_VERSION(createInfo.engineVersionMajor, createInfo.engineVersionMinor, createInfo.engineVersionPatch) )
           .setApiVersion( VK_API_VERSION_1_0);

    vk::InstanceCreateInfo vulkan_createInfo;

    vulkan_createInfo.setPApplicationInfo(&appInfo)
              .setEnabledExtensionCount(  static_cast<uint32_t>(required_extensions.size()) )
              .setPpEnabledExtensionNames(required_extensions.data())
              .setEnabledLayerCount(0);

    if( m_enable_validation_layers )
    {
        vulkan_createInfo.setEnabledLayerCount(   static_cast<uint32_t>(validationLayers.size()) )
                  .setPpEnabledLayerNames( validationLayers.data() );
    }

    auto instance = vk::createInstance(vulkan_createInfo);
    if(!instance)
    {
        VKA_CRIT("Failed to create Instance!");
        throw std::runtime_error("Failed to create instance");
    }


#if VK_HEADER_VERSION==70
    //G_DYNAMIC_DISPATCHER.init(instance);
#else
    static_assert( VK_HEADER_VERSION >= 126 , "Incorrect version of vulkan.hpp!");
   // G_DYNAMIC_DISPATCHER.init(vkGetInstanceProcAddr);
   // G_DYNAMIC_DISPATCHER.init(instance);

    //G_DYNAMIC_DISPATCHER = vka::getInstanceDispatcher(instance);
#endif

    return instance;
}


vk::Device createLogicalDevice(physicalDeviceInfo & p_physical_device,
                               vk::ArrayProxy<const std::string> requiredValidationLayers,
                               vk::ArrayProxy<const std::string> requiredDeviceExtensions);

VulkanObjects System::createVulkanObjects(vk::Instance instance, vk::SurfaceKHR surface, const SystemCreateInfo &createInfo)
{
    VulkanObjects objects;
    objects.instance = instance;
    //objects.surface = surface;
    objects.createInfo = createInfo;

    // Create the debug callback function if needed
    if( createInfo.debugCallbackFunction )
    {
        objects.debugCallback = createDebugCallback( instance, createInfo.debugCallbackFunction );
    }

    auto pd = createPhysicalDevice( instance, surface, createInfo.requiredDeviceExtensions);

    objects.physicalDevice    = pd.physicalDevice;
    objects.deviceQueueFamily = pd.queueFamily;

    objects.device = createLogicalDevice( pd,
                                          createInfo.requiredValidationLayers,
                                          createInfo.requiredDeviceExtensions);
    return objects;
}

bool are_device_extensions_supported(const vk::PhysicalDevice &device, vk::ArrayProxy<const std::string> requiredExtensions)
{
    std::vector<vk::ExtensionProperties> availableExtensions = device.enumerateDeviceExtensionProperties();


    for(auto const & req : requiredExtensions)
    {
        bool found = false;
        for(auto & avail : availableExtensions)
        {
            if( avail.extensionName == req)
            {
                found = true;
                break;
            }
        }
        if( found ) break;
        VKA_DEBUG( "Extension Not Supported: {}", req );

        return false;
    }
    return true;
}

bool can_present_to_surface(const vk::PhysicalDevice &device, const vk::SurfaceKHR &surface)
{
    //auto capabilities = device.getSurfaceCapabilitiesKHR( surface );
    auto formats      = device.getSurfaceFormatsKHR(      surface );
    auto presentModes = device.getSurfacePresentModesKHR( surface );

    return !formats.empty() && !presentModes.empty();
}

QFamily find_queue_families(const vk::PhysicalDevice &device, const vk::SurfaceKHR &surface)
{
    uint32_t graphicsQ = std::numeric_limits<uint32_t>::max();
    uint32_t presentQ  = std::numeric_limits<uint32_t>::max();

    auto queueFamilies = device.getQueueFamilyProperties();

    uint i = 0;
    for (const vk::QueueFamilyProperties & queueFamily : queueFamilies)
    {
        if (queueFamily.queueCount > 0 && (queueFamily.queueFlags & vk::QueueFlagBits::eGraphics) ) {
            graphicsQ = i;
        }

        //VkBool32 presentSupport = false;

        auto presentSupport = device.getSurfaceSupportKHR(i, surface);

        if( queueFamily.queueCount > 0 && presentSupport)
        {
            presentQ = i;
        }

        if ( presentQ < std::numeric_limits<uint32_t>::max() && graphicsQ < std::numeric_limits<uint32_t>::max() )
        {
            break;
        }

        i++;
    }

    return { graphicsQ , presentQ};
}


physicalDeviceInfo System::createPhysicalDevice( vk::Instance instance,
                                                 vk::SurfaceKHR surface,
                                                 vk::ArrayProxy<const std::string> deviceExtensions)
{
    auto devices = instance.enumeratePhysicalDevices();

    if( devices.size() == 0)
    {
        throw std::runtime_error("failed to find GPUs with Vulkan support!");
    }
    VKA_DEBUG( "Total Devices found: {}", devices.size() );

    std::vector<const char *> DeviceExtensions;

    for(auto & d : deviceExtensions)
        DeviceExtensions.push_back( d.data() );

    physicalDeviceInfo returnVal;

    for ( vk::PhysicalDevice & device : devices)
    {
        // a device is suitable if it supports all the extensions
        if( are_device_extensions_supported( device, deviceExtensions ) )
        {
            VKA_DEBUG( " supports all the required extensions" );

            // If it has appropriate graphics and present queues
            auto Qindex = find_queue_families(device, surface );

            if( Qindex.present < std::numeric_limits<uint32_t>::max() && Qindex.graphics < std::numeric_limits<uint32_t>::max() )
            {
                VKA_DEBUG( "Device supports Graphics and Present Queues" );
                // it can present to the surface
                if( can_present_to_surface(device,  surface ) )
                {
                    VKA_DEBUG( " Can present to surface" );
                    VKA_DEBUG( "Suitable graphics device found!" );

                    returnVal.physicalDevice = device;
                    returnVal.queueFamily = Qindex;
                    return returnVal;
                }
            }
        }
    }
    VKA_CRIT("Valid device not found");
    return returnVal;
}




bool System::checkValidationLayerSupport(const std::vector<std::string> & requiredValidationLayers)
{
    auto available_layers = vk::enumerateInstanceLayerProperties();

    for(auto & layers : available_layers)
    {
        VKA_DEBUG( "Validation Layer: {:s}" , layers.layerName );
    }

    for (auto & layerName : requiredValidationLayers)
    {
        bool layerFound = false;

        for (const auto& layerProperties : available_layers)
        {
            if( std::string( layerProperties.layerName ) == layerName)
            {
                layerFound = true;
                break;
            }
        }

        if (!layerFound)
        {
            VKA_ERROR(  "Validation layers requested but not available: {:s}" , layerName );
            return false;
        }
    }

    return true;
}




vk::Device createLogicalDevice(physicalDeviceInfo & p_physical_device, vk::ArrayProxy<const std::string> requiredValidationLayers, vk::ArrayProxy<const std::string> requiredDeviceExtensions)
{
    auto graphicsQ = p_physical_device.queueFamily.graphics;
    auto presentQ = p_physical_device.queueFamily.present;

    if( graphicsQ == std::numeric_limits<uint32_t>::max() || presentQ == std::numeric_limits<uint32_t>::max() )
    {
        VKA_ERROR( "Cannot create logical device. The physical device does not support the proper queues" );
        throw std::runtime_error("Cannot create logical device. The physical device does not support the proper queues");
    }

    std::set<uint32_t> uniqueQueueFamilies = {graphicsQ, presentQ};

    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;

    float queuePriority = 1.0f;
    for(auto queueFamily : uniqueQueueFamilies)
    {
        vk::DeviceQueueCreateInfo queueCreateInfo;

        queueCreateInfo.setQueueFamilyIndex(queueFamily)
                        .setQueueCount( 1 )
                        .setPQueuePriorities(&queuePriority);

        queueCreateInfos.push_back(queueCreateInfo);
    }

    //auto features = p_physical_device.physicalDevice.getFeatures();

    vk::PhysicalDeviceFeatures deviceFeatures;
    deviceFeatures.fillModeNonSolid   = 1;
    deviceFeatures.tessellationShader = 1;
    deviceFeatures.wideLines = 1;

    vk::DeviceCreateInfo createInfo;

    createInfo.setPQueueCreateInfos(    queueCreateInfos.data() )
            .setQueueCreateInfoCount(   static_cast<uint32_t>(queueCreateInfos.size()) )
            .setPEnabledFeatures(       &deviceFeatures )
            .setEnabledExtensionCount(  0)
            .setEnabledLayerCount(      0); // set default to zero

    bool enableValidationLayers = requiredValidationLayers.size() > 0;

    std::vector<const char*> validationLayers;

    for(auto & s : requiredValidationLayers)
        validationLayers.push_back( &s[0] );

    if( enableValidationLayers )
    {
        VKA_DEBUG( "Enabling validation layers" );
        for(auto & c : validationLayers)
            VKA_DEBUG(  "   layer: {:s}" , c );

        createInfo.setEnabledLayerCount(   static_cast<uint32_t>(validationLayers.size()) )
                  .setPpEnabledLayerNames( validationLayers.data() );
    }

    std::vector<const char*> deviceExtensions;
    for(auto & s : requiredDeviceExtensions)
        deviceExtensions.push_back( &s[0] );

    for(auto & c : deviceExtensions)
        VKA_DEBUG(  "device extensions: {:s}" , c );

    createInfo.setEnabledExtensionCount(   static_cast<uint32_t>(deviceExtensions.size()) )
            .setPpEnabledExtensionNames( deviceExtensions.data() );


    auto device = p_physical_device.physicalDevice.createDevice(createInfo);

    if( !device )
    {
        VKA_CRIT( "failed to create logical device!" );
        throw std::runtime_error("failed to create logical device!");
    }
    VKA_DEBUG( "Logical Device created" );

    return device;
#if 0
    m_graphics_queue = device.getQueue( p_Qfamily.graphics, 0u);
    m_present_queue  = device.getQueue( p_Qfamily.present,  0u);

    if(!m_graphics_queue)
    {
        VKA_ERROR( "Error getting graphics queue" );;
        throw std::runtime_error("Error getting graphics queue");
    }
    if( !m_present_queue )
    {
        VKA_ERROR( "Error getting present queue" );;
        throw std::runtime_error("Error getting present queue");
    }

    VKA_LOG( "Graphics Queue created" );
    VKA_LOG( "Present Queue created" );

    VKA_LOG( "Logical Device created successfully" );
    //vkGetDeviceQueue(device, indices.graphics, 0, &graphicsQueue);
#endif
}




vk::SurfaceFormatKHR System::getSurfaceFormat(vk::PhysicalDevice physical_device, vk::SurfaceKHR  surface)
{
    vk::SurfaceFormatKHR srfFormat;

    vk::Format        & colorFormat = srfFormat.format;
    vk::ColorSpaceKHR & colorSpace  = srfFormat.colorSpace;

    //auto device = get_device();
    //auto physical_device = get_physical_device();

    auto surfaceFormats = physical_device.getSurfaceFormatsKHR(surface);
    //std::vector<vk::SurfaceFormatKHR> surfaceFormats(formatCount);
    //VK_CHECK_RESULT(fpGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, &formatCount, surfaceFormats.data()));

    const auto defaultFormat = vk::Format::eB8G8R8A8Unorm;

    // If the surface format list only includes one entry with VK_FORMAT_UNDEFINED,
    // there is no preferered format, so we assume VK_FORMAT_B8G8R8A8_UNORM
    if ((surfaceFormats.size() == 1) && (surfaceFormats[0].format == vk::Format::eUndefined/*VK_FORMAT_UNDEFINED*/))
    {
        colorFormat = defaultFormat;// VK_FORMAT_B8G8R8A8_UNORM;
        colorSpace  = surfaceFormats[0].colorSpace;
    }
    else
    {
        // iterate over the list of available surface format and
        // check for the presence of VK_FORMAT_B8G8R8A8_UNORM
        bool found_B8G8R8A8_UNORM = false;
        for (auto&& surfaceFormat : surfaceFormats)
        {
            if (surfaceFormat.format == defaultFormat)
            {
                colorFormat = surfaceFormat.format;
                colorSpace = surfaceFormat.colorSpace;
                found_B8G8R8A8_UNORM = true;
                break;
            }
        }

        // in case VK_FORMAT_B8G8R8A8_UNORM is not available
        // select the first available color format
        if (!found_B8G8R8A8_UNORM)
        {
            colorFormat = surfaceFormats[0].format;
            colorSpace = surfaceFormats[0].colorSpace;
        }
    }

    return srfFormat;
}

void System::destroyScreenBuffer(ScreenBuffer & sb)
{
    VKA_DEBUG("==== Destroying ScreenBuffer ====");

    for(auto x : sb.m_frameBuffers)
    {
        auto imageViews = info(x).createInfo.attachments;
        destroyFramebuffer(x);

        for(auto & v : imageViews)
        {
            // Only destroy the image view if it is NOT a depth texture
            auto format = info(v).createInfo.format;

             if(
             format == vk::Format::eD16Unorm         ||
             format == vk::Format::eX8D24UnormPack32 ||
             format == vk::Format::eD32Sfloat        ||
             format == vk::Format::eS8Uint           ||
             format == vk::Format::eD16UnormS8Uint   ||
             format == vk::Format::eD24UnormS8Uint   ||
             format == vk::Format::eD32SfloatS8Uint )
             {
                 // don't delete, owned by something else.
             }
             else
             {
                 destroyImageView(v);
             }

        }

    }
    sb.m_frameBuffers.clear();
    if( sb.m_renderPass)
    {
        destroyRenderPass(sb.m_renderPass);
        sb.m_renderPass = vk::RenderPass();
    }

    if( sb.m_SwapChain)
    {
        destroySwapchain( sb.m_SwapChain );
        sb.m_SwapChain = vk::SwapchainKHR();
    }

    VKA_DEBUG("==== Finished Destroying ScreenBuffer ====");
}


ScreenBuffer System::createScreenBuffer(ScreenBufferCreateInfo const & createInfo,  ScreenBuffer * oldScreenBuffer)
{

    auto physicalDevice = m_Vulkan.physicalDevice;
    auto device         = m_Vulkan.device;

    bool verticalSync   = createInfo.verticalSync;
    auto surface        = createInfo.surface;
    auto extent         = createInfo.extent;

    ScreenBuffer SC;

    vk::SwapchainKHR oldSwapchain;

    if( oldScreenBuffer )
        oldSwapchain = oldScreenBuffer->m_SwapChain;

    auto surfaceFormat = getSurfaceFormat(physicalDevice, surface);

    // Get physical device surface properties and formats
    vk::SurfaceCapabilitiesKHR surfCaps = physicalDevice.getSurfaceCapabilitiesKHR(surface);

    // Get available present modes
    auto presentModes = physicalDevice.getSurfacePresentModesKHR(surface);
    assert(presentModes.size() > 0);


    vk::Extent2D swapchainExtent;
    // If width (and height) equals the special value 0xFFFFFFFF, the size of the surface will be set by the swapchain
    if (surfCaps.currentExtent.width == 0xFFFFFFFF)
    {
        // If the surface size is undefined, the size is set to
        // the size of the images requested.
        swapchainExtent = extent;
    }
    else
    {
        // If the surface size is defined, the swap chain size must match
        swapchainExtent = surfCaps.currentExtent;
    }


    // Select a present mode for the swapchain

    // The VK_PRESENT_MODE_FIFO_KHR mode must always be present as per spec
    // This mode waits for the vertical blank ("v-sync")
    vk::PresentModeKHR swapchainPresentMode = vk::PresentModeKHR::eFifo;// VK_PRESENT_MODE_FIFO_KHR;

    // If v-sync is not requested, try to find a mailbox mode
    // It's the lowest latency non-tearing present mode available
    if ( !verticalSync )
    {
        //for (size_t i = 0; i < presentModes.size(); i++)
        for(auto & presentMode : presentModes)
        {
            if (presentMode == vk::PresentModeKHR::eMailbox /*VK_PRESENT_MODE_MAILBOX_KHR*/)
            {
                swapchainPresentMode = vk::PresentModeKHR::eMailbox;// VK_PRESENT_MODE_MAILBOX_KHR;
                break;
            }
            if ((swapchainPresentMode != vk::PresentModeKHR::eMailbox ) && (presentMode == vk::PresentModeKHR::eImmediate))
            {
                swapchainPresentMode = vk::PresentModeKHR::eImmediate;
            }
        }
    }

    // Determine the number of images
    uint32_t desiredNumberOfSwapchainImages = surfCaps.minImageCount + createInfo.extraSwapchainImages;
    desiredNumberOfSwapchainImages          = std::max( createInfo.maxSwapchainImages, desiredNumberOfSwapchainImages);

    if ((surfCaps.maxImageCount > 0) && (desiredNumberOfSwapchainImages > surfCaps.maxImageCount))
    {
        desiredNumberOfSwapchainImages = surfCaps.maxImageCount;
    }

    // Find the transformation of the surface
    vk::SurfaceTransformFlagBitsKHR preTransform;

    if (surfCaps.supportedTransforms & vk::SurfaceTransformFlagBitsKHR::eIdentity /*VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR*/)
    {
        // We prefer a non-rotated transform
        preTransform = vk::SurfaceTransformFlagBitsKHR::eIdentity;
    }
    else
    {
        preTransform = surfCaps.currentTransform;
    }

    // Find a supported composite alpha format (not all devices support alpha opaque)
    vk::CompositeAlphaFlagBitsKHR compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;

    // Simply select the first composite alpha format available
    std::vector<vk::CompositeAlphaFlagBitsKHR> compositeAlphaFlags = {
        vk::CompositeAlphaFlagBitsKHR::eOpaque /*VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR*/,
        vk::CompositeAlphaFlagBitsKHR::ePreMultiplied /*VK_COMPOSITE_ALPHA_PRE_MULTIPLIED_BIT_KHR*/,
        vk::CompositeAlphaFlagBitsKHR::ePostMultiplied /*VK_COMPOSITE_ALPHA_POST_MULTIPLIED_BIT_KHR*/,
        vk::CompositeAlphaFlagBitsKHR::eInherit /*VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR*/,
    };

    for (auto& compositeAlphaFlag : compositeAlphaFlags)
    {
        if (surfCaps.supportedCompositeAlpha & compositeAlphaFlag)
        {
            compositeAlpha = compositeAlphaFlag;
            break;
        }
    }


    vk::SwapchainCreateInfoKHR swapchainCI;
    swapchainCI.surface               = surface;
    swapchainCI.minImageCount         = desiredNumberOfSwapchainImages;
    swapchainCI.imageFormat           = surfaceFormat.format;
    swapchainCI.imageColorSpace       = surfaceFormat.colorSpace;
    swapchainCI.imageExtent           = swapchainExtent;
    swapchainCI.imageUsage            = vk::ImageUsageFlagBits::eColorAttachment;// VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapchainCI.preTransform          = preTransform;
    swapchainCI.imageArrayLayers      = 1;
    swapchainCI.imageSharingMode      = vk::SharingMode::eExclusive;
    swapchainCI.queueFamilyIndexCount = 0;
    swapchainCI.pQueueFamilyIndices   = nullptr;
    swapchainCI.presentMode           = swapchainPresentMode;

    swapchainCI.oldSwapchain   = oldSwapchain;

    // Setting clipped to VK_TRUE allows the implementation to discard rendering outside of the surface area
    swapchainCI.clipped               = VK_TRUE;
    swapchainCI.compositeAlpha        = compositeAlpha;

    // Enable transfer source on swap chain images if supported
    if (surfCaps.supportedUsageFlags & vk::ImageUsageFlagBits::eTransferSrc/* VK_IMAGE_USAGE_TRANSFER_SRC_BIT*/)
    {
        swapchainCI.imageUsage |= vk::ImageUsageFlagBits::eTransferSrc/* VK_IMAGE_USAGE_TRANSFER_SRC_BIT*/;
    }

    // Enable transfer destination on swap chain images if supported
    if (surfCaps.supportedUsageFlags & vk::ImageUsageFlagBits::eTransferDst/* VK_IMAGE_USAGE_TRANSFER_DST_BIT*/)
    {
        swapchainCI.imageUsage |= vk::ImageUsageFlagBits::eTransferDst;
    }

    SC.m_SwapChain = device.createSwapchainKHR(swapchainCI);
    if( !SC.m_SwapChain )
    {
        throw std::runtime_error("Error creating swapchain");
    }





    auto & Info = GLOBAL.swapchains[SC.m_SwapChain];

    // Get the swap chain images
    Info.images          = device.getSwapchainImagesKHR(SC.m_SwapChain);
    auto swapchainImages = Info.images;

    assert(Info.images.size());

    // temporary, no depth testing
    vk::ImageView depthView   = vk::ImageView();
    vk::Format    depthFormat = vk::Format::eUndefined;

    if( std::holds_alternative<vk::ImageView>(createInfo.depthStencilFormatOrImage))
    {
        depthView = std::get<vk::ImageView>(createInfo.depthStencilFormatOrImage);

        auto & imageViewInfo = GLOBAL.imageViewsContainer.info(depthView);
        depthFormat = imageViewInfo.createInfo.format;
    }
    else if( std::holds_alternative<vk::Format>(createInfo.depthStencilFormatOrImage) )
    {
        // need to allocate the image ourselves
    }
    //============================================================
    // Create the render pass
    //============================================================
    {
        vka::RenderPassCreateInfo2 renderPassCreateInfo;
        auto swapchain_format = swapchainCI.imageFormat;

        renderPassCreateInfo.addColorAttachment( swapchain_format, vk::ImageLayout::ePresentSrcKHR);
        if( depthFormat != vk::Format::eUndefined)
        {
            renderPassCreateInfo.addDepthStenchilAttachment( depthFormat, vk::ImageLayout::eDepthStencilAttachmentOptimal);
        }

        auto renderPass = createRenderPass(renderPassCreateInfo);

        SC.m_clearColors.resize( renderPassCreateInfo.attachmentDescription.size() );

        if( depthFormat != vk::Format::eUndefined )
        {
            SC.setDepthClear(1.f, 0);
        }
        SC.setClearColor(0,0,0,1);
        SC.m_renderPass = renderPass;
    }
    //============================================================


    //============================================================
    // Create the Framae buffers.
    //============================================================
    for (uint32_t i = 0; i < Info.images.size() ; i++)
    {
        vk::ImageViewCreateInfo colorAttachmentView;

        colorAttachmentView.format = swapchainCI.imageFormat;
        colorAttachmentView.components = {
                                            vk::ComponentSwizzle::eR,
                                            vk::ComponentSwizzle::eG,
                                            vk::ComponentSwizzle::eB,
                                            vk::ComponentSwizzle::eA
                                        };
        colorAttachmentView.subresourceRange.aspectMask     = vk::ImageAspectFlagBits::eColor;// VK_IMAGE_ASPECT_COLOR_BIT;
        colorAttachmentView.subresourceRange.baseMipLevel   = 0;
        colorAttachmentView.subresourceRange.levelCount     = 1;
        colorAttachmentView.subresourceRange.baseArrayLayer = 0;
        colorAttachmentView.subresourceRange.layerCount     = 1;
        colorAttachmentView.viewType                        = vk::ImageViewType::e2D;// VK_IMAGE_VIEW_TYPE_2D;
        colorAttachmentView.image                           = swapchainImages[i];

        // create the image view
        auto framebufferImageView = createImageView(colorAttachmentView);// device.createImageView(colorAttachmentView);

        {
            std::vector<vk::ImageView> attachments;
            attachments.push_back(framebufferImageView);

            if( depthView )
                attachments.push_back(depthView);

            vka::FramebufferCreateInfo2 frameBufferCreateInfo;
            // All frame buffers use the same renderpass setup
            frameBufferCreateInfo.renderPass      = SC.m_renderPass;
            frameBufferCreateInfo.attachments     = attachments;
            frameBufferCreateInfo.width           = swapchainExtent.width;
            frameBufferCreateInfo.height          = swapchainExtent.height;
            frameBufferCreateInfo.layers          = 1;

            auto fb = createFramebuffer(frameBufferCreateInfo);
            SC.m_frameBuffers.push_back(fb);
        }
    }


    SC.m_extent     = swapchainExtent;
    Info.createInfo = swapchainCI;

    // If an existing swap chain is re-created, destroy the old swap chain
    // This also cleans up all the presentable images
    if (oldScreenBuffer)
    {
        destroyScreenBuffer(*oldScreenBuffer);
    }

    return SC;
}


vk::Semaphore   System::createSemaphore(const vk::SemaphoreCreateInfo &info)
{
    auto s = m_Vulkan.device.createSemaphore(info);
    GLOBAL.semaphores[s].createInfo = info;

    return s;
}


vk::Framebuffer System::createFramebuffer(vka::FramebufferCreateInfo2 const & createInfo)
{
    auto C = createInfo;

    auto ci = C.create();

    auto fb = m_Vulkan.device.createFramebuffer(ci);

    GLOBAL.framebuffersContainer.insert( fb, FrameBufferInfo());

    auto & info = GLOBAL.framebuffersContainer.info(fb);

    info.createInfo = C;

    auto & rpInfo = GLOBAL.renderPassContainer.info( C.renderPass );
    rpInfo.frameBuffers.insert(fb);


    for(auto v : C.attachments)
    {
        auto & imageViewInfo = GLOBAL.imageViewsContainer.info(v);
        imageViewInfo.framebuffers.insert(fb);
    }
    return fb;
}




void System::freeCommandBuffers( vk::CommandPool commandPool, vk::ArrayProxy<const vk::CommandBuffer> commandBuffers )
{
    m_Vulkan.device.freeCommandBuffers(commandPool,commandBuffers );
}

std::vector<vk::CommandBuffer> System::allocateCommandBuffers(vk::CommandBufferAllocateInfo const & cp)
{
    return m_Vulkan.device.allocateCommandBuffers( cp );
}

vka::CommandBuffer System::allocateCommandBuffer( vk::CommandBufferLevel level, vk::CommandPool cp )
{
    vk::CommandBufferAllocateInfo allocInfo;

    allocInfo.level              = level;
    allocInfo.commandPool        = cp;
    allocInfo.commandBufferCount = 1;

    std::vector<vk::CommandBuffer> commandBuffer_v = allocateCommandBuffers( allocInfo );

    if( commandBuffer_v.size() == 0 )
        throw std::runtime_error("Error creating command buffers");

    return commandBuffer_v[0];
}

vk::CommandPool System::createCommandPool()
{
    vk::CommandPoolCreateInfo poolInfo;


    poolInfo.queueFamilyIndex = static_cast<uint32_t>( GLOBAL.system->m_Vulkan.queueGraphicsFamily );
    poolInfo.flags            = vk::CommandPoolCreateFlagBits::eResetCommandBuffer; // Optional

    return createCommandPool(poolInfo);
}

vk::CommandPool System::createCommandPool(vk::CommandPoolCreateInfo const & info)
{
    auto cp = vka::System::get().m_Vulkan.device.createCommandPool(info);

    if(!cp)
    {
        throw std::runtime_error("Error Creating CommandPool");
    }
    VKA_DEBUG("CommandPool Created: {}", static_cast<void*>(cp));

    GLOBAL.commandPools[cp].createInfo = info;

    return cp;
}


void System::submitCommandBuffers( vk::ArrayProxy<const vk::CommandBuffer> buffers,
                           vk::ArrayProxy<const vk::Semaphore> waitSemaphores,
                           vk::ArrayProxy<const vk::PipelineStageFlags> waitStages,
                           vk::ArrayProxy<const vk::Semaphore> signalSemaphores
                           )
{
    vk::SubmitInfo info;
    info.waitSemaphoreCount   = waitSemaphores.size();
    info.pWaitSemaphores      = waitSemaphores.data();

    info.signalSemaphoreCount = signalSemaphores.size();
    info.pSignalSemaphores    = signalSemaphores.data();

    info.commandBufferCount   = buffers.size();
    info.pCommandBuffers      = buffers.data();

    info.pWaitDstStageMask = waitStages.data();

    submitCommandBuffers(info);
}

void System::submitCommandBuffers( vk::ArrayProxy<const vk::SubmitInfo> submits)
{
    m_Vulkan.queuePresent.submit(submits, vk::Fence());
    vka::System::get().m_Vulkan.device.waitIdle();
}

bool System::createSystem(SystemCreateInfo2 createInfo)
{
    auto & S = get();

    S.m_Vulkan      = createInfo;
    S.m_surfaceTemp = createInfo.surface;
    GLOBAL.system   = &S;
    return true;
}

System& System::get()
{
    static auto s = std::shared_ptr<System>( new System() );

    return *s;
}

System::~System()
{
    destroy();
}


void System::destroy()
{


    {
        auto d = GLOBAL.descriptorSetsContainer;
        for(auto & x : d)
        {
            destroyDescriptorSet(x.first);
        }
    }



    {
        auto d = GLOBAL.descriptorPoolsContainer;
        for(auto & x : d)
        {
            destroyDescriptorPool(x.first);
        }
    }

    {
        auto d = GLOBAL.pipelinesContainer;
        for(auto & x : d)
        {
            destroyPipeline(x.first);
        }
    }

    {
        auto d = GLOBAL.shaderModulesContainer;
        for(auto & x : d)
        {
            destroyShaderModule(x.first);
        }
    }

    {
        auto d = GLOBAL.pipelineLayoutsContainer;
        for(auto & x : d)
        {
            destroyPipelineLayout(x.first);
        }
    }

    {
        auto d = GLOBAL.descriptorSetLayoutsContainer;
        for(auto & x : d)
        {
            destroyDescriptorSetLayout(x.first);
        }
    }

    {
        auto d = GLOBAL.bufferPools;
        for(auto & x : d)
        {
            BufferPool p;
            p.m_buffer = x.first;
            destroyBufferPool(p);
        }
    }

    {
        auto d = GLOBAL.buffersContainer;
        for(auto & x : d)
        {
            destroyBuffer(x.first);
        }
    }






    {
        auto d = GLOBAL.semaphores;
        for(auto & x : d)
        {
            destroySemaphore(x.first);
        }
    }

    {
        auto d = GLOBAL.commandPools;
        for(auto & x : d)
        {
            destroyCommandPool(x.first);
        }
    }

    {
        auto d = GLOBAL.samplersContainer;
        for(auto & x : d)
        {
            destroySampler(x.first);
        }
    }

    {
        auto d = GLOBAL.framebuffersContainer;
        for(auto & x : d)
        {
            destroyFramebuffer(x.first);
        }
    }

    {
        auto d = GLOBAL.renderPassContainer;
        for(auto & x : d)
        {
            destroyRenderPass(x.first);
        }
    }

    {
        auto d = GLOBAL.swapchains;
        for(auto & x : d)
        {
            destroySwapchain(x.first);
        }
    }

    {
        auto d = GLOBAL.imageViewsContainer;
        for(auto & x : d)
        {
            destroyImageView(x.first);
        }
    }

    {
        auto d = GLOBAL.imagesContainer;
        for(auto & x : d)
        {
            destroyImage(x.first);
        }
    }

    {
        auto d = GLOBAL.memoryContainer;
        for(auto & x : d)
        {
            freeMemory(x.first);
        }
    }

}


void System::freeMemory( vk::DeviceMemory mem)
{

    if( GLOBAL.memoryContainer.exists(mem))
    {
        auto & info = GLOBAL.memoryContainer.info(mem);
        if( info.boundBuffers.size() )
        {           
            throw std::runtime_error("Cannot free this memory, buffers are currently bound to it");
        }

        if( info.boundImages.size() )
        {
            throw std::runtime_error("Cannot free this memory, images are currently bound to it");
        }

        if( info.map )
        {
            unmapMemory(mem);
        }

        vka::System::get().m_Vulkan.device.freeMemory(mem);

        VKA_DEBUG("Memory Freed ({} bytes): {}",  GLOBAL.memoryContainer.info(mem).createInfo.allocationSize, static_cast<void*>(mem));
        GLOBAL.memoryContainer.erase(mem);
    }
}

void System::destroyBuffer(vk::Buffer s)
{
    if( GLOBAL.buffersContainer.exists(s) )
    {
        auto & binfo = GLOBAL.buffersContainer.info(s);


        if( binfo.memory ) // this buffer is bound to a memory block
        {
            VKA_DEBUG("Buffer {} unlinked from memory {}",  static_cast<void*>(s), static_cast<void*>(binfo.memory));
            // erase the buffer from that memory's set
            GLOBAL.memoryContainer.info(binfo.memory).boundBuffers.erase(s);
            binfo.memory = vk::DeviceMemory();
        }

        // destroy the buffer
        vka::System::get().m_Vulkan.device.destroy(s);

        VKA_DEBUG("Buffer destroyed ({} bytes): {}",  GLOBAL.buffersContainer.info(s).createInfo.size, static_cast<void*>(s));

        // Erase any information stored on it.
        GLOBAL.buffersContainer.erase(s);
    }
}

void System::destroySemaphore(vk::Semaphore s)
{
    if( GLOBAL.semaphores.count(s) == 1)
    {
        vka::System::get().m_Vulkan.device.destroy(s);
        GLOBAL.semaphores.erase(s);
        VKA_DEBUG("Semaphore destroyed: {}", static_cast<void*>(s));
    }
}

void System::destroyCommandPool(vk::CommandPool rp)
{
    if( GLOBAL.commandPools.count(rp) == 1)
    {
        vka::System::get().m_Vulkan.device.destroy(rp);
        GLOBAL.commandPools.erase(rp);
        VKA_DEBUG("CommandPool destroyed: {}", static_cast<void*>(rp));
    }
}

void System::destroyFramebuffer(vk::Framebuffer fb)
{
    if( GLOBAL.framebuffersContainer.exists(fb) )
    {

        auto & info = GLOBAL.framebuffersContainer.info(fb);

        // destory the actual frame buffer
        vka::System::get().m_Vulkan.device.destroy(fb);

        {
            // unlink the framebuffer from the renderpass
            auto & rpInfo = GLOBAL.renderPassContainer.info(info.createInfo.renderPass);
            rpInfo.frameBuffers.erase(fb);
        }

        // unlink the framebuffer from all the image views
        for(auto v  : info.createInfo.attachments)
        {
            auto & vInfo = GLOBAL.imageViewsContainer.info(v);

            vInfo.framebuffers.erase(fb);
        }

        // erase the actual container
        GLOBAL.framebuffersContainer.erase(fb);
        VKA_DEBUG("Framebuffer destroyed: {}", static_cast<void*>(fb));
    }
}

void System::destroyRenderPass(vk::RenderPass rp)
{
    if( GLOBAL.renderPassContainer.exists(rp) )
    {
        auto & info = GLOBAL.renderPassContainer.info(rp);

        if(info.pipelines.size() )
        {
            VKA_WARN("RenderPass Is currently being used {} pipelines. Cannot destroy it: ", info.pipelines.size() );
            return;
        }
        if(info.frameBuffers.size() )
        {
            VKA_WARN("RenderPass Is currently being used {} Framebuffers. Cannot destroy it: ", info.frameBuffers.size() );
            return;
        }
        vka::System::get().m_Vulkan.device.destroy(rp);
        GLOBAL.renderPassContainer.erase(rp);
        VKA_DEBUG("RenderPass destroyed: {}", static_cast<void*>(rp));
    }
}
/**
 * @brief System::destroySwapchain
 * @param sw
 *
 * Destroy a swapchain and any image views
 * that were created for it.
 */
void System::destroySwapchain(vk::SwapchainKHR sw)
{
    m_Vulkan.device.destroySwapchainKHR(sw);

    GLOBAL.swapchains.erase(sw);
    VKA_DEBUG("Swapchain destroyed: {}", static_cast<void*>(sw));

}

void System::destroyImageView(vk::ImageView view)
{
    if( GLOBAL.imageViewsContainer.exists(view))
    {
        auto & info = GLOBAL.imageViewsContainer.info(view);

        if( info.swapchains.size() )
        {
            VKA_WARN("This image view is being used by a swapchain, cannot destroy it");
            return;
        }
        if( info.framebuffers.size() )
        {
            VKA_WARN("This image view is being used by a framebuffer, cannot destroy it");
            return;
        }

        m_Vulkan.device.destroyImageView(view);

        // if this view has an associated image.
        if( info.createInfo.image)
        {
            if( GLOBAL.imagesContainer.exists(info.createInfo.image) != 0)
            {
                // unlink the view from it's parent image.
                GLOBAL.imagesContainer.info( info.createInfo.image ).imageViews.erase(view);
            }
        }

        GLOBAL.imageViewsContainer.erase(view);
    }


    VKA_DEBUG("ImageView destroyed: {}", static_cast<void*>(view));
}

/**
 * @brief System::destroyImage
 * @param img
 *
 * Destroy an image. If any imageViews reference it
 * those image views will be destroyed as well.
 */
void System::destroyImage(vk::Image img)
{ 
    auto & info = GLOBAL.imagesContainer.info(img);

    if( info.imageViews.size() )
    {
        for(auto v : info.imageViews)
        {
            VKA_WARN("Image ({}) is still being used by the following imageView: {}", static_cast<void*>(img), v );
        }
        return;
    }


    m_Vulkan.device.destroyImage(img);
    if(info.memory) // we're bound to some mememory
    {

        auto & memInfo = GLOBAL.memoryContainer.info(info.memory);
        memInfo.boundImages.erase(img);
    }
    GLOBAL.imagesContainer.erase(img);


    VKA_DEBUG("Image destroyed: {}", static_cast<void*>(img));

}



bool isEqual( vk::SubpassDescription const& lhs, vk::SubpassDescription const& rhs )
{
    if( lhs.inputAttachmentCount == rhs.inputAttachmentCount )
    {
        for(size_t i=0;i<lhs.inputAttachmentCount;i++)
        {
            if(  lhs.pInputAttachments[i] != rhs.pInputAttachments[i]  )
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }

    if( lhs.colorAttachmentCount == rhs.colorAttachmentCount )
    {
        for(size_t i=0;i<lhs.colorAttachmentCount;i++)
        {
            if(  lhs.pColorAttachments[i] != rhs.pColorAttachments[i]  )
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }

    if( lhs.pDepthStencilAttachment && rhs.pDepthStencilAttachment )
    {
        if( *lhs.pDepthStencilAttachment != *rhs.pDepthStencilAttachment )
        {
            return false;
        }
    }


    if( lhs.preserveAttachmentCount == rhs.preserveAttachmentCount )
    {
        for(size_t i=0;i<lhs.preserveAttachmentCount;i++)
        {
            if(  lhs.pPreserveAttachments[i] != rhs.pPreserveAttachments[i]  )
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }

    return
           ( lhs.pipelineBindPoint == rhs.pipelineBindPoint )
        && ( lhs.inputAttachmentCount == rhs.inputAttachmentCount )
       //& ( lhs.pInputAttachments == rhs.pInputAttachments )
        && ( lhs.colorAttachmentCount == rhs.colorAttachmentCount )
     // && ( lhs.pColorAttachments == rhs.pColorAttachments )
     // && ( lhs.pResolveAttachments == rhs.pResolveAttachments )
     // && ( lhs.pDepthStencilAttachment == rhs.pDepthStencilAttachment )
        && ( lhs.preserveAttachmentCount == rhs.preserveAttachmentCount )
     // && ( lhs.pPreserveAttachments == rhs.pPreserveAttachments );
            ;
}
/**
 * @brief isEqual
 * @param lhs
 * @param rhs
 * @return
 *
 * Checks if the create info of two render passes are equal
 */
bool isEqual( vk::RenderPassCreateInfo const& lhs, vk::RenderPassCreateInfo const& rhs )
{
    if( lhs.subpassCount == rhs.subpassCount)
    {
        for(size_t i=0;i<lhs.subpassCount;i++)
        {
            if( !isEqual( lhs.pSubpasses[i] , rhs.pSubpasses[i] ) )
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }

    if( lhs.dependencyCount == rhs.dependencyCount)
    {
        for(size_t i=0;i<lhs.dependencyCount;i++)
        {
            if( lhs.pDependencies[i] != rhs.pDependencies[i] )
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }

    if( lhs.attachmentCount == rhs.attachmentCount)
    {
        for(size_t i=0;i<lhs.attachmentCount;i++)
        {
            if( lhs.pAttachments[i] != rhs.pAttachments[i] )
            {
                return false;
            }
        }
    }
    else
    {
        return false;
    }
  return
        // ( lhs.pNext == rhs.pNext )
         ( lhs.flags == rhs.flags )
      && ( lhs.attachmentCount == rhs.attachmentCount )
      //&& ( lhs.pAttachments == rhs.pAttachments )
      && ( lhs.subpassCount == rhs.subpassCount )
      //&& ( lhs.pSubpasses == rhs.pSubpasses )
      && ( lhs.dependencyCount == rhs.dependencyCount )
      //&& ( lhs.pDependencies == rhs.pDependencies )
          ;
}

vk::RenderPass System::createRenderPass(vka::RenderPassCreateInfo2 const & createInfo)
{
    auto & container = GLOBAL.renderPassContainer;

    if( container.exists(createInfo) )
    {
        auto x = container.at( createInfo );
        VKA_DEBUG("RenderPass with that structure already exists. Returning: {}", static_cast<void*>(x));
        return x;
    }
    else
    {
        auto info2 = createInfo;
        auto i = info2.create();

        //auto handle = GLOBAL.device.createRenderPass(i);
        auto handle = vka::System::get().m_Vulkan.device.createRenderPass(i);
        container.insert( handle, RenderPassInfo(), info2);

        auto & I      = container.info( handle );
        I.createInfo2 = createInfo;
        I.hash        = container.hash(handle);

        return handle;
    }
}

#if 0
vk::RenderPass System::createRenderPass(vk::RenderPassCreateInfo const & CI)
{
    for(auto & r : GLOBAL.renderPasses)
    {
        if( isEqual(r.second.createInfo, CI) )
        {
            VKA_DEBUG("RenderPass with that CreateInfo already exists. Returning that one instead");
            return r.first;
        }
    }

    auto rp = vka::System::get().m_Vulkan.device.createRenderPass(CI);

    auto &info = GLOBAL.renderPasses[rp];

    for(size_t i=0;i<CI.subpassCount;i++)
    {
        info.subPasses.push_back(CI.pSubpasses[i]);
    }

    for(size_t i=0;i<CI.attachmentCount;i++)
    {
        info.attachments.push_back(CI.pAttachments[i]);
    }

    for(size_t i=0;i<CI.dependencyCount;i++)
    {
        info.dependencies.push_back(CI.pDependencies[i]);
    }

    info.createInfo = CI;

    info.createInfo.pSubpasses    = info.subPasses.data();
    info.createInfo.pAttachments  = info.attachments.data();
    info.createInfo.pDependencies = info.dependencies.data();

    return rp;
}
#endif

static uint32_t findMemoryType(vk::PhysicalDevice physicalDevice, uint32_t typeFilter, vk::MemoryPropertyFlags properties)
{
    auto memProperites        = physicalDevice.getMemoryProperties();

    for (uint32_t i = 0; i < memProperites.memoryTypeCount ; i++)
    {
        const vk::MemoryPropertyFlags MemPropFlags = static_cast<vk::MemoryPropertyFlags>(memProperites.memoryTypes[i].propertyFlags);
        #define GET_BIT(a, j) (a & (1u << j))

        if ( GET_BIT(typeFilter,i) && ( MemPropFlags & properties) == properties)
        {
            return i;
        }
    }
    throw std::runtime_error("failed to find suitable memory type!");
}


vk::DeviceMemory System::allocateMemoryForImage(vk::Image img, vk::MemoryPropertyFlags properties)
{
    vk::MemoryAllocateInfo  memInfo;
    vk::MemoryRequirements  memRequirements = device().getImageMemoryRequirements(img);

    memInfo.allocationSize  = memRequirements.size;
    memInfo.memoryTypeIndex = findMemoryType( physicalDevice(),
                                              memRequirements.memoryTypeBits,
                                              properties);

   auto memory = allocateMemory(memInfo);
   return memory;
}

vk::DeviceMemory System::allocateMemoryForBuffer( vk::Buffer buffer, vk::MemoryPropertyFlags properties )
{

    auto bufferMemoryRequirements = vka::System::get().m_Vulkan.device.getBufferMemoryRequirements(buffer);

    vk::MemoryAllocateInfo info;

    info.allocationSize  = bufferMemoryRequirements.size;

    info.memoryTypeIndex = findMemoryType(GLOBAL.system->m_Vulkan.physicalDevice,
                                          bufferMemoryRequirements.memoryTypeBits,
                                          properties);

    auto mem = allocateMemory(info);

    return mem;
}

void System::bindBufferMemory(vk::Buffer buffer , vk::DeviceMemory memory, vk::DeviceSize offset)
{
    auto & info  = GLOBAL.buffersContainer.info(buffer);
    auto & minfo = GLOBAL.memoryContainer.info(memory);


    if( info.memory ) // this buffer is already bound to another memory
    {
        // make sure we erase the buffer from that memory's set of bound buffers
        GLOBAL.memoryContainer.info( info.memory).boundBuffers.erase( buffer );

        // and clear our current info
        info.memory = vk::DeviceMemory();
        info.offset = 0;
    }

    vka::System::get().m_Vulkan.device.bindBufferMemory(buffer, memory, offset);

    info.memory = memory;
    info.offset = offset;
    minfo.boundBuffers.insert(buffer);

}

void System::bindImageMemory(vk::Image image, vk::DeviceMemory memory, vk::DeviceSize offset)
{
    auto & info  = GLOBAL.imagesContainer.info(image);
    auto & minfo = GLOBAL.memoryContainer.info(memory);


    if( info.memory ) // this buffer is already bound to another memory
    {
        // make sure we erase the buffer from that memory's set of bound buffers
        GLOBAL.memoryContainer.info( info.memory).boundImages.erase( image );

        // and clear our current info
        info.memory = vk::DeviceMemory();
        info.offset = 0;
    }

    vka::System::get().m_Vulkan.device.bindImageMemory(image, memory, offset);

    info.memory = memory;
    info.offset = offset;

    minfo.boundImages.insert(image);

}

vk::DeviceMemory System::allocateMemory( vk::MemoryAllocateInfo const & info)
{
    auto mem = vka::System::get().m_Vulkan.device.allocateMemory(info);

    vka::DeviceMemoryInfo memInfo;
    memInfo.createInfo = info;

    if( !mem)
    {
        throw std::runtime_error("Error allocating memory");
    }

    VKA_DEBUG("Memory Allocated: {} bytes,  {} Mb", info.allocationSize, static_cast<float>(info.allocationSize)/(1024.f*1024.f));
    GLOBAL.memoryContainer.insert( mem, memInfo);
    return mem;
}

vk::Buffer System::createBuffer( vk::BufferCreateInfo const & info)
{
    auto buff = vka::System::get().m_Vulkan.device.createBuffer(info);

    vka::BufferInfo buffInfo;
    buffInfo.createInfo = info;

    if( !buff)
    {
        throw std::runtime_error("Error creating buffer");
    }

    GLOBAL.buffersContainer.insert(buff, buffInfo);
    return buff;
}


BufferPool System::createBufferPool(vk::BufferCreateInfo const & info, vk::MemoryPropertyFlags memoryProperties )
{
    BufferPool p;

    p.m_buffer = createBuffer(info);



    auto mem = allocateMemoryForBuffer(p.m_buffer, memoryProperties);

    bindBufferMemory(p.m_buffer, mem, 0);

    auto & pInfo = GLOBAL.bufferPools[p.m_buffer];
    pInfo.memory = mem;
    pInfo.memoryRequirements = vka::System::get().m_Vulkan.device.getBufferMemoryRequirements(p.m_buffer);

    pInfo.allocator.setSize( GLOBAL.memoryContainer.info(mem).createInfo.allocationSize );

    return p;
}

vk::ImageView System::createImageView(vk::ImageViewCreateInfo const & I)
{
    auto i = vka::System::get().m_Vulkan.device.createImageView(I);

    if(i)
    {
        auto parentImage = I.image;

        GLOBAL.imageViewsContainer.insert(i, ImageViewInfo());

        auto & info = GLOBAL.imageViewsContainer.info(i);

        if( !GLOBAL.imagesContainer.exists(parentImage) )
        {
            VKA_DEBUG("The parentImage was not registered, meaning it was created by the swapchain");
            // The parentImage was not registered, meaning it
            // was created by the swapchain and not by System::createImage( )
        }
        else
        {
            auto & parentImageInfo = GLOBAL.imagesContainer.info(parentImage);
            parentImageInfo.imageViews.insert(i);
        }


        info.createInfo = I;
    }

    return i;
}

vk::Image System::createImage(vk::ImageCreateInfo const & I)
{
    auto i = vka::System::get().m_Vulkan.device.createImage(I);

    if(i)
    {
        GLOBAL.imagesContainer.insert(i, ImageInfo());

        auto & info = GLOBAL.imagesContainer.info(i);

        info.createInfo = I;

        //----------------------------
        std::vector<vk::ImageLayout> layouts( I.mipLevels, I.initialLayout);
        info.m_layouts.clear();
        info.m_layouts.insert( info.m_layouts.begin(), I.arrayLayers, layouts);
        //----------------------------

    }

    return i;
}

vk::Sampler System::getDefaultSampler(vk::ImageView i)
{
    static_assert( sizeof(vka::SamplerCreateInfo2) == 80, "" );

    auto & i_info = GLOBAL.imageViewsContainer.info(i);

    //auto mipLevels = GLOBAL.images.at(i_info.createInfo.image).createInfo.mipLevels;

    auto mipLevels = i_info.createInfo.subresourceRange.levelCount;

    vk::SamplerCreateInfo ci;
    ci.magFilter               =  vk::Filter::eLinear ;
    ci.minFilter               =  vk::Filter::eLinear ;
    ci.mipmapMode              =  vk::SamplerMipmapMode::eLinear ;
    ci.addressModeU            =  vk::SamplerAddressMode::eRepeat ;
    ci.addressModeV            =  vk::SamplerAddressMode::eRepeat ;
    ci.addressModeW            =  vk::SamplerAddressMode::eRepeat ;
    ci.mipLodBias              =  0.0f  ;
    ci.anisotropyEnable        =  VK_FALSE;
    ci.maxAnisotropy           =  1 ;
    ci.compareEnable           =  VK_FALSE ;
    ci.compareOp               =  vk::CompareOp::eAlways ;
    ci.minLod                  =  0 ;
    ci.maxLod                  =  static_cast<float>(mipLevels) ;
    ci.borderColor             =  vk::BorderColor::eIntOpaqueBlack ;
    ci.unnormalizedCoordinates =  VK_FALSE ;

    return createSampler( vka::SamplerCreateInfo2(ci) );
}

void System::destroySampler(vk::Sampler s)
{
    if( GLOBAL.samplersContainer.exists(s))
    {
        auto & info = GLOBAL.samplersContainer.info(s);

        vka::System::get().m_Vulkan.device.destroySampler(s);

        VKA_DEBUG("Sampler Destroyed: {}  hash: {:x}", static_cast<void*>(s), info.hash);
        GLOBAL.samplersContainer.erase(s);
    }

}

vk::Sampler System::createSampler(vka::SamplerCreateInfo2 const & I)
{
    vka::SamplerCreateInfo2    const &    SamplerInfo = I;

    if( GLOBAL.samplersContainer.exists(I) )
    {
        return GLOBAL.samplersContainer.at(I);
    }
    static_assert( sizeof(vk::SamplerCreateInfo) == 80, "" );


    auto s = vka::System::get().m_Vulkan.device.createSampler(I);

    if(!s)
    {
        throw std::runtime_error("Error creating sampler");
    }

    vka::SamplerInfo info;
    info.createInfo = I;

    GLOBAL.samplersContainer.insert(s, info, SamplerInfo);

    GLOBAL.samplersContainer.info(s).hash = GLOBAL.samplersContainer.hash(s);
    return s;
}

void System::destroyTexturePool(TexturePool & p)
{
    // Make sure we free all the images/imageViews
    p.destroyAll();
    {
        auto & I = p.m_info;//GLOBAL.texturePools.at(p.m_image);

        // destroy the image
        destroyImage( p.m_image );

        // free the memory
        freeMemory( I.memory );

        p.m_image = vk::Image();
    }
}

TexturePool System::createTexturePool(vk::DeviceSize size,
                                      vk::Format format,
                                      vk::MemoryPropertyFlags properties,
                                      vk::ImageUsageFlags usage)
{
    vk::MemoryAllocateInfo  memInfo;
    vk::ImageCreateInfo create_info;

    create_info.imageType     = vk::ImageType::e2D;// VK_IMAGE_TYPE_2D;
    create_info.extent.width  = 1024;
    create_info.extent.height = 1024;
    create_info.extent.depth  = 1;
    create_info.mipLevels     = 9;
    create_info.arrayLayers   = 10;
    create_info.format        = format;
    create_info.tiling        = vk::ImageTiling::eOptimal;
    create_info.initialLayout = vk::ImageLayout::eUndefined;
    create_info.usage         = usage;//vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled;// VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    create_info.samples       = vk::SampleCountFlagBits::e1; // VK_SAMPLE_COUNT_1_BIT;
    create_info.sharingMode   = vk::SharingMode::eExclusive; // VK_SHARING_MODE_EXCLUSIVE;



    auto image = createImage(create_info);//m_Vulkan.device.createImage( create_info );

    if(!image)
        throw std::runtime_error("Failed to create image for textureMemoryPool");
    //=================

    vk::MemoryRequirements memRequirements = vka::System::get().m_Vulkan.device.getImageMemoryRequirements(image);

    size = std::max( memRequirements.size, size);

    auto alignment = memRequirements.alignment;

    // When we are allocating, make sure our total size is always a multiple
    // of the alignment.
    size = size%alignment==0 ? size : ((size/alignment + 1)*alignment);

    memInfo.allocationSize = size;


    memInfo.memoryTypeIndex = findMemoryType(m_Vulkan.physicalDevice,
                                             memRequirements.memoryTypeBits,
                                             properties);


    auto m_memory = allocateMemory(memInfo);

    // Bind the image to the memory so that we
    // always keep a reference to the memory.
    bindImageMemory( image, m_memory, 0);

    memRequirements.size = memInfo.allocationSize;

    TexturePool TP;

    auto & info = TP.m_info;//GLOBAL.texturePools[image];
    info.memory             = m_memory;
    info.memoryRequirements = memRequirements;
    info.allocator.setSize( memRequirements.size );

    TP.m_image = image;



//    BMP.m_Image = image;
//    BMP.m_info->m_allocator.setSize( memInfo.allocationSize );
//    BMP.m_info->m_MemRequirements = memRequirements;
//    BMP.m_info->m_Device = m_Device;
//    BMP.m_info->m_Memory = m_memory;
//    BMP.m_info->m_Info   = create_info;
//    BMP.m_info->m_Alignment = static_cast<uint32_t>(memRequirements.alignment);
//    BMP.m_info->m_size = memInfo.allocationSize;
//
//    m_TexturePools.push_back(BMP);
    return TP;
}

void System::destroyDescriptorSet(vk::DescriptorSet p)
{
    if( GLOBAL.descriptorSetsContainer.exists(p) )
    {
        auto P = p;
        {
            auto & info = GLOBAL.descriptorSetsContainer.info(p);

            vka::System::get().m_Vulkan.device.freeDescriptorSets( info.pool, 1, &p);

            // unlink the descriptor set from it's layout
            GLOBAL.descriptorSetLayoutsContainer.info( info.layout ).descriptorSets.erase(P);
            // unlink the descriptor set from its pool
            GLOBAL.descriptorPoolsContainer.info(info.pool).sets.erase( P );
        }

        // erase the pipeline info
        GLOBAL.descriptorSetsContainer.erase( P );

        VKA_DEBUG("DescriptorSet Freed: {}", static_cast<void*>(P));
    }
}

void System::destroyDescriptorPool(vk::DescriptorPool p)
{
    if( GLOBAL.descriptorPoolsContainer.exists(p)  )
    {
        auto P = p;
        {
            auto & info = GLOBAL.descriptorPoolsContainer.info(p);

            if( info.sets.size() )
            {
                throw std::runtime_error("Cannot Destroy Descriptor Pool. There are still DescriptorSets allocated");
            }

            vka::System::get().m_Vulkan.device.destroyDescriptorPool( p );
        }

        // erase the pipeline info
        GLOBAL.descriptorPoolsContainer.erase( P );

        VKA_DEBUG("DescriptorPool destroyed: {}", static_cast<void*>(p));
    }
}


void System::destroyPipeline(vk::Pipeline p , bool deleteShaderModules)
{
    std::vector<vk::ShaderModule> modules;

    if( GLOBAL.pipelinesContainer.exists(p) == 1)
    {
        {
            auto & info = GLOBAL.pipelinesContainer.info(p);

            // destroy the pipeline
            vka::System::get().m_Vulkan.device.destroyPipeline( p );

            // disconnect the pipeline from all its shaders
            for(auto & stage: info.createInfo.stages)
            {
                if(stage.module)
                {
                    GLOBAL.shaderModulesContainer.info(stage.module).pipelines.erase(p);
                }
                if(deleteShaderModules)
                    modules.push_back(stage.module);
            }

            if( info.createInfo.renderPass)
            {
                if( GLOBAL.renderPassContainer.exists(info.createInfo.renderPass))
                {
                    GLOBAL.renderPassContainer.info(info.createInfo.renderPass).pipelines.erase(p);
                }
            }

            if( info.createInfo.layout)
                GLOBAL.pipelineLayoutsContainer.info(info.createInfo.layout).pipelines.erase(p);
        }

        // erase the pipeline info
        GLOBAL.pipelinesContainer.erase( p );

        VKA_DEBUG("Pipeline destroyed: {}", static_cast<void*>(p));

        for(auto & m : modules)
            destroyShaderModule(m);
    }
}

void System::destroyShaderModule(vk::ShaderModule p )
{
    auto & container = GLOBAL.shaderModulesContainer;
    auto & info      = container.info(p);//.at(l);

    if(info.pipelines.size() )
    {
        VKA_WARN("This shader module is currently being used by another pipeline. Cannot destroy");
        return;
    }

    // destroy the actual shader module.
    vka::System::get().m_Vulkan.device.destroyShaderModule(p);


    // erase the object from the container
    container.erase(p);

    VKA_DEBUG("Shader Module Destroyed: {}", static_cast<void*>(p) );
}

void       System::destroyBufferPool(BufferPool const & p )
{
    if( GLOBAL.bufferPools.count(p.m_buffer) == 1)
    {
        auto mem = GLOBAL.bufferPools.at(p.m_buffer).memory;

        destroyBuffer(p.m_buffer);
        freeMemory(mem);

        GLOBAL.bufferPools.erase(p.m_buffer);

        VKA_DEBUG("BufferPool destroyed: {}", static_cast<void*>(p.m_buffer));
    }
}

BufferPool System::createBufferPool(vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags memoryProperties)
{
    vk::BufferCreateInfo binfo;
    binfo.setSize(size);
    binfo.setUsage( usage);
    return createBufferPool(binfo, memoryProperties);
}

void* System::mapMemory(vk::DeviceMemory mem, vk::DeviceSize offset)
{
    auto & mInfo = GLOBAL.memoryContainer.info(mem);

    if( mInfo.map )
    {
        return static_cast<void*>( static_cast<unsigned char*>(mInfo.map) + offset );
    }
    else
    {
        // if it hasn't already been mapped, map the entire
        // memory space from the start and set the mInfo.map
        auto size = mInfo.createInfo.allocationSize;

        mInfo.map = vka::System::get().m_Vulkan.device.mapMemory( mem, 0, size);

        return static_cast<void*>( static_cast<unsigned char*>(mInfo.map) + offset );
    }
}

void  System::unmapMemory(vk::DeviceMemory mem)
{
    auto & mInfo = GLOBAL.memoryContainer.info(mem);

    if( mInfo.map )
    {
        vka::System::get().m_Vulkan.device.unmapMemory(mem);
        mInfo.map = nullptr;
    }
}



template<typename T>
struct vectorHash_t
{
    std::size_t operator()(std::vector<T> const& s) const noexcept
    {
        std::hash<T> intHash;

        std::size_t h1 = intHash(s.front());

        for(auto & h2 : s)
        {
            h1 = h1 ^ (h2 << 1); // or use boost::hash_combine (see Discussion)
        }
        return h1;
    }
};

vk::ShaderModule System::createShaderModule(const vka::ShaderModuleCreateInfo2 & createInfo)
{
    auto & container = GLOBAL.shaderModulesContainer;

    if( container.exists(createInfo) )
    {
        auto x = container.at( createInfo );
        VKA_DEBUG("Shader with that source code. Returning: {}", static_cast<void*>(x));
        return x;
    }
    else
    {
        ShaderModuleCreateInfo2 info2 = createInfo;
        auto i = info2.create();

        auto handle = vka::System::get().m_Vulkan.device.createShaderModule(i);

        container.insert( handle, ShaderModuleInfo(), info2);

        auto & I     = container.info( handle );
        I.createInfo = createInfo;
        I.hash       = container.hash(handle);
        I.reflection.parseSPIRV( createInfo.code);
        return handle;
    }
}


std::vector<uint32_t> _from_file(const std::string & path)
{
    std::ifstream file(path, std::ios::ate | std::ios::binary);

    if ( !file.is_open() )
    {
        throw std::runtime_error("Failed to open file: " + path);
    }

    auto fileSize = file.tellg(); // in bytes
    std::string buffer;
    buffer.resize( static_cast<size_t>(fileSize) );

    file.seekg(0);
    file.read(&buffer[0], fileSize);
    file.close();

    const auto fs = static_cast<size_t>(fileSize);
    auto word_size = fs/sizeof(uint32_t) + (fs % sizeof(uint32_t) == 0 ? 0u : 1u);

    std::vector<uint32_t> m_data;
    m_data.resize(  word_size  );

    memcpy( &m_data[0], &buffer[0], static_cast<size_t>(fileSize) );
    //m_type = t;

    return m_data;
}


std::vector<uint32_t> System::compileGLSL(std::string const & path)
{
    auto unique = std::chrono::system_clock::now().time_since_epoch().count();

    std::string opath = "/tmp/sprv.spv." + std::to_string(unique);

    std::string cmd = std::string("glslangValidator -V ") + path + " -o " + opath;

    VKA_DEBUG("Compiling Shader {} ", path);

    if( std::system( cmd.c_str() ) == 0)
    {
        return _from_file(opath);
    }
    else
    {
        throw std::runtime_error("Failed to compile");
    }
}

vk::ShaderModule System::createShaderModuleFromGLSL(std::string const & path)
{
    vka::ShaderModuleCreateInfo2 ci;
    ci.code = compileGLSL(path);

    return createShaderModule(ci);

}

vk::PipelineLayout System::createPipelineLayout(const PipelineLayoutCreateInfo2 &info)
{
    if( GLOBAL.pipelineLayoutsContainer.exists(info))
    {
        auto x = GLOBAL.pipelineLayoutsContainer.at( info );
        VKA_DEBUG("PipelineLayout already exists. Returning: {}", static_cast<void*>(x));
        return x;
    }
    else
    {
        PipelineLayoutCreateInfo2 info2 = info;
        auto i = info2.create();

        auto layout = vka::System::get().m_Vulkan.device.createPipelineLayout(i);

        GLOBAL.pipelineLayoutsContainer.insert( layout, PipelineLayoutInfo(), info2);
        auto & I     = GLOBAL.pipelineLayoutsContainer.info( layout );
        I.createInfo = info;
        I.hash       = GLOBAL.pipelineLayoutsContainer.hash(layout);

        return layout;
    }

}

void System::destroyPipelineLayout(vk::PipelineLayout layout)
{
    auto & info = GLOBAL.pipelineLayoutsContainer.info(layout);

    if( info.pipelines.size() )
    {
        VKA_WARN("Cannot destroy this pipeline layout, it is being used by another pipeline");
        return;
    }

    vka::System::get().m_Vulkan.device.destroyPipelineLayout(layout);


    for(auto s : info.createInfo.SetLayouts )
    {
        GLOBAL.descriptorSetLayoutsContainer.info(s).pipelineLayouts.erase(layout);
    }

    GLOBAL.pipelineLayoutsContainer.erase(layout);
}



std::vector<vk::VertexInputAttributeDescription> System::createPipelineVertexAttributeDescriptionFromShader( vk::ShaderModule vertexShader) const
{
    std::vector<vk::VertexInputAttributeDescription> desc;

    auto & v = GLOBAL.info(vertexShader).reflection;

    for(auto & in : v.inputs)
    {
        desc.push_back( vk::VertexInputAttributeDescription(in.location, in.location, in.format));
    }
    return desc;
}

/**
 * @brief PipelineLayoutCreateInfo3::createLayout
 * @return
 *
 * Create the pipeline layout from the shader's reflection.
 *
 * That is, create all descriptor set layouts
 */
vk::PipelineLayout System::createPipelineLayoutFromShaders(vk::ShaderModule vertexShader, vk::ShaderModule fragmentShader)
{
    auto & v = GLOBAL.info(vertexShader).reflection;
    auto & f = GLOBAL.info(fragmentShader).reflection;

    std::map<uint32_t, vka::DescriptorSetLayoutCreateInfo2 > setLayouts;
    std::vector< vk::DescriptorSetLayout> layouts;
    std::vector<vk::PushConstantRange> pushConstantRanges;

//    ds.bindings
    {
        vk::ShaderStageFlagBits stage = vk::ShaderStageFlagBits::eVertex;
        auto & stageDescriptors = v.descriptors;
        auto & stagePushConsts  = v.pushConstants;

        for(auto & d : stageDescriptors)
        {
            auto & descriptor = d.second;

            auto & layoutCreate = setLayouts[ descriptor.set ];

            //uint32_t count=1;
            //if( d.second.arraySize >= 1 )
            //    count = static_cast<uint32_t>(d.second.arraySize);

            layoutCreate.addDescriptor( descriptor.binding, descriptor.type, std::max<uint32_t>(1u, static_cast<uint32_t>(descriptor.arraySize) ), stage);
        }
        if( stagePushConsts.size )
        {
            pushConstantRanges.resize(1);
            pushConstantRanges.back().size        = static_cast<uint32_t>(stagePushConsts.size);
            pushConstantRanges.back().stageFlags |= stage;
        }
    }


    //==============================================================================================
    {
        vk::ShaderStageFlagBits stage = vk::ShaderStageFlagBits::eFragment;
        auto & stageDescriptors = f.descriptors;
        auto & stagePushConsts  = f.pushConstants;

        for(auto & d : stageDescriptors)
        {
            auto & descriptor = d.second;

            auto & layoutCreate = setLayouts[ descriptor.set ];

            //uint32_t count=1;
            //if( d.second.arraySize >= 1 )
            //    count = static_cast<uint32_t>(d.second.arraySize);

            layoutCreate.addDescriptor( descriptor.binding, descriptor.type, std::max<uint32_t>(1u, static_cast<uint32_t>(descriptor.arraySize) ), stage);
        }
        if( stagePushConsts.size )
        {
            pushConstantRanges.resize(1);
            pushConstantRanges.back().size        = static_cast<uint32_t>(stagePushConsts.size);
            pushConstantRanges.back().stageFlags |= stage;
        }
    }



    for(auto & x : setLayouts)
    {
        VKA_DEBUG("Creating Descriptor Set {}", x.first);
        layouts.push_back( createDescriptorSetLayout( x.second ) );
    }

    vka::PipelineLayoutCreateInfo2 ci;
    ci.SetLayouts         = layouts;
    ci.PushConstantRanges = pushConstantRanges;

    return createPipelineLayout(ci);

}


//vk::Pipeline System::createPipeline(vk::GraphicsPipelineCreateInfo const & info)
//{
//    auto p = vka::System::get().m_Vulkan.device.createGraphicsPipeline(vk::PipelineCache(), info);

//    if( !p )
//    {
//        throw std::runtime_error("Failed at creating pipeline");
//    }

//    auto & pInfo = GLOBAL.pipelines[p];

//    for(size_t i=0; i < info.stageCount; i++)
//    {
//        if( info.pStages[i].stage == vk::ShaderStageFlagBits::eVertex)
//        {
//            pInfo.vertexShader = info.pStages[i].module;
//        }
//        if( info.pStages[i].stage == vk::ShaderStageFlagBits::eFragment)
//        {
//            pInfo.fragmentShader = info.pStages[i].module;
//        }
//    }

//    pInfo.layout = info.layout;
//    pInfo.renderPass = info.renderPass;

//    if( GLOBAL.renderPasses.count(pInfo.renderPass) )
//    {
//        // only add the pipeline to the renderpass if it exists.
//        GLOBAL.renderPasses.at(pInfo.renderPass).pipelines.insert( p );
//    }
//    GLOBAL.pipelineLayoutsContainer.info(pInfo.layout).pipelines.insert(p);

//    return p;
//}


vk::RenderPass System::_createRenderPassFromFragmentOutputs( std::vector<VertexInput_t> const & output, vk::Format depth_format)
{

    vka::RenderPassCreateInfo2 createInfo;

    for(auto & v : output)
    {
        createInfo.addColorAttachment( v.format, vk::ImageLayout::eShaderReadOnlyOptimal);
    }
    if( depth_format != vk::Format::eUndefined )
    {
        createInfo.addDepthStenchilAttachment( depth_format, vk::ImageLayout::eShaderReadOnlyOptimal);
    }

    auto rp = GLOBAL.system->createRenderPass(createInfo);
    return rp;
}

#if 0
vk::Pipeline System::createPipeline(vka::GraphicsPipelineCreateInfo3 const & info)
{
    vka::GraphicsPipelineCreateInfo2 C;

    C.stages.push_back( {vk::ShaderStageFlagBits::eVertex,   info.vertexShader  , "main"} );
    C.stages.push_back( {vk::ShaderStageFlagBits::eFragment, info.fragmentShader, "main"} );


    C.inputAssemblyState.topology = info.topology;

    {

        C.rasterizationState.cullMode                = info.cullMode;
        C.rasterizationState.frontFace               = info.frontFace;
        C.rasterizationState.polygonMode             = info.polygonMode;

        VKA_DEBUG("CullMode                = {}"  , to_string(C.rasterizationState.cullMode)   );
        VKA_DEBUG("FrontFace               = {}"  , to_string(C.rasterizationState.frontFace)  );
        VKA_DEBUG("PolygonMode             = {}", to_string(C.rasterizationState.polygonMode)  );
        VKA_DEBUG("depthClampEnable        = {}",  C.rasterizationState.depthClampEnable       );
        VKA_DEBUG("rasterizerDiscardEnable = {}",  C.rasterizationState.rasterizerDiscardEnable);
        VKA_DEBUG("depthBiasEnable         = {}",  C.rasterizationState.depthBiasEnable        );
        VKA_DEBUG("depthBiasConstantFactor = {}",  C.rasterizationState.depthBiasConstantFactor);
        VKA_DEBUG("depthBiasClamp          = {}",  C.rasterizationState.depthBiasClamp         );
        VKA_DEBUG("depthBiasSlopeFactor    = {}",  C.rasterizationState.depthBiasSlopeFactor   );
        VKA_DEBUG("lineWidth               = {}",  C.rasterizationState.lineWidth              );

        // C.rasterizationState.depthClampEnable        = false;
        // C.rasterizationState.rasterizerDiscardEnable = true;
        // C.rasterizationState.depthBiasEnable         = false;
        // C.rasterizationState.depthBiasConstantFactor = 0.0f;
        // C.rasterizationState.depthBiasClamp          = 0.0f;
        // C.rasterizationState.depthBiasSlopeFactor    = 0.0f;
        // C.rasterizationState.lineWidth               = 1.0f;
    }

    C.layout = createPipelineLayoutFromShaders( info.vertexShader, info.fragmentShader);


    // we're rendering to the screen, so make sure we find
    // a renderpass that has only 1 attachment whose finalLayout is PresentKHR
    if( !info.renderToTexture )
    {
        if( info.renderPass )
        {
            C.renderPass = *info.renderPass;
            if( info.enableDepthTest)
            {
                C.depthStencilState.depthTestEnable = true;
                C.depthStencilState.depthWriteEnable = true;
            }
        }
        else
        {
            // no renderpass was given, so attempt to find a renderpas that might work
            if( info.enableDepthTest )
            {
                for(auto & r : GLOBAL.renderPassContainer)
                {
                    if( r.second.attachments.size() == 2 && r.second.attachments[0].finalLayout == vk::ImageLayout::ePresentSrcKHR
                                                         && r.second.attachments[1].finalLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
                    {
                        VKA_DEBUG("Found a viable render pass that can present to screen and has a depth texture");
                        C.renderPass = r.first;
                        C.depthStencilState.depthTestEnable = true;
                        C.depthStencilState.depthWriteEnable = true;
                        break;
                    }
                }
                if( !C.renderPass )
                {
                    throw std::runtime_error("Could not find a proper renderPass for this pipeline");
                }
            }
            else
            {

                for(auto & r : GLOBAL.renderPassContainer)
                {
                    if( r.second.attachments.size() == 1 && r.second.attachments[0].finalLayout == vk::ImageLayout::ePresentSrcKHR)
                    {
                        VKA_DEBUG("Found a viable render pass that can present to screen");
                        C.renderPass = r.first;
                        break;
                    }
                }
                if( !C.renderPass )
                {
                    throw std::runtime_error("Could not find a proper renderPass for this pipeline.");
                }
            }
        }
    }
    else  // we are rendering to a Texture so we need to find proper color attachments
    {
        //throw std::runtime_error("This is not supported yet.");

        auto & fR = GLOBAL.shaderModulesContainer.info( info.fragmentShader ).reflection;

        if( fR.outputs.size() != info.colorOutputs.size() )
        {
            throw std::runtime_error("The Fragment shader outputs does not match the colorOutput in the create info");
        }

        auto out = fR.outputs;
        uint32_t i=0;
        for(auto & o : out)
        {
            o.format = info.colorOutputs[i++];
        }

        auto depthFormat = vk::Format::eUndefined;
        if( info.enableDepthTest )
        {
            depthFormat = vk::Format::eD32Sfloat;
            C.depthStencilState.depthTestEnable  = true;
            C.depthStencilState.depthWriteEnable = true;
        }

        auto renderPass = _createRenderPassFromFragmentOutputs( out , depthFormat);

        C.renderPass    = renderPass;


        for(auto fO : fR.outputs)
        {
            vk::PipelineColorBlendAttachmentState Cb;
            Cb.colorWriteMask      = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
            Cb.blendEnable         = info.enableBlending;
            Cb.colorBlendOp        = vk::BlendOp::eAdd;
            Cb.srcAlphaBlendFactor = vk::BlendFactor::eSrcAlpha;
            Cb.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
            Cb.dstAlphaBlendFactor = vk::BlendFactor::eOneMinusDstAlpha;
            Cb.dstColorBlendFactor = vk::BlendFactor::eOneMinusDstAlpha;

            C.colorBlendState.attachments.push_back(Cb);
        }


    }

    C.scissors.push_back(  info.scissor  );
    C.viewports.push_back( info.viewport );
    C.dynamicStates.push_back( vk::DynamicState::eScissor );
    C.dynamicStates.push_back( vk::DynamicState::eViewport );

    //================================================================

    auto & vR = GLOBAL.shaderModulesContainer.info( info.vertexShader ).reflection;

    if( !info.interleavedAttributes )
    {
        uint32_t i=0;
        for(auto & vI : vR.inputs)
        {
            auto format = i < info.vertexInputFormats.size() ? info.vertexInputFormats[i] : vI.format;

            uint32_t stride = formatSize( format );


            if( formatComponents(format) != formatComponents(vI.format) )
            {
                VKA_CRIT("The Requested Input vertex format, {}, has different number of components as the input format listed in the Shader: {}", to_string(format), to_string(vI.format));
            }

            C.vertexAttributeDescriptions.push_back( vk::VertexInputAttributeDescription(vI.location,vI.location,format,0));
            C.vertexBindingDescriptions.push_back(   vk::VertexInputBindingDescription(vI.location,stride));

            i++;
        }
    }
    else
    {
        uint32_t i=0;
        uint32_t offset=0;
        uint32_t stride=0;
        for(auto & vI : vR.inputs)
        {
            auto format = i < info.vertexInputFormats.size() ? info.vertexInputFormats[i] : vI.format;

            stride += formatSize( format );
            i++;
        }
        i=0;
        for(auto & vI : vR.inputs)
        {

            uint32_t binding  = vI.location;
            uint32_t location = vI.location;
            auto format = i < info.vertexInputFormats.size() ? info.vertexInputFormats[i] : vI.format;

            if( formatComponents(format) != formatComponents(vI.format) )
            {
                VKA_CRIT("The Requested Input vertex format, {}, has different number of components as the input format listed in the Shader: {}", to_string(format), to_string(vI.format));
            }

            C.vertexAttributeDescriptions.push_back( vk::VertexInputAttributeDescription(location, binding, format,offset));
            C.vertexBindingDescriptions.push_back(   vk::VertexInputBindingDescription(binding, stride));
            i++;
            //INFO("PipelineConstruction3: {}  format: {}", vI.name, to_string(format) );
        }
    }
    //================================================================


    //================================================================
    // Fragment Outputs
    //================================================================

    //================================================================

    auto p = createGraphicsPipeline(C);

    return p;
}
#endif

vk::Pipeline System::createGraphicsPipeline(vka::GraphicsPipelineCreateInfo2 const & createInfo)
{
   auto C = createInfo;

   auto cc = C.create();

   auto p = vka::System::get().m_Vulkan.device.createGraphicsPipeline(vk::PipelineCache(), cc);

   // link the pipline with all stages
   for(auto & stage : C.stages)
   {
       GLOBAL.shaderModulesContainer.info( stage.module ).pipelines.insert(p);
   }

   GLOBAL.renderPassContainer.info( C.renderPass ).pipelines.insert(p);

   GLOBAL.pipelinesContainer.insert(p, PipelineInfo());
   auto & pInfo = GLOBAL.pipelinesContainer.info(p);
   pInfo.createInfo = C;

   return p;
}

vk::Pipeline System::createGraphicsPipeline(vka::GraphicsPipelineCreateInfo4 const & info)
{
    vka::GraphicsPipelineCreateInfo2 C;

    C.stages.push_back( {vk::ShaderStageFlagBits::eVertex,   info.vertexShader  , info.vertexShaderMain} );
    C.stages.push_back( {vk::ShaderStageFlagBits::eFragment, info.fragmentShader, info.fragmentShaderMain} );


    C.inputAssemblyState.topology = info.topology;

    {

        C.rasterizationState.cullMode                = info.cullMode;
        C.rasterizationState.frontFace               = info.frontFace;
        C.rasterizationState.polygonMode             = info.polygonMode;

        VKA_DEBUG("CullMode                = {}"  , to_string(C.rasterizationState.cullMode)   );
        VKA_DEBUG("FrontFace               = {}"  , to_string(C.rasterizationState.frontFace)  );
        VKA_DEBUG("PolygonMode             = {}", to_string(C.rasterizationState.polygonMode)  );
        VKA_DEBUG("depthClampEnable        = {}",  C.rasterizationState.depthClampEnable       );
        VKA_DEBUG("rasterizerDiscardEnable = {}",  C.rasterizationState.rasterizerDiscardEnable);
        VKA_DEBUG("depthBiasEnable         = {}",  C.rasterizationState.depthBiasEnable        );
        VKA_DEBUG("depthBiasConstantFactor = {}",  C.rasterizationState.depthBiasConstantFactor);
        VKA_DEBUG("depthBiasClamp          = {}",  C.rasterizationState.depthBiasClamp         );
        VKA_DEBUG("depthBiasSlopeFactor    = {}",  C.rasterizationState.depthBiasSlopeFactor   );
        VKA_DEBUG("lineWidth               = {}",  C.rasterizationState.lineWidth              );

        // C.rasterizationState.depthClampEnable        = false;
        // C.rasterizationState.rasterizerDiscardEnable = true;
        // C.rasterizationState.depthBiasEnable         = false;
        // C.rasterizationState.depthBiasConstantFactor = 0.0f;
        // C.rasterizationState.depthBiasClamp          = 0.0f;
        // C.rasterizationState.depthBiasSlopeFactor    = 0.0f;
         C.rasterizationState.lineWidth               = 3.0f;
    }


    //  handle blending
    {
        if( info.colorOutputs.size() == 0)
        {
            VKA_DEBUG("colorOutputs not set. Using a default one.");
            {
                vk::PipelineColorBlendAttachmentState blendstate;
                blendstate.colorWriteMask      = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
                blendstate.blendEnable         = true;
                blendstate.colorBlendOp        = vk::BlendOp::eAdd;
                blendstate.srcAlphaBlendFactor = vk::BlendFactor::eSrcAlpha;
                blendstate.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
                blendstate.dstAlphaBlendFactor = vk::BlendFactor::eOneMinusDstAlpha;
                blendstate.dstColorBlendFactor = vk::BlendFactor::eOneMinusDstAlpha;

                C.colorBlendState.attachments.push_back( blendstate );
            }
        }
        else
        {
            for(auto & output : info.colorOutputs)
            {
                vk::PipelineColorBlendAttachmentState blendstate;
                blendstate.colorWriteMask      = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
                blendstate.blendEnable         = output.enableBlending;
                blendstate.colorBlendOp        = vk::BlendOp::eAdd;
                blendstate.srcAlphaBlendFactor = vk::BlendFactor::eSrcAlpha;
                blendstate.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
                blendstate.dstAlphaBlendFactor = vk::BlendFactor::eOneMinusDstAlpha;
                blendstate.dstColorBlendFactor = vk::BlendFactor::eOneMinusDstAlpha;

                C.colorBlendState.attachments.push_back( blendstate );
            }
        }
    }

    C.layout = createPipelineLayoutFromShaders( info.vertexShader, info.fragmentShader);

    C.renderPass = info.renderPass;

    if( info.enableDepthTest )
    {
        // check if the renderpass has an appropriate depth attachment.
    }
    if( info.enableDepthTest)
    {
        C.depthStencilState.depthTestEnable  = info.enableDepthTest;//true;
        C.depthStencilState.depthWriteEnable = info.enableDepthWrite;//true;
    }

    vk::Rect2D            scissor;
    vk::Viewport          viewport;

    C.scissors.push_back(  scissor  );
    C.viewports.push_back( viewport );
    C.dynamicStates.push_back( vk::DynamicState::eScissor );
    C.dynamicStates.push_back( vk::DynamicState::eViewport );
    C.dynamicStates.push_back( vk::DynamicState::eLineWidth );
    C.dynamicStates.push_back( vk::DynamicState::eDepthBias );
    C.dynamicStates.push_back( vk::DynamicState::eDepthBounds );


    //================================================================

    auto & vR = GLOBAL.shaderModulesContainer.info( info.vertexShader ).reflection;

    if( vR.inputs.size() != info.vertexInputFormats.size() )
    {
        throw std::runtime_error("createInfo::vertexInputFormats.size() is different from the the inputs listed in the shader");
    }

    {
        uint32_t i=0;
        for(auto & vI : vR.inputs)
        {
            auto format = info.vertexInputFormats[i];

            uint32_t stride = static_cast<uint32_t>( formatSize( format ) );

            if( formatComponents(format) != formatComponents(vI.format) )
            {
                VKA_CRIT("The Requested Input vertex format, {}, has different number of components as the input format listed in the Shader: {}", to_string(format), to_string(vI.format));
            }

            C.vertexAttributeDescriptions.push_back( vk::VertexInputAttributeDescription(vI.location,vI.location,format,0));
            C.vertexBindingDescriptions.push_back(   vk::VertexInputBindingDescription(  vI.location,stride));

            i++;
        }
    }


    auto p = createGraphicsPipeline(C);

    return p;
}


vk::DescriptorPool System::createDescriptorPool(vka::DescriptorPoolCreateInfo2 const & I)
{
    auto CI = I.create();
    auto p = vka::System::get().m_Vulkan.device.createDescriptorPool(CI);

    if( !p )
    {
        throw std::runtime_error("Error creating descriptor pool");
    }

    vka::DescriptorPoolInfo info;
    info.createInfo = I;

    GLOBAL.descriptorPoolsContainer.insert(p,  info);

    return p;
}


vk::DescriptorSetLayout System::createDescriptorSetLayout(const DescriptorSetLayoutCreateInfo2 &I)
{
    // Has a layout with this CreateInfo struct already
    // been created?
    if( GLOBAL.descriptorSetLayoutsContainer.exists(I))
    {
        auto x = GLOBAL.descriptorSetLayoutsContainer.at(I);
        VKA_DEBUG("Layout already exists. Returning: {}", static_cast<void*>(x));
        return x;
    }
    else
    {
        auto i = I.create();
        auto device = vka::System::device();
        auto s = device.createDescriptorSetLayout( i );

        vka::DescriptorSetLayoutInfo info;
        info.createInfo = I;

        GLOBAL.descriptorSetLayoutsContainer.insert(s, info, I);

        GLOBAL.descriptorSetLayoutsContainer.info(s).hash = GLOBAL.descriptorSetLayoutsContainer.hash(s);

        VKA_DEBUG("Creating new Layout: {}", static_cast<void*>(s));
        return s;
    }

}

vk::DescriptorSetLayout System::createDescriptorSetLayout(const vk::DescriptorSetLayoutCreateInfo &I)
{
    vka::DescriptorSetLayoutCreateInfo2 i2;//(I);

    VKA_DEBUG("++++++++++++++++++++++++Creating Descriptor Set Layout ++++++++++++++++++++++++++++++++");
    for(uint32_t i=0;i<I.bindingCount;i++)
    {
        i2.addDescriptor( I.pBindings[i].binding, I.pBindings[i].descriptorType, I.pBindings[i].descriptorCount, I.pBindings[i].stageFlags);
        VKA_DEBUG("Binding:  {}" , I.pBindings[i].binding );
        VKA_DEBUG("Type:     {}" , to_string(I.pBindings[i].descriptorType) );
        VKA_DEBUG("Count:    {}" , I.pBindings[i].descriptorCount );
        VKA_DEBUG("Stages:   {}" , to_string(I.pBindings[i].stageFlags) );
    }
    if( I.flags & vk::DescriptorSetLayoutCreateFlagBits::ePushDescriptorKHR)
        i2.enablePushDescriptor(true);

    return createDescriptorSetLayout(i2);
}

void System::destroyDescriptorSetLayout(vk::DescriptorSetLayout l)
{
    auto & info = GLOBAL.descriptorSetLayoutsContainer.info(l);//.at(l);

    if(info.descriptorSets.size() )
    {
        VKA_WARN("This layout is currently being used by a descriptor set. Cannot destroy it");
        return;
    }
    if(info.pipelineLayouts.size() )
    {
        VKA_WARN("This DescriptorSetLayout is currently being used by a PipelineLayout. Cannot destroy it");
        return;
    }

    // destory the actual layout
    vka::System::get().m_Vulkan.device.destroyDescriptorSetLayout(l);

    // erase the object from the container
    GLOBAL.descriptorSetLayoutsContainer.erase(l);

    VKA_DEBUG("DescriptorSetLayout Destroyed: {}", static_cast<void*>(l) );

}

void System::updateDescriptorSets( vk::ArrayProxy<const vk::WriteDescriptorSet> x)
{
    vka::System::get().m_Vulkan.device.updateDescriptorSets(x, nullptr);
}

vk::DescriptorSet System::allocateDescriptorSet( vk::DescriptorPool pool,
                                                 vka::DescriptorSetLayoutCreateInfo2 const & layout)
{
    auto l1 = createDescriptorSetLayout(layout);

    return allocateDescriptorSet( pool, l1 );
}

vk::DescriptorSet  System::allocateDescriptorSet(vk::DescriptorPool pool, vk::DescriptorSetLayout layout)
{

    vk::DescriptorSetAllocateInfo allocInfo;

    allocInfo.setDescriptorPool(pool);
    allocInfo.setPSetLayouts(&layout);
    allocInfo.setDescriptorSetCount(1);

    vka::DescriptorSetInfo dInfo;
    dInfo.pool   = pool;
    dInfo.layout = layout;

    auto d = vka::System::get().m_Vulkan.device.allocateDescriptorSets( allocInfo );


    { // link the descirptor set with the layout so that we dont destroy the layout

        GLOBAL.descriptorSetLayoutsContainer.info(layout).descriptorSets.insert(d[0]);
        GLOBAL.descriptorPoolsContainer.info(pool).sets.insert(d[0]);
    }

    GLOBAL.descriptorSetsContainer.insert(d[0], dInfo);

    return d[0];
}


void  System::destroyRenderTarget(RenderTarget & I)
{
    destroyFramebuffer( I.m_frameBuffers );
    destroyRenderPass( I.m_renderPass );
}

RenderTarget System::createRenderTarget(vka::RenderTargetCreateInfo const & CI)
{
    #pragma message ("TODO: Replace this with CreateRenderPassInfo2")

    bool hasDepth = CI.depthTarget;
    uint32_t width  = std::numeric_limits<uint32_t>::max();
    uint32_t height = std::numeric_limits<uint32_t>::max();

    vka::RenderPassCreateInfo2 createInfo;
    for(auto & a : CI.colorTargets)
    {
        auto & vInfo = GLOBAL.imageViewsContainer.info(a);
        auto & iInfo = GLOBAL.imagesContainer.info(vInfo.createInfo.image);

        createInfo.addColorAttachment(vInfo.createInfo.format, vk::ImageLayout::eShaderReadOnlyOptimal);

        width   = std::min(width, iInfo.createInfo.extent.width);
        height  = std::min(width, iInfo.createInfo.extent.height);
    }

    if( hasDepth )
    {
        auto & vInfo = GLOBAL.imageViewsContainer.info( CI.depthTarget );
        createInfo.addDepthStenchilAttachment( vInfo.createInfo.format, vk::ImageLayout::eShaderReadOnlyOptimal);
    }



    auto renderPass =  createRenderPass(createInfo);

    RenderTarget rt;
    rt.m_renderPass = renderPass;
    {
        std::vector<vk::ImageView> attachments = CI.colorTargets;
        if(hasDepth)
        {
            attachments.push_back( CI.depthTarget );
        }

        vka::FramebufferCreateInfo2 fbufCreateInfo;// = {};

        fbufCreateInfo.renderPass      = renderPass;
        fbufCreateInfo.attachments     = attachments;
        fbufCreateInfo.width           = width;// offScreenFrameBuf.width;
        fbufCreateInfo.height          = height;//offScreenFrameBuf.height;
        fbufCreateInfo.layers          = 1;

        auto fb = createFramebuffer( fbufCreateInfo);

        rt.m_frameBuffers = fb;
        rt.m_clearValues.resize( attachments.size() );
        rt.m_extent = vk::Extent2D(width, height);
    }

    if(hasDepth)
    {
        rt.m_clearValues.back().depthStencil.setDepth(1.0);
        rt.m_clearValues.back().depthStencil.setStencil(0.0);
    }

    return rt;


}

}
