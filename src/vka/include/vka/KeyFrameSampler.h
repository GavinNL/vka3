#ifndef VKA_KEYFRAME_SAMPLER_H
#define VKA_KEYFRAME_SAMPLER_H

#include <array>
#include <vector>
#include <algorithm>
#include <variant>

//#include <vka/math/sequence.h>
#include <vka/math/linalg.h>
//#include <vka/math/transform.h>

#include <iterator>     // std::iterator_traits
#include <typeinfo>     // typeid

namespace vka
{

enum class InterpolationType
{
    STEP,
    LINEAR,
    CUBIC
};

/**
 * @brief The KeyFrameSampler_t class
 *
 * The KeyFrameSampler_t class is used to animate values along keyframes using
 * linear interpolation.
 *
 * This can be used for Skinning a model using a skeleton.
 * A sequence of bone transformations can be use to animate a model
 *
 * Any template type can be animated as long as the mix(T const&, T const&, float)
 * function is defined for _T's namespace
 */
template<typename T>
class KeyFrameSampler_t
{
public:
    using input_type  = float;
    using output_type = T;
    using value_type  = T;

    output_type operator()(input_type t) const
    {
        return get(t);
    }

    uint32_t findIndex(input_type t) const
    {
        if( t <= m_input.front() )
        {
           return 0;
        }
        else if( t >= m_input.back() )
        {
            return m_input.size()-1;
        }

        auto it = std::lower_bound( std::begin(m_input), std::end(m_input), t);
        auto t1 = *it--;
        auto t0 = *it;
        auto i = std::distance( std::begin(m_input), it);
        return i;
    }
    /**
     * @brief get
     * @param t
     * @return
     *
     * Returns the output_value sampled at the input_value.
     *
     * The output value is linearly interpolated using the
     * channel's keyframes.
     */
    output_type get(input_type t) const
    {
        if( t <= m_input.front() )
        {
           return m_output.front();
        }
        else if( t >= m_input.back() )
        {
            return m_output.back();
        }

        auto it = std::lower_bound( std::begin(m_input), std::end(m_input), t);
        auto t1 = *it--;
        auto t0 = *it;
        auto i = std::distance( std::begin(m_input), it);

        //return m_output[i];
        switch(m_type)
        {
            case InterpolationType::STEP:
                return m_output[i];
            default:
                break;
        }
        t = (t-t0) / (t1-t0);

        return get( i, i+1, t);

    }
    output_type get(size_t i1, size_t i2, input_type t) const
    {
        if constexpr ( std::is_same<output_type, glm::quat>::value )
        {
            return glm::slerp( m_output[i1], m_output[i2], t);
        }
        else
        {
            return glm::mix( m_output[i1], m_output[i2], t);
        }
    }
    /**
     * @brief insertKeyFrame
     * @param t
     * @param v
     *
     * Inserts a keyframe into the animation sampler.
     *
     */
    void insertKeyFrame(input_type t, output_type v)
    {
        auto it = std::lower_bound( std::begin(m_input), std::end(m_input), t);
        auto i = std::distance( std::begin(m_input), it);

        m_input.insert(it, t);
        m_output.insert(m_output.begin()+i, v);
    }

    input_type minTime() const
    {
        if(m_input.size() )   return m_input.front();
        return 0.0f;
    }

    input_type maxTime() const
    {
        if(m_input.size() )   return m_input.back();
        return 0.0f;
    }

public:
    std::vector<input_type>   m_input;
    std::vector<value_type>   m_output;
    InterpolationType         m_type = InterpolationType::LINEAR;
};

}

#endif

