#ifndef VKA_PIPELINE_CREATE_INFO2_H
#define VKA_PIPELINE_CREATE_INFO2_H

#include <vulkan/vulkan.hpp>
#include <functional>
#include <vka/utils/hash.h>
#include "DescriptorSets.h"

namespace vka
{

struct PipelineColorBlendStateCreateInfo2
{
    vk::PipelineColorBlendStateCreateFlags flags;
    vk::Bool32 logicOpEnable = 0;
    vk::LogicOp logicOp = vk::LogicOp();
    std::vector<vk::PipelineColorBlendAttachmentState> attachments;
    float blendConstants[4];

    void create( vk::PipelineColorBlendStateCreateInfo & c) const
    {
        c.pAttachments      = attachments.data();
        c.attachmentCount   = static_cast<uint32_t>(attachments.size());
        c.flags             = flags;
        c.logicOp           = logicOp;
        c.logicOpEnable     = logicOpEnable;
        c.blendConstants[0] = blendConstants[0];
        c.blendConstants[1] = blendConstants[1];
        c.blendConstants[2] = blendConstants[2];
        c.blendConstants[3] = blendConstants[3];
    }
};

struct PipelineShaderStageCreateInfo2
{
  void create(vk::PipelineShaderStageCreateInfo & c) const
  {
      c.flags = vk::PipelineShaderStageCreateFlags();
      c.stage = stage;
      c.module = module;
      c.pName = name.data();
      c.pSpecializationInfo = nullptr;
  }

public:
  vk::ShaderStageFlagBits              stage;
  vk::ShaderModule                     module;
  std::string                          name;
  //vk::SpecializationInfo               specializationInfo;
};


struct GraphicsPipelineCreateInfo2
{

    vk::PipelineCreateFlags                           flags;

    std::vector<PipelineShaderStageCreateInfo2>       stages;

    vk::PipelineVertexInputStateCreateFlags           vertexInputFlags;
    std::vector<vk::VertexInputBindingDescription>    vertexBindingDescriptions;
    std::vector<vk::VertexInputAttributeDescription>  vertexAttributeDescriptions;

    std::vector<vk::Viewport>                         viewports;
    std::vector<vk::Rect2D>                           scissors;

    vk::PipelineInputAssemblyStateCreateInfo          inputAssemblyState;

    vk::PipelineTessellationStateCreateInfo           tessellationState;
    vk::PipelineRasterizationStateCreateInfo          rasterizationState;
    vk::PipelineMultisampleStateCreateInfo            multisampleState;

    vk::PipelineDepthStencilStateCreateInfo           depthStencilState;
        PipelineColorBlendStateCreateInfo2            colorBlendState;


    std::vector<vk::DynamicState>                     dynamicStates;

    vk::PipelineLayout                                layout;
    vk::RenderPass                                    renderPass;
    uint32_t                                          subpass = 0;
    vk::Pipeline                                      basePipelineHandle;
    int32_t                                           basePipelineIndex;

private:
    vk::PipelineViewportStateCreateInfo           _viewport_info;
    vk::PipelineVertexInputStateCreateInfo        _vertexInput_info;
    vk::PipelineColorBlendStateCreateInfo         _colorBlendState;
    vk::PipelineDynamicStateCreateInfo            _dynamicState;
    std::vector<vk::PipelineShaderStageCreateInfo>       _stages;

public:
    GraphicsPipelineCreateInfo2()
    {
        depthStencilState
                           .setDepthTestEnable       ( VK_FALSE)
                           .setDepthWriteEnable      ( VK_FALSE)
                           .setDepthCompareOp        ( vk::CompareOp::eLess)// VK_COMPARE_OP_LESS;
                           .setDepthBoundsTestEnable ( VK_FALSE)
                           .setStencilTestEnable     ( VK_FALSE)
                           .setMinDepthBounds        ( 0.0f) // Optional
                           .setMaxDepthBounds        ( 1.0f) // Optional
                           .setStencilTestEnable     ( VK_FALSE);


        rasterizationState.setDepthClampEnable(VK_FALSE)
                          .setDepthBiasClamp(VK_FALSE)
                          .setRasterizerDiscardEnable(VK_FALSE)
                          .setPolygonMode(vk::PolygonMode::eFill)
                          .setLineWidth(1.0f)
                          .setCullMode(vk::CullModeFlagBits::eNone)
                          .setFrontFace( vk::FrontFace::eCounterClockwise)
                          .setDepthBiasEnable(VK_FALSE);

        multisampleState
                          .setRasterizationSamples(vk::SampleCountFlagBits::e1)
                          .setSampleShadingEnable(VK_FALSE);

        inputAssemblyState.topology = vk::PrimitiveTopology::eTriangleList;


    }
    vk::GraphicsPipelineCreateInfo create()
    {
        vk::GraphicsPipelineCreateInfo p;

        {
            for(auto & s : stages)
            {
                vk::PipelineShaderStageCreateInfo i;
                i.pName = s.name.data();
                i.stage = s.stage;
                i.module = s.module;
                _stages.push_back(i);
            }
        //    p.flags      = flags;
            p.pStages    = _stages.data();
            p.stageCount = static_cast<uint32_t>(_stages.size());
        }


        if( colorBlendState.attachments.size() == 0)
        {
            vk::PipelineColorBlendAttachmentState C;
            C.colorWriteMask      = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
            C.blendEnable         = VK_TRUE;
            C.colorBlendOp        = vk::BlendOp::eAdd;
            C.srcAlphaBlendFactor = vk::BlendFactor::eSrcAlpha;
            C.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
            C.dstAlphaBlendFactor = vk::BlendFactor::eOneMinusDstAlpha;
            C.dstColorBlendFactor = vk::BlendFactor::eOneMinusDstAlpha;
            colorBlendState.attachments.push_back(C);
        }

        colorBlendState.create(_colorBlendState);


        {
            _vertexInput_info.flags = vertexInputFlags;
            _vertexInput_info.pVertexBindingDescriptions    = vertexBindingDescriptions.data();
            _vertexInput_info.vertexBindingDescriptionCount = static_cast<uint32_t>(vertexBindingDescriptions.size());

            _vertexInput_info.pVertexAttributeDescriptions    = vertexAttributeDescriptions.data();
            _vertexInput_info.vertexAttributeDescriptionCount = static_cast<uint32_t>(vertexAttributeDescriptions.size());

            p.pVertexInputState   = &_vertexInput_info;
        }


        p.pInputAssemblyState = &inputAssemblyState;
        p.pTessellationState  = &tessellationState;

        {

            _viewport_info.pScissors = scissors.data();
            _viewport_info.scissorCount = static_cast<uint32_t>(scissors.size());

            _viewport_info.pViewports = viewports.data();
            _viewport_info.viewportCount = static_cast<uint32_t>( viewports.size() );

            p.pViewportState      = &_viewport_info;
        }


        p.pRasterizationState = &rasterizationState;
        p.pMultisampleState   = &multisampleState;

        p.pDepthStencilState  = &depthStencilState;
        {
            colorBlendState.create( _colorBlendState );
            p.pColorBlendState    = &_colorBlendState;
        }

        if( dynamicStates.size() )
        {
            _dynamicState.pDynamicStates    = dynamicStates.data();
            _dynamicState.dynamicStateCount = static_cast<uint32_t>(dynamicStates.size());
            p.pDynamicState                 = &_dynamicState;
        }

        p.layout     = layout;
        p.renderPass = renderPass;

        //p.subpass            = subpass;
        //p.basePipelineHandle = basePipelineHandle;
        //p.basePipelineIndex  = basePipelineIndex;

        return p;
    }
};


struct GraphicsPipelineColorOutput
{
    vk::Format format;
    bool       enableBlending = false;

    bool operator<(GraphicsPipelineColorOutput const & right) const
    {
        return std::tie(format,enableBlending) < std::tie(right.format,right.enableBlending);
    }
};

/**
 * @brief The GraphicsPipelineCreateInfo_SwapChain struct
 *
 * A helper class used to help create pipelines that
 * render to the swapchain.
 */
struct GraphicsPipelineCreateInfo4
{
    vk::ShaderModule        vertexShader;
    vk::ShaderModule        fragmentShader;

    std::string             vertexShaderMain = "main";
    std::string             fragmentShaderMain = "main";

    vk::RenderPass          renderPass;
    std::vector<vk::Format> vertexInputFormats; // currently assume that vertex attributes
                                                // are stored in separate buffers and are tightly packed
    std::vector<GraphicsPipelineColorOutput> colorOutputs;

    bool                    enableBlending   = true;
    bool                    enableDepthTest  = false;
    bool                    enableDepthWrite = false;

    vk::PrimitiveTopology   topology    = vk::PrimitiveTopology::eTriangleList;
    vk::CullModeFlags       cullMode    = vk::CullModeFlagBits::eNone;
    vk::FrontFace           frontFace   = vk::FrontFace::eCounterClockwise;
    vk::PolygonMode         polygonMode = vk::PolygonMode::eFill;
};

}

#endif 

