#ifndef VKA_SCREEN_H
#define VKA_SCREEN_H

#include <vulkan/vulkan.hpp>
#include <vector>
#include <set>

#include <vka/core/allocator.h>
#include <vka/core/ShaderReflect.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/ShaderModuleCreateInfo.h>
#include <vka/core/PipelineLayoutCreateInfo.h>
#include <vka/core/DescriptorPoolCreateInfo.h>
#include <vka/core/RenderPassCreateInfo.h>
#include <vka/core/FramebufferCreateInfo.h>
#include <vka/core/SamplerCreateInfo.h>

namespace vka
{

struct ImageViewInfo
{
    vk::ImageViewCreateInfo createInfo;

    std::set<vk::SwapchainKHR> swapchains; // which swapchains uses this
    std::set<vk::Framebuffer>  framebuffers; // which framebuffers are using this imageview
};

struct ImageInfo
{
    vk::ImageCreateInfo     createInfo;

    vk::DeviceMemory        memory; // which memory object its bound to
    vk::DeviceSize          offset; // at what offset


    //----------------
    std::set<vk::ImageView> imageViews; // all images views that currently
                                        // use this image
    std::vector< std::vector<vk::ImageLayout > > m_layouts; //  the current layout at [layer][mip level]

};

struct SamplerInfo
{
    vka::SamplerCreateInfo2 createInfo;
    std::size_t             hash;
};

struct SwapchainInfo
{
    vk::SwapchainCreateInfoKHR createInfo;
    std::vector<vk::Image>     images;
};

struct RenderPassInfo
{
    std::vector<vk::AttachmentDescription> attachments;

    std::vector<vk::SubpassDescription>    subPasses;
    std::vector<vk::SubpassDependency>     dependencies;



    vk::RenderPassCreateInfo createInfo;

    vka::RenderPassCreateInfo2 createInfo2;
    size_t                     hash;


    // which framebuffers are using this renderpass.
    std::set<vk::Framebuffer> frameBuffers;
    // which pipelines are using this renderpass
    std::set<vk::Pipeline>    pipelines;
};

struct FrameBufferInfo
{
    vka::FramebufferCreateInfo2  createInfo;
};

struct CommandPoolInfo
{
    vk::CommandPoolCreateInfo createInfo;
};

struct SemaphoreInfo
{
    vk::SemaphoreCreateInfo createInfo;
};


struct DeviceMemoryInfo
{
    vk::MemoryAllocateInfo createInfo;

    std::set<vk::Buffer> boundBuffers; // which buffers are bound to this memory.
    std::set<vk::Image>  boundImages; // which images are bound to this memory.

    void * map = nullptr;
};

struct BufferInfo
{
    vk::BufferCreateInfo createInfo;


    vk::DeviceMemory   memory; // which memory object are we currently bound to
    vk::DeviceSize     offset=0; // at what offset are we bound to.
};

struct BufferPoolInfo
{
    vk::DeviceMemory     memory; // which memory are we using
    vk::MemoryRequirements   memoryRequirements;
    vka::AllocatorHelper allocator;
};




struct ShaderModuleInfo
{
    vka::ShaderModuleCreateInfo2 createInfo;
    size_t                       hash;
    ShaderReflect                reflection;

    std::set<vk::Pipeline>       pipelines; // which pipelines are using this

    //std::set<vk::Pipeline>     pipelines; // which pipelines are using this
};



struct DescriptorSetInfo
{
    vk::DescriptorPool      pool;
    vk::DescriptorSetLayout layout;
};

struct DescriptorPoolInfo
{
    vka::DescriptorPoolCreateInfo2      createInfo;

    std::set<vk::DescriptorSet> sets;
};

struct DescriptorSetLayoutInfo
{
    vka::DescriptorSetLayoutCreateInfo2 createInfo;
    size_t                              hash;

    // is this layout used by any of the following:
    std::set<vk::PipelineLayout> pipelineLayouts;
    std::set<vk::DescriptorSet>  descriptorSets;
};


struct PipelineLayoutInfo
{
    PipelineLayoutCreateInfo2  createInfo;
    size_t                     hash;


    // Dependences
    std::set<vk::Pipeline>     pipelines; // which pipelines are using this layout
};


struct PipelineInfo
{
    GraphicsPipelineCreateInfo2 createInfo;
};


}

#endif 

