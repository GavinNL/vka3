#ifndef VKA_DESCRIPTOR_SET_LAYOUT_CREATE_INFO_H
#define VKA_DESCRIPTOR_SET_LAYOUT_CREATE_INFO_H

#include <vulkan/vulkan.hpp>
#include <vector>
#include <vka/utils/hash.h>


namespace vka
{

/**
 * @brief The PipelineLayoutCreateInfo2 struct
 *
 * This is a helper struct used to create DescriptorSetLayouts.
 *
 * Unlike the regular DescriptorSetLayoutCreateInfo, this
 * struct always keeps the binding array in sorted order
 * It also is able to produce a hash of the layout information
 *
 */
struct DescriptorSetLayoutCreateInfo2
{
    vk::DescriptorSetLayoutCreateFlags          flags;
    std::vector<vk::DescriptorSetLayoutBinding> bindings;

    vk::DescriptorSetLayoutCreateInfo create() const
    {
        vk::DescriptorSetLayoutCreateInfo I;

        I.pBindings    = bindings.data();
        I.bindingCount = static_cast<uint32_t>(bindings.size());
        I.flags        = flags;

        return I;
    }

    DescriptorSetLayoutCreateInfo2(){}

    void enablePushDescriptor(bool enable)
    {
        if( enable )
        {
            flags |= vk::DescriptorSetLayoutCreateFlagBits::ePushDescriptorKHR;
        }
        else
        {
            flags &= ~(vk::DescriptorSetLayoutCreateFlagBits::ePushDescriptorKHR);
        }
    }
    /**
     * @brief addDescriptor
     * @param binding
     * @param descriptorType_
     * @param stages
     *
     * Adds a descriptor to the layout. You can call this function multiple times with the same
     * information
     */
    void addDescriptor( uint32_t binding, vk::DescriptorType descriptorType_, uint32_t arrayCount, vk::ShaderStageFlags stages)
    {
        for(auto & b : bindings)
        {
            if( b.binding == binding && b.descriptorType == descriptorType_ )
            {
                b.stageFlags |= stages;
                b.descriptorCount = arrayCount;
                return;
            }
        }

        vk::DescriptorSetLayoutBinding b;
        b.binding        = binding;
        b.descriptorType = descriptorType_;
        b.stageFlags     = stages;
        b.descriptorCount = arrayCount;
        bindings.push_back(b);

        // always sort the bindings based on binding number
        // so that the hash calculations will produce correct values
        std::sort( bindings.begin(), bindings.end(),
                   [](vk::DescriptorSetLayoutBinding const & A, vk::DescriptorSetLayoutBinding const & B)
        {
            return A.binding < B.binding;
        });

    }

    size_t getHash() const;

};

}

namespace std
{
    template<>
    struct hash<vka::DescriptorSetLayoutCreateInfo2>
    {
        std::size_t operator()(vka::DescriptorSetLayoutCreateInfo2 const& s) const noexcept
        {
            std::hash<size_t> H;

            size_t h = H( s.bindings.size() );

            for(auto & b : s.bindings)
            {
                h = vka::hashCombine(h, getHash(b) );
            }
            h = vka::hashCombine(h, H( static_cast<uint32_t>(s.flags)) );

            return h;
        }

        static size_t getHash( vk::DescriptorSetLayoutBinding const & B) noexcept
        {
            std::hash<size_t> H;

            auto h = H(B.binding);

            h = vka::hashCombine(h, H(static_cast<uint32_t>(B.stageFlags    )) );
            h = vka::hashCombine(h, H(static_cast<size_t>(B.descriptorType))   );
            h = vka::hashCombine(h, H(B.descriptorCount)                       );

            return h;
        }
    };
}

namespace vka
{
    inline size_t DescriptorSetLayoutCreateInfo2::getHash() const
    {
        std::hash< vka::DescriptorSetLayoutCreateInfo2 > H;
        return H(*this);
    }
}

#endif
