#pragma once
#ifndef VKA_ALLOCATOR_HELPER
#define VKA_ALLOCATOR_HELPER

#include <list>
#include <algorithm>

namespace vka {


/**
 * @brief The Allocator class
 *
 * The AllocatorHelper class is used to
 * find blocks of memory in a buffer and flag them as
 * allocated.
 */
class AllocatorHelper
{
public:
    using size_type   = size_t;
    using offset_type = size_t;

private:
    struct block_t
    {
        offset_type offset;
        size_type   size;
        bool        allocated=false;
    };

public:

    /**
     * @brief setSize
     * @param size
     *
     * Sets the size of the helper. This will clear any
     * current information within the helper.
     */
    void setSize( size_type size)
    {
        m_blocks.clear();
        m_blocks.emplace_back( block_t{0, size, false} );
    }


    /**
     * @brief allocate
     * @param size
     * @return
     *
     * Allocate a block of size, size, and return the
     * offset from the star of the buffer.
     */
    offset_type allocate(size_type size)
    {
        auto it_FreeBlock =
        std::find_if( m_blocks.begin(),
                      m_blocks.end(),
                      [&](block_t const& v)
        {
            return !v.allocated && size <= v.size;
        });

        if( it_FreeBlock==m_blocks.end() )
        {
            return std::numeric_limits<offset_type>::max();
        }
        _splitBlock(it_FreeBlock, size);

        it_FreeBlock->allocated =  true;

        return it_FreeBlock->offset;
    }

    bool canAllocate(size_type size) const
    {
        auto it_FreeBlock =
        std::find_if( m_blocks.begin(),
                      m_blocks.end(),
                      [&](block_t const& v)
        {
            return !v.allocated && size <= v.size;
        });

        if( it_FreeBlock==m_blocks.end() )
        {
            return false;
        }
        return true;
    }

    size_type getAllocationSize(offset_type offset) const
    {
        auto it_FreeBlock =
        std::find_if( m_blocks.begin(),
                      m_blocks.end(),
                      [&](block_t const& v)
        {
            return v.offset == offset;
        });

        if( it_FreeBlock==m_blocks.end() )
        {
            return 0;
        }
        return it_FreeBlock->size;
    }

    /**
     * @brief free
     * @param offset
     *
     * Free a block which was previously allocated.
     */
    void free(offset_type offset)
    {
        auto it_FreeBlock =
        std::find_if( m_blocks.begin(),
                      m_blocks.end(),
                      [&](block_t const& v)
        {
            return offset == v.offset && v.allocated;
        });

        if( it_FreeBlock != m_blocks.end() )
        {
            it_FreeBlock->allocated=false;
            _mergeForwardUnallocatedBlocks(it_FreeBlock);
        }
    }

    /**
     * @brief _mergeForwardBlock
     * @param iterator
     *
     * Merges the block given by iterator with the block ahead of it.
     * Both blocks must be unallocated.
     */
    void _mergeForwardUnallocatedBlocks(std::list<block_t>::iterator iterator)
    {
        auto prev = iterator;

        // if the previous iterator is also empty, do a forward merge on that one instead
        if( prev != m_blocks.begin() )
        {
            prev = std::prev(iterator);
            if( !prev->allocated)
            {
                _mergeForwardUnallocatedBlocks(prev);
                return;
            }
        }


        auto next = std::next(iterator);

        // loop forward through all the iterators and
        // merge them if they are unallocated. stop when we
        // reach the first allocated block.
        while( next != std::end(m_blocks))
        {
            if( !next->allocated )
            {
                iterator->size += next->size;
                next = m_blocks.erase( next );
            }
            else {
                break;
            }
        }
    }

    /**
     * @brief _splitBlock
     * @param iterator
     * @param s
     *
     * Splits a block of size N into a block of  size s and N-s.
     */
    void _splitBlock(std::list<block_t>::iterator iterator, size_type s)
    {
        block_t b{iterator->offset+s, iterator->size-s};
        iterator->size = s;

        if( b.size )
            m_blocks.insert(++iterator, b);
    }

    size_t numBlocks() const
    {
        return m_blocks.size();
    }
public:
    std::list<block_t> m_blocks;
};

}

#endif
