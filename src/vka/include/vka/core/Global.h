#ifndef VKA_GLOBAL_OBJECTS
#define VKA_GLOBAL_OBJECTS

#include <map>
#include "RenderPassInfo.h"
#include "ObjectContainer.h"
#include "PipelineLayoutCreateInfo.h"
#include "ShaderModuleCreateInfo.h"
#include "RenderPassCreateInfo.h"
#include "FramebufferCreateInfo.h"
#include "SamplerCreateInfo.h"

namespace vka
{

class System;
class BufferPool;
class SubBuffer;
class ScreenBuffer;

struct GLOBAL_OBJECTS_t
{
public:
    vka::DescriptorPoolInfo const & info(vk::DescriptorPool p) const
    {
        return descriptorPoolsContainer.info(p);
    }
    vka::DescriptorSetInfo const & info(vk::DescriptorSet p) const
    {
        return descriptorSetsContainer.info(p);
    }
    vka::ShaderModuleInfo const & info(vk::ShaderModule p) const
    {
        return shaderModulesContainer.info(p);
    }
    vka::PipelineInfo const & info(vk::Pipeline p) const
    {
        return pipelinesContainer.info(p);
    }
    vka::BufferInfo const & info(vk::Buffer p) const
    {
        return buffersContainer.info(p);
    }
    vka::BufferPoolInfo const & bufferPoolInfo(vk::Buffer p) const
    {
        return bufferPools.at(p);
    }
    vka::DeviceMemoryInfo const & info(vk::DeviceMemory p) const
    {
        return memoryContainer.info(p);
    }
    vka::ImageInfo const & info(vk::Image p) const
    {
        return imagesContainer.info(p);
    }
    vka::ImageViewInfo const & info(vk::ImageView p) const
    {
        return imageViewsContainer.info(p);
    }
    vka::RenderPassInfo const & info(vk::RenderPass p) const
    {
        return renderPassContainer.info(p);
    }
    vka::DescriptorSetLayoutInfo const & info(vk::DescriptorSetLayout p) const
    {
        return descriptorSetLayoutsContainer.info(p);
    }

    vka::System * system=nullptr;
protected:
    std::map<vk::Semaphore, SemaphoreInfo> semaphores;
    std::map<vk::CommandPool, CommandPoolInfo> commandPools;

    std::map<vk::SwapchainKHR, SwapchainInfo> swapchains;


    std::map<vk::Buffer, BufferPoolInfo>  bufferPools;

    //==========================================================================
    // Object containers
    //==========================================================================
    vka::ObjContainer<vk::Buffer,
                      BufferInfo>                       buffersContainer;

    vka::ObjContainer<vk::DeviceMemory,
                      DeviceMemoryInfo>                       memoryContainer;

    vka::ObjContainer<vk::ImageView,
                      ImageViewInfo>                       imageViewsContainer;

    vka::ObjContainer<vk::Framebuffer,
                      FrameBufferInfo>                     framebuffersContainer;



    vka::ObjContainer<vk::Pipeline,
                      PipelineInfo>                        pipelinesContainer;


    vka::ObjContainer<vk::DescriptorPool,
                      DescriptorPoolInfo>                  descriptorPoolsContainer;

    vka::ObjContainer<vk::Image,
                      ImageInfo>                           imagesContainer;
    //==========================================================================

    //==========================================================================
    // Hashed Object Containers
    //   - Hashed object containers hold objects which can be used multiple times
    //     by different entities. They are hashed based on their CreateInfo struct
    //==========================================================================
    vka::HashedObjContainer<vk::ShaderModule,
                            ShaderModuleInfo,
                            ShaderModuleCreateInfo2>       shaderModulesContainer;

    vka::HashedObjContainer<vk::RenderPass,
                            RenderPassInfo,
                            RenderPassCreateInfo2>         renderPassContainer;

    vka::HashedObjContainer<vk::DescriptorSetLayout,
                            DescriptorSetLayoutInfo,
                            DescriptorSetLayoutCreateInfo2>         descriptorSetLayoutsContainer;


    vka::HashedObjContainer<vk::PipelineLayout,
                            PipelineLayoutInfo,
                            PipelineLayoutCreateInfo2>              pipelineLayoutsContainer;

    vka::ObjContainer<vk::DescriptorSet , DescriptorSetInfo>   descriptorSetsContainer;

    vka::HashedObjContainer<vk::Sampler,
                            SamplerInfo,
                            SamplerCreateInfo2>              samplersContainer;
    //==========================================================================


    friend class System;
    friend class BufferPool;
    friend class SubBuffer;
    friend class ScreenBuffer;
    friend class TexturePool;
    friend class CommandBuffer;
};

extern GLOBAL_OBJECTS_t GLOBAL;

}
#endif

