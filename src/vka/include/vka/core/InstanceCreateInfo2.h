#ifndef VKA_INSTANCE_CREATE_INFO2_H
#define VKA_INSTANCE_CREATE_INFO2_H

#include <vulkan/vulkan.hpp>
#include <set>

namespace vka
{

struct SystemCreateInfo2
{
    vk::Instance       instance;
    vk::PhysicalDevice physicalDevice;
    vk::Device         device;
    uint32_t           queuePresentFamily = std::numeric_limits<uint32_t>::max();
    vk::Queue          queuePresent;
    uint32_t           queueGraphicsFamily = std::numeric_limits<uint32_t>::max();
    vk::Queue          queueGraphics;
    vk::SurfaceKHR     surface;
};

struct InstanceCreateInfo2
{
    std::vector<std::string> requiredExtensions;
    std::vector<std::string> validationLayers;

    uint32_t vulkanAPIVersion = VK_API_VERSION_1_0;

    std::string applicationName = "";
    uint32_t    applicationVersionMajor = 0;
    uint32_t    applicationVersionMinor = 0;
    uint32_t    applicationVersionPatch = 0;

    std::string engineName = "";

    uint32_t    engineVersionMajor = 0;
    uint32_t    engineVersionMinor = 0;
    uint32_t    engineVersionPatch = 0;

    vk::InstanceCreateInfo create()
    {
        vk::InstanceCreateInfo vulkan_createInfo;

        for(auto & v : requiredExtensions)
        {
            m_required_extensions.push_back( &v[0] );
        }
        for(auto & v : validationLayers)
        {
            m_validationLayers.push_back( &v[0] );
        }

        m_appInfo.setPApplicationName( applicationName.c_str() )
                 .setApplicationVersion( VK_MAKE_VERSION(applicationVersionMajor, applicationVersionMinor, applicationVersionPatch) )
                 .setPEngineName( engineName.data() )
                 .setEngineVersion(VK_MAKE_VERSION(engineVersionMajor, engineVersionMinor, engineVersionPatch) )
                 .setApiVersion( VK_API_VERSION_1_0);

        vulkan_createInfo.setPApplicationInfo(&m_appInfo)
                  .setEnabledExtensionCount(  static_cast<uint32_t>(m_required_extensions.size()) )
                  .setPpEnabledExtensionNames(m_required_extensions.data())
                  .setEnabledLayerCount(0);

        if( validationLayers.size() )
            vulkan_createInfo.setEnabledLayerCount(   static_cast<uint32_t>(m_validationLayers.size()) )
                             .setPpEnabledLayerNames( m_validationLayers.data() );

        return vulkan_createInfo;
    }

protected:
    vk::ApplicationInfo       m_appInfo;
    std::vector<char const*>  m_required_extensions;
    std::vector<char const*>  m_validationLayers;
};


struct PhysicalDeviceCreateInfo2
{
    vk::Instance             instance;
    std::vector<std::string> deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME};

    // optional?
    vk::SurfaceKHR surface;
};


struct DeviceCreateInfo2
{
    vk::PhysicalDevice         physicalDevice;
    vk::PhysicalDeviceFeatures deviceFeatures;

    // the queue index in the device to use for graphics
    // and presenting. If set to -1, will find an appropriate
    // queue
    uint32_t                        graphicsQueueFamily = std::numeric_limits<uint32_t>::max();
    uint32_t                        presentQueueFamily  = std::numeric_limits<uint32_t>::max();

    std::vector<std::string>   requiredValidationLayers;
    std::vector<std::string>   requiredDeviceExtensions;

    vk::DeviceCreateInfo create()
    {
        vk::DeviceCreateInfo createInfo;

        {
            std::set<uint32_t> uniqueQueueFamilies = {graphicsQueueFamily, presentQueueFamily};

            float queuePriority = 1.0f;
            for(auto queueFamily : uniqueQueueFamilies)
            {
                vk::DeviceQueueCreateInfo queueCreateInfo;

                queueCreateInfo.setQueueFamilyIndex( queueFamily )
                               .setQueueCount( 1 )
                               .setPQueuePriorities(&queuePriority);

                _queueCreateInfos.push_back(queueCreateInfo);
            }
        }
        createInfo.setPQueueCreateInfos(    _queueCreateInfos.data() )
                .setQueueCreateInfoCount(   static_cast<uint32_t>(_queueCreateInfos.size()) )
                .setPEnabledFeatures(       &deviceFeatures )
                .setEnabledExtensionCount(  0)
                .setEnabledLayerCount(      0); // set default to ze

        std::vector<const char*> validationLayers;
        for(auto & s : requiredValidationLayers)
            _validationLayers.push_back( &s[0] );

        for(auto & s : requiredDeviceExtensions)
            _deviceExtensions.push_back( &s[0] );

        createInfo.setEnabledLayerCount(   static_cast<uint32_t>(_validationLayers.size()) )
                  .setPpEnabledLayerNames( _validationLayers.data() );

        createInfo.setEnabledExtensionCount( static_cast<uint32_t>(_deviceExtensions.size()) )
                .setPpEnabledExtensionNames( _deviceExtensions.data() );
        return createInfo;
    }

    /**
     * @brief findAppropriateQueues
     * @param surface
     * @return
     *
     * Find an approprate queue that can present the surface.
     */
    bool findAppropriateQueues(vk::SurfaceKHR surface);


protected:
    std::vector<const char*>               _validationLayers;
    std::vector<const char*>               _deviceExtensions;
    std::vector<vk::DeviceQueueCreateInfo> _queueCreateInfos;
};

}

#endif
