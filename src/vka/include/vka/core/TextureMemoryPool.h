#ifndef VKA_TEXTURE_MEMORY_POOL_H
#define VKA_TEXTURE_MEMORY_POOL_H


#include <vulkan/vulkan.hpp>
#include "allocator.h"
#include <set>

namespace vka
{

class HostImage;

struct TexturePoolInfo
{
    vk::DeviceMemory         memory; // which memory are we using
    vk::MemoryRequirements   memoryRequirements;
    vka::AllocatorHelper     allocator;


    std::set<vk::Image>      images;     // All allocated images from this pool
    std::set<vk::ImageView>  imageViews; // All allocated images from this pool
};

/**
 * @brief The BufferPool struct
 *
 * A BufferPool is used to allocate SubBuffers to use
 * as vertex attributes or uniform buffers.
 *
 * A BufferPool is simply a vk::Buffer that is large and
 * has some inner bookkeeping to know where SubBuffers
 * are allocated.
 */
class TexturePool
{
    protected:
        vk::Image       m_image; // look up in GLOBAL.texturePools
        TexturePoolInfo m_info;

        vk::ImageView  _createDefaultView(vk::Image img, vk::ImageViewType type);

        vk::Image   _allocateImage( vk::Format format,
                                    vk::ImageType type,
                                        vk::Extent3D extent,
                                        uint32_t arrayLayers,
                                        uint32_t mipLevels);

        void _destroyImageView(vk::ImageView v);
        void _destroyImage(vk::Image i);

        /**
         * @brief createNewImage
         * @return
         *
         * Allocates a vk::Image and a vk::ImageView and returns the new
         * vk::imageView. The vk::Image can be retrieved using the info method.
         */
        vk::ImageView   createNewImage( vk::Format format,
                                        vk::Extent3D extent,
                                        uint32_t arrayLayers,
                                        uint32_t mipLevels);
    public:

        /**
         * @brief allocationSize
         * @return
         *
         * Return the total size in bytes of the texture pool
         */
        vk::DeviceSize allocationSize() const;

        /**
         * @brief destroyAll
         *
         * Destroys all the images in the texture pool. The texture
         * pool can still be used to allocate memory.
         */
        void destroyAll();





        vk::ImageView   createNewImage2D( vk::Format format,
                                        vk::Extent2D extent,
                                        uint32_t arrayLayers,
                                        uint32_t mipLevels);

        vk::ImageView   createNewImageCube( vk::Format format,
                                        vk::Extent2D extent,
                                        uint32_t mipLevels);


        /**
         * @brief destroyImage
         * @param v
         * @return
         *
         * Destroy the imageview that was allocated by calling createNewImage( ).
         *
         * If the vk::Image is not referenced by another imageView, it will be
         * destroyed as well.
         */
        void            destroyImage(vk::ImageView v);

        friend class System;

};


}


#endif
