#ifndef VKA_DESCRIPTOR_WRITER_H
#define VKA_DESCRIPTOR_WRITER_H

#include <iostream>


#include <vulkan/vulkan.hpp>
#include <vka/core/BufferMemoryPool.h>

namespace vka
{


/**
 * @brief The DescriptorWriter struct
 *
 * DescriptorWriter is a helper for
 * creating DescriptorWrite objects to update descriptor sets.
 */
struct DescriptorWriter
{
public:
    std::vector<vk::WriteDescriptorSet>        writeDescriptorSet;

    DescriptorWriter()
    {
        writeDescriptorSet.reserve(10000);
    }
    void clear()
    {
        writeDescriptorSet.clear();
        m_imageInfos.clear();
        m_bufferInfos.clear();
    }

    /**
     * @brief attachBufferDescriptor
     * @param set
     * @param binding
     * @param type
     * @param buff
     *
     * Attach a Buffer to the set
     */
    void attachBufferDescriptor( vk::DescriptorSet  set,
                                 uint32_t           binding,
                                 vk::DescriptorType type,
                                 vka::SubBuffer     buff,
                                 uint32_t           offset=0)
    {
            auto * bInfo2  = allocateDescriptorBufferInfo();
            bInfo2->buffer = buff.getParentBuffer();
            bInfo2->offset = buff.getOffset()+offset;
            bInfo2->range  = buff.getSize();

            vk::WriteDescriptorSet wr2;
            wr2.setDescriptorType(type);
            wr2.setPBufferInfo(bInfo2);
            wr2.setDstSet( set );
            wr2.setDstBinding( binding );
            wr2.setDstArrayElement( 0 );
            wr2.setDescriptorCount(  1 );

            writeDescriptorSet.push_back(wr2);
    }


    /**
     * @brief attachTextureDescriptor
     * @param set
     * @param binding
     * @param type
     * @param img
     * @param imgSampler
     *
     * Attach a single texture
     */
    void attachTextureDescriptor(vk::DescriptorSet  set,
                                 uint32_t           binding,
                                 vk::DescriptorType type,
                                 vk::ImageView      img,
                                 vk::Sampler        imgSampler)
    {
        vk::DescriptorImageInfo * pImageInfo = allocateDescriptorImageInfo();

        pImageInfo->imageView   = img;
        pImageInfo->sampler     = imgSampler;
        pImageInfo->imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;


        vk::WriteDescriptorSet wr ( set,
                                    binding,
                                    0,           // array element
                                    1,           // total images
                                    type,
                                    pImageInfo,
                                    nullptr,
                                    nullptr );


        writeDescriptorSet.push_back(wr);

    }


    /**
     * @brief attachArrayOfTextureDescriptor
     * @param set
     * @param binding
     * @param type
     * @param img
     * @param imgSampler
     *
     * Attach an array of textures. Use this when you want to update
     * an entire array. This method uses the same sampler for all
     * textures.
     *
     * layout(set = 1, binding = 0) uniform sampler2D Textures[8];
     *
     */
    void attachArrayOfTextureDescriptor(vk::DescriptorSet          set,
                                        uint32_t                   binding,
                                        vk::DescriptorType         type,
                                        std::vector<vk::ImageView> const &img,
                                        vk::Sampler                imgSampler)
    {
        vk::DescriptorImageInfo * pImageInfo = allocateDescriptorImageInfo( img.size() );

        auto siz = img.size();
        for(size_t i=0; i<siz; i++)
        {
            pImageInfo[i].imageView   = img[i];
            pImageInfo[i].sampler     = imgSampler;
            pImageInfo[i].imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
        }

        vk::WriteDescriptorSet wr ( set,
                                    binding,
                                    0,           // array element
                                    static_cast<uint32_t>(siz),         // total images
                                    type,
                                    pImageInfo,
                                    nullptr,
                                    nullptr );


        writeDescriptorSet.push_back(wr);

    }



    vk::DescriptorImageInfo* allocateDescriptorImageInfo(size_t number=1)
    {
        std::vector<vk::DescriptorImageInfo> n(number);
        m_imageInfos.push_back( std::move(n) );
        return m_imageInfos.back().data();
    }

    vk::DescriptorBufferInfo* allocateDescriptorBufferInfo()
    {
        std::vector<vk::DescriptorBufferInfo> n(1);
        m_bufferInfos.push_back( std::move(n) );
        return m_bufferInfos.back().data();
    }
protected:

    std::vector< std::vector<vk::DescriptorBufferInfo> >      m_bufferInfos;
    std::vector< std::vector<vk::DescriptorImageInfo> >      m_imageInfos;

};

}

#endif
