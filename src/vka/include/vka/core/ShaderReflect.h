#pragma once
#ifndef VKA_SHADER_REFLECT_H
#define VKA_SHADER_REFLECT_H

#include <string>
#include <cstdint>
#include <map>
#include <vulkan/vulkan.hpp>

namespace vka
{


struct DescriptorBinding_t
{
    std::string        name;
    vk::DescriptorType type;
    uint32_t           set;
    uint32_t           binding;
    uint32_t           size; // size of buffer if type==uniform
    uint32_t           arraySize; // number of elements in the array if any.
};

struct VertexInput_t
{
    vk::Format  format;
    uint32_t    location; // the location in the shader. eg  layout(location=3) in uvec4 in_Joints_0;
    std::string name;   // the name of the variable
};

struct PushConstantData_t
{
    std::string name;
    size_t size  = 0;
};

struct ShaderReflect
{
    std::map<std::string, DescriptorBinding_t> descriptors;
    std::vector<VertexInput_t>                 inputs;     // incoming attributes attributes
    std::vector<VertexInput_t>                 outputs;    // outgoing attributes
    PushConstantData_t                         pushConstants;

    void parseSPIRV(const std::vector<uint32_t> & src);
};

}

#endif
