#ifndef VKA2_DEVICE_MESH_PRIMITIVE_H
#define VKA2_DEVICE_MESH_PRIMITIVE_H

#include <vector>
#include <string.h>
#include <map>

#include <cassert>

#include <vka/utils/ModelTypes.h>
#include <vka/utils/VulkanHelperFunctions.h>
#include <vka/utils/PrimitiveDrawCall.h>
#include <vka/utils/HostTriMesh.h>

#include "BufferMemoryPool.h"
#include <ugltf/ugltf.h>

namespace vka
{

class HostMeshPrimitive;

/**
 * @brief The DeviceMeshPrimitive struct
 *
 * A DeviceMeshPrimitive (dmp) is the GPU equivelent of a HostMeshPrimtiive.
 * it is a single triangular mesh which exists on the gpu.
 *
 * A DMP can be allocated from a BufferPool
 *
 */
class DeviceMeshPrimitive
{
protected:
    vka::SubBuffer               m_subBuffer;
    // offsets within the subbuffer
    std::array<vk::DeviceSize, static_cast<size_t>(PrimitiveAttribute::__LAST)+1 > m_attributeOffsets = {0};
    vk::IndexType        m_indexType;
    PrimitiveDrawCall    m_drawInfo;
    uint32_t             m_offsetFlags = 0;

public:
    void setBuffer( vka::SubBuffer b)
    {
        m_subBuffer = b;
    }
    /**
     * @brief getAttributesMask
     * @return
     *
     * Returns 32-bit flag mask indicating which
     * attributes exist in this device mesh primitive;
     *
     * POSITION   = bit 0
     * NORMAL	  = bit 1
     * TANGENT	  = bit 2
     * TEXCOORD_0 = bit 3
     * TEXCOORD_1 = bit 4
     * COLOR_0	  = bit 5
     * JOINTS_0   = bit 6
     * WEIGHTS_0  = bit 7
     */
    uint32_t getAttributesMask() const
    {
        return m_offsetFlags;
    }

    PrimitiveDrawCall const & drawCall() const
    {
        return m_drawInfo;
    }

    void setDrawCall(PrimitiveDrawCall const & p)
    {
        m_drawInfo = p;
    }

    vka::SubBuffer getSubBuffer() const
    {
        return m_subBuffer;
    }

    vk::DeviceSize getIndexByteOffset() const
    {
        return m_attributeOffsets.back();
    }

    /**
     * @brief setOffset
     * @param attribute
     * @param offset
     *
     * Sets the offset within the buffer where this attribute
     * exists.
     */
    void setOffset(PrimitiveAttribute attribute, std::optional<vk::DeviceSize> offset)
    {
        if( offset.has_value() )
        {
            m_offsetFlags |= ( uint32_t(1) << static_cast<uint32_t>(attribute) );
            m_attributeOffsets[ static_cast<uint32_t>(attribute) ]  = *offset;
        }
        else
        {
            m_offsetFlags &= ~( uint32_t(1) << static_cast<uint32_t>(attribute) );
            m_attributeOffsets[ static_cast<uint32_t>(attribute) ]  = std::numeric_limits<vk::DeviceSize>::max();
        }
    }

    decltype(m_attributeOffsets) const & getOffsets() const
    {
        return m_attributeOffsets;
    }

    uint32_t indexCount() const
    {
        return m_drawInfo.indexCount;
    }
    uint32_t vertexCount() const
    {
        return m_drawInfo.vertexCount;
    }

    /**
     * @brief copyFrom
     * @param P
     *
     * Copy the data from the host mesh primitive into the
     * DeviceMeshPrimtive. The BufferPool that this
     * DMP was allocated from must be host visible.
     *
     */
    void copyFrom(HostMeshPrimitive const & P);

    bool copyFrom(HostTriPrimitive const & P, bool copyToMapped=true);

    friend class BufferPool;
    friend class CommandBuffer;
};

}

#endif
