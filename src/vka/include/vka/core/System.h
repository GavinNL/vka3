#ifndef VKA_SYSTEM_H
#define VKA_SYSTEM_H

#include <vka/vka.h>
#include <vka/core/vka_vulkan.h>
#include <vector>
#include <map>
#include <vka/core/Global.h>
#include <vka/core/RenderPassInfo.h>
#include <vka/core/ScreenBuffer.h>
#include <vka/core/CommandBuffer.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/ScreenBufferCreateInfo.h>
#include <vka/core/RenderTarget.h>

namespace vka
{


class BufferPool;
class TexturePool;


struct SamplerCreateInfo2;

struct SystemCreateInfo
{
    std::vector<std::string> requiredExtensions;
    std::vector<std::string> requiredValidationLayers = std::vector<std::string>({
//                                                              "VK_LAYER_LUNARG_standard_validation"
                                                               });

    std::string applicationName = "Default";
    std::string engineName      = "Default";

    uint32_t engineVersionMajor = 0;
    uint32_t engineVersionMinor = 0;
    uint32_t engineVersionPatch = 0;

    uint32_t applicationVersionMajor = 0;
    uint32_t applicationVersionMinor = 0;
    uint32_t applicationVersionPatch = 0;

    PFN_vkDebugReportCallbackEXT debugCallbackFunction = nullptr;

    std::vector<std::string> requiredDeviceExtensions {
        VK_KHR_SWAPCHAIN_EXTENSION_NAME
    };
};


struct QFamily
{
    uint32_t graphics = std::numeric_limits<uint32_t>::max();
    uint32_t present  = std::numeric_limits<uint32_t>::max();
};

struct VulkanObjects
{
    vk::Instance        instance;
    vk::SurfaceKHR      surface;

    vk::PhysicalDevice  physicalDevice;
    QFamily             deviceQueueFamily;
    vk::Device          device;

    vk::DebugReportCallbackEXT debugCallback;


    SystemCreateInfo createInfo;

    /**
     * @brief destroy
     *
     * Destroys all the objects in their proper order
     */
    void destroy()
    {
        vka::destroyDebugCallback( instance, debugCallback );
        device.destroy();

        //instance.destroySurfaceKHR(surface);

        instance.destroy();

    }
};


struct physicalDeviceInfo
{
    vk::PhysicalDevice physicalDevice;
    QFamily            queueFamily;
};

class System
{
    protected:
        System()
        {
        }
    public:

        //====================================================================================================
        // Helper functions for creating vulkan objects
        //====================================================================================================
        static vk::Instance createInstance(SystemCreateInfo const & createInfo);


        /**
         * @brief createVulkanObjects
         * @param instance
         * @param surface
         * @param createInfo
         * @return
         *
         * Create all the vulkan objects that we need
         */
        static VulkanObjects createVulkanObjects(vk::Instance instance,
                                                 vk::SurfaceKHR surface,
                                                 SystemCreateInfo const & createInfo);

        static physicalDeviceInfo  createPhysicalDevice(vk::Instance instance, vk::SurfaceKHR surface, vk::ArrayProxy<const std::string> deviceExtensions);



        /**
         * @brief getSurfaceFormats
         * @param physical_device
         * @param surface
         * @return
         *
         * Returns the surface format for this given surface
         */
        static vk::SurfaceFormatKHR getSurfaceFormat(vk::PhysicalDevice physical_device, vk::SurfaceKHR surface);
        //====================================================================================================


        ~System();

        static bool createSystem(SystemCreateInfo2 createInfo);
        static System& get();
        //System(SystemCreateInfo2 createInfo);

        // temporary for now
        vk::SurfaceKHR m_surfaceTemp = vk::SurfaceKHR();


        static vk::PhysicalDevice physicalDevice()
        {
            return get().m_Vulkan.physicalDevice;
        }
        static vk::Device device()
        {
            return get().m_Vulkan.device;
        }
        static vk::Queue presentQueue()
        {
            return get().m_Vulkan.queuePresent;
        }
        static vk::Queue graphicsQueue()
        {
            return get().m_Vulkan.queueGraphics;
        }

        SystemCreateInfo2& getCreateInfo()
        {
            return m_Vulkan;
        }
        //====================================================================================================
        // VKA Specific Objects
        //====================================================================================================
        /**
         * @brief createScreenBuffer
         * @param extent
         * @return
         *
         * A ScreenBuffer consists of a swapchain, renderpass and frame buffer
         * which can be used to draw onto so that it may be presented to the
         * screen.
         */
        vk::SwapchainKHR createSwapchain(vk::SurfaceKHR surface, vk::Extent2D extent, std::optional<vk::SwapchainKHR> oldSwapchain);

        ScreenBuffer    createScreenBuffer(ScreenBufferCreateInfo const & createInfo, ScreenBuffer * oldScreenBuffer=nullptr);

        void            destroyScreenBuffer(ScreenBuffer &sb);



        /**
         * @brief createRenderTarget
         * @param I
         * @return
         *
         * A RenderTarget is used to render to a texture. For example it
         * can be used to implement differred shading.
         */
        RenderTarget    createRenderTarget(const RenderTargetCreateInfo & I);
        void            destroyRenderTarget(RenderTarget & I);


        //====================================================================================
        // Create Buffer Pool
        //====================================================================================
        /**
         * @brief createBufferPool
         * @param info
         * @param memoryProperties
         * @return
         *
         * Create a buffer pool which can be used to allocate subbuffers.
         * Sub buffers. All SubBuffers allocated from a bufferpool
         * share the same vk::Buffer, but at different offsets.
         * This should be the primary way of allocating data for meshs/uniforms/storages, etc
         */
        BufferPool createBufferPool(vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags memoryProperties );


        BufferPool createBufferPool(vk::BufferCreateInfo const & info, vk::MemoryPropertyFlags memoryProperties );


        void       destroyBufferPool(BufferPool const & p );

        /**
         * @brief createTexturePool
         * @param size
         * @param format
         * @param properties
         * @param usage
         * @return
         *
         * Create a texture pool to allocate textures from a pool of memory. TexturePools
         * can be used to allocate ImageViews. The Images that are created are automatically
         * linked to the imageview. Each of the images allocated from a TexturePool share the
         * same DeviceMemory at different offsets.
         */
        TexturePool createTexturePool(vk::DeviceSize size,
                                      vk::Format format,
                                      vk::MemoryPropertyFlags properties,
                                      vk::ImageUsageFlags usage);

        void       destroyTexturePool(TexturePool &p );


        /**
         * @brief createDefaultSampler
         * @param i
         * @return
         *
         * Create a default sampler for this particular image view. This is a quick
         * method to create a Sampler for an imageView
         */
        vk::Sampler getDefaultSampler(vk::ImageView i);







        vk::CommandPool   createCommandPool();




        /**
         * @brief allocateCommandBuffer
         * @param level
         * @param cp
         * @return
         *
         * Allocate a vka::CommandBuffer, which provides a few extra functions.
         */
        vka::CommandBuffer allocateCommandBuffer(vk::CommandBufferLevel level, vk::CommandPool cp);



        void submitCommandBuffers( vk::ArrayProxy<const vk::CommandBuffer> buffers,
                                   vk::ArrayProxy<const vk::Semaphore> waitSemaphores,
                                   vk::ArrayProxy<const vk::PipelineStageFlags> waitStages,
                                   vk::ArrayProxy<const vk::Semaphore> signalSemaphores
                                   );

        void submitCommandBuffers( vk::ArrayProxy<const vk::SubmitInfo> submits);



        //====================================================================================

        /**
         * @brief allocateMemoryForBuffer
         * @param buffer
         * @param properties
         * @return
         *
         * Allocate device memory for a buffer which was created earlier.
         * This does not automatically bind the buffer, you must call
         * bindBufferMemory( ) to properly bind the buffer to the memory
         */
        vk::DeviceMemory allocateMemoryForBuffer(vk::Buffer buffer, vk::MemoryPropertyFlags properties);


        /**
         * @brief allocateMemoryForImage
         * @param img
         * @param properties
         * @return
         *
         * Allocate device memory for an image which was created earlier.
         * This does not automatically bind the buffer, you must call
         * bindImage( ) to properly bind the buffer to the memory
         */
        vk::DeviceMemory allocateMemoryForImage(vk::Image img, vk::MemoryPropertyFlags properties);

        /**
         * @brief bindBufferMemory
         * @param buffer
         * @param memory
         * @param offset
         *
         * Binds the buffer to a piece of memory. This keeps track of which buffers are bound
         * to which memory objects
         */
        void bindBufferMemory(vk::Buffer buffer , vk::DeviceMemory memory, vk::DeviceSize offset=0);

        /**
         * @brief bindImageMemory
         * @param image
         * @param memory
         * @param offset
         *
         * Bind an image to a piece of memory. This keeps track of which images are bound to which
         * memory objects
         */
        void bindImageMemory(vk::Image image, vk::DeviceMemory memory, vk::DeviceSize offset);




        /**
         * @brief mapMemory
         * @param mem
         * @param offset
         * @return
         *
         * Maps the memory. This can be called multiple times as
         * it only maps the memory once. It maps the entire address space
         * and returns a pointer to the byte offset
         */
        void* mapMemory(vk::DeviceMemory mem, vk::DeviceSize offset=0);

        /**
         * @brief unmapMemory
         * @param mem
         * Unmaps the memory, all pointers to mapped memory will be
         * invalided. Best not to call this.
         */
        void  unmapMemory(vk::DeviceMemory mem);
        //====================================================================================




         //====================================================================================================
        // LEVEL 3 Creation Methods:
        //
        //  Level 3 creation methods are higher level methods to create vulkan objects. Much of the
        //  requirements are done for you, but it may not be the most configurable.
        //====================================================================================================


        vk::Pipeline createGraphicsPipeline(vka::GraphicsPipelineCreateInfo4   const & info);

        //====================================================================================================
        // LEVEL 2 Creation Methods:
        //
        //  Level 2 creation methods are helper methods to assist in creating Vulkan Objects.
        //====================================================================================================

        /**
         * @brief createDescriptorPool
         * @param I
         * @return
         *
         * Create a Descriptorool using DescriptorPoolCreateInfo2 struct.
         */
        vk::DescriptorPool      createDescriptorPool(vka::DescriptorPoolCreateInfo2 const & I);

        /**
         * @brief createDescriptorSetLayout
         * @param I
         * @return
         *
         * Helper function to simplify a DescriptorSetLayout.
         * create a descriptor set layout. This does not create new layouts if another
         * of the same CreateInfo exists.
         */
        vk::DescriptorSetLayout createDescriptorSetLayout(vka::DescriptorSetLayoutCreateInfo2 const & I);


        vk::PipelineLayout      createPipelineLayout( const vka::PipelineLayoutCreateInfo2 & info);

        vk::DescriptorSet       allocateDescriptorSet(vk::DescriptorPool pool, const DescriptorSetLayoutCreateInfo2 &layout);

        vk::DescriptorSet       allocateDescriptorSet(vk::DescriptorPool pool, vk::DescriptorSetLayout layout);


        void updateDescriptorSets( vk::ArrayProxy<const vk::WriteDescriptorSet> x);
        //====================================================================================================





        //====================================================================================
        // Shader Creation
        //====================================================================================

        /**
         * @brief compileGLSL
         * @param path
         * @return
         *
         * Compiles the GLSL file from the path into SPIRV code.
         * Currently this uses an external system call to
         * execute the glslValidator.
         */
        static std::vector<uint32_t> compileGLSL(const std::string & path);




        vk::ShaderModule createShaderModuleFromGLSL( const std::string & path);






        /**
         * @brief createPipelineLayoutFromShaders
         * @param vertexShader
         * @param fragmentShader
         * @return
         *
         * Create a PipelineLayout from precompiled shader modules. These modules must have been
         * built from System::createShaderModule( ).
         *
         * This method will look at the shader's reflection and construct the descriptorSetLayouts
         * and PiplineLayout automatically.
         */
        vk::PipelineLayout createPipelineLayoutFromShaders( vk::ShaderModule vertexShader, vk::ShaderModule fragmentShader);


        /**
         * @brief createVertexAttributeDescriptionFromShader
         * @param vertexShader
         * @return
         *
         * Given a compiled vertesShade created by System::createShaderModule() return
         * a vector of input attribute descriptions to use in the PiplineCreateInfo
         * struct. This assumes at binding == location. If this is different
         * you will have to change it.
         */
        std::vector<vk::VertexInputAttributeDescription> createPipelineVertexAttributeDescriptionFromShader(vk::ShaderModule vertexShader) const;




        vka::FrameBufferInfo const & info(vk::Framebuffer p)
        {
            return GLOBAL.framebuffersContainer.info(p);
        }
        vka::DescriptorSetLayoutInfo const & info(vk::DescriptorSetLayout p)
        {
            return GLOBAL.descriptorSetLayoutsContainer.info(p);
        }

        vka::DescriptorPoolInfo const & info(vk::DescriptorPool p)
        {
            return GLOBAL.descriptorPoolsContainer.info(p);
        }
        vka::DescriptorSetInfo const & info(vk::DescriptorSet p)
        {
            return GLOBAL.descriptorSetsContainer.info(p);
        }
        vka::ShaderModuleInfo const & info(vk::ShaderModule p)
        {
            return GLOBAL.shaderModulesContainer.info(p);
        }
        vka::PipelineInfo const & info(vk::Pipeline p)
        {
            return GLOBAL.pipelinesContainer.info(p);
        }
        vka::BufferInfo const & info(vk::Buffer p)
        {
            return GLOBAL.buffersContainer.info(p);
        }
        vka::DeviceMemoryInfo const & info(vk::DeviceMemory p)
        {
            return GLOBAL.memoryContainer.info(p);
        }
        vka::ImageInfo const & info(vk::Image p)
        {
            return GLOBAL.imagesContainer.info(p);
        }
        vka::ImageViewInfo const & info(vk::ImageView p)
        {
            return GLOBAL.imageViewsContainer.info(p);
        }
        vka::RenderPassInfo const & info(vk::RenderPass p)
        {
            return GLOBAL.renderPassContainer.info(p);
        }
        vka::SwapchainInfo const & info(vk::SwapchainKHR p)
        {
            return GLOBAL.swapchains.at(p);
        }

public:
        //===========================================================================
        // Default Object creation methods. These mimic the default
        // creation method by vulkan, but they also track
        // which objects refer to which so that when you call destroy( ) it does
        // not allow you destroy items that are currently in use.
        //============================================================================
        vk::Buffer                     createBuffer(  vk::BufferCreateInfo const & info);
        vk::RenderPass                 createRenderPass(vka::RenderPassCreateInfo2 const & CI);

        vk::Framebuffer                createFramebuffer(vka::FramebufferCreateInfo2 const & info);
        vk::CommandPool                createCommandPool(vk::CommandPoolCreateInfo const & info);
        vk::Image                      createImage(vk::ImageCreateInfo const & info);
        vk::ImageView                  createImageView(const vk::ImageViewCreateInfo &I);
        vk::Sampler                    createSampler(vka::SamplerCreateInfo2 const & I);
        std::vector<vk::CommandBuffer> allocateCommandBuffers(vk::CommandBufferAllocateInfo const & cp);


        vk::DescriptorSetLayout createDescriptorSetLayout(vk::DescriptorSetLayoutCreateInfo const & I);

        vk::ShaderModule        createShaderModule( vka::ShaderModuleCreateInfo2 const & createInfo);

        vk::Pipeline            createGraphicsPipeline(vka::GraphicsPipelineCreateInfo2 const & info);


        vk::DeviceMemory        allocateMemory( vk::MemoryAllocateInfo const & info);
        vk::Semaphore           createSemaphore(vk::SemaphoreCreateInfo const & info = vk::SemaphoreCreateInfo());

        void destroy();
        void destroySampler(vk::Sampler s);
        void destroyPipelineLayout(vk::PipelineLayout layout);
        void destroyDescriptorSetLayout(vk::DescriptorSetLayout l);
        void destroyDescriptorSet(vk::DescriptorSet p);
        void destroyDescriptorPool(vk::DescriptorPool p);
        void destroyPipeline(vk::Pipeline p, bool deleteShaderModules=false);
        void destroyShaderModule(vk::ShaderModule p);
        void destroyBuffer(vk::Buffer s);
        void destroySemaphore(vk::Semaphore s);
        void destroyCommandPool(vk::CommandPool rp);
        void destroyFramebuffer(vk::Framebuffer rp);
        void destroyRenderPass(vk::RenderPass rp);
        void destroySwapchain(vk::SwapchainKHR sw);
        void destroyImageView(vk::ImageView view);
        void destroyImage(vk::Image img);
        void freeMemory( vk::DeviceMemory mem);
        void freeCommandBuffers( vk::CommandPool commandPool, vk::ArrayProxy<const vk::CommandBuffer> commandBuffers );
        //========================================================================
protected:
        template<typename owner_t, typename obj_t>
        void link(owner_t owner, obj_t obj);
        template<typename owner_t, typename obj_t>
        void unlink(owner_t owner, obj_t obj);

        static bool checkValidationLayerSupport(const std::vector<std::string> & requiredValidationLayers);
        /**
         * @brief createRenderPassFromFragmentOutputs
         * @param output
         * @param depth_format
         * @return
         *
         * This is mostly used by the createPipeline(vka::GraphicsPipelineCreateInfo3) method.
         * It takes the output information from the fragment shader's reflection
         * and creates a simple renderpass for it with 1 subpass.
         */
        vk::RenderPass _createRenderPassFromFragmentOutputs(const std::vector<VertexInput_t> &output, vk::Format depth_format=vk::Format::eUndefined);


    private:
        SystemCreateInfo2 m_Vulkan;
};


}

#endif 

