#ifndef VKA_SAMPLER_CREATE_INFO2_H
#define VKA_SAMPLER_CREATE_INFO2_H

#include <vulkan/vulkan.hpp>
#include <functional>
#include <vka/utils/hash.h>
#include "DescriptorSets.h"

namespace vka
{

struct SamplerCreateInfo2 : public vk::SamplerCreateInfo
{
    SamplerCreateInfo2() : vk::SamplerCreateInfo()
    {}
    SamplerCreateInfo2(vk::SamplerCreateInfo const & I) : vk::SamplerCreateInfo(I)
    {}
};

}


namespace std
{
    template<>
    struct hash<vka::SamplerCreateInfo2>
    {
        std::size_t operator()(vka::SamplerCreateInfo2 const& SamplerInfo) const noexcept
        {
            //std::hash<size_t> H;

            size_t hash1 = std::hash<uint32_t>()(static_cast<uint32_t>(SamplerInfo.magFilter   ));

            hash1 = vka::hashCombine_t(hash1, static_cast<uint32_t>(SamplerInfo.magFilter   )            ); //= vk::Filter::eLinear;// VK_FILTER_LINEAR;
            hash1 = vka::hashCombine_t(hash1, static_cast<uint32_t>(SamplerInfo.minFilter   )            ); //= vk::Filter::eLinear;// VK_FILTER_LINEAR;
            hash1 = vka::hashCombine_t(hash1, static_cast<uint32_t>(SamplerInfo.addressModeU)            ); //= vk::SamplerAddressMode::eRepeat;//VK_SAMPLER_ADDRESS_MODE_REPEAT;
            hash1 = vka::hashCombine_t(hash1, static_cast<uint32_t>(SamplerInfo.addressModeV)            ); //= vk::SamplerAddressMode::eRepeat;//VK_SAMPLER_ADDRESS_MODE_REPEAT;
            hash1 = vka::hashCombine_t(hash1, static_cast<uint32_t>(SamplerInfo.addressModeW)            ); //= vk::SamplerAddressMode::eRepeat;//VK_SAMPLER_ADDRESS_MODE_REPEAT;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.anisotropyEnable        );                        // = VK_FALSE;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.maxAnisotropy           );             // = 1;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.borderColor             );                        //= vk::BorderColor::eIntOpaqueBlack;// VK_BORDER_COLOR_INT_OPAQUE_BLACK ;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.unnormalizedCoordinates );                        //= VK_FALSE;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.compareEnable           );                        //= VK_FALSE;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.compareOp               );                        //= vk::CompareOp::eAlways;// VK_COMPARE_OP_ALWAYS;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.mipmapMode              );                        //= vk::SamplerMipmapMode::eLinear;// VK_SAMPLER_MIPMAP_MODE_LINEAR;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.mipLodBias              );                        //= 0.0f;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.minLod                  );                        //= 0.0f;
            hash1 = vka::hashCombine_t(hash1, SamplerInfo.maxLod                  );                        //= getMipLevels();

            return hash1;
        }
    };
}


#endif 

