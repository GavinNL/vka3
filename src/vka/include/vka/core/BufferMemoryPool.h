#ifndef VKA_BUFFER_MEMORY_POOL_H
#define VKA_BUFFER_MEMORY_POOL_H


#include <vulkan/vulkan.hpp>
#include "allocator.h"


namespace vka
{

class DeviceMeshPrimitive;
class HostMeshPrimitive;

/**
 * @brief The SubBuffer struct
 *
 * A SubBuffer is allocated from a BufferPool.
 * A BufferPool is essentially just a very large vk::Buffer
 * and SubBuffers are offets into them.
 */
class SubBuffer
{
private:
    vk::Buffer     bufferPool; // the bufferPool this SubBuffer was allocated from
    vk::DeviceSize byteOffset; // b
public:
    vk::Buffer getParentBuffer() const
    {
        return bufferPool;
    }

    /**
     * @brief free
     *
     * Frees the subbuffer, so that it's memory can be used again.
     */
    void free();


    /**
     * @brief getOffset
     * @return
     *
     * Returns the offset from the start of the memory region in the
     * parent buffer where this sub buffer resides
     */
    vk::DeviceSize getOffset() const
    {
        return byteOffset;
    }

    /**
     * @brief getSize
     * @return
     *
     * Returns the size of the SubBuffer
     */
    vk::DeviceSize getSize() const;



    /**
     * @brief getMappedMemory
     * @return
     *
     * Return he mapped memory region for this
     * SubBuffer.
     */
    void* getMappedMemory();


    void copyData(const void* src, vk::DeviceSize size, vk::DeviceSize offset=0);

    friend class BufferPool;
};


/**
 * @brief The BufferPool struct
 *
 * A BufferPool is used to allocate SubBuffers to use
 * as vertex attributes or uniform buffers.
 *
 * A BufferPool is simply a vk::Buffer that is large and
 * has some inner bookkeeping to know where SubBuffers
 * are allocated.
 */
class BufferPool
{
public:
    vk::Buffer       m_buffer;


    /**
     * @brief clear
     *
     * Clears the buffer pool. All SubBuffers allocated from this
     * are invalidated.
     */
    void clear();

    /**
     * @brief allocate
     * @param size
     * @return
     *
     * Allocate a subbuffer from the pool. The subbuffer
     * is simple a reference to vk::Buffer and a byte offset
     * into the
     */
    SubBuffer allocate(vk::DeviceSize size);


    /**
     * @brief allocate
     * @param P
     * @return
     *
     * Allocate a DeviceMeshPrimitive
     */
    DeviceMeshPrimitive allocateDeviceMeshPrimitive(vk::DeviceSize bytes);

    /**
     * @brief free
     * @param S
     *
     * Frees a subbuffer
     */
    void free(SubBuffer & S);
    void free(DeviceMeshPrimitive & S);

    /**
     * @brief canAllocate
     * @param size
     * @return
     *
     * Returns true if there is enouge space to allocate
     */
    bool canAllocate(vk::DeviceSize size) const;


};


}


#endif
