#ifndef VKA_SHADER_MODULE_CREATE_INFO_H
#define VKA_SHADER_MODULE_CREATE_INFO_H

#include <vulkan/vulkan.hpp>
#include <vector>
#include <vka/utils/hash.h>

namespace vka
{

struct ShaderModuleCreateInfo2
{
    std::vector<uint32_t>             code;  // SPIR-V code

    ShaderModuleCreateInfo2( )
    {
    }

    vk::ShaderModuleCreateInfo create() const
    {
        vk::ShaderModuleCreateInfo c;

        c.pCode = code.data();
        c.codeSize = code.size() * sizeof(uint32_t);

        return c;
    }
};


}

namespace std
{
    template<>
    struct hash<vka::ShaderModuleCreateInfo2>
    {
        std::size_t operator()(vka::ShaderModuleCreateInfo2 const& s) const noexcept
        {
            std::hash<uint32_t> H;

            size_t h = H( static_cast<uint32_t>(s.code.size()) );

            for(auto & b : s.code)
            {
                h = vka::hashCombine(h, H( b ) );
            }

            return h;
        }
    };
}

#endif
