#pragma once
#ifndef VKA_PRIMATIVES_H
#define VKA_PRIMATIVES_H

//#include "HostTriMesh.h"
//#include <vka/deprecated/HostMeshPrimitive.h>
#include <iostream>
#include <cmath>

namespace vka
{
#if 0
inline HostMeshPrimitive boxPrimitive(float dx , float dy , float dz )
{
    using vec2 = std::array<float,2>;
    using vec3 = std::array<float,3>;

    HostMeshPrimitive M;

    M.lowerBound = {-dx*0.5f,-dy*0.5f,-dz*0.5f};
    M.upperBound = { dx*0.5f, dy*0.5f, dz*0.5f};

    M.setAttribute(PrimitiveAttribute::POSITION   , AccessorType::eVec3, ComponentType::eFloat);
    M.setAttribute(PrimitiveAttribute::NORMAL     , AccessorType::eVec3, ComponentType::eFloat);
    M.setAttribute(PrimitiveAttribute::TEXCOORD_0 , AccessorType::eVec2, ComponentType::eFloat);

    M.setIndices(AccessorType::eVec1, ComponentType::eUnsignedShort);

    //auto & P = M.getAttribute(ModelVertexAttribute::ePosition);
    //auto & U = M.getAttribute(ModelVertexAttribute::eTexCoord_0      );
    //auto & N = M.getAttribute(ModelVertexAttribute::eNormal  );
    //auto & I = M.getAttribute(ModelVertexAttribute::eIndex  );


    auto & P = M.getAttribute( PrimitiveAttribute::POSITION   );
    auto & N = M.getAttribute( PrimitiveAttribute::NORMAL     );
    auto & U = M.getAttribute( PrimitiveAttribute::TEXCOORD_0 );
    auto & I = M.indices;


//       |       Position                           |   UV         |     Normal    |
        P.push_back( vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{0.0f,0.0f}) ; N.push_back( vec3{0.0f,  0.0f,  1.0f}) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,0.0f}) ; N.push_back( vec3{0.0f,  0.0f,  1.0f}) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,1.0f}) ; N.push_back( vec3{0.0f,  0.0f,  1.0f}) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{0.0f,0.0f}) ; N.push_back( vec3{0.0f,  0.0f,  1.0f}) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,1.0f}) ; N.push_back( vec3{0.0f,  0.0f,  1.0f}) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{0.0f,1.0f}) ; N.push_back( vec3{0.0f,  0.0f,  1.0f}) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,1.0f}) ; N.push_back( vec3{0.0f,  0.0f, -1.0f}) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{1.0f,1.0f}) ; N.push_back( vec3{0.0f,  0.0f, -1.0f}) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{1.0f,0.0f}) ; N.push_back( vec3{0.0f,  0.0f, -1.0f}) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,1.0f}) ; N.push_back( vec3{0.0f,  0.0f, -1.0f}) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{1.0f,0.0f}) ; N.push_back( vec3{0.0f,  0.0f, -1.0f}) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,0.0f}) ; N.push_back( vec3{0.0f,  0.0f, -1.0f}) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,0.0f}) ; N.push_back( vec3{-1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,0.0f}) ; N.push_back( vec3{-1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,1.0f}) ; N.push_back( vec3{-1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,0.0f}) ; N.push_back( vec3{-1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,1.0f}) ; N.push_back( vec3{-1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,1.0f}) ; N.push_back( vec3{-1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,1.0f}) ; N.push_back( vec3{1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,1.0f}) ; N.push_back( vec3{1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,0.0f}) ; N.push_back( vec3{1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,1.0f}) ; N.push_back( vec3{1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,0.0f}) ; N.push_back( vec3{1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,0.0f}) ; N.push_back( vec3{1.0f, 0.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,0.0f}) ; N.push_back( vec3{0.0f,-1.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{1.0f,0.0f}) ; N.push_back( vec3{0.0f,-1.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,1.0f}) ; N.push_back( vec3{0.0f,-1.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,0.0f}) ; N.push_back( vec3{0.0f,-1.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,1.0f}) ; N.push_back( vec3{0.0f,-1.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{0.0f,1.0f}) ; N.push_back( vec3{0.0f,-1.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{0.0f,1.0f}) ; N.push_back( vec3{0.0f, 1.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{1.0f,1.0f}) ; N.push_back( vec3{0.0f, 1.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{1.0f,0.0f}) ; N.push_back( vec3{0.0f, 1.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( vec2{0.0f,1.0f}) ; N.push_back( vec3{0.0f, 1.0f,  0.0f }) ;
        P.push_back( vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{1.0f,0.0f}) ; N.push_back( vec3{0.0f, 1.0f,  0.0f }) ;
        P.push_back( vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( vec2{0.0f,0.0f}) ; N.push_back( vec3{0.0f, 1.0f,  0.0f }) ;

    //=========================
    // Edges of the triangle : postion delta


    //=========================
    for( uint16_t j=0;j<36;j++)
        I.push_back( static_cast<uint16_t>(j) );

    vka::PrimitiveDrawCall dc;
    dc.indexCount   = 36;
    dc.vertexOffset = 0;
    dc.firstIndex   = 0;
    dc.vertexCount  = 36;
    M.drawCalls.push_back(dc);

    return M;

}


inline HostMeshPrimitive spherePrimitive(float radius , uint32_t rings=20, uint32_t sectors=20)
{
    using vec2 = std::array<float,2>;
    using vec3 = std::array<float,3>;

    HostMeshPrimitive M;
    M.lowerBound = {-radius, -radius, -radius};
    M.upperBound = { radius, radius, radius};

    M.setAttribute(PrimitiveAttribute::POSITION   , AccessorType::eVec3, ComponentType::eFloat);
    M.setAttribute(PrimitiveAttribute::NORMAL     , AccessorType::eVec3, ComponentType::eFloat);
    M.setAttribute(PrimitiveAttribute::TEXCOORD_0 , AccessorType::eVec2, ComponentType::eFloat);

    M.setIndices(AccessorType::eVec1, ComponentType::eUnsignedShort);

    //auto & P = M.getAttribute(ModelVertexAttribute::ePosition);
    //auto & U = M.getAttribute(ModelVertexAttribute::eTexCoord_0      );
    //auto & N = M.getAttribute(ModelVertexAttribute::eNormal  );
    //auto & I = M.getAttribute(ModelVertexAttribute::eIndex  );


    auto & P = M.getAttribute( PrimitiveAttribute::POSITION   );
    auto & N = M.getAttribute( PrimitiveAttribute::NORMAL     );
    auto & U = M.getAttribute( PrimitiveAttribute::TEXCOORD_0 );
    auto & I = M.indices;


    float const R = 1.0f / static_cast<float>(rings-1);
    float const S = 1.0f / static_cast<float>(sectors-1);
    unsigned int r, s;


    for(r = 0; r < rings; r++)
    {
        for(s = 0; s < sectors; s++)
        {
            float const y = std::sin( -3.141592653589f*0.5f + 3.141592653589f * r * R );
            float const x = std::cos(2*3.141592653589f * s * S) * std::sin( 3.141592653589f * r * R );
            float const z = std::sin(2*3.141592653589f * s * S) * std::sin( 3.141592653589f * r * R );

            P.push_back( vec3{ radius*x ,radius*y ,radius*z} );
            U.push_back( vec2{s*S, r*R} );
            N.push_back( vec3{x,y,z} );
        }
    }


    for(r = 0 ; r < rings   - 1 ; r++)
    {
        for(s = 0 ; s < sectors - 1 ; s++)
        {
            I.push_back(  static_cast<uint16_t>( (r+1) * sectors + s) ); //0
            I.push_back(  static_cast<uint16_t>( (r+1) * sectors + (s+1) ) ); //1
            I.push_back(  static_cast<uint16_t>(  r * sectors + (s+1) )); //2
            I.push_back(  static_cast<uint16_t>( (r+1) * sectors + s )); //0
            I.push_back(  static_cast<uint16_t>(  r * sectors + (s+1) )); //2
            I.push_back(  static_cast<uint16_t>(   r * sectors + s )); //3
        }
    }

    vka::PrimitiveDrawCall dc;
    dc.indexCount   = I.count();
    dc.vertexOffset = 0;
    dc.firstIndex   = 0;
    dc.vertexCount  = P.count();
    M.drawCalls.push_back(dc);

    return M;
}


inline HostModel sphere_mesh(float radius , uint32_t rings=20, uint32_t sectors=20)
{
    using namespace vka2;
    using vec2 = std::array<float,2>;
    using vec3 = std::array<float,3>;

    HostModel M;

    M.setFormat(ModelVertexAttribute::ePosition  , AccessorType::eVec3, ComponentType::eFloat);
    M.setFormat(ModelVertexAttribute::eNormal    , AccessorType::eVec3, ComponentType::eFloat);
    M.setFormat(ModelVertexAttribute::eTexCoord_0, AccessorType::eVec2, ComponentType::eFloat);

    M.setFormat(ModelVertexAttribute::eIndex,   AccessorType::eVec1, ComponentType::eUnsignedShort );

    auto & P = M.getAttribute(ModelVertexAttribute::ePosition);
    auto & U = M.getAttribute(ModelVertexAttribute::eTexCoord_0      );
    auto & N = M.getAttribute(ModelVertexAttribute::eNormal  );
    auto & I = M.getAttribute(ModelVertexAttribute::eIndex  );



    float const R = 1.0f / static_cast<float>(rings-1);
    float const S = 1.0f / static_cast<float>(sectors-1);
    unsigned int r, s;


    for(r = 0; r < rings; r++)
    {
        for(s = 0; s < sectors; s++)
        {
            float const y = std::sin( -3.141592653589f*0.5f + 3.141592653589f * r * R );
            float const x = std::cos(2*3.141592653589f * s * S) * std::sin( 3.141592653589f * r * R );
            float const z = std::sin(2*3.141592653589f * s * S) * std::sin( 3.141592653589f * r * R );

            P.push_back( vec3{ radius*x ,radius*y ,radius*z} );
            U.push_back( vec2{s*S, r*R} );
            N.push_back( vec3{x,y,z} );
        }
    }


    for(r = 0 ; r < rings   - 1 ; r++)
    {
        for(s = 0 ; s < sectors - 1 ; s++)
        {
            I.push_back(  static_cast<uint16_t>( (r+1) * sectors + s) ); //0
            I.push_back(  static_cast<uint16_t>( (r+1) * sectors + (s+1) ) ); //1
            I.push_back(  static_cast<uint16_t>(  r * sectors + (s+1) )); //2
            I.push_back(  static_cast<uint16_t>( (r+1) * sectors + s )); //0
            I.push_back(  static_cast<uint16_t>(  r * sectors + (s+1) )); //2
            I.push_back(  static_cast<uint16_t>(   r * sectors + s )); //3
        }
    }


    //=========================


    return M;
}

inline HostModel grid_mesh(int32_t N = 10)
{
    using vec2 = std::array<float,2>;
    using vec3 = std::array<float,3>;

    HostModel M;

    M.setFormat(ModelVertexAttribute::ePosition  , AccessorType::eVec3, ComponentType::eFloat);
    M.setFormat(ModelVertexAttribute::eColor_0   , AccessorType::eVec3, ComponentType::eFloat);

    auto & P = M.getAttribute(ModelVertexAttribute::ePosition);
    auto & C = M.getAttribute(ModelVertexAttribute::eColor_0 );

    vec3 grey       = {0.2,0.2,0.2};
    vec3 light_grey = {0.5,0.5,0.5};

    // draw a grid of lines. every 5th line will be light grey
    for(int i=-N; i <= N;i++)
    {
        //if(i!=0)
        {
            P.push_back( vec3{-(float)N, 0, (float)i} );
            P.push_back( vec3{ (float)N, 0, (float)i} );

            P.push_back( vec3{ (float)i, 0, -(float)N} );
            P.push_back( vec3{ (float)i, 0,  (float)N} );

            if(i%5==0)
            {
                C.push_back( light_grey );
                C.push_back( light_grey );
                C.push_back( light_grey );
                C.push_back( light_grey );
            } else {
                C.push_back( grey );
                C.push_back( grey );
                C.push_back( grey );
                C.push_back( grey );
            }
        }
    }

    return M;
}

inline HostModel axis_mesh()
{
    using vec2 = std::array<float,2>;
    using vec3 = std::array<float,3>;

    HostModel M;

    M.setFormat(ModelVertexAttribute::ePosition  , AccessorType::eVec3, ComponentType::eFloat);
    M.setFormat(ModelVertexAttribute::eColor_0   , AccessorType::eVec3, ComponentType::eFloat);

    auto & P = M.getAttribute(ModelVertexAttribute::ePosition);
    auto & C = M.getAttribute(ModelVertexAttribute::eColor_0 );

    vec3 grey       = {0.2,0.2,0.2};
    vec3 light_grey = {0.5,0.5,0.5};

    // draw a grid of lines. every 5th line will be light grey

    P.push_back( vec3{0.0f, 0.0f, 0.0f} );
    P.push_back( vec3{5.0f, 0.0f, 0.0f} );

    P.push_back( vec3{0.0f, 0.0f, 0.0f} );
    P.push_back( vec3{0.0f, 5.0f, 0.0f} );

    P.push_back( vec3{0.0f, 0.0f, 0.0f} );
    P.push_back( vec3{0.0f, 0.0f, 5.0f} );

    C.push_back( vec3{1.0f,0.0f,0.0f} );
    C.push_back( vec3{1.0f,0.0f,0.0f} );

    C.push_back( vec3{0.0f,1.0f,0.0f} );
    C.push_back( vec3{0.0f,1.0f,0.0f} );

    C.push_back( vec3{0.0f,0.0f,1.0f} );
    C.push_back( vec3{0.0f,0.0f,1.0f} );

    return M;
}


inline HostMeshPrimitive planePrimitive(uint32_t Nx, uint32_t Nz, float sx=1.0f, float sz=1.0f)
{
    using vec2 = std::array<float,2>;
    using vec3 = std::array<float,3>;

    HostMeshPrimitive M;

    M[ vka2::PrimitiveAttribute::POSITION ].type          = AccessorType::eVec3;
    M[ vka2::PrimitiveAttribute::POSITION ].componentType = ComponentType::eFloat;

    M[ vka2::PrimitiveAttribute::NORMAL ].type          = AccessorType::eVec3;
    M[ vka2::PrimitiveAttribute::NORMAL ].componentType = ComponentType::eFloat;

    M[ vka2::PrimitiveAttribute::TEXCOORD_0 ].type          = AccessorType::eVec2;
    M[ vka2::PrimitiveAttribute::TEXCOORD_0 ].componentType = ComponentType::eFloat;

    M.indices.type          = AccessorType::eVec1;
    M.indices.componentType = ComponentType::eUnsignedShort;


    auto & P = M[ vka2::PrimitiveAttribute::POSITION ];
    auto & N = M[ vka2::PrimitiveAttribute::NORMAL ];
    auto & U = M[ vka2::PrimitiveAttribute::TEXCOORD_0 ];
    auto & I = M.indices;


    const float Xm = Nx / 2.0f;
    const float Zm = Nz / 2.0f;
    for(uint32_t z=0 ;z <= Nz ; z++)
    {
        for(uint32_t x=0 ; x <= Nx ; x++)
        {
            P.push_back( vec3{  (x-Xm)*sx   ,  0  , (z-Zm) * sz} );
            N.push_back( vec3{0,1,0});
            U.push_back( vec2{static_cast<float>(x), static_cast<float>(z) } );
        }
    }

    for(uint32_t z=0 ;z < Nz ; z++)
    {
        for(uint32_t x=0 ; x < Nx ; x++)
        {
            #define to_index(x,z) ((z)*(Nx+1)+(x))

            I.push_back( static_cast<uint16_t>( to_index(x+1,z+1)));
            I.push_back( static_cast<uint16_t>( to_index(x+1,z)));
            I.push_back( static_cast<uint16_t>( to_index(x,z)));
            I.push_back( static_cast<uint16_t>( to_index(x , z+1)));
            I.push_back( static_cast<uint16_t>( to_index(x+1 , z+1)));
            I.push_back( static_cast<uint16_t>( to_index(x , z)));
        }

    }

    return M;

}


inline HostMeshPrimitive cylinderPrimitive(float R=1.0f, float H=1.0f, uint32_t rSegments=16, uint32_t hSegments=1)
{
    using vec2 = std::array<float,2>;
    using vec3 = std::array<float,3>;

    HostMeshPrimitive M;

    M[ vka2::PrimitiveAttribute::POSITION ].type          = AccessorType::eVec3;
    M[ vka2::PrimitiveAttribute::POSITION ].componentType = ComponentType::eFloat;

    M[ vka2::PrimitiveAttribute::NORMAL ].type          = AccessorType::eVec3;
    M[ vka2::PrimitiveAttribute::NORMAL ].componentType = ComponentType::eFloat;

    M[ vka2::PrimitiveAttribute::TEXCOORD_0 ].type          = AccessorType::eVec2;
    M[ vka2::PrimitiveAttribute::TEXCOORD_0 ].componentType = ComponentType::eFloat;

    M.indices.type          = AccessorType::eVec1;
    M.indices.componentType = ComponentType::eUnsignedShort;


    auto & P = M[ vka2::PrimitiveAttribute::POSITION ];
    auto & N = M[ vka2::PrimitiveAttribute::NORMAL ];
    auto & U = M[ vka2::PrimitiveAttribute::TEXCOORD_0 ];
    auto & I = M.indices;


    float dt = 2.0f * 3.141592653589f / static_cast<float>(rSegments);

    float t = 0;
    if(1)
    {
        for(uint32_t r=0 ; r<rSegments; r++)
        {
            vec3 p{  R*std::cos(t) ,  R * std::sin(t),  0 };
            t += dt;
            P.push_back(p);
            N.push_back( vec3{ std::cos(t), std::sin(t), 0.f } );
            U.push_back( vec2{ t, 0} );
        }

        for(uint32_t r=0 ; r<rSegments; r++)
        {
            vec3 p{  R*std::cos(t) ,  R * std::sin(t),  H };
            t += dt;
            P.push_back(p);
            N.push_back( vec3{ std::cos(t), std::sin(t), 0.f } );
            U.push_back( vec2{ t, 1} );
        }


        for(uint32_t i=0 ; i < rSegments; ++i)
        {
            const uint32_t a = (i + 0) % rSegments;
            const uint32_t b = (i + 1) % rSegments;
            const uint32_t c = b + rSegments;

            const uint32_t d = a + rSegments;

            I.push_back( static_cast<uint16_t>(a) );
            I.push_back( static_cast<uint16_t>(b) );
            I.push_back( static_cast<uint16_t>(c) );

            I.push_back( static_cast<uint16_t>(a) );
            I.push_back( static_cast<uint16_t>(c) );
            I.push_back( static_cast<uint16_t>(d) );

            //std::cout << a << ", " << b << ", " << c << std::endl;
            //std::cout << a << ", " << c << ", " << d << std::endl;
        }
    }


    if(1){ // top cap

        HostMeshPrimitive M2;
        M2[ vka2::PrimitiveAttribute::POSITION ].type          = AccessorType::eVec3;
        M2[ vka2::PrimitiveAttribute::POSITION ].componentType = ComponentType::eFloat;
        M2[ vka2::PrimitiveAttribute::NORMAL ].type          = AccessorType::eVec3;
        M2[ vka2::PrimitiveAttribute::NORMAL ].componentType = ComponentType::eFloat;
        M2[ vka2::PrimitiveAttribute::TEXCOORD_0 ].type          = AccessorType::eVec2;
        M2[ vka2::PrimitiveAttribute::TEXCOORD_0 ].componentType = ComponentType::eFloat;

        M2.indices.type          = AccessorType::eVec1;
        M2.indices.componentType = ComponentType::eUnsignedShort;

        auto & P2 = M2[ vka2::PrimitiveAttribute::POSITION ];
        auto & N2 = M2[ vka2::PrimitiveAttribute::NORMAL ];
        auto & U2 = M2[ vka2::PrimitiveAttribute::TEXCOORD_0 ];
        auto & I2 = M2.indices;


        t = 0;
        P2.push_back( vec3{ 0.f, 0.f, H});
        N2.push_back( vec3{ 0.f, 0.f, 1.f } );
        U2.push_back( vec2{ 0.5f, 0.5f } );

        for(uint32_t r=0 ; r < rSegments; r++)
        {
            vec3 p{  R * std::cos(t) ,  R * std::sin(t),  H };
            t += dt;
            P2.push_back(p);
            N2.push_back( vec3{ 0.f, 0.f, 1.f } );
            U2.push_back( vec2{ 0.5f+std::cos(t), 0.5f+std::sin(t)} );

            const uint16_t A = 0;
            const uint16_t B = r+1;
            const uint16_t C = (r+1)%rSegments+1;

            I2.push_back( static_cast<uint16_t>(A) );
            I2.push_back( static_cast<uint16_t>(B) );
            I2.push_back( static_cast<uint16_t>(C) );

            std::cout << A << ", " << B << ", " << C << std::endl;
        }

        M.append(M2);
    }

    if(1){ // bottom cap

        HostMeshPrimitive M2;
        M2[ vka2::PrimitiveAttribute::POSITION ].type          = AccessorType::eVec3;
        M2[ vka2::PrimitiveAttribute::POSITION ].componentType = ComponentType::eFloat;
        M2[ vka2::PrimitiveAttribute::NORMAL ].type          = AccessorType::eVec3;
        M2[ vka2::PrimitiveAttribute::NORMAL ].componentType = ComponentType::eFloat;
        M2[ vka2::PrimitiveAttribute::TEXCOORD_0 ].type          = AccessorType::eVec2;
        M2[ vka2::PrimitiveAttribute::TEXCOORD_0 ].componentType = ComponentType::eFloat;

        M2.indices.type          = AccessorType::eVec1;
        M2.indices.componentType = ComponentType::eUnsignedShort;

        auto & P2 = M2[ vka2::PrimitiveAttribute::POSITION ];
        auto & N2 = M2[ vka2::PrimitiveAttribute::NORMAL ];
        auto & U2 = M2[ vka2::PrimitiveAttribute::TEXCOORD_0 ];
        auto & I2 = M2.indices;


        t = 0;
        P2.push_back( vec3{ 0.f, 0.f, 0});
        N2.push_back( vec3{ 0.f, 0.f, 1.f } );
        U2.push_back( vec2{ 0.5f, 0.5f } );

        for(uint32_t r=0 ; r < rSegments; r++)
        {
            vec3 p{  R * std::cos(t) ,  R * std::sin(t),  0 };
            t += dt;
            P2.push_back(p);
            N2.push_back( vec3{ 0.f, 0.f, 1.f } );
            U2.push_back( vec2{ 0.5f+std::cos(t), 0.5f+std::sin(t)} );

            const uint16_t A = 0;
            const uint16_t B = (r+1)%rSegments+1;
            const uint16_t C = r+1;

            I2.push_back( static_cast<uint16_t>(A) );
            I2.push_back( static_cast<uint16_t>(B) );
            I2.push_back( static_cast<uint16_t>(C) );

            std::cout << A << ", " << B << ", " << C << std::endl;
        }

        M.append(M2);
    }
    return M;

}

#endif

}

#endif

