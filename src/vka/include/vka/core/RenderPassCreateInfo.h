#ifndef VKA_RENDERPASS_CREATE_INFO
#define VKA_RENDERPASS_CREATE_INFO

#include <vulkan/vulkan.hpp>
#include <vector>
#include <vka/utils/hash.h>

namespace vka
{

/**
 * @brief The RenderPassCreateInfo2 struct
 *
 * This is a helper class which can be used
 * to generate a simple renderpass with
 * only a single subpass dependency.
 *
 *
 */
struct RenderPassCreateInfo2
{
    std::vector<vk::AttachmentDescription> attachmentDescription;
    std::vector<vk::AttachmentReference>   colorReferences;

    RenderPassCreateInfo2()
    {
        attachmentDescription.reserve(10);
        subpassDependencies.reserve(10);
        colorReferences.reserve(10);

        //============== Default ===========================
        // only create one dependency.
        subpassDependencies.resize(2);
        // First dependency at the start of the renderpass
        // Does the transition from final to initial layout
        subpassDependencies[0].srcSubpass    = VK_SUBPASS_EXTERNAL;								// Producer of the dependency
        subpassDependencies[0].dstSubpass    = 0;													// Consumer is our single subpass that will wait for the execution depdendency
        subpassDependencies[0].srcStageMask  = vk::PipelineStageFlagBits::eBottomOfPipe;// VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        subpassDependencies[0].dstStageMask  = vk::PipelineStageFlagBits::eColorAttachmentOutput;// VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpassDependencies[0].srcAccessMask = vk::AccessFlagBits::eMemoryRead;// VK_ACCESS_MEMORY_READ_BIT;
        subpassDependencies[0].dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;// VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
        subpassDependencies[0].dependencyFlags = vk::DependencyFlagBits::eByRegion;// VK_DEPENDENCY_BY_REGION_BIT;

        // Second dependency at the end the renderpass
        // Does the transition from the initial to the final layout
        subpassDependencies[1].srcSubpass      = 0;													// Producer of the dependency is our single subpass
        subpassDependencies[1].dstSubpass      = VK_SUBPASS_EXTERNAL;								// Consumer are all commands outside of the renderpass
        subpassDependencies[1].srcStageMask    = vk::PipelineStageFlagBits::eColorAttachmentOutput;//VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
        subpassDependencies[1].dstStageMask    = vk::PipelineStageFlagBits::eBottomOfPipe;//VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
        subpassDependencies[1].srcAccessMask   = vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite;
        subpassDependencies[1].dstAccessMask   = vk::AccessFlagBits::eMemoryRead;
        subpassDependencies[1].dependencyFlags = vk::DependencyFlagBits::eByRegion;

    }

    /**
     * @brief addColorAttachment
     * @param format
     * @param finalLayout
     * @return
     *
     * Add a color attachment to the renderpass. the colour
     * attachment indicates how many targets you can render to
     * in the fragment shader. If this renderpass is to be used
     * for rendering to the swapchain, the finalLayout should be
     * vk::ImageLayout::ePresentSrcKHR.
     * If this renderpass is to be used for an offscreen rendertarget
     * it should be set to vk::imageLayout::eShaderReadOnlyOptimal
     */
    size_t addColorAttachment( vk::Format format,
                               vk::ImageLayout finalLayout)
    {
        if( attachmentDescription.size() )
        {
            if( colorReferences.back().layout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
            {
                throw std::runtime_error("Cannot add anymore colour attachments since a depth attachment was already added. Add the depth attachment last");
            }
        }
        vk::AttachmentDescription a;
        // Color attachment
        a.format         = format;
        a.samples        = vk::SampleCountFlagBits::e1;//VK_SAMPLE_COUNT_1_BIT;									// We don't use multi sampling in this example
        a.loadOp         = vk::AttachmentLoadOp::eClear;//VK_ATTACHMENT_LOAD_OP_CLEAR;							// Clear this attachment at the start of the render pass
        a.storeOp        = vk::AttachmentStoreOp::eStore;//VK_ATTACHMENT_STORE_OP_STORE;							// Keep it's contents after the render pass is finished (for displaying it)
        a.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;//VK_ATTACHMENT_LOAD_OP_DONT_CARE;					// We don't use stencil, so don't care for load
        a.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;// VK_ATTACHMENT_STORE_OP_DONT_CARE;				// Same for store
        a.initialLayout  = vk::ImageLayout::eUndefined;//VK_IMAGE_LAYOUT_UNDEFINED;						// Layout at render pass start. Initial doesn't matter, so we use undefined
        a.finalLayout    = finalLayout;//VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;					// Layout to which the attachment is transitioned when the render pass is finished

        attachmentDescription.push_back(a);

        // Setup attachment references
        vk::AttachmentReference colorReference;
        colorReference.attachment = static_cast<uint32_t>(attachmentDescription.size()-1);													// Attachment 0 is color
        colorReference.layout     = vk::ImageLayout::eColorAttachmentOptimal;// VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;				// Attachment layout used as color during the subpass
        colorReferences.push_back(colorReference);

        return attachmentDescription.size()-1;
    }

    /**
     * @brief addDepthStenchilAttachment
     * @param depthStencilFormat
     * @param finalLayout
     * @return
     *
     * Add a depthStenil to the renderpass. If adding a depthStencil
     * it should be added AFTER all the colorAttachments have been added.
     */
    size_t addDepthStenchilAttachment(vk::Format depthStencilFormat,
                                    vk::ImageLayout finalLayout)
    {
        auto i = addColorAttachment(depthStencilFormat, finalLayout);
        colorReferences[i].layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
        return i;
    }

    /**
     * @brief generateRenderPassCreateInfo
     * @return
     *
     * Generate a vk::RenderPassCreateInfo struct out of this helper struct
     * The generated data contains pointers to this struct so
     * this struct should not be freed before it is used to generate
     * a renderpass.
     */
    vk::RenderPassCreateInfo create()
    {

        subpassDescription.pipelineBindPoint       = vk::PipelineBindPoint::eGraphics;// VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpassDescription.pColorAttachments       = colorReferences.data();
        subpassDescription.colorAttachmentCount    = static_cast<uint32_t>(colorReferences.size());

        subpassDescription.pDepthStencilAttachment = nullptr;
        if( colorReferences.back().layout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
        {
            subpassDescription.pDepthStencilAttachment = &colorReferences.back();
            subpassDescription.colorAttachmentCount--;
        }

        vk::RenderPassCreateInfo renderPassInfo;
        renderPassInfo.attachmentCount = static_cast<uint32_t>(attachmentDescription.size());		// Number of attachments used by this render pass
        renderPassInfo.pAttachments    = attachmentDescription.data();								// Descriptions of the attachments used by the render pass
        renderPassInfo.subpassCount    = 1;												// We only use one subpass in this example
        renderPassInfo.pSubpasses      = &subpassDescription;								// Description of that subpass
        renderPassInfo.dependencyCount = static_cast<uint32_t>(subpassDependencies.size());	// Number of subpass dependencies
        renderPassInfo.pDependencies   = subpassDependencies.data();

        return renderPassInfo;
    }

    vk::RenderPassCreateInfo generateRenderPassCreateInfo()
    {
        return create();
    }

protected:
    std::vector<vk::SubpassDependency>     subpassDependencies;
    vk::SubpassDescription                 subpassDescription;
    friend class std::hash<vka::RenderPassCreateInfo2>;
};

}


namespace std
{
    template<>
    struct hash<vka::RenderPassCreateInfo2>
    {
        std::size_t operator()(vka::RenderPassCreateInfo2 const& s) const noexcept
        {
            std::hash<size_t> H;

            size_t h = H( s.colorReferences.size() );
            h = vka::hashCombine(h, H(s.attachmentDescription.size() ) );
//            h = vka::hashCombine(h, H(s.subpassDependencies.size() ) );

            for(auto & b : s.colorReferences)
            {
                h = vka::hashCombine(h, getHash(b) );
            }
            for(auto & b : s.attachmentDescription)
            {
                h = vka::hashCombine(h, getHash(b) );
            }
            //for(auto & b : s.subpassDependencies)
            //{
            //    h = vka::hashCombine(h, getHash(b) );
            //}

            return h;
        }

        template<typename POD_t>
        static size_t getHash( POD_t const & d)
        {
            std::hash<size_t> H;
            size_t h = H( sizeof(d));

            if constexpr ( sizeof(d)%sizeof(size_t)==0)
            {
                size_t const * v = reinterpret_cast<size_t const *>(&d);
                size_t const * e = v + sizeof(d)/sizeof(size_t);
                while( v != e)
                {
                    h = vka::hashCombine(h, *v++);
                }
                return h;
            }
            if constexpr ( sizeof(d)%sizeof(uint32_t)==0)
            {
                uint32_t const * v = reinterpret_cast<uint32_t const *>(&d);
                uint32_t const * e = v + sizeof(d)/sizeof(uint32_t);
                while( v != e)
                {
                    h = vka::hashCombine(h, *v++);
                }
                return h;
            }
            if constexpr ( sizeof(d)%sizeof(uint16_t)==0)
            {
                uint16_t const * v = reinterpret_cast<uint16_t const *>(&d);
                uint16_t const * e = v + sizeof(d)/sizeof(uint16_t);
                while( v != e)
                {
                    h = vka::hashCombine(h, *v++);
                }
                return h;
            }
            if constexpr ( sizeof(d)%sizeof(uint8_t)==0)
            {
                uint8_t const * v = reinterpret_cast<uint8_t const *>(&d);
                uint8_t const * e = v + sizeof(d)/sizeof(uint8_t);
                while( v != e)
                {
                    h = vka::hashCombine(h, *v++);
                }
                return h;
            }
            throw std::runtime_error("bad");
        }
    };
}

#endif
