#ifndef VKA_PIPELINE_LAYOUT_CREATE_INFO_H
#define VKA_PIPELINE_LAYOUT_CREATE_INFO_H

#include <vulkan/vulkan.hpp>
#include <vector>
#include "DescriptorSetLayoutCreateInfo.h"

namespace vka
{

struct PipelineLayoutCreateInfo2
{
    std::vector<vk::DescriptorSetLayout>             SetLayouts;             // can either provide the acutal set layouts
    std::vector<vk::PushConstantRange>               PushConstantRanges;


    PipelineLayoutCreateInfo2( )
    {
    }

    vk::PipelineLayoutCreateInfo create() const
    {
        vk::PipelineLayoutCreateInfo c;
        c.setPSetLayouts( SetLayouts.data() );
        c.setSetLayoutCount( static_cast<uint32_t>(SetLayouts.size()) );
        c.setPPushConstantRanges( PushConstantRanges.data());
        c.setPushConstantRangeCount( static_cast<uint32_t>(PushConstantRanges.size()) );
        return c;
    }

   // PipelineLayoutCreateInfo2( vk::PipelineLayoutCreateInfo const & info)
   // {
   //     for(size_t i=0;i<info.setLayoutCount;i++)
   //         SetLayouts.push_back( info.pSetLayouts[i]);
   //
   //     for(size_t i=0;i<info.pushConstantRangeCount;i++)
   //         PushConstantRanges.push_back( info.pPushConstantRanges[i]);
   // }

    size_t getHash() const;
};


}

namespace std
{
    template<>
    struct hash<vka::PipelineLayoutCreateInfo2>
    {
        std::size_t operator()(vka::PipelineLayoutCreateInfo2 const& s) const noexcept
        {
            std::hash<size_t> H;
            std::hash<void*>  Hv;

            size_t h = H( s.SetLayouts.size() );

            for(auto & b : s.SetLayouts)
            {
                h = vka::hashCombine(h, Hv( static_cast<void*>(b)) );
            }

            return h;
        }
    };
}

#endif
