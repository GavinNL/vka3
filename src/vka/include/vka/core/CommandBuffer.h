#pragma once
#ifndef VKA_COMMAND_BUFFER_H
#define VKA_COMMAND_BUFFER_H

#include <vulkan/vulkan.hpp>

namespace vka
{

class DeviceMeshPrimitive;
class SubBuffer;

enum class CommandBufferErrors
{
    SUCCESS = 0
};

struct BufferSubImageCopyToImage
{
    vk::Extent2D  srcBufferImageExtent;      // the full width/height of the image in the buffer

    vk::Rect2D    srcBufferRegion;           // the region within the srcBuffer image to copy from

    vk::Offset2D  dstImageOffset;            // the offset in the vk::image to copy the src to
    uint32_t      layer;                     // the layer to copy the src to
    uint32_t      mip;                       // the mip level to copy the src to
};


class CommandBuffer : public vk::CommandBuffer
{
public:
    CommandBuffer()
    {
    }

    CommandBuffer(const vk::CommandBuffer & C ) : vk::CommandBuffer(C)
    {
    }


    /**
     * @brief get_BufferCopy
     * @param src
     * @param dst
     * @return
     *
     * Gets the BufferCopy struct used to copy data from src to dst.
     * Copies the data from one subbuffer to another. Src subbuffer is
     * copied to the beginning of dst subbuffer.
     */
    vk::BufferCopy get_BufferCopy( vka::SubBuffer const & src, vka::SubBuffer const & dst) const;

    /**
     * @brief copySubBuffer
     * @param src
     * @param dst
     *
     * Copies the data fome one subbuffer to another. Src subbuffer is
     * copied to the begining of dst buffer.
     */
    void copySubBuffer(vka::SubBuffer const & src, const SubBuffer &dst);



    void copySubBufferToImage(vka::SubBuffer const &src,
                              vk::Image img,
                              vk::ImageLayout imageLayout,
                              const vk::BufferImageCopy &C) const;


    /**
     * @brief setImageLayout
     * @param img
     * @param range
     * @param _oldLayout
     * @param _newLayout
     * @param srcAccess
     * @param dstAccess
     * @param srcStageFlags
     * @param dstStageFlags
     *
     * Set the image Layout for a particaular SubresourceRange
     * for an image
     */
    void setImageLayout(vk::Image img,
                        vk::ImageSubresourceRange const & range,
                        vk::ImageLayout _oldLayout,
                        vk::ImageLayout _newLayout,
                        vk::AccessFlags srcAccess,
                        vk::AccessFlags dstAccess,
                        vk::PipelineStageFlags srcStageFlags,
                        vk::PipelineStageFlags dstStageFlags);
    /**
     * @brief setImageLayout
     * @param img
     * @param aspects
     * @param oldLayout
     * @param newLayout
     * @param srcAccess
     * @param dstAccess
     * @param srcStageFlags
     * @param dstStageFlags
     *
     * Sets the imagelayout for the entire image.
     */
    void setImageLayout(vk::Image img,
                        vk::ImageAspectFlags aspects,
                        vk::ImageLayout oldLayout,
                        vk::ImageLayout newLayout,
                        vk::AccessFlags srcAccess,
                        vk::AccessFlags dstAccess,
                        vk::PipelineStageFlags srcStageFlags,
                        vk::PipelineStageFlags dstStageFlags);


    /**
     * @brief generateMipMaps
     * @param img
     * @param layer
     *
     * Generate mipmaps for an image layer. The image layer should
     * be in ShaderReadOnlyOptimal layout.
     */
    void generateMipMaps(vk::Image img, uint32_t layer);
    void generateMipMaps(vk::ImageView img, uint32_t layer);



    void copyBufferToImageAndConvert (vka::SubBuffer srcBuffer,
                                      vk::ImageView dstImageView,
                                      vk::ArrayProxy<BufferSubImageCopyToImage const > subImageCopies);


    /**
     * @brief copyBufferToImageWithBorderAndConvert
     * @param srcBuffer
     * @param dstImageView
     * @param borderSize
     * @param subImageCopies
     *
     * Copies a subImage from a buffer into a location on an image, this will
     * additionally it will create a border around the copied area. The border
     * will contain a repeated pattern of the image. This can be used if
     * you you are planning on using a TextureAtlas and require proper sampling
     * at the border regions.
     *
     *
     * +--+--------------+--+
     * | D|A            D|A |
     * +--+--------------+--+
     * | C|B            C|B |
     * |  |              |  |
     * |  |              |  |
     * |  |              |  |
     * | D|A            D|A |
     + +--+--------------+--+
     * | C|B             |B |
     * +--+--------------+--+
     */
    void copyBufferToImageWithBorderAndConvert(
                            vka::SubBuffer srcBuffer,
                            vk::ImageView dstImageView,
                            uint32_t borderSize,
                            vka::BufferSubImageCopyToImage subImageCopies);

    /**
     * @brief copyDeviceMeshPrimitive
     * @param src
     * @param dst
     *
     * Copies the device mesh primitive from src to dst. Used to copy
     * from host visible to device local memory.
     */
    void copyDeviceMeshPrimitive( DeviceMeshPrimitive & src, DeviceMeshPrimitive & dst);


    void bindDescriptorSets( vk::Pipeline pipeline, uint32_t firstSet, vk::ArrayProxy<const vk::DescriptorSet> descriptorSets, vk::ArrayProxy<const uint32_t> dynamicOffsets) const;


    /**
     * @brief bindDeviceMeshPrimitive
     * @param src
     * @param bindAvailable
     *
     * Bind all the buffers in the mesh primitive. If bindAvailable is set to true, only
     * the attributes which are available are bound in the order they appear in the primitive.
     * If set to false, all buffer bindings are bound (0-7) in the order they appear
     * in the enum. If the attribute is not available, it is bound to the beginning of the buffer.
     */
    void bindDeviceMeshPrimitive(DeviceMeshPrimitive const & src, bool bindAvailable);

    void bindIndexSubBuffer(vka::SubBuffer const &src, vk::DeviceSize offset, vk::IndexType indexType);

    void bindVertexSubBuffer( uint32_t firstBinding, vka::SubBuffer const & src, vk::DeviceSize subBufferOffset);

    void waitForHostTransfers()
    {
        vk::MemoryBarrier memoryBarrier;
        memoryBarrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite; //vk::AccessFlags::eTra  eT VK_ACCESS_TRANSFER_WRITE_BIT;
        memoryBarrier.dstAccessMask = vk::AccessFlagBits::eVertexAttributeRead ;//vk::Access:e VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;

        pipelineBarrier( vk::PipelineStageFlagBits::eTransfer,
                         vk::PipelineStageFlagBits::eVertexInput,
                         vk::DependencyFlags(),
                         memoryBarrier,
                         nullptr,
                         nullptr);
        //vkCmdPipelineBarrier(
        //    ...
        //    VK_PIPELINE_STAGE_TRANSFER_BIT ,      // srcStageMask
        //    VK_PIPELINE_STAGE_VERTEX_INPUT_BIT,   // dstStageMask
        //    1,                                    // memoryBarrierCount
        //    &memoryBarrier,                       // pMemoryBarriers
        //    ...);
    }
    void simplePipelineBarrier(vk::PipelineStageFlags srcStage, vk::PipelineStageFlags dstStage )
    {
        pipelineBarrier( srcStage,
                         dstStage,
                         vk::DependencyFlags(),
                         nullptr, nullptr, nullptr);
    }


    //==================================================================================
    // Some helper functions to do certain image layout transitions
    //
    // These are very specific transitions and may be subject to change.
    //
    //==================================================================================

    // Converts a ShaderReadOnlyOptimal image into
    // TransferSrcOptimal so that the data can be copied to
    // another location.
    void convertShaderImageToTransferSrc(vk::Image img)
    {
        vk::ImageMemoryBarrier imageBarrier;
        imageBarrier.image            = img;
        imageBarrier.oldLayout        = vk::ImageLayout::eShaderReadOnlyOptimal;
        imageBarrier.newLayout        = vk::ImageLayout::eTransferSrcOptimal;
        imageBarrier.subresourceRange = vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor,0,1,0,1);
        imageBarrier.srcAccessMask    = vk::AccessFlagBits::eColorAttachmentWrite;
        imageBarrier.dstAccessMask    = vk::AccessFlagBits::eTransferRead;

        pipelineBarrier( vk::PipelineStageFlagBits::eColorAttachmentOutput,
                         vk::PipelineStageFlagBits::eTransfer,
                         vk::DependencyFlags(),
                         nullptr,
                         nullptr,
                         imageBarrier);
    }

    // convert a swapchain image to transfer src dst so that
    // another image can be copied onto i.
    void convertSwaphainImageToTransferDst(vk::Image swapchainImage)
    {
        vk::ImageMemoryBarrier imageBarrier;

        imageBarrier.image            = swapchainImage;
        imageBarrier.oldLayout        = vk::ImageLayout::eUndefined;
        imageBarrier.newLayout        = vk::ImageLayout::eTransferDstOptimal;
        imageBarrier.subresourceRange = vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor,0,1,0,1);
        imageBarrier.srcAccessMask    = vk::AccessFlagBits::eColorAttachmentWrite;
        imageBarrier.dstAccessMask    = vk::AccessFlagBits::eTransferWrite;

        pipelineBarrier( vk::PipelineStageFlagBits::eAllGraphics,
                         vk::PipelineStageFlagBits::eTransfer,
                         vk::DependencyFlags(),
                         nullptr,
                         nullptr,
                         imageBarrier);

    }

    void convertSwapchainImageFromTransferDstToPresentSrc(vk::Image swapchainImage)
    {
        vk::ImageMemoryBarrier imageBarrier;

        imageBarrier.image            = swapchainImage;
        imageBarrier.oldLayout        = vk::ImageLayout::eTransferDstOptimal;
        imageBarrier.newLayout        = vk::ImageLayout::ePresentSrcKHR;
        imageBarrier.subresourceRange = vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor,0,1,0,1);
        imageBarrier.srcAccessMask    = vk::AccessFlagBits::eTransferWrite;
        imageBarrier.dstAccessMask    = vk::AccessFlagBits::eTransferRead;

        pipelineBarrier( vk::PipelineStageFlagBits::eTransfer,
                         vk::PipelineStageFlagBits::eTransfer,
                         vk::DependencyFlags(),
                         nullptr,
                         nullptr,
                         imageBarrier);

    }

    // add a pipeline barrier which will block vertex shaders
    // from executing until all the memory transfers have been finished
    void waitForTransfersBeforeRunningVertexShaders()
    {
        vk::MemoryBarrier memoryBarrier;
        memoryBarrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite; //vk::AccessFlags::eTra  eT VK_ACCESS_TRANSFER_WRITE_BIT;
        memoryBarrier.dstAccessMask = vk::AccessFlagBits::eVertexAttributeRead ;//vk::Access:e VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;

        pipelineBarrier( vk::PipelineStageFlagBits::eTransfer,
                         vk::PipelineStageFlagBits::eVertexInput,
                         vk::DependencyFlags(),
                         memoryBarrier,
                         nullptr,
                         nullptr);
    }
};

}

#endif
