#ifndef VKA_OBJECT_CONTAINER_H
#define VKA_OBJECT_CONTAINER_H

#include <map>

namespace vka
{
template<typename obj_t, typename objInfo_t>
struct ObjContainer
{
    std::map<obj_t, objInfo_t>    m_objects;

    auto begin()
    {
        return m_objects.begin();
    }
    auto end()
    {
        return m_objects.end();
    }

    auto begin() const
    {
        return m_objects.begin();
    }
    auto end() const
    {
        return m_objects.end();
    }

    bool exists(obj_t obj) const
    {
        return m_objects.count(obj) > 0;
    }

    /**
     * @brief insert
     * @param obj
     * @param info
     * @return
     *
     * Inserts an object into the container. Returns true if it was able
     * to be inserted, returns false if the object already exists in there,
     * or if the createInfo.
     *
     * The objInfo_t must contain a member variable called createInfo
     * If hashed==true, then std::hash< decltype(info.createInfo) > must
     * be defined
     */
    bool insert( obj_t obj, objInfo_t info)
    {
        if( m_objects.count(obj) )
        {
            return false;
        }
        else
        {
            m_objects[obj] = info;
            return true;
        }
    }

    bool erase(obj_t obj)
    {
        return m_objects.erase(obj) > 0;
    }

    objInfo_t & info(obj_t obj)
    {
        return m_objects.at(obj);
    }
    objInfo_t const & info(obj_t obj) const
    {
        return m_objects.at(obj);
    }
};


template<typename obj_t, typename objInfo_t, typename createInfo_t>
struct HashedObjContainer
{
    std::map<obj_t, objInfo_t>    m_objects;
    std::map<std::size_t, obj_t>  m_hashToObject;
    std::map<obj_t, size_t>       m_objectToHash;

    auto begin()
    {
        return m_objects.begin();
    }
    auto end()
    {
        return m_objects.end();
    }

    auto begin() const
    {
        return m_objects.begin();
    }
    auto end() const
    {
        return m_objects.end();
    }

    /**
     * @brief exists
     * @param CI
     *
     * Returns true if the hashed createInfo type
     * already exists in the container
     */
    bool exists( createInfo_t const & CI) const
    {
        std::hash< createInfo_t > H;
        auto hash = H(CI);

        if( m_hashToObject.count(hash) )
        {
            return true;
        }
        return false;
    }

    bool exists( obj_t object) const
    {
        return m_objects.count(object)==1;
    }

    /**
     *
     * If this is a hashed type, then return the object assocaited
     * with the hashed createInfo
     */
    obj_t at( createInfo_t const & CI)
    {
        std::hash< createInfo_t > H;
        auto hash = H(CI);

        return m_hashToObject.at(hash);
    }

    /**
     * @brief hash
     * @param obj
     * @return
     *
     * Returns the hash of the object.
     */
    size_t hash(obj_t obj) const
    {
        return m_objectToHash.at(obj);
    }

    bool insert( obj_t obj, objInfo_t info, createInfo_t createInfo)
    {
        if( exists(createInfo) )
        {
            return false;
        }
        else
        {
            std::hash< createInfo_t > H;

            auto hash = H(createInfo);

            m_hashToObject[hash]  = obj;
            m_objectToHash[obj]   = hash;
            m_objects[obj]        = info;
            return true;
        }
    }

    bool erase(obj_t obj)
    {
        auto h = hash(obj);

        m_hashToObject.erase(h);
        m_objectToHash.erase(obj);
        return m_objects.erase(obj) > 0;
    }

    objInfo_t & info(obj_t obj)
    {
        return m_objects.at(obj);
    }
    objInfo_t const & info(obj_t obj) const
    {
        return m_objects.at(obj);
    }
};

}

#endif
