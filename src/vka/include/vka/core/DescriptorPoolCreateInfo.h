#ifndef VKA_DESCRIPTOR_POOL_CREATE_INFO_H
#define VKA_DESCRIPTOR_POOL_CREATE_INFO_H

#include <vulkan/vulkan.hpp>
#include <vector>
#include <vka/utils/hash.h>


namespace vka
{

struct DescriptorPoolCreateInfo2
{
    vk::DescriptorPoolCreateFlags       flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
    uint32_t                            maxSets;
    std::vector<vk::DescriptorPoolSize> poolSizes;

    vk::DescriptorPoolCreateInfo create() const
    {
        vk::DescriptorPoolCreateInfo I;
        I.maxSets       = maxSets;
        I.flags         = flags;
        I.pPoolSizes    = poolSizes.data();
        I.poolSizeCount = static_cast<uint32_t>(poolSizes.size());

        return I;
    }

};

}

#endif
