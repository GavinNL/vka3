#ifndef VKA_SCREENBUFFER_H
#define VKA_SCREENBUFFER_H

#include <vulkan/vulkan.hpp>
#include <vector>

namespace vka
{

class ScreenBuffer
{
public:
    vk::SwapchainKHR             m_SwapChain;
    std::vector<vk::Framebuffer> m_frameBuffers;
    vk::RenderPass               m_renderPass;
    vk::Extent2D                 m_extent;

    std::vector<vk::ClearValue>  m_clearColors;


    vk::RenderPass getDefaultRenderPass() const
    {
        return m_renderPass;
    }
    /**
     * @brief getNextFrameIndex
     * @param signalSemaphore
     * @return
     *
     * Returns the next frame index available in the swapchain
     * and trigger the signalSemaphore when it is available.
     */
    uint32_t getNextFrameIndex(vk::Semaphore signalSemaphore);


    /**
     * @brief getRendeRassBeginInfo
     * @return
     *
     * Get a default constructed RenderPassBeginInfo struct
     * You must have called getNextFrameIndex( ... ) prior to
     * calling this.
     *
     * You can also do this manually.
     *
     *       vk::RenderPassBeginInfo RPBI;
     *
     *       vk::ClearValue cv( vk::ClearColorValue( std::array<float,4>{1.0f,0.0,0.0,1.0} ));
     *       RPBI.renderPass        = ScreenB.m_renderPass;
     *       RPBI.framebuffer       = ScreenB.m_frameBuffers[frameBufferIndex];
     *       RPBI.clearValueCount   = 1;
     *       RPBI.pClearValues      = &cv;
     *       RPBI.renderArea.extent = ScreenB.m_extent;
     */
    vk::RenderPassBeginInfo getRenderPassBeginInfo();


    /**
     * @brief presentFrameIndex
     * @param frameIndex
     * @param wait_semaphore
     *
     * Present a frame index to the screen, but wait for all the wait_semaphores
     * to trigger first.
     */
    void presentFrameIndex(uint32_t frameIndex, const vk::ArrayProxy<const vk::Semaphore > wait_semaphore);


    /**
     * @brief createRenderPass
     * @param depth_format
     * @return
     *
     * Create the renderpass for this screen buffer.
     */
    void createRenderPass(vk::Format depth_format=vk::Format::eUndefined);
    void createFramebuffers(vk::ImageView depth_format = vk::ImageView());


    /**
     * @brief setClearColor
     * @param r
     * @param g
     * @param b
     * @param a
     *
     * Sets the clear color for the screenBuffer
     */
    void setClearColor(float r, float g, float b, float a);

    void setDepthClear(float depth, uint32_t stencil=0);

private:
    uint32_t m_next_frame_index;
};


}

#endif 

