#ifndef VKA_RENDER_TARGET_H
#define VKA_RENDER_TARGET_H

#include <vulkan/vulkan.hpp>
#include <vka/core/Global.h>
#include <vector>
namespace vka
{

/**
 * @brief The RenderTargetCreateInfo struct
 *
 * A very simple RenderTargetCreateInfo struct. USed to create
 * render-to-texture render passes.
 *
 * Simple give it a list of imageviews to render to
 * and an optional depthTarget imageView and
 *
 * pass it to Syste::createRenderTarget( );
 */
struct RenderTargetCreateInfo
{
    std::vector<vk::ImageView> colorTargets;
    vk::ImageView              depthTarget;
};

struct RenderTarget
{
public:
    vk::Framebuffer              m_frameBuffers;
    vk::RenderPass               m_renderPass;
    std::vector<vk::ClearValue>  m_clearValues;
    vk::Extent2D                 m_extent;

    vk::RenderPass getDefaultRenderPass() const
    {
        return m_renderPass;
    }
    vk::RenderPassBeginInfo getRenderPassBeginInfo() const
    {
        vk::RenderPassBeginInfo m_renderpass_info;

        m_renderpass_info.renderPass        = m_renderPass;
        m_renderpass_info.framebuffer       = m_frameBuffers;
        m_renderpass_info.clearValueCount   = static_cast<uint32_t>(m_clearValues.size());
        m_renderpass_info.pClearValues      = m_clearValues.data();//m_clear_values.data();
        m_renderpass_info.renderArea.extent = m_extent;

        return m_renderpass_info;
    }

};


}

#endif 

