#ifndef VKA_SCREENBUFFER_CREATE_INFO
#define VKA_SCREENBUFFER_CREATE_INFO

#include <vulkan/vulkan.hpp>
#include <vector>
#include <variant>
#include <vka/utils/hash.h>

namespace vka
{

struct ScreenBufferCreateInfo
{
    vk::SurfaceKHR   surface;  // which surface to use
    vk::Extent2D     extent;   // what are the dimensions of the surface
    bool             verticalSync;

    std::optional<vk::SwapchainKHR> oldSwapchain;

    uint32_t         extraSwapchainImages=0; // how many extra swapchain images should we allocate?
    uint32_t         maxSwapchainImages  =0xFFFFFFFF;     // what the maximum swapchain imges we should allocate?

    std::variant<vk::Format, vk::ImageView> depthStencilFormatOrImage;
    //vk::Format       depthStencilFormat;           // what depth format to use.

    bool             generateInternalDepthTexture; // if set to true, it will manage
                                                   // creation of an internal
};

struct ScreenBufferInfo
{
    ScreenBufferCreateInfo     createInfo;
    std::vector<vk::ImageView> swapchainImageViews;
};

}

#endif
