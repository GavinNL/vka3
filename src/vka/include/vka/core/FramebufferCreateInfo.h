#ifndef VKA_FRAMEBUFFER_CREATE_INFO2
#define VKA_FRAMEBUFFER_CREATE_INFO2

#include <vulkan/vulkan.hpp>
#include <vector>
#include <vka/utils/hash.h>

namespace vka
{

/**
 * @brief The RenderPassCreateInfo2 struct
 *
 * This is a helper class which can be used
 * to generate a simple renderpass with
 * only a single subpass dependency.
 *
 *
 */
struct FramebufferCreateInfo2
{
    vk::RenderPass             renderPass;
    std::vector<vk::ImageView> attachments;
    uint32_t                         width;
    uint32_t                        height;
    uint32_t                        layers=1;

    FramebufferCreateInfo2()
    {


    }


    /**
     * @brief generateRenderPassCreateInfo
     * @return
     *
     * Generate a vk::RenderPassCreateInfo struct out of this helper struct
     * The generated data contains pointers to this struct so
     * this struct should not be freed before it is used to generate
     * a renderpass.
     */
    vk::FramebufferCreateInfo create() const
    {
        vk::FramebufferCreateInfo fbufCreateInfo;// = {};
        fbufCreateInfo.pNext           = NULL;
        fbufCreateInfo.renderPass      = renderPass;
        fbufCreateInfo.pAttachments    = attachments.data();
        fbufCreateInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
        fbufCreateInfo.width           = width;// offScreenFrameBuf.width;
        fbufCreateInfo.height          = height;//offScreenFrameBuf.height;
        fbufCreateInfo.layers          = layers;

        return fbufCreateInfo;
    }
};

}


#endif
