#ifndef VKA_SCENE_LOADER_H
#define VKA_SCENE_LOADER_H

#include<array>
#include<vector>
#include <ugltf/ugltf.h>
#include <vka/utils/HostTriMesh.h>
#include <vka/utils/uGLTF_Helpers.h>
#include "Scene.h"
#include <optional>

namespace vka
{


inline vka::Scene fromUGLTF( uGLTF::GLTFModel const & M,
                             std::vector<vka::HostTriPrimitive> & hostPrimitives,
                             std::vector<vka::HostImage>        & hostImages)
{
    vka::Scene S;

    struct conv
    {
        static glm::quat toQuat( glm::vec4 const &q )
        {
            return
            glm::quat(
                       q.w,
                       q.x,
                       q.y,
                       q.z
                     ) ;
        }
        static glm::quat toQuat( glm::i8vec4 const &q )
        {
            return
            glm::quat(
                       std::max( static_cast<float>(q.w) / 127.0f, -1.0f),
                       std::max( static_cast<float>(q.x) / 127.0f, -1.0f),
                       std::max( static_cast<float>(q.y) / 127.0f, -1.0f),
                       std::max( static_cast<float>(q.z) / 127.0f, -1.0f)
                     ) ;
        }
        static glm::quat toQuat( glm::u8vec4 const &q )
        {
            return
                    glm::quat(
                               static_cast<float>(q.w) / 255.0f,
                               static_cast<float>(q.x) / 255.0f,
                               static_cast<float>(q.y) / 255.0f,
                               static_cast<float>(q.z) / 255.0f
                         ) ;
        }

        static glm::quat toQuat( glm::i16vec4 const &q )
        {
            return
            glm::quat(
                       std::max( static_cast<float>(q.w) / 32767.0f, -1.0f),
                       std::max( static_cast<float>(q.x) / 32767.0f, -1.0f),
                       std::max( static_cast<float>(q.y) / 32767.0f, -1.0f),
                       std::max( static_cast<float>(q.z) / 32767.0f, -1.0f)
                     ) ;
        }
        static glm::quat toQuat( glm::u16vec4 const &q )
        {
            return
                    glm::quat(
                               static_cast<float>(q.w) / 32767.0f,
                               static_cast<float>(q.x) / 32767.0f,
                               static_cast<float>(q.y) / 32767.0f,
                               static_cast<float>(q.z) / 32767.0f
                         ) ;
        }
    };

    for(auto & s : M.scenes)
    {
        auto & Sc = S.scenes.emplace_back();
        Sc.name      = s.name;
        Sc.rootNodes = s.nodes;
    }

    S.nodes.resize(M.nodes.size() );
    for(auto & x : M.nodes )
    {
        auto nodeIndex = std::distance(&M.nodes.front(), &x );
        vka::Scene::Node & n = S.nodes.at( static_cast<size_t>(nodeIndex) );

        n.name     = x.name;
        n.children = x.children;
        n.transform.position    = {x.translation[0], x.translation[1], x.translation[2]};
        n.transform.scale       = {x.scale[0], x.scale[1], x.scale[2]};
        n.transform.rotation = glm::quat(x.rotation[3], x.rotation[0], x.rotation[1], x.rotation[2]);

        for(auto & c : n.children)
        {
            S.nodes.at(c).parentIndex = static_cast<int32_t>(nodeIndex);
        }
        if( x.mesh != std::numeric_limits<uint32_t>::max() )
            n.meshIndex = x.mesh;

        if( x.skin != std::numeric_limits<uint32_t>::max() )
            n.skinIndex = x.skin;

    }

    for(auto & m : M.materials)
    {
        vka::Scene::Material mat;

        mat.baseColorFactor[0] = m.pbrMetallicRoughness.baseColorFactor[0];
        mat.baseColorFactor[1] = m.pbrMetallicRoughness.baseColorFactor[1];
        mat.baseColorFactor[2] = m.pbrMetallicRoughness.baseColorFactor[2];
        mat.baseColorFactor[3] = m.pbrMetallicRoughness.baseColorFactor[3];

        mat.metallicFactor = m.pbrMetallicRoughness.metallicFactor;
        mat.roughnessFactor= m.pbrMetallicRoughness.roughnessFactor;

        mat.emissiveFactor[0] = m.emissiveFactor[0];
        mat.emissiveFactor[1] = m.emissiveFactor[1];
        mat.emissiveFactor[2] = m.emissiveFactor[2];


        if(m.hasPBR())
        {
            if(m.pbrMetallicRoughness.baseColorTexture)
                mat.baseColorTexture = static_cast<int32_t>(m.pbrMetallicRoughness.baseColorTexture.index);

            if(m.pbrMetallicRoughness.baseColorTexture)
                mat.metallicRoughnessTexture =  static_cast<int32_t>( m.pbrMetallicRoughness.metallicRoughnessTexture.index );
        }

        if( m.hasNormalTexture() )    mat.normalTexture    = static_cast<int32_t>(m.normalTexture.index);
        if( m.hasOcclusionTexture() ) mat.occlusionTexture = static_cast<int32_t>(m.occlusionTexture.index);
        if( m.hasEmissiveTexture() )  mat.emissiveTexture  = static_cast<int32_t>(m.emissiveTexture.index);

        S.materials.push_back(mat);
    }

    for(auto & T : M.textures)
    {
        vka::Scene::Texture t;
        t.imageIndex   = T.source;
        t.samplerIndex = T.sampler;
        S.textures.push_back(t);
    }

    std::vector<uGLTF::Primitive> _pr;
    uint32_t count=0;
    for(auto & m : M.meshes)
    {
        vka::Scene::Mesh mesh;

        mesh.name        = m.name;

        // the primitives listed in a mesh could refer to the same actual
        // accessor values, so we need to make sure that we do not duplicate
        // them.
        for(auto & p : m.primitives )
        {
            count++;
            // seach the vector ot see if there is already a primitive with those values
            auto it = std::find_if( _pr.begin(), _pr.end(), [&](auto const &p1) { return p.refersToSame(p1);});

            if( it == _pr.end()) // hasn't been added already
            {
                // add the primitive to the end
                _pr.push_back(p);

                // create a HostTriMesh from the GLTF primitive.
                hostPrimitives.push_back( vka::convertUGLTFPrimitiveToHostTriPrimitive(p) );

                vka::Scene::Primitive primitive;
                primitive.rawPrimitiveIndex = static_cast<uint32_t>( hostPrimitives.size()-1 );
                primitive.drawCall          = hostPrimitives.back().getDrawCall();

                S.primitives.push_back(primitive);

                vka::Scene::MeshPrimitive meshPrimitive;
                meshPrimitive.materialIndex  = p.material;
                meshPrimitive.primitiveIndex = static_cast<uint32_t>( S.primitives.size()-1 );

                mesh.meshPrimitives.push_back(meshPrimitive);
            }
            else
            {
                //std::cout << "Primitive Already exists: using the old one" << std::endl;
                auto primIndex = std::distance(_pr.begin(), it);
                vka::Scene::MeshPrimitive meshPrimitive;
                meshPrimitive.materialIndex  = p.material;
                meshPrimitive.primitiveIndex = static_cast<uint32_t>( primIndex );
                mesh.meshPrimitives.push_back(meshPrimitive);
            }
        }

        S.meshes.push_back(mesh);
    }

    for(auto & a : M.animations)
    {
        auto & A    = S.animations.emplace_back();
        auto & Ch   = A.channels;
        auto & Samp = A.samplers;

        A.name  = a.name;
        A.start = std::numeric_limits<float>::max();
        A.end   = std::numeric_limits<float>::lowest();

        //====================================================
        // Find the total number of nodes that are
        // being affected by this animation.
        //====================================================
        std::map<uint32_t, vka::SceneBase::Animation::Channel> channelMap;
        for(auto & c : a.channels)
        {
            channelMap[c.target.node].node = c.target.node;
            channelMap[c.target.node].samplers[ static_cast<size_t>(c.target.path) ] = static_cast<int32_t>( c.sampler );
        }
        Ch.reserve(channelMap.size());
        for(auto & c : channelMap)
        {
            Ch.push_back(c.second);
        }
        //====================================================

        for(auto & c : a.samplers)
        {
            auto & s = Samp.emplace_back();
            auto & frames = s.frames;

            auto & t = c.getInputAccessor();
            auto & v = c.getOutputAccessor();

            s.interpolation = static_cast<uint8_t>( c.interpolation );

            A.start = std::min(A.start, t.getValue<float>(0) );
            A.end   = std::max(A.start, t.getValue<float>(t.count-1) );

            bool isCubic=false;
            if( s.interpolation == 2 )
            {
                isCubic = true;
            }

            if( v.type == uGLTF::AccessorType::VEC3)
            {
                if(isCubic)
                {
                    frames = vka::Scene::Animation::cubicVec3Sampler();
                }
                else
                {
                    frames = vka::Scene::Animation::linearVec3Sampler();
                }
            }
            else
            {
                if(isCubic)
                {
                    frames = vka::Scene::Animation::cubicQuatSampler();
                }
                else
                {
                    frames = vka::Scene::Animation::linearQuatSampler();
                }
            }

            std::visit([&](auto&& arg)
            {
                using T = std::decay_t<decltype(arg)>;

                if constexpr (std::is_same_v<T, vka::Scene::Animation::linearVec3Sampler>)
                {
                    // will always be vec3:
                    for(uint32_t i=0;i<t.count;i++)
                    {
                        auto time_val = t.getValue<float>(i);
                        auto s_value  = v.getValue<glm::vec3>(i);

                        arg.insert( time_val, s_value);
                    }
                }
                if constexpr (std::is_same_v<T, vka::Scene::Animation::cubicVec3Sampler>)
                {
                    // will always be vec3:
                    for(uint32_t i=0;i<t.count;i++)
                    {
                        auto time_val = t.getValue<float>(i);

                        auto s_value_int  = v.getValue<glm::vec3>(3*i+0);
                        auto s_value_in   = v.getValue<glm::vec3>(3*i+1);
                        auto s_value_outt = v.getValue<glm::vec3>(3*i+2);

                        arg.insert( time_val, {s_value_int, s_value_in, s_value_outt});
                    }
                }
                else if constexpr (std::is_same_v<T, vka::Scene::Animation::linearQuatSampler>)
                {
                    for(uint32_t i=0;i<t.count;i++)
                    {
                        auto time_val = t.getValue<float>(i);
                        glm::quat q;
                        switch(v.componentType)
                        {
                            case uGLTF::ComponentType::FLOAT:
                                q = conv::toQuat( v.getValue<glm::vec4>(i) );
                                break;
                            case uGLTF::ComponentType::BYTE:
                                q = conv::toQuat( v.getValue<glm::i8vec4>(i) );
                                break;
                            case uGLTF::ComponentType::UNSIGNED_BYTE:
                                q = conv::toQuat( v.getValue<glm::u8vec4>(i) );
                                break;
                            case uGLTF::ComponentType::SHORT:
                                q = conv::toQuat( v.getValue<glm::i16vec4>(i) );
                                break;
                            case uGLTF::ComponentType::UNSIGNED_SHORT:
                                q = conv::toQuat( v.getValue<glm::u16vec4>(i) );
                                break;
                            default:
                                throw std::invalid_argument("Not a valid component for this sampler");
                                break;
                        }
                        arg.insert( time_val, q);
                    }
                }
                else if constexpr (std::is_same_v<T, vka::Scene::Animation::cubicQuatSampler>)
                {
                    for(uint32_t i=0;i<t.count;i++)
                    {
                        auto time_val = t.getValue<float>(i);
                        glm::quat q1;
                        glm::quat q2;
                        glm::quat q3;
                        switch(v.componentType)
                        {
                            case uGLTF::ComponentType::FLOAT:
                                q1 = conv::toQuat( v.getValue<glm::vec4>(3*i+0) );
                                q2 = conv::toQuat( v.getValue<glm::vec4>(3*i+1) );
                                q3 = conv::toQuat( v.getValue<glm::vec4>(3*i+2) );
                                break;
                            case uGLTF::ComponentType::BYTE:
                                q1 = conv::toQuat( v.getValue<glm::i8vec4>(3*i+0) );
                                q2 = conv::toQuat( v.getValue<glm::i8vec4>(3*i+1) );
                                q3 = conv::toQuat( v.getValue<glm::i8vec4>(3*i+2) );
                                break;
                            case uGLTF::ComponentType::UNSIGNED_BYTE:
                                q1 = conv::toQuat( v.getValue<glm::u8vec4>(3*i+0) );
                                q2 = conv::toQuat( v.getValue<glm::u8vec4>(3*i+1) );
                                q3 = conv::toQuat( v.getValue<glm::u8vec4>(3*i+2) );
                                break;
                            case uGLTF::ComponentType::SHORT:
                                q1 = conv::toQuat( v.getValue<glm::i16vec4>(3*i+0) );
                                q2 = conv::toQuat( v.getValue<glm::i16vec4>(3*i+1) );
                                q3 = conv::toQuat( v.getValue<glm::i16vec4>(3*i+2) );
                                break;
                            case uGLTF::ComponentType::UNSIGNED_SHORT:
                                q1 = conv::toQuat( v.getValue<glm::u16vec4>(3*i+0) );
                                q2 = conv::toQuat( v.getValue<glm::u16vec4>(3*i+1) );
                                q3 = conv::toQuat( v.getValue<glm::u16vec4>(3*i+2) );
                                break;
                            default:
                                throw std::invalid_argument("Not a valid component for this sampler");
                                break;
                        }
                        arg.insert( time_val, {q1,q2,q3});
                    }
                }
            }, frames );
        }
    }

    for(auto & s : M.skins)
    {
        SceneBase::Skin sk;
        sk.joints   = s.joints;
        sk.rootNode = static_cast<uint32_t>(s.skeleton);

        if( s.hasInverseBindMatrices())
        {
            auto  bindMat = s.getInverseBindMatricesAccessor();
            sk.inverseBindMatrices.resize( bindMat.count );

            bindMat.memcpy_all( &sk.inverseBindMatrices[0] );
        }
        S.skins.push_back(sk);
    }


    for(auto & t : M.images)
    {
        hostImages.push_back( convertUGLTFImage(t) );
    }

    S.finalize();
    return S;
}




inline vka::HostScene fromUGLTF( uGLTF::GLTFModel const & M, bool optimize=true )
{
    std::vector<vka::HostTriPrimitive> P;
    std::vector<vka::HostImage> I;

    auto S = fromUGLTF(M, P, I);

    vka::HostScene hS;
    hS.nodes        = S.nodes;
    hS.primitives   = S.primitives;
    hS.meshes       = S.meshes;
    hS.materials    = S.materials;
    hS.animations   = S.animations;
    hS.skins        = S.skins;
    hS.scenes       = S.scenes; // root nodes per scene
    hS.textures     = S.textures;

    hS.rawPrimitives = std::move(P);
    hS.images        = std::move(I);

    if(optimize)
        hS.optimizePrimitives();

    return hS;
}

}


#endif
