#ifndef VKA_CAMERA_H
#define VKA_CAMERA_H

#include<array>
#include<vector>

#include<vka/math/transform.h>

#include<optional>

namespace vka
{

struct Camera
{
    vka::transform transform;
    glm::mat4      projMatrix;
    glm::vec2      screenSize;

    glm::mat4 calculateViewMatrix() const
    {
        return transform.getViewMatrix();
        //return glm::inverse( transform.getMatrix() );
    }

    void setPerspective( glm::vec2 _screenSize, float fov_deg, float near=0.1f, float far=30.f)
    {
        screenSize = _screenSize;
        float AR = screenSize.x / screenSize.y;
        projMatrix = glm::perspective( glm::radians(fov_deg), AR, near, far);
        projMatrix[1][1] *= -1;
    }

    line3 projectMouseRay( glm::vec2 mousePosition) const
    {
        auto cameraViewMatrix = calculateViewMatrix();

        auto proj = projMatrix;
        //proj[1][1] *= -1;
        auto ray1 =
        glm::unProject( glm::vec3(mousePosition.x,mousePosition.y,1),
                        cameraViewMatrix,
                        proj,
                        glm::vec4{0,0,screenSize.x, screenSize.y});

//        auto ray2 =
//        glm::unProject( glm::vec3(mousePosition.x,mousePosition.y,-1),
//                        cameraViewMatrix,
//                        projMatrix,
//                        glm::vec4{0,0,screenSize.x, screenSize.y});

        vec3 MOUSE_NEAR = transform.get_position();
        vec3 MOUSE_FAR  = 2.0f*MOUSE_NEAR - glm::vec3(ray1.x, ray1.y, ray1.z);

        return line3( point3(MOUSE_NEAR), point3(MOUSE_FAR));
    }
};



}


#endif

