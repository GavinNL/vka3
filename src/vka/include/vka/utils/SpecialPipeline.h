#ifndef VKA_SPECIAL_PIPELINE_H
#define VKA_SPECIAL_PIPELINE_H

#include <vka/core/System.h>
#include <vka/core/PipelineCreateInfo.h>

namespace vka
{




/**
 * @brief The SpecialPipeline struct
 *
 * The SpecialPipeline is able to recompile pipelines when the state changes.
 * For example, you can switch polygon modes/fillmods/depth testing/ etc
 */
struct SpecialPipeline
{
public:

    struct SpecialCompare
    {
      bool operator()(const GraphicsPipelineCreateInfo4& k1, const GraphicsPipelineCreateInfo4& k2) const
      {
        return
                std::tie(
                k1.enableBlending
                ,k1.enableDepthTest
                ,k1.enableDepthWrite
                ,k1.topology
                ,*reinterpret_cast<uint32_t const*>(&k1.cullMode)
                ,k1.frontFace
                ,k1.polygonMode
                ,k1.renderPass
                ,k1.vertexShaderMain
                ,k1.fragmentShaderMain
                ,k1.colorOutputs
                    )

                <

                std::tie(
                k2.enableBlending
                ,k2.enableDepthTest
                ,k2.enableDepthWrite
                ,k2.topology
                ,*reinterpret_cast<uint32_t const*>(&k2.cullMode)
                ,k2.frontFace
                ,k2.polygonMode
                ,k2.renderPass
                ,k2.vertexShaderMain
                ,k2.fragmentShaderMain
                ,k2.colorOutputs
                        );

      }
    };

    using map_type = std::map<GraphicsPipelineCreateInfo4, vk::Pipeline, SpecialCompare>;


    /**
     * @brief init
     * @param P
     *
     * Initialize the pipeline with the current state
     */
    void init(GraphicsPipelineCreateInfo4 const & P)
    {
        m_currentState = P;
        m_currentStateIterator = std::begin(m_allStates);
        m_stateChanged = true;
       // _checkAndCompileCurrentState();
    }
    void destroy()
    {
        for(auto & _p : m_allStates)
        {
            vka::System::get().destroyPipeline(_p.second);
        }
        m_allStates.clear();
        m_currentStateIterator = m_allStates.end();
        m_stateChanged = true;
    }

    /**
     * @brief getCurrentPipeline
     * @return
     *
     * Returns the current pipeline. This function may recompile the
     * pipeline if the state has changed
     */
    vk::Pipeline getCurrentPipeline()
    {
        if( m_stateChanged)
        {
            _checkAndCompileCurrentState();
        }
        return m_currentStateIterator->second;
    }



    size_t numPipelines() const
    {
        return m_allStates.size();
    }

    void setRenderPass(vk::RenderPass p)
    {
        m_currentState.renderPass = p;
        m_stateChanged = true;
    }
    void enableDepthTest(bool t)
    {
        m_currentState.enableDepthTest = t;
        m_stateChanged = true;
    }
    void enableDepthWrite(bool t)
    {
        m_currentState.enableDepthWrite = t;
        m_stateChanged = true;
    }
    void enableBlending(bool t)
    {
        m_currentState.enableBlending = t;
        m_stateChanged = true;
    }
    void setTopology(vk::PrimitiveTopology t)
    {
        m_currentState.topology = t;
        m_stateChanged = true;
    }
    void setFrontFace(vk::FrontFace t)
    {
        m_currentState.frontFace = t;
        m_stateChanged = true;
    }
    void setPolygonMode(vk::PolygonMode t)
    {
        m_currentState.polygonMode = t;
        m_stateChanged = true;
    }
    void setCullMode(vk::CullModeFlags t)
    {
        m_currentState.cullMode = t;
        m_stateChanged = true;
    }
    void setColorOutputs( std::vector<GraphicsPipelineColorOutput> colorOutputs)
    {
        m_currentState.colorOutputs = colorOutputs;
        m_stateChanged = true;
    }

private:
    bool                        m_stateChanged = false;
    map_type                    m_allStates;
    GraphicsPipelineCreateInfo4 m_currentState;
    map_type::iterator          m_currentStateIterator;

    void _checkAndCompileCurrentState()
    {
        auto f = m_allStates.find(m_currentState);
        if( f == m_allStates.end())
        {
            auto v = vka::System::get().createGraphicsPipeline(m_currentState);

            m_allStates[m_currentState] = v;

        }
        m_currentStateIterator = m_allStates.find(m_currentState);
        m_stateChanged = false;
    }
};

}


#endif
