#pragma once
#ifndef VKA_UTILS_TEXTURE_STAGER_H
#define VKA_UTILS_TEXTURE_STAGER_H

#include <queue>
#include <chrono>
#include <vka/core/System.h>
#include <vka/core/DescriptorWriter.h>

namespace vka
{


class TextureStager2
{
public:
    std::vector<vk::DescriptorImageInfo> m_textures;
    std::map< vk::ImageView, size_t>     m_viewToIndex;
    std::vector<size_t>                  m_freeIndices;
    std::set<size_t>                     m_toUpdate;

    void setMaxSize(size_t totalTextures)
    {
        m_textures.resize(totalTextures);
        m_freeIndices.clear();

        for(size_t i=0;i<totalTextures;i++)
            m_freeIndices.push_back(31u-i);
    }

    /**
     * @brief size
     * @return
     *
     * Returns the total number of textures in the
     * Stager
     */
    size_t size() const
    {
        return m_viewToIndex.size();
    }


    /**
     * @brief stageTexture
     * @param v
     * @param s
     * @param L
     * @return
     *
     * Stage a texture into the texture stager.
     *
     * The image will be placed at a paricular index into the
     * array of textures descriptor.
     *
     * It will return the index into the array of textures
     * where the image exists.
     */
    int32_t stageTexture(vk::ImageView v, vk::Sampler s, vk::ImageLayout L)
    {
        if( m_viewToIndex.size() >= m_textures.size() )
        {
            return -1;
        }

        // checks if the
        auto j = findIndex(v);
        if( j != -1)  return j;

        auto i = m_freeIndices.back();
        m_freeIndices.pop_back();

        m_textures[i].setSampler(s)
                     .setImageView(v)
                     .setImageLayout(L);

        m_viewToIndex[v] = i;

        if( m_viewToIndex.size() == 1)
        {
            _replace( vk::ImageView(), m_textures[i].imageView, m_textures[i].sampler, m_textures[i].imageLayout);
        }
        m_toUpdate.insert(i);

        return static_cast<int32_t>(i);
    }

    /**
     * @brief unstageTexture
     * @param v
     *
     * removes a texture from the array. You should make sure that
     * you should always leave one texture in the stager, otherwise
     * the entire array will be empty.
     *
     */
    void unstageTexture(vk::ImageView v)
    {
        auto i = findIndex(v);
        if( i == -1) return;

        auto j = static_cast<size_t>(i);
        _replace(v, vk::ImageView(), m_textures[j].sampler, m_textures[j].imageLayout);
        m_freeIndices.push_back(j);
        m_viewToIndex.erase(v);
        for(auto & t : m_textures)
        {
            if( t.imageView != vk::ImageView())
            {
                _replace( vk::ImageView(), t.imageView, t.sampler, t.imageLayout);
                return;
            }
        }

    }

    /**
     * @brief writeAll
     * @param writer
     * @param set
     * @param binding
     * @return
     *
     * Writes all the updated textures
     */
    size_t writeAll(std::vector<vk::WriteDescriptorSet> & writer, vk::DescriptorSet set, uint32_t binding)
    {
        size_t count=0;
        //for(auto i : m_toUpdate)
        for(uint32_t i=0;i<m_textures.size();i++)
        {
            vk::WriteDescriptorSet wr ( set,
                                        binding,
                                        static_cast<uint32_t>(i),           // array element
                                        1,         // total images
                                        vk::DescriptorType::eCombinedImageSampler,
                                        &m_textures.at(i),
                                        nullptr,
                                        nullptr );

            writer.push_back(wr);
        }
        count = m_toUpdate.size();
        m_toUpdate.clear();
        return count;
    }
protected:
    int32_t findIndex(vk::ImageView v) const
    {
        auto f = m_viewToIndex.find(v);
        if( f != m_viewToIndex.end() ) return static_cast<int32_t>(f->second);
        return -1;
    }

    void _forceUpdate(size_t i, vk::ImageView v, vk::Sampler s, vk::ImageLayout L)
    {
        m_textures[i].setSampler(s)
                     .setImageView(v)
                     .setImageLayout(L);
    }

    void _replace(vk::ImageView old,  vk::ImageView v, vk::Sampler s, vk::ImageLayout L)
    {
        for(size_t i=0;i<m_textures.size();i++)
        {
            if( m_textures[i].imageView == old)
            {
                m_textures[i].setSampler(s)
                             .setImageView(v)
                             .setImageLayout(L);
                m_toUpdate.insert(i);
            }
        }
    }
};


struct TextureStager
{
    void setMaxSize(size_t totalTextures)
    {
        m_textures.resize(totalTextures);
        m_isUpdated.clear();
        m_isUpdated.resize(totalTextures,false);
    }

    size_t maxSize() const
    {
        return m_textures.size();
    }

    /**
     * @brief stageTexture
     * @param v
     * @param s
     * @param L
     * @return
     *
     * Stage a texture by placing it somewhere in the array. Staging the same
     * texture multiple times will not duplcate the texture in the array.
     *
     * The image must be valid while the TextureStager is being used. If you
     * destroy the ImageView, you must call removeTexture( )
     */
    int32_t stageTexture(vk::ImageView v, vk::Sampler s, vk::ImageLayout L)
    {
        if( !m_isValid )
        {
            // the texture Stager is invalid if it does not contain any images

            for(size_t i=0;i<maxSize();i++)
            {
                updateImage(i, v, s, L);
            }

            m_isValid = true;
        }

        // cannot add more textures
        if( m_count >= m_textures.size())
            return -1;

        auto f = m_set.find(v);

        if( f == m_set.end() )
        {
            updateImage(m_count, v, s, L);

            auto & dd     = m_set[v];
            dd.index      = static_cast<int32_t>(m_count);
            dd.lastStaged = time_point::clock::now();
            ++m_count;
            return static_cast<int32_t>(m_count-1);
        }

        f->second.lastStaged = time_point::clock::now();

        return f->second.index;
    }

    /**
     * @brief unstageTexture
     * @param v
     * @return
     *
     * Unstage a texture. If the texture exists, it will be removed
     * from the internal array and replaced with another texture
     * that already exists in the array. The texture stager will
     * stay valid as long as there is at least 1 texture staged.
     */
    int32_t unstageTexture(vk::ImageView iv)
    {
        auto f = m_set.find(iv);
        if( f == m_set.end() )
        {
            return -1;
        }

        // This is the index we need to replace
        auto index = static_cast<size_t>( f->second.index );

        // erase the image from the set
        m_set.erase(f);

        // set this image to a blank image
        updateImage( index, vk::ImageView(), vk::Sampler(), vk::ImageLayout::eShaderReadOnlyOptimal);

        // if there is at least one image in the stager,
        // we can keep the stager valid
        if( m_set.size() )
        {
            // find the first that is available
            for(auto & v : m_set)
            {
                auto j = static_cast<size_t>( v.second.index );
                updateImage( index, m_textures[j].imageView, m_textures[j].sampler, m_textures[j].imageLayout);
                break;
            }
        }
        else
        {
            // Since there are no more images in the stager, our stager
            // is now completely invalid
            for(size_t i=0;i<maxSize();i++)
            {
                updateImage( index, vk::ImageView(), vk::Sampler(), vk::ImageLayout::eShaderReadOnlyOptimal);
            }
            m_isValid = false;
        }

        return f->second.index;
        return -1;
    }

    int32_t findTexture(vk::ImageView v) const
    {
        auto f = m_set.find(v);
        if( f == m_set.end() )
        {
            return -1;
        }
        return f->second.index;
    }
    int32_t findTexture( std::optional<vk::ImageView> const & ov) const
    {
        if( !ov ) return -1;
        return findTexture(*ov);
    }

    void clear()
    {
        m_set.clear();
        m_count=0;
    }

    size_t size() const
    {
        return m_set.size();
    }
    size_t capacity() const
    {
        return m_textures.size();
    }

    size_t writeUpdated(vka::DescriptorWriter & writer, vk::DescriptorSet set, uint32_t binding)
    {
        size_t count=0;
        for(size_t i=0 ; i < maxSize() ; i++)
        {
            if( _isUpdated(i) )
            {
                vk::WriteDescriptorSet wr ( set,
                                            binding,
                                            static_cast<uint32_t>(i),           // array element
                                            1,         // total images
                                            vk::DescriptorType::eCombinedImageSampler,
                                            &m_textures.at(i),
                                            nullptr,
                                            nullptr );
                writer.writeDescriptorSet.push_back(wr);

                m_isUpdated.at(i) = false;
                ++count;
            }
        }
        return count;
    }
    size_t writeAll(vka::DescriptorWriter & writer, vk::DescriptorSet set, uint32_t binding) const
    {
        size_t count=0;
        for(size_t i=0 ; i < maxSize() ; i++)
        {
           // if( _isUpdated(i) )
            {
                vk::WriteDescriptorSet wr ( set,
                                            binding,
                                            static_cast<uint32_t>(i),           // array element
                                            1,         // total images
                                            vk::DescriptorType::eCombinedImageSampler,
                                            &m_textures.at(i),
                                            nullptr,
                                            nullptr );

                writer.writeDescriptorSet.push_back(wr);

                //m_isUpdated[i] = false;
                ++count;
            }
        }
        return count;
    }
    bool _isUpdated(size_t i) const
    {
        return m_isUpdated[i];
    }

    void updateImage(size_t i, vk::ImageView v, vk::Sampler s, vk::ImageLayout L)
    {
        auto & V = m_textures.at(i);

        V.sampler         =  s;
        V.imageView       =  v;
        V.imageLayout     =  L;
        m_isUpdated.at(i) = true;
    }

    void write(vka::DescriptorWriter & writer, vk::DescriptorSet set, uint32_t binding)
    {
        auto u = m_count;
        auto last = m_count-1;
        while(u!=m_textures.size())
        {
            m_textures[u] = m_textures[last];
            ++u;
        }
        auto siz = m_textures.size();

        vk::WriteDescriptorSet wr ( set,
                                    binding,
                                    0,           // array element
                                    static_cast<uint32_t>(siz),         // total images
                                    vk::DescriptorType::eCombinedImageSampler,
                                    m_textures.data(),
                                    nullptr,
                                    nullptr );
        writer.writeDescriptorSet.push_back(wr);
    }

private:
    using time_point = std::chrono::system_clock::time_point;
    std::vector<vk::DescriptorImageInfo> m_textures;
    std::vector<bool>                    m_isUpdated;

    uint32_t                             m_count=0;

    struct  XX
    {
        int32_t    index = -1;
        time_point lastStaged;
    };

    std::map<vk::ImageView,  XX>    m_set;
    bool m_isValid = false;
};

}

#endif
