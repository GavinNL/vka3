#ifndef VKA_UTILS_FRAMEGRAPH_H
#define VKA_UTILS_FRAMEGRAPH_H

#include <iostream>
#include <any>
#include <thread>
#include <mutex>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/RenderPassCreateInfo.h>
#include <vka/core/DescriptorWriter.h>

#if !defined VKA_FG_CRIT
    #define VKA_FG_CRIT(...)
#endif

#if !defined VKA_FG_INFO
    #define VKA_FG_INFO(...)
#endif


namespace vka
{


enum class ResourceUsage : uint32_t
{
    read_only  = 1,
    write_only = 2,
    read_write = 4
};


struct AttachmentResource  // color output
{
    vk::Format   format;

    std::optional<vk::Extent2D> extents; // if not given, assumes swapchain size

    //------- private ----
    std::optional<size_t>       physicalResourceIndex;
};

// the actual physical properties of the resource
// multiple attachment resources can
// point to a single physical resource
struct AttachmentPhysicalResource  // color output
{
    vk::Extent3D        _extents;
    vk::Format          _format;
    vk::Image           _image;
    vk::ImageView       _view;
    vk::Sampler         _sampler;
    vk::DeviceMemory    _mem;
    vk::ImageUsageFlags _usage;
};

struct FrameGraph;

struct TaskVarBase
{
    FrameGraph * graph;
    std::shared_ptr<std::map< std::string,  std::any>> m_localResources_p;

    //====================================================================
    // Local resources are stored in a map<string,any>
    //====================================================================
    template<typename T>
    T& emplaceLocalResource(const std::string & name)
    {
        auto & m_localResources = *m_localResources_p;
        m_localResources[name].emplace<T>();
        return *std::any_cast<T>( &m_localResources[name] );
    }
    template<typename T>
    T& getOrEmplaceLocalResource(const std::string & name)
    {
        auto & m_localResources = *m_localResources_p;
        auto & x = m_localResources[name];
        if( !x.has_value() )
        {
            x.emplace<T>();
        }
        return *std::any_cast<T>( &x );
    }
    template<typename T>
    T& getLocalResource(const std::string & name)
    {
        auto & m_localResources = *m_localResources_p;
        auto & x = m_localResources[name];
        if( !x.has_value() )
        {
            throw std::runtime_error("Local variable, " + name + ", has not been emplaced");
        }
        return *std::any_cast<T>( &x );
    }
    bool localResourceHasValue(const std::string & name) const
    {
        auto & m_localResources = *m_localResources_p;
        auto it = m_localResources.find(name);
        if( it == m_localResources.end() ) return false;
        return it->second.has_value();
    }
    //====================================================================
    template<typename resourceType>
    resourceType& emplaceResource(const std::string & name)
    {
        // get the resource and set the "created" flag to true
        auto & a = _getHostResource(name, true);
        if( !a.has_value() )
            a.emplace<resourceType>();
        return *std::any_cast<resourceType>(&a);
    }

    template<typename resourceType>
    resourceType& getHostResource(const std::string & name)
    {
        auto & a = _getHostResource(name, {});
        if( a.has_value())
        {
            return *std::any_cast<resourceType>( &a );
        }
        throw std::runtime_error("This resource has not been created yet. Use emplaceResource() to create it first");
    }

    template<typename resourceType>
    resourceType& getOrEmplaceResource(const std::string & name)
    {
        auto & a = _getHostResource(name, {});
        if( a.has_value())
        {
            return *std::any_cast<resourceType>( &a );
        } else
        {
            return emplaceResource<resourceType>(name);
        }
    }
protected:
    std::any& _getHostResource(const std::string & name, std::optional<bool> createdValue);
};

// This struct is passed into the callback functin
// to write the command buffers.
struct RenderTaskVar : public TaskVarBase
{
    vka::CommandBuffer                       commandBuffer;
    std::optional<vk::RenderPassBeginInfo>   renderPassBeginInfo;           // if this task contains output resources, this
                                                                            // optional will be filled.


    std::vector<vk::ClearValue>              clearValues;

    std::vector<vk::Format>                  inputAttachmentImageFormat;
    std::vector<vk::Image>                   inputAttachmentImage;
    std::vector<vk::ImageView>               inputAttachmentImageView;
    std::vector<vk::Sampler>                 inputAttachmentImageSampler;
    std::vector<vk::Extent3D>                inputAttachmentImageExtents;

    vk::DescriptorSet                        inputAttachmentDescriptorSet;  // a descriptor set which is created for you to use
                                                                            // in your shaders for this task. it is an array of
                                                                            // images at binding=0;


    std::vector<vk::Format>                  outputAttachmentImageFormat;
    std::vector<vk::Image>                   outputAttachmentImage;
    std::vector<vk::ImageView>               outputAttachmentImageView;
    vk::Extent2D                             imageExtent;
};

struct FrameGraph
{

    struct Task
    {
        struct OutResourceInfo
        {
            std::string     name;
            vk::ImageLayout inLayout;
            vk::ImageLayout outLayout;
        };
        struct InResourceInfo
        {
            std::string     name;
            vk::ImageLayout inLayout;

            vk::Sampler     sampler;
        };

        std::string                        name;
        std::vector< InResourceInfo  >     inputResources;
        std::vector< OutResourceInfo >     outputResources;

        std::vector< std::string > inHostResource;
        std::vector< std::string > outHostResource;

        std::shared_ptr<std::map< std::string,  std::any>> m_localResources_p = std::make_shared< std::map< std::string,  std::any> > ();
        // the process callback is used to do any non-graphics related
        // processes, for example, determinign which objects are going
        // to be rendered, perform any physics calculations
        // the order in which these callbacks are called are based
        // on the hostDependencies and hostResources that are produced
        // by each task.

        // The writeCommandCallback is used to
        // provide a function that writes all the commands to the command buffer
        // when this callback is executed, the renderpass has already been initated
        // and there is no need to call cmd.beginRenderPass( ) or cmd.endRenderPass( )
        std::function< void(RenderTaskVar&) >                      m_writeCommandCallback;
        std::function< vk::ClearColorValue(uint32_t, vk::Format) > m_clearColorCallback;
        std::function< vk::ClearDepthStencilValue(vk::Format) >    m_clearDepthCallback;
        std::function< void(vka::DescriptorWriter&) >              m_descriptorUpdateCallback;

        std::function< void(RenderTaskVar&) >                      destroyCallback;
        std::function< void(RenderTaskVar&) >                      initCallback;

        bool m_isInitialized = false;
        bool m_isDestroyed   = true;

        FrameGraph * m_graph;
        bool         m_isRenderTask=false;
        Task()
        {
            inputResources.reserve(10);
            outputResources.reserve(10);
        }
        Task& addColorOutput(const std::string & resourceName, vk::Format format, std::optional<vk::Extent2D> extent = {});
        Task& addAttachmentInput(const std::string & resourceName);


        Task& addHostResourceInput( const std::string & resourceName);
        Task& addHostResourceOutput(const std::string & resourceName);


        vk::Format getOutputFormat(size_t index) const;
        size_t     getOutputColorCount() const
        {
            return outputResources.size();
        }

        // add the callback which will be used to
        // write the commands to the command buffer.
        // the callback function should NOT begin or end the renderpass
        // that will be done for you.
        template<typename Callable>
        Task& setCommandBufferWrite(Callable  && c)
        {
            m_writeCommandCallback = c;
            return *this;
        }

        template<typename Callable>
        Task& setDescriptorUpdate(Callable  && c)
        {
            m_descriptorUpdateCallback = c;
            return *this;
        }

        template<typename Callable>
        Task& setClearColorCallback(Callable  && c)
        {
            m_clearColorCallback = c;
            return *this;
        }

        template<typename Callable>
        Task& setClearDepthCallback(Callable  && c)
        {
            m_clearDepthCallback = c;
            return *this;
        }

        template<typename Callable>
        Task& setInitCallback(Callable  && c)
        {
            initCallback = c;
            return *this;
        }
        template<typename Callable>
        Task& setDestroyCallback(Callable  && c)
        {
            destroyCallback = c;
            return *this;
        }
        auto const & getOutputResource(const std::string & _name)
        {
            for(auto & o : outputResources)
            {
                if( _name == o.name)
                {
                    auto & pD = m_graph->m_physicalResources.at(*m_graph->m_resources.at(_name).resourceInfo.physicalResourceIndex);

                    return pD;
                }
            }
            throw std::out_of_range("That resource does not exist");
        }

        vk::RenderPass getRenderPass() const
        {
            return m_renderPass;
        }
    protected:
        bool                         m_renderPassNeedsRebuild=false;
        vk::RenderPass               m_renderPass;
        vk::Framebuffer              m_framebuffer;
        std::vector<vk::ClearValue>  m_clearValues;
        vk::Extent2D                 m_extent;

        vk::DescriptorSet            m_inputAttachmentDescriptorSet;
        friend struct FrameGraph;
    };

    struct Resource
    {
        std::vector<std::string> writtenBy; // which task writes to this resource?
        std::vector<std::string> readBy;    // which task reads from this resource?
        ResourceUsage            usage;

        AttachmentResource       resourceInfo;
    };

    struct HostResource
    {
        std::vector<std::string> writtenBy; // which task writes to this resource?
        std::vector<std::string> readBy;    // which task reads from this resource?

        bool                     created=false;
        std::any                 resource;
        std::mutex               mutex;
    };

    void setSwapchainExtent( vk::Extent2D e)
    {
        m_swapChainSize        = e;
        m_swapChainSizeChanged = true;
    }

    template<typename callable>
    Task& addRenderTask( const std::string & name, callable && c)
    {
        addTask(name).m_writeCommandCallback = c;
        return m_tasks[name];
    }

    Task& addTask(const std::string & name)
    {
        //m_needsRecompile = true;
        m_tasks[name].name   = name;
        m_tasks[name].m_graph=this;
        return m_tasks[name];
    }

    Task& getTask(const std::string & taskname)
    {
        return m_tasks.at(taskname);
    }

    void setFinalOutput(const std::string & resourceName)
    {
        m_finalOutput = resourceName;
    }

    bool needRecompile() const
    {
        return m_needsRecompile;
    }
    /**
     * @brief compile
     *
     * Compiles the graph. This m
     */
    void compile()
    {
        if( !m_needsRecompile ) return;

        assert(traverse());
        m_taskExecutionOrder = calculateTaskOrder();

        // figure out which resources are read-write, read-only, or write-only.
        for(auto & r : m_resources)
        {
            std::string type;
            if( r.second.readBy.size() != 0u && r.second.writtenBy.size() != 0)  r.second.usage = ResourceUsage::read_write;//"read-write";
            if( r.second.readBy.size() != 0u && r.second.writtenBy.size() == 0)  r.second.usage = ResourceUsage::read_only; //"read-only";
            if( r.second.readBy.size() == 0u && r.second.writtenBy.size() != 0)  r.second.usage = ResourceUsage::write_only;//"write-only";
        }

        // set the output resources final layout to undefined
        for(auto & t : m_tasks)
        {

            t.second.m_isRenderTask=true;
            if( t.second.outputResources.size() == 0 && t.second.inputResources.size() == 0)
                t.second.m_isRenderTask=false;

            for( size_t i = 0 ; i < t.second.outputResources.size() ; i++)
            {
                auto & res = m_resources.at( t.second.outputResources[i].name );
                vk::ImageLayout L = vk::ImageLayout::eUndefined;

                //t.second.m_CmdRecord.outputAttachmentImageFormat.push_back(res.resourceInfo.format);

                switch( res.usage )
                {
                    case ResourceUsage::read_write: L = vk::ImageLayout::eShaderReadOnlyOptimal; break;
                    case ResourceUsage::read_only:  L = vk::ImageLayout::eShaderReadOnlyOptimal; break;
                    case ResourceUsage::write_only: L = vk::ImageLayout::eColorAttachmentOptimal; break;
                }
                switch( res.resourceInfo.format )
                {
                    case vk::Format::eD16Unorm         :
                    case vk::Format::eD32Sfloat        :
                        if( res.usage == ResourceUsage::read_only )  L = vk::ImageLayout::eDepthStencilReadOnlyOptimal;
                        if( res.usage == ResourceUsage::write_only ) L = vk::ImageLayout::eDepthStencilAttachmentOptimal;
                        if( res.usage == ResourceUsage::read_write ) L = vk::ImageLayout::eShaderReadOnlyOptimal;
                        break;
                    case vk::Format::eD16UnormS8Uint   :
                    case vk::Format::eD24UnormS8Uint   :
                    case vk::Format::eD32SfloatS8Uint  :
                        if( res.usage == ResourceUsage::read_only )  L = vk::ImageLayout::eDepthStencilReadOnlyOptimal;
                        if( res.usage == ResourceUsage::write_only ) L = vk::ImageLayout::eDepthStencilAttachmentOptimal;
                        if( res.usage == ResourceUsage::read_write ) L = vk::ImageLayout::eShaderReadOnlyOptimal;
                        break;
                    default:
                        break;
                }
                t.second.outputResources[i].outLayout = L;
            }

        }
        m_needsRecompile = false;
    }

    static bool isDepthFormat(vk::Format f)
    {
        switch( f )
        {
            case vk::Format::eD16Unorm         :
            case vk::Format::eD32Sfloat        :
            case vk::Format::eD16UnormS8Uint   :
            case vk::Format::eD24UnormS8Uint   :
            case vk::Format::eD32SfloatS8Uint  :
                return true;
            default:
                return false;
                break;
        }
    }

    void init()
    {
        if( m_needsRecompile )
        {
            throw std::runtime_error("Cannot execute this render graph. The graph needs to be recompiled");
        }

        for(auto & taskName : m_taskExecutionOrder)
        {
            auto & task = m_tasks.at(taskName);
            if( !task.m_isInitialized)
            {
                task.m_isInitialized = true;
                task.m_isDestroyed   = false;

                RenderTaskVar V;

                V.clearValues        = task.m_clearValues;
                V.imageExtent        = task.m_extent;
                V.graph              = this;
                V.m_localResources_p = task.m_localResources_p;

                if( task.initCallback )
                {
                    task.initCallback(V);
                }
            }
        }
    }


    void execute(vk::CommandBuffer cmd)
    {
        if( m_needsRecompile )
        {
            throw std::runtime_error("Cannot execute this render graph. The graph needs to be recompiled");
        }

        for(auto & taskName : m_taskExecutionOrder)
        {
            _executeTask( m_tasks.at(taskName), cmd);
        }
    }


    std::vector<std::string> calculateTaskOrder() const
    {
        std::vector< std::string > tasks;
        std::vector< std::string > tasks2;
        std::set<std::string> _s;
        std::set<std::string> _s2;

        if( m_resources.count(m_finalOutput))
        {
            _traverseResource(m_finalOutput, _s,
                              [&](auto & ss)
            {
                tasks.push_back(ss);
            });
        }
        else if( m_hostResources.count(m_finalOutput))
        {
            _traverseHostResource(m_finalOutput, _s,
                                  [&](auto & ss)
            {
                tasks.push_back(ss);
            });
        }
        std::reverse( tasks.begin(), tasks.end() );

        for(auto & t : tasks)
        {
            if( _s2.count(t) == 0)
            {
                _s2.insert(t);
                tasks2.push_back(t);
            }
        }
        return tasks2;
    }


    /**
     * @brief checkReinitalizeResources
     *
     * Checks if any of the resources need to be reinitialized.
     * This usually happens when you change the swapchain size.
     * If it has been changed, the resources will be reinitialized.
     *
     */
    void checkReinitalizeResources()
    {
        if( m_swapChainSizeChanged)
        {
            std::set<size_t> physicalResourcesThatNeedToBeDestroyed;
            std::vector<std::string> tasksThatNeedNewFrameBuffers;

            std::map<size_t, ResourceUsage> physicalResourceUsage;
            std::map<size_t, vk::Extent2D> physicalResourceExtents;
            std::map<size_t, vk::Format>   physicalResourceFormat;
            std::map<size_t, std::string>   physicalResourceName;

            // if the swapchian has changed sizes, we need to go through all
            // the renderTasks and see if any of them have output resources
            // that depend on the swapchain size. If so, we need to
            // destroy the framebuffers, recreate the output images
            // and then recreate the framebuffer.
            for(auto & t : m_tasks)
            {
                bool xx=false;
                for(auto & r : t.second.outputResources)
                {
                    auto & vR = m_resources.at(r.name);

                    // if this physical resource has not been initialized already,
                    // we'll need to do it
                    if( !vR.resourceInfo.physicalResourceIndex )
                    {
                        m_physicalResources.emplace_back();
                        vR.resourceInfo.physicalResourceIndex = m_physicalResources.size()-1;
                        physicalResourcesThatNeedToBeDestroyed.insert( *vR.resourceInfo.physicalResourceIndex );
                        auto ind = *vR.resourceInfo.physicalResourceIndex;

                        physicalResourceUsage[ ind ] = vR.usage;
                        physicalResourceFormat[ind ] = vR.resourceInfo.format;
                        physicalResourceName[ind ]   = r.name;

                        if( vR.resourceInfo.extents )
                        {
                            physicalResourceExtents[ind] = *vR.resourceInfo.extents;
                        }
                        else
                        {
                            physicalResourceExtents[ind] = m_swapChainSize;
                        }
                        xx = true;
                    }

                    // if this resource's extents are not set, that means
                    // we should use teh swapchain size
                    if( !vR.resourceInfo.extents )
                    {
                        auto ind = *vR.resourceInfo.physicalResourceIndex;
                        physicalResourcesThatNeedToBeDestroyed.insert( *vR.resourceInfo.physicalResourceIndex );
                        physicalResourceUsage[ ind ] = static_cast<ResourceUsage>( static_cast<uint32_t>(physicalResourceUsage[ ind ]) | static_cast<uint32_t>( vR.usage ) );
                        physicalResourceExtents[ind] = m_swapChainSize;
                        physicalResourceFormat[ind]  = vR.resourceInfo.format;
                        xx = true;
                    }
                }
                if( xx )
                {
                    tasksThatNeedNewFrameBuffers.push_back(t.first);
                }
            }

            // destroy the frame buffers
            for(auto & t : tasksThatNeedNewFrameBuffers)
            {
                auto & T = m_tasks.at(t);
                if( T.m_framebuffer)
                {
                    vka::System::get().destroyFramebuffer(T.m_framebuffer);
                    T.m_framebuffer = vk::Framebuffer();
                }
            }

            for(auto x : physicalResourcesThatNeedToBeDestroyed)
            {
                auto & R = m_physicalResources.at(x);
                VKA_FG_INFO("Allocating Resources for: {}", physicalResourceName[x]);
                _reallocatePhysicalResource( R, physicalResourceFormat.at(x),  physicalResourceExtents.at(x), physicalResourceUsage.at(x) );
            }

            for(auto & t : tasksThatNeedNewFrameBuffers)
            {
                auto & T = m_tasks.at(t);
                _buildFrameBuffer( T );
            }

            for(auto & T : m_tasks)
            {
                for(auto & x : T.second.inputResources)
                {
                    if( !x.sampler)
                        x.sampler = _createImageSampler();
                }
            }

            m_swapChainSizeChanged = false;
            _freeDescriptorSets();
            _createDescriptorSets();
        }
    }

    void _destroyTask(Task & T)
    {
        auto & f = T.destroyCallback;

        if( !T.m_isDestroyed )
        {
            if( T.m_isInitialized )
            {
                T.m_isDestroyed   = true;
                T.m_isInitialized = false;
                RenderTaskVar V;
                V.clearValues        = T.m_clearValues;
                V.imageExtent        = T.m_extent;
                V.graph              = this;
                V.m_localResources_p = T.m_localResources_p;

                if( f )
                {
                    f( V );
                }
            }
        }
    }
    /**
     * @brief destroy
     *
     * Completely destroy all resources from this graph.
     */
    void destroy()
    {
        for(auto & m : m_taskExecutionOrder)
        {
            _destroyTask( m_tasks.at(m) );
        }
        _destroyFrameBuffers();
        _destroyRenderPasses();
        _freeDescriptorSets();
        for(auto & m : m_physicalResources)
        {
            _destroyAttachmentResource(&m);
        }
        m_physicalResources.clear();

        for(auto & t : m_tasks)
        {
            for(auto & x : t.second.inputResources)
            {
                if( x.sampler )
                {
                    vka::System::get().destroySampler(x.sampler);
                    x.sampler = vk::Sampler();
                }
            }
        }

        vka::System::get().destroyDescriptorPool(m_descriptorPool);
        m_descriptorPool = vk::DescriptorPool();

        m_resources.clear();
        m_tasks.clear();
        m_finalOutput.clear();
        m_taskExecutionOrder.clear();
    }

    void dotGraph(std::ostream & out) const
    {
        std::cout << "digraph finite_state_machine {                         " << std::endl;
        std::cout << "    rankdir=LR;                                        " << std::endl;
        //std::cout << "    size="8,5"                                         " << std::endl;
        std::cout << "    node [shape = doublecircle]; ";
        for(auto & t:m_tasks)
        {
            out << t.first << " ";
        }
        out << "\n";


        std::cout << "    node [shape = Msquare]; ";
        for(auto & t : m_resources)
        {
            out << t.first << " ";
        }
        out << "\n";

        for(auto & t : m_tasks)
        {
            for(auto & input : t.second.inputResources)
            {
                out << "    " << input.name << " -> " << t.second.name << "\n";
            }
            for(auto & output : t.second.outputResources)
            {
                out << "    " << t.second.name << " -> " << output.name << "\n";
            }
        }

        out << "    node [shape = rect]; ";
        for(auto & t : m_resources)
        {
            std::cout << t.first << " ";
        }
        out << "\n";

        for(auto & t : m_tasks)
        {
            for(auto & input : t.second.inHostResource)
            {
                out << "    " << input << " -> " << t.second.name << "\n";
            }
            for(auto & output : t.second.outHostResource)
            {
                out  << "    " << t.second.name << " -> " << output << "\n";
            }

        }

        out  << "}                                                      \n" << std::endl;
    }

    template<typename T>
    T& getOrEmplaceHostResource(const std::string & name)
    {
        auto & H = m_hostResources[name];
        if( !H.resource.has_value() )
        {
            H.resource.emplace<T>();
            return *std::any_cast<T>(&H.resource);
        }
        return *std::any_cast<T>(&H.resource);
    }

    bool hostResourceHasValue(const std::string & name) const
    {
        auto it = m_hostResources.find(name);
        if( it == m_hostResources.end() ) return false;
        return it->second.resource.has_value();
    }
    HostResource& getHostResource(const std::string & name)
    {
        return m_hostResources.at(name);
    }

    bool isHostResourceCreated(const std::string & name) const
    {
        auto it = m_hostResources.find(name);
        if( it == m_hostResources.end() ) return false;
        return it->second.created;
    }

protected:
    vk::Extent2D                                        m_swapChainSize;
    bool                                                m_swapChainSizeChanged=true;
    std::map< std::string,  Resource>                   m_resources;
    std::map< std::string,  Task>                       m_tasks;
    std::string                                         m_finalOutput;

    std::vector<std::string>                            m_taskExecutionOrder;

    std::map< std::string,  HostResource>               m_hostResources;
    //------------------------------------------------------------------------

    std::vector< AttachmentPhysicalResource>            m_physicalResources;

    vk::DescriptorPool m_descriptorPool;

    // create the descriptor sets for each task
    // these descriptor sets are for the input images.
    //
    // this must be called after the images have been created
    void _createDescriptorSets()
    {
        // check if any of the tasks actually need descriptors
        // they only need descriptors if the task requires
        // an attachment input.
        bool createDescriptorPool = false;
        for(auto & t : m_tasks)
        {
            if( t.second.inputResources.size() )
            {
                createDescriptorPool = true;
            }
        }
        // no need to continue since do not need to create
        // any descriptor sets
        if( !createDescriptorPool) return;

        if( !m_descriptorPool)
        {
            vka::DescriptorPoolCreateInfo2 I;
            I.maxSets = 25;
            I.poolSizes.push_back( {vk::DescriptorType::eCombinedImageSampler, 1000 });
            m_descriptorPool = vka::System::get().createDescriptorPool(I);
        }

        vka::DescriptorWriter dwriter;

        for(auto & t : m_tasks)
        {
            uint32_t totalInputImages =  static_cast<uint32_t>( t.second.inputResources.size() );
            if( totalInputImages == 0)
                continue;
            // Allocate a DescriptorSet
            vka::DescriptorSetLayoutCreateInfo2 L;
            L.bindings.push_back( vk::DescriptorSetLayoutBinding(0, vk::DescriptorType::eCombinedImageSampler, totalInputImages, vk::ShaderStageFlagBits::eFragment) );
            t.second.m_inputAttachmentDescriptorSet = vka::System::get().allocateDescriptorSet( m_descriptorPool, L );

            if( totalInputImages > 0)
            {
                uint32_t i=0;
                vk::DescriptorImageInfo * pImageInfo = dwriter.allocateDescriptorImageInfo( totalInputImages );

                std::vector<vk::ImageView> imgArray;
                for(auto & x : t.second.inputResources)
                {
                    auto & pR = m_physicalResources.at(*m_resources[x.name].resourceInfo.physicalResourceIndex);

                    pImageInfo[i].imageView   = pR._view;
                    pImageInfo[i].imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
                    pImageInfo[i].sampler     = x.sampler;
                    ++i;
                }
                vk::WriteDescriptorSet wr ( t.second.m_inputAttachmentDescriptorSet,
                                            0,
                                            0,           // array element
                                            static_cast<uint32_t>(i),         // total images
                                            vk::DescriptorType::eCombinedImageSampler,
                                            pImageInfo,
                                            nullptr,
                                            nullptr );


                dwriter.writeDescriptorSet.push_back(wr);
            }

            vka::System::get().updateDescriptorSets( dwriter.writeDescriptorSet );
        }

    }


    void _buildRenderPass(Task & task)
    {
        if( task.outputResources.size() == 0)
        {
            return;
        }
        if( !task.m_renderPassNeedsRebuild )
            return;

        VKA_FG_INFO("Building Render pass for task: {}", task.name );
        vka::RenderPassCreateInfo2 rp;

        for(auto & o : task.outputResources)
        {
            auto & res = m_resources.at(o.name);

            VKA_FG_INFO("      {} --> {}  ", to_string( res.resourceInfo.format) , to_string(o.outLayout) );

            if( isDepthFormat(res.resourceInfo.format) )
            {
                rp.addDepthStenchilAttachment(res.resourceInfo.format,         o.outLayout);
            }
            else
            {
                rp.addColorAttachment(res.resourceInfo.format,         o.outLayout);
            }
            //-------------------
        }
        assert( !task.m_renderPass );
        task.m_renderPass = vka::System::get().createRenderPass(rp);
        task.m_renderPassNeedsRebuild = false;
    }


    void _buildFrameBuffer(Task & task)
    {
        if( task.outputResources.size() ==0 )
            return;

        VKA_FG_INFO("Building Framebuffer for task: {}", task.name );

        vka::FramebufferCreateInfo2 fci;

        {
            auto & res              = m_resources.at( task.outputResources.front().name);
            auto & physicalResource = m_physicalResources.at( *res.resourceInfo.physicalResourceIndex );

            assert( physicalResource._extents.width * physicalResource._extents.height != 0);

            fci.width               = physicalResource._extents.width;
            fci.height              = physicalResource._extents.height;
        }

        if( !task.m_renderPass)
            _buildRenderPass(task);

        fci.renderPass = task.m_renderPass;

        fci.layers     = 1;

        for(auto & o : task.outputResources)
        {
            auto & res = m_resources.at(o.name);

            auto & physicalResource = m_physicalResources.at( *res.resourceInfo.physicalResourceIndex );

            fci.attachments.push_back( physicalResource._view  ) ;
        }

        task.m_extent = vk::Extent2D(fci.width, fci.height);
        task.m_framebuffer = vka::System::get().createFramebuffer(fci);
    }

    vk::Sampler _createImageSampler()
    {
        auto mipLevels = 1;
        vk::SamplerCreateInfo sampinfo;

        sampinfo.magFilter               = vk::Filter::eLinear;
        sampinfo.minFilter               = vk::Filter::eLinear;
        sampinfo.mipmapMode              = vk::SamplerMipmapMode::eLinear ;
        sampinfo.addressModeU            = vk::SamplerAddressMode::eRepeat;
        sampinfo.addressModeV            = vk::SamplerAddressMode::eRepeat;
        sampinfo.addressModeW            = vk::SamplerAddressMode::eRepeat;
        sampinfo.mipLodBias              = 0.0f ;
        sampinfo.anisotropyEnable        = VK_FALSE ;
        sampinfo.maxAnisotropy           = 1 ;
        sampinfo.compareEnable           = VK_FALSE ;
        sampinfo.compareOp               = vk::CompareOp::eAlways ;
        sampinfo.minLod                  = 0.0f ;
        sampinfo.maxLod                  = static_cast<float>(mipLevels) ;
        sampinfo.borderColor             = vk::BorderColor::eIntOpaqueBlack ;
        sampinfo.unnormalizedCoordinates = VK_FALSE;

        return vka::System::get().createSampler( vka::SamplerCreateInfo2(sampinfo) );
    }

    void _createImageView( AttachmentPhysicalResource & r)
    {
        vk::ImageViewCreateInfo colorAttachmentView;

        vk::ImageAspectFlags aspectMask;
        switch( r._format )
        {
            case vk::Format::eD16Unorm         :
            case vk::Format::eD32Sfloat        :
            case vk::Format::eD16UnormS8Uint   :
            case vk::Format::eD24UnormS8Uint   :
            case vk::Format::eD32SfloatS8Uint  :
                aspectMask = vk::ImageAspectFlagBits::eDepth;
                break;
            default:
                aspectMask = vk::ImageAspectFlagBits::eColor;
                break;
        }

        colorAttachmentView.format = r._format;
        colorAttachmentView.components = {
                                            vk::ComponentSwizzle::eR,
                                            vk::ComponentSwizzle::eG,
                                            vk::ComponentSwizzle::eB,
                                            vk::ComponentSwizzle::eA
                                        };
        colorAttachmentView.subresourceRange.aspectMask     = aspectMask;// VK_IMAGE_ASPECT_COLOR_BIT;
        colorAttachmentView.subresourceRange.baseMipLevel   = 0;
        colorAttachmentView.subresourceRange.levelCount     = 1;
        colorAttachmentView.subresourceRange.baseArrayLayer = 0;
        colorAttachmentView.subresourceRange.layerCount     = 1;
        colorAttachmentView.viewType                        = vk::ImageViewType::e2D;// VK_IMAGE_VIEW_TYPE_2D;
        colorAttachmentView.image                           = r._image;

        auto v = vka::System::get().createImageView(colorAttachmentView);
        r._view = v;
        r._sampler = createInputAttachmentSampler(1.0f);
        VKA_FG_INFO(" Creating Image View ({}) for Image {} ", static_cast<void*>(r._view), static_cast<void*>(r._image));
    }

    vk::Sampler createInputAttachmentSampler(float mipLevels)
    {
        static_assert( sizeof(vka::SamplerCreateInfo2) == 80, "" );

        vk::SamplerCreateInfo ci;
        ci.magFilter               =  vk::Filter::eNearest ;
        ci.minFilter               =  vk::Filter::eNearest ;
        ci.mipmapMode              =  vk::SamplerMipmapMode::eNearest ;
        ci.addressModeU            =  vk::SamplerAddressMode::eRepeat ;
        ci.addressModeV            =  vk::SamplerAddressMode::eRepeat ;
        ci.addressModeW            =  vk::SamplerAddressMode::eRepeat ;
        ci.mipLodBias              =  0.0f  ;
        ci.anisotropyEnable        =  VK_FALSE;
        ci.maxAnisotropy           =  1 ;
        ci.compareEnable           =  VK_FALSE ;
        ci.compareOp               =  vk::CompareOp::eAlways ;
        ci.minLod                  =  0 ;
        ci.maxLod                  =  static_cast<float>(mipLevels) ;
        ci.borderColor             =  vk::BorderColor::eIntOpaqueBlack ;
        ci.unnormalizedCoordinates =  VK_FALSE ;

        return vka::System::get().createSampler( vka::SamplerCreateInfo2(ci) );
    }

    void _reallocatePhysicalResource( AttachmentPhysicalResource & R, vk::Format f, vk::Extent2D extents, ResourceUsage R_usage)
    {
        vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eColorAttachment;

        switch( f )
        {
            case vk::Format::eD16Unorm         :
            case vk::Format::eD32Sfloat        :
            case vk::Format::eD16UnormS8Uint   :
            case vk::Format::eD24UnormS8Uint   :
            case vk::Format::eD32SfloatS8Uint  :
                usage= vk::ImageUsageFlagBits::eDepthStencilAttachment;
                break;
            default:
                usage = vk::ImageUsageFlagBits::eColorAttachment;
                break;
        }

        if( R_usage == ResourceUsage::read_write )
        {
            usage |= vk::ImageUsageFlagBits::eSampled;
            usage |= vk::ImageUsageFlagBits::eTransferSrc;
        }

        auto format  = f;

        //================================================================
        // Destroy the view/image and memory
        //================================================================
        if( R._view )
        {
            vka::System::get().destroyImageView(R._view);
            R._view  = vk::ImageView();
        }
        //if( R._sampler )
        //{
        //    vka::System::get().destroySampler(R._sampler);
        //    R._sampler = vk::Sampler();
        //}
        if( R._image)
        {
            vka::System::get().destroyImage(R._image);
            R._image = vk::Image();
        }
        try
        {
            vka::System::get().freeMemory(  R._mem  );
            R._mem   = vk::DeviceMemory();
        }
        catch ( std::exception & e )
        {
            R._mem   = vk::DeviceMemory();
            VKA_FG_CRIT("Cannot free this memory. It's being used by something. This is a memory leak!");
        }
        //================================================================

        R = _allocateAttachmentResource( format, extents, usage);
        VKA_FG_INFO(" -- resource allocated:  usage {}", to_string(usage));
    }

    AttachmentPhysicalResource _allocateAttachmentResource( vk::Format format, vk::Extent2D extent, vk::ImageUsageFlags usage )
    {
        vk::ImageCreateInfo create_info;

        create_info.imageType     = vk::ImageType::e2D;// VK_IMAGE_TYPE_2D;
        create_info.extent.width  = extent.width;
        create_info.extent.height = extent.height;
        create_info.extent.depth  = 1;
        create_info.mipLevels     = 1;
        create_info.arrayLayers   = 1;
        create_info.format        = format;
        create_info.tiling        = vk::ImageTiling::eOptimal;
        create_info.initialLayout = vk::ImageLayout::eUndefined;
        create_info.usage         = usage;
        create_info.samples       = vk::SampleCountFlagBits::e1;
        create_info.sharingMode   = vk::SharingMode::eExclusive;

        auto image = vka::System::get().createImage(create_info);

        if( !image )
            throw std::runtime_error("Failed to create image for textureMemoryPool");

        vk::DeviceMemory mem = vka::System::get().allocateMemoryForImage(image,  vk::MemoryPropertyFlagBits::eDeviceLocal);

        vka::System::get().bindImageMemory(image, mem, 0);

        AttachmentPhysicalResource res;
        res._extents = vk::Extent3D( extent.width, extent.height, 1);
        res._mem     = mem;
        res._image   = image;
        res._format  = format;
        res._usage   = usage;
        _createImageView(res);
        return res;
    }

    void _freeDescriptorSets()
    {
        for(auto & t : m_tasks)
        {
            if( t.second.m_inputAttachmentDescriptorSet )
            {
                vka::System::get().destroyDescriptorSet(t.second.m_inputAttachmentDescriptorSet);
                t.second.m_inputAttachmentDescriptorSet = vk::DescriptorSet();
            }
        }
    }

    void _destroyAttachmentResource(AttachmentPhysicalResource * R)
    {
        vka::System::get().destroyImageView(R->_view);
        R->_view  = vk::ImageView();

        vka::System::get().destroyImage(R->_image);
        R->_image = vk::Image();

        vka::System::get().destroySampler(R->_sampler);
        R->_sampler = vk::Sampler();

        try
        {
            vka::System::get().freeMemory(  R->_mem  );
            R->_mem   = vk::DeviceMemory();
        }
        catch ( std::exception & e )
        {
            std::cout << "WARNING: Cannot free this memory. It's being used by something." << std::endl;
        }

    }

    void _destroyFrameBuffers()
    {
        for(auto & t : m_tasks)
        {
            if( t.second.m_framebuffer)
            {
                vka::System::get().destroyFramebuffer(t.second.m_framebuffer);
                t.second.m_framebuffer = vk::Framebuffer();
            }
        }
    }

    void _destroyRenderPasses()
    {
        for(auto & t : m_tasks)
        {
            if( t.second.m_renderPass)
            {
                vka::System::get().destroyRenderPass(t.second.m_renderPass);
                t.second.m_renderPass = vk::RenderPass();
                if( t.second.outputResources.size() )
                    t.second.m_renderPassNeedsRebuild = true;
            }
        }
    }


    template<typename callable>
    bool _traverseTask(const std::string & resourceName, std::set<std::string> & _s, callable && c) const
    {
        if( _s.find(resourceName) != _s.end() )
            throw std::runtime_error("Cyclic graph detected");

        _s.insert(resourceName);
        c(resourceName);

        for(auto & x : m_tasks.at(resourceName).inHostResource )
        {
            if(!_traverseHostResource(x, _s, c))
            {
                return false;
            }
        }

        for(auto & x : m_tasks.at(resourceName).inputResources )
        {
            if(!_traverseResource(x.name, _s, c))
            {
                return false;
            }
        }
        _s.erase(resourceName);
        return true;
    }

    template<typename callable>
    bool _traverseResource(const std::string & resourceName, std::set<std::string> & _s, callable && c) const
    {
        if( _s.find(resourceName) != _s.end() )
            return false;

        _s.insert(resourceName);

        for(auto & writtenBy : m_resources.at(resourceName).writtenBy )
        {
            if(!_traverseTask( writtenBy, _s, c) )
                return false;
        }
        _s.erase(resourceName);
        return true;
    }
    template<typename callable>
    bool _traverseHostResource(const std::string & resourceName, std::set<std::string> & _s, callable && c) const
    {
        if( _s.find(resourceName) != _s.end() )
            return false;

        _s.insert(resourceName);

        for(auto & writtenBy : m_hostResources.at(resourceName).writtenBy )
        {
            if(!_traverseTask( writtenBy, _s, c) )
                return false;
        }
        _s.erase(resourceName);
        return true;
    }
    bool traverse() const
    {
        if( m_finalOutput == "")
        {
            throw std::runtime_error("No final output set. Each graph needs an end point resource.");
        }

        std::set<std::string> _s;

        if( m_resources.count( m_finalOutput) )
            return _traverseResource(m_finalOutput, _s, [&](auto & ss){ (void)ss; });

        if( m_hostResources.count( m_finalOutput ))
            return _traverseHostResource(m_finalOutput, _s, [&](auto & ss){ (void)ss; });

        throw std::runtime_error( std::string("Final output resource, " + m_finalOutput + ", does not exist"));
        return false;
    }

    void _validateOutputResources(Task & task)
    {
        for(auto & o : task.outHostResource)
        {
            if( !m_hostResources.at(o).created )
            {
                throw std::runtime_error( "Task " + task.name + " is commited to produce resource: (" + o + ") but has not produced it. Task callback must execute emplaceResource<type>(\"" + o + "\")" );
            }
        }
    }
    void _clearOutputResources(Task & task)
    {
        for(auto & o : task.outHostResource)
        {
            auto & R = m_hostResources.at(o);

            R.created = false;
            R.mutex.unlock();
        }
    }

    void _executeTask(Task & task, vk::CommandBuffer cmd)
    {
        if( !task.m_writeCommandCallback) return;

        vk::RenderPassBeginInfo rpbi;

        if( task.m_clearValues.size() != task.outputResources.size())
        {
            task.m_clearValues.resize(task.outputResources.size());
        }

        uint32_t index=0;
        for(auto & o : task.outputResources)
        {
            auto f = m_resources.at(o.name).resourceInfo.format;
            if( isDepthFormat(f))
            {
                if( task.m_clearDepthCallback)
                {
                    task.m_clearValues[index] = task.m_clearDepthCallback(f);
                }
            }
            else
            {
                if( task.m_clearColorCallback )
                {
                    task.m_clearValues[index] = task.m_clearColorCallback(index, f);
                }
            }
            index++;
        }
        RenderTaskVar V;
        V.clearValues        = task.m_clearValues;
        V.imageExtent        = task.m_extent;
        V.graph              = this;
        V.m_localResources_p = task.m_localResources_p;

        V.commandBuffer = cmd;

        V.inputAttachmentImageFormat.clear();
        V.inputAttachmentImageView.clear();
        V.inputAttachmentImageSampler.clear();
        V.inputAttachmentImage.clear();

        V.outputAttachmentImageFormat.clear();
        V.outputAttachmentImageView.clear();
        V.outputAttachmentImage.clear();
        V.renderPassBeginInfo = {};
        V.inputAttachmentDescriptorSet = task.m_inputAttachmentDescriptorSet;

        for(auto & x : task.inputResources)
        {
            auto i = *m_resources[x.name].resourceInfo.physicalResourceIndex;
            V.inputAttachmentImage      .push_back(m_physicalResources[i]._image ); // the image is the actual physical resource
            V.inputAttachmentImageFormat.push_back(m_resources[x.name].resourceInfo.format );
            V.inputAttachmentImageView  .push_back(m_physicalResources[i]._view   );
            V.inputAttachmentImageSampler.push_back(m_physicalResources[i]._sampler   );
            V.inputAttachmentImageExtents.push_back( m_physicalResources[i]._extents);
        }

        if( task.m_renderPass )
        {
            for(auto & x : task.outputResources)
            {
                auto i = *m_resources[x.name].resourceInfo.physicalResourceIndex;
                V.outputAttachmentImage      .push_back(m_physicalResources[i]._image ); // the image is the actual physical resource
                V.outputAttachmentImageFormat.push_back(m_resources[x.name].resourceInfo.format );
                V.outputAttachmentImageView  .push_back(m_physicalResources[i]._view   );
            }

            rpbi.renderPass        = task.m_renderPass;
            rpbi.framebuffer       = task.m_framebuffer;
            rpbi.clearValueCount   = static_cast<uint32_t>(task.m_clearValues.size());
            rpbi.pClearValues      = task.m_clearValues.data();//m_clear_values.data();
            rpbi.renderArea.extent = task.m_extent;

            V.renderPassBeginInfo = rpbi;
            V.inputAttachmentDescriptorSet = task.m_inputAttachmentDescriptorSet;
        }
        else
        {

        }

        task.m_writeCommandCallback( V );

        for(auto & out : task.outHostResource)
        {
            if( !isHostResourceCreated(out) )
                throw std::runtime_error("Output Resource, " + out + ", has not been created by task, " + task.name );
        }

        if( task.m_renderPass)
        {
            //cmd.endRenderPass();
            // wait for all the PBR's colour attachments to be written
            // before we continue with any using of the fragment shaders
            vka::CommandBuffer(cmd).simplePipelineBarrier(vk::PipelineStageFlagBits::eColorAttachmentOutput,
                                      vk::PipelineStageFlagBits::eFragmentShader);
        }
    }

    bool m_needsRecompile = true; // the graph needs to be recompiled because a new resource/task was added.
};

inline FrameGraph::Task &FrameGraph::Task::addColorOutput(const std::string &resourceName, vk::Format format, std::optional<vk::Extent2D> extent)
{
    assert(&format);
    outputResources.emplace_back();
    outputResources.back().name = resourceName;

    auto & res = m_graph->m_resources[resourceName];

    res.writtenBy.push_back( name );
    res.resourceInfo.format  = format;
    res.resourceInfo.extents = extent;

    m_graph->m_needsRecompile = true;
    m_renderPassNeedsRebuild  = true;
    return *this;
}

inline FrameGraph::Task &FrameGraph::Task::addAttachmentInput(const std::string &resourceName)
{
    inputResources.emplace_back();
    inputResources.back().name = resourceName;

    auto & res = m_graph->m_resources[resourceName];

    res.readBy.push_back(name);

    m_graph->m_needsRecompile = true;

    return *this;
}

inline FrameGraph::Task& FrameGraph::Task::addHostResourceInput( const std::string & resourceName)
{
    inHostResource.emplace_back(resourceName);

    auto & res = m_graph->m_hostResources[resourceName];

    res.readBy.push_back(name);

    m_graph->m_needsRecompile = true;

    return *this;
}

inline FrameGraph::Task& FrameGraph::Task::addHostResourceOutput(const std::string & resourceName)
{
    outHostResource.emplace_back(resourceName);

    auto & res = m_graph->m_hostResources[resourceName];

    res.writtenBy.push_back( name );

    return *this;
}

inline std::any& TaskVarBase::_getHostResource(const std::string & name, std::optional<bool> createdValue)
{
    auto & R = graph->getHostResource(name);

    if( createdValue)
    {
        R.created = *createdValue;
    }
    return R.resource;
}

}

#endif
