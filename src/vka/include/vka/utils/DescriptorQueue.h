#pragma once
#ifndef VKA_UTILS_DESCRIPTOR_QUEUE_H
#define VKA_UTILS_DESCRIPTOR_QUEUE_H

#include <queue>
#include <vka/core/System.h>
#include <vka/core/DescriptorWriter.h>

namespace vka
{

struct DescriptorSetQueue
{
    // returns a reference fo the descriptor set that
    // is in the front of the queue
    vk::DescriptorSet& front()
    {
        return m_sets.front();
    }

    // shifts the queue, sending the set which is in the front
    // to the back
    void shift()
    {
        m_sets.push( m_sets.front() );
        m_sets.pop();
    }
    void insert(vk::DescriptorSet s)
    {
        m_sets.push(s);
    }

    void free()
    {
        while(m_sets.size())
        {
            vka::System::get().destroyDescriptorSet( m_sets.front() );
            m_sets.pop();
        }
    }
    size_t size() const
    {
        return m_sets.size();
    }

    std::queue<vk::DescriptorSet> m_sets;
};

}

#endif
