#ifndef VKA_BRDFGENERATOR_H
#define VKA_BRDFGENERATOR_H

#include <vka/math/linalg.h>
#include <vka/core/HostImage.h>
#include <array>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>

namespace vka
{

struct BRDFGenerator
{
static float RadicalInverse_VdC(unsigned int bits)
{
        bits = (bits << 16u) | (bits >> 16u);
        bits = ((bits & 0x55555555u) << 1u) | ((bits & 0xAAAAAAAAu) >> 1u);
        bits = ((bits & 0x33333333u) << 2u) | ((bits & 0xCCCCCCCCu) >> 2u);
        bits = ((bits & 0x0F0F0F0Fu) << 4u) | ((bits & 0xF0F0F0F0u) >> 4u);
        bits = ((bits & 0x00FF00FFu) << 8u) | ((bits & 0xFF00FF00u) >> 8u);
        return float(bits) * 2.3283064365386963e-10f;
}

static glm::vec2 Hammersley(unsigned int i, unsigned int N)
{
        return glm::vec2(float(i) / float(N), RadicalInverse_VdC(i));
}

static glm::vec3 ImportanceSampleGGX(glm::vec2 Xi, float roughness, glm::vec3 N)
{
        float a = roughness*roughness;

        float phi = 2.0f * glm::pi<float>() * Xi.x;
        float cosTheta = std::sqrt((1.0f - Xi.y) / (1.0f + (a*a - 1.0f) * Xi.y));
        float sinTheta = std::sqrt(1.0f - cosTheta*cosTheta);

        // from spherical coordinates to cartesian coordinates
        glm::vec3 H;
        H.x = std::cos(phi) * sinTheta;
        H.y = std::sin(phi) * sinTheta;
        H.z = cosTheta;

        // from tangent-space vector to world-space sample vector
        glm::vec3 up = abs(N.z) < 0.999f ? glm::vec3(0.0, 0.0, 1.0) : glm::vec3(1.0, 0.0, 0.0);
        glm::vec3 tangent = normalize(cross(up, N));
        glm::vec3 bitangent = cross(N, tangent);

        glm::vec3 sampleVec = tangent * H.x + bitangent * H.y + N * H.z;
        return normalize(sampleVec);
}

static float GeometrySchlickGGX(float NdotV, float roughness)
{
        float a = roughness;
        float k = (a * a) / 2.0f;

        float nom = NdotV;
        float denom = NdotV * (1.0f - k) + k;

        return nom / denom;
}

static float GeometrySmith(float roughness, float NoV, float NoL)
{
        float ggx2 = GeometrySchlickGGX(NoV, roughness);
        float ggx1 = GeometrySchlickGGX(NoL, roughness);

        return ggx1 * ggx2;
}

static glm::vec2 IntegrateBRDF(float NdotV, float roughness, unsigned int samples)
{
        glm::vec3 V;
        V.x = std::sqrt(1.0f - NdotV * NdotV);
        V.y = 0.0f;
        V.z = NdotV;

        float A = 0.0f;
        float B = 0.0f;

        glm::vec3 N = glm::vec3(0.0, 0.0, 1.0);

        for (unsigned int i = 0u; i < samples; ++i)
        {
                glm::vec2 Xi = Hammersley(i, samples);
                glm::vec3 H = ImportanceSampleGGX(Xi, roughness, N);
                glm::vec3 L = normalize(2.0f * dot(V, H) * H - V);

                float NoL = glm::max(L.z, 0.0f);
                float NoH = glm::max(H.z, 0.0f);
                float VoH = glm::max(dot(V, H), 0.0f);
                float NoV = glm::max(dot(N, V), 0.0f);

                if (NoL > 0.0f)
                {
                        float G = GeometrySmith(roughness, NoV, NoL);

                        float G_Vis = (G * VoH) / (NoH * NoV);
                        float Fc = std::pow(1.0f - VoH, 5.0f);

                        A += (1.0f - Fc) * G_Vis;
                        B += Fc * G_Vis;
                }
        }

        return glm::vec2(A,B) / static_cast<float>(samples);
}

/**
 * @brief generate
 * @param size
 * @return
 *
 * Generate a BRDF image where the rg and ba channels are the
 * corresponding values:
 *
 * To get the floating point values for each component,
 * use the following formula.
 *
 * A = ( g * 255 + r ) / 255.0f
 * B = ( a * 255 + b ) / 255.0f
 */
static vka::HostImage generate(uint32_t size)
{
    unsigned int samples = 1024;


        vka::HostImage tex;
        tex.resize(size,size);

        auto sizef = static_cast<float>(size);

        for (uint32_t y = 0; y < size; y++)
        {
                for (uint32_t x = 0; x < size; x++)
                {
                        float NoV       = (  static_cast<float>(y) + 0.5f) * (1.0f / sizef);
                        float roughness = (  static_cast<float>(x) + 0.5f) * (1.0f / sizef);

                        auto storeValue = IntegrateBRDF(NoV, roughness,samples);

                        tex(x,y,0) = static_cast<uint8_t>(storeValue.x * 255.0f);
                        tex(x,y,1) = static_cast<uint8_t>(storeValue.y*255.0f);
                        tex(x,y,2) = 255;//static_cast<uint8_t>(storeValue.y*255.0f);
                        tex(x,y,3) = 255;//static_cast<uint8_t>(storeValue.y*255.0f);
#if 0

#if 0
                        uint16_t RG = static_cast<uint16_t>(storeValue.x * 65535.f);
                        uint16_t BA = static_cast<uint16_t>(storeValue.y * 65535.f);

                        uint8_t r = static_cast<uint8_t>( (RG >> 0) & 0xFF );
                        uint8_t g = static_cast<uint8_t>( (RG >> 8) & 0xFF );
                        uint8_t b = static_cast<uint8_t>( (BA >> 0) & 0xFF );
                        uint8_t a = static_cast<uint8_t>( (BA >> 8) & 0xFF );
                        tex(x,y) = vka::pixel4(r,g,b,a);
#else
                        float RG = storeValue.x * 65535.f;
                        float BA = storeValue.y * 65535.f;

                        uint8_t r = static_cast<uint8_t>( std::fmod(RG , 255.f) );
                        uint8_t g = static_cast<uint8_t>( std::floor(RG / 255.f) );
                        uint8_t b = static_cast<uint8_t>( std::fmod(BA , 255.f) );
                        uint8_t a = static_cast<uint8_t>( std::floor(BA / 255.f) );

                        //tex(size-1-x,y) = vka::pixel4(r,g,b,a);
                        tex(x,y) = vka::pixel4(r,g,b,a);
#endif
#endif
                        //uint8_t r = static_cast<uint8_t>( storeValue.x * 255 );
                        //uint8_t g = static_cast<uint8_t>( storeValue.y * 255 );

                        //tex[y][size-1-x] = storeValue;
                        //tex(size-1-x,y) = storeValue.x;
//                        if (bits == 16)
//                                tex.store<glm::uint32>({ y, size - 1 - x }, 0, gli::packHalf2x16(IntegrateBRDF(NoV, roughness, samples)));
//                        if (bits == 32)
//                                tex.store<glm::vec2>({ y, size - 1 - x }, 0, IntegrateBRDF(NoV, roughness,samples));
                }
        }
        return tex;
}

};

}
#endif

