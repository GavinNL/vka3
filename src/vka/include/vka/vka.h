#ifndef VKA_VKA_H
#define VKA_VKA_H

#include <vulkan/vulkan.hpp>

#include "core/InstanceCreateInfo2.h"
#include <map>

namespace vka
{

/**
 * @brief getInstanceDispatcher
 * @param instance
 * @return
 *
 * A static function that creates the DispatcherLoaderDynamic.
 *
 * Call this function when you need to get the loader, which is created
 * per instance in a singleton map.
 */
static vk::DispatchLoaderDynamic& getInstanceDispatcher(vk::Instance instance)
{
    using map_type   = std::map<vk::Instance, vk::DispatchLoaderDynamic>;
    static auto _map = std::make_shared<map_type>();

    auto f = _map->find(instance);
    if( f == _map->end() )
    {
        auto & d = (*_map)[instance];
        d.init(vkGetInstanceProcAddr);
        d.init(instance);
        return d;
    }
    else
    {
        return f->second;
    }
}

/**
 * @brief createInstance
 * @param createInfo
 * @return
 *
 * Create a vulkan instance using the vka::InstanceCreateInfo2 struct.
 */
inline vk::Instance createInstance(vka::InstanceCreateInfo2 createInfo)
{
    auto ci = createInfo.create();
    return vk::createInstance(ci);
}

/**
 * @brief createDebugCallback
 * @param instance
 * @param debugCallback
 * @return
 *
 * Create a debug callback for a vulkan instance.
 */
inline vk::DebugReportCallbackEXT createDebugCallback(vk::Instance instance, PFN_vkDebugReportCallbackEXT debugCallback)
{
    vk::DebugReportCallbackEXT m_callback;

    vk::DebugReportCallbackCreateInfoEXT createInfo;

    createInfo.setFlags( vk::DebugReportFlagBitsEXT::eError | vk::DebugReportFlagBitsEXT::eWarning)
              .setPfnCallback( debugCallback );


    auto & dispatcher = getInstanceDispatcher(instance);
    if(
    dispatcher.vkCreateDebugReportCallbackEXT(instance,
                                                 reinterpret_cast<VkDebugReportCallbackCreateInfoEXT const*>(&createInfo),
                                                 nullptr,
                                                 reinterpret_cast<VkDebugReportCallbackEXT*>(&m_callback) )
            != VK_SUCCESS)
    {
        throw std::runtime_error("failed to set up debug callback!");
    }

    if (!m_callback)
    {
        throw std::runtime_error("failed to set up debug callback!");
    }
    return m_callback;
}

/**
 * @brief destroyDebugCallback
 * @param instance
 * @param debugCallback
 *
 * Destroy the debug callback.
 */
inline void destroyDebugCallback(vk::Instance instance, vk::DebugReportCallbackEXT debugCallback)
{
    auto & dispatcher = getInstanceDispatcher(instance);
    dispatcher.vkDestroyDebugReportCallbackEXT(instance, debugCallback, nullptr);
}


inline bool areDeviceExtensionsSupported(const vk::PhysicalDevice &device, std::vector<std::string> const & requiredExtensions)
{
    std::vector<vk::ExtensionProperties> availableExtensions = device.enumerateDeviceExtensionProperties();


    for(auto const & req : requiredExtensions)
    {
        bool found = false;
        for(auto & avail : availableExtensions)
        {
            if( avail.extensionName == req)
            {
                found = true;
                break;
            }
        }
        if( found )
            break;

        return false;
    }
    return true;
}

/**
 * @brief canQueuePresentToSurface
 * @param queueIndex
 * @param device
 * @param surface
 * @return
 *
 * Returns true if queue, queueIndex, on the physical device can present to
 * the surface.
 */
inline bool canQueuePresentToSurface( uint32_t queueIndex, vk::PhysicalDevice device, vk::SurfaceKHR surface)
{
    auto queueFamilies = device.getQueueFamilyProperties();
    auto & qFamily = queueFamilies.at( queueIndex );

    VkBool32 presentSupport = false;

    device.getSurfaceSupportKHR(queueIndex, surface, &presentSupport);

    if( qFamily.queueCount > 0 && presentSupport)
    {
        // this queue can present to the surface
        return true;
    }
    return false;
}

inline bool canQueueDoGraphics( uint32_t queueIndex, vk::PhysicalDevice device)
{
    auto queueFamilies = device.getQueueFamilyProperties();
    auto & qFamily = queueFamilies.at(queueIndex);

    if (qFamily.queueCount > 0 && (qFamily.queueFlags & vk::QueueFlagBits::eGraphics) )
    {
        // queue index, i, can do graphics related tasks
        return true;
    }

    return false;
}

/**
 * @brief createPhysicalDevice
 * @param createInfo
 * @return
 *
 * Returns an appropriate physical device. If createInfo.surface is provided
 * it will return a device that can present to that surface.
 */
inline vk::PhysicalDevice createPhysicalDevice( PhysicalDeviceCreateInfo2 createInfo)
{
    auto instance = createInfo.instance;

    auto devices = instance.enumeratePhysicalDevices();

    if( devices.size() == 0)
    {
        throw std::runtime_error("failed to find GPUs with Vulkan support!");
    }

    std::vector<const char *> DeviceExtensions;

    for(auto & d : createInfo.deviceExtensions)
        DeviceExtensions.push_back( d.data() );


    for ( vk::PhysicalDevice & device : devices)
    {
        // a device is suitable if it supports all the extensions
        if( areDeviceExtensionsSupported( device, createInfo.deviceExtensions ) )
        {
            auto queueFamilies = device.getQueueFamilyProperties();

            for(uint32_t i=0;i<queueFamilies.size();i++)
            {
                if( canQueueDoGraphics(i, device) && canQueuePresentToSurface(i, device, createInfo.surface))
                {
                    return device;
                }
            }
        }
    }
    throw std::runtime_error("Could not find an appropriate PhysicalDevice");
}

inline bool vka::DeviceCreateInfo2::findAppropriateQueues(vk::SurfaceKHR surface)
{
    uint32_t gQueue = std::numeric_limits<uint32_t>::max();
    uint32_t pQueue = std::numeric_limits<uint32_t>::max();
    if( areDeviceExtensionsSupported( physicalDevice, requiredDeviceExtensions ) )
    {
        auto queueFamilies = physicalDevice.getQueueFamilyProperties();

        for(uint32_t i=0;i<queueFamilies.size();i++)
        {
            if( canQueueDoGraphics(i, physicalDevice) )
            {
                gQueue = i;
            }

            if( canQueuePresentToSurface(i, physicalDevice, surface))
            {
                pQueue = i;
            }

            if( gQueue < std::numeric_limits<uint32_t>::max() && pQueue < std::numeric_limits<uint32_t>::max())
            {
                graphicsQueueFamily = gQueue;
                presentQueueFamily  = pQueue;
                return true;
            }
        }
    }
    return false;
}


inline vk::Device createDevice( vka::DeviceCreateInfo2 createInfo)
{
    auto ci = createInfo.create();

    auto device = createInfo.physicalDevice.createDevice(ci);

    if( !device )
    {
        throw std::runtime_error("failed to create logical device!");
    }
    return device;
}

}

#endif
