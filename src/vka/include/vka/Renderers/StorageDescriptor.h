#ifndef VKA_STORAGE_DESCRIPTOR_H
#define VKA_STORAGE_DESCRIPTOR_H

#include <iostream>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>

#include <vka/math.h>


#ifndef VKA_LOG
#define VKA_LOG(...)
#endif

namespace vka
{
/**
 * @brief The StorageDescriptor_t struct
 *
 * The StorageDescriptor is mean to be used
 * to store large amounts of similar data, for example
 * matrices. For example, In your shader you can
 * have something like this:
 *
 *
 * layout(set = 0, binding = 1) buffer STORAGE_MATRIX_t
 * {
 *     mat4 transform[];
 * } STORAGE_MATRIX;
 *
 * You would then create this on the host by
 *
 * StorageDescriptor_t<glm::mat4> storageDescriptor;
 *
 * storageDescriptor( mySubbuffer, 0 ,mySubbuffer.getSize() );
 *
 * This storageDescriptor can then be passed to the various
 * renderers
 */
template<typename T>
struct StorageDescriptor_t
{
    using value_type = T;

    struct Info_t
    {
        T *        m_begin  = nullptr;
        T *        m_dst    = nullptr;
        size_t     m_maxSize;


        vka::SubBuffer m_buffer;
        uint32_t       m_offset;
        uint32_t       m_byteSize;
    };

    StorageDescriptor_t()
    {
    }

    void init(vka::SubBuffer buffer)
    {
        init(buffer, 0, static_cast<uint32_t>(buffer.getSize()) );
    }
    void init(vka::SubBuffer buffer, uint32_t offset, uint32_t byteSize)
    {
        m_info = std::make_shared<Info_t>();

        //auto parentBuffer       = buffer.getParentBuffer();
        //auto bufferOffset       = buffer.getOffset();

        //auto parentBufferMemory      = vka::System::get().info(parentBuffer).memory;
        //vka::DeviceMemoryInfo const & info = vka::System::get().info(parentBufferMemory);

        auto typeAlignment = sizeof(value_type);

        auto byteStart = offset;
        auto byteEnd   = offset+byteSize;

        auto roundUp = [](auto N, auto S)
        {
            return ((((N) + (S) - 1) / (S)) * (S));
        };
        if( byteStart%typeAlignment != 0)
        {
            #define ROUND_UP(N, S) ((((N) + (S) - 1) / (S)) * (S))
            byteStart = roundUp(byteStart, static_cast<uint32_t>(typeAlignment) );
            #undef ROUND_UP

            byteEnd -= (byteStart-offset);
        }

        //auto actualOffset = offset + (typeAlignment-bufferOffset%typeAlignment);
        //auto lastByte     = offset + byteSize - (bufferOffset%typeAlignment);
        auto actualSize   = (byteEnd-byteStart) / sizeof(value_type);


        auto * c = static_cast<uint8_t*>( buffer.getMappedMemory() ) + byteStart;

        m_info->m_dst                 = reinterpret_cast<value_type*>( c );
        m_info->m_begin               = m_info->m_dst;
        m_info->m_maxSize             = actualSize;

        m_info->m_buffer              = buffer;
        m_info->m_offset              = byteStart;
        m_info->m_byteSize            = byteEnd-byteStart;

    }

    uint32_t push(vk::ArrayProxy<const value_type> B)
    {
        auto cu =  (m_info->m_dst-m_info->m_begin );
        std::memcpy( m_info->m_dst, B.data(), sizeof(value_type)*B.size());
        m_info->m_dst += B.size();
        return static_cast<uint32_t>(cu);
    }

    uint32_t push(T const v)
    {
        auto cu = (m_info->m_dst - m_info->m_begin );

        std::memcpy( m_info->m_dst, &v, sizeof(v));
        ++m_info->m_dst;

        return static_cast<uint32_t>(cu);
    }

    uint32_t push(T const *v, size_t count)
    {
        auto d = getUsage();
        std::memcpy(m_info->m_dst, v, sizeof(T)*count);
        m_info->m_dst += count;
        return static_cast<uint32_t>(d);
    }

    uint32_t getByteSize() const
    {
        return m_info->m_byteSize;
    }

    uint32_t getOffset() const
    {
        return m_info->m_offset;
    }

    vka::SubBuffer getSubBuffer() const
    {
        return m_info->m_buffer;
    }

    void reset()
    {
        m_info->m_dst = m_info->m_begin;
    }

    size_t getUsage() const
    {
         return static_cast<size_t>( std::distance( m_info->m_begin, m_info->m_dst) );
    }

    size_t getCapacity() const
    {
        return m_info->m_maxSize;
    }

    size_t getAvailable() const
    {
        return  m_info->m_maxSize - static_cast<size_t>( std::distance( m_info->m_begin, m_info->m_dst) );
    }
private:
    std::shared_ptr<Info_t> m_info;
};

}
#endif
