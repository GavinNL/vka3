#ifndef VKA_MULTI_STORAGE_DESCRIPTOR_H
#define VKA_MULTI_STORAGE_DESCRIPTOR_H

#include <iostream>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>

#include <vka/math.h>
#include <type_traits>

#ifndef VKA_LOG
#define VKA_LOG(...)
#endif

namespace vka
{
/**
 * @brief The StorageDescriptor_t struct
 *
 * The StorageDescriptor is mean to be used
 * to store large amounts of similar data, for example
 * matrices. For example, In your shader you can
 * have something like this:
 *
 *
 * layout(set = 0, binding = 1) buffer STORAGE_MATRIX_t
 * {
 *     mat4 transform[];
 * } STORAGE_MATRIX;
 *
 * You would then create this on the host by
 *
 * StorageDescriptor_t<glm::mat4> storageDescriptor;
 *
 * storageDescriptor( mySubbuffer, 0 ,mySubbuffer.getSize() );
 *
 * This storageDescriptor can then be passed to the various
 * renderers
 */
struct MultiStorageDescriptor_t
{
    struct Info_t
    {
        uint8_t *  m_begin  = nullptr;
        size_t     m_end    = 0;
        size_t     m_maxSize= 0;

        vka::SubBuffer m_buffer;
        //uint32_t       m_offset;
        //uint32_t       m_byteSize;
    };

    MultiStorageDescriptor_t()
    {
    }

    void init(vka::SubBuffer buffer)
    {
        init(buffer, 0, static_cast<uint32_t>(buffer.getSize()) );
    }



    // round up the current offset to the next byte alignment
    size_t roundUp(size_t alignment)
    {
        auto _roundUp = [](auto N, auto S)
        {
            return ((((N) + (S) - 1) / (S)) * (S));
        };
        m_info->m_end = _roundUp( m_info->m_end, alignment);

        if(m_info->m_end > m_info->m_maxSize)
        {
            m_info->m_end = 0;
        }

        return m_info->m_end;
    }

    // returns the number of bytes available until the buffer
    // rolls over.
    size_t available() const
    {
        return m_info->m_maxSize - m_info->m_end;
    }

    /**
     * @brief push
     * @param v
     * @return
     *
     * Push data into the buffer. The value will always be aligned to
     * sizeof(T).
     *
     * Ths function runs the array offset if the buffer was
     * representing a an array of T[x].
     *
     *  For example, given an empty storage, M:
     *
     * M.push(glm::mat4() ) == 0 (since it is the first element)
     * M.push(glm::mat4() ) == 1 (since it is now the second element in mat4[]
     * M.push( float() )    == 32 (since the float was pushed after the 2 mat4(), it exists at
     *                             byte offset 2*sizeof(mat4)==128.
     *                             If the underlying buffer was an array of float[], then byte 128
     *                             would be at array index 32.
     *
     *
     */
    template<typename T>
    int32_t push(T const & v)
    {
        static_assert(  !std::is_pointer<T>::value, "Cannot use a pointer");
        return push(&v, 1);
    }
    template<typename T>
    int32_t push(T const * v, size_t count)
    {
        auto alignment = sizeof(T);
        auto totalBytes= sizeof(T)*count;

        if( available() < totalBytes )
        {
            m_info->m_end = 0;
        }
        // move the current end point to a byte offset that aligns with alignment
        auto i = roundUp(alignment);

        std::memcpy( &_back(), v, totalBytes );
        m_info->m_end += totalBytes;

        return static_cast<int32_t>( i / sizeof(T) );
    }

    void reset()
    {
        m_info->m_end = 0;
    }
    void init(vka::SubBuffer buffer, uint32_t offset, uint32_t byteSize)
    {
        m_info = std::make_shared<Info_t>();

        auto byteStart = offset;
        auto byteEnd   = offset+byteSize;

        //auto actualOffset = offset + (typeAlignment-bufferOffset%typeAlignment);
        //auto lastByte     = offset + byteSize - (bufferOffset%typeAlignment);
        auto actualSize   = (byteEnd-byteStart);


        auto * c = static_cast<uint8_t*>( buffer.getMappedMemory() ) + byteStart;

        m_info->m_begin               = c;
        m_info->m_maxSize             = actualSize;

        m_info->m_buffer              = buffer;
        //m_info->m_offset              = byteStart;
        //m_info->m_byteSize            = byteEnd-byteStart;

    }

    size_t getCurrentOffset() const
    {
        return m_info->m_end % m_info->m_maxSize;;
    }

    vka::SubBuffer getSubBuffer() const
    {
        return m_info->m_buffer;
    }

    size_t getCapacity() const
    {
        return m_info->m_maxSize;
    }

private:
    std::shared_ptr<Info_t> m_info;
    uint8_t& _back()
    {
        return m_info->m_begin[ m_info->m_end ];
    }

};

}
#endif
