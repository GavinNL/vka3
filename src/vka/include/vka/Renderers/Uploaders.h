#ifndef VKA_HELPERS_UPLOADERS_H
#define VKA_HELPERS_UPLOADERS_H

#include <iostream>

#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DeviceMeshPrimitive.h>

#include <vka/core/TextureMemoryPool.h>
#include <vka/utils/HostImage.h>

#include <queue>

namespace vka
{
/**
 * @brief The PrimitiveUploader struct
 *
 * This class helps upload primitives from the host to the device.
 *
 * When you want to upload a primitive to the GPU, first called
 *
 * upload( hostTriPrimitive, deviceOnlyBufferPool).
 *
 * The deviceOnlyBufferPool, but be a buffer pool that can allocate vertex and index
 * buffers
 *
 * then during your main loop's command recording, call:
 *
 * .processUploads(cmdbuffer, stagingBufferPool)
 *
 */
struct PrimitiveUploader
{
protected:
    std::queue< std::pair<vka::DeviceMeshPrimitive, vka::HostTriPrimitive> > m_uploadQueue;

public:

    vka::DeviceMeshPrimitive upload( vka::HostTriPrimitive src, vka::BufferPool & deviceOnlyBufferPool)
    {
        auto byteSize = src.requiredByteSize(16);

        // allocate space from the bufferpool
        auto devPrimitive  = deviceOnlyBufferPool.allocateDeviceMeshPrimitive( byteSize );

        devPrimitive.copyFrom( src, false);

        // push the two primitives onto the queue so the data can be scheduled for copy.
        // Since the primitives have a SubBuffer in them, as long as we
        // dont free the memory, the memory will always exist.
        m_uploadQueue.push( std::pair<vka::DeviceMeshPrimitive,vka::HostTriPrimitive>( devPrimitive, std::move(src) ) );

        return devPrimitive;
    }


    /**
     * @brief processTransfers
     * @param cmd
     * @param stagingBufferPool
     * @param allocatedSubBuffers
     *
     * This method processes whatever transfers are in the queue.
     *
     * You must provide it with a stagingBufferPool which can be used
     * to allocate staging buffers.
     *
     * Once the staging buffers have been allocated, it will be pushed
     * into the allocatedSubBuffers vector.
     *
     * You must free these buffers yourself.
     */
    void processTransfers(vka::CommandBuffer cmd,
                          vka::BufferPool & stagingBufferPool,
                          std::vector<vka::SubBuffer> & allocatedSubBuffers)
    {

        while( m_uploadQueue.size() )
        {
            auto & front = m_uploadQueue.front();

            auto & dst = front.first;
            auto & src = front.second;

            auto bufferSize = dst.getSubBuffer().getSize();

            if( !stagingBufferPool.canAllocate(bufferSize) ) break;

            // allocate a a MeshPrimitive from the staging pool.
            vka::DeviceMeshPrimitive stagingBox = stagingBufferPool.allocateDeviceMeshPrimitive( bufferSize );

            // copy the data from the HostTriPrimitive to the DeviceMeshPrimitive;
            stagingBox.copyFrom(src);

            //-----------
            // Technically we dont need to do this because
            // the commandbuffers always execute after host transfers
            // have finished.
            //cmd.waitForHostTransfers();
            //-----------

            // execute the copy of teh buffer data
            cmd.copySubBuffer( stagingBox.getSubBuffer(), dst.getSubBuffer());

            allocatedSubBuffers.push_back( stagingBox.getSubBuffer() );

            m_uploadQueue.pop();

        }
    }
};


/**
 * @brief The ImageUploader struct
 *
 * Image uploader assists in uploading images to the GPU
 */
struct ImageUploader
{
protected:
#define L1

#if defined (L1)
    struct D
    {
        vka::HostImage src; // the source image
        vk::ImageView  dst; // which image we are going to upload to.
        uint32_t       layer; // the layer in the dst we going to upload the image to
        uint32_t       mip;  // the mip level
    };
    std::queue< D > m_uploadQueue;
#else
    std::queue< std::pair<vk::ImageView, vka::HostImage> > m_uploadQueue;
#endif

public:
    void processTransfers(vka::CommandBuffer cmd,
                          vka::BufferPool & stagingBufferPool,
                          std::vector<vka::SubBuffer> & allocatedSubBuffers)
    {

        while( m_uploadQueue.size() )
        {
            auto & front = m_uploadQueue.front();

            #if defined (L1)
            auto & dst = front.dst;
            auto & src = front.src;
            auto layer = front.layer;
            auto mip   = front.mip;
            #else
            auto & dst = front.first;
            auto & src = front.second;
            auto layer = 0;//front.layer;
            auto mip   = 0;//front.mip;
            #endif

            try
            {

                auto imgSubBuffer = stagingBufferPool.allocate( src.size() );

                imgSubBuffer.copyData( src.data(), src.size() );

                auto img       = vka::System::get().info(dst).createInfo.image;
                auto imgExtent = vka::System::get().info(img).createInfo.extent;

                cmd.waitForHostTransfers();

                vka::BufferSubImageCopyToImage D1;
                D1.srcBufferRegion.offset = vk::Offset2D(0,0);
                D1.srcBufferRegion.extent = vk::Extent2D(imgExtent.width,imgExtent.height);
                D1.srcBufferImageExtent   = vk::Extent2D(imgExtent.width,imgExtent.height);

                D1.dstImageOffset = vk::Offset2D(0,0);
                D1.mip = 0;
                D1.layer = layer;

                cmd.copyBufferToImageAndConvert(imgSubBuffer, dst, D1);

//                cmd.copyBufferToImageAndConvert(imgSubBuffer, vk::Offset2D(0,0), vk::Extent2D(imgExtent.width, imgExtent.height), dst, vk::Offset2D(0,0), layer,0);

                cmd.generateMipMaps(dst, mip);

                allocatedSubBuffers.push_back( imgSubBuffer );

                m_uploadQueue.pop();
            }
            catch (...)
            {
                break;

            }
        }
    }

    vk::ImageView uploadImage( vka::HostImage _img, vka::TexturePool & deviceTexturePool)
    {


        auto w = _img.width();
        auto h = _img.height();
        auto m =  uint32_t( std::log2( std::min(w,h) ) );


        while(w>1024)
        {
            _img = _img.nextMipMap();
            w = _img.width();
            h = _img.height();
            m =  uint32_t( std::log2( std::min(w,h) ) );
        }
        auto outImg = deviceTexturePool.createNewImage2D( vk::Format::eR8G8B8A8Unorm, vk::Extent2D(w,h), 1, m);


        #if defined (L1)
        auto & d = m_uploadQueue.emplace();
        d.dst   = outImg;
        d.src   = std::move(_img);
        d.mip   = 0;
        d.layer = 0;
        #else
        // push the two primitives onto the queue so the data can be scheduled for copy.
        // Since the primitives have a SubBuffer in them, as long as we
        // dont free the memory, the memory will always exist.
        m_uploadQueue.push( std::pair<vk::ImageView, vka::HostImage>( outImg, std::move(_img) ) );
        #endif

        return outImg;
    }

#undef L1
};


}

#endif
