#ifndef VKA_LOGGING_H
#define VKA_LOGGING_H

#include <fmt/format.h>
#include <chrono>
#include <iostream>

namespace vka
{

struct Logger
{
    std::function<void(std::string const&)> trace;
    std::function<void(std::string const&)> info;
    std::function<void(std::string const&)> debug;
    std::function<void(std::string const&)> warn;
    std::function<void(std::string const&)> error;
    std::function<void(std::string const&)> crit;

    Logger()
    {
        #define MS_ std::chrono::duration<double>( std::chrono::system_clock::now() - startTime ).count()

        trace = [&](std::string const& s) { std::cout << MS_ << " [trace]: " << s << "\n";   };
        info  = [&](std::string const& s) { std::cout << MS_ << " [info]: " << s << "\n";   };
        debug = [&](std::string const& s) { std::cout << MS_ << " [debug]: " << s << "\n";   };
        warn  = [&](std::string const& s) { std::cout << MS_ << " [warn]: " << s << "\n";   };
        error = [&](std::string const& s) { std::cout << MS_ << " [error]: " << s << "\n";   };
        crit  = [&](std::string const& s) { std::cout << MS_ << " [crit]: " << s << "\n";   };
    }

    std::chrono::system_clock::time_point startTime = std::chrono::system_clock::now();
};

extern Logger vkalogger;

#if defined VKA_NO_LOGGING
    #define VKA_TRACE(...)
    #define VKA_INFO(...)
    #define VKA_DEBUG(...)
    #define VKA_WARN(...)
    #define VKA_ERROR(...)
    #define VKA_CRIT(...)
#else
    #define VKA_TRACE(...) if( vka::vkalogger.trace ) vka::vkalogger.trace( fmt::format( __VA_ARGS__)  )
    #define VKA_INFO(...)  if( vka::vkalogger.info  ) vka::vkalogger.info(  fmt::format( __VA_ARGS__)  )
    #define VKA_DEBUG(...) if( vka::vkalogger.debug ) vka::vkalogger.debug( fmt::format( __VA_ARGS__)  )
    #define VKA_WARN(...)  if( vka::vkalogger.warn  ) vka::vkalogger.warn(  fmt::format( __VA_ARGS__)  )
    #define VKA_ERROR(...) if( vka::vkalogger.error ) vka::vkalogger.error( fmt::format( __VA_ARGS__)  )
    #define VKA_CRIT(...)  if( vka::vkalogger.crit  ) vka::vkalogger.crit(  fmt::format( __VA_ARGS__)  )
#endif

}

#endif 

