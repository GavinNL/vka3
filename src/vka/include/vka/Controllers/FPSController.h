#ifndef VKA_FPS_CONTROLLER_H
#define VKA_FPS_CONTROLLER_H

#include <vka/math/transform.h>

namespace vka
{

/**
 * @brief The FPS_Controller class
 *
 * A controller for handling FPS style control.
 *
 * This class is used to update a vk::Tranform Cass
 *
 * Use lookH( ) and lookV( ) to change the horizontal
 * and vertical look directions, then use
 *
 * step.(&transform, deltaTime) during the main
 * game loop
 */
class FPS_Controller
{
    public:

        void lookH(float dx)
        {
            m_desiredTransform.rotateGlobal( {0,1,0}, dx);
        }

        void lookV(float dy)
        {
            m_desiredTransform.rotateLocal( {1,0,0}, dy);
        }

        void moveFoward(float dz)
        {
            m_localSpeed.z = dz;
        }

        void moveSide(float dx)
        {
            m_localSpeed.x = dx;
        }


        // steps the simulator by a time dt
        void step(vka::transform * tr, float dt)
        {
            if( m_tranform != tr)
            {
                m_desiredTransform = *tr;
                m_tranform = tr;
            }
            if(m_tranform)
            {
                // the actual speed we want to go at
                auto globalSpeed = (m_tranform->m_orientation * m_localSpeed);

                m_speed = glm::mix( m_speed, globalSpeed, 0.05f);

                m_tranform->m_position   += m_speed * dt;
                m_tranform->m_orientation = glm::slerp( m_tranform->m_orientation, m_desiredTransform.rotation, 0.50f);
                //m_speed = glm::mix( m_speed, vec3(0,0,0), dt);
            }


        }

    public:
        glm::vec3 m_localSpeed;
        glm::vec3 m_speed;
        vka::transform * m_tranform=nullptr;
        vka::transform m_desiredTransform;
};


}

#endif
