#ifndef VKA_TRANSLATION_CONTROLLER_H
#define VKA_TRANSLATION_CONTROLLER_H

#include <vka/math/transform.h>
#include <vka/math/geometry/frustum.h>
#include <vka/math/geometry.h>
#include <vka/utils/Camera.h>
#include <iostream>

namespace vka
{


/**
 * @brief The GizmoController class
 *
 * Lets you use the mouse to translate the transform
 * in a paricular plane.
 *
 * Use the pbrSceneRenderer::createGizmoScene()
 * to generate the Gizmo which can be used to visually
 * display a gizmo.
 */
class GizmoController
{
public:
    /**
     * @brief step
     * @param tr
     * @param mouseProjectionRay
     *
     * Call this function with an appropriate mouseRay to
     * activate the Controller. calling this function
     * assumes that the user has pressed a button.
     */
    void step(vka::transform * tr, line3 const mouseProjectionRay)
    {

        if(m_tranform != tr)
        {
            m_constraintType = 0;
            m_tranform = tr;

            line3 tRay;

            auto qinv = glm::inverse(tr->m_orientation);

            tRay.p = point3(qinv * ( mouseProjectionRay.p.asVec() - tr->m_position));//point3( glm::vec3(invM * vec4(mouseProjectionRay.p.asVec(), 1.0f)) );
            tRay.v = qinv * mouseProjectionRay.v;//glm::vec3( invM * glm::vec4(mouseProjectionRay.v,0.0f) );


            vec3 axes[3] = { tr->m_orientation * glm::vec3(1,0,0) ,
                             tr->m_orientation * glm::vec3(0,1,0) ,
                             tr->m_orientation * glm::vec3(0,0,1) };

            vec3 axes_e[3] = { {1, 0, 0 },
                               {0, 1, 0 },
                               {0, 0, 1 }};

            // First check if the mouse ray intersects
            // any of the three axis bounding boxes
            //for(auto & ax : axes)
            for(uint32_t i=0;i<3;i++)
            {
                auto & ax = axes[i];
                auto & e  = axes_e[i];

                // axis box is in transform-space
                vka::box_t<float> axisBox( point3( e ),  e + (vec3(1.0f)-e)*0.1f );

                if( intersects( axisBox, tRay) )
                {
                    m_axis = line3( point3(m_tranform->m_position), ax );


                    // find the intersection point between the axis and the mouseray
                    auto L  = intersectingLine(m_axis, mouseProjectionRay);
                    // project the point onto the axis
                    m_offset = (L.p.asVec() + displacement(L.p, m_axis)) - m_tranform->m_position;
                    m_constraintType = 1;
                    return;
                }
            }

            // now check if the mouse ray intersects any of the axis planes
            // either XY, XZ, or YZ;
//            for(auto & n : axes)
            for(uint32_t i=0;i<3;i++)
            {
                auto & n = axes_e[i];

                auto p = vec3(1.0f) - n;
                auto size = p * 0.25f * 0.5f + n*0.05f;

                vka::box_t<float> planeBox( point3(0.5f*p),  size );

                if( intersects(planeBox, tRay) )
                {
                    m_plane  = plane3( point3( m_tranform->m_position), axes[i] );
                    m_offset = intersection(m_plane, mouseProjectionRay).asVec() - m_tranform->m_position;
                    m_constraintType=2;
                    return;
                }
            }



#if 1
            // now check if the mouse ray intersects any of the axis planes
            // either XY, XZ, or YZ;
            plane3 cloestPlane;
            float  closestDistance = std::numeric_limits<float>::max();

            for(uint32_t i=0;i<3;i++)
            {
                auto & n = axes[i];
                auto & e = axes_e[i];

                // center if disc in transform-space
                vec3 center = 1.0f * ( glm::vec3(1.0f)-e );

                // center of disc in worldspace
                vec3 wcenter = m_tranform->m_orientation * center + m_tranform->m_position;

                // plane of the disc in world space
                vka::plane3 rPlane( point3( wcenter), n);

                // find the point where the mouse projection ray
                // intersects with each of the axis planes
                auto p = intersection(rPlane, mouseProjectionRay);

                auto distanceFromOrigin = glm::length( p.asVec() - wcenter );
                auto distanceFromCamera = glm::length( p - mouseProjectionRay.p);

                if( distanceFromCamera < closestDistance)
                {
                    if( distanceFromOrigin < 0.2f )
                    {
                        cloestPlane = rPlane;
                        closestDistance = distanceFromCamera;

                        m_constraintType    = 3;
                        m_plane             = rPlane;
                        m_rotationOffset    = glm::normalize(p.asVec() - m_tranform->m_position);
                        m_quatOffset        = m_tranform->m_orientation;
                        m_rotationAxisLocal = axes_e[i];
                    }
                }

            }
#endif
        }

        if(m_constraintType==2)
        {

            // point at which the ray intersects the plane
            auto _point   = intersection( m_plane, mouseProjectionRay ).asVec();
            m_tranform->m_position = _point - m_offset;
        }
        else if(m_constraintType==1)
        {
            auto L  = intersectingLine(m_axis, mouseProjectionRay);
            m_tranform->m_position = L.p.asVec() - m_offset;
        }
        else if( m_constraintType==3) // rotation
        {
            auto p = intersection(m_plane, mouseProjectionRay);

            // vector that points from origin to the mouse intersection
            auto currentOffset = glm::normalize(p.asVec()-m_tranform->m_position);

            auto & x = m_rotationOffset;
            auto & r = currentOffset;

            auto dx = glm::dot( x, r);

            auto dy = glm::dot( -glm::cross( x, m_plane.n), r);

            m_rotationAngle = std::atan2( dy,dx);

            m_tranform->m_orientation = m_quatOffset;
            m_tranform->rotateLocal( m_rotationAxisLocal, m_rotationAngle);
        }
    }
    vka::transform * m_tranform=nullptr;

    line3  m_axis;
    plane3 m_plane;


    glm::vec3 m_rotationAxisLocal;
    glm::vec3 m_rotationOffset;
    glm::quat m_quatOffset;

    float     m_rotationAngle;
    int    m_constraintType=0;

    glm::vec3 m_offset;
};



}


#endif
