#ifndef VKA_ORBIT_CONTROLLER_H
#define VKA_ORBIT_CONTROLLER_H

#include <vka/math/transform.h>
#include "FPSController.h"

namespace vka
{

/**
 * @brief The OrbitController class
 *
 * The Orbit controller is used to keep a transform
 * pointing at another transform
 */
class OrbitController
{
public:

    void step(vka::transform * tr,
              vka::transform const * target,
              float dt)
    {
        (void)dt;
        bool changed=false;
        if( m_transform != tr )
        {
            m_transform        = tr;
            changed=true;
            m_desiredTransform = *m_transform;

        }
        if( m_target != target )
        {
            m_target = target;
            changed=true;
        }

        if( changed && m_target && m_transform)
        {
            setAngles( m_target->m_position, m_transform->m_position);
        }

        // get the unit-vector offset
        auto r = offset();

        // where our end goal should be for the camera
        m_desiredTransform.position = m_target->m_position + m_R * r;
        m_desiredTransform.lookat( target->m_position, {0,1,0} );

        auto q0 = m_transform->m_orientation;
        auto q1 = m_desiredTransform.rotation;

        auto qs = slerp(q0,q1, 0.3f);
        auto qr = qs * glm::vec3(0,0,1) * m_R;//glm::length(m_transform->m_position-m_target->m_position);

        m_transform->m_position    = m_target->m_position + qr;
        m_transform->m_orientation = qs;
    }

    void lookH(float dH_angle)
    {
        phi += dH_angle;
    }

    void lookV(float dV_angle)
    {
        theta += dV_angle;
        theta = glm::clamp( theta, thetaRange.x, thetaRange.y);
    }

    void zoom(float dR)
    {
        m_R += dR;
    }

    void setAzimuth(float radians)
    {
        phi = radians;
    }
    void setElevation(float radians)
    {
        theta = glm::clamp(radians, thetaRange.x, thetaRange.y);
    }
    void setRange(float R)
    {
        m_R = R;
    }

    glm::vec3 offset() const
    {
        float Rc = cosf(theta);
        return glm::vec3{ Rc*sinf(phi), sinf(theta), Rc*cosf(phi) };
    }

    static glm::vec3 getAzimuthElevationRange(glm::vec3 const& target, glm::vec3 const & camera)
    {
        auto dr = camera - target;
        auto R  = glm::length(dr);
        dr /= R;

        float t = std::asin(dr.y);
        float p = std::atan2( dr.x, dr.z);
        return {p,t,R};
    }

    void setAngles(glm::vec3 const & target, glm::vec3 const & camera)
    {
        auto dr = glm::normalize(camera - target);
        theta = std::asin(dr.y);

        phi = std::atan2( dr.x, dr.z);
    }

    vka::transform       * m_transform = nullptr;
    vka::transform const * m_target    = nullptr;

    vka::transform m_desiredTransform;

    glm::vec3  m_desiredAngles;

    float     theta = 0.0f;
    float     phi   = 0.0f;

    float     thetaFactor = 1.0f;
    float     phiFactor   = 1.0f;
    float     rangeFactor = 1.0f;

    glm::vec2 thetaRange = glm::vec2{ -glm::half_pi<float>()*0.98f, glm::half_pi<float>()*0.98f };

    glm::vec3              m_up = {0,1,0};
    float                  m_R  = 10.0f;

};

}
#endif

