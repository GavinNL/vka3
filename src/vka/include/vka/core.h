#ifndef VKA_CORE_H
#define VKA_CORE_H

#include <vka/utils/HostTriMesh.h>
#include <vka/utils/HostImage.h>

#include <vka/core/TextureMemoryPool.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/RenderTarget.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/Primatives.h>

#include <vka/core/System.h>
#include <vka/core/BufferMemoryPool.h>
#include <vka/core/DescriptorSets.h>
#include <vka/core/PipelineCreateInfo.h>
#include <vka/core/DeviceMeshPrimitive.h>
#include <vka/core/TextureMemoryPool.h>
#include <vka/core/Primatives.h>


#endif
