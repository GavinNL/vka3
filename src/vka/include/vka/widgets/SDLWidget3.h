#ifndef VKA_SDL_WIDGET3_H
#define VKA_SDL_WIDGET3_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.hpp>
#include <vkw/SDLVulkanWindow.h>

#include "InputEvents.h"

#include "Application.h"

#include <thread>
#include <iostream>

namespace vka
{


/**
 * @brief The SDLVulkanWidget struct
 *
 * The SDLVulkanWidget use an SDL_Window to provide
 * a drawing surface. This mimics the QtVulkanWidget
 * so that they can be used interchangeably without
 * much modification.
 */
class SDLVulkanWidget3 : public vkw::SDLVulkanWindow
{
    std::vector<std::string> m_validationLayers;// = {"VK_LAYER_KHRONOS_validation"};

public:
    struct CreateInfo  : public InitilizationInfo,
                         public SurfaceInitilizationInfo
    {
        std::string windowTitle;
        uint32_t width;
        uint32_t height;
    };

    ~SDLVulkanWidget3()
    {
    }

    CreateInfo m_createInfo;
    void create(CreateInfo &C)
    {
        m_createInfo = C;

        createWindow( C.windowTitle.c_str(),
                     SDL_WINDOWPOS_CENTERED,
                     SDL_WINDOWPOS_CENTERED,
                     static_cast<int>(C.width),
                     static_cast<int>(C.height));

        createVulkanInstance(C);

        initSurface(C);
    }

    void finalize(vka::Application * app)
    {
        app->releaseSwapChainResources();
        app->releaseResources();
    }

    template<typename callable_t>
    void  poll( vka::Application * app, callable_t && c)
    {
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            c(event);
            app->nativeWindowEvent(&event);

            switch (event.type)
            {
                case SDL_QUIT : // User pressed the x button.
                {
                    EvtInputQuit e;
                    app->quitEvent(&e);
                    break;
                }
                case SDL_DROPFILE:
                {
                    if( event.drop.file )
                    {
                        EvtInputFileDrop e;
                        e.path = event.drop.file;
                        SDL_free(event.drop.file);
                        app->fileDropEvent(&e);
                    }
                    break;
                }
                case SDL_KEYUP:
                {
                    EvtInputKey E;
                    E.down     = false;
                    E.repeat   = event.key.repeat;
                    E.keycode  = static_cast<vka::KeyCode>(event.key.keysym.sym);
                    E.timestamp = std::chrono::system_clock::now();

                    E.windowKeyCode  = static_cast<uint32_t>(event.key.keysym.sym);
                    E.windowScanCode = static_cast<uint32_t>(event.key.keysym.scancode);
                    E.windowEvent = &E;
                    app->keyReleaseEvent(&E);
                    break;
                }
                case SDL_KEYDOWN:
                {
                    EvtInputKey E;
                    E.down      = true;
                    E.repeat    = event.key.repeat;
                    E.keycode   = static_cast<vka::KeyCode>(event.key.keysym.sym);
                    E.timestamp = std::chrono::system_clock::now();

                    E.windowKeyCode  = static_cast<uint32_t>(event.key.keysym.sym);
                    E.windowScanCode = static_cast<uint32_t>(event.key.keysym.scancode);
                    E.windowEvent    = &E;
                    app->keyPressEvent(&E);
                    break;
                }
                case SDL_MOUSEBUTTONDOWN:
                {
                    EvtInputMouseButton E;
                    E.x      = event.button.x;
                    E.y      = event.button.y;
                    E.button = static_cast<MouseButton>(event.button.button);
                    E.state  = event.button.state;
                    E.clicks = event.button.clicks;

                    app->mousePressEvent(&E);
                    break;
                }
            case SDL_MOUSEBUTTONUP:
                {
                    EvtInputMouseButton E;
                    E.x      = event.button.x;
                    E.y      = event.button.y;
                    E.button = static_cast<MouseButton>(event.button.button);
                    E.state  = event.button.state;
                    E.clicks = event.button.clicks;

                    app->mouseReleaseEvent(&E);
                    break;
                }
            case SDL_MOUSEMOTION:
                {
                    // we have moved the mouse cursor

                    EvtInputMouseMotion M;
                    M.x = event.motion.x;
                    M.y = event.motion.y;
                    M.xrel = event.motion.xrel;
                    M.yrel = event.motion.yrel;

                    app->mouseMoveEvent(&M);
                    break;
                }
            case SDL_MOUSEWHEEL:
                {
                    // we have moved the mouse cursor

                    EvtInputMouseWheel M;
                    M.delta = event.wheel.y;
                    M.x = event.wheel.x;
                    M.y = event.wheel.y;

                    app->mouseWheelEvent(&M);
                    break;
                }
            }
        }
    }

    ApplicationFrame _frame;
    auto render( vka::Application * app)
    {
        auto fr = acquireNextFrame();

       // _frame.currentCommandBuffer.reset(vk::CommandBufferResetFlagBits::eReleaseResources);
        _frame.currentCommandBuffer = fr.commandBuffer;
        _frame.currentCommandBuffer.begin( vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse) );

        _frame.swapchainImageSize        = fr.swapchainSize;
        _frame.defaultRenderPass         = fr.renderPass;

        _frame.depthStencilFormat        = vk::Format(fr.depthFormat);
        _frame.swapchainColorFormat      = vk::Format(fr.swapchainFormat);

        _frame.currentSwaphainIndex      = fr.swapchainIndex;
        _frame.currentFrameBuffer        = fr.framebuffer;
        _frame.currentSwapchainImage     = fr.swapchainImage;
        _frame.currentSwapchainImageView = fr.swapchainImageView;

        _frame.currentCommandBuffer      = fr.commandBuffer;

        _frame.lastFrameTime             = std::chrono::system_clock::now();
        std::swap(_frame.lastFrameTime, _frame.frameTime);

        app->m_renderNextFrame = false;
        app->render(_frame);

        _frame.currentCommandBuffer.end();
        return fr;
    }

    void _initSwapchainVars(vka::Application * app)
    {
        app->m_swapChainSize       = getSwapchainExtent();
        app->m_swapChainFormat     = vk::Format(getSwapchainFormat());
        app->m_swapChainDepthFormat= vk::Format(getDepthFormat());
        app->m_concurrentFrameCount= static_cast<uint32_t>(m_swapchainFrameBuffers.size());
        app->m_currentFrame        = 0;
        app->m_defaultRenderPass   = m_renderPass;

        app->m_swapchainImageViews.clear();
        app->m_swapchainImages.clear();
        for(auto & f : m_swapchainImageViews)
        {
            app->m_swapchainImageViews.push_back( f );
        }
        for(auto & f : m_swapchainImages)
        {
            app->m_swapchainImages.push_back(f);
        }

        app->m_currentSwapchainIndex=0;
    }
    /**
     * @brief exec
     * @return
     *
     * Similar to Qt's app.exec(). this will
     * loop until the the windows is closed
     */
    int exec(vka::Application * app)
    {
        app->m_device         = getDevice();
        app->m_physicalDevice = getPhysicalDevice();
        app->m_instance       = getInstance();
        _initSwapchainVars(app);
        app->initResources();
        app->initSwapChainResources();

        while( true )
        {
            bool resize=false;
            poll(app, [&resize](SDL_Event const &E)
            {
                if (E.type == SDL_WINDOWEVENT && E.window.event == SDL_WINDOWEVENT_RESIZED
                        /*&& event.window.windowID == SDL_GetWindowID( window->getSDLWindow()) */ )
                {
                    resize=true;
                }
            });
            if(resize)
            {
                app->releaseSwapChainResources();
                rebuildSwapchain();

                _initSwapchainVars(app);
                app->initSwapChainResources();
            }

            if( app->shouldQuit() )
                break;

            if( app->shouldRender() )
            {
                auto fr = render(app);

                frameReady(fr);
            }

            std::this_thread::sleep_for( std::chrono::milliseconds(1));
        }

        finalize(app);

        return 0;
    }

    /**
     * @brief frameReady
     *
     * Call this function to present the frame.
     */
    void frameReady(vkw::Frame & fr)
    {
        submitFrame(fr);
        presentFrame(fr);
        waitForPresent();
    }
};



}

#endif

