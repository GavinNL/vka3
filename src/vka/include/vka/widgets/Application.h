#ifndef VKA_VULKAN_APPLICATION2_H
#define VKA_VULKAN_APPLICATION2_H

#include <thread>

#include <vulkan/vulkan.hpp>

#include <vka/utils/resource_path.h>

#include "InputEvents.h"

#define VKA_APP_UNUSED(expr) (void)expr;

namespace vka
{

struct ApplicationFrame
{
    vk::Extent2D           swapchainImageSize;
    vk::RenderPass         defaultRenderPass;
    vk::Format             depthStencilFormat;
    vk::Format             swapchainColorFormat;

    uint32_t               currentSwaphainIndex;
    vk::Framebuffer        currentFrameBuffer;
    vk::Image              currentSwapchainImage;
    vk::ImageView          currentSwapchainImageView;

    vk::CommandBuffer      currentCommandBuffer;

    std::chrono::system_clock::time_point frameTime     = std::chrono::system_clock::now();
    std::chrono::system_clock::time_point lastFrameTime = std::chrono::system_clock::now();
};

class Application
{
public:


    virtual ~Application()
    {
    }
    /**
     * @brief init
     * @param System
     *
     * This function will be called to initilize
     * the all the memory/objects you need.
     *
     * This should basically be used as your constructor.
     *
     * The swapchain may not have been created by this point
     *
     */
    virtual void initResources() = 0;

    /**
     * @brief releaseResources
     *
     * This is called when the vulkan application is about to be shut down
     * Use this to release all vulkan resources.
     */
    virtual void releaseResources() = 0;


    /**
     * @brief initSwapChainResources
     *
     *
     * This method is called whenever the swapchain changes its size.
     * we can use this method to allocate any offscreen render targets.
     * that might be dependent on the swapchain size.
     */
    virtual void initSwapChainResources() = 0;


    /**
     * @brief releaseSwapChainResources
     *
     * This method gets called whenever the swapchain has been
     * resized. This method will be called to release any
     * memory or resources which was allocated by a previous call to
     * initSwapChainResources()
     *
     * After this method is called, another call to initSwapChainResources()
     * will automatically be called.
     */
    virtual void releaseSwapChainResources() = 0;



    /**
     * @brief preRender
     *
     * Called prior to rendering the frame. You can use this method
     * to update any descriptor sets that will be used for the n
     * next frame.
     */
    virtual void preRender() {};


    /**
     * @brief render
     * @param frame
     *
     * The render() method is called at each frame and at
     * a rate determiend by the RenderSurface.
     *
     * frame contains the following information which you can use
     * : frame.
     *
     *   frame.defaultRenderPass - the default render pass
     *   frame.currentFrameBuffer - the current framebuffer in the render pass;
     *   frame.currentCommandBuffer - the command buffer to be used to draw;
     *   frame.swapChainImageSize - the extents of the swapchain;
     */
    virtual void render(ApplicationFrame &frame) = 0;


    virtual void postRender()  {};
    //=========================================================================



    //=========================================================================
    // Input Callbacks
    //=========================================================================
    virtual void mouseWheelEvent(EvtInputMouseWheel const * e)
    {
        VKA_APP_UNUSED(e);
    }
    virtual void mouseMoveEvent(EvtInputMouseMotion const * e)
    {
        VKA_APP_UNUSED(e);
    }
    virtual void mousePressEvent(EvtInputMouseButton const * e)
    {
        VKA_APP_UNUSED(e);
    }
    virtual void mouseReleaseEvent(EvtInputMouseButton const * e)
    {
        VKA_APP_UNUSED(e);
    }
    virtual void keyPressEvent(EvtInputKey const * e)
    {
        VKA_APP_UNUSED(e);
    }
    virtual void keyReleaseEvent(EvtInputKey const * e)
    {
        VKA_APP_UNUSED(e);
    }
    virtual void fileDropEvent(EvtInputFileDrop const * e)
    {
        VKA_APP_UNUSED(e);
    }
    // called when the user has hit the close button
    // and has instructed the window to close.
    virtual void quitEvent(EvtInputQuit const * e)
    {
        quit();
        VKA_APP_UNUSED(e);
    }
    virtual void nativeWindowEvent(void const * e)
    {
        VKA_APP_UNUSED(e);
    }
    //=========================================================================

    void requestNextFrame()
    {
        renderNextFrame();
    }
    void renderNextFrame()
    {
        m_renderNextFrame=true;
    }
    bool shouldRender() const
    {
        return m_renderNextFrame;
    }

    //=========================================================================
    vk::Extent2D swapchainImageSize() const
    {
        return m_swapChainSize;
    }

    vk::Format colorFormat() const
    {
        return m_swapChainFormat;
    }
    vk::Format depthStencilFormat() const
    {
        return m_swapChainDepthFormat;
    }
    uint32_t swapchainImageCount() const
    {
        return static_cast<uint32_t>(m_swapchainImages.size());
    }
    uint32_t currentSwapchainImageIndex() const
    {
        return m_currentSwapchainIndex;
    }
    vk::Image swapchainImage(uint32_t indx) const
    {
        return m_swapchainImages.at(indx);
    }
    vk::ImageView swapchainImageView(uint32_t indx) const
    {
        return m_swapchainImageViews.at(indx);
    }
    //=========================================================================


    uint32_t concurrentFrameCount() const
    {
        return m_concurrentFrameCount;
    }

    uint32_t currentFrame() const
    {
        return m_currentFrame;
    }

    vk::RenderPass getDefaultRenderPass() const
    {
        return m_defaultRenderPass;
    }

    void quit()
    {
        m_quit=true;
    }

    bool shouldQuit() const
    {
        return m_quit;
    }

    void addPath(const std::string & path)
    {
        m_rootPaths->addPath(path);
    }
    std::string getPath(const std::string & filename) const
    {
        return m_rootPaths->get(filename);
    }
    std::shared_ptr<vka::Resources> getRootPaths()
    {
        return m_rootPaths;
    }




    void startThreadPolling( std::chrono::microseconds frameTime = std::chrono::milliseconds(15))
    {
        stopThreadPolling();
        m_stopPolling = false;
        m_pollThread = std::thread( [&]( std::chrono::microseconds ft)
        {
            while( !m_stopPolling )
            {
                std::this_thread::sleep_for( ft );
                requestNextFrame();
            }
        }, frameTime);

    }
    void stopThreadPolling()
    {
        m_stopPolling = true;
        if( m_pollThread.joinable())
        {
            m_pollThread.join();
        }

    }

protected:
    friend class QTRenderer;
    friend class SDLVulkanWidget;
    friend class QTVulkanWidget;

    vk::Instance               m_instance;
    vk::Device                 m_device;
    vk::PhysicalDevice         m_physicalDevice;

    vk::Extent2D               m_swapChainSize;
    vk::Format                 m_swapChainFormat = vk::Format::eUndefined;
    vk::Format                 m_swapChainDepthFormat= vk::Format::eUndefined;
    uint32_t                   m_concurrentFrameCount=0;
    uint32_t                   m_currentFrame=0;
    uint32_t                   m_currentSwapchainIndex=0;
    std::vector<vk::Image>     m_swapchainImages;
    std::vector<vk::ImageView> m_swapchainImageViews;

    vk::RenderPass             m_defaultRenderPass;
    bool                       m_quit=false;
    bool                       m_renderNextFrame=true;
    bool                       m_stopPolling=false;
    std::thread                m_pollThread;

    std::shared_ptr<vka::Resources> m_rootPaths = std::make_shared<vka::Resources>();

    friend class QTVulkanWidget;
    friend class SDLVulkanWidget;
    friend class SDLVulkanWidget2;
    friend class QtVulkanWidget2;
    friend class SDLVulkanWidget3;

};

}

#undef VKA_APP_UNUSED

#endif
