#ifndef VKA_SDL_WIDGET2_H
#define VKA_SDL_WIDGET2_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_vulkan.h>
#include <vulkan/vulkan.hpp>

#include <vka/core/System.h>

#include <vka/widgets/InputEvents.h>

#include "Application.h"

#include <thread>
#include <iostream>

namespace vka
{


/**
 * @brief The SDLVulkanWidget struct
 *
 * The SDLVulkanWidget use an SDL_Window to provide
 * a drawing surface. This mimics the QtVulkanWidget
 * so that they can be used interchangeably without
 * much modification.
 */
class SDLVulkanWidget2
{
    public:

    std::vector<std::string> m_validationLayers;// = {"VK_LAYER_KHRONOS_validation"};

    ~SDLVulkanWidget2()
    {
    }


    void create(uint32_t width, uint32_t height, PFN_vkDebugReportCallbackEXT callback)
    {                                                         
        auto window = initWindow(width, height );
        _initVulkanObjects(window, callback);
        createSystem();
    }


    void   init( vka::Application * app)
    {
        _createScreenBuffer();
        _createPools();

        _initSwapchainVars(app);

        app->m_device         = m_device;
        app->m_physicalDevice = m_physicalDevice;
        app->m_instance       = m_instance;
        app->initResources();
        app->initSwapChainResources();
    }

    void finalize(vka::Application * app)
    {
        app->releaseSwapChainResources();
        app->releaseResources();
        destroySystem();
    }

    void  poll( vka::Application * app)
    {
        SDL_Event event;

        while (SDL_PollEvent(&event))
        {
            app->nativeWindowEvent(&event);

            switch (event.type)
            {
                case SDL_QUIT : // User pressed the x button.
                {
                    EvtInputQuit e;
                    app->quitEvent(&e);
                    break;
                }
                case SDL_DROPFILE:
                {
                    if( event.drop.file )
                    {
                        EvtInputFileDrop e;
                        e.path = event.drop.file;
                        SDL_free(event.drop.file);
                        app->fileDropEvent(&e);
                    }
                    break;
                }
                case SDL_KEYUP:
                {
                    EvtInputKey E;
                    E.down     = false;
                    E.repeat   = event.key.repeat;
                    E.keycode  = static_cast<vka::KeyCode>(event.key.keysym.sym);
                    E.timestamp = std::chrono::system_clock::now();

                    E.windowKeyCode  = static_cast<uint32_t>(event.key.keysym.sym);
                    E.windowScanCode = static_cast<uint32_t>(event.key.keysym.scancode);
                    E.windowEvent = &E;
                    app->keyReleaseEvent(&E);
                    break;
                }
                case SDL_KEYDOWN:
                {
                    EvtInputKey E;
                    E.down      = true;
                    E.repeat    = event.key.repeat;
                    E.keycode   = static_cast<vka::KeyCode>(event.key.keysym.sym);
                    E.timestamp = std::chrono::system_clock::now();

                    E.windowKeyCode  = static_cast<uint32_t>(event.key.keysym.sym);
                    E.windowScanCode = static_cast<uint32_t>(event.key.keysym.scancode);
                    E.windowEvent    = &E;
                    app->keyPressEvent(&E);
                    break;
                }
                case SDL_MOUSEBUTTONDOWN:
                {
                    EvtInputMouseButton E;
                    E.x      = event.button.x;
                    E.y      = event.button.y;
                    E.button = static_cast<MouseButton>(event.button.button);
                    E.state  = event.button.state;
                    E.clicks = event.button.clicks;

                    app->mousePressEvent(&E);
                    break;
                }
            case SDL_MOUSEBUTTONUP:
                {
                    EvtInputMouseButton E;
                    E.x      = event.button.x;
                    E.y      = event.button.y;
                    E.button = static_cast<MouseButton>(event.button.button);
                    E.state  = event.button.state;
                    E.clicks = event.button.clicks;

                    app->mouseReleaseEvent(&E);
                    break;
                }
            case SDL_MOUSEMOTION:
                {
                    // we have moved the mouse cursor

                    EvtInputMouseMotion M;
                    M.x = event.motion.x;
                    M.y = event.motion.y;
                    M.xrel = event.motion.xrel;
                    M.yrel = event.motion.yrel;

                    app->mouseMoveEvent(&M);
                    break;
                }
            case SDL_MOUSEWHEEL:
                {
                    // we have moved the mouse cursor

                    EvtInputMouseWheel M;
                    M.delta = event.wheel.y;
                    M.x = event.wheel.x;
                    M.y = event.wheel.y;

                    app->mouseWheelEvent(&M);
                    break;
                }
            }
        }
    }

    ApplicationFrame _frame;
    void render( vka::Application * app)
    {
        m_frameBufferIndex = m_screenBuffer.getNextFrameIndex(m_semaphore_imageAvailable);

        auto & m_mainCommandBuffer = m_commandBuffers.at(m_frameBufferIndex);

        m_mainCommandBuffer.reset(vk::CommandBufferResetFlagBits::eReleaseResources);
        m_mainCommandBuffer.begin( vk::CommandBufferBeginInfo(vk::CommandBufferUsageFlagBits::eSimultaneousUse) );

                auto rp = m_screenBuffer.getRenderPassBeginInfo();

                _frame.swapchainImageSize        = rp.renderArea.extent;
                _frame.defaultRenderPass         = m_screenBuffer.getDefaultRenderPass();

                _frame.depthStencilFormat        = app->depthStencilFormat();
                _frame.swapchainColorFormat      = app->colorFormat();

                _frame.currentSwaphainIndex      = m_frameBufferIndex;
                _frame.currentFrameBuffer        = rp.framebuffer;
                _frame.currentSwapchainImage     = app->m_swapchainImages.at(_frame.currentSwaphainIndex);
                _frame.currentSwapchainImageView = app->m_swapchainImageViews.at(_frame.currentSwaphainIndex);

                _frame.currentCommandBuffer      = m_mainCommandBuffer;

                _frame.lastFrameTime             = std::chrono::system_clock::now();
                std::swap(_frame.lastFrameTime, _frame.frameTime);

                app->m_renderNextFrame = false;
                app->render(_frame);
        m_mainCommandBuffer.end();
    }

    /**
     * @brief exec
     * @return
     *
     * Similar to Qt's app.exec(). this will
     * loop until the the windows is closed
     */
    int exec(vka::Application * app)
    {
        init(app); // init resources

        while( true )
        {
            poll(app);

            if( app->shouldQuit() )
                break;

            if( app->shouldRender() )
            {
                render(app);

                try
                {
                    frameReady();
                }
                catch (vk::OutOfDateKHRError & e)
                {
                    app->releaseSwapChainResources();
                    _createScreenBuffer();
                    _initSwapchainVars(app);
                    app->initSwapChainResources();
                }
                //app->renderNextFrame();
            }

            std::this_thread::sleep_for( std::chrono::milliseconds(1));
        }

        finalize(app);

        return 0;
    }

    /**
     * @brief frameReady
     *
     * Call this function to present the frame.
     */
    void frameReady()
    {
        {
            vk::SubmitInfo info;
            info.waitSemaphoreCount   = 1;
            info.pWaitSemaphores      = &m_semaphore_imageAvailable;
            info.signalSemaphoreCount = 1;
            info.pSignalSemaphores    = &m_semaphore_renderComplete;
            info.commandBufferCount   = 1;
            info.pCommandBuffers      = &m_commandBuffers[m_frameBufferIndex];

            vk::PipelineStageFlags wait_stage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
            info.pWaitDstStageMask = &wait_stage;

            vka::System::get().submitCommandBuffers(info);


            m_screenBuffer.presentFrameIndex(m_frameBufferIndex, m_semaphore_renderComplete);
        }
    }

protected:
    vka::ScreenBuffer            m_screenBuffer;
    vk::SurfaceKHR               m_surface;
    vk::CommandPool              m_commandPool;

    std::vector<vka::CommandBuffer> m_commandBuffers;

    uint32_t                     m_frameBufferIndex;
    // create a default command pool
    vk::Semaphore                m_semaphore_imageAvailable;
    vk::Semaphore                m_semaphore_renderComplete;

    vk::DebugReportCallbackEXT   m_callbackObj;
    vk::PhysicalDevice           m_physicalDevice;
    vk::Device                   m_device;
    SDL_Window                  *m_window = nullptr;
    vk::Instance                 m_instance;

    uint32_t m_graphicsQueueFamily = std::numeric_limits<uint32_t>::max();
    uint32_t m_presentQueueFamily  = std::numeric_limits<uint32_t>::max();



    void _initSwapchainVars(vka::Application * app)
    {
        app->m_swapChainSize       = m_screenBuffer.m_extent;
        app->m_swapChainFormat     = System::getSurfaceFormat( m_physicalDevice, m_surface).format;
        app->m_swapChainDepthFormat= vk::Format::eUndefined;
        app->m_concurrentFrameCount= static_cast<uint32_t>(m_screenBuffer.m_frameBuffers.size());
        app->m_currentFrame        = 0;
        app->m_defaultRenderPass   = m_screenBuffer.m_renderPass;

        for(auto & f : m_screenBuffer.m_frameBuffers)
        {
            assert( System::get().info(f).createInfo.attachments.size() == 1);
            app->m_swapchainImageViews.push_back( System::get().info(f).createInfo.attachments[0] );
        }
        app->m_swapchainImages = System::get().info( m_screenBuffer.m_SwapChain ).images;

        app->m_currentSwapchainIndex=0;
    }

    void _initVulkanObjects(SDL_Window* window, PFN_vkDebugReportCallbackEXT debugCallback=nullptr)
    {
        m_window = window;
        //=======================================================================
        // BOILER PLATE CODE TO GET THE INSTANCE and DEVICE SETUP
        //=======================================================================
        // 1. Create an instance using teh vka::InstanceCreateInfo2 struct
        vka::InstanceCreateInfo2 instanceCreateInfo;
        instanceCreateInfo.requiredExtensions  = SDL_GetInstanceExtensions(m_window);
        instanceCreateInfo.requiredExtensions.push_back( VK_EXT_DEBUG_REPORT_EXTENSION_NAME); // need this for debug callback
        instanceCreateInfo.validationLayers    = m_validationLayers;

        m_instance    = vka::createInstance(instanceCreateInfo);

        if( debugCallback)
        {
            m_callbackObj = vka::createDebugCallback( m_instance, debugCallback );
        }

        // 2. Create a surface using the SDL function. We need this first to
        //    figure out if we can draw to this surface.
        m_surface = SDL_CreateVulkanSurface(m_instance, m_window);

        // 3. Create a physical device using the physicalDeviceCreateInfo2 struct
        vka::PhysicalDeviceCreateInfo2 physicalDeviceCreateInfo;
        physicalDeviceCreateInfo.instance = m_instance;
        physicalDeviceCreateInfo.surface  = m_surface;
        physicalDeviceCreateInfo.deviceExtensions = {VK_KHR_SWAPCHAIN_EXTENSION_NAME}; // no need for this
                                                                                       // it's automatically set as the default.


        m_physicalDevice = vka::createPhysicalDevice(physicalDeviceCreateInfo);
        assert(m_physicalDevice);


        // 4. create a logical device using the deviceCreateInfo2 struct
        vka::DeviceCreateInfo2 deviceCreateInfo;
        deviceCreateInfo.physicalDevice = m_physicalDevice;
        deviceCreateInfo.requiredDeviceExtensions = physicalDeviceCreateInfo.deviceExtensions; //
        deviceCreateInfo.requiredValidationLayers = instanceCreateInfo.validationLayers;
        // 4.1 find appropriate queues that can present graphics to the surface.
        deviceCreateInfo.findAppropriateQueues(m_surface);
        assert( deviceCreateInfo.graphicsQueueFamily != std::numeric_limits<uint32_t>::max() );
        assert( deviceCreateInfo.presentQueueFamily  != std::numeric_limits<uint32_t>::max() );

        deviceCreateInfo.deviceFeatures = m_physicalDevice.getFeatures();

        m_device = vka::createDevice(deviceCreateInfo);

        m_graphicsQueueFamily = deviceCreateInfo.graphicsQueueFamily;
        m_presentQueueFamily  = deviceCreateInfo.presentQueueFamily;
        return;

    }


    void _createPools()
    {
       auto & Sys = vka::System::get();
       m_commandPool = Sys.createCommandPool();

       for(uint32_t i=0;i<m_screenBuffer.m_frameBuffers.size();i++)
       {
           m_commandBuffers.push_back( Sys.allocateCommandBuffer(vk::CommandBufferLevel::ePrimary, m_commandPool) );
       }

       m_semaphore_imageAvailable = Sys.createSemaphore();
       m_semaphore_renderComplete = Sys.createSemaphore();
    }

    void _createScreenBuffer()
    {
        auto ext = getWindowExtent(m_window);

        //auto surfaceFormat = System::getSurfaceFormat( m_physicalDevice, m_surface);

        vka::ScreenBufferCreateInfo createInfo;
        createInfo.surface              = m_surface;
        createInfo.extent               = ext;
        createInfo.verticalSync         = true;
        createInfo.extraSwapchainImages = 1; // request 1 extra swapchain image
        createInfo.maxSwapchainImages   = 3; // up to a max of 3
        //createInfo.depthStencilFormatOrImage  = depthTexture;

        if( m_screenBuffer.m_SwapChain)
            m_screenBuffer = vka::System::get().createScreenBuffer( createInfo, &m_screenBuffer );
        else
            m_screenBuffer = vka::System::get().createScreenBuffer( createInfo );

    }

    void createSystem()
    {
        // Create all the vulkan objects
        // You do not need to use this function.
        // You can create all the objects yourself
        vka::SystemCreateInfo2 sysCreateInfo;
        sysCreateInfo.device              = m_device;
        sysCreateInfo.instance            = m_instance;
        sysCreateInfo.surface             = m_surface;
        sysCreateInfo.physicalDevice      = m_physicalDevice;
        sysCreateInfo.queuePresentFamily  = m_presentQueueFamily;
        sysCreateInfo.queueGraphicsFamily = m_graphicsQueueFamily;
        sysCreateInfo.queuePresent        = m_device.getQueue( static_cast<uint32_t>(m_presentQueueFamily) , 0u );
        sysCreateInfo.queueGraphics       = m_device.getQueue( static_cast<uint32_t>(m_graphicsQueueFamily) , 0u );

        vka::System::createSystem(sysCreateInfo);
    }

    void destroySystem()
    {
        vka::System::get().destroy();

        // Destroy any left over objects we didn't free
        if( m_callbackObj)
            vka::destroyDebugCallback(   m_instance, m_callbackObj );
        m_instance.destroySurfaceKHR(m_surface);
        m_device.destroy();
        m_instance.destroy();
    }

    SDL_Window* initWindow(uint32_t width, uint32_t height)
    {
        SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS );

        if(SDL_Vulkan_LoadLibrary(nullptr) == -1)
        {
            //VKA_CRIT("Error loading vulkan");
            exit(1);
        }
        atexit(SDL_Quit);

        auto window = SDL_CreateWindow("APPLICATION_NAME",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            static_cast<int32_t>(width),
            static_cast<int32_t>(height),
            SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE);

        if(window == nullptr)
        {
            //VKA_CRIT("Couldn\'t set video mode: {}", SDL_GetError());
            exit(1);
        }
        return window;
    }

    inline vk::Extent2D getWindowExtent(SDL_Window * window)
    {
        int w,h;
        SDL_GetWindowSize(window,&w,&h);
        return vk::Extent2D( static_cast<uint32_t>(w) , static_cast<uint32_t>(h) );
    }

    inline std::vector<std::string> SDL_GetInstanceExtensions(SDL_Window * window)
    {
        std::vector<std::string> ext;

        // We only need to populate the .requiredExtensions array
        // with the values returned by SDL
        unsigned int count = 0;
        SDL_Vulkan_GetInstanceExtensions(window, &count, nullptr);

        const char **names = new const char *[count];
        SDL_Vulkan_GetInstanceExtensions(window, &count, names);

        for(uint32_t i=0; i < count ; i++)
        {
            ext.push_back( names[i] );
        }
        return ext;
    }

    inline vk::SurfaceKHR SDL_CreateVulkanSurface(vk::Instance instance, SDL_Window * window)
    {
        // Create a surface using the SDL function
        vk::SurfaceKHR             surface;
        if( !SDL_Vulkan_CreateSurface( window, instance, reinterpret_cast<VkSurfaceKHR*>(&surface)  ) )
        {
            throw std::runtime_error( "Failed to create surface" );
        }
        return surface;
    }
};



}

#endif

