#ifndef VKA2_RENDER_SYSTEM_ID_H
#define VKA2_RENDER_SYSTEM_ID_H

#include <cstdint>
#include <limits>

namespace vka2
{
namespace RenderSystem
{
/**
 * @brief The _ID struct
 * Meant to be used as an index into an array or map that is non-copyable
 * between tempate paramters.
 *
 * eg: _ID<0> x = _ID<1>(3); // wil not work
 */
template<uint32_t _i>
struct _ID
{
    _ID() : id(  std::numeric_limits<uint32_t>::max() )
    {

    }
    explicit _ID(uint32_t index) : id(index)
    {
    }

    _ID( _ID const & i) : id( i.id )
    {
    }

    _ID( _ID && i) : id( i.id )
    {
        i.id = std::numeric_limits<uint32_t>::max();
    }

    _ID& operator = ( _ID const & i)
    {
        if( &i != this)
        {
            id = i.id;
        }
        return *this;
    }

    _ID& operator = ( _ID && i)
    {
        if( &i != this)
        {
            id = i.id;
            i.id = std::numeric_limits<uint32_t>::max();
        }

        return *this;
    }

    bool operator < (_ID const & other) const
    {
        return id < other.id;
    }
    bool operator > (_ID const & other) const
    {
        return id > other.id;
    }
    bool operator == (_ID const & other) const
    {
        return id == other.id;
    }
    bool operator != (_ID const & other) const
    {
        return id != other.id;
    }
    bool operator <= (_ID const & other) const
    {
        return id <= other.id;
    }
    bool operator >= (_ID const & other) const
    {
        return id >= other.id;
    }

    uint32_t get() const
    {
        return id;
    }

    operator bool() const
    {
        return id!=std::numeric_limits<uint32_t>::max();
    }

private:
    uint32_t id = std::numeric_limits<uint32_t>::max();
};


using RenderPassID    = _ID<0>;   // a texture can be a 1d 2d 3d texture as well as a 2d 3d texture array
using TextureID       = _ID<0>;   // a texture can be a 1d 2d 3d texture as well as a 2d 3d texture array
using BufferID        = _ID<1>;
using PrimitiveID     = _ID<2>;   // A primitive is a a static mesh object which includes vertex buffers and optional index buffers
using PipelineID      = _ID<3>;   // A Pipeline is a shader
using DescriptorSetID = _ID<4>;   //
using RenderTargetID  = _ID<5>;   //

}

}

#endif
