#include <iostream>
#include <queue>
#include <tuple>
#include <future>

#include <vka/math.h>

#include <vka/ecs/Engine.h>
#include <glslang/Public/ShaderLang.h>

#include <vka/ecs/Components/CLight.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CCollider.h>
#include <vka/ecs/Components/CRigidBody.h>

#include <vka/ecs/Components/CWSADMovement.h>
#include <vka/ecs/Components/CMouseLook.h>

#include <glm/gtx/io.hpp>
#include <glm/gtc/random.hpp>

#define WIDTH  1024
#define HEIGHT 768

#include <vka/ecs/SDLWindowSystem.h>


// callback function for validation layers
VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;
    #define VKA_INFO(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_WARN(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_ERROR(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_DEBUG(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}


void init(std::shared_ptr<vka::ecs::SystemBus> sb1)
{
        auto m_SystemBus = sb1;
        //auto m_SystemBus = m_enginePtr->m_SystemBus;

        vka::ecs::PBRMaterial unlit;
        unlit.unlit = true;

        auto sphere_id = m_SystemBus->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("sphere1", vka::ecs::HostMeshPrimitive::Sphere(1.0f,32,32) );
        auto box_id    = m_SystemBus->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("box1", vka::ecs::HostMeshPrimitive::Box( glm::vec3(0.5f)) );
        auto grid_id   = m_SystemBus->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("grid1", vka::ecs::HostMeshPrimitive::Grid(10,10) );
        auto unlit_id  = m_SystemBus->resourceCreateAndEmplace<vka::ecs::PBRMaterial>("unlitMat", std::move(unlit) );

        vka::ecs::PBRMaterial defaultMaterial;
        defaultMaterial.roughnessFactor = 0;
        defaultMaterial.metallicFactor = 0;
        auto default_material_id  = m_SystemBus->resourceCreateAndEmplace<vka::ecs::PBRMaterial>("defaultMaterial", std::move(defaultMaterial) );
        (void)sphere_id;// = m_SystemBus->createResource<vka::ecs::HostMeshPrimitive>("sphere1", vka::ecs::HostMeshPrimitive::Sphere(1.0f) );
        (void)grid_id;
        (void)box_id;
        (void)default_material_id;

        if(1)
        {
            auto meshId = grid_id;

            auto E      = m_SystemBus->createEntity("gridEntity");

            E.emplaceComponent( vka::ecs::CMesh(meshId, unlit_id, vk::PrimitiveTopology::eLineList) );
            E.emplaceComponent( vka::ecs::CPosition() );
            E.emplaceComponent( vka::ecs::CRotation() );

        }


        if(1)
        {
            auto E = m_SystemBus->createEntity();
            auto & C = E.emplaceComponent<vka::ecs::CCamera>();
            C.environment = m_SystemBus->resourceCreate<vka::ecs::Environment>(vka::uri("rc:environments/green_hills.environment"));

            vka::Transform t;
            t.position = {-3,3,-3};
            t.lookat( {0,0,0}, {0,1,0});


            E.emplaceComponent( vka::ecs::CPosition    ( t.position ));
            E.emplaceComponent( vka::ecs::CRotation    ( t.rotation ));
            E.emplaceComponent( vka::ecs::CMouseLook   ()            );
            E.emplaceComponent( vka::ecs::CWSADMovement()            );
        }



            auto scene_id = m_SystemBus->resourceGetOrCreate<vka::ecs::GLTFScene2>( vka::uri("rc:models/download/DamagedHelmet.glb"));
        //    auto scene_id = m_SystemBus->resourceGetOrCreate<vka::ecs::GLTFScene2>( vka::uri("rc:models/BoxTextured.gltf"));

            if(1)
            {
                auto E  = m_SystemBus->createEntity();

                auto sb = m_SystemBus;

                E.emplaceComponent( vka::ecs::CScene(scene_id));
                E.emplaceComponent( vka::ecs::CRotation());
                E.emplaceComponent( vka::ecs::CPosition(glm::vec3(0,0,0)));

            }

            if(1)
            {
                auto E  = m_SystemBus->createEntity();

                auto sb = m_SystemBus;

                E.emplaceComponent( vka::ecs::CLight());
                E.emplaceComponent( vka::ecs::CRotation());
                E.emplaceComponent( vka::ecs::CPosition(glm::vec3(3,0,0)));

            }
            if(1)
            {
                auto E  = m_SystemBus->createEntity();

                auto sb = m_SystemBus;

                E.emplaceComponent( vka::ecs::CLight()).color = {1,0,0};
                E.emplaceComponent( vka::ecs::CRotation());
                E.emplaceComponent( vka::ecs::CPosition(glm::vec3(-3,0,0)));

            }

            //==================================================================
}


int main(int argc, char ** argv)
{
    (void)argc;
    (void)argv;

    // This is the main vka engine. It provides all the systems
    // except the window system. The window s ystem will be
    // configured by us.
    std::shared_ptr<vka::ecs::Engine>          m_enginePtr = std::make_shared<vka::ecs::Engine>();

    // grab the system bus so we can configure
    // everything
    auto systemBus = m_enginePtr->m_SystemBus;

    // add the paths to the resources
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");

    // initialize the engine. This will
    // add all the systems to the systembus
    // and configure them
    m_enginePtr->initializeSystems();

    // Call our init function so that
    // we can configure it for our example
    init(systemBus);

    // Finally add the window system

    // the SDLWindow system is an additional system not provided by the Engine class
    // the SDLWindow (or any other window systemn) system should not
    // be added as a main system
    std::shared_ptr<vka::ecs::SDLWindowSystem> m_window = m_enginePtr->m_SystemBus->createSystem<vka::ecs::SDLWindowSystem>();


    // put the window it its main loop
    // the Window System will be responsible for
    // handling the the main loop and all the inputs
    // to send to the systembus.
    //
    // SDL will be initialized at the start of this
    // function call and will be destroyed at the
    // end.
    m_window->mainLoop();


    // Disconnect all the systems except the
    // window system
    systemBus->disconnectAllSystems();

    m_enginePtr.reset();

    return 0;
}

