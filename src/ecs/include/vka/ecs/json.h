#ifndef VKA_ECS_JSON_H
#define VKA_ECS_JSON_H

#include <nlohmann/json.hpp>
#include <vka/math/linalg.h>

namespace vka
{
using json = nlohmann::json;

namespace ecs
{

    template<typename T, int N>
    inline json to_json(glm::vec<N, T, glm::defaultp> const & value )
    {
        //using vec_type = glm::vec<N, T, glm::defaultp>;
        //using value_type = typename vec_type::value_type;
        //(void)value_type;

        json J = json::array();
        int len = value.length() ;
        for(int j=0;j<len;j++)
        {
            J[ static_cast<size_t>(j) ] = value[j];
        }
        return J;
    }

    inline void from(json const & J, const std::string & key, float & value )
    {
        auto i = J.find(key);
        if( i != J.end())
        {
            if( i->is_number() )
            {
                value = i->get<float>();
            }
        }
    }
    inline void from(json const & J, const std::string & key, bool & value )
    {
        auto i = J.find(key);
        if( i != J.end())
        {
            if( i->is_boolean() )
            {
                value = i->get<bool>();
            }
        }
    }
    inline void from(json const & J, const std::string & key, std::string & value )
    {
        auto i = J.find(key);
        if( i != J.end())
        {
            if( i->is_string() )
            {
                value = i->get<std::string>();
            }
        }
    }

    template<typename T, int N>
    inline void from(json const & J, const std::string & key, glm::vec<N, T, glm::defaultp> & value )
    {
        using vec_type = glm::vec<N, T, glm::defaultp>;
        using value_type = typename vec_type::value_type;

        auto i = J.find(key);
        if( i != J.end())
        {
            if( i->is_array() )
            {
                int len = std::min(value.length(), static_cast<int>( i->size() ) );
                for(int j=0;j<len;j++)
                {
                    value[j] = i->at( static_cast<size_t>(j) ).get< value_type >();
                }
            }
        }
    }
    inline void from(json const & J, const std::string & key, glm::quat & value )
    {
        using vec_type = glm::quat;
        using value_type = typename vec_type::value_type;

        auto i = J.find(key);
        if( i != J.end())
        {
            if( i->is_array() )
            {
                int len = std::min(value.length(), static_cast<int>( i->size() ) );
                for(int j=0;j<len;j++)
                {
                    value[j] = i->at( static_cast<size_t>(j) ).get< value_type >();
                }
            }
        }
    }
}
}

#endif
