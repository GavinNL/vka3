#ifndef VKA_ECS_SYSTEM_BASE_2_H
#define VKA_ECS_SYSTEM_BASE_2_H

#include "ECS.h"
#include "SystemBus.h"
#include "SystemBase.h"

#include <easy/profiler.h>

namespace vka
{
namespace ecs
{

struct SystemBaseInternal : public loggers
{
    using entity_type   = entt::entity;
    using registry_type = entt::registry;

    //=================================================================================
    // Called when the system is constructed and added
    // to the list of systems.
    virtual void onConstruct()  {};

    // called during the first iteration of the main loop
    virtual void onStart()  {};

    // called when the systems have stopped
    virtual void onStop()   {};

    // called once every frame.
    virtual void onUpdate() = 0;
    //=================================================================================
    std::vector< std::tuple<size_t, std::string, std::shared_ptr<SystemBaseInternal> > > m_preSystems;
    std::vector< std::tuple<size_t, std::string, std::shared_ptr<SystemBaseInternal> > > m_postSystems;

    void stop()
    {
        EASY_FUNCTION(profiler::colors::Rose);

        EASY_BLOCK( "PreSystems" );
        for(auto & x : m_preSystems)
        {
            EASY_BLOCK( std::get<1>(x) );
            std::get<2>(x)->stop();
            EASY_END_BLOCK
        }
        EASY_END_BLOCK

        if(m_started)
            this->onStop();

        EASY_BLOCK( "PostSystems" );
        for(auto & x : m_postSystems)
        {
            EASY_BLOCK( std::get<1>(x) );
            std::get<2>(x)->stop();
            EASY_END_BLOCK
        }
        EASY_END_BLOCK
    }

    void execute()
    {
        EASY_FUNCTION(profiler::colors::Rose);

        this->start();

        EASY_BLOCK( "PreSystems" );
        for(auto & x : m_preSystems)
        {
            EASY_BLOCK( std::get<1>(x) );
            std::get<2>(x)->m_deltaTime = m_deltaTime;
            std::get<2>(x)->execute();
            EASY_END_BLOCK
        }
        EASY_END_BLOCK

        this->onUpdate();

        EASY_BLOCK( "PostSystems" );
        for(auto & x : m_postSystems)
        {
            EASY_BLOCK( std::get<1>(x) );
            std::get<2>(x)->m_deltaTime = m_deltaTime;
            std::get<2>(x)->execute();
            EASY_END_BLOCK
        }
        EASY_END_BLOCK
    }

    virtual ~SystemBaseInternal()
    {

    }

    //================
    template<typename System_type>
    std::shared_ptr<System_type> addPostSubSystem( std::shared_ptr<System_type> subSystem = nullptr)
    {
        static_assert( std::is_base_of<SystemBaseInternal, System_type>::value, "" );
        std::type_index i( typeid(System_type) );
        auto h = i.hash_code();
        for(auto & s : m_postSystems)
        {
            if( std::get<0>(s) == h)
                return std::dynamic_pointer_cast<System_type>( std::get<2>(s) );
        }

        if( subSystem==nullptr)
            subSystem = std::make_shared<System_type>();

        const std::type_info  &ti = typeid(*subSystem);
        auto name = std::string(ti.name());

        std::shared_ptr<SystemBaseInternal> base = subSystem;
        base->m_systemBus  = m_systemBus;
        base->m_registry   = &m_systemBus->registry;
        m_postSystems.push_back(  {h,name,subSystem} );

        return subSystem;
    }
    template<typename System_type>
    std::shared_ptr<System_type> addPreSubSystem(std::shared_ptr<System_type> subSystem = nullptr)
    {
        static_assert( std::is_base_of<SystemBaseInternal, System_type>::value, "" );
        std::type_index i( typeid(System_type) );
        auto h = i.hash_code();
        for(auto & s : m_preSystems)
        {
            if( std::get<0>(s) == h)
                return std::dynamic_pointer_cast<System_type>( std::get<2>(s) );
        }

        if( subSystem==nullptr)
            subSystem = std::make_shared<System_type>();

        const std::type_info  &ti = typeid(*subSystem);
        auto name = std::string(ti.name());

        std::shared_ptr<SystemBaseInternal> base = subSystem;
        base->m_systemBus  = m_systemBus;
        base->m_registry   = &m_systemBus->registry;
        m_preSystems.push_back(  {h,name,subSystem} );

        return subSystem;
    }

    //================
    template<typename System_type>
    std::shared_ptr<System_type> getOrCreateSystem()
    {
        return m_systemBus->getOrCreateSystem2<System_type>();
    }
    template<typename Component>
    [[nodiscard]] auto on_construct() {
        return m_registry->on_construct<Component>();
    }
    template<typename Component>
    [[nodiscard]] auto on_update() {
        return m_registry->on_update<Component>();
    }
    template<typename Component>
    [[nodiscard]] auto on_destroy() {
        return m_registry->on_destroy<Component>();
    }
    template<typename... Component, typename... Exclude>
    auto view( entt::exclude_t<Exclude...> = {} )
    {
        return m_registry->view<Component...>( entt::exclude<Exclude...>);
    }
    template<typename... Component>
    [[nodiscard]] decltype(auto) get([[maybe_unused]] const entity_type entity) {
        return m_registry->get<Component...>(entity);
    }
    template<typename... Component>
    [[nodiscard]] decltype(auto) get([[maybe_unused]] const entity_type entity) const {
        return m_registry->get<Component...>(entity);
    }
    template<typename... Component>
    [[nodiscard]] bool has(const entity_type entity) const {
        return m_registry->has<Component...>(entity);
    }
    template<typename Component, typename... Args>
    decltype(auto) emplace(const entity_type entity, Args &&... args) {
        return m_registry->emplace<Component>( entity, std::forward<Args>(args)...);
    }
    template<typename Component, typename... Args>
    decltype(auto) emplace_or_replace(const entity_type entity, Args &&... args) {
        return m_registry->emplace_or_replace<Component>( entity, std::forward<Args>(args)...);
    }
    template<typename... Component>
    auto remove(const entity_type entity)  {
        return m_registry->remove<Component...>(entity);
    }
    template<typename... Component>
    auto remove_if_exists(const entity_type entity)  {
        return m_registry->remove_if_exists<Component...>(entity);
    }
    template<typename... Component>
    auto clear()  {
        return m_registry->clear<Component...>();
    }
    bool valid(entity_type e) const
    {
        return m_registry->valid(e);
    }
    auto createEntity()
    {
        return m_systemBus->createEntity().entity;
    }

    void linkToParent(entity_type child, entity_type parent)
    {
        m_systemBus->linkToParent(child, parent);
    }

    template<typename ResourceType>
    bool stageResource( _ID<ResourceType> id)
    {
        return m_systemBus->stageResource<ResourceType>(id);
    }

    // resources
    template<typename Resource_type>
    bool resourceIsHostLoaded( _ID<Resource_type> id ) const
    {
        return m_systemBus->getResourceManager<Resource_type>().template has<Resource_type>(id);
    }
    template<typename Resource_type>
    bool resourceIsDeviceLoaded( _ID<Resource_type> id ) const
    {
        return m_systemBus->getResourceManager<Resource_type>().template has<tag_resource_device_fully_loaded>(id);
    }
    template<typename Resource_type>
    bool resourceIsDeviceLoaded( _ID<Resource_type> id , ResourceManager<Resource_type> & RM) const
    {
        return RM.template has<tag_resource_device_fully_loaded>(id);
    }
    template<typename Resource_type>
    bool resourceIsDeviceLoaded( entity_type e , ResourceManager<Resource_type> & RM) const
    {
        return RM.registry().template has<tag_resource_device_fully_loaded>(e);
    }

    template<typename Resource_type>
    Resource_type & getManagedResource(typename ResourceManager< Resource_type >::id_type id)
    {
        return m_systemBus->getManagedResource<Resource_type>(id);
    }
    template<typename Resource_type>
    ResourceManager< Resource_type > & getResourceManager()
    {
        return m_systemBus->getResourceManager<Resource_type>();
    }
    template<typename Resource_type>
    ResourceManager< Resource_type > const & getResourceManager() const
    {
        return m_systemBus->getResourceManager<Resource_type>();
    }

    virtual void onBuildComponents(entity_type e, nlohmann::json const &components)
    {
        (void)e;
        (void)components;
    }
    virtual void onSaveComponents(entity_type e,  nlohmann::json &components)
    {
        (void)e;
        (void)components;
    }

    virtual std::string name() const
    {
        return "UnnamedSystem";
    }

    auto getSystemBus()
    {
        return m_systemBus;
    }

    double DeltaTime() const
    {
        return m_deltaTime;
    }
    template<typename Event_type>
    auto sink()
    {
        return m_systemBus->dispatcher.sink<Event_type>();
    }

    template<class F, class... Args>
    auto pushTaskPool( F&&f, Args&&... args)
    {
        return m_systemBus->pushTaskPool(  std::forward<F>(f), std::forward<Args>(args)...);
    }
    size_t getTaskPoolSize() const
    {
        return m_systemBus->getTaskPoolSize();
    }
    //gnl::thread_pool& getThreadPool()
    //{
    //    return m_systemBus->getThreadPool();
    //}
    auto const & getInput() const
    {
        return m_systemBus->variables.INPUT;
    }

    json & getJsonInfo()
    {
        return m_json_info;
    }

    template<typename T>
    void reportStat(const std::string &s, T value)
    {
        m_json_info[s] = value;
    }
    json m_json_info;

    void start()
    {
        if( !m_started)
        {
            m_started = true;
            this->onStart();
        }
    }
private:
    std::shared_ptr<SystemBus> m_systemBus;
    entt::registry * m_registry = nullptr;
    double m_deltaTime;
    bool m_started=false;
    friend class SystemBus;
};

}
}

#endif
