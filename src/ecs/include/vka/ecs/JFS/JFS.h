#ifndef VKA_ECS_JFS_H
#define VKA_ECS_JFS_H

#include <nlohmann/json.hpp>
#include <gnl/socket_shell.h>
#include <queue>

namespace vka
{
namespace ecs
{

// JSON File System

class [[ deprecated ]] JFS
{
public:
    using json_type = nlohmann::json;

    enum class JFSError
    {
        SUCCESS=0,
        INVALID_PERMISSIONS,
        DOES_NOT_EXIST,
        NOT_DIRECTORY
    };

    class Shell
    {
    public:
        /**
         * @brief pwd
         * @return
         *
         * returns the current working directory
         */
        std::string pwd() const
        {
            return m_pwd->at("path").get<std::string>();
        }

        /**
         * @brief cd
         * @param path
         * @return
         *
         * CD into a path
         */
        JFSError cd(std::string path)
        {
            auto p = m_fs->cd2(m_pwd, path);
            if( p )
            {
                m_pwd = p;
                return JFSError::SUCCESS;
            }
            return JFSError::DOES_NOT_EXIST;
        }

        /**
         * @brief mkdir
         * @param path
         * @param recurse
         * @return
         *
         * Make a directory and all its parents (if recurse=true)
         */
        JFSError mkdir(std::string path)
        {
            m_fs->mkdir2(m_pwd, path);
            return JFSError::SUCCESS;
        }


        json_type data(std::string path) const
        {
            auto c = m_fs->cn(m_pwd,path);
            if(c)
                return c->at("data");
            return {};
        }

        std::string realpath( std::string path )
        {
            auto x = m_fs->cn(m_pwd,path);
            if( x )
                return x->at("path").get<std::string>();
            return "";
        }

        std::string file( std::string path )
        {
            auto x = m_fs->cn(m_pwd,path);
            if( x )
                return x->at("type").get<std::string>();
            return "";
        }

        std::vector<std::string> ls(std::string path) const
        {
            auto x = m_fs->cd2(m_pwd, path);
            std::vector<std::string> lsout;
            if( x )
            {
                if( x->at("type") == "directory")
                {
                    auto & data = x->at("data");
                    for(auto it=data.begin(); it!=data.end(); it++ )
                    {
                        lsout.push_back( it.key() );
                    }
                }
            }
            return lsout;
        }
        protected:
            JFS * m_fs;
            nlohmann::json * m_pwd;
            size_t m_uid=1;
            friend class JFS;
    };

    JFS()
    {
        m_filesystem["type"] = "directory";
        m_filesystem["path"] = "/";
        m_filesystem["data"] = nlohmann::json::object();
    }

    nlohmann::json const & get() const
    {
        return m_filesystem;
    }


    Shell getShell()
    {
        Shell s;
        s.m_fs = this;
        s.m_pwd = &m_filesystem;
        return s;
    }

    // gets the data object
    nlohmann::json & data(nlohmann::json * root)
    {
        if(!root) root = &m_filesystem;
        return root->at("data");
    }

    void setData(std::string absPath, nlohmann::json const & J)
    {
        auto x = getNode(absPath);
        x->at("data") = J;
    }

    std::string realpath(std::string absPath) const
    {
        if( absPath.front() != '/')
            return nullptr;

        auto dirs = splitString(absPath, "/");
        dirs.pop();

        std::string _realpath;
        auto p = &m_filesystem;
        while(dirs.size())
        {
            auto dir = dirs.front();
            p = getNode(dirs.front(), p);
            dirs.pop();

            if( p==nullptr)
                return _realpath;

            if( p->at("type")=="link")
            {
                auto spath = p->at("data").get<std::string>();
                if( spath.front() == '/')
                {
                    _realpath = spath;
                    p = getNode(_realpath);
                    continue;
                }
                else
                {
                    _realpath += "/" + spath;
                    _realpath = _realPath(_realpath);
                    p = getNode(_realpath );
                    continue;
                }
            }
            else
            {
                _realpath += "/" + dir;
            }

        }
        return _realpath;
    }

    /**
     * @brief getNode
     * @param absPath
     * @return
     *
     * Returns the node from the absolute path. Returns nullptr
     * if the node does not exist.
     * Absolute path must start with /
     */
    nlohmann::json* getNode(std::string absPath)
    {
        if( absPath.front() != '/')
            return nullptr;

        auto dirs = splitString(absPath, "/");
        dirs.pop();

        auto p = &m_filesystem;
        while(dirs.size())
        {
            p = getNode(dirs.front(), p);
            if( p==nullptr) return p;
            dirs.pop();
        }
        return p;
    }
    nlohmann::json const* getNode(std::string absPath) const
    {
        if( absPath.front() != '/')
            return nullptr;

        auto dirs = splitString(absPath, "/");
        dirs.pop();

        auto p = &m_filesystem;
        while(dirs.size())
        {
            p = getNode(dirs.front(), p);
            if( p==nullptr) return p;
            dirs.pop();
        }
        return p;
    }

//    void ln(std::string absTarget, std::string absLink, bool recursive, size_t owner=0)
//    {
//        mknode(absLink, "link", recursive, owner);
//        setData(absLink, absTarget);
//    }

//    std::pair<nlohmann::json*, JFSError>
//     mknode( std::string absPath, std::string nodetype, bool recursive, size_t owner=0)
//    {
//        auto q = splitString(absPath, "/");

//        q.pop();
//        auto * p = &m_filesystem;


//        while( q.size() )
//        {
//            if( p->at("data").count(q.front() ) == 0)
//            {
//                if( recursive)
//                {
//                    (*p)["data"][q.front()]["data"] = nlohmann::json::object();
//                    (*p)["data"][q.front()]["type"] = "directory";
//                    (*p)["data"][q.front()]["owner"] = owner;
//                    auto newPath = p->at("path").get<std::string>() + "/" + q.front();
//                    newPath.erase( std::unique( newPath.begin(), newPath.end(), [](auto a, auto b){return a=='/' && a==b;}), newPath.end());
//                    (*p)["data"][q.front()]["path"] = newPath;
//                }
//                else
//                {
//                    // does not exist
//                    return {nullptr, JFSError::DOES_NOT_EXIST};
//                }
//            }

//            p = &p->at("data").at(q.front());
//            if( p->at("owner").get<size_t>() != owner)
//            {
//                // permission error
//                return {nullptr, JFSError::INVALID_PERMISSIONS};
//            }

//            if( q.size() == 1)
//            {
//                (*p)["type"] = nodetype;
//                if( nodetype == "directory") (*p)["data"] = nlohmann::json::object();
//            }
//            q.pop();
//        }
//        return {p, JFSError::SUCCESS};
//    }
    /**
     * @brief mknode
     * @param nodeName
     * @param parentNode
     * @return
     *
     * makes a single node. You shouldn't use call this.
     *
     * Returns nullptr if the node already exists.
     */
    nlohmann::json* mknode( std::string nodeName, std::string nodetype, nlohmann::json * parentNode)
    {
        if(parentNode==nullptr)
            parentNode = &m_filesystem;

        if( parentNode->count("data") == 1)
        {
            if( parentNode->at("type") == "directory")
            {
                if( parentNode->at("data").count(nodeName) == 0)
                {
                    auto & x = parentNode->at("data")[nodeName];
                    x["type"] = nodetype;
                    auto newPath = parentNode->at("path").get<std::string>() + "/" + nodeName;
                    newPath.erase( std::unique( newPath.begin(), newPath.end(), [](auto a, auto b){return a=='/' && a==b;}), newPath.end());

                    x["path"] = newPath;
                    if( nodetype == "directory")
                        x["data"] = nlohmann::json::object();
                    return &x;
                }
            }
        }
        return nullptr;
    }

    nlohmann::json* mknode( nlohmann::json * parentNode, std::string nodeName, std::string nodetype )
    {

        if( parentNode->count("data") == 1)
        {
            if( parentNode->at("type") == "directory")
            {
                if( parentNode->at("data").count(nodeName) == 0)
                {
                    auto & x = parentNode->at("data")[nodeName];
                    x["type"] = nodetype;
                    auto newPath = parentNode->at("path").get<std::string>() + "/" + nodeName;
                    newPath.erase( std::unique( newPath.begin(), newPath.end(), [](auto a, auto b){return a=='/' && a==b;}), newPath.end());

                    x["path"] = newPath;
                    if( nodetype == "directory")
                        x["data"] = nlohmann::json::object();
                    return &x;
                }
            }
        }
        return nullptr;
    }

    nlohmann::json* getNode(std::string relPath, nlohmann::json * parentNode)
    {
        if(relPath.size()==0) return parentNode;

        if(parentNode==nullptr) parentNode = &m_filesystem;
        relPath.erase( std::unique( relPath.begin(), relPath.end(), [](auto x, auto y){return x=='/' && x==y;}),
        relPath.end());

        if( relPath.front() == '/')
            relPath = std::string( relPath.begin()+1, relPath.end());

        auto i = std::find_if(relPath.begin(), relPath.end(), [](auto x) { return x=='/';});
        if( i==relPath.end() )
        {
            if( parentNode->at("data").count(relPath) == 0)
                return nullptr;
            return &parentNode->at("data").at(relPath);
        }

        auto nodeName = std::string( relPath.begin(), i);

        return getNode( std::string(i+1,relPath.end()), &parentNode->at("data").at(nodeName));
    }

    nlohmann::json* follow(nlohmann::json * parentNode)
    {
        if( parentNode == nullptr) return nullptr;
        if( parentNode->at("type") == "link")
        {
            auto linkto = parentNode->at("data").get<std::string>();
            return follow(cd2(parentNode, linkto));
        }
        else
        {
            return parentNode;
        }

    }


    static std::pair< std::string, std::string> _splitPath(std::string relPath)
    {
        auto i = std::find(relPath.begin(), relPath.end(), '/');
        std::string nextPath;

        if( i != relPath.end() )
            nextPath = std::string( i+1, relPath.end() );

        return {std::string(relPath.begin(),i), nextPath};
    }

    std::string file(nlohmann::json* parentNode) const
    {
        return parentNode->at("type").get<std::string>();
    }

    std::vector<std::string> ls(nlohmann::json * parentNode, std::string relPath)
    {
        auto x = cd2(parentNode, relPath);
        std::vector<std::string> lsout;
        if( x )
        {
            if( x->at("type") == "directory")
            {
                auto & data = x->at("data");
                for(auto it=data.begin(); it!=data.end(); it++ )
                {
                    lsout.push_back( it.key() );
                }
            }
        }
        return lsout;
    }

    nlohmann::json* cd2(nlohmann::json * parentNode, std::string relPath)
    {
        if( parentNode == nullptr && relPath.size())
        {
            if( relPath.front() == '/')
            {
                auto x = _splitPath(relPath);
                return cd2(&m_filesystem, x.second);
            }
            else
            {
                parentNode = &m_filesystem;
            }
        }

        if(!parentNode) return nullptr;

        assert( parentNode->at("type") == "directory");

        if( parentNode && relPath == "" )
        {
            return parentNode;
        }

        if( relPath== "/")
        {
            return &m_filesystem;
        }

        if( relPath.front() == '/')
        {
            return cd2(&m_filesystem, relPath.substr(1));
        }

        auto x = _splitPath(relPath);

        auto & data = parentNode->at("data");

        if( x.first == "..")
        {
            return cd2( parent(parentNode), x.second);
        }
        if( x.first == ".")
        {
            return cd2( parentNode, x.second);
        }
        if( data.count(x.first))
        {
            auto ch = &data.at(x.first);
            while( ch->at("type") == "link")
            {
                ch = cd2(parentNode, ch->at("data").get<std::string>());
            }
            if( ch->at("type") == "directory")
            {
                return cd2(ch, x.second);
            }
        }

        return nullptr;
    }

    // change node, like CD but will return any node
    nlohmann::json* cn(nlohmann::json * parentNode, std::string relPath)
    {
        if( parentNode == nullptr && relPath.size())
        {
            if( relPath.front() == '/')
            {
                auto x = _splitPath(relPath);
                return cn(&m_filesystem, x.second);
            }
            else
            {
                parentNode = &m_filesystem;
            }
        }

        if(!parentNode) return nullptr;

        assert( parentNode->at("type") == "directory");

        if( parentNode && relPath == "" )
        {
            return parentNode;
        }

        if( relPath== "/")
        {
            return &m_filesystem;
        }

        if( relPath.front() == '/')
        {
            return cn(&m_filesystem, relPath.substr(1));
        }

        auto x = _splitPath(relPath);

        auto & data = parentNode->at("data");

        if( x.first == "..")
        {
            return cn( parent(parentNode), x.second);
        }
        if( x.first == ".")
        {
            return cn( parentNode, x.second);
        }
        if( data.count(x.first))
        {
            auto ch = &data.at(x.first);
            while( ch->at("type") == "link")
            {
                ch = cn(parentNode, ch->at("data").get<std::string>());
            }
            if( ch->at("type") == "directory")
            {
                return cn(ch, x.second);
            }
            return ch;
        }

        return nullptr;
    }

    nlohmann::json* parent(nlohmann::json * node)
    {
        std::string p = node->at("path");
        if( p == "/")
            return node;

        auto i = p.find_last_of('/');
        auto parentP =  p.substr(0,i);
        return cd2(nullptr, parentP);
    }

    nlohmann::json* mkln2(nlohmann::json * parentNode, std::string linkName, std::string targetPath)
    {
        auto l = mknode2_p(parentNode, linkName, "link");
        if( l )
            l->at("data") = targetPath;
        return l;
        if( parentNode==nullptr)
            parentNode=&m_filesystem;

        if( parentNode->at("type") == "directory")
        {
            auto d = mknode2(parentNode, linkName);
            if(!d) return nullptr;

            (*d)["type"] = "link";
            (*d)["data"] = targetPath;

            return d;
        }
        return nullptr;
    }


    /**
     * @brief mkdir2
     * @param parentNode
     * @param dirName
     * @return
     *
     * Creates a directory in the parent direcotry. returns nullptr
     * if the parent is not a dir or the dirName already exists
     */
    nlohmann::json* mkdir2(nlohmann::json * parentNode, std::string dirName)
    {
        if( parentNode==nullptr)
            parentNode=&m_filesystem;

        if( parentNode->at("type") == "directory")
        {
            auto d = mknode2(parentNode, dirName);
            if(!d) return nullptr;

            (*d)["type"] = "directory";
            (*d)["data"] = nlohmann::json::object();

            return d;
        }
        return nullptr;
    }

    bool exists(nlohmann::json * parentNode, std::string nodeName) const
    {
        auto f = parentNode->at("data").find(nodeName);
        if( f == parentNode->at("data").end())
        {
            return false;
        }
        return true;
    }
    nlohmann::json* mkdir2_p(nlohmann::json * parentNode, std::string relPath)
    {
        if( parentNode==nullptr)
            parentNode=&m_filesystem;

        if( relPath.front() =='/')
        {
            return mkdir2_p( parentNode, relPath.substr(1));
        }
        else
        {
            auto i = std::find(relPath.begin(), relPath.end(), '/');
            if( i == relPath.end())
            {
                return mkdir2(parentNode, relPath);
            }
            else
            {
                auto dirName = std::string(relPath.begin(), i);
                auto nextPath= std::string(i+1, relPath.end());

                auto nextNode = !exists(parentNode, dirName) ? mkdir2(parentNode,dirName) : cd2(parentNode,dirName);
                return mkdir2_p(nextNode, nextPath);
            }
        }

        return nullptr;
    }

    nlohmann::json* mknode2_p(nlohmann::json * parentNode, std::string relPath, std::string type)
    {
        auto x = mkdir2_p(parentNode, relPath);
        if(x)
            x->at("type") = type;
        return x;
    }
    /**
     * @brief mknode2
     * @param parentNode
     * @param nodeName
     * @return
     *
     * Create an empty node. Returns nullptr if the node already exists
     */
    nlohmann::json* mknode2(nlohmann::json * parentNode, std::string nodeName)
    {
        if( parentNode == nullptr) parentNode = &m_filesystem;
        auto f = parentNode->at("data").find(nodeName);
        if( f == parentNode->at("data").end())
        {
            auto & data = parentNode->at("data");
            auto nodePath = parentNode->at("path").get<std::string>() + "/" + nodeName;
            nodePath.erase( std::unique( nodePath.begin(), nodePath.end(), [](auto x, auto y){return x=='/' && x==y;}),
            nodePath.end());
            data[nodeName]["path"] = nodePath;
            data[nodeName]["owner"] = 0;
            return &data[nodeName];
        }
        return nullptr;
    }

    nlohmann::json const* getNode(std::string relPath, nlohmann::json const* parentNode) const
    {
        if(relPath.size()==0) return parentNode;

        if(parentNode==nullptr) parentNode = &m_filesystem;
        relPath.erase( std::unique( relPath.begin(), relPath.end(), [](auto x, auto y){return x=='/' && x==y;}),
        relPath.end());

        if( relPath.front() == '/')
            relPath = std::string( relPath.begin()+1, relPath.end());

        auto i = std::find_if(relPath.begin(), relPath.end(), [](auto x) { return x=='/';});
        if( i==relPath.end() )
            return &parentNode->at("data").at(relPath);

        auto nodeName = std::string( relPath.begin(), i);

        return getNode( std::string(i+1,relPath.end()), &parentNode->at("data").at(nodeName));
    }
    static std::string _join(std::string a, std::string b)
    {
        a += "/";
        a += b;
        a.erase( std::unique( a.begin(), a.end(), [](auto x, auto y){return x=='/' && x==y;}),
        a.end());
        return a;
    }
private:
    nlohmann::json m_filesystem;

    static std::queue<std::string> splitString(const std::string& str, const std::string& delims)
    {
        std::queue<std::string> cont;
        std::size_t current, previous = 0;
        current = str.find_first_of(delims);
        while (current != std::string::npos)
        {
            cont.push(str.substr(previous, current - previous));
            previous = current + 1;
            current = str.find_first_of(delims, previous);
        }
        cont.push(str.substr(previous, current - previous));
        return cont;
    }
    static std::string _realPath(std::string path)
    {
        auto q = JFS::splitString(path, "/");
        std::vector<std::string> pw;
        q.pop();
        while(q.size())
        {
            if( q.front() == "..")
            {
                pw.pop_back();
            }
            else if( q.front() == ".")
            {
                // do nothing
            }
            else
            {
                pw.push_back( std::move(q.front()));
            }
            q.pop();
        }
        path = "";

        for(auto & x : pw)
            path += "/" + std::move(x);
        return path;
    }
    friend class Shell;
};

}
}

#endif
