#ifndef VKA_ECS_COMPONENTS_SCHEMAS_BASE_H
#define VKA_ECS_COMPONENTS_SCHEMAS_BASE_H

#include <vka/ecs/ECS.h>
#include <vka/ecs/json.h>
#include <memory>

#include <vka/ecs/SystemBus.h>
#include <variant>
namespace vka
{
namespace ecs
{

/**
 * @brief schema
 * @param C
 * @param s
 * @return
 *
 * Template function to get the schema for a particular
 * type.
 */
template<typename T>
json toSchema(T const & C, std::shared_ptr<SystemBus> s);

/**
 * @brief fromJson
 * @param value
 * @param J
 * @param S
 *
 * Converts a json value to a particular type which may
 * depend on other data within the systembus
 */
template<typename T>
void fromJson(T & value, json const & J, std::shared_ptr<SystemBus> S);




template<typename ...T>
json toSchema(std::variant<T...> const & C, std::shared_ptr<SystemBus> s)
{
    (void)C;
    using variant_type = std::variant<T...>;


    json J =
    std::visit([s](auto&& arg) {
        return toSchema(arg, s);
    }, C);

    J["type"] = "object";

    //constexpr size_t N = 0;
#define VAR_SCH(N)\
    if constexpr ( N < std::variant_size<variant_type>::value )\
    {\
        std::variant_alternative_t<N, variant_type> result;\
        auto j = toSchema(result, s);\
        J["oneOf"].push_back( j );\
    }

    VAR_SCH(0)
    VAR_SCH(1)
    VAR_SCH(2)
    VAR_SCH(3)
    VAR_SCH(4)
    VAR_SCH(5)
    VAR_SCH(6)
    VAR_SCH(7)
    VAR_SCH(8)
    VAR_SCH(9)
#undef VAR_SCH
    return J;
}



template<typename T>
json toSchema(std::vector<T> const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "array";
    for(auto & xx : C)
    {
        J["items"].push_back( toSchema(xx,s) );
    }
    J["additionalItems"].push_back( toSchema( T(), s) );
    return J;
}


template<typename T>
void fromJson(std::vector<T> & value, json const & J, std::shared_ptr<SystemBus> S)
{
    if( J.is_array() )
    {
        value.clear();
        for(auto & j : J)
        {
            T v;
            fromJson(v, j, S);
            value.emplace_back(v);
        }
    }
}

template<typename ...T>
void fromJson(std::variant<T...> & value, json const & J, std::shared_ptr<SystemBus> s)
{
    using variant_type = std::variant<T...>;


    #define VAR_SCH(M)\
    if constexpr ( M < std::variant_size<variant_type>::value )\
    {\
        using val_type = std::variant_alternative_t<M, variant_type>;\
        std::cout << "Checking: " << val_type::_type << " == " << J.at("_type").get<std::string>() << std::endl;\
        if( val_type::_type == J.at("_type").get<std::string>())\
        {\
            val_type v;\
            fromJson(v,J,s);\
            value = std::move(v);\
            return;\
        }\
    }


    VAR_SCH(0)
    VAR_SCH(1)
    VAR_SCH(2)
    VAR_SCH(3)
    VAR_SCH(4)
    VAR_SCH(5)
    VAR_SCH(6)
    VAR_SCH(7)
    VAR_SCH(8)
    VAR_SCH(9)

    #undef VAR_SCH
}



template<>
inline json toSchema<bool>(bool const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "boolean";
    J["default"] = C;
    J["ui:widget"] = "switch";
    return J;
    (void)s;
}

template<>
inline json toSchema<float>(float const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "number";
    J["default"] = C;
    return J;
    (void)s;
}

template<>
inline json toSchema<uint32_t>(uint32_t const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "integer";
    J["minimum"] = 0;
    J["default"] = C;
    return J;
    (void)s;
}

template<>
inline json toSchema<int32_t>(int32_t const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "integer";
    J["default"] = C;
    return J;
    (void)s;
}

template<>
inline json toSchema<std::string>(std::string const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";
    J["default"] = C;
    return J;
    (void)s;
}

template<>
inline json toSchema<glm::vec2>(glm::vec2 const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "object";

    J["properties"]["x"] = toSchema(C.x, s);
    J["properties"]["y"] = toSchema(C.y, s);

    return J;
    (void)s;
}



template<>
inline json toSchema<glm::vec4>(glm::vec4 const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "object";

    J["properties"]["x"] = toSchema(C.x, s);
    J["properties"]["y"] = toSchema(C.y, s);
    J["properties"]["z"] = toSchema(C.z, s);
    J["properties"]["w"] = toSchema(C.w, s);

    return J;
    (void)s;
}

template<>
inline json toSchema<glm::vec3>(glm::vec3 const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "object";

    J["properties"]["x"] = toSchema(C.x, s);
    J["properties"]["y"] = toSchema(C.y, s);
    J["properties"]["z"] = toSchema(C.z, s);

    return J;
    (void)s;
}


template<>
inline void fromJson<float>(float & value, json const & J, std::shared_ptr<SystemBus> S)
{
    value = J.get<float>();
    (void)S;
}
template<>
inline void fromJson<uint32_t>(uint32_t & value, json const & J, std::shared_ptr<SystemBus> S)
{
    value = J.get<uint32_t>();
    (void)S;
}
template<>
inline void fromJson<int32_t>(int32_t & value, json const & J, std::shared_ptr<SystemBus> S)
{
    value = J.get<int32_t>();
    (void)S;
}

template<>
inline void fromJson<glm::vec3>(glm::vec3 & value, json const & J, std::shared_ptr<SystemBus> S)
{
    fromJson(value.x, J.at("x"), S);
    fromJson(value.y, J.at("y"), S);
    fromJson(value.z, J.at("z"), S);
}
template<>
inline void fromJson<glm::vec2>(glm::vec2 & value, json const & J, std::shared_ptr<SystemBus> S)
{
    fromJson(value.x, J.at("x"), S);
    fromJson(value.y, J.at("y"), S);
}


#define COMPONENT_JSON_DESERIALIZE_BEGIN(COMP) template<> inline void fromJson<COMP>(COMP & value, json const & J, std::shared_ptr<SystemBus> S) {
#define COMPONENT_JSON_DESERIALIZE_END (void)S; }
#define COMPONENT_JSON_DESERIALIZE_PROPERTY(prop) fromJson( value.prop, J.at(#prop), S);


#define COMPONENT_SCHEMA_BEGIN(COMP) \
template<>\
inline json toSchema<COMP>(COMP const & C, std::shared_ptr<SystemBus> s) \
{\
    (void)s;(void)C;\
    json J;\
    J["type"] = "object";\
    J["title"] = COMP::_type;\
    J["properties"]["_type"] = toSchema( std::string(COMP::_type), s);\
    J["properties"]["_type"]["ui:visible"] = false;\
    J["properties"]["_subType"] = toSchema( std::string(COMP::_subType), s);\
    J["properties"]["_subType"]["ui:visible"] = false;

#define COMPONENT_SCHEMA_PROPERTY(prop) J["properties"][#prop] = toSchema(C.prop, s); J["ui:order"].push_back(#prop);
#define COMPONENT_SCHEMA_END return J;}



#define SCHEMA_RESOURCE_BEGIN(COMP) \
template<>\
inline json toSchema<COMP>(COMP const & C, std::shared_ptr<SystemBus> s) \
{\
    (void)s;(void)C;\
    json J;\
    J["type"] = "object";\
    J["properties"]["_type"] = toSchema( std::string(COMP::_type), s);\
    J["properties"]["_type"]["ui:visible"] = false;\
    J["properties"]["_subType"] = toSchema( std::string(COMP::_subType), s);\
    J["properties"]["_subType"]["ui:visible"] = false;

#define SCHEMA_RESOURCE_PROPERTY(prop) J["properties"][#prop] = toSchema(C.prop, s); J["ui:order"].push_back(#prop);
#define SCHEMA_RESOURCE_END return J;}



}
}
#endif
