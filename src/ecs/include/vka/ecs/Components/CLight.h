#ifndef VKA_ECS_LIGHT_COMPONENT_H
#define VKA_ECS_LIGHT_COMPONENT_H


#include <memory>
#include <vector>
#include <vka/math.h>
#include <vka/math/geometry/aabb.h>
#include <vka/ecs/json.h>
#include <vka/ecs/ECS.h>

namespace vka
{
namespace ecs
{

//=====================================================================
// Set the name of the enum
#define ENUM_DEFINE    LightType

// Set the base type
#define ENUM_BASE_TYPE int32_t

// Set the label/values
#define ENUM_DEFINE_VALUES \
    ENUM_VALUE( DIRECTIONAL ,           0)\
    ENUM_VALUE( POINT,      1)\
    ENUM_VALUE( SPOT,        2)

// include this inline header to generate the relection
// classes/functions
#include "vka/ecs/enum_reflect.inl"
//=====================================================================

struct CLight
{
    static constexpr auto _type="CLight";

    vec3      color = vec3(1,1,1);
    float     intensity = 100.0f;
    LightType type      = LightType::POINT;
    float     range     = 25.0f;
    float     inner_angle_rad = 25.0f * 3.141592653589f/180.0f; // cos(45)
    float     outer_angle_rad = 45.0f * 3.141592653589f/180.0f; // cos(45)


    //==============================================================
    // Required
    //==============================================================
    json to_json() const
    {
        json J;
        J["_type"] = _type;
        J["color"]  = {color.x, color.y, color.z};
        J["intensity"] = intensity;
        J["type"] = to_string(type);
        J["range"] = range;
        J["inner_angle_rad"] = inner_angle_rad;
        J["outer_angle_rad"] = outer_angle_rad;

        return J;
    }
    void from_json(const json & J)
    {
        if( J.count("color"))
        {
            color.x = J.at("color").at(0).get<float>();
            color.y = J.at("color").at(1).get<float>();
            color.z = J.at("color").at(2).get<float>();
        }
        if( J.count("intensity"))
        {
            intensity = J.at("intensity").get<float>();
        }
        if( J.count("range"))
        {
            intensity = J.at("range").get<float>();
        }
        if( J.count("inner_angle_rad"))
        {
            intensity = J.at("inner_angle_rad").get<float>();
        }
        if( J.count("outer_angle_rad"))
        {
            intensity = J.at("outer_angle_rad").get<float>();
        }
        if( J.count("type"))
        {
            type = from_string<LightType>(J.at("type").get<std::string>());
        }
    }
    //==============================================================


    CLight()
    {

    }

    CLight(glm::vec3 col) : color(col)
    {
    }

    /**
     * @brief calculateBoundingBox
     * @return
     *
     * Calculates the bounding box of the light component as if the light was
     * at the origin and is pointing in the +z axis (if its a directional type light)
     */
    AABB calculateBoundingBox() const
    {
        static const auto sqrt2 = std::sqrt(2.0f);
        switch (type)
        {
            case LightType::POINT:
                return AABB( vec3(-range*sqrt2), vec3(range*sqrt2) );
            case LightType::DIRECTIONAL:
                return AABB( vec3(std::numeric_limits<float>::lowest()), vec3( std::numeric_limits<float>::max()) );
            case LightType::SPOT:
            {
                AABB a;
                a.lowerBound = {0,0,0};
                a.upperBound = {
                    range*std::cos(outer_angle_rad),
                    range*std::cos(outer_angle_rad),
                    range,
                };
                return a;
            }
        }
        return AABB();
    }
    AABB  _boundingBox;
};

}
}

#endif
