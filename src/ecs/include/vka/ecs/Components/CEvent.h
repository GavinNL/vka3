#ifndef VKA_ECS_EVENT_COMPONENT_H
#define VKA_ECS_EVENT_COMPONENT_H

#include <vka/ecs/SystemBase.h>
#include <map>
#include <set>

namespace vka
{
namespace ecs
{

enum class EventType
{
    FRAME, // called every frame
    TIMER, // called at a specific time intervals
};

struct EventRef
{
    EntityRef entity;
    size_t    count=0;

    void stop()
    {
        m_exit=true;
    }

    void setIntervalC(std::chrono::nanoseconds interval)
    {
        m_interval = interval;
    }

    void setInterval(double seconds)
    {
        m_interval = std::chrono::nanoseconds( static_cast<size_t>(seconds*1e9) );
    }
protected:
    bool m_exit = false;
    std::chrono::system_clock::time_point m_lastExecuted;
    std::chrono::nanoseconds m_interval = std::chrono::nanoseconds(0);
    friend class EventSystem;
};

class EventSystem;

struct CEvent
{
    using CallbackType = std::function<void(EventRef&)>;

    void addEvent(CallbackType f, std::chrono::nanoseconds interval = std::chrono::nanoseconds(0) );
    void addEvent(EventType type, CallbackType f, std::chrono::nanoseconds interval = std::chrono::nanoseconds(0) );

    std::vector< std::pair<EventRef,CallbackType> > callbacks;

    CEvent()
    {
        callbacks.reserve(10);
    }
protected:
    //using pair_type = std::pair<EventType, CallbackType>;
    //entt::entity             m_entity;
    //EventSystem * m_parent=nullptr;
    //std::set<EventType> m_registeredEvents;
    friend class EventSystem;
};



}
}

#endif
