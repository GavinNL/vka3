#ifndef VKA_ECS_ANIMATOR_COMPONENT_2_H
#define VKA_ECS_ANIMATOR_COMPONENT_2_H

#include <vka/utils/Scene.h>

#include <vka/math/transform.h>
#include <vka/math/geometry/frustum.h>
#include <vka/math/geometry.h>

#include <vka/utils/uri.h>

#include <vka/ecs/ResourceObjects/GltfScene.h>

#include <memory>
#include <vector>
#include <variant>
#include <queue>
#include <chrono>

namespace vka
{
namespace ecs
{

struct CAnimator2;
struct SystemBus;
class  AnimatorSystem2;

struct AnimatorNode
{
    std::string                label;
    std::vector<uint32_t>      inputNodeIndex;


    std::chrono::system_clock::time_point lastTime;

    float _length=0;
    vka::SceneBase::Animation::KeyFrameState m_currentKeyFrame;

    vka::SceneBase::Animation::KeyFrameState const & get() const
    {
        return m_currentKeyFrame;
    }

    virtual ~AnimatorNode()
    {

    }

    virtual vka::SceneBase::Animation::KeyFrameState getKeyFrameState(CAnimator2 & C, float s) = 0;

    /**
     * @brief getLength
     * @return
     *
     * Returns the length of the Animation. This value must be
     * a constant and should not change depending on parameters
     */
    virtual float getLength() const
    {
        return _length;
    }

    vka::SceneBase::Animation::KeyFrameState getInput(uint32_t index);

    std::shared_ptr<vka::ecs::SystemBus> m_systemBus;
    entt::entity                         m_entity;
};



//============================================================
// AnimatorNodeInput
//============================================================
struct AnimatorNodeInput : public AnimatorNode
{
    AnimatorNodeInput(){}

    AnimatorNodeInput(uint32_t index) : animationIndex(index)
    {
    }

    AnimatorNodeInput& setAnimationIndex(uint32_t i)
    {
        animationIndex = i;
        return *this;
    }

    vka::SceneBase::Animation::KeyFrameState getKeyFrameState(CAnimator2 & C, float s) override;

public:
    uint32_t    animationIndex    = std::numeric_limits<uint32_t>::max();
    bool        disableRootMotion = true;
    bool        reverse           = false;
    entt::entity entity;
};

//============================================================
// AnimatorNodeBlend
//============================================================
struct AnimatorNodeBlend : public AnimatorNode
{
    AnimatorNodeBlend(){}


    vka::SceneBase::Animation::KeyFrameState getKeyFrameState(CAnimator2 & C, float s) override;

    float blendValue = 1.0f;
};
//============================================================

using AnimatorNodeV = std::variant< AnimatorNodeInput, AnimatorNodeBlend >;


struct CAnimator2
{
    std::vector<AnimatorNodeV>      animationNode;
    uint32_t                         rootNode;

    bool _isCompiled=false;

    template<typename T>
    uint32_t newNode()
    {
        static_assert( std::is_base_of<AnimatorNode, T>::value, "");
        animationNode.emplace_back( T() );
        auto & b = std::get<T>(animationNode.back());
        b.m_entity=m_entity;

        return static_cast<uint32_t>( animationNode.size()-1 );
    }

    template<typename T>
    T& node(uint32_t i)
    {
        if constexpr ( std::is_same<T, AnimatorNode>::value )
        {
            AnimatorNode * x = nullptr;
            std::visit( [&](auto && args)
            {
                x = &args;
            },
            animationNode.at(i) );
            return *x;
        }
        else
        {
            return std::get<T>(animationNode.at(i));
        }
    }

    vka::SceneBase::Animation::KeyFrameState getOutput(float s)
    {
        return node<AnimatorNode>(rootNode).getKeyFrameState(*this, s);
    }

//private:
    friend class  AnimatorSystem;
    std::shared_ptr<vka::ecs::SystemBus> m_systemBus;
    float _length   = 0;
    float       t   = 0;
    float playSpeed = 1.0f;
    entt::entity m_entity = entt::null;
};





}
}

#endif
