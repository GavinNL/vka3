#ifndef VKA_ECS_COMPONENT_TAGS_H
#define VKA_ECS_COMPONENT_TAGS_H

#include <vka/ecs/ECS.h>


namespace vka
{
namespace ecs {

using VisibleTag       = entt::tag<"VisibleTag"_hs>;
using StaticObjectTag  = entt::tag<"StaticObject"_hs>;
using DynamicObjectTag = entt::tag<"DynamicObject"_hs>;
using UpdateBoundingBoxTag = entt::tag<"UpdateBoundingBox"_hs>;


// the local transform has been  updated
using LocalTransformUpdatedTag      = entt::tag<"LocalTransformUpdated"_hs>;

using tag_animation_nodes_updated   = entt::tag<"tag_animation_nodes_updated"_hs>;

// this tag indicates that the transform
// of the entity has been updated, which means
// we need to make sure that we update the world
// matrix
using tag_transform_update   = entt::tag<"tag_transform_update"_hs>;

using tag_global_transform_update   = entt::tag<"tag_global_transform_update"_hs>;


using tag_render_in_frustum = entt::tag<"tag_render_in_frustum"_hs>;

}
}

#endif
