#ifndef VKA_ECS_POSE_COMPONENT
#define VKA_ECS_POSE_COMPONENT

#include <vka/math/transform.h>
#include <vector>

namespace vka
{
namespace ecs
{

/**
 * @brief The CPose struct
 *
 * The pose Component holds the local node-space transforamations
 * of all the entities in the GLTF Scene.
 *
 * This includes all the node-space bone transforms. They will have
 * to be copied to the CSkeleton component
 *
 */
struct CPose
{
    static constexpr auto _type = "CPose";

    CPose()
    {
    }

    std::vector<vka::Transform> nodeSpaceTransforms; // node space transforms of the childnodes


protected:
    friend struct SceneSystem;
    friend struct AnimatorSystem2;

};

// this will be attached to an entity that
// has a CMesh. These contain the bone matrices
// that will be sent to the shader.
struct CSkeleton
{
    std::vector<glm::mat4>             boneMatrices;
};

}
}

#endif
