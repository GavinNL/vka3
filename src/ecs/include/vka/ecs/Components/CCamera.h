#ifndef VKA_ECS_CAMERA_COMPONENT_H
#define VKA_ECS_CAMERA_COMPONENT_H

#include <vka/math/transform.h>
#include <vka/math/geometry/frustum.h>
#include <vka/math/geometry.h>
#include <vka/utils/uri.h>

#include <memory>
#include <vector>


#include <vka/ecs/json.h>
#include <vka/ecs/ResourceObjects/Environment.h>

namespace vka
{
namespace ecs
{
struct CCamera
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CCamera";


    float          fieldOfViewDegrees = 45.0f;
    float          nearPlane= 0.1f;
    float          farPlane = 100.0f;


    vec2           viewPortSizeNormalized   = {1.0f,1.0f};
    vec2           viewPortOffsetNormalized = {0,0};

    _ID<Environment> environment;

    CCamera()
    {
    }

    glm::mat4 getProjectionMatrix(glm::vec2 screenSize, bool invertY=true) const
    {
        glm::mat4 projMatrix = glm::mat4(1.0f);
        float AR = float(screenSize.x) / float(screenSize.y);
        projMatrix = glm::perspective( glm::radians(fieldOfViewDegrees), AR, nearPlane, farPlane);
        if(invertY)
            projMatrix[1][1] *= -1;
        return projMatrix;
    }

    vka::frustum getFrustum(vka::Transform const & worldspaceTransform, glm::vec2 screenSize) const
    {
        auto f = vka::frustum( getProjectionMatrix(screenSize) );
        f.transform(worldspaceTransform.getMatrix());
        return f;
    }
    /**
     * @brief projectMouseRay
     * @param mousePosition
     * @param cameraViewMatrix
     * @param screenSize
     * @return
     *
     * Project
     */
    line3 projectMouseRay( glm::vec2 mousePosition,
                           vka::Transform const & cameraWorldSpaceTransform,
                           glm::vec2 screenSize) const
    {
        auto proj             = getProjectionMatrix(screenSize);
        auto cameraViewMatrix = cameraWorldSpaceTransform.getViewMatrix();

        auto ray1 =
        glm::unProject( glm::vec3(mousePosition.x,mousePosition.y,1),
                        cameraViewMatrix,
                        proj,
                        glm::vec4{0,0,screenSize.x, screenSize.y});

//        auto ray2 =
//        glm::unProject( glm::vec3(mousePosition.x,mousePosition.y,-1),
//                        cameraViewMatrix,
//                        projMatrix,
//                        glm::vec4{0,0,screenSize.x, screenSize.y});

        vec3 MOUSE_NEAR = cameraWorldSpaceTransform.position;
        vec3 MOUSE_FAR  = 2.0f*MOUSE_NEAR - glm::vec3(ray1.x, ray1.y, ray1.z);

        return line3( point3(MOUSE_NEAR), point3(MOUSE_FAR));
    }

    //==============================================================
    // Required
    //==============================================================
    json to_json() const
    {
        json J;
        J["fieldOfViewDegrees"] = fieldOfViewDegrees;
        J["nearPlane"] = nearPlane;
        J["farPlane"] = farPlane;

        return J;
    }
    void from_json(const json & J)
    {
        if( J.count("fieldOfViewDegrees"))
        {
            fieldOfViewDegrees = J.at("fieldOfViewDegrees").get<float>();
        }
        if( J.count("nearPlane"))
        {
            nearPlane = J.at("nearPlane").get<float>();
        }
        if( J.count("farPlane"))
        {
            farPlane = J.at("farPlane").get<float>();
        }
    }
    //==============================================================
};

}
}

#endif
