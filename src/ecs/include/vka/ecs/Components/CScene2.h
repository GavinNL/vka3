#ifndef VKA_ECS_RENDERER_SceneComponent22_H
#define VKA_ECS_RENDERER_SceneComponent22_H

#include <vka/ecs/ECS.h>
#include <vka/ecs/ID.h>
#include <vka/ecs/ResourceObjects/GltfScene.h>

namespace vka
{
namespace ecs
{

struct CScene
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CScene";

    CScene()
    {
    }

    CScene( Scene2_ID id) : sceneId(id)
    {
    }
    Scene2_ID      sceneId;
    uint32_t       sceneIndex=0;

    std::vector<entt::entity>               nodes;
};

}
}

#endif
