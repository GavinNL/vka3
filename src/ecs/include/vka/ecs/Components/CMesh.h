#ifndef VKA_ECS_RENDERER_MATERIALPRIMITIVECOMPONENT
#define VKA_ECS_RENDERER_MATERIALPRIMITIVECOMPONENT

#include <vka/utils/uri.h>
#include <vka/math/transform.h>

#include <vka/ecs/ID.h>
#include <vka/utils/HostImage.h>
#include <vka/ecs/ResourceObjects/HostPrimitive.h>
#include <vka/utils/HostCubeImage.h>
#include <vka/utils/hash.h>

#include <vka/ecs/ResourceObjects/PBRMaterial.h>
#include<vector>

namespace vka
{
namespace ecs
{
struct DrawReq
{
    static constexpr auto _type = "data";
    static constexpr auto _subType = "DrawReq";

    vk::PrimitiveTopology topology    = vk::PrimitiveTopology::eTriangleList;
    vk::PolygonMode       polygonMode = vk::PolygonMode::eFill;
    vk::CullModeFlagBits  cullMode    = vk::CullModeFlagBits::eBack;
    bool                  wireframe   = false;

    size_t hash() const
    {
        std::hash<size_t> H;
        size_t seed = wireframe;
        seed = hashCombine( seed, H( static_cast<size_t>(topology)) );
        seed = hashCombine( seed, H( static_cast<size_t>(polygonMode)) );
        seed = hashCombine( seed, H( static_cast<size_t>(cullMode)) );
        return seed;
    }
};

struct CPrimitive
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CPrimitive";

    MeshPrimitive_ID      mesh;
    PBRMaterial_ID        material;
    uint32_t              subMeshIndex=0;
    DrawReq               drawReq;

    CPrimitive()
    {
        m_drawReqHash = drawReq.hash();
    }
    CPrimitive(MeshPrimitive_ID mp,
               PBRMaterial_ID m,
               vk::PrimitiveTopology _t = vk::PrimitiveTopology::eTriangleList,
               vk::PolygonMode _m = vk::PolygonMode::eFill,
               vk::CullModeFlagBits _c = vk::CullModeFlagBits::eBack) : mesh(mp), material(m), subMeshIndex(0u)
    {
        drawReq.topology = _t;
        drawReq.polygonMode = _m;
        drawReq.cullMode = _c;
        m_drawReqHash = drawReq.hash();

    }
    size_t m_drawReqHash;
};


struct CMesh
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CMesh";

    std::vector<CPrimitive> primitives;
    bool visible=true;

    CMesh(){}

    CMesh(MeshPrimitive_ID id,
          PBRMaterial_ID mat_id,
          vk::PrimitiveTopology top=vk::PrimitiveTopology::eTriangleList,
          vk::PolygonMode _m = vk::PolygonMode::eFill,
          vk::CullModeFlagBits _c = vk::CullModeFlagBits::eBack)
    {
        addMesh(id, mat_id, top, _m, _c);
    }
    CMesh& addMesh(MeshPrimitive_ID id,
                   PBRMaterial_ID mat_id,
                   vk::PrimitiveTopology top=vk::PrimitiveTopology::eTriangleList,
                   vk::PolygonMode _m = vk::PolygonMode::eFill,
                   vk::CullModeFlagBits _c = vk::CullModeFlagBits::eBack)
    {
        primitives.push_back( CPrimitive(id, mat_id, top, _m, _c) );
        return *this;
    }
};

}
}

#endif
