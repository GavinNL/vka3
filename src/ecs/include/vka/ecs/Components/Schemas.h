#ifndef VKA_ECS_COMPONENTS_SCHEMAS_H
#define VKA_ECS_COMPONENTS_SCHEMAS_H

#include <vka/ecs/ResourceObjects/Schemas.h>

#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CCamera.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CCollider.h>
#include <vka/ecs/Components/CRigidBody.h>

#include <vka/ecs/Components/CWSADMovement.h>
#include <vka/ecs/Components/CMouseLook.h>

namespace vka
{
namespace ecs
{

template<>
inline json toSchema<vk::PrimitiveTopology>(vk::PrimitiveTopology const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";

    J["enum"].push_back( to_string(vk::PrimitiveTopology::eTriangleList) );
    J["enum"].push_back( to_string(vk::PrimitiveTopology::eLineList) );

    J["default"] = vk::to_string(C);
    return J;
    (void)s;
}

template<>
inline void fromJson<vk::PrimitiveTopology>(vk::PrimitiveTopology & value, json const & J, std::shared_ptr<SystemBus> S)
{
    auto s = J.get<std::string>();
    if( s == to_string(vk::PrimitiveTopology::eTriangleList))
    {
        value = vk::PrimitiveTopology::eTriangleList;
        return;
    }
    if( s == to_string(vk::PrimitiveTopology::eLineList))
    {
        value = vk::PrimitiveTopology::eLineList;
        return;
    }
    (void)S;
}

template<>
inline json toSchema<vk::CullModeFlagBits>(vk::CullModeFlagBits const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";

    J["enum"].push_back( to_string(vk::CullModeFlagBits::eBack) );
    J["enum"].push_back( to_string(vk::CullModeFlagBits::eFront) );
    J["enum"].push_back( to_string(vk::CullModeFlagBits::eFrontAndBack) );

    J["default"] = vk::to_string(C);
    return J;
    (void)s;
}

template<>
inline void fromJson<vk::CullModeFlagBits>(vk::CullModeFlagBits & value, json const & J, std::shared_ptr<SystemBus> S)
{
    auto s = J.get<std::string>();
    if( s == to_string(vk::CullModeFlagBits::eBack))
    {
        value = vk::CullModeFlagBits::eBack;
        return;
    }
    if( s == to_string(vk::CullModeFlagBits::eFront))
    {
        value = vk::CullModeFlagBits::eFront;
        return;
    }
    if( s == to_string(vk::CullModeFlagBits::eFrontAndBack))
    {
        value = vk::CullModeFlagBits::eFrontAndBack;
        return;
    }
    (void)S;
}


template<>
inline json toSchema<vk::PolygonMode>(vk::PolygonMode const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";

    J["enum"].push_back( to_string(vk::PolygonMode::eFill) );
    J["enum"].push_back( to_string(vk::PolygonMode::eLine) );
    J["enum"].push_back( to_string(vk::PolygonMode::ePoint) );

    J["default"] = vk::to_string(C);
    return J;
    (void)s;
}

template<>
inline void fromJson<vk::PolygonMode>(vk::PolygonMode & value, json const & J, std::shared_ptr<SystemBus> S)
{
    auto s = J.get<std::string>();
    if( s == to_string(vk::PolygonMode::eFill))
    {
        value = vk::PolygonMode::eFill;
        return;
    }
    if( s == to_string(vk::PolygonMode::eLine))
    {
        value = vk::PolygonMode::eLine;
        return;
    }
    if( s == to_string(vk::PolygonMode::ePoint))
    {
        value = vk::PolygonMode::ePoint;
        return;
    }
    (void)S;
}


//===============================================================================
// Converting from JSON -> Component
//===============================================================================
COMPONENT_JSON_DESERIALIZE_BEGIN(CRotation)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(x)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(y)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(z)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(w)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(CPosition)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(x)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(y)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(z)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(CScale)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(x)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(y)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(z)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(DrawReq)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(polygonMode)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(wireframe)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(topology)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(cullMode)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(CPrimitive)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(mesh)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(material)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(subMeshIndex)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(CMesh)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(primitives)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(CScene)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(sceneId)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(sceneIndex)
COMPONENT_JSON_DESERIALIZE_END


COMPONENT_JSON_DESERIALIZE_BEGIN(CCamera)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(fieldOfViewDegrees)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(nearPlane)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(farPlane)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(environment)
COMPONENT_JSON_DESERIALIZE_END


COMPONENT_JSON_DESERIALIZE_BEGIN(CMouseLook)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(sensitivity)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(easing)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(CWSADMovement)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(maxSpeed)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(slowSpeed)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(easing)
COMPONENT_JSON_DESERIALIZE_END


COMPONENT_JSON_DESERIALIZE_BEGIN(CRigidBody)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(mass)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(drag)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(angularDrag)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(isKinematic)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(angularFactor)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(linearFactor)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(disableDeactivation)
COMPONENT_JSON_DESERIALIZE_END


COMPONENT_JSON_DESERIALIZE_BEGIN(BoxCollider)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(x)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(y)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(z)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(SphereCollider)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(radius)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(CapsuleCollider)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(height)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(radius)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(axis)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(TorusCollider)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(centralRadius)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(tubeRadius)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(segments)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(axis)
COMPONENT_JSON_DESERIALIZE_END

COMPONENT_JSON_DESERIALIZE_BEGIN(ConvexHullCollider)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(points)
COMPONENT_JSON_DESERIALIZE_END


COMPONENT_JSON_DESERIALIZE_BEGIN(CCollider)
    COMPONENT_JSON_DESERIALIZE_PROPERTY(colliderType)
COMPONENT_JSON_DESERIALIZE_END

//===============================================================================




//===============================================================================
// Defining Schema for each component
//===============================================================================
COMPONENT_SCHEMA_BEGIN(CRotation)
    COMPONENT_SCHEMA_PROPERTY(x)
    COMPONENT_SCHEMA_PROPERTY(y)
    COMPONENT_SCHEMA_PROPERTY(z)
    COMPONENT_SCHEMA_PROPERTY(w)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(CPosition)
    COMPONENT_SCHEMA_PROPERTY(x)
    COMPONENT_SCHEMA_PROPERTY(y)
    COMPONENT_SCHEMA_PROPERTY(z)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(CScale)
    COMPONENT_SCHEMA_PROPERTY(x)
    COMPONENT_SCHEMA_PROPERTY(y)
    COMPONENT_SCHEMA_PROPERTY(z)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(CCamera)
    COMPONENT_SCHEMA_PROPERTY(fieldOfViewDegrees)
    COMPONENT_SCHEMA_PROPERTY(nearPlane)
    COMPONENT_SCHEMA_PROPERTY(farPlane)
    COMPONENT_SCHEMA_PROPERTY(environment)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(CScene)
    COMPONENT_SCHEMA_PROPERTY(sceneId)
    COMPONENT_SCHEMA_PROPERTY(sceneIndex)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(DrawReq)
    COMPONENT_SCHEMA_PROPERTY(cullMode)
    COMPONENT_SCHEMA_PROPERTY(polygonMode)
    COMPONENT_SCHEMA_PROPERTY(topology)
    COMPONENT_SCHEMA_PROPERTY(wireframe)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(CPrimitive)
    COMPONENT_SCHEMA_PROPERTY(mesh)
    COMPONENT_SCHEMA_PROPERTY(subMeshIndex)
    COMPONENT_SCHEMA_PROPERTY(material)
    COMPONENT_SCHEMA_PROPERTY(drawReq)
COMPONENT_SCHEMA_END


COMPONENT_SCHEMA_BEGIN(CMesh)
    COMPONENT_SCHEMA_PROPERTY(visible)
    COMPONENT_SCHEMA_PROPERTY(primitives)
COMPONENT_SCHEMA_END


COMPONENT_SCHEMA_BEGIN(CWSADMovement)
    COMPONENT_SCHEMA_PROPERTY(maxSpeed)
    COMPONENT_SCHEMA_PROPERTY(slowSpeed)
    COMPONENT_SCHEMA_PROPERTY(easing)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(CMouseLook)
    COMPONENT_SCHEMA_PROPERTY(sensitivity)
    COMPONENT_SCHEMA_PROPERTY(easing)
COMPONENT_SCHEMA_END


COMPONENT_SCHEMA_BEGIN(CRigidBody)
    COMPONENT_SCHEMA_PROPERTY(mass)
    COMPONENT_SCHEMA_PROPERTY(drag)
    COMPONENT_SCHEMA_PROPERTY(angularDrag)
    COMPONENT_SCHEMA_PROPERTY(isKinematic)
    COMPONENT_SCHEMA_PROPERTY(angularFactor)
    COMPONENT_SCHEMA_PROPERTY(linearFactor)
    COMPONENT_SCHEMA_PROPERTY(disableDeactivation)
COMPONENT_SCHEMA_END


COMPONENT_SCHEMA_BEGIN(SphereCollider)
    COMPONENT_SCHEMA_PROPERTY(radius)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(BoxCollider)
    COMPONENT_SCHEMA_PROPERTY(x)
    COMPONENT_SCHEMA_PROPERTY(y)
    COMPONENT_SCHEMA_PROPERTY(z)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(CapsuleCollider)
    COMPONENT_SCHEMA_PROPERTY(height)
    COMPONENT_SCHEMA_PROPERTY(radius)
    COMPONENT_SCHEMA_PROPERTY(axis)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(TorusCollider)
    COMPONENT_SCHEMA_PROPERTY(centralRadius)
    COMPONENT_SCHEMA_PROPERTY(tubeRadius)
    COMPONENT_SCHEMA_PROPERTY(segments)
    COMPONENT_SCHEMA_PROPERTY(axis)
COMPONENT_SCHEMA_END

COMPONENT_SCHEMA_BEGIN(ConvexHullCollider)
    COMPONENT_SCHEMA_PROPERTY(points)
COMPONENT_SCHEMA_END


COMPONENT_SCHEMA_BEGIN(CCollider)
    COMPONENT_SCHEMA_PROPERTY(colliderType)
COMPONENT_SCHEMA_END



}
}
#endif
