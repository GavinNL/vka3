#ifndef VKA_ECS_NAME_COMPONENT_H
#define VKA_ECS_NAME_COMPONENT_H

#include <string>
#include <functional>
#include <vka/utils/hash.h>

namespace vka
{
namespace ecs {


struct NameComponent
{
protected:
    std::string name;
public:
    NameComponent()
    {
        static size_t i = 1;
        std::hash<void*>  H;
        std::hash<size_t> Hs;
        name = std::to_string( hashCombine( H(this), Hs(i++)) );
    }

    NameComponent(const std::string & n) : name(n)
    {}

    std::string getName() const
    {
        return name;
    }
};

}
}
#endif

