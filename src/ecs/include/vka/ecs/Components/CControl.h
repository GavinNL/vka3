#ifndef VKA_ECS_CONTROL_COMPONENT_H
#define VKA_ECS_CONTROL_COMPONENT_H

#include <memory>
#include <vector>
#include <map>
#include <chrono>
#include <vka/ecs/ECS.h>
#include <vka/ecs/GlobalVariables.h>
#include <vka/ecs/Components/CTransform.h>

namespace vka
{

struct EvtInputMouseWheel;
struct EvtInputMouseButton;
struct EvtInputMouseMotion;
struct EvtInputKey;
struct EvtInputMouseWheel;

struct EvtTick
{
    std::chrono::system_clock::time_point now     = std::chrono::system_clock::now();     // the time point at the start of the frame
    std::chrono::system_clock::time_point nowObj  = std::chrono::system_clock::now();  // the time point at the instant the onTick was called for this object
    std::chrono::system_clock::time_point last    = std::chrono::system_clock::now();    // the time point of the last frame
    std::chrono::system_clock::duration   delta   = now-last;
    double                                delta_d = 0.0;
    uint64_t                              count=0;
};

namespace ecs
{

struct SystemBus;
class ControlSystem;

struct ControlObjectEventTick
{
    virtual ~ControlObjectEventTick(){}
    virtual void onTick(EvtTick m) = 0;
    void enableEvent(bool b) { m_enable = b;}
private:
    bool m_enable=true;
    friend class ControlSystem;
    friend class ControlSystem2;
};

struct ControlObjectEventMouse
{
    virtual ~ControlObjectEventMouse(){}
    virtual void onMouseMove(EvtInputMouseMotion m) = 0;
    virtual void onMouseButton(EvtInputMouseButton m) = 0;
    virtual void onMouseWheel(EvtInputMouseWheel m) = 0;
    void enableEvent(bool b) { m_enable = b;}
private:
    bool m_enable=true;
    friend class ControlSystem;
    friend class ControlSystem2;
};

struct ControlObjectEventKey
{
    virtual ~ControlObjectEventKey(){}
    virtual void onKey(vka::EvtInputKey m) = 0;
    void enableEvent(bool b) { m_enable = b;}
private:
    bool m_enable=true;
    friend class ControlSystem;
    friend class ControlSystem2;
};

enum class ControlObjectState
{
    STOPPED,
    ENABLED,
    DISABLED,
    DESTROYED
};

struct ControlObjectBase
{
    ControlObjectState m_state = ControlObjectState::STOPPED;

    virtual ~ControlObjectBase(){}

    // diable the object, preventing any events from being
    // triggered
    void disable()
    {
        m_enabled = false;
    }

    // enable the object, allowing events to
    // be triggered
    void enable()
    {
        m_enabled = true;
    }

    // destroy this control object.
    // the object will first be disabled
    // and then destroyed when it is appropriate to
    // be destroyed
    void destroy()
    {
        m_toDestroy = true;
    }

    /**
     * @brief onStart
     *
     * Called when the control object has first started.
     * this is called only once.
     */
    virtual void onStart() = 0;

    /**
     * @brief onDestroy
     *
     * Called when the script object is destroyed.
     * This is called only once.
     */
    virtual void onDestroy() = 0;

    /**
     * @brief onEnabled
     *
     * Called when the object has been enabled
     */
    virtual void onEnabled() = 0;

    /**
     * @brief onDisabled
     *
     * Called when the object has been disabled.
     */
    virtual void onDisabled() = 0;

    SystemBus* getSystemBus()
    {
        return m_systemBus;
    }
    SystemBus const* getSystemBus() const
    {
        return m_systemBus;
    }
    auto getEntity() const
    {
        return m_entity;
    }

    bool isEnabled() const
    {
        return m_state==ControlObjectState::ENABLED;
    }
    bool isDisabled() const
    {
        return !isEnabled();
    }
    bool isDestroyed() const
    {
        return m_state==ControlObjectState::DESTROYED;
    }

    vka::Transform getTransform() const
    {
        vka::Transform t;
        auto & r = getSystemBus()->registry;

        if(r.has<CPosition>(getEntity()) )
            t.position = getSystemBus()->registry.get<CPosition>(getEntity());
        if(r.has<CRotation>(getEntity()) )
            t.rotation = getSystemBus()->registry.get<CRotation>(getEntity());

        return t;
    }

    entt::entity getParent(entt::entity e ) const
    {
        return m_systemBus->getParent(e);
    }
    entt::entity getParent() const
    {
        return m_systemBus->getParent(getEntity());
    }

    vka::Transform calculateWorldSpaceTransform(entt::entity e) const
    {
        return m_systemBus->calculateWorldSpaceTransform(e);
    }

    glm::vec3 getPosition() const
    {
        return getSystemBus()->registry.get<CPosition>(getEntity());
    }
    void setPosition(glm::vec3 const & p)
    {
        getSystemBus()->registry.emplace_or_replace<CPosition>(getEntity(), p);
    }
    void translate(glm::vec3 const & p)
    {
        getSystemBus()->registry.emplace_or_replace<CPosition>(getEntity(), getPosition() + p);
    }
private:
    bool m_enabled=false;
    bool m_toDestroy=false;
    SystemBus             * m_systemBus = nullptr;
    entt::entity            m_entity;

    friend class ControlSystem;
    friend class ControlSystem2;
    friend struct CControl;
    std::shared_ptr< std::map<std::string, dynamic_type> > m_variables;
};


struct CControl
{
    using tag_updatedObjects   = entt::tag<"CControl::tag_updatedObjects"_hs>;

    template<typename T>
    CControl& attachObject()
    {
        static_assert( std::is_base_of<ControlObjectBase,T>::value, "Object must be derived from ControlObjectBase" );
        attachObject( std::make_shared<T>() );
        return *this;
    }
    CControl& attachObject( std::shared_ptr<ControlObjectBase> b)
    {
        m_controlObjects.push_back(b);
        return *this;
    }

    template<typename T>
    std::shared_ptr<T> getObject()
    {
        static_assert( std::is_base_of<ControlObjectBase,T>::value, "Object must be derived from ControlObjectBase" );
        for(auto &x : m_controlObjects)
        {
            if( auto s= std::dynamic_pointer_cast<T>(x))
            {
                return s;
            }
        }
        return nullptr;
    }

protected:
    std::vector<std::shared_ptr<ControlObjectBase> > m_controlObjects;
    friend class ControlSystem;
    friend class ControlSystem2;
};


}
}

#endif

