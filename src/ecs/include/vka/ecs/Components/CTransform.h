#ifndef VKA_ECS_TRANSFORM_COMPONENT_H
#define VKA_ECS_TRANSFORM_COMPONENT_H

#include <vka/ecs/json.h>
#include <vka/math/transform.h>
#include <optional>

namespace vka
{
namespace ecs
{

/**
 * @brief The GlobalTransformComponent struct
 *
 * The CWorldTransform is used by the RenderingSystem
 * to place the entity in the world. This component is updated
 * automatically the SceneSystem.
 */
struct CWorldTransform
{
    glm::mat4 worldMatrix;
};

struct CGlobalPosition : public glm::vec3
{
    static constexpr auto _type    = "component";
    static constexpr auto _subType = "CGlobalPosition";

    CGlobalPosition() : glm::vec3(0.0f){}
    CGlobalPosition(glm::vec3 const & _x) : glm::vec3(_x){}
    CGlobalPosition(float _x, float _y, float _z) : glm::vec3(_x,_y,_z){}
};

struct CGlobalRotation : public glm::quat
{
    static constexpr auto _type    = "component";
    static constexpr auto _subType = "CGlobalRotation";

    CGlobalRotation() : glm::quat( glm::identity<glm::quat>() )
    {

    }
    CGlobalRotation(glm::quat const & q) : glm::quat(q)
    {
    }
    CGlobalRotation(float _w, float _x, float _y, float _z) : glm::quat(_w,_x,_y,_z){}

    inline CGlobalRotation& rotateGlobal(const glm::vec3 & axis, float AngleRadians)
    {
        return rotateLocal(glm::conjugate(*this) * axis, AngleRadians);
    }

    inline CGlobalRotation& rotateLocal(const glm::vec3 & axis, float AngleRadians)
    {
        auto q = glm::rotate( *this, AngleRadians, axis );
        x = q.x;
        y = q.y;
        z = q.z;
        w = q.w;
        return *this;
    }
    inline CGlobalRotation& rotate(const glm::vec3 & axis, float AngleRadians)
    {
        auto q = glm::rotate( *this, AngleRadians, axis );
        x = q.x;
        y = q.y;
        z = q.z;
        w = q.w;
        return *this;
    }
    void lookat(glm::vec3 const & pos, glm::vec3 const & at, glm::vec3 const & up )
    {

        glm::vec3 _z = -glm::normalize(pos-at);
        glm::vec3 _x = glm::normalize(glm::cross(up,_z));
        glm::vec3 _y = glm::cross(_z,_x);

        glm::mat3 R(_x,_y,_z);
        auto q = glm::quat_cast(R);
        x = q.x;
        y = q.y;
        z = q.z;
        w = q.w;
    }

};


struct CGlobalScale : public glm::vec3
{
    static constexpr auto _type    = "component";
    static constexpr auto _subType = "CGlobalScale";

    CGlobalScale() : glm::vec3(1,1,1){}
    CGlobalScale(glm::vec3 const & v) : glm::vec3(v){}
    CGlobalScale(float _x, float _y, float _z) : glm::vec3(_x,_y,_z){}
};



struct CPosition : public glm::vec3
{
    static constexpr auto _type    = "component";
    static constexpr auto _subType = "CPosition";

    CPosition() : glm::vec3(0.0f){}
    CPosition(glm::vec3 const & _x) : glm::vec3(_x){}
    CPosition(float _x, float _y, float _z) : glm::vec3(_x,_y,_z){}
};

struct CRotation : public glm::quat
{
    static constexpr auto _type    = "component";
    static constexpr auto _subType = "CRotation";

    CRotation() : glm::quat( glm::identity<glm::quat>() )
    {

    }
    CRotation(glm::quat const & q) : glm::quat(q)
    {
    }
    CRotation(float _x, float _y, float _z) : glm::quat( vec3(_x,_y,_z) ){}
    CRotation(float _w, float _x, float _y, float _z) : glm::quat(_w,_x,_y,_z){}

    inline CRotation& rotateGlobal(const glm::vec3 & axis, float AngleRadians)
    {
        return rotateLocal(glm::conjugate(*this) * axis, AngleRadians);
    }

    inline CRotation& rotateLocal(const glm::vec3 & axis, float AngleRadians)
    {
        auto q = glm::rotate( *this, AngleRadians, axis );
        x = q.x;
        y = q.y;
        z = q.z;
        w = q.w;
        return *this;
    }
    inline CRotation& rotate(const glm::vec3 & axis, float AngleRadians)
    {
        auto q = glm::rotate( *this, AngleRadians, axis );
        x = q.x;
        y = q.y;
        z = q.z;
        w = q.w;
        return *this;
    }
    CRotation& lookat(glm::vec3 const & pos, glm::vec3 const & at, glm::vec3 const & up )
    {

        glm::vec3 _z = -glm::normalize(pos-at);
        glm::vec3 _x = glm::normalize(glm::cross(up,_z));
        glm::vec3 _y = glm::cross(_z,_x);

        glm::mat3 R(_x,_y,_z);
        auto q = glm::quat_cast(R);
        x = q.x;
        y = q.y;
        z = q.z;
        w = q.w;
        return *this;
    }

};

struct CScale : public glm::vec3
{
    static constexpr auto _type    = "component";
    static constexpr auto _subType = "CScale";

    CScale() : glm::vec3(1,1,1){}
    CScale(glm::vec3 const & v) : glm::vec3(v){}
    CScale(float _x, float _y, float _z) : glm::vec3(_x,_y,_z){}
};

}
}

#endif

