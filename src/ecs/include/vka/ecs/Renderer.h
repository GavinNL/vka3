#ifndef VKA_ECS_RENDERER
#define VKA_ECS_RENDERER

#include <vulkan/vulkan.hpp>
#include <vka/math/linalg.h>

namespace vka
{

class SimpleRenderer;

namespace ecs
{

struct RendererBase
{
    RendererBase()
    {
    }
    virtual ~RendererBase(){};
    virtual void beginTopology(vk::PrimitiveTopology topology, glm::mat4 const & transform) { (void)topology; (void)transform;};
    virtual void setLineWidth(float x){(void)x;};
    virtual void setEnableDepth(bool x){(void)x;};
    virtual void setEnableDepthWrite(bool x){(void)x;};
    virtual size_t pushVertex(glm::vec3 const * p, uint32_t const * c, size_t count){ (void)p; (void)c; (void)count; return 0;};
    virtual size_t pushVertex(glm::vec3 const & p, uint32_t const & c){ (void)p; (void)c; return 0;};
    virtual size_t pushVertex(glm::vec3 const & p, glm::u8vec4 const & c){ (void)p; (void)c; return 0;};
};

struct Renderer : public RendererBase
{
    Renderer()
    {
    }

    Renderer(SimpleRenderer * R) : m_simpleRenderer(R){}

    ~Renderer() override {};

    void beginTopology(vk::PrimitiveTopology topology, glm::mat4 const & transform) override;
    void beginTopology(vk::PrimitiveTopology topology, vk::PolygonMode fillMode, glm::mat4 const & transform);
    void setLineWidth(float x) override;
    void setEnableDepth(bool x) override;
    void setEnableDepthWrite(bool x) override;
    size_t pushVertex(glm::vec3 const * p, uint32_t const * c, size_t count) override;
    size_t pushVertex(glm::vec3 const & p, uint32_t const & c) override;
    size_t pushVertex(glm::vec3 const & p, glm::u8vec4 const & c) override;

    SimpleRenderer * m_simpleRenderer;
};

}
}

#endif
