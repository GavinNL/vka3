#ifndef VKA_ECS_H
#define VKA_ECS_H


#include <memory>
#include <vector>
#include <spdlog/spdlog.h>

#define ENTT_ASSERT(condition) if( !(condition) )\
{\
    while(true)\
    {\
        spdlog::critical("ASSERT FAILED");\
        std::this_thread::sleep_for( std::chrono::seconds(1));\
    }\
}

#include <entt/entt.hpp>

namespace vka
{
namespace ecs
{

using namespace entt::literals;

}
}

#endif
