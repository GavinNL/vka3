#ifndef VKA_ECS_FLAGS
#define VKA_ECS_FLAGS

#include<type_traits>

namespace vka
{
namespace ecs
{

template<typename enumType>
struct Flags
{
    using bit_type        = enumType;
    using underlying_type = typename std::underlying_type<enumType>::type;

    static_assert( std::is_enum<enumType>::value, "Must be an enum");

    constexpr bool isSet(bit_type e) const
    {
        return m_flags & static_cast<underlying_type>(e);
    }
    void set(bit_type e)
    {
        m_flags |= static_cast<underlying_type>(e);
    }
    void clear(bit_type e)
    {
        m_flags &= ~(static_cast<underlying_type>(e));
    }
    void clear()
    {
        m_flags = static_cast<underlying_type>(0);
    }
    underlying_type  m_flags = static_cast<underlying_type>(0);
};


}
}

#endif
