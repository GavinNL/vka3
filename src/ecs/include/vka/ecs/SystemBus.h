#ifndef VKA_ECS_SYSTEM_BUS_H
#define VKA_ECS_SYSTEM_BUS_H


#include <memory>
#include <vector>
#include <mutex>
#include <any>
#include <map>
#include <typeindex>
#include <fstream>

#include <vka/math/transform.h>
#include <vka/utils/resource_path.h>

#include "ECS.h"
#include "GlobalVariables.h"
#include "EntityCommandBuffer.h"

#include <vka/ecs/Managers/ResourceManager4.h>
#include <vka/ecs/Components/CName.h>

#include <vka/ecs/json.h>
#include <gnl/socket_shell.h>
#include <vka/utils/threadpool.h>
#include <fmt/format.h>

#include <vka/ecs/Flags.h>
#include <vka/ecs/Events/ResourceEvents.h>

#if 1
#include <spdlog/spdlog.h>

#define SB_TRACE(...) spdlog::trace( __VA_ARGS__ ) //if(m_trace ) m_trace( fmt::format(__VA_ARGS__) )
#define SB_INFO(...)  spdlog::info( __VA_ARGS__ ) //if(m_info  ) m_info ( fmt::format(__VA_ARGS__) )
#define SB_DEBUG(...) spdlog::debug( __VA_ARGS__ ) //if(false && m_debug ) m_debug( fmt::format(__VA_ARGS__) )
#define SB_WARN(...)  spdlog::warn( __VA_ARGS__ ) //if(m_warn  ) m_warn ( fmt::format(__VA_ARGS__) )
#define SB_ERROR(...) spdlog::error( __VA_ARGS__ ) //if(m_error ) m_error( fmt::format(__VA_ARGS__) )
#define SB_CRIT(...)  spdlog::crit( __VA_ARGS__ ) //if(m_critical  ) m_crit ( fmt::format(__VA_ARGS__) )
#else
#define SB_TRACE(...)
#define SB_INFO(...)
#define SB_DEBUG(...)
#define SB_WARN(...)
#define SB_ERROR(...)
#define SB_CRIT(...)

#endif

namespace vka
{

class HostImage;

namespace ecs
{


enum class ResourceFlagBits : uint32_t
{
    eHostBackgroundLoading  = 1<<0,     // Resource is currently being loaded from the filesystem
    eHostLoaded   = 1<<1,     // Resource is in host memory and ready to be copied to device
    eStaged       = 1<<2,     // Resource has been staged and ready to be transfered to the device
    eTransfering  = 1<<3,     // Resource is currently being transfered
    eDeviceLoaded = 1<<4,     // Resource has fully been copied to the device and ready to be used
    eDeviceLoading = 1<<5
};

using ResourceFlags = Flags<ResourceFlagBits>;

struct SystemBus;

template<typename T>
json serialize(SystemBus const & S, T const & P);

struct EvtGameLoop
{
    double dt = 0.0;
};

struct EvtComponentChange
{
    enum _type
    {
        ADDED,
        REMOVED,
        MODIFIED
    };
    entt::entity entity;
    std::string  component;
   _type         type;
};

struct SystemBase;
struct SystemBaseInternal;

using  tag_has_children  = entt::tag<"tag_has_children"_hs>;

// this tag is set if the node is a top-level node
// that means, it does not inhert any parent transforms.
// This does not mean that the node doesn't have a parent.
using  tag_is_root       = entt::tag<"tag_is_root"_hs>;


// Tag used to tell the systems to load
// the resource from host to device
using tag_resource_device_load = entt::tag<"SystemBus::tag_resource_device_load"_hs>;
using tag_resource_device_unload = entt::tag<"SystemBus::tag_resource_device_unload"_hs>;
using tag_resource_device_fully_loaded = entt::tag<"tag_resource_device_fully_loaded"_hs>;


// this tag will be placed on the resource when
// the resource is currently scheduled to load or a background
// thread is currently processing it
using tag_resource_host_visible      = entt::tag<"tag_resource_host_visible"_hs>;
using tag_resource_host_unload       = entt::tag<"tag_resource_host_unload"_hs>;
using tag_resource_host_loading      = entt::tag<"tag_resource_host_loading"_hs>;
using tag_resource_host_fully_loaded = entt::tag<"tag_resource_host_fully_loaded"_hs>;


// if added to the resource entity, it will
// make sure to allocate this from a hostvisible buffer



// if this tag is on an entity
// it will not be serialized
using tag_entity_dont_serialize = entt::tag<"SystemBus::tag_entity_dont_serialize"_hs>;



struct CMain
{
    static constexpr auto _type="CMain";

    entt::entity              entity;


    std::string               name;
    entt::entity              parent     = entt::null;
    entt::entity              rootEntity = entt::null;

    // the actual children in the node hierarchy
    // transforms are propated
    std::vector<entt::entity> children;

    // the entities in this list are "owned" by the
    // current entity, in the sense that they are deleted
    // when the parent is destroyed, but otherwise act
    // like a root entity.
    std::vector<entt::entity> dependents;

    std::vector<char const *> components;

    // temporary variables that can be used
    // for various purposes
    std::map<std::string, std::any> m_variables;

    CMain(uint64_t unq_id) : id(unq_id)
    {

    }
    entt::entity getParent() const
    {
        return parent;
    }
    uint64_t getId() const
    {
        return id;
    }
    bool isSerializable() const
    {
        return m_serializable;
    }
    void setSerializable(bool f )
    {
        m_serializable = f;
    }

    json to_json() const
    {
        json J;
        J["parent"] = to_integral(parent);
        return J;
    }
protected:
    uint64_t                  id;
    uint64_t                  parentId;
    bool m_serializable = true;
    bool m_throwError=true;
    friend struct SystemBus;
};

struct SystemBus;

struct EntityRef
{
    entt::entity      entity  = entt::null;
    entt::registry  * registry = nullptr;
    SystemBus       * systemBus = nullptr;

    operator bool() const
    {
        return entity != entt::null;
    }
    void destroy()
    {
        registry->destroy(entity);
        entity=entt::null;
    }

    operator entt::entity() const
    {
        return entity;
    }
    template<typename Component>
    decltype(auto) get()
    {
        return registry->get<Component>(entity);
    }


    EntityRef createChild();

#if defined VKA_USE_DEPRECATED

    template<typename Component>
    void remove()
    {
        registry->remove<Component>(entity);
    }

    template<typename Component_t, typename Callable_t>
    void patch(Callable_t && C)
    {
        registry->patch<Component_t>(entity, C);
    }

    template<typename Component, typename... Args>
    decltype(auto) assign_or_replace(Args &&... args)
    {
        return registry->emplace_or_replace<Component>(  entity, std::forward<Args>(args)... );
    }

    template<typename Component, typename... Args>
    decltype(auto) create(Args &&... args)
    {
        return registry->emplace_or_replace<Component>(  entity, std::forward<Args>(args)... );
    }

    template<typename Component, typename Callable_t, typename... Args>
    void create_and_update(Callable_t && C, Args &&... args)
    {
        Component comp(std::forward<Args>(args)... );
        C(comp);
        registry->emplace<Component>(entity, std::move(comp));
    }

    template<typename Component, typename... Args>
    decltype(auto) emplace_or_replace(Args &&... args)
    {
        return registry->emplace_or_replace<Component>(  entity, std::forward<Args>(args)... );
    }


    template<typename Component_t>
    decltype(auto) emplace_or_replace(const Component_t & C)
    {
        return registry->emplace_or_replace<Component_t>(  entity, C );
    }

    template<typename Component_t>
    decltype(auto) emplace(const Component_t & C)
    {
        return registry->emplace<Component_t>(  entity, C );
    }
#endif
    template<typename Component_t>
    decltype(auto) emplaceComponent(const Component_t & C={})
    {
        return registry->emplace<Component_t>(  entity, C );
    }
    template<typename Component_t>
    decltype(auto) replaceComponent(const Component_t & C={})
    {
        return registry->replace<Component_t>(  entity, C );
    }
    template<typename Component_t>
    decltype(auto) emplaceOrReplace(const Component_t & C={})
    {
        return registry->replace<Component_t>(  entity, C );
    }
    template<typename Component_t>
    decltype(auto) removeComponent()
    {
        return registry->remove<Component_t>(  entity );
    }
};


struct SystemBus : public std::enable_shared_from_this<SystemBus>
{
    using registry_type = entt::registry;
    using entity_type   = registry_type::entity_type;

    registry_type                   registry;
    entt::dispatcher                dispatcher;
    std::shared_ptr<vka::Resources> rootPaths;

    std::unordered_map< uint64_t, entity_type> m_IdToEntity;
    std::map< std::string, entity_type> m_NameToEntity;
    std::map< entity_type, std::string> m_EntityToName;

    uint64_t m_uniqueId= 0;
public:


    //================================================================
    // Global Variables
    //================================================================
    SystemGlobalVariables variables;

    SystemBus();
    ~SystemBus();

    void onConstructNameComponent(entt::registry &r, entity_type e);

    void onDestroyNameComponent(entt::registry &r, entity_type e);

    void onReplaceNameComponent(entt::registry &r, entity_type e);


    entity_type findEntityById(uint64_t uniqueId) const
    {
        auto f = m_IdToEntity.find(uniqueId);
        if( f == m_IdToEntity.end() ) return entt::null;
        return f->second;
    }

    entity_type findEntityByName( const std::string & name) const;

    template<typename event_t>
    void triggerEvent(event_t const & x)
    {
        dispatcher.trigger<event_t>(x);
    }
    template<typename event_t>
    void queueEvent(const event_t & x)
    {
        dispatcher.enqueue<event_t>(x);
    }
    void processEventQueue()
    {
        dispatcher.update();
    }


    //=====================================================================================
    // Hierarchy Management
    //
    //  An entity can have a parent as well as children. A parent entity
    //  mostly manages it's children. This means, that when a parent is destroyed, all
    //  children are destroyed as well.
    //
    //  By default, child entites will move with the parent entity
    //
    //  A root entity, can be a child of another entity, but it does not inher
    //=====================================================================================
    /**
     * @brief unlinkFromParent
     * @param e
     * @return
     *
     * Unlinks the entity from its parent and returns it's old parent.
     *
     * returns entt::null if it is not a child
     */
    entity_type unlinkFromParent(entity_type e);

    /**
     * @brief linkToParent
     * @param e
     * @param parent
     * @return
     *
     * Links an entity to a parent. If the entity already has a
     * parent, it will be removed from that parent
     */
    entity_type linkToParent(entity_type e, entity_type parent);

    entity_type getParent(entity_type e) const;

    entity_type getRoot(entity_type e) const;

    /**
     * @brief traverseChildren
     * @param e
     * @param c
     * @param parentType
     *
     * Traverse down the tree hierarchy and execute the
     * callback function on each entity along with the parent
     *
     * The value returned by the callback function will be passed down the
     * hierarchy to the next level.
     *
     * This can be used, for example to calculate the global position
     *
     * traversChildren(e,
     * [](auto entity, vec3 position)
     * {
     *      return position + getEntityPosition(entity);
     * },  vec3(0,0,0));
     *
     *
     */
    template<typename Callable_t, typename T>
    void traverseChildren(entity_type e, Callable_t && c, T value)
    {
        auto & M = registry.get<CMain>(e);

        value = c(e, value);

        for(auto ch : M.children)
        {
            traverseChildren(ch, c , value);
        }
    }
    template<typename Callable_t, typename T>
    void traverseChildren(entity_type e, Callable_t && c, T value) const
    {
        auto & M = registry.get<CMain>(e);

        value = c(e, value);

        for(auto ch : M.children)
        {
            traverseChildren(ch, c , value);
        }
    }

    template<typename Callable_t, typename T>
    T traverseParents(entity_type e, Callable_t && c, T value)
    {
        auto & M = registry.get<CMain>(e);

        value = c(e, value);
        if( M.parent != entt::null)
            return traverseParents(M.parent, c, value);
        return value;
    }
    template<typename Callable_t, typename T>
    T traverseParents(entity_type e, Callable_t && c, T value) const
    {
        auto & M = registry.get<CMain>(e);

        value = c(e, value);
        if( M.parent != entt::null)
            return traverseParents(M.parent, c, value);
        return value;
    }
    glm::mat4 calculateWorldSpaceMatrix(entity_type e) const;

    glm::quat calculateWorldSpaceRotation(entity_type e) const;

    vka::Transform calculateWorldSpaceTransform(entity_type e) const;
    //=====================================================================================
    void onDestroyCMain(registry_type & r, entity_type e);


    std::string getEntityName(entity_type id) const;


    //=====================================================================================
    // Entity Creation/Destruction
    //=====================================================================================
    /** whe
     * @brief create
     * @param parentEntity
     * @return
     *
     * Create an empty entity. If parentEntity is given,
     * it is created as a child to that entity
     */
    EntityRef createEntity(entity_type parentEntity=entt::null, std::string const &name="");

    /**
     * @brief createEntity
     * @param uniqueName
     * @param parent
     * @return
     *
     * Create an empty entity with a unique name. Will throw an error
     * if a name already exists.
     */
    EntityRef createEntity(const std::string & uniqueName, entt::entity parent=entt::null);

    /**
     * @brief constructEntity
     * @param _args
     * @return
     *
     * Construct an entity from a parameter pack of components.
     *
     *  constructEntity(
     *        CMesh( box_id, default_material_id, dc ),
     *        CRotation(),
     *        CPosition(0,0,0),
     *        CCollider(vka::ecs::BoxCollider(), true),
     *        CStaticBody(),
     *        CScale( 20,1,20) );
     */
    template<typename... _Elements>
    EntityRef constructEntity( _Elements const&... _args)
    {
        auto E = createEntity();
        buildComponentsFromTuple(registry, E.entity, std::make_tuple(_args...));
        return E;
    }

    template<typename... _Elements>
    EntityRef constructChildEntity( entity_type parent, _Elements const&... _args)
    {
        auto E = createEntity();
        buildComponentsFromTuple(registry, E.entity, std::make_tuple(_args...));
        linkToParent(E.entity,parent);
        return E;
    }
    /**
     * @brief destroyEntity
     * @param e
     *
     * This is the ONLY method of destroying an entity along with all
     * of its children.
     */
    void destroyEntity(entity_type e);


    /**
     * @brief destroyEntityLater
     * @param e
     *
     * Destroys the entity on the next interation of the loop
     */
    void destroyEntityLater(entity_type e)
    {
        queueEntityCommand(
                    [e,this]()
        {
            this->destroyEntity(e);
        });

    }

    void buildFromJson(entity_type e, json const &J);


    //======================================================================================
    // Component Management
    //======================================================================================
    /**
     * @brief removeComponent
     * @param e
     *
     * Remove a component from the entity.
     */
    template<typename Component_t>
    void removeComponent(entity_type e)
    {
        registry.remove<Component_t>(e);
    }

    /**
     * @brief removeComponentLater
     * @param e
     *
     * Schedule the removal of a component from the
     * entity. The component is removed
     * at the start of the next frame.
     */
    template<typename Component_t>
    void removeComponentLater(entity_type e)
    {
        queueEntityCommand(
                    [e,this]()
        {
            this->removeComponent<Component_t>(e);
        });
    }

    template<typename Component_t, typename Callable_t>
    void patchComponent(entity_type e, Callable_t && C)
    {
        registry.patch<Component_t>(e, C );
    }
    //==================================================================================


protected:
    template<size_t I = 0, typename... Tp>
    void buildComponentsFromTuple(entt::registry & r, entt::entity e, std::tuple<Tp...> const & t)
    {
        using type = typename std::tuple_element<I, std::tuple<Tp...>>::type;

        r.emplace_or_replace<type>(e, std::get<I>(t) );

        // do things
        if constexpr(I+1 != sizeof...(Tp))
        {
            buildComponentsFromTuple<I+1>(r,e,t);
        }
    }

public:
    void _recurseSetRoot(entity_type e, entity_type root)
    {
        auto & M = registry.get<CMain>(e);
        M.rootEntity = root;
        for(auto c : M.children)
        {
            _recurseSetRoot(c, root);
        }
    }

    /**
     * @brief _recurseGetChildren
     * @param e
     * @param entites
     *
     * Fills teh vector of entities with all the children down the heirarchy.
     *
     */
    void _recurseGetChildren(entity_type e, std::vector<entity_type> & entites) const
    {
        if( registry.valid(e))
        {
            if( registry.has<CMain>(e))
            {
                auto & M = registry.get<CMain>(e);
                for(auto c : M.children)
                {
                    entites.push_back(c);
                }
                for(auto c : M.children)
                {
                    _recurseGetChildren(c, entites);
                }

            }
        }
    }

    EntityRef _jsonCreate(json const & J, entity_type parent=entt::null);
public:

    void addPath(const std::string & path)
    {
        if(!rootPaths) rootPaths = std::make_shared<vka::Resources>();
        rootPaths->addPath(path);
    }

    /**
     * @brief getPath
     * @param _uri
     * @return
     *
     * given a URi in teh form of  "rc:path_to_resource" or "file:/abs_path"
     */
    std::string getPath(const vka::uri & _uri) const
    {
        if( _uri.scheme == "rc")
        {
            return rootPaths->get( _uri.path );
        }
        else if( _uri.scheme == "file")
        {
            return _uri.path;
        }
        else
        {
            throw std::runtime_error("URI scheme must be either rc: or file:");
        }
    }

    nlohmann::json readResourceJSON(const vka::uri & _uri) const
    {
        return nlohmann::json::parse( readResourceASCII(_uri));
    }
    std::string readResourceASCII(const vka::uri & _uri) const
    {
        auto path = getPath(_uri);

        {
            std::ifstream t( path );
            if(!t)
            {
                throw std::invalid_argument( std::string("File does not exist: ") + path);
            }
            std::string shaderSrc((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
            return shaderSrc;
        }
    }
    std::vector<uint8_t> readResourceBIN(const vka::uri & _uri) const
    {
        auto path = getPath(_uri);

        std::ifstream stream(path, std::ios::in | std::ios::binary);
        std::vector<uint8_t> contents((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
        return contents;
    }
    //=======================
    std::map< std::type_index, std::any> m_managers;



    template<typename Resource_type>
    std::shared_ptr<ResourceManager< Resource_type> > getResourceManager_p()
    {
        using Manager_t = ResourceManager< Resource_type>;
        using Manager_p = std::shared_ptr<Manager_t>;

        auto index_type = std::type_index(typeid(Manager_t));

        auto f = m_managers.find(index_type);
        if( f != m_managers.end() )
        {
            return std::any_cast<Manager_p&>( f->second );
        }
        else
        {
            m_managers[ std::type_index(typeid(Manager_t)) ] = std::make_shared<Manager_t>();
            return getResourceManager_p<Resource_type>();
        }
    }

    /**
     * @brief getResourceManager
     * @return
     *
     * Get a reference to the resource manager of Resource_type, any
     * typename can be a resource.
     */
    template<typename Resource_type>
    ResourceManager< Resource_type>& getResourceManager()
    {
        using Manager_t = ResourceManager< Resource_type>;
        using Manager_p = std::shared_ptr<Manager_t>;

        auto index_type = std::type_index(typeid(Manager_t));

        auto f = m_managers.find(index_type);
        if( f != m_managers.end() )
        {
            return *std::any_cast<Manager_p const&>( f->second );
        }
        else
        {
            m_managers[ std::type_index(typeid(Manager_t)) ] = std::make_shared<Manager_t>();
            return getResourceManager<Resource_type>();
        }
    }

    template<typename Resource_type>
    ResourceManager< Resource_type> const & getResourceManager() const
    {
        using Manager_t = ResourceManager< Resource_type>;
        using Manager_p = std::shared_ptr<Manager_t>;

        auto index_type = std::type_index(typeid(Manager_t));

        auto f = m_managers.find(index_type);
        if( f != m_managers.end() )
        {
            return *std::any_cast<Manager_p const&>( f->second );
        }
        else
        {
            throw std::runtime_error("That resource manager is not available");
        }
    }


    template<typename Resource_type>
    Resource_type& getManagedResource( _ID< Resource_type > id)
    {
        auto & M = getResourceManager<Resource_type>();
        return M.getResource(id);
    }

    /**
     * @brief getResourceId
     * @param name
     * @return
     *
     * Gets a resource id by name. if the name does not exist, returns an invalid id.
     *
     * auto id = getResourceId("non-existant");
     *
     * id.valid() == false
     */
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type getResourceId(const std::string & name)
    {
        auto & M = getResourceManager<Resource_type>();
        return M.findResource(name);
    }

    template<typename Resource_type, typename callable_t>
    typename ResourceManager< Resource_type >::id_type updateOrCreateResource(const std::string & name, callable_t && c)
    {
        auto & M = getResourceManager<Resource_type>();
        auto i = M.findResource(name);
        if( i.valid() )
        {
            updateResource<Resource_type>(i, c);
            return i;
        }
        else
        {
            Resource_type R;
            c(R);
            return resourceCreateAndEmplace<Resource_type>(name, std::move(R) );
        }
    }

    template<typename Resource_type, typename callable_t>
    void updateResource( typename ResourceManager< Resource_type >::id_type id, callable_t && c)
    {
        auto & M = getResourceManager<Resource_type>();

        M.updateResource(id, c);

        EvtResourceEvent_t<Resource_type> DD;
        DD.type = ResourceEvtType::HOST_UPDATED;
        DD.id = id;
        dispatcher.trigger(DD);
    }

    //==============================================================================
    // Creating Resources
    //==============================================================================

    struct _StagingInfoPrivate
    {
        std::chrono::system_clock::time_point emplacedTime = std::chrono::system_clock::now();
        std::chrono::system_clock::time_point lastStaged   = std::chrono::system_clock::now();

        void update()
        {
            lastStaged = std::chrono::system_clock::now();
        }

        double timeSinceLastStaged() const
        {
            auto n = std::chrono::system_clock::now();
            return std::chrono::duration<double>( n- lastStaged ).count();
        }
        double timeSinceEmplaced() const
        {
            auto n = std::chrono::system_clock::now();
            return std::chrono::duration<double>( n- emplacedTime ).count();
        }
    };


    //=====================================================================================================
    /**
     * @brief emplaceResource
     * @param name
     * @param resource
     * @return
     *
     * Emplaces the resource. The resource id must have already been created.
     */
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type emplaceResource(typename ResourceManager< Resource_type >::id_type id, Resource_type && resource)
    {
        auto & M = getResourceManager<Resource_type>();
        auto e = M.entity(id);

        _emplaceResource<Resource_type>(M, e, std::move(resource));

        EvtResourceEvent_t<Resource_type> DD;
        DD.type = ResourceEvtType::HOST_LOADED;
        DD.id = id;
        dispatcher.trigger(DD);

        return id;
    }

    /**
     * @brief emplaceResourceLater
     * @param id
     * @param resource
     * @return
     *
     * Emplaces a resource, but only does so on the main thread.
     * Use this if you are planning on loading a resource in the background
     * on another thread.
     */
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type emplaceResourceLater(typename ResourceManager< Resource_type >::id_type id, std::shared_ptr<Resource_type> resource)
    {
        queueEntityCommand(
        [this, id, resource]()
        {
            this->emplaceResource(id, std::move(*resource));
        });
        return id;
    }
private:
    template<typename Resource_type>
    void _emplaceResource(ResourceManager< Resource_type > & M, entity_type e, Resource_type && resource)
    {
        SB_INFO("RESOURCE Emplaced: {}", M.getResourceName(e));
        M.registry().template emplace<Resource_type>(e, std::move(resource));
        M.registry().template emplace_or_replace<_StagingInfoPrivate>(e);
        M.registry().template emplace<tag_resource_host_fully_loaded>(e);
        M.registry().template remove_if_exists<tag_resource_host_loading>(e);   
    }

    public:

    struct _deviceUseCount
    {
        std::shared_ptr<size_t> m_useCount = std::make_shared<size_t>();
    };

    template<typename Resource_type>
    void setDeviceResourceValid(_ID<Resource_type> const &id, bool valid)
    {
        auto p = getResourceManager_p<Resource_type>();
        auto e= p->entity(id);
        setDeviceResourceValid(*p, e, valid);
    }

    /**
     * @brief setDeviceResourceValid
     * @param M
     * @param e
     * @param valid
     *
     * Sets the Device Resource to the valid/invalid state. If the resource is in a
     * valid state, it means it can be used for graphics related tasks. ie: the
     * resource is fully loaded into the GPU and ready to go.
     *
     * a call to resourceIsDeviceLoaded() will return false for the resource.
     */
    template<typename Resource_type>
    void setDeviceResourceValid(ResourceManager< Resource_type > & M, entity_type e, bool valid)
    {
        if(valid)
        {
            M.registry().template emplace_or_replace<_deviceUseCount>(e);
            M.registry().template emplace_or_replace<tag_resource_device_fully_loaded>(e);
            M.registry().template remove_if_exists<tag_resource_host_loading>(e);
            M.registry().template remove_if_exists<tag_resource_host_updated>(e);
            M.registry().template remove_if_exists<tag_resource_device_load>(e);
            SB_DEBUG("RESOURCE, {} copied to Device", M.getResourceName(e));

            {
                EvtResourceEvent_t<Resource_type> DD;
                DD.type = ResourceEvtType::DEVICE_LOADED;
                DD.id = M.getID(e);
                dispatcher.trigger(DD);
            }
        }
        else
        {
            M.registry().template remove_if_exists<_deviceUseCount>(e);
            M.registry().template remove_if_exists<tag_resource_device_fully_loaded>(e);
            M.registry().template remove_if_exists<tag_resource_device_unload>(e);

            {
                EvtResourceEvent_t<Resource_type> DD;
                DD.type = ResourceEvtType::DEVICE_UNLOADED;
                DD.id = M.getID(e);
                dispatcher.trigger(DD);
            }
        }
    }


    /**
     * @brief hostUnloadResource
     * @param id
     * @param C
     *
     * Method to invoke to unload a particular resource form the host.
     * Use this to unload a custom resource that may depend on
     * other resources. For Example PBRMaterials contains additional
     * texture resources. When a PBRMaterial is unloaded, it should
     * make sure to unload any other resources.
     *
     * This should only be called once id.useCount()==0 otherwise
     * there could be problems.
     */
    template<typename RTYPE, typename Callable_t>
    void hostUnloadResource( _ID<RTYPE> const &id, Callable_t && C)
    {
        auto & RM = getResourceManager<RTYPE>();
        auto & rM = RM.registry();
        auto e = RM.entity(id);

        if( RM.template has<RTYPE>(id))
        {
            auto  & P = rM.template get<RTYPE>(e);
            auto name = RM.getResourceName(e);
            C( P );
            SB_DEBUG("RESOURCE, {} Unloaded from HOST", name);

            rM.template remove<RTYPE>(e);
            rM.template remove_if_exists<tag_resource_host_unload>(e);
            rM.template remove_if_exists<tag_resource_host_fully_loaded>(e);
            rM.template remove_if_exists<tag_resource_host_updated>(e);

            {
                EvtResourceEvent_t<RTYPE> D;
                D.type = ResourceEvtType::HOST_UNLOADED;
                D.id = id;
                dispatcher.trigger(D);
            }
        }
    }


    /**
     * @brief forEachToHostUnload
     * @param C
     *
     * This should be used by various systems to unload
     * resources that have been scheduled to be unloaded from the host.
     * The callback function wil be called for every resource that had
     * hostScheduleHostUnload( ) called on it.
     *
     * It should be used to schedule unloading of any subresources
     */
    template<typename RTYPE, typename Callable_t>
    void forEachResourceToHostUnload( Callable_t && C)
    {
        auto & RM = getResourceManager<RTYPE>();
        auto & rM = RM.registry();
        for(auto e : rM.template view<tag_resource_host_unload, RTYPE>() )
        {
            auto  & P = rM.template get<RTYPE>(e);

            auto name = RM.getResourceName(e);
            C( P );
            SB_DEBUG("RESOURCE {} unloaded from HOST", name);
            rM.template remove<RTYPE>(e);
            rM.template remove<tag_resource_host_unload>(e);
            rM.template remove_if_exists<tag_resource_host_fully_loaded>(e);
            rM.template remove_if_exists<tag_resource_host_updated>(e);

            {
                EvtResourceEvent_t<RTYPE> D;
                D.type = ResourceEvtType::HOST_UNLOADED;
                D.id = RM.getID(e);
                dispatcher.trigger(D);
            }
        }
    }

    template<typename RTYPE, typename DTYPE, typename Callable_t>
    void forEachResourceToDeviceUnload( Callable_t && C)
    {
        auto & RM = getResourceManager<RTYPE>();
        auto & rM = RM.registry();
        for(auto e : rM.template view<tag_resource_device_unload, DTYPE>() )
        {
            auto  & P = rM.template get<DTYPE>(e);

            auto name = RM.getResourceName(e);

            EvtResourceEvent_t<RTYPE> E;
            E.type = ResourceEvtType::DEVICE_WILL_DESTROY;
            E.id   = getResourceManager_p<RTYPE>()->getID(e);
            dispatcher.trigger(E);

            C( P );
            rM.template remove<DTYPE>(e);
            rM.template remove<tag_resource_device_unload>(e);
            SB_DEBUG("Resource {} unloaded from DEVICE", name);

            setDeviceResourceValid<RTYPE>(RM, e, false);
            {
                EvtResourceEvent_t<RTYPE> D;
                D.type = ResourceEvtType::DEVICE_UNLOADED;
                D.id = RM.getID(e);
                dispatcher.trigger(D);
            }
        }
    }


    //=====================================================================================================
    // RESOURCE CREATION FUNCTIONS
    //=====================================================================================================

    /**
     * @brief resourceCreate
     * @param name
     * @return
     *
     * Create a resource id and give it a unique name.
     */
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type resourceCreate(const std::string & name)
    {
        auto & M = getResourceManager<Resource_type>();

        auto id = M.createResource(name);
        {
            EvtResourceEvent_t<Resource_type> DD;
            DD.type = ResourceEvtType::ID_CREATED;
            DD.id = id;
            dispatcher.trigger(DD);
        }

        return id;
    }

    /**
     * @brief resourceCreate
     * @param name
     * @param location
     * @return
     *
     * Create a resource with a unique name and give it a URI so that
     * it can be loaded externally.
     */
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type resourceCreate(const std::string & name, vka::uri const & location)
    {
        auto & M = getResourceManager<Resource_type>();
        auto  id =  M.createResource(name, location);

        auto loc = location;
        M.emplaceData(id, std::move(loc));

        return id;
    }

    /**
     * @brief createResource
     * @param location
     * @return
     *
     * Create a resource from a URI only. The name of the resource will be set to
     * the URI
     */
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type resourceCreate(vka::uri const & location)
    {
        auto name = location.toString();
        return resourceCreate<Resource_type>(name, location);
    }

    /**
     * @brief createResource
     * @param name
     * @param resource
     * @return
     *
     * Create a resource with a given name, the name must be unique. THis will return the ID of the resource.
     */
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type resourceCreateAndEmplace(const std::string & name, Resource_type && resource)
    {
        auto id = resourceCreate<Resource_type>(name);//M.createResource(name);

        emplaceResource(id, std::move(resource));

        return id;
    }
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type resourceCreateAndEmplaceLater(const std::string & name, std::shared_ptr<Resource_type> resource)
    {
        auto id = resourceCreate<Resource_type>(name);//M.createResource(name);

        emplaceResourceLater(id, resource);

        return id;
    }

    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type resourceGetOrEmplace(const std::string & name, Resource_type && resource)
    {
        auto & M = getResourceManager<Resource_type>();
        auto id = M.findResource(name);
        if( id.valid() )
            return id;
        return resourceCreateAndEmplace(name, std::move(resource));
    }
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type resourceGetOrEmplaceLater(const std::string & name, std::shared_ptr<Resource_type> resource)
    {
        auto & M = getResourceManager<Resource_type>();
        auto id = M.findResource(name);
        if( id.valid() )
            return id;
        return resourceCreateAndEmplaceLater<Resource_type>(name, resource);
    }
    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type resourceGetOrCreate(vka::uri const & location)
    {
        auto & M = getResourceManager<Resource_type>();
        //auto name= location.toString();
        auto name = location.toString();
        auto id = M.findResource(name);
        if( id.valid() )
            return id;
        return resourceCreate<Resource_type>(name, location);
    }

    template<typename Resource_type>
    typename ResourceManager< Resource_type >::id_type resourceGetOrCreate(const std::string name, vka::uri const & location)
    {
        auto & M = getResourceManager<Resource_type>();
        auto id = M.findResource(name);

        if( id.valid() )
            return id;
        return resourceCreate<Resource_type>(name, location);
    }

    /**
     * @brief resourceGetOrGenerate
     * @param name
     * @param C
     * @return
     *
     * Returns the resource id if it exists, if it doesn't, calls the callable to generate the
     * resource.
     */
    template<typename Resource_type, typename callable_t>
    typename ResourceManager< Resource_type >::id_type resourceGetOrGenerate(std::string const & name, callable_t && C)
    {
        auto id = getResourceId<Resource_type>(name);
        if( !id.valid() )
        {
            return resourceCreateAndEmplace(name, C());
        }
        return id;
    }

    //==============================================================================
    // Resource Processing/Loading functions
    //==============================================================================

    //===================================================================================================================================
    // THE TWO MAIN LOADING FUNCTIONS:
    //
    //  resourceHostLoadBackground() - spawns a thread to load the resource in from the filesystem
    //
    //  resourceDeviceScheduleLoad() - Schedules loading of the resource into the GPU for use within the rendering stage.
    //                         This method does not acutally do any loading, it only flags the resource to be loaded
    //                         It is up to the RenderSystem to deal with this.
    //
    //  resourceDeviceScheduleUnload() - schedules the resource to be unloaded from the GPU
    //===================================================================================================================================
    /**
     * @brief loadBackground
     * @param id
     * @return
     *
     * Schedule a resource to load in the background.
     * The void loadResource<HostTexture2D>(std::shared_ptr<SystemBus> & sb, typename ResourceManager<HostTexture2D>::id_type id)
     * must have been implemented for this to work.
     *
     * This immediately adds the resouce loading into the thread pool.
     *
     * Returns the current status of the resource.
     */
    template<typename Resource_type>
    ResourceFlags resourceHostLoadBackground( _ID<Resource_type> const &id)
    {
        ResourceFlags flags;

        if( resourceIsDeviceLoaded(id) )
            flags.set( ResourceFlagBits::eDeviceLoaded );

        if( resourceIsHostLoaded(id) )
        {
            // already loaded, no need to schedule anything.
            flags.set( ResourceFlagBits::eHostLoaded );
            return flags;
        }

        if( resourceIsHostLoading(id) )
        {
            // already loaded, no need to schedule anything.
            flags.set( ResourceFlagBits::eHostBackgroundLoading );
            return flags;
        }

        auto & M = getResourceManager<Resource_type>();
        auto e   = M.entity(id);

        // place the tag on it.
        M.registry().template emplace_or_replace<tag_resource_host_loading>( e );
        flags.set( ResourceFlagBits::eHostBackgroundLoading );

        {
            auto _name = M.getResourceName(e);
            SB_DEBUG("RESOURCE {} Background Loader Spawned", _name);
        }

        // make the call to the resource loader.
        // this may schedule in teh background or load
        // imediately depending on what resource it is.
        {
            auto U = M.getUri(id);
            auto index_type = std::type_index(typeid(Resource_type));
            m_resourceLoader.at( index_type )(shared_from_this(), U);
        }

        // did the loader finsh loading?
        // ie: did the loader make the call to emplaceResource( ) ?
        if( resourceIsHostLoaded(id) )
        {
            M.registry().template remove<tag_resource_host_loading>(e);

            flags.set(   ResourceFlagBits::eHostLoaded  );
            flags.clear(   ResourceFlagBits::eHostBackgroundLoading  );
        }
        return flags;
    }

    /**
     * @brief resourceScheduleHostUnload
     * @param id
     *
     * Schedule the resource to be unloaded from the Host. It
     * is up to the appropriate system to properly unload
     * the resource.
     */
    template<typename ResourceType>
    void resourceScheduleHostUnload( _ID<ResourceType> id)
    {
        auto & M = getResourceManager<ResourceType>();
        auto   e = M.entity(id);
        M.registry().template emplace_or_replace<tag_resource_host_unload>(e);
    }


    /**
     * @brief resourceDeviceScheduleLoad
     * @param id
     * @return
     *
     * Set the resource to be loaded into the GPU. When the device
     * is fully loaded into the gpu resourceIsDeviceLoaded(id) will
     * return true
     */
    template<typename Resource_type>
    ResourceFlags resourceDeviceScheduleLoad( _ID<Resource_type> const &id)
    {
        ResourceFlags flags;

        if( resourceIsDeviceLoaded(id) )
        {
            flags.set( ResourceFlagBits::eDeviceLoaded );
            return flags;
        }

        if( !resourceIsDeviceLoading(id) )
        {
            auto & M = getResourceManager<Resource_type>();
            auto   e = M.entity(id);
            M.registry().template emplace_or_replace<tag_resource_device_load>(e);
            flags.set(ResourceFlagBits::eDeviceLoading);
        }

        return flags;
    }


    /**
     * @brief deviceUnload
     * @param id
     * @return
     *
     * Unload a resource from the device (gpu). This simply
     * sets the flag for the resource to be unloaded. A system
     * must actually do the unloaded.
     *
     * Calling this while a a resource has been scheduled to
     * be copied into the device, will first copy the resource
     * into the device, and then remove it.
     */
    template<typename Resource_type>
    ResourceFlags resourceDeviceScheduleUnload( _ID<Resource_type> const &id)
    {
        ResourceFlags flags;
        auto & M = getResourceManager<Resource_type>();
        auto e = M.entity(id);
        auto name = M.getResourceName(e);

        M.registry().template emplace_or_replace<tag_resource_device_unload>(e);
        M.registry().template remove_if_exists<tag_resource_device_load>(e);

        SB_DEBUG("RESOURCE {} SCHEDULED to Unloaded from Device", name);
        return flags;
    }


    template<typename Resource_type>
    ResourceFlags resourceHostUnload( _ID<Resource_type> const &id)
    {
        ResourceFlags flags;
        auto & M = getResourceManager<Resource_type>();
        auto e = M.entity(id);
        auto name = M.getName(e);
        M.registry().template remove<Resource_type>(e);
        M.registry().template remove_if_exists<tag_resource_host_fully_loaded>(e);

        SB_DEBUG("RESOURCE {} unloaded from HOST", name);

        return flags;
    }


    //===================================================================================================================================
    // Resource Validation Functions
    //===================================================================================================================================
    /**
     * @brief resourceIsHostLoaded
     * @param id
     * @return
     *
     * Returns true if the resource is currently loaded into host memory
     *
     */
    template<typename Resource_type>
    bool resourceIsHostLoaded( _ID<Resource_type> const &id )
    {
        auto & M = getResourceManager<Resource_type>();
        auto e = M.entity(id);
        return M.registry().template has<tag_resource_host_fully_loaded>(e);
    }

    template<typename Resource_type>
    bool resourceIsHostLoaded( ResourceManager<Resource_type> & M, typename ResourceManager<Resource_type>::entity_id e )
    {
        return M.registry().template has<tag_resource_host_fully_loaded>(e);
    }

    /**
     * @brief resourceIsDeviceLoaded
     * @param id
     * @return
     *
     * Determines if a resource has been placed into the device (gpu).
     * A various systems determine when and how to place a resource into
     * the device. The system transfering the data to the device must call
     * setDeviceLoadedValid( )  after it has finished transferring to
     * have resourceIsDeviceLoaded() return true.
     */
    template<typename Resource_type>
    bool resourceIsDeviceLoaded( _ID<Resource_type> const & id )
    {
        auto & M = getResourceManager<Resource_type>();
        auto e = M.entity(id);
        return resourceIsDeviceLoaded(M, e);
    }

    template<typename Resource_type>
    bool resourceIsDeviceLoaded( ResourceManager<Resource_type> & M, typename ResourceManager<Resource_type>::entity_id e )
    {
        return M.registry().template has<tag_resource_device_fully_loaded>(e);
    }

    /**
     * @brief resourceIsHostLoading
     * @param id
     * @return
     *
     * Returns true if the resource is currently loading into the device.
     * that is it has been scheduled to load.
     */
    template<typename Resource_type>
    bool resourceIsDeviceLoading( _ID<Resource_type> const &id )
    {
        auto & M = getResourceManager<Resource_type>();
        auto e = M.entity(id);
        return M.registry().template has<tag_resource_device_load>(e);
    }

    /**
     * @brief resourceIsHostLoading
     * @param e
     * @return
     *
     * Returns true if the resource is currently lodaing in the background
     */
    template<typename Resource_type>
    bool resourceIsDeviceLoading( ResourceManager<Resource_type> & M, typename ResourceManager<Resource_type>::entity_id e ) const
    {
        return M.registry().template has<tag_resource_device_load>(e);
    }


    /**
     * @brief resourceIsHostLoading
     * @param id
     * @return
     *
     * Returns true if the resource is currently loading in teh background
     */
    template<typename Resource_type>
    bool resourceIsHostLoading( _ID<Resource_type> const &id )
    {
        auto & M = getResourceManager<Resource_type>();
        auto e = M.entity(id);
        return M.registry().template has<tag_resource_host_loading>(e);
    }

    /**
     * @brief resourceIsHostLoading
     * @param e
     * @return
     *
     * Returns true if the resource is currently lodaing in the background
     */
    template<typename Resource_type>
    bool resourceIsHostLoading( ResourceManager<Resource_type> & M, typename ResourceManager<Resource_type>::entity_id e ) const
    {
        return M.registry().template has<tag_resource_host_loading>(e);
    }

    //==================================================================================================================================
    template<typename ResourceType>
    void updateUseTime(_ID<ResourceType> const &id)
    {
        auto & M = getResourceManager<ResourceType>();
        auto   e = M.entity(id);
        updateUseTime(M, e);
    }
    template<typename ResourceType>
    void updateUseTime( ResourceManager<ResourceType> & M, entt::entity e)
    {
        M.registry().template get<_StagingInfoPrivate>(e).update();
    }
    /**
     * @brief stageResource
     * @param id
     * @return
     *
     * Stage a resource and load it into the GPU. THis will load
     * the resource from it's URI if it is defined.
     *
     * Returns true if it is already staged, returns false
     * if it hasnt.
     */
    template<typename ResourceType>
    bool stageResource( _ID<ResourceType> const &id)
    {
        auto & M = getResourceManager<ResourceType>();
        auto   e = M.entity(id);

        if( resourceIsDeviceLoaded(M,e) )
        {
            M.registry().template get<_StagingInfoPrivate>(e).update();
            return true;
        }
        else
        {
            // This is temprorary for now. eventually
            if( resourceIsHostLoaded(M,e) && !resourceIsHostLoading(M,e) )
            {
                if( !resourceIsDeviceLoading(M,e) )
                {
                    resourceDeviceScheduleLoad(id);
                    return false;
                }
                else
                {
                    return false;
                }
            }
            // This is temporary for now.
            else
            {
                resourceHostLoadBackground(id);
                return false;
            }
        }
        return false;

    }


    /**
     * @brief unstageResource
     * @param id
     *
     * Unstage a resource so that it will be removed
     * from Device (GPU) memory on the next available
     * iteration. This does not remove it
     * from host memory.
     *
     * Any calls to resourceIsDeviceLoaded( ) will return false
     * until a stageResource() is called again and the
     * resource is pushed back into the GPU.
     */
    template<typename ResourceType>
    void unstageResource( _ID<ResourceType> const &id)
    {
        auto & M = getResourceManager<ResourceType>();
        auto   e = M.entity(id);
        if( !M.registry().template has<tag_resource_device_unload>(e) )
        {
            M.registry().template emplace<tag_resource_device_unload>(e);
        }
        M.registry().template remove_if_exists<tag_resource_device_load>(e);
        M.registry().template remove_if_exists<tag_resource_device_fully_loaded>(e);
    }


    template<typename ResourceType>
    double resourceGetTimeSinceLastStaged(_ID<ResourceType> const & id) const
    {
        auto & M = getResourceManager<ResourceType>();
        auto   e = M.entity(id);
        return M.registry().template get<_StagingInfoPrivate>(e).timeSinceLastStaged();
    }
    //==============================================================================



    template<typename Resource_type>
    void resourceDestroy( _ID<Resource_type> id )
    {
        auto & M = getResourceManager<Resource_type>();
        M.destroyResource(id);
    }





    //==============================================================================
    // These two methods are meant for the render system mostly to
    // increment/decrement the device use count.
    //==============================================================================
    template<typename Resource_type>
    LockedResource<Resource_type> resourceDeviceLock( _ID<Resource_type> id )
    {
        auto & M = getResourceManager<Resource_type>();
        auto e = M.entity(id);

        LockedResource<Resource_type> l;
        l.id = id;
        l.m_useCount = M.template get<_deviceUseCount>(e).m_useCount;
        return l;
    }

    template<typename Resource_type>
    size_t resourceGetLockCount( _ID<Resource_type> id )
    {
        auto & M = getResourceManager<Resource_type>();
        auto e = M.entity(id);

        return static_cast<size_t>( M.template get<_deviceUseCount>(e).m_useCount.use_count() );
    }
    //==============================================================================




#if defined VKA_USE_DEPRECATED
    template<typename Component_t>
    void patch(entity_type e)
    {
        registry.patch<Component_t>(e, [](auto & T){(void)T;} );
    }

    template<typename Component_t, typename Callable_t>
    void patch(entity_type e, Callable_t && C)
    {
        registry.patch<Component_t>(e, C );
    }

    template<typename Component_t>
    void patch_if(entity_type e)
    {
        if( registry.has<Component_t>(e))
            registry.patch<Component_t>(e, [](auto & T){(void)T;} );
    }

    template<typename Component_t, typename Callable_t>
    void patch_if(entity_type e, Callable_t && C)
    {
        if( registry.has<Component_t>(e))
            registry.patch<Component_t>(e, C );
    }

#endif



    /**
     * @brief setVariable
     * @param e
     * @param varName
     * @param value
     *
     * Sets an entity variable
     */
    void setVariable(entity_type e, std::string const& varName, std::any value)
    {
        auto & C = registry.get<CMain>(e);
        C.m_variables[varName] = value;
    }

    template<typename T>
    T getVariable(entity_type e, std::string const& varName)
    {
        auto & C = registry.get<CMain>(e);
        return std::any_cast<T>(C.m_variables.at(varName));
    }

    void step(double dt);


    void buildEntity(entity_type e, json const & root);

    //========================================================================================
    // Systems
    //========================================================================================
public:
    /**
     * @brief getSystem
     * @param sysName
     * @return
     *
     * Return a pointer to the system with the given name. Returns nullptr
     * if the system doesn't exist.
     */
    std::shared_ptr<SystemBaseInternal> getSystem(std::string const &sysName);

    void disconnectAllSystems();

    template<typename System_type>
    std::shared_ptr<System_type> disconnectSystem()
    {
        static_assert( std::is_base_of<SystemBaseInternal, System_type>::value, "" );
        std::type_index i( typeid(System_type) );
        auto h = i.hash_code();

        auto it =
        std::remove_if( m_systems2.begin(), m_systems2.end(),
                        [h](auto & s)
        {
            if( std::get<0>(s) == h)
            {
                auto S = std::dynamic_pointer_cast<System_type>( std::get<2>(s) );
                S->stop();
                return true;
            }
            return false;
        });

        m_systems2.erase(it, m_systems2.end());
        auto s = std::make_shared<System_type>();

        const std::type_info  &ti = typeid(*s);
        auto name = std::string(ti.name());

        s->m_systemBus  = shared_from_this();
        s->m_registry   = &registry;
        m_systems2.push_back(  {h,name,s} );
        s->onConstruct();

        return s;
    }


    template<typename System_type>
    std::shared_ptr<System_type> getOrCreateSystem2()
    {
        static_assert( std::is_base_of<SystemBaseInternal, System_type>::value, "" );
        std::type_index i( typeid(System_type) );
        auto h = i.hash_code();
        for(auto & s : m_systems2)
        {
            if( std::get<0>(s) == h)
                return std::dynamic_pointer_cast<System_type>( std::get<2>(s) );
        }

        auto s = std::make_shared<System_type>();

        const std::type_info  &ti = typeid(*s);
        auto name = std::string(ti.name());

        s->m_systemBus  = shared_from_this();
        s->m_registry   = &registry;
        m_systems2.push_back(  {h,name,s} );
        s->onConstruct();

        return s;
    }


    template<typename System_type>
    void addSystemFront( std::shared_ptr<System_type> s)
    {
        static_assert( std::is_base_of<SystemBaseInternal, System_type>::value, "" );
        std::type_index i( typeid(System_type) );
        auto h = i.hash_code();

        const std::type_info  &ti = typeid(*s);
        auto name = std::string(ti.name());

        s->m_systemBus  = shared_from_this();
        s->m_registry   = &registry;
        m_systems2.insert( m_systems2.begin(), {h,name,s} );
        s->onConstruct();
    }

    template<typename System_type>
    void addSystemBack( std::shared_ptr<System_type> s)
    {
        static_assert( std::is_base_of<SystemBaseInternal, System_type>::value, "" );
        std::type_index i( typeid(System_type) );
        auto h = i.hash_code();

        const std::type_info  &ti = typeid(*s);
        auto name = std::string(ti.name());

        s->m_systemBus  = shared_from_this();
        s->m_registry   = &registry;
        m_systems2.push_back(  {h,name,s} );
        s->onConstruct();
    }

    /**
     * @brief createSystem
     * @return
     *
     * Create a system and set its variables, but
     * do not add it to the systembus
     */
    template<typename System_type>
    std::shared_ptr<System_type> createSystem()
    {
        static_assert( std::is_base_of<SystemBaseInternal, System_type>::value, "" );

        auto s = std::make_shared<System_type>();

        const std::type_info  &ti = typeid(*s);
        auto name = std::string(ti.name());

        s->m_systemBus  = shared_from_this();
        s->m_registry   = &registry;

        return s;
    }
protected:
    std::vector< std::tuple<size_t, std::string, std::shared_ptr<SystemBaseInternal> > > m_systems2;

    //========================================================================================
public:
    std::mutex m_taskpoolMutex;

    std::unique_lock<std::mutex> lockTaskPool()
    {
        return std::unique_lock<std::mutex>(m_taskpoolMutex);
    }
    template<class F, class... Args>
    auto pushTaskPool( F&&f, Args&&... args)
    {
        return m_threadpool.push(  std::forward<F>(f), std::forward<Args>(args)...);
    }
    size_t getTaskPoolSize() const
    {
        return m_threadpool.num_tasks();
    }

    void waitThreadPoolIdle() const
    {
        do
        {
            std::this_thread::sleep_for( std::chrono::milliseconds(1));
        } while( getTaskPoolSize() );
    }

    /**
     * @brief lock
     *
     * Lock the mutex associated with the system bus
     * Use this when you are running multiple threads
     * which may change the registry or the resources
     * within.
     */
    [[ nodiscard ]] auto  lock()
    {
        return std::scoped_lock<std::mutex>(m_mutex);
    }

    /**
     * @brief executeLater
     * @param f
     * @param dt - milliseconds from now
     *
     * Execute a functional at a later time.
     */
    template<typename _Rep, typename _Period>
    void executeLater( std::function<void(void)> f, std::chrono::duration<_Rep, _Period> dt)
    {
        m_toCallLater.push( {f, std::chrono::system_clock::now()+dt});
    }

    void executeLater( std::function<void(void)> f, double secondsFromNow)
    {
        m_toCallLater.push( {f, std::chrono::system_clock::now() + std::chrono::milliseconds( static_cast<uint64_t>(secondsFromNow * 1000.0) ) });
    }
    /**
     * @brief queueEntityCommand
     * @param c
     *
     * Queue a single command into the command buffer so that
     * it can be executed later
     */
    template<typename callable_t>
    void queueEntityCommand(callable_t && c)
    {
        m_toCall.push( std::move(c) );
    }

    /**
     * @brief queueEntityCommandBuffer
     * @param cmd
     *
     * queue up another command buffer so that it can be
     * executed later.
     */
    void queueEntityCommandBuffer(  EntityCommandBuffer && cmd )
    {
        for(auto x : cmd.m_toCall)
        {
            m_toCall.push( std::move(x) );
        }
    }

    /**
     * @brief saveScene
     * @return
     *
     * Saves the scene to a json file which can be loaded
     * again later.
     */
    json saveScene() const
    {
        json J;

        for(auto e : registry.view<const CMain>())
        {
            if( registry.has<tag_is_root>(e))
            {
                json jc = json::object();

                if( !registry.has<tag_entity_dont_serialize>(e))
                {
                    J.push_back( saveObject(e));
                }
            }
        }

        return J;
    }
    json saveObject(entity_type e) const
    {
        json jc = json::object();
        for(auto & s : m_serializers)
        {
            s(e, *this, jc);

        }

        auto & M = registry.get<CMain>(e);
        if( M.children.size())
        {
            json jchild = json::array();
            for(auto ec : M.children)
            {
                if( !registry.has<tag_entity_dont_serialize>(ec) )
                {
                    jchild.push_back( saveObject(ec) );
                }
            }
            jc["children"] = std::move(jchild);
        }

        return jc;
    }

    /**
     * @brief registerSerializer
     *
     * Register a component for serialzation.
     */
    template<typename Comp_t>
    void registerSerializer()
    {
        auto lamb = [](entity_type e, SystemBus const & S, nlohmann::json & J)
        {
            if( S.registry.has<Comp_t>(e))
            {
                auto & C = S.registry.get<Comp_t>(e);
                J[Comp_t::_type] = serialize(S, C);
            }
        };
        m_serializers.push_back( lamb );
    }

    //========================================================================

    /**
     * @brief registerPrefab
     * @param name
     * @param builderFunc
     *
     * Register a prefabricated item. You must pass in a functional object
     * which will return an entity_type.
     */
    void registerPrefab(const std::string & name, std::function<entity_type(SystemBus&)> builderFunc)
    {
        if( m_prefabs.count(name))
        {
            throw std::runtime_error( fmt::format("Prefab, {}, already exists",name) );
        }
        m_prefabs[name] = builderFunc;
    }

    /**
     * @brief initPrefab
     * @param name
     * @return
     *
     * Create an instantiation of a prefabricated item.
     */
    EntityRef initPrefab(const std::string & name)
    {
        auto f = m_prefabs.find(name);
        if( f != m_prefabs.end())
        {
            auto & m = m_prefabs.at(name);
            EntityRef R;
            R.entity = m(*this);
            R.systemBus = this;
            R.registry = &registry;
            return  R;
        }
        throw std::runtime_error( fmt::format("Prefab, {}, does not exist.",name) );

    }
    //========================================================================

    template<typename ResourceType>
    void registerResourceLoader(std::function<  void(std::shared_ptr<SystemBus>,vka::uri) > f)
    {
        auto index_type = std::type_index(typeid(ResourceType));
        m_resourceLoader[index_type] = f;
    }

protected:
    std::unordered_map< std::string, std::function<entity_type(SystemBus&)> > m_prefabs;
    std::vector< std::function< void(entity_type, SystemBus const & S, nlohmann::json & J)>> m_serializers;
    vka::thread_pool  m_threadpool;

    std::queue< std::function< void(void) > > m_toCall;

    struct toCallLater_t
    {
        std::function< void(void) >           func;
        std::chrono::system_clock::time_point time;
    };

    std::queue< toCallLater_t > m_toCallLater;

    std::mutex m_mutex;
    friend struct EntityRef;

    std::map< std::type_index, std::function<  void(std::shared_ptr<SystemBus>,vka::uri) > > m_resourceLoader;


};


vka::HostImage loadSingleImage(std::string const& p);


}
}

#undef SB_TRACE
#undef SB_INFO
#undef SB_DEBUG
#undef SB_WARN
#undef SB_ERROR
#undef SB_CRIT

#endif

