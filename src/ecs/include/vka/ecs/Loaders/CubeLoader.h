#ifndef VKA_ECS_CUBE_TEXTURE_LOADER
#define VKA_ECS_CUBE_TEXTURE_LOADER

#include <vka/utils/HostImageLoader.h>
#include <vka/ecs/SystemBus.h>
#include <vka/utils/uri.h>
#include <stdexcept>
#include <fmt/format.h>

#include <vka/ecs/ResourceObjects/HostTexture.h>

#include <easy/profiler.h>

namespace vka
{

namespace ecs
{

struct CubeTextureLoader
{
    using resource_type = HostTextureCube;
    static constexpr auto resourceName = "cubemap";

    static json to_json(resource_type const & C)
    {
        json J;
        J["width"]      = C.getWidth();
        J["height"]     = C.getHeight();
        J["miplevesls"] = C.getLevelCount();
        J["channels"]   = C.getChannels();
        size_t s=0;
        for(size_t i=0;i<6;i++)
        {
            for(auto & M : C.getLayer(i).level)
            {
                s+=M.m_data.size();
            }
        }
        J["byteSize"] = s;
        return J;
    }
    /**
     * @brief load
     * @param sb
     * @param _uri
     * @return
     *
     * This must load a resource from the uri and
     * return the proper resource_type. If the resource_type in turn
     * requires other resources, this function should force load those
     */
    resource_type load(vka::ecs::SystemBus & sb, vka::uri const & _uri)
    {
        auto J = sb.readResourceJSON(_uri);
        auto rootPath = vka::fs::parent_path(sb.getPath(_uri));

        if( J.at("resourceType").get<std::string>() == "cubeMap")
        {
            auto mips = J.at("mipMaps").get< std::vector<std::string> >();
            return cubeFromVector(mips, sb, rootPath);
        }
        throw std::runtime_error("Failed at loading image");
    }



    template<typename T>
    static constexpr bool isPowerOfTwo(T x)
    {
        /* First x in the below expression is for the case when x is 0 */
        return x && (!(x&(x-static_cast<T>(1) )));
    }

    // given a list of URIs of lat-long images, return a HostCubeImage
    static HostTextureCube cubeFromVector( std::vector<std::string> const & uri, vka::ecs::SystemBus & sb, std::string rootPath="")
    {

        HostTextureCube C;
        for(auto & l : C.layer)
        {
            l.level.clear();
        }

        auto SB = &sb;

#define USE_POOL

        std::optional<uint32_t> _W;
        std::queue<std::future<int> > futures;

        std::vector< HostImage > latlongImages;
        latlongImages.resize(20);

        std::mutex mutex;
        size_t i=0;
        for(auto &s : uri)
        {
#if defined USE_POOL
            futures.push( sb.pushTaskPool(
            [SB, s, rootPath, &_W, &mutex, &futures, &C, &latlongImages, i]()
#endif
            {
                vka::uri u(s);
                std::string path;

                if( u.scheme == "rc")
                {
                    path = SB->getPath(u);
                }
                else if( u.scheme == "file")
                {
                    if( u.path.front() == '/')
                    {
                        path = u.path;
                    }
                    else
                    {
                        path = vka::fs::join( rootPath, u.path);
                    }
                }

                EASY_BLOCK("Loading Cube Level");

                EASY_BLOCK("Loading FS");
                    vka::HostImage I = vka::loadHostImage_NoRGB(path);
                EASY_END_BLOCK

                std::cout << "Loading Cube Level: " << path << std::endl;
                if( !isPowerOfTwo( I.width() ))
                {
                    throw std::runtime_error("Image is not a power of two");
                }
                if( I.width() != 2*I.height())
                {
                    throw std::runtime_error("Image width must be 2*height");
                }
                if( _W.has_value())
                {
                    if( *_W != 2* I.width())
                    {
                        throw std::runtime_error( fmt::format("Mip level error. {} image expected to be {}, but is {}", s, *_W/2, I.width()) );
                    }
                }


                latlongImages[i] = std::move(I);

                // For each face in the cube
                // spawn a new threadpool task to convert it
                // from latlong into a cube face.
                for(uint32_t l=0;l<6;l++)
                {
                    auto * Ip = &latlongImages[i];

                    futures.push( SB->pushTaskPool(
                                           [&C, &mutex,l, Ip ]()
                    {
                        EASY_BLOCK("Converting LatLong to CubeFace");
                        auto Cface = HostTextureCube::fromLatLong(*Ip, Ip->height()/2,l);
                        EASY_END_BLOCK
                        {
                            std::scoped_lock<std::mutex> L(mutex);
                            C.getLayer(l).level.push_back( std::move(Cface));
                        }
                        return 0;
                    }) );
                }

                return 0;
            }
#if defined USE_POOL
            ));
#endif
            ++i;
        }

        // Now that all the initial tasks have been placed
        // start popping them off until
        // all of them are completed.
        while(futures.size())
        {
            auto & x = futures.front();
            x.get();
            futures.pop();
        }

        // The order of the mipmaps will be random
        // so we need to sort them according to their
        // size.
        for(uint32_t l=0;l<6;l++)
        {
            auto & faceImage = C.getLayer(l);
            std::sort( faceImage.level.begin(), faceImage.level.end(),[](HostImage const & a, HostImage const & b)
            {
               return a.getWidth()  > b.getWidth();
            });
        }
        return C;
    }



};

}


}

#endif
