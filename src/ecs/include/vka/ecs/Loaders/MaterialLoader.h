#ifndef VKA_ECS_MATERIAL_LOADER
#define VKA_ECS_MATERIAL_LOADER

#include <vka/utils/HostImageLoader.h>
#include <vka/ecs/SystemBus.h>
#include <vka/utils/uri.h>
#include <vka/ecs/ResourceObjects/PBRMaterial.h>

namespace vka
{

namespace ecs
{

struct MaterialLoader
{
    using resource_type = vka::ecs::PBRMaterial;
    static constexpr auto resourceName = "material";

    static json to_json(resource_type const & C)
    {
        json J;
        J["baseColorFactor"]          = {C.baseColorFactor.x,C.baseColorFactor.y,C.baseColorFactor.z,C.baseColorFactor.a};
        J["emissiveFactor"]           = {C.emissiveFactor.x,C.emissiveFactor.y,C.emissiveFactor.z};
        J["metallicFactor"]           = C.metallicFactor          ;
        J["roughnessFactor"]          = C.roughnessFactor         ;
        if( C.baseColorTexture.valid() )
        J["baseColorTexture"]         = C.baseColorTexture.value()        ;
        if( C.metallicRoughnessTexture.valid() )
        J["metallicRoughnessTexture"] = C.metallicRoughnessTexture.value();
        if( C.normalTexture.valid() )
        J["normalTexture"]            = C.normalTexture.value()           ;
        if( C.occlusionTexture.valid() )
        J["occlusionTexture"]         = C.occlusionTexture.value()        ;
        if( C.emissiveTexture.valid() )
        J["emissiveTexture"]          = C.emissiveTexture.value()         ;

        J["normalScale"]              = C.normalScale             ;
        J["exposure"]                 = C.exposure                ;
        J["alphaCutoff"]              = C.alphaCutoff             ;
        J["debugMode"]                = C.debugMode               ;
        J["wireframe"]                = C.wireframe               ;
        J["unlit"]                    = C.unlit                   ;

        return J;
    }

    /**
     * @brief load
     * @param sb
     * @param _uri
     * @return
     *
     * This must load a resource from the uri and
     * return the proper resource_type. If the resource_type in turn
     * requires other resources, this function should force load those
     */
    resource_type load(vka::ecs::SystemBus & sb, vka::uri const & _uri)
    {
        auto strJ = sb.readResourceASCII(_uri);

        auto J = nlohmann::json::parse(strJ);

        auto & IM = sb.getResourceManager<HostTexture2D>();

        resource_type pbr;

        std::string rootPath = vka::fs::parent_path( sb.getPath(_uri) );
        auto _getTexture = [&](const std::string name)
        {
            if( J.count(name) == 0)
                return vka::ecs::Texture2D_ID();

            auto u1 = vka::uri( J.at(name).get<std::string>());
            auto u1i = IM.findResource( u1.toString() );
            if( !u1i.valid() )
            {
                if( u1.scheme == "file" && u1.path.front() =='/')
                {
                    u1.path = vka::fs::join(rootPath, u1.path);
                }


                u1i = sb.resourceGetOrCreate<HostTexture2D>( u1 );
                assert(u1i.valid());
                //sb.forceLoadResource2(u1i);
            }
            return u1i;
        };

        pbr.baseColorTexture         = _getTexture("baseColorTexture");
        pbr.metallicRoughnessTexture = _getTexture("metallicRoughnessTexture");
        pbr.normalTexture            = _getTexture("normalTexture");
        pbr.occlusionTexture         = _getTexture("occlusionTexture");

        from(J, "baseColorFactor", pbr.baseColorFactor);
        from(J, "emissiveFactor" , pbr.emissiveFactor);
        from(J, "metallicFactor" , pbr.metallicFactor);
        from(J, "roughnessFactor", pbr.roughnessFactor);

        from(J, "normalScale"    , pbr.normalScale);
        from(J, "exposure"       , pbr.exposure);
        from(J, "alphaCutoff"    , pbr.alphaCutoff);
        from(J, "wireframe"      , pbr.wireframe);
        from(J, "unlit"          , pbr.unlit);
        from(J, "alphaCutoff"    , pbr.alphaCutoff);


        pbr.debugMode = vka::ecs::PBRDebugMode::NONE;
        if( J.count("debugMode"))
        {
            try {
                pbr.debugMode = from_string<vka::ecs::PBRDebugMode>( J.at("debugMode").get<std::string>() );
            } catch (...) {

            }
        }

        std::cout << "Material Loaded: " << sb.getPath(_uri) << std::endl;
        return pbr;
    }
};

}

}

#endif
