#ifndef VKA_ECS_GLTF_LOADER
#define VKA_ECS_GLTF_LOADER

#include <vka/utils/HostImageLoader.h>
#include <vka/ecs/SystemBus.h>
#include <vka/utils/uri.h>
#include <vka/ecs/ResourceObjects/GltfScene.h>
#include <vka/utils/SceneLoader.h>

namespace vka
{

namespace ecs
{

struct PrimitiveLoader
{
    using resource_type = vka::ecs::HostMeshPrimitive;
    static constexpr auto resourceName = "primitive";

    static json to_json(resource_type const & C)
    {
        (void)C;
        json J;

        J["vertexCount"] = C.vertexCount();
        J["indexCount"]  = C.indexCount();
        J["byteSize"]    = C.requiredByteSize(16);
        J["attributes"] = json::array();

        if( C.POSITION  .byteLength() ) J["attributes"].push_back("POSITION"  );
        if( C.NORMAL    .byteLength() ) J["attributes"].push_back("NORMAL"    );
        if( C.TANGENT   .byteLength() ) J["attributes"].push_back("TANGENT"   );
        if( C.TEXCOORD_0.byteLength() ) J["attributes"].push_back("TEXCOORD_0");
        if( C.TEXCOORD_1.byteLength() ) J["attributes"].push_back("TEXCOORD_1");
        if( C.COLOR_0   .byteLength() ) J["attributes"].push_back("COLOR_0"   );
        if( C.JOINTS_0  .byteLength() ) J["attributes"].push_back("JOINTS_0"  );
        if( C.WEIGHTS_0 .byteLength() ) J["attributes"].push_back("WEIGHTS_0" );


        return J;
    }

    /**
     * @brief load
     * @param sb
     * @param _uri
     * @return
     *
     * This must load a resource from the uri and
     * return the proper resource_type. If the resource_type in turn
     * requires other resources, this function should force load those
     */
    resource_type load(vka::ecs::SystemBus & sb, vka::uri const & _uri)
    {
        (void)sb;
        (void)_uri;
        throw std::runtime_error("Cannot load this object from a URI");
    }

};

#if 0
struct [[ deprecated ]] GLTFLoader
{
    using resource_type = vka::ecs::GLTFScene;
    static constexpr auto resourceName = "scene";

    static json to_json(resource_type const & C)
    {
        (void)C;
        json J;

        return J;
    }

    void _standardize(std::vector< vka::ecs::HostMeshPrimitive   > & rawPrimitives)
    {
        for(auto & p : rawPrimitives)
        {
            if( p.JOINTS_0.componentSize() == 1 ) // if bytes
            {
                p.JOINTS_0 = p.JOINTS_0.convert<uint8_t, uint16_t>();
            }

            if( p.WEIGHTS_0.componentSize() == 1 ) // if bytes
            {
                p.WEIGHTS_0 = p.WEIGHTS_0.convert<uint8_t, float>();

                VertexAttribute newWeights;
                newWeights.reset<float>(4);
                p.WEIGHTS_0.forEach<float>([&](auto & x)
                {
                   for(auto & y : x)
                   {
                       y /= 255.0f;
                   }
                   newWeights.push_back(x);
                });

                p.WEIGHTS_0 =  std::move(newWeights);
            }
            else if( p.WEIGHTS_0.componentSize() == 2 ) // if bytes
            {
                p.WEIGHTS_0 = p.WEIGHTS_0.convert<uint16_t, float>();

                VertexAttribute newWeights;
                newWeights.reset<float>(4);

                p.WEIGHTS_0.forEach<float>([](auto & x)
                {
                   for(auto & y : x)
                       y /= 65535.0f;
                });

                p.WEIGHTS_0 =  std::move(newWeights);
            }
        }
    }
    /**
     * @brief load
     * @param sb
     * @param _uri
     * @return
     *
     * This must load a resource from the uri and
     * return the proper resource_type. If the resource_type in turn
     * requires other resources, this function should force load those
     */
    resource_type load(vka::ecs::SystemBus & sb, vka::uri const & _uri, uint32_t maxDimSize=8*1024)
    {
        auto strJ = sb.getPath(_uri);
        //std::cout << "Image Loaded: " << strJ << std::endl;
        //return vka::loadHostImage(strJ);

        std::string _path = sb.getPath(_uri);

        auto st = std::chrono::system_clock::now();
        std::ifstream in(_path);
        uGLTF::GLTFModel Mo;
        Mo.load(in);

        {

            GLTFScene GS;

            std::vector<vka::ecs::HostMeshPrimitive> P1;
            std::vector<vka::HostTriPrimitive> P;
            std::vector<HostImage> I;

            auto S = fromUGLTF(Mo, P, I);

            GS.nodes        = std::move(S.nodes);
            GS.primitives   = std::move(S.primitives);
            GS.meshes       = std::move(S.meshes);
            GS.materials    = std::move(S.materials);
            GS.animations   = std::move(S.animations);
            GS.skins        = std::move(S.skins);
            GS.scenes       = std::move(S.scenes); // root nodes per scene
            GS.textures     = std::move(S.textures);

            for(auto & x : P)
            {
                P1.push_back( vka::ecs::HostMeshPrimitive(std::move(x)));
            }
            GS.rawPrimitives = std::move(P1);
            GS.images        = std::move(I);

            GS.optimizePrimitives();

            _standardize(GS.rawPrimitives);

            std::cout << "   Optimizing Primitives: " << std::chrono::duration<double>(std::chrono::system_clock::now() - st).count() << std::endl;
            std::string _name = std::to_string(std::hash<std::string>()( _uri.toString() ) );
            //auto _name = _uri.toString();
            {
                uint32_t j=0;
                for(auto & rawP : GS.rawPrimitives)
                {
                    std::string resName = _name + "_P" + std::to_string(j);

                    auto id = sb.resourceGetOrCreate<vka::ecs::HostMeshPrimitive>(resName, std::move(rawP));
                    GS.rawPrimitiveId.push_back( id );
                    std::cout << "   Primitive Resource Created: " << resName  << "  " << std::chrono::duration<double>(std::chrono::system_clock::now() - st).count() << std::endl;
                    j++;
                }
            }

            {
                uint32_t j=0;
                for(auto & image : GS.images)
                {
                    std::string resName = _name + "_I" + std::to_string(j);

                    assert( image.getWidth()*image.getHeight() != 0);

                    while( std::max( image.getWidth(), image.getHeight() ) > maxDimSize)
                    {

                        std::cout << " Downsampling: " << resName  << "  " << std::chrono::duration<double>(std::chrono::system_clock::now() - st).count() << std::endl;
                        std::cout << "    w: " << image.getWidth()  << "  h: " << image.getHeight() << std::endl;
                        image = image.nextMipMap();
                    }

                    HostTexture2D imgtex( std::move(image) );
                    auto id = sb.resourceGetOrCreate<HostTexture2D>(resName, std::move(imgtex));
                    std::cout << "   Image Resource Created: " << resName  << "  " << std::chrono::duration<double>(std::chrono::system_clock::now() - st).count() << std::endl;
                    GS.imageId.push_back( id );
                    j++;
                }
            }

            {
                for(size_t i=0;i<GS.materials.size();i++)
                {
                    PBRMaterial m;
                    auto & M = GS.materials[i];
                    m.baseColorFactor          = M.baseColorFactor;
                    m.roughnessFactor          = M.roughnessFactor;
                    m.metallicFactor           = M.metallicFactor;
                    m.baseColorTexture         = M.baseColorTexture         != -1 ? GS.imageId[ GS.textures[ static_cast<size_t>(M.baseColorTexture)         ].imageIndex ] : Texture2D_ID();
                    m.metallicRoughnessTexture = M.metallicRoughnessTexture != -1 ? GS.imageId[ GS.textures[ static_cast<size_t>(M.metallicRoughnessTexture) ].imageIndex ] : Texture2D_ID();
                    m.normalTexture            = M.normalTexture            != -1 ? GS.imageId[ GS.textures[ static_cast<size_t>(M.normalTexture)            ].imageIndex ] : Texture2D_ID();
                    m.occlusionTexture         = M.occlusionTexture         != -1 ? GS.imageId[ GS.textures[ static_cast<size_t>(M.occlusionTexture)         ].imageIndex ] : Texture2D_ID();
                    m.emissiveTexture          = M.emissiveTexture          != -1 ? GS.imageId[ GS.textures[ static_cast<size_t>(M.emissiveTexture)          ].imageIndex ] : Texture2D_ID();
                    m.emissiveFactor           = M.emissiveFactor;

                    std::string resName = _name + "_M" + std::to_string(i);
                    //std::string resName = _name + "&rawPrimitive=" + std::to_string(i);

                    auto mId = sb.resourceGetOrCreate<PBRMaterial>(resName, std::move(m));

                    GS.pbrMaterials.push_back(mId);
                }
            }

            //RM.emplace_or_replace<GLTFScene>(id) = std::move(GS);
            return  GS;
        }

    }

};
#endif

struct GLTFLoader2
{
    using resource_type = vka::ecs::GLTFScene2;
    static constexpr auto resourceName = "scene";

    static json to_json(resource_type const & C)
    {
        (void)C;
        json J;

        return J;
    }

    void _standardize(std::vector< vka::HostTriPrimitive   > & rawPrimitives)
    {
        for(auto & p : rawPrimitives)
        {
            if( p.JOINTS_0.componentSize() == 1 ) // if bytes
            {
                p.JOINTS_0 = p.JOINTS_0.convert<uint8_t, uint16_t>();
            }

            if( p.WEIGHTS_0.componentSize() == 1 ) // if bytes
            {
                p.WEIGHTS_0 = p.WEIGHTS_0.convert<uint8_t, float>();

                VertexAttribute newWeights;
                newWeights.reset<float>(4);
                p.WEIGHTS_0.forEach<float>([&](auto & x)
                {
                   for(auto & y : x)
                   {
                       y /= 255.0f;
                   }
                   newWeights.push_back(x);
                });

                p.WEIGHTS_0 =  std::move(newWeights);
            }
            else if( p.WEIGHTS_0.componentSize() == 2 ) // if bytes
            {
                p.WEIGHTS_0 = p.WEIGHTS_0.convert<uint16_t, float>();

                VertexAttribute newWeights;
                newWeights.reset<float>(4);

                p.WEIGHTS_0.forEach<float>([](auto & x)
                {
                   for(auto & y : x)
                       y /= 65535.0f;
                });

                p.WEIGHTS_0 =  std::move(newWeights);
            }
        }
    }
    /**
     * @brief load
     * @param sb
     * @param _uri
     * @return
     *
     * This must load a resource from the uri and
     * return the proper resource_type. If the resource_type in turn
     * requires other resources, this function should force load those
     */
    resource_type load(vka::ecs::SystemBus & sb, vka::uri const & _uri, uint32_t maxDimSize=8*1024)
    {
        auto strJ = sb.getPath(_uri);
        //std::cout << "Image Loaded: " << strJ << std::endl;
        //return vka::loadHostImage(strJ);

        std::string _path = sb.getPath(_uri);

        auto st = std::chrono::system_clock::now();
        std::ifstream in(_path);
        uGLTF::GLTFModel Mo;
        Mo.load(in);

        std::string _name = _uri.toString();//std::to_string(std::hash<std::string>()( _uri.toString() ) );

        {

            GLTFScene2 GS;

            auto S = fromUGLTF(Mo, true);

            _standardize(S.rawPrimitives);

            GS.nodes        = std::move(S.nodes);
            GS.animations   = std::move(S.animations);
            GS.skins        = std::move(S.skins);
            GS.scenes       = std::move(S.scenes); // root nodes per scene
            //GS.textures     = std::move(S.textures);


            std::map<uint32_t, Texture2D_ID>     indexToTexture;
            std::map<uint32_t, PBRMaterial_ID>   indexToMaterial;
            std::map<uint32_t, MeshPrimitive_ID> indexToHostMesh;


            std::vector< std::shared_ptr<HostTexture2D> >     _textures;
            std::vector< std::shared_ptr<PBRMaterial> >       _materials;
            std::vector< std::shared_ptr<HostMeshPrimitive> > _hostmesh;

            //=======================================================
            // convert all RawPrimitives into
            // Host MeshPrimitives so that they can be used
            // We will place them as shared pointers for now
            // until we are ready to send them to the resource manager.
            {
                auto LL = sb.getResourceManager_p<HostMeshPrimitive>()->acquireLock();
                for(size_t i=0;i<S.rawPrimitives.size();i++)
                {
                    auto & P = S.rawPrimitives.at(i);

                    HostMeshPrimitive newP(std::move(P));
                    newP.subMeshes.clear();
                    newP.subMeshes.push_back( newP.getDrawCall() );

                    _hostmesh.push_back( std::make_shared<HostMeshPrimitive>(std::move(newP)) );
                }

                //=======================================================
                // convert all meshes
                {
                    // Each mesh in the HostScene (GLTF) is actually
                    // a set of (rawPrimitiveIndex, drawCall)
                    for(auto & gltfMesh : S.meshes)
                    {

                        GLTFScene2::Mesh newMesh;

                        for(auto & gltfMeshPrimitive : gltfMesh.meshPrimitives)
                        {
                            auto & gltfPrimitive = S.primitives[ gltfMeshPrimitive.primitiveIndex ];

                            auto & rawPrimitive = _hostmesh.at(gltfPrimitive.rawPrimitiveIndex);
                            auto drawCallIndex  = static_cast<uint32_t>( rawPrimitive->addSubMesh( gltfPrimitive.drawCall ) );

                            GLTFScene2::Primitive newPrim;
                            newPrim.subMeshIndex = drawCallIndex;

                            newMesh.primitives.push_back( newPrim );
                        }

                        GS.meshes.push_back(newMesh);
                    }

                    assert( GS.meshes.size() == S.meshes.size() );
                }
            }

            //=======================================================
            // convert all textures
            {
                uint32_t j=0;

                for(auto & image : S.images)
                {
//                    std::string resName = "img:" + _name + "_" + std::to_string(j);
                    std::string resName = _name + "/image/" + std::to_string(j);
                    assert( image.getWidth()*image.getHeight() != 0);

                    while( std::max( image.getWidth(), image.getHeight() ) > maxDimSize)
                    {
                        std::cout << " Downsampling: " << resName  << "  " << std::chrono::duration<double>(std::chrono::system_clock::now() - st).count() << std::endl;
                        std::cout << "    w: " << image.getWidth()  << "  h: " << image.getHeight() << std::endl;
                        image = image.nextMipMap();
                    }

                    HostTexture2D imgtex( std::move(image) );
                    auto id = sb.resourceGetOrEmplaceLater<HostTexture2D>(resName, std::make_shared<HostTexture2D>(std::move(imgtex)));

                    indexToTexture[j] = id;
                    j++;
                }
            }


            auto _getTextureID = [&indexToTexture, &S](int32_t textureIndex)
            {
                return textureIndex != -1 ? indexToTexture.at( S.textures[ static_cast<size_t>(textureIndex)].imageIndex ) : Texture2D_ID();
            };
            //=======================================================
            // convert all materials
            for(size_t i=0;i<S.materials.size();i++)
            {
                PBRMaterial m;
                auto & M = S.materials[i];
                m.baseColorFactor          = M.baseColorFactor;
                m.roughnessFactor          = M.roughnessFactor;
                m.metallicFactor           = M.metallicFactor;
                m.baseColorTexture         = _getTextureID(M.baseColorTexture         );
                m.metallicRoughnessTexture = _getTextureID(M.metallicRoughnessTexture );
                m.normalTexture            = _getTextureID(M.normalTexture            );
                m.occlusionTexture         = _getTextureID(M.occlusionTexture         );
                m.emissiveTexture          = _getTextureID(M.emissiveTexture          );
                m.emissiveFactor           = M.emissiveFactor;

                std::string resName = _name + "/mat/" + std::to_string(i);

                auto l = sb.lock();
                auto mId = sb.resourceGetOrEmplaceLater<PBRMaterial>(resName, std::make_shared<PBRMaterial>(std::move(m)));

                indexToMaterial[ static_cast<uint32_t>(i) ] = mId;
            }

            // loop through all the shared pointers that were created
            // and make official resources out of them
            for(size_t i=0 ; i < _hostmesh.size() ; i++)
            {
                std::string resName = _name + "/mesh/" + std::to_string(i);
                auto id = sb.resourceGetOrEmplaceLater(resName, _hostmesh[i]);
                indexToHostMesh[ static_cast<uint32_t>(i) ] = id;
            }

            for(auto & gltfMesh : S.meshes)
            {
                GLTFScene2::Mesh newMesh;

                for(auto & gltfMeshPrimitive : gltfMesh.meshPrimitives)
                {

                     GS.meshes[0].primitives[0].material = indexToMaterial[ gltfMeshPrimitive.materialIndex ];
                     GS.meshes[0].primitives[0].mesh     = indexToHostMesh[ gltfMeshPrimitive.primitiveIndex ];
                }
            }

            for(size_t i=0 ; i < S.meshes.size() ; i++)
            {
                auto & gltfMesh = S.meshes[i];

                for(size_t j=0 ; j < gltfMesh.meshPrimitives.size() ; j++)
                {
                    auto & gltfMeshPrimitive = gltfMesh.meshPrimitives[j];

                    auto & gltfPrimitive = S.primitives[ gltfMeshPrimitive.primitiveIndex];

                    GS.meshes[i].primitives[j].material = indexToMaterial.at(  gltfMeshPrimitive.materialIndex  );
                    GS.meshes[i].primitives[j].mesh     = indexToHostMesh.at(  gltfPrimitive.rawPrimitiveIndex );
                }
            }



            return  GS;
        }

    }

};


}

}

#endif
