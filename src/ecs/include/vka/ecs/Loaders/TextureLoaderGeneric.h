#ifndef VKA_ECS_CUBE_TEXTURE_LOADER_GENERIC
#define VKA_ECS_CUBE_TEXTURE_LOADER_GENERIC

#include <vka/utils/HostImageLoader.h>
#include <vka/ecs/SystemBus.h>
#include <vka/utils/uri.h>
#include <vka/utils/HostCubeImage.h>
#include <stdexcept>
#include <fmt/format.h>

#include <easy/profiler.h>

#include <vka/ecs/ResourceObjects/HostTexture.h>

namespace vka
{

namespace ecs
{

struct HostTextureArrayLoader
{
    using resource_type = vka::HostImageArray;
    static constexpr auto resourceName = "cubemap";

    static json to_json(resource_type const & C)
    {
        json J;
        J["width"]      = C.getWidth();
        J["height"]     = C.getHeight();
        J["miplevesls"] = C.getLevelCount();
        J["channels"]   = C.getChannels();
        size_t s=0;
        for(size_t i=0;i<6;i++)
        {
            for(auto & M : C.getLayer(i).level)
            {
                s+=M.m_data.size();
            }
        }
        J["byteSize"] = s;
        return J;
    }
    /**
     * @brief load
     * @param sb
     * @param _uri
     * @return
     *
     * This must load a resource from the uri and
     * return the proper resource_type. If the resource_type in turn
     * requires other resources, this function should force load those
     */
    resource_type load(vka::ecs::SystemBus & sb, vka::uri const & _uri)
    {
        auto ext = vka::fs::extension(_uri.path);

        if( ext == "jpg" || ext == "jpeg" || ext == "png")
        {
            return loadImage(&sb,_uri);
        }
        else
        {
            auto J = sb.readResourceJSON(_uri);
            auto rootPath = vka::fs::parent_path(sb.getPath(_uri));

            if( J.at("resourceType").get<std::string>() == "cubeMap")
            {
                auto mips = J.at("mipMaps").get< std::vector<std::string> >();
                return cubeFromVector(mips, sb, rootPath);
            }
            else if( J.at("resourceType").get<std::string>() == "image2D" )
            {
                return loadImage(&sb, _uri);
            }
        }
        throw std::runtime_error("Failed at loading image");
    }

    template<typename T>
    static constexpr bool isPowerOfTwo(T x)
    {
        /* First x in the below expression is for the case when x is 0 */
        return x && (!(x&(x-static_cast<T>(1) )));
    }


    // given a list of URIs of lat-long images, return a HostCubeImage
    static vka::HostImageArray cubeFromVector( std::vector<std::string> const & uri, vka::ecs::SystemBus & sb, std::string rootPath="")
    {
        vka::HostImageArray C;
        C.layer.resize(6);
        for(auto & l : C.layer)
        {
            l.level.clear();
        }

        auto SB = &sb;

        // Lat-Long mipmap chain
        std::vector< vka::HostImage > mipmapChain;
        mipmapChain.resize(uri.size());

        size_t i=0;
        for(auto &s : uri)
        {
            auto Lamda1 =
            [SB, s, rootPath, &C, i, &mipmapChain]()
            {
                vka::uri u(s);
                std::string path;

                if( u.scheme == "rc")
                {
                    path = SB->getPath(u);
                }
                else if( u.scheme == "file")
                {
                    if( u.path.front() == '/')
                    {
                        path = u.path;
                    }
                    else
                    {
                        path = vka::fs::join( rootPath, u.path);
                    }
                }

                EASY_BLOCK("Loading Cube Level");

                EASY_BLOCK("Loading FS");
                    vka::HostImage I = vka::loadHostImage_NoRGB(path);
                EASY_END_BLOCK

                std::cout << "Loading Cube Level: " << path << std::endl;
                if( !isPowerOfTwo( I.width() ))
                {
                    throw std::runtime_error("Image is not a power of two");
                }
                if( I.width() != 2*I.height())
                {
                    throw std::runtime_error("Image width must be 2*height");
                }

               mipmapChain[i] = std::move(I);



                // For each face in the cube
                // spawn a new threadpool task to convert it
                // from latlong into a cube face.
                for(uint32_t l=0;l<6;l++)
                {
                    auto * Ip = &mipmapChain[i];

                    auto Lamda2 = [&C,l, Ip ]()
                    {
                        EASY_BLOCK("Converting LatLong to CubeFace");
                        auto Cface = vka::HostCubeImage::fromLatLong(*Ip, Ip->height()/2,l);
                        EASY_END_BLOCK
                        {
                            C.getLayer(l).level.push_back( std::move(Cface));
                        }
                        return 0;
                    };

                    Lamda2();

                }

                return 0;
            };

            Lamda1();
            ++i;
        }

        return C;
    }


    vka::HostImageArray loadImage(vka::ecs::SystemBus * sb, vka::uri const & _uri)
    {
        auto I = _loadSingleImage(sb, _uri);

        HostImageArray Ia;
        Ia.layer.resize(1);
        Ia.layer[0].level.resize(1);
        Ia.layer[0].level[0] = std::move(I);

        return Ia;
    }

    static vka::HostImage _loadSingleImage( vka::ecs::SystemBus * sb, vka::uri _uri )
    {
        auto ext = vka::fs::extension(_uri.path);

        if( ext == "json" )
        {
            auto J = sb->readResourceJSON(_uri);

            auto w = J.at("size").at(0).get<uint32_t>();
            auto h = J.at("size").at(1).get<uint32_t>();
            auto c = static_cast<uint32_t>( J.at("channels").size() );

            std::vector< std::shared_ptr<vka::HostImage> > channels;
            for(auto & x : J.at("images"))
            {
                auto pth = sb->getPath(vka::uri(x.get<std::string>()));

                channels.push_back( std::make_shared<vka::HostImage>( vka::ecs::loadSingleImage( pth ) ) );
            }

            vka::HostImage I;
            I.resize(w,h,c);

            uint32_t ch=0;
            for(auto & x : J.at("channels"))
            {
                auto img  = x.at("image").get<uint32_t>();
                auto chan = x.at("channel").get<uint32_t>();

                I[ch] = (*channels[ img ])[ chan ];

                ch++;
            }
            channels.clear();

            return I;
        }
        else if( ext == "jpg" || ext == "png" || ext=="jpeg" )
        {
            auto strJ = sb->getPath(_uri);

            auto I = vka::ecs::loadSingleImage(strJ);

            return I;
        }
        throw std::runtime_error("Invalid image file");
    }

};

}


}

#endif
