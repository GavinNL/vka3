#ifndef VKA_ECS_ENVIRONMENT_LOADER
#define VKA_ECS_ENVIRONMENT_LOADER

#include <vka/utils/HostImageLoader.h>
#include <vka/ecs/SystemBus.h>
#include <vka/utils/uri.h>
#include <vka/utils/HostCubeImage.h>
#include <stdexcept>
#include <fmt/format.h>

#include <vka/ecs/ResourceObjects/Environment.h>
namespace vka
{

namespace ecs
{

struct EnvironmentLoader
{
    using resource_type = Environment;
    static constexpr auto resourceName = "environment";

    static json to_json(resource_type const & C)
    {
        json J;

        if(C.skybox.valid())
            J["skybox"] = C.skybox.value();
        if(C.radiance.valid())
            J["radiance"] = C.radiance.value();
        if(C.irradiance.valid())
            J["irradiance"] = C.irradiance.value();

        return J;
    }

    /**
     * @brief load
     * @param sb
     * @param _uri
     * @return
     *
     * This must load a resource from the uri and
     * return the proper resource_type. If the resource_type in turn
     * requires other resources, this function should force load those
     */
    resource_type load(vka::ecs::SystemBus & sb, vka::uri const & _uri)
    {
        auto J = sb.readResourceJSON(_uri);
        auto rootPath = vka::fs::parent_path(sb.getPath(_uri));

        auto rad = J.at("radiance").get<std::string>();
        auto irr = J.at("irradiance").get<std::string>();

        auto u_rad = vka::uri(rad);
        auto u_irr = vka::uri(irr);

        if( u_rad.scheme == "file" && u_rad.path.front()!='/')
        {
            u_rad.path = vka::fs::join(rootPath, u_rad.path);
        }
        if( u_irr.scheme == "file" && u_irr.path.front()!='/')
        {
            u_irr.path = vka::fs::join(rootPath, u_irr.path);
        }

        if( J.count("skybox") )
        {
            auto u_sky = vka::uri(J.at("skybox").get<std::string>());
            if( u_sky.scheme == "file" && u_sky.path.front()!='/')
            {
                u_sky.path = vka::fs::join(rootPath, u_sky.path);
            }

            //E.skybox =  m_systemBus->getResourceId<vka::HostCubeImage>(sky);
            auto E = Environment( sb.resourceGetOrCreate<HostTextureCube>( u_rad ),
                                  sb.resourceGetOrCreate<HostTextureCube>( u_irr ),
                                  sb.resourceGetOrCreate<HostTextureCube>( u_sky ) );
            //sb.forceLoadResource2( E.skybox );
            //sb.forceLoadResource2( E.radiance );
            //sb.forceLoadResource2( E.irradiance );
            return E;
        }
        else
        {
           auto E = Environment( sb.resourceGetOrCreate<HostTextureCube>( u_rad ),
                                sb.resourceGetOrCreate< HostTextureCube>( u_irr ));
           //sb.forceLoadResource2( E.skybox );
           //sb.forceLoadResource2( E.radiance );
           //sb.forceLoadResource2( E.irradiance );
           return E;
        }
        throw std::runtime_error("Failed at loading Environment");
    }

};

}

}

#endif
