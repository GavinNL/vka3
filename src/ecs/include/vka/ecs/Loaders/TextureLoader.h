#ifndef VKA_ECS_TEXTURE_LOADER
#define VKA_ECS_TEXTURE_LOADER

#include <vka/utils/HostImageLoader.h>
#include <vka/ecs/SystemBus.h>
#include <vka/utils/uri.h>
#include "TextureLoaderGeneric.h"

namespace vka
{

namespace ecs
{


struct TextureLoader
{
    using resource_type = HostTexture2D;
    static constexpr auto resourceName = "texture";

    resource_type load(vka::ecs::SystemBus & sb, vka::uri const & _uri)
    {
        HostTextureArrayLoader loader;
        auto I = loader.load(sb, _uri);

        resource_type Ir;
        Ir.layer = std::move(I.layer);
        return Ir;
    }

    static json to_json(resource_type const & C)
    {
        json J;
        J["width"]    = C.getWidth();
        J["height"]   = C.getHeight();
        J["channels"] = C.getChannels();
        J["byteSize"] = 0;//C.m_data.size();
        return J;
    }
};

}

}

#endif
