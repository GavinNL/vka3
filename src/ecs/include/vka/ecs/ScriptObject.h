#ifndef VKA_ECS_SCRIPT_OBJECT
#define VKA_ECS_SCRIPT_OBJECT

#include <vka/ecs/ECS.h>
#include <iostream>
#include <vka/ecs/Components/CTransform.h>
#include <variant>
#include "SystemBus.h"

namespace vka
{
namespace ecs
{


struct Engine_t
{
    entt::registry        * m_registry;
    SystemBus             * m_systemBus;

    template<typename... Component>
    auto has(const entt::entity entity) const
    {
        return m_registry->has<Component...>(entity);
    }

    /*! @copydoc get */
    template<typename... Component>
    decltype(auto) get( const entt::entity entity)
    {
        return m_registry->get< Component...>(entity);
    }

    /*! @copydoc get */
    template<typename... Component>
    decltype(auto) get( const entt::entity entity) const
    {
        return m_registry->get< Component...>(entity);
    }
};

struct ScriptObject
{
    entt::entity _entity_id;
    uint64_t     _scriptId;
    Engine_t     ENGINE;
    bool _started = false;
    bool _stopped = false;

    using var_type  = std::variant<  int8_t, int16_t, int32_t, int64_t,
                                     uint8_t, uint16_t, uint32_t, uint64_t,
                                    glm::vec3, glm::vec2, glm::vec4, glm::quat, vka::transform,
                                   float, double, std::string>;

    std::map< std::string, var_type > m_variables;
    //=============================================
    entt::entity entity_id() const
    {
        if( ENGINE.m_registry->valid(_entity_id))
            return _entity_id;
        return entt::null;

    }
    auto scriptId() const
    {
        return _scriptId;
    }
    auto& registry()
    {
        return ENGINE.m_systemBus->registry;
    }
    auto & global()
    {
        return ENGINE.m_systemBus->variables;
    }
    //=============================================
    virtual ~ScriptObject()
    {

    }

    // returns the name of the script, should be a unique name.
    virtual std::string Name() const = 0;


    virtual void setVariable( const std::string & name, var_type v)
    {
        m_variables[name] = v;
        this->Update();
    }
    //=============================================
    // These are the 3 required methods that should
    // be overridden.
    //=============================================
    // called when the script is initialized
    // use this to register any callbacks.
    virtual void Start()
    {
        std::cout << "ScriptObject::Start()" << std::endl;
    }

    // called when the script object is destroyed.
    virtual void Stop()
    {
        std::cout << "ScriptObject::Stop()" << std::endl;
    }

    // called when the entity has been updated in any way
    // this can be due to components added/removied
    // or script variables changed
    virtual void Update()
    {
        std::cout << "ScriptObject::Update()" << std::endl;
    }
    //=============================================



    template<typename Event>
    auto sink()
    {
        return ENGINE.m_systemBus->dispatcher.sink<Event>();
    }
};

}
}

#endif
