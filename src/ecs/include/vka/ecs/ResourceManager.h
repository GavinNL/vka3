#ifndef VKA_ECS_RESOURCE_MANAGER_H
#define VKA_ECS_RESOURCE_MANAGER_H

#include <string>
#include <vector>
#include <map>
#include <queue>
#include <stdexcept>

#include "ID.h"

namespace vka
{
namespace ecs
{

template<typename T>
struct ResourceManager
{
    using key_type   = std::string;
    using value_type = T;
    using id_type    = _ID<T>;

    /**
     * @brief at
     * @param id
     *
     * This is the prefered method of accessing the resource.
     * the resource is stored in a vector, and the id is
     * basically the index. This is the fastest way of accessing the resource.
     */
    value_type & at(id_type id)
    {
        return resources[id.value()];
    }

    /**
     * @brief find
     * @param key
     * @return
     *
     * Find an id in the resource manager. Use this once
     * to find the id, then use the at(id) to get the actual
     * resource.
     *
     * Returns -1 if the id does not exist
     */
    id_type find(key_type const & key) const
    {
        auto f = nameToId.find(key);
        if( f == nameToId.end() )
        {
            return id_type();
        }
        return f->second;
    }

    auto at(key_type const key)
    {
        auto id = find(key);
        if( id.valid() )
            return at(id);
        throw std::out_of_range("That Key does not exist in the manager");
    }

    size_t count(key_type const & k) const
    {
        return find(k).valid();
    }

    id_type newResource(key_type const k)
    {
        auto f = find(k);
        if( !f.valid() )
        {
            auto id = _newId();
            nameToId[k] = id;
            return id;
        }
        throw std::out_of_range("A Key already exists with that name");
    }

    auto begin()
    {
        return nameToId.begin();
    }
    auto end()
    {
        return nameToId.end();
    }
    // this is the main erase function
    void erase(id_type id)
    {
        auto name = IdToName.at(id);

        // remove the actual resource
        resources[id.value()].reset();// = value_type();

        // push the id into the free slots
        freeIds.push(id);

        // erase the name
        nameToId.erase(name);
    }
    void erase(key_type const & k)
    {
        auto id = find(k);
        if( id.valid() )
            erase(id);
    }
    void erase(value_type const & k)
    {
        erase( id_type( static_cast<typename id_type::integral_type>(k->resourceId) ) );
    }

    void clear()
    {
        nameToId.clear();
        resources.clear();
    }

    id_type _newId()
    {
        if( freeIds.size() )
        {
            auto i = freeIds.front();
            freeIds.pop();
            return i;
        }
        resources.emplace_back();
        return id_type( static_cast<typename id_type::integral_type>(resources.size()-1) );
    }
    std::vector<T>        resources;
    std::map<key_type,id_type> nameToId;
    std::map<id_type, key_type> IdToName;
    std::queue<id_type>   freeIds;
};


}
}
#endif
