#ifndef VKA_ECS_RESOURCE_MANAGER4_H
#define VKA_ECS_RESOURCE_MANAGER4_H

#include <unordered_map>
#include <set>

#include <vka/ecs/ECS.h>

#include <vka/utils/uri.h>
#include <vka/ecs/ID.h>
#include <mutex>

namespace vka
{
namespace ecs
{


using tag_resource_force_unload = entt::tag<"ResourceForceUnLoad"_hs>;


// this tag will be placed on a resource
// that needs to be loaded immediately.
using tag_resource_load_immediately = entt::tag<"ResourceLoadImmediately"_hs>;


// Place this tag on a resource if the host has been updated
using tag_resource_host_updated = entt::tag<"tag_resource_host_updated"_hs>;


template<typename ResourceType>
struct ResourceManager4
{
    using id_type       = vka::ecs::_ID<ResourceType>;
    using entity_id     = entt::entity;
    using resource_type = ResourceType;//vka::ecs::ManagedResource3<ResourceType>;
    using value_type    = ResourceType;
    using registry_type = entt::registry;

    struct _nameComp
    {
        std::string name;
        _nameComp(const std::string & n):name(n){}
    };

    ResourceManager4()
    {
        m_registry = std::make_shared<entt::registry>();
    }

    void destroyAll()
    {
        std::vector<id_type> toDestroy;
        for(auto x : m_nameToID)
            toDestroy.push_back(x.second);
        for(auto x : toDestroy)
        {
            destroyResource(x);
        }
    }

    size_t size() const
    {
        auto s = m_registry->size<resource_type>();
        assert( s == m_nameToID.size()     );
        return s;
    }

    /**
     * @brief destroyResource
     * @param i
     *
     * Destroys the resource and any data attached to it.
     * This always destroys the resource_type component
     * last.
     */
    void destroyResource(id_type const& i)
    {
        auto ei   = entity(i);
        auto name = m_registry->get<_nameComp>(ei).name;

        // Visit all the components of the entity
        // and destroy them if it is not the CHeader
        // the CHeader entity should always be the last one to be
        // destroyed
        m_registry->visit([&](const auto info)
        {
            if( info != entt::type_id<_nameComp>() && info != entt::type_id<resource_type>() )
            {
                entity_id as_array[1]{ei};
                m_registry->storage(info)->remove(*m_registry, as_array, as_array+1u);
            }
        });

        m_registry->remove<resource_type>( ei  );

        // destroy the nameComp if it exists
        m_registry->destroy(ei);

        m_nameToID.erase(name);
    }

    id_type createResource(const std::string & name)
    {
        if( _isNameUnique(name) )
        {
            auto e  = m_registry->create();
            id_type id( static_cast<uint32_t>(e), true);

            _emplace(name, id);

            m_registry->emplace<_nameComp>(e, name);

            return id;
        }
        throw std::runtime_error("Non unique name");
    }
    id_type createResource(const std::string & name, value_type && R)
    {
        if( _isNameUnique(name) )
        {
            auto id = createResource(name);
            auto  e = entity(id);

            m_registry->emplace<resource_type>(e, std::move(R));


            return id;
        }
        throw std::runtime_error("Non unique name");
    }

    id_type createResource(const std::string & name, vka::uri const & location)
    {
        if( _isNameUnique(name) )
        {
            auto id = createResource(name);
            auto  e = entity(id);

            m_registry->emplace<vka::uri>(  e, location);

            return id;
        }

        throw std::runtime_error("Error creating resource");
    }

    id_type findResource(const std::string & uniqueName) const
    {
        auto it = m_nameToID.find(uniqueName);
        if( it != m_nameToID.end() )
        {
            return it->second;;
        }
        return id_type();
    }
    vka::uri getUri(id_type const&i) const
    {
        auto e = entity(i);
        if( m_registry->has<vka::uri>( e ) )
        {
            return m_registry->get<vka::uri>(e);
        }
        return {};
    }
    id_type getIdFromUri(vka::uri const & u) const
    {
        for(auto x : m_registry->view<vka::uri, _nameComp>())
        {
            auto & ux = m_registry->get<vka::uri>(x);
            if( ux.toString() == u.toString())
            {
                auto & n = m_registry->get<_nameComp>(x);
                return m_nameToID.at(n.name);
            }
        }
        return id_type();
    }
    std::string getResourceName(id_type const& i) const
    {
        return m_registry->get<_nameComp>( entity(i) ).name;
    }
    std::string getResourceName(entity_id i) const
    {
        return m_registry->get<_nameComp>( i ).name;
    }
    template<typename Callable_t>
    void each( Callable_t && c)
    {
        m_registry->each( c );
    }
    resource_type& getResource(entity_id id)
    {
        return m_registry->get<resource_type>( id );
    }

    resource_type& getResource(id_type const &id)
    {
        return m_registry->get<resource_type>( entity(id) );
    }
    resource_type const & getResource(id_type const &id) const
    {
        return m_registry->get<resource_type>( entity(id) );
    }

    template<typename callable_t>
    void updateResource(id_type const& id, callable_t && C)
    {
        m_registry->patch<resource_type>( entity(id) , C);
    }

    /*! @copydoc get */
    template<typename... Component>
    decltype(auto) get( id_type const &id)
    {
        return m_registry->get<Component...>( entity(id) );
    }

    /*! @copydoc get */
    template<typename... Component>
    decltype(auto) get( const entity_id e)
    {
        return m_registry->get<Component...>(e);
    }

    /*! @copydoc get */
    template<typename... Component>
    decltype(auto) has( const entity_id e)
    {
        return m_registry->has<Component...>(e);
    }
    template<typename... Component>
    decltype(auto) has( id_type const &i)
    {
        return m_registry->has<Component...>( entity(i) );
    }

    template<typename Component>
    decltype(auto) emplaceData(id_type const& id, Component && args)
    {
        return m_registry->emplace_or_replace<Component>( entity(id), std::move(args) );
    }

    template<typename TagComponent>
    decltype(auto) emplaceTag(id_type const& id)
    {
        return m_registry->emplace_or_replace<TagComponent>( entity(id) );
    }
    //template<typename Component, typename... Args>
    //decltype(auto) emplace_or_replace(id_type const& id, Args &&... args)
    //{
    //    return m_registry->emplace_or_replace<Component>( entity(id), std::forward<Args>(args)...);
    //}
    //template<typename Component, typename... Args>
    //decltype(auto) emplace_or_replace(const entity_id entity, Args &&... args)
    //{
    //    return m_registry->emplace_or_replace<Component>( entity, std::forward<Args>(args)...);
    //}
    template<typename Component>
    decltype(auto) remove(entity_id entity)
    {
        m_registry->remove<Component>(entity);
    }
    template<typename Component>
    decltype(auto) remove(id_type const &id)
    {
        m_registry->remove_if_exists<Component>( entity(id));
    }
    template<typename... Component, typename... Exclude>
    auto view( entt::exclude_t<Exclude...> e= {})
    {
        return m_registry->view<Component...>( e);
    }

    auto getID(entity_id e) const
    {
        return id_type( static_cast<uint32_t>(e), false);
    }

    auto & registry()
    {
        return *m_registry;
    }
    auto const & registry() const
    {
        return *m_registry;
    }

    entity_id entity(id_type const &id) const
    {
        return entity_id( id.value() );
        //return m_idToEntity.at(id.value());
    }

    /**
     * @brief acquireLock
     *
     * Lock the mutex associated with the system bus
     * Use this when you are running multiple threads
     * which may change the registry or the resources
     * within.
     */
    [[ nodiscard ]] auto  acquireLock()
    {
        return std::scoped_lock<std::mutex>(m_mutex);
    }

private:
    void _emplace(const std::string & name, id_type const &id)
    {
        m_nameToID[name]     = id;
    }
    bool _isNameUnique(const std::string & name) const
    {
        return m_nameToID.count(name)==0;
    }
    id_type _nextID()
    {
        id_type id(m_nextId+1, true);
        ++m_nextId;
        return id;
    }
    std::shared_ptr<entt::registry> m_registry;
    typename id_type::integral_type m_nextId=0;

    std::unordered_map<std::string, id_type>   m_nameToID;

    std::mutex m_mutex;
};

template<typename T>
using ResourceManager = ResourceManager4<T>;

template<typename T>
using ResourceManager_p = std::shared_ptr<ResourceManager<T> >;

}

}

#endif
