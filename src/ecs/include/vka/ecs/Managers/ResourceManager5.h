#ifndef VKA_ECS_RESOURCE_MANAGER_5_H
#define VKA_ECS_RESOURCE_MANAGER_5_H

#include <typeindex>
#include <string>
#include <vector>
#include <map>
#include <queue>
#include <stdexcept>

#include <entt/entt.hpp>

namespace vka
{
namespace ecs
{

template<typename T>
struct Object_t
{
    using resource_type = T;

#define Def_Op(OP)\
    bool operator OP (Object_t<T> const & other) const\
    {\
        return m_entity OP other.m_entity;\
    }

    Def_Op(==)
    Def_Op(!=)
    Def_Op(<)
    Def_Op(>)
    Def_Op(<=)
    Def_Op(>=)
#undef Def_Op
protected:
    entt::entity m_entity;
    friend struct ResourceManager5;
};

struct ResourceManager5
{
    using entity_type   = entt::entity;
    using registry_type = entt::basic_registry<entity_type>;
    using ResourceLock  = std::shared_ptr<int32_t>;

    struct Header
    {
        std::size_t  typeInfoHash;
        std::string  name;
        std::size_t  typeIdHash;//entt
        ResourceLock lock;

        bool m_throw = true;
    };

    ResourceManager5()
    {
        getRegistry().on_destroy<Header>().connect<&ResourceManager5::on_destroyHeader>(*this);
    }

    ~ResourceManager5()
    {
        // make sure all resources are destroyed properly
        getRegistry().each( [this](auto e)
        {
            this->_resourceDestroy(e);
        });
        getRegistry().on_destroy<Header>().disconnect<&ResourceManager5::on_destroyHeader>(*this);
    }
    /**
     * @brief resourceCreate
     * @return
     *
     * Create a resource id and return it
     */
    template<typename T>
    Object_t<T> resourceCreate()
    {
        Object_t<T> id;

        id.m_entity = getRegistry().create();
        auto &    H = getRegistry().emplace<Header>(id.m_entity);

        H.typeInfoHash = std::type_index(typeid(T)).hash_code();
        H.typeIdHash   = entt::type_id<T>().hash();
        H.lock         = std::make_shared<ResourceLock::element_type>();
        return id;
    }

    /**
     * @brief resourceDestroy
     * @param id
     * @return
     *
     * Destroys the entity and any components attached to it.
     * Returns true if the resourcewas destroyed, and false if
     * the resource cannot be destroyed.
     *
     * A resource cannot be destroyed if it has been locked.
     */
    template<typename T>
    bool resourceDestroy(Object_t<T> const & id)
    {
        return _resourceDestroy(id.m_entity);
    }

    /**
     * @brief resourceIsValid
     * @param id
     * @return
     *
     * Returns true if the resource id is valid.
     */
    template<typename T>
    bool resourceIsValid(Object_t<T> const & id) const
    {
        return getRegistry().valid(id.m_entity);
    }

    /**
     * @brief setName
     * @param name
     * @return
     *
     * Sets the name of the resource, returns true
     * if the name was set, returns false if it wasn't
     * due to a name conflict. All resources must have
     * a unique name
     */
    template<typename T>
    bool resourceSetName(Object_t<T> const & id, std::string const & name)
    {
        if( m_nameToEntity.count(name))
        {
            return false;
        }
        m_nameToEntity[name] = id.m_entity;
        auto & H = getRegistry().get<Header>(id.entity);
        H.name = name;
        return true;
    }

    /**
     * @brief resourceEmplace
     * @param id
     * @param resource
     * @return
     *
     * Emplace the data on the resource id. The resource must not
     * already have a resource data.
     */
    template<typename T, typename R>
    Object_t<T> resourceEmplaceData(Object_t<T> const & id, R && resource)
    {
        getRegistry().emplace<R>(id.m_entity, std::move(resource));
        return id;
    }

    template<typename R, typename T>
    Object_t<T> resourceRemoveData(Object_t<T> const & id)
    {
        getRegistry().remove<R>(id.m_entity);
        return id;
    }

    /**
     * @brief resourceCreateAndEmplace
     * @param resource
     * @return
     *
     * Create and emplace the resource data
     */
    template<typename T>
    Object_t<T> resourceCreateAndEmplace(T && resource)
    {
        auto id = resourceCreate<T>();
        resourceEmplaceData<T>(id, std::move(resource));
        return id;
    }


    template<typename T>
    [[nodiscard]] ResourceLock resourceLock(Object_t<T> const & id)
    {
        auto & H = getRegistry().get<Header>(id.m_entity);
        return H.lock;
    }

    template<typename T>
    T& resourceGet(Object_t<T> const & id)
    {
        return getRegistry().get<T>(id.m_entity);
    }
    template<typename T>
    T const & resourceGet(Object_t<T> const & id) const
    {
        return getRegistry().get<T>(id.m_entity);
    }
    template<typename R, typename T>
    bool resourceHasData(Object_t<T> const & id) const
    {
        return getRegistry().has<R>(id.m_entity);
    }

    registry_type& getRegistry()
    {
        return m_registry;
    }
    registry_type const & getRegistry() const
    {
        return m_registry;
    }




    void on_destroyHeader(registry_type &r, entity_type e )
    {
        auto & H = r.get<Header>(e);
        if( H.lock.use_count() > 0)
        {
            throw std::runtime_error("You must call resourceDestroy( ) to destroy this object.");
        }
        if( H.m_throw)
            throw std::runtime_error("You must call resourceDestroy( ) to destroy this object.");
    }
protected:
    bool _resourceDestroy(entity_type e)
    {
        auto & r = getRegistry();

        auto & H = r.get<Header>(e);

        if( H.lock.use_count() > 1)
            return false;

        auto idhash = H.typeIdHash;

        // destroy everything except CMain
        r.visit(e, [&](const auto info)
        {
            if( info.hash() != idhash && info != entt::type_id<Header>())
            {
                entity_type as_array[1]{e};
                auto storage = r.storage(info);;
                storage->remove(r, as_array, as_array+1u);
            }
        });

        if( H.name.size() )
        {
            m_nameToEntity.erase(H.name);
        }

        H.lock.reset();
        H.m_throw = false;
        getRegistry().destroy(e);

        return true;
    }
    std::unordered_map<std::string, entity_type> m_nameToEntity;
    registry_type m_registry;
};

}
}
#endif
