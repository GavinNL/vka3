#ifndef VKA_ECS_RENDER_COMPONENT2_PBRMATERIAL_H
#define VKA_ECS_RENDER_COMPONENT2_PBRMATERIAL_H

#include <vka/math/linalg.h>
#include <vka/ecs/json.h>

#include <vka/ecs/ResourceObjects/HostTexture.h>

namespace vka
{
namespace ecs
{

//=====================================================================
// Set the name of the enum
#define ENUM_DEFINE    PBRDebugMode

// Set the base type
#define ENUM_BASE_TYPE int32_t

// Set the label/values
#define ENUM_DEFINE_VALUES \
    ENUM_VALUE( NONE ,           0)\
    ENUM_VALUE( BASE_COLOR,      1)\
    ENUM_VALUE( METALLIC,        2)\
    ENUM_VALUE( ROUGHNESS,       3)\
    ENUM_VALUE( NORMAL_SAMPLER,  4)\
    ENUM_VALUE( NORMAL,          5)\
    ENUM_VALUE( TANGENT,         6)\
    ENUM_VALUE( BITANGENT,       7)\
    ENUM_VALUE( OCCLUSION,       8)\
    ENUM_VALUE( F0,              9)\
    ENUM_VALUE( EMISSIVE,       10)\
    ENUM_VALUE( SPECULAR,       11)\
    ENUM_VALUE( DIFFUSE,        12)\
    ENUM_VALUE( THICKNESS,      13)\
    ENUM_VALUE( CLEARCOAT,      14)\
    ENUM_VALUE( SHEEN,          15)\
    ENUM_VALUE( ALPHA,          16)\
    ENUM_VALUE( SUBSURFACE,     17)\
    ENUM_VALUE( TRANSMISSION,   18)\
    ENUM_VALUE( LAMBERTIAN,     19)\
    ENUM_VALUE( GGX,            20)\
    ENUM_VALUE( BRDF,           21)

// include this inline header to generate the relection
// classes/functions
#include "vka/ecs/enum_reflect.inl"
//=====================================================================

struct PBRMaterial
{
    static constexpr auto _type    = "resource";
    static constexpr auto _subType = "PBRMaterial";

    // Vertex modifications
    float vertexOffsetDistance = 0.0f;

    // Fragment level data
    vec4        baseColorFactor          =  vec4(1,1,1,1);
    vec3        emissiveFactor           =  vec3(0,0,0);
    float       metallicFactor           =  0.0f         ;
    float       roughnessFactor          =  1.0f         ;

    Texture2D_ID baseColorTexture        ;
    Texture2D_ID metallicRoughnessTexture;
    Texture2D_ID normalTexture           ;
    Texture2D_ID occlusionTexture        ;
    Texture2D_ID emissiveTexture         ;

    float normalScale = 1.0f;
    float exposure    = 1.0f;
    float alphaCutoff = 0.0f;

    // Flags
    PBRDebugMode debugMode = PBRDebugMode::NONE;
    bool         wireframe = false;
    bool         unlit     = false;


    static size_t maxTextures()
    {
        return ( offsetof(PBRMaterial, emissiveTexture) - offsetof(PBRMaterial, emissiveTexture) ) / sizeof(Texture2D_ID);
    }
    Texture2D_ID& texture(size_t i)
    {
        return (&baseColorTexture)[i];
    }
};

using PBRMaterial_ID = _ID<PBRMaterial>;

}
}

#endif
