#ifndef VKA_ECS_RESOURCEOBJECTS_PIPELINE_H
#define VKA_ECS_RESOURCEOBJECTS_PIPELINE_H

#include <map>

#include <vka/utils/uri.h>
#include <vka/ecs/ID.h>

namespace vka
{
namespace ecs
{

/**
 * @brief The PBRPipeline struct
 *
 */
struct PBRPipeline
{
    vka::uri vertexShader;
    vka::uri fragmentShader;

    std::map<std::string, std::string> compileTimeDefinitions;
};

using PBRPipeline_ID = _ID<PBRPipeline>;


}
}


#endif
