#ifndef VKA_ECS_RESOURCE_OBJECT_BASE_H
#define VKA_ECS_RESOURCE_OBJECT_BASE_H

// Standard Headers
#include <memory>

// External Headers
// #include <glm/glm.hpp>

// Project Headers
#include <vka/ecs/ID.h>


namespace vka
{
namespace ecs
{

struct SystemBus;

template<typename ResourceType>
void loadResource(std::shared_ptr<SystemBus> sb, _ID<ResourceType> id);


}
}


#endif



