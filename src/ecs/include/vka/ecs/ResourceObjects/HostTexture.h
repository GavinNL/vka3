#ifndef VKA_ECS_HOST_TEXTURE_H
#define VKA_ECS_HOST_TEXTURE_H

#include <vka/utils/HostImage3.h>
#include <vka/utils/HostCubeImage.h>

#include <vka/ecs/ID.h>
#include <memory>

namespace vka
{
namespace ecs
{

struct HostTexture2D : public vka::HostImageArray
{
    static constexpr auto _type    = "resource";
    static constexpr auto _subType = "Texture2D";

    HostTexture2D()
    {
    }
    HostTexture2D(HostImage const & I) : vka::HostImageArray(I)
    {
    }
    HostTexture2D(HostImage && I) : vka::HostImageArray(std::move(I))
    {
    }
    HostTexture2D(HostImageArray const & I) : vka::HostImageArray(I)
    {
    }
    HostTexture2D(HostImageArray && I) : vka::HostImageArray(std::move(I))
    {
    }
};

struct HostTextureCube : public vka::HostCubeImage
{
    static constexpr auto _type    = "resource";
    static constexpr auto _subType = "TextureCube";
};

//using HostTextureCube = vka::HostCubeImage;


using TextureCube_ID  = _ID<HostTextureCube>;
using Texture2D_ID    = _ID<HostTexture2D>;


}
}

#endif
