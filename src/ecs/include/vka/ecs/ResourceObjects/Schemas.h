#ifndef VKA_ECS_SCHEMA_RESOURCES_H
#define VKA_ECS_SCHEMA_RESOURCES_H

#include <vka/ecs/JSchema/SchemaBase.h>
#include <vka/ecs/ResourceObjects/Environment.h>
#include <vka/ecs/ResourceObjects/GltfScene.h>
#include <vka/ecs/ResourceObjects/PBRMaterial.h>
#include <vka/ecs/ResourceObjects/HostTexture.h>

namespace vka
{
namespace ecs
{

template<>
inline json toSchema<MeshPrimitive_ID>(MeshPrimitive_ID const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";
    auto & M = s->getResourceManager<HostMeshPrimitive>();

    M.each([&M, &J](auto e)
    {
        J["enum"].push_back( M.getResourceName(e) );
    });

    if( C.valid())
        J["default"] = M.getResourceName(C);
    return J;
    (void)s;
}

template<>
inline json toSchema<PBRMaterial_ID>(PBRMaterial_ID const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";
    auto & M = s->getResourceManager<PBRMaterial>();

    M.each([&M, &J](auto e)
    {
        J["enum"].push_back( M.getResourceName(e) );
    });

    if( C.valid())
        J["default"] = M.getResourceName(C);
    return J;
    (void)s;
}

template<>
inline json toSchema<Scene2_ID>(Scene2_ID const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";
    auto & M = s->getResourceManager<GLTFScene2>();

    M.each([&M, &J](auto e)
    {
        J["enum"].push_back( M.getResourceName(e) );
    });

    if( C.valid())
        J["default"] = M.getResourceName(C);
    return J;
    (void)s;
}

template<>
inline json toSchema<Environment_ID>(Environment_ID const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";
    auto & M = s->getResourceManager<Environment>();

    M.each([&M, &J](auto e)
    {
        J["enum"].push_back( M.getResourceName(e) );
    });

    if( C.valid())
        J["default"] = M.getResourceName(C);
    return J;
    (void)s;
}

template<>
inline json toSchema<Texture2D_ID>(Texture2D_ID const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";
    auto & M = s->getResourceManager<HostTexture2D>();

    M.each([&M, &J](auto e)
    {
        J["enum"].push_back( M.getResourceName(e) );
    });

    if( C.valid())
        J["default"] = M.getResourceName(C);
    return J;
    (void)s;
}

template<>
inline json toSchema<TextureCube_ID>(TextureCube_ID const & C, std::shared_ptr<SystemBus> s)
{
    json J;
    J["type"] = "string";
    auto & M = s->getResourceManager<HostTextureCube>();

    M.each([&M, &J](auto e)
    {
        J["enum"].push_back( M.getResourceName(e) );
    });

    if( C.valid())
        J["default"] = M.getResourceName(C);
    return J;
    (void)s;
}

template<>
inline void fromJson<MeshPrimitive_ID>(MeshPrimitive_ID & value, json const & J, std::shared_ptr<SystemBus> S)
{
    value = S->getResourceId<HostMeshPrimitive>(J.get<std::string>());
}
template<>
inline void fromJson<Texture2D_ID>(Texture2D_ID & value, json const & J, std::shared_ptr<SystemBus> S)
{
    value = S->getResourceId<HostTexture2D>(J.get<std::string>());
}
template<>
inline void fromJson<TextureCube_ID>(TextureCube_ID & value, json const & J, std::shared_ptr<SystemBus> S)
{
    value = S->getResourceId<HostTextureCube>(J.get<std::string>());
}
template<>
inline void fromJson<Scene2_ID>(Scene2_ID & value, json const & J, std::shared_ptr<SystemBus> S)
{
    value = S->getResourceId<GLTFScene2>(J.get<std::string>());
}
template<>
inline void fromJson<Environment_ID>(Environment_ID & value, json const & J, std::shared_ptr<SystemBus> S)
{
    value = S->getResourceId<Environment>(J.get<std::string>());
}
template<>
inline void fromJson<PBRMaterial_ID>(PBRMaterial_ID & value, json const & J, std::shared_ptr<SystemBus> S)
{
    value = S->getResourceId<PBRMaterial>(J.get<std::string>());
}


SCHEMA_RESOURCE_BEGIN(Environment)
    SCHEMA_RESOURCE_PROPERTY(radiance)
    SCHEMA_RESOURCE_PROPERTY(irradiance)
    SCHEMA_RESOURCE_PROPERTY(skybox)
SCHEMA_RESOURCE_END


SCHEMA_RESOURCE_BEGIN(PBRMaterial)
    SCHEMA_RESOURCE_PROPERTY(vertexOffsetDistance)
    SCHEMA_RESOURCE_PROPERTY(baseColorFactor)
    SCHEMA_RESOURCE_PROPERTY(emissiveFactor)
    SCHEMA_RESOURCE_PROPERTY(metallicFactor)
    SCHEMA_RESOURCE_PROPERTY(roughnessFactor)

    SCHEMA_RESOURCE_PROPERTY(baseColorTexture)
    SCHEMA_RESOURCE_PROPERTY(normalTexture)
    SCHEMA_RESOURCE_PROPERTY(metallicRoughnessTexture)
    SCHEMA_RESOURCE_PROPERTY(occlusionTexture)
    SCHEMA_RESOURCE_PROPERTY(emissiveTexture)

    SCHEMA_RESOURCE_PROPERTY(normalScale)
    SCHEMA_RESOURCE_PROPERTY(exposure)
    SCHEMA_RESOURCE_PROPERTY(alphaCutoff)
    SCHEMA_RESOURCE_PROPERTY(wireframe)
    SCHEMA_RESOURCE_PROPERTY(unlit)

SCHEMA_RESOURCE_END


SCHEMA_RESOURCE_BEGIN(HostMeshPrimitive)
SCHEMA_RESOURCE_END

SCHEMA_RESOURCE_BEGIN(GLTFScene2)
SCHEMA_RESOURCE_END

}
}
#endif
