#ifndef VKA_ECS_RENDER_COMPONENT2_RENDERABLE_GLTF_SCENE_PRIMITIVE_H
#define VKA_ECS_RENDER_COMPONENT2_RENDERABLE_GLTF_SCENE_PRIMITIVE_H

#include <vka/math/transform.h>

#include <memory>
#include <vector>
#include <map>



#include <vka/ecs/ID.h>
#include <vka/ecs/ResourceObjects/HostPrimitive.h>

#include <vka/utils/Scene.h>

#include "PBRMaterial.h"

namespace vka
{

namespace ecs
{

struct GLTFScene2
{
    static constexpr auto _type    = "resource";
    static constexpr auto _subType = "GLTFScene";

    struct Primitive
    {
        MeshPrimitive_ID mesh;
        PBRMaterial_ID   material;
        uint32_t         subMeshIndex; // basically a drawCall within the MeshPrimitive
    };

    struct Mesh
    {
        std::vector<Primitive> primitives;
    };

    std::string                            name;
    std::vector<SceneBase::Node>           nodes;
    std::vector<Mesh>                      meshes;

    std::vector<SceneBase::Animation>      animations;
    std::vector<SceneBase::Skin>           skins;
    std::vector<SceneBase::Sc >            scenes; // root nodes per scene

    std::vector< PBRMaterial_ID     >      materials;

    //std::vector< vka::transform > transforms;
    //std::vector< glm::mat4      > matrices;

    GLTFScene2()
    {}

    std::vector<vka::Transform> getNodeSpaceTransforms() const
    {
        std::vector<vka::Transform> out;
        for(auto & n : nodes)
            out.push_back(n.transform);
        return out;
    }
    std::vector<glm::mat4> getModelSpaceMatrices(std::vector<vka::Transform> const& pose) const
    {
        std::vector<glm::mat4> out(pose.size());

        struct R
        {
            static void _recurse( GLTFScene2 const & S,
                                  uint32_t  node,
                                  glm::mat4 nodeParentTransform,
                                  vk::ArrayProxy<vka::Transform const> nodeSpaceTransforms,
                                  vk::ArrayProxy<glm::mat4> nodeModelSpaceMatrices)
            {
                nodeParentTransform = nodeParentTransform * nodeSpaceTransforms.data()[node].getMatrix();
                nodeModelSpaceMatrices.data()[node] = nodeParentTransform;

                for(auto c : S.nodes[node].children)
                {
                    _recurse( S, c,
                              nodeParentTransform,
                              nodeSpaceTransforms,
                              nodeModelSpaceMatrices);
                }
            }
        };

        for(auto & s : scenes)
        {
            for(auto r : s.rootNodes)
            {
                R::_recurse( *this, r, glm::mat4(1.0f), pose, out);
            }
        }
        return out;
    }
};


using Scene2_ID    = _ID<GLTFScene2>;
}
}

#endif
