#ifndef VKA_ECS_HOST_MESH_PRIMITIVE_H
#define VKA_ECS_HOST_MESH_PRIMITIVE_H

#include <vka/utils/HostTriMesh.h>
#include <vka/ecs/ID.h>
#include <memory>

namespace vka
{
namespace ecs
{

struct HostMeshPrimitive : public vka::HostTriPrimitive
{
    static constexpr auto _type    = "resource";
    static constexpr auto _subType = "MeshPrimitive";


    HostMeshPrimitive()
    {
    }
    HostMeshPrimitive(HostTriPrimitive const & I) : vka::HostTriPrimitive(I)
    {
    }
    HostMeshPrimitive(HostTriPrimitive && I) : vka::HostTriPrimitive(std::move(I))
    {
    }



    static HostMeshPrimitive Grid(int length, int width, int dl=1, int dw=1, int majorL=5, int majorW=5, float lscale=1.0f, float wscale=1.0f)
    {
        return HostTriPrimitive::Grid(length, width, dl, dw,majorL,majorW,lscale,wscale);
    }

    static HostMeshPrimitive Box(glm::vec3 const & halfExtents)
    {
        return HostTriPrimitive::Box(halfExtents);
    }
    static HostMeshPrimitive Box(float dx , float dy , float dz )
    {
        return HostTriPrimitive::Box(dx,dy,dz);
    }

    static HostMeshPrimitive Sphere(float radius , uint32_t rings=20, uint32_t sectors=20)
    {
        return HostTriPrimitive::Sphere(radius, rings, sectors);
    }
    static HostMeshPrimitive Cylinder(float R=1.0f, float H=3.0f, uint32_t rSegments=16)
    {
        return HostTriPrimitive::Cylinder(R,H,rSegments);
    }

    static HostMeshPrimitive Arrow(float headLength, float headWidth,
                                  float bodyLength, float bodyWidth,
                                  int axis, //1 - x axis, 2-y axis, 3 -z axis
                                  int revolutionSegements)
    {
        return HostTriPrimitive::Arrow(headLength, headWidth, bodyLength, bodyWidth, axis, revolutionSegements);
    }


};

using MeshPrimitive_ID  = _ID<HostMeshPrimitive>;

}
}

#endif
