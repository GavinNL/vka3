#ifndef VKA_ECS_RENDER_RESOURCE_ENVIRONMENT_H
#define VKA_ECS_RENDER_RESOURCE_ENVIRONMENT_H

#include "HostTexture.h"

namespace vka
{
namespace ecs
{

struct Environment
{
    static constexpr auto _type    = "resource";
    static constexpr auto _subType = "Environment";

    TextureCube_ID radiance;
    TextureCube_ID irradiance;
    TextureCube_ID skybox;

    Environment()
    {

    }
    Environment( TextureCube_ID  rad, TextureCube_ID  irr) : radiance(rad), irradiance(irr), skybox(radiance)
    {

    }
    Environment( TextureCube_ID  rad, TextureCube_ID  irr, TextureCube_ID sky) : radiance(rad), irradiance(irr), skybox(sky)
    {

    }
};

using Environment_ID  = _ID<Environment>;
}
}

#endif
