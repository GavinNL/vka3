#ifndef VKA_ECS_ENGINE_H
#define VKA_ECS_ENGINE_H

#include <vka/ecs/LightSystem.h>
#include <vka/ecs/TransformSystem.h>
#include <vka/ecs/SceneSystem.h>
#include <vka/ecs/RenderSystem3.h>
#include <vka/ecs/ControlSystem.h>
#include <vka/ecs/PhysicsSystem2.h>
#include <vka/ecs/EventSystem.h>
#include <vka/ecs/AnimatorSystem2.h>
#include <vka/ecs/PhysicsSystem2.h>
#include <vka/ecs/ResourceManagementSystem.h>
#include <vka/ecs/Components/CCollider.h>
#include <vka/ecs/Components/CRigidBody.h>
#include <vka/ecs/Prefab/RotationGizmo.h>
#include <vka/ecs/Prefab/TranslationGizmo.h>

namespace vka
{
namespace ecs
{

/**
 * @brief The Engine struct
 *
 * This is mostly a helper container that sets up
 * all the various systems in the order they should
 * be initialized in.
 */
struct Engine
{
    Engine()
    {
        m_SystemBus = std::make_shared<vka::ecs::SystemBus>();
    }

    void initializeSystems()
    {
        m_SystemBus->getOrCreateSystem2<vka::ecs::SceneSystem>();

        m_SystemBus->getOrCreateSystem2<vka::ecs::PhysicsSystem2>();
        m_SystemBus->getOrCreateSystem2<vka::ecs::ControlSystem>();
        m_SystemBus->getOrCreateSystem2<vka::ecs::AnimatorSystem2>();
        m_SystemBus->getOrCreateSystem2<vka::ecs::EventSystem>();
        m_SystemBus->getOrCreateSystem2<vka::ecs::LightSystem>();
        m_SystemBus->getOrCreateSystem2<vka::ecs::TransformSystem>();
        m_SystemBus->getOrCreateSystem2<vka::ecs::RenderSystem3>();
        m_SystemBus->getOrCreateSystem2<vka::ecs::ResourceManagementSystem>();


        m_SystemBus->registerPrefab("prefab:gizmo/translation",
                                       [](SystemBus & S)
        {
            return createTranslationGizmo(S);
        });
        m_SystemBus->registerPrefab("prefab:gizmo/rotation",
                                       [](SystemBus & S)
        {
            return createRotationGizmo(S);
        });

    }
    std::shared_ptr<SystemBus> m_SystemBus;

};


}
}

#endif
