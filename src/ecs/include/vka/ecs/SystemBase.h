#ifndef VKA_ECS_SYSTEM_BASE_H
#define VKA_ECS_SYSTEM_BASE_H


#include <memory>
#include <vector>
#include <mutex>
#include <chrono>

#include "ECS.h"
#include "SystemBus.h"
#include <fmt/format.h>
#include <spdlog/spdlog.h>
namespace vka
{
namespace ecs
{

#define S_TRACE(...) spdlog::trace( __VA_ARGS__ ) //if(m_trace ) m_trace( fmt::format(__VA_ARGS__) )
#define S_INFO(...)  spdlog::info( __VA_ARGS__ ) //if(m_info  ) m_info ( fmt::format(__VA_ARGS__) )
#define S_DEBUG(...) spdlog::debug( __VA_ARGS__ ) //if(false && m_debug ) m_debug( fmt::format(__VA_ARGS__) )
#define S_WARN(...)  spdlog::warn( __VA_ARGS__ ) //if(m_warn  ) m_warn ( fmt::format(__VA_ARGS__) )
#define S_ERROR(...) spdlog::error( __VA_ARGS__ ) //if(m_error ) m_error( fmt::format(__VA_ARGS__) )
#define S_CRIT(...)  spdlog::crit( __VA_ARGS__ ) //if(m_critical  ) m_crit ( fmt::format(__VA_ARGS__) )

// #define S_TRACE(...) if(m_trace ) m_trace( fmt::format(__VA_ARGS__) )
// #define S_INFO(...)  if(m_info  ) m_info ( fmt::format(__VA_ARGS__) )
// #define S_DEBUG(...) if(false && m_debug ) m_debug( fmt::format(__VA_ARGS__) )
// #define S_WARN(...)  if(m_warn  ) m_warn ( fmt::format(__VA_ARGS__) )
// #define S_ERROR(...) if(m_error ) m_error( fmt::format(__VA_ARGS__) )
// #define S_CRIT(...)  if(m_critical  ) m_crit ( fmt::format(__VA_ARGS__) )

struct loggers
{
    std::function<void(std::string const&)> m_trace;
    std::function<void(std::string const&)> m_info;
    std::function<void(std::string const&)> m_debug;
    std::function<void(std::string const&)> m_warn;
    std::function<void(std::string const&)> m_error;
    std::function<void(std::string const&)> m_critical;

    loggers()
    {
        #define MSs_ std::chrono::duration<double>( std::chrono::system_clock::now() - m_startTime ).count()

        m_trace = [&](std::string const& s) { std::cout << fmt::format("{} [trace]: {}\n", MSs_, s); };
        m_info  = [&](std::string const& s) { std::cout << fmt::format("{} [info] : {}\n", MSs_, s); };
        m_debug = [&](std::string const& s) { std::cout << fmt::format("{} [debug]: {}\n", MSs_, s); };
        m_warn  = [&](std::string const& s) { std::cout << fmt::format("{} [warn] : {}\n", MSs_, s); };
        m_error = [&](std::string const& s) { std::cout << fmt::format("{} [error]: {}\n", MSs_, s); };
        m_critical  = [&](std::string const& s) { std::cout << fmt::format("{} [crit] : {}\n", MSs_, s); };
    }

protected:
    std::chrono::system_clock::time_point m_startTime = std::chrono::system_clock::now();
};

}
}

#endif
