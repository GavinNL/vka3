#ifndef VKA_ECS_ID_H
#define VKA_ECS_ID_H

#include <string>
#include <vector>
#include <map>
#include <queue>
#include <stdexcept>
#include <limits>
#include <memory>

namespace vka
{
namespace ecs
{

/**
 * @brief The _ID struct
 * Meant to be used as an index into an array or map that is non-copyable
 * between tempate paramters.
 *
 * eg: _ID<0> x = _ID<1>(3); // wil not work
 */
template<typename base_t>
struct _ID
{
    using integral_type = uint32_t;
    using object_type   = base_t;
    using value_type    = integral_type;


    _ID() : id(  std::numeric_limits<integral_type>::max() )
    {
    }

    explicit _ID(integral_type index, bool x) : id(index)
    {
        (void)x;
        if( x )
            m_useCount = std::make_shared<integral_type>();
    }

    _ID( _ID const & i) : id( i.id ), m_useCount(i.m_useCount)
    {
    }

    _ID( _ID && i) : id( i.id ), m_useCount( std::move(i.m_useCount))
    {
        i.id = std::numeric_limits<integral_type>::max();
    }

    _ID& operator = ( _ID const & i)
    {
        if( &i != this)
        {
            id = i.id;
            m_useCount = i.m_useCount;
        }
        return *this;
    }

    _ID& operator = ( _ID && i)
    {
        if( &i != this)
        {
            id = i.id;
            m_useCount = std::move(i.m_useCount);
            i.id = std::numeric_limits<integral_type>::max();
        }

        return *this;
    }

    bool operator < (_ID const & other) const
    {
        return id < other.id;
    }
    bool operator > (_ID const & other) const
    {
        return id > other.id;
    }
    bool operator == (_ID const & other) const
    {
        return id == other.id;
    }
    bool operator != (_ID const & other) const
    {
        return id != other.id;
    }
    bool operator <= (_ID const & other) const
    {
        return id <= other.id;
    }
    bool operator >= (_ID const & other) const
    {
        return id >= other.id;
    }

    integral_type value() const
    {
        return id;
    }

    bool valid() const
    {
        return id!=std::numeric_limits<integral_type>::max();
    }

    size_t useCount() const
    {
        auto l=m_useCount.use_count();
        // this value is based on the ResourceManager. It keeps
        // two references per resource id.
        if( l <=2 )
            return size_t(0);
        return static_cast<size_t>(l)-2;
    }

private:
    integral_type id = std::numeric_limits<integral_type>::max();
    std::shared_ptr<integral_type> m_useCount;
};

/**
 * @brief The LockedResource struct
 *
 * A locked resource is meant to tell the sytem that
 * you have taken a resource and intend to use it,
 * so do not remove it from device memory.
 *
 * You MUST properly unlock the resource when you are done with it
 * otherwise when the destructor is called,
 */
template<typename Resource_type>
struct LockedResource
{
    size_t useCount() const
    {
        return static_cast<size_t>(m_useCount.use_count());
    }

    _ID<Resource_type> const& getID() const
    {
        return id;
    }
    protected:
    _ID<Resource_type> id;
    std::shared_ptr<size_t> m_useCount;

    friend struct SystemBus;
};

}
}
#endif
