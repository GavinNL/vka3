#ifndef VKA_ECS_PBR_SCENE_PRESENT_RENDERER_H
#define VKA_ECS_PBR_SCENE_PRESENT_RENDERER_H

#include <vka/Renderers/PBRSceneRenderer.h>
#include "HostDeviceObject.h"

namespace vka
{
namespace ecs
{

struct PBRSceneRenderer : public vka::PBRPrimitiveRenderer
{
    SceneResourceManager     * m_sceneManager;
    ImageResourceManager     * m_imageManager;
    PrimitiveResourceManager * m_primitiveManager;
    MaterialResourceManager  * m_materialManager;

public:
    std::map<materialId, int32_t>                            m_stagedMaterials;

    void clearStagedMaterials()
    {
        m_stagedMaterials.clear();
        PBRSceneRenderer::resetMaterials();
        Material_t defaultMaterial;
        auto index = PBRSceneRenderer::pushMaterial(defaultMaterial);
        assert(index==0);
        (void)index;
    }

    /**
     * @brief drawPrimitive
     * @param p
     * @param m
     * @param t
     *
     * Draw a single primitive with a material
     * using a single model-transform.
     *
     * The drawcall used is the drawcall of the primitive. ie the full
     * set of all triangles. If there are multiple sub objects within
     * the primitive, they will all be drawn.
     *
     * The matrixIndex is the index into the matrix storage array
     * where this primitive is.
     *
     * use:
     *
     * // Call once per frame:
     * setCameraMatrices( view, projection);
     *
     *
     * auto i0 = pushMatrix(modelMatrix)
     * setModelMatrixIndex(i0)
     *
     * auto i1 = pushMatrix( primitiveMatrix1 );
     * drawPrimitive(p1, m1, i1);
     *
     * auto i2 = pushMatrix( primitiveMatrix2 );
     * drawPrimitive(p2, m2, i2);
     *
     *
     * Final world-space of the two primitives are:
     *
     * modelMatrix * primitiveMatrix1
     * modelMatrix * primitiveMatrix2
     *
     */
    void bindPrimitive(primitiveId p)
    {
        auto & pr = m_primitiveManager->at(p)->device;
        bindDeviceMeshPrimitive(pr);
    }

    //===================================================================
    // Matrix Uploading methods.
    //
    // There are 3 types of matrices in this scene renderer
    // Model, Node and Bone matrices
    // these must be pushed in the following order: model, node, bone
    //
    //
    //  eg:
    //   pushModelMatrix( SomePlaceInTheWorld )
    //       pushNodeMatrix( Mat_translate_X)
    //           bindMaterial(m);
    //           bindPrimitive(p);
    //           drawPrimitive(p);
    //       pushNodeMatrix( Mat_translate_Y)
    //           bindMaterial(m);
    //           bindPrimitive(p);
    //           enableBones(10);
    //           for(int b=0;b<10;b++)
    //               pushBoneMatrix( bone[b] )
    //           drawPrimitive(p);
    //           disableBones();
    //===================================================================
    void pushModelMatrix(glm::mat4 const & m)
    {
        auto i = pushMatrix(m);
        setModelMatrixIndex( static_cast<int32_t>(i) );
    }
    void pushNodeMatrix(glm::mat4 const & m)
    {
        auto i = pushMatrix(m);
        setNodeMatrixIndex( static_cast<int32_t>(i) );
    }
    //===================================================================
    void bindMaterial(materialId p)
    {
        auto f = m_stagedMaterials.find(p);
        int32_t materialIndex=0;
        if( f != m_stagedMaterials.end())
        {
            materialIndex = m_stagedMaterials.at(p);
        }
        PBRSceneRenderer::setMaterialIndex(materialIndex);
    }
    // draw primitive p using the last material
    void drawPrimitive(primitiveId p)
    {
        auto & pr = m_primitiveManager->at(p)->device;

        bindDeviceMeshPrimitive(pr);
        drawIndexed(pr.drawCall().indexCount,1,pr.drawCall().firstIndex,pr.drawCall().vertexOffset,0);
    }

    void drawPrimitive(PrimitiveDrawCall const & drawCall)
    {
        drawIndexed(drawCall.indexCount,
                    1, // number of instances
                    drawCall.firstIndex,
                    drawCall.vertexOffset,
                    0);
    }
    void drawPrimitive(SceneBase::DrawCall const & drawCall)
    {
        drawIndexed(drawCall.indexCount,
                    1, // number of instances
                    drawCall.firstIndex,
                    drawCall.vertexOffset,
                    0);
    }


    void drawScene( sceneId s,
                    glm::mat4                 modelToWorldTransform,
                    vk::ArrayProxy<glm::mat4> modelSpaceNodeTransforms,
                    uint32_t sceneIndex = 0
                   )
    {
        auto & S = m_sceneManager->at(s);

        pushModelMatrix(modelToWorldTransform);
        auto rS = S->scenes[sceneIndex];
        for(auto r : rS.rootNodes )
        {
            _drawNodeT( *S, r, modelSpaceNodeTransforms );
        }
    }
    void drawScene( HostDeviceScene const & S,
                    glm::mat4                 modelToWorldTransform,
                    vk::ArrayProxy<glm::mat4> modelSpaceNodeTransforms,
                    uint32_t sceneIndex = 0
                   )
    {
        pushModelMatrix(modelToWorldTransform);

        auto rS = S.scenes[sceneIndex];
        for(auto r : rS.rootNodes )
        {
            _drawNodeT( S, r, modelSpaceNodeTransforms );
        }
    }
    void _drawNodeT( HostDeviceScene const & S,
                    uint32_t       nodeIndex,
                    vk::ArrayProxy<glm::mat4> modelSpaceNodeTransforms)
    {
        auto & N = S.nodes[nodeIndex];

        if( N.meshIndex )
        {
            // Upload the node matrix which
            // all primitives of this mesh will use
            pushNodeMatrix( modelSpaceNodeTransforms.data()[nodeIndex] );

            if( N.skinIndex )
            {
                auto & sk = S.skins.at(*N.skinIndex);
                //enableBones(sk.inverseBindMatrices.size());
                int32_t firstBoneMatrixIndex = -1;
                uint32_t b=0;
                for(auto j : sk.joints)
                {
                    auto kk = pushMatrix( modelSpaceNodeTransforms.data()[j] * sk.inverseBindMatrices[b++]);
                    if( firstBoneMatrixIndex == -1)
                        firstBoneMatrixIndex = static_cast<int32_t>( kk );
                }
                setFirstBoneMatrixIndex(firstBoneMatrixIndex);
            }

            // draw the mesh with the current model transformation.
            _drawMeshT( S, *N.meshIndex);
        }

        for(auto i : N.children)
        {
            _drawNodeT(S, i, modelSpaceNodeTransforms);
        }
    }

    void _drawMeshT( HostDeviceScene const & M,
                     uint32_t   meshIndex)
    {
        // for each of the primitives
        for(auto & mP : M.meshes[meshIndex].meshPrimitives)
        {
            auto & p = M.primitives[ mP.primitiveIndex ];

            bindMaterial(M.materials2.at(mP.materialIndex));
            bindPrimitive( M.rawPrimitives[p.rawPrimitiveIndex] );
            drawPrimitive( p.drawCall );

        }
    }

protected:


};

}
}
#endif


