#pragma once

#ifndef VKA_INPUT_SYSTEM_EVENTS_H
#define VKA_INPUT_SYSTEM_EVENTS_H

#include <cstdint>
#include <chrono>

#include <vka/KeyCode.h>

namespace vka
{

struct EvtInput
{

};

struct EvtInputQuit
{

};

struct EvtInputFileDrop
{
    std::string path;
};

struct EvtInputKey
{
    std::chrono::time_point<std::chrono::system_clock> timestamp;   /**< In milliseconds, populated using SDL_GetTicks() */
    bool        down;
    uint8_t     repeat;       /**< Non-zero if this is a key repeat */
    KeyCode     keycode;
    //KeyScanCode scancode;

    uint32_t    windowKeyCode;  // The keycode produced by the window manager (qt or sdl)
    uint32_t    windowScanCode; // The scancode produced by the window manager (qt or SDL)

    //SDL_Keysym keysym;  /**< The key that was pressed or released */
    void const * windowEvent; // a pointer to the envent struct produced by the window manager
};

struct EvtInputMouseButton
{
    std::chrono::time_point<std::chrono::system_clock> timestamp;
    //uint32_t type;        /**< ::SDL_MOUSEBUTTONDOWN or ::SDL_MOUSEBUTTONUP */
    //uint32_t timestamp;   /**< In milliseconds, populated using SDL_GetTicks() */
    //uint32_t windowID;    /**< The window with mouse focus, if any */
    //uint32_t which;       /**< The mouse instance id, or SDL_TOUCH_MOUSEID */
    MouseButton  button;       /**< The mouse button index */
    uint8_t  state;        /**< ::SDL_PRESSED or ::SDL_RELEASED */
    uint8_t  clicks;       /**< 1 for single-click, 2 for double-click, etc. */
    int32_t  x;           /**< X coordinate, relative to window */
    int32_t  y;           /**< Y coordinate, relative to window */
};

struct EvtInputMouseMotion
{
    std::chrono::time_point<std::chrono::system_clock> timestamp;

    //uint32_t which;       /**< The mouse instance id, or SDL_TOUCH_MOUSEID */
    //uint32_t state;       /**< The current button state */

    int32_t x;           /**< X coordinate, relative to window */
    int32_t y;           /**< Y coordinate, relative to window */
    int32_t xrel;        /**< The relative motion in the X direction */
    int32_t yrel;        /**< The relative motion in the Y direction */
};

struct EvtInputMouseWheel
{
    std::chrono::time_point<std::chrono::system_clock> timestamp;

    int32_t x;           /**< X coordinate, relative to window */
    int32_t y;           /**< X coordinate, relative to window */
    int32_t delta;           /**< X coordinate, relative to window */
};

}

#endif
