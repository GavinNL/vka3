#pragma once

#ifndef VKA_RESOURCE_EVENTS_H
#define VKA_RESOURCE_EVENTS_H

#include <cstdint>
#include <chrono>
#include <typeindex>
#include <any>

#include <vka/ecs/ECS.h>
#include <vka/ecs/ID.h>
namespace vka
{
namespace ecs
{

enum class ResourceEvtType
{
    ID_CREATED,            // called when the resource is first created

    URI_ADDED,
    HOST_LOADED,           // loaded from the filesystem into host memory
    HOST_UPDATED,           // loaded from the filesystem into host memory
    DEVICE_LOADED,         // loaded from host memory into GPU memory

    DEVICE_TRANSFERRING,   // (unused) scheduled transferring from host to GPU

    HOST_UNLOADED,         // unloaded from the host memory
    DEVICE_UNLOADED,        // unloaded from gpu memory

    DEVICE_INACTIVE_STATE,  // the device resources is in an inactive state, meaning
                            // nothing is currently using it, and it may end up being
                            // being unloaded.

    DEVICE_WILL_UNLOAD,     // The device object has been "unloaded", but may not have been destroyed
                            // yet. The vk::image/imageView might still be available, but it
                            // will be destroyed relatively soon. So make sure to detacth them
                            // from any descriptor sets,

                            // soon. (vk::imageView/vk::Buffer/etc will no longer be valid after this)
    DEVICE_WILL_DESTROY,    // Device resource is about to be destroyed, so do something
                            // soon. (vk::imageView/vk::Buffer/etc will no longer be valid after this)

};

template<typename T>
struct EvtResourceEvent_t
{
    ResourceEvtType type;
    _ID<T>          id;
};

}
}

#endif
