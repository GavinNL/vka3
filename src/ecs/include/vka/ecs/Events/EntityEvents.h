#pragma once

#ifndef VKA_ENTITY_EVENTS_H
#define VKA_ENTITY_EVENTS_H

#include <cstdint>
#include <chrono>
#include <typeindex>

#include <vka/ecs/ECS.h>

namespace vka
{
namespace ecs
{
struct EvtEntityCreated
{
    entt::entity entity = entt::null;
};
struct EvtEntityDestroyed
{
    entt::entity entity = entt::null;
};
struct EvtParentChanged
{
    entt::entity entity = entt::null;
    entt::entity oldParent = entt::null;
    entt::entity newParent = entt::null;

    entt::entity oldRoot = entt::null;
    entt::entity newRoot = entt::null;
};


struct EvtComponentCreated
{
    entt::entity    entity;
    std::type_index componentTypeIndex;
};
}
}

#endif
