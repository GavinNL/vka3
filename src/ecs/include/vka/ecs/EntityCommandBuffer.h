#ifndef VKA_ECS_ENTITY_COMMAND_BUFFER_H
#define VKA_ECS_ENTITY_COMMAND_BUFFER_H

#include <functional>
#include <vector>

namespace vka
{
namespace ecs
{

/**
 * @brief The EntityCommandBuffer struct
 *
 * The Entity Command Buffer is used to make structural
 * changes to the entities or components in the system.
 *
 * Commands written to the command buffer will be
 * executed at the start of the next frame and
 * are always executed on a single thread.
 */
struct EntityCommandBuffer
{
    EntityCommandBuffer()
    {
        m_toCall.reserve(40);
    }

    template<typename callable_t>
    void queueCommand(callable_t && c)
    {
        m_toCall.push_back( c );
    }

private:
    std::vector< std::function< void(void) > > m_toCall;
    friend struct SystemBus;
};

}
}

#endif
