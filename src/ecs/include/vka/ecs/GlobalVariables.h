#ifndef VKA_ECS_SYSTEM_GLOBAL_VARIABLES_H
#define VKA_ECS_SYSTEM_GLOBAL_VARIABLES_H


#include <memory>
#include <vector>
#include <map>

#include <vka/math/transform.h>
#include <vka/KeyCode.h>
#include "ECS.h"
#include "SystemBus.h"
#include <variant>

namespace vka
{
namespace ecs
{

using dynamic_type  = std::variant<  int8_t, int16_t, int32_t, int64_t,
                                 uint8_t, uint16_t, uint32_t, uint64_t,
                                glm::vec3, glm::vec2, glm::vec4, glm::quat, vka::Transform,
                               float, double, std::string>;

struct InputVariables_t
{
    struct
    {
        int x;
        int y;
        int xrel;
        int yrel;

        struct
        {
            bool LEFT  ;
            bool MIDDLE;
            bool RIGHT ;
            bool X1    ;
            bool X2    ;
        } BUTTON;

    } MOUSE;

    struct _key
    {
        bool operator[](KeyCode k) const
        {
            return _keymap.count(k) ? _keymap.at(k) : false;
        }
        std::unordered_map<KeyCode, bool> _keymap;
    };
    _key KEY;
};

struct SystemGlobalVariables
{
    //================================================================
    // Global Variables
    //================================================================

    //------------------------------------------------------
    // RenderSystem variables
    //------------------------------------------------------
    entt::entity currentCameraEntity = entt::null;
    vec2         screenSize;
    uint64_t     frameCount = 0;
    double       frameTime = 0.0;
    double       time = 0.0;

    //------------------------------------------------------
    // ControlSystem variables
    //------------------------------------------------------
    vec2                        mouseScreenPosition;
    vec2                        mouseScreenPositionDelta;
    line3                       mouseRay;
    std::map<MouseButton, bool> mouseButtonState;
    std::map<KeyCode, bool>     keyState;
    //================================================================


    InputVariables_t INPUT;

    //------------------------------------------------------
    // PhysicsSystem variables
    //------------------------------------------------------
    entt::entity mouseEntityHit = entt::null;  // which entity did the mouse hit
    glm::vec3    mouseWorldHitPosition;        // the position in world space where the mouse ray hit an object
    glm::vec3    mouseWorldHitNormal;          // the normal of the surface where the mouse hit
    //================================================================
    std::map<std::string, dynamic_type> other;
};



}
}

#endif
