#include <iostream>
#include <vka/ecs/Engine.h>
#include <fmt/format.h>

#include <vka/ecs/Components/CWSADMovement.h>
#include <vka/ecs/Components/CMouseLook.h>

#include <glslang/Public/ShaderLang.h>

// callback function for validation layers
VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;
    #define VKA_INFO(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_WARN(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_ERROR(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_DEBUG(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}

#include <vka/ecs/SDLWindowSystem.h>


void init( std::shared_ptr<vka::ecs::SystemBus> sb)
{
    using namespace vka::ecs;
    if(1)
    {
        auto E = sb->createEntity("cameraEntity");
        auto & C = E.emplaceComponent( vka::ecs::CCamera() );
        C.environment = sb->resourceCreate<vka::ecs::Environment>(vka::uri("rc:environments/green_hills.environment"));

        vka::Transform t;
        t.position = {-3,3,-3};
        t.lookat( {0,0,0}, {0,1,0});


        auto & RB = E.emplaceComponent( CRigidBody() );
        RB.isKinematic = true;
        RB.mass = 0;

        E.emplaceComponent( CPosition( t.position ));
        E.emplaceComponent( CRotation( t.rotation ));
        E.emplaceComponent( CMouseLook());
        E.emplaceComponent( CWSADMovement());
    }

    if(1)
    {
       auto sphere_id = sb->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("sphere1", vka::ecs::HostMeshPrimitive::Sphere(1.0f,32,32) );

       vka::ecs::PBRMaterial defaultMaterial;
       defaultMaterial.baseColorFactor = {0,1,0,1};
       defaultMaterial.roughnessFactor = 0;
       defaultMaterial.metallicFactor = 1.0;

       auto default_material_id   = sb->resourceCreateAndEmplace<vka::ecs::PBRMaterial>("defaultMaterial", std::move(defaultMaterial) );

       auto E      = sb->createEntity("sphereEntity3");

       E.emplaceComponent( CMesh(sphere_id, default_material_id));
       E.emplaceComponent( CPosition(glm::vec3{0,2.5,0}));
       E.emplaceComponent( CRotation());
       E.emplaceComponent( CCollider( vka::ecs::BoxCollider() ));
       E.emplaceComponent( CGhostBody());

    }
}


int main(int argc, char ** argv)
{
    (void)argc;
    (void)argv;

    // This is the main vka engine. It provides all the systems
    // except the window system. The window s ystem will be
    // configured by us.
    std::shared_ptr<vka::ecs::Engine>          m_enginePtr = std::make_shared<vka::ecs::Engine>();

    // grab the system bus so we can configure
    // everything
    auto systemBus = m_enginePtr->m_SystemBus;

    // add the paths to the resources
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");

    // initialize the engine. This will
    // add all the systems to the systembus
    // and configure them
    m_enginePtr->initializeSystems();

    // Call our init function so that
    // we can configure it for our example
    init(systemBus);

    // Finally add the window system

    // the SDLWindow system is an additional system not provided by the Engine class
    // the SDLWindow (or any other window systemn) system should not
    // be added as a main system
    std::shared_ptr<vka::ecs::SDLWindowSystem> m_window = m_enginePtr->m_SystemBus->createSystem<vka::ecs::SDLWindowSystem>();


    // put the window it its main loop
    // the Window System will be responsible for
    // handling the the main loop and all the inputs
    // to send to the systembus.
    //
    // SDL will be initialized at the start of this
    // function call and will be destroyed at the
    // end.
    m_window->mainLoop();


    // Disconnect all the systems except the
    // window system
    systemBus->disconnectAllSystems();

    m_enginePtr.reset();

    return 0;
}

