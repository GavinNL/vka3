cmake_minimum_required(VERSION 3.10)

# Find all files named unit-*.cpp
file(GLOB files "*.cpp")

foreach(file ${files})

    get_filename_component(file_basename ${file} NAME_WE)
    string(REGEX REPLACE "unit-([^$]+)\\.cpp" "\\1" exe_name ${file_basename}SDL)

    message("New File: ${file}    Exe name: ${exe_name}")

    add_executable( ${exe_name} ${file} )
    target_include_directories( ${exe_name} PRIVATE base)
    target_link_libraries( ${exe_name}
                                PUBLIC
                                    vka::ecs
                                    vka::ecsPhysicsSystem2
                                    vka::ecsLightSystem
                                    vka::ecsControlSystem
                                    vka::ecsEventSystem
                                    vka::ecsAnimatorSystem2
                                    vka::ecsRenderSystem3
                                    vka::ecsSceneSystem
                                    vka::ecsTransformSystem
                                    vka::ecsSDLWindowSystem
                                    vka::ecsResourceManagementSystem
                                    vkw::vkw
                                    vka_project_warnings)

endforeach()
