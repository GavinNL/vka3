#include <vka/ecs/SystemBus.h>
#include <vka/ecs/SystemBase.h>
#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/Components/CTransform.h>
#include <easy/profiler.h>

#include <vka/utils/HostImage.h>
#include <vka/utils/HostImageLoader.h>
#include <vka/ecs/ResourceObjects/HostTexture.h>
#include <vka/ecs/ResourceObjects/GltfScene.h>
#include <vka/ecs/ResourceObjects/PBRMaterial.h>
#include <vka/ecs/ResourceObjects/Environment.h>


#include <vka/ecs/Events/EntityEvents.h>

namespace vka
{
namespace ecs
{


//===================================================================================================

std::shared_ptr<SystemBaseInternal> SystemBus::getSystem(std::string const &sysName)
{
    for(auto & s : m_systems2)
    {
        if( std::get<2>(s)->name() == sysName)
            return std::get<2>(s);
    }
    return nullptr;
}


void SystemBus::step(double dt)
{
    EASY_BLOCK("Executing CommandBuffer"); // Begin block with default color == Amber10
    {
        auto s = m_toCall.size();
        while(s--)
        {
            m_toCall.front()();
            m_toCall.pop();
        }
    }
    EASY_END_BLOCK

    EASY_BLOCK("Deferred Calls"); // Begin block with default color == Amber10
    {
        auto now = std::chrono::system_clock::now();
        auto s = m_toCallLater.size();
        while(s--)
        {
            auto & fr = m_toCallLater.front();
            if( now > fr.time)
            {
                fr.func();
            }
            else
            {
                m_toCallLater.push( std::move(fr));
            }
            m_toCallLater.pop();
        }
    }
    EASY_END_BLOCK

    EASY_BLOCK("Updating Systems2");
    for(auto & S : m_systems2)
    {
        std::get<2>(S)->m_deltaTime = dt;

        EASY_BLOCK( std::get<1>(S) );
        std::get<2>(S)->execute();
        EASY_END_BLOCK
    }
    EASY_END_BLOCK

}

void SystemBus::buildEntity(SystemBus::entity_type e, const json &root)
{
    for(auto & s : m_systems2)
    {
        std::get<2>(s)->onBuildComponents(e,root);
    }
}

void SystemBus::disconnectAllSystems()
{
    while(m_systems2.size() )
    {
        std::get<2>(m_systems2.back() )->onStop();
        std::get<2>(m_systems2.back() )->m_systemBus = nullptr;
        m_systems2.pop_back();
    }
}
//===================================================================================================

template<typename ResourceType>
void _loadResource(std::shared_ptr<SystemBus> sb, vka::uri _uri, _ID<ResourceType> id);


SystemBus::SystemBus() : m_threadpool(4)
{
    EASY_PROFILER_ENABLE
    profiler::startListen();

    spdlog::set_level(spdlog::level::debug);
    spdlog::set_pattern("[%H:%M:%S.%e] [%n] [%^---%L---%$] [thread %t] %v");

    registry.on_construct<NameComponent>().connect<  &SystemBus::onConstructNameComponent>(*this);
    registry.on_update<   NameComponent>().connect<  &SystemBus::onReplaceNameComponent>(*this);
    registry.on_destroy<  NameComponent>().connect<  &SystemBus::onDestroyNameComponent>(*this);
    registry.on_destroy<  CMain>().connect<  &SystemBus::onDestroyCMain>(*this);


    registerResourceLoader<HostTexture2D>(
                [](std::shared_ptr<SystemBus> sb, vka::uri u)
    {
        auto & M = sb->getResourceManager<HostTexture2D>();
        auto id  = M.getIdFromUri(u);
        _loadResource<HostTexture2D>(sb, u, id);
    });

    registerResourceLoader<HostTextureCube>(
                [](std::shared_ptr<SystemBus> sb, vka::uri u)
    {
        auto & M = sb->getResourceManager<HostTextureCube>();
        auto id  = M.getIdFromUri(u);
        _loadResource<HostTextureCube>(sb, u, id);
    });

    registerResourceLoader<HostMeshPrimitive>(
                [](std::shared_ptr<SystemBus> sb, vka::uri u)
    {
        auto & M = sb->getResourceManager<HostMeshPrimitive>();
        auto id  = M.getIdFromUri(u);
        _loadResource<HostMeshPrimitive>(sb, u, id);
    });

    registerResourceLoader<Environment>(
                [](std::shared_ptr<SystemBus> sb, vka::uri u)
    {
        auto & M = sb->getResourceManager<Environment>();
        auto id  = M.getIdFromUri(u);
        _loadResource<Environment>(sb, u, id);
    });

    registerResourceLoader<PBRMaterial>(
                [](std::shared_ptr<SystemBus> sb, vka::uri u)
    {
        auto & M = sb->getResourceManager<PBRMaterial>();
        auto id  = M.getIdFromUri(u);
        _loadResource<PBRMaterial>(sb, u, id);
    });

    registerResourceLoader<GLTFScene2>(
                [](std::shared_ptr<SystemBus> sb, vka::uri u)
    {
        auto & M = sb->getResourceManager<GLTFScene2>();
        auto id  = M.getIdFromUri(u);
        _loadResource<GLTFScene2>(sb, u, id);
    });
}

SystemBus::~SystemBus()
{
    registry.on_construct<NameComponent>().disconnect<  &SystemBus::onConstructNameComponent>(*this);
    registry.on_update<   NameComponent>().disconnect<  &SystemBus::onReplaceNameComponent>(*this);
    registry.on_destroy<  NameComponent>().disconnect<  &SystemBus::onDestroyNameComponent>(*this);
    registry.on_destroy<  CMain>().disconnect<  &SystemBus::onDestroyCMain>(*this);
}

void SystemBus::onConstructNameComponent(entt::registry &r, SystemBus::entity_type e)
{
    auto & N = r.get<NameComponent>(e);
    auto name  = N.getName();

    if( m_NameToEntity.count(name))
    {
        // name already exists
        throw std::runtime_error("Name already exists");
    }

    m_NameToEntity[name] = e;
    m_EntityToName[e] = name;
}

void SystemBus::onDestroyNameComponent(entt::registry &r, SystemBus::entity_type e)
{
    auto & N = r.get<NameComponent>(e);
    m_NameToEntity.erase( N.getName() );
    m_EntityToName.erase(e);
}

void SystemBus::onReplaceNameComponent(entt::registry &r, SystemBus::entity_type e)
{
    auto & N     = r.get<NameComponent>(e);
    auto oldName = m_EntityToName[e];


    auto name = N.getName();
    if( m_NameToEntity.count(name))
    {
        // name already exists
        N = NameComponent(oldName);
    }
    else
    {
        m_NameToEntity.erase(oldName);
        m_NameToEntity[name] = e;
        m_EntityToName[e] = name;
    }
}

SystemBus::entity_type SystemBus::findEntityByName(const std::string &name) const
{
    auto f = m_NameToEntity.find(name);
    if( f == m_NameToEntity.end() ) return entt::null;
    return f->second;
}

SystemBus::entity_type SystemBus::unlinkFromParent(SystemBus::entity_type e)
{
    auto & H = registry.get<CMain>(e);
    auto oldParent = H.parent;
    auto oldRoot = H.rootEntity;
    if( H.parent != entt::null )
    {
        auto p = H.parent;
        auto & Hp = registry.get<CMain>(p);
        auto & vec = Hp.children;
        vec.erase(std::remove(vec.begin(), vec.end(), e), vec.end());
        H.parent = entt::null;
        H.rootEntity = e;
        registry.emplace_or_replace<tag_is_root>(e);

        if( H.children.size() )
        {
            _recurseSetRoot(e, e);
        }

        EvtParentChanged ee;
        ee.entity = e;
        ee.newParent = entt::null;
        ee.oldParent = oldParent;
        ee.oldRoot   = oldRoot;
        ee.newRoot   = H.rootEntity;

        triggerEvent(ee);

        return p;
    }
    if( H.children.size() )
    {
        _recurseSetRoot(e, e);
    }



    return entt::null;
}

SystemBus::entity_type SystemBus::linkToParent(SystemBus::entity_type e, SystemBus::entity_type parent)
{
    auto oldParent = unlinkFromParent(e);
    auto & H = registry.get<CMain>(e);
    auto & Hp= registry.get<CMain>(parent);

    assert( H.parent == entt::null );
    //Hp.children.push_back( e );
    //H.parent = parent;
    //H.rootEntity = Hp.rootEntity;
//    registry.remove_if_exists<tag_is_root>(e);

    auto oldRoot   = H.rootEntity;

    {
        auto & P = Hp;

        // the child is no longer a root entity, so we should remove
        // the tag
        registry.remove_if_exists<tag_is_root>(e);

        // make sure the parent entity has the tag
        registry.emplace_or_replace<tag_has_children>(parent);

        P.children.push_back(e);
        H.parent     = parent;
        H.rootEntity = P.rootEntity;
    }

    if( H.children.size() )
    {
        _recurseSetRoot(e, Hp.rootEntity);
    }

    EvtParentChanged ee;
    ee.entity = e;
    ee.newParent = parent;
    ee.oldParent = oldParent;
    ee.oldRoot      = oldRoot;
    ee.newRoot      = H.rootEntity;

    triggerEvent(ee);

    return oldParent;
}

SystemBus::entity_type SystemBus::getParent(entity_type e) const
{
     auto const & H = registry.get<CMain>(e);
     return H.parent;
}

SystemBus::entity_type SystemBus::getRoot(entity_type e) const
{
     auto const & H = registry.get<CMain>(e);
     return H.rootEntity;
}
#if 1
void SystemBus::destroyEntity(SystemBus::entity_type e)
{
    auto & r = registry;
    uint64_t id;
    std::string name;

    {
        auto & C = r.get<CMain>(e);
        while( C.children.size() )
        {
            auto ce = C.children.back();
            destroyEntity(ce);
            C.children.pop_back();
        }

        // destroy everything except CMain
        r.visit(e, [&](const auto info)
        {
            if( info != entt::type_id<CMain>() )
            {
                entity_type as_array[1]{e};
                auto storage = r.storage(info);;
                spdlog::debug("Removing component {}", info.name());
                storage->remove(r, as_array, as_array+1u);
            }
        });
        C.m_throwError = false;
        id = C.getId();
        name = C.name;
    }
    r.destroy(e);

    // remove the
    m_IdToEntity.erase( id );

    if( name.size())
    {
        m_NameToEntity.erase( name );
    }
    // remove the
    m_IdToEntity.erase( id );
    m_EntityToName.erase(e);

}
#else
void SystemBus::destroyEntity(SystemBus::entity_type e)
{
    //VKA_DEBUG("Destroying CMain {} ", to_integral(e) );

    // make sure we unlink it from its parent
    unlinkFromParent(e);

    // get a list of all the children
    std::vector<entt::entity> ent;
    _recurseGetChildren(e, ent);

    auto & r = registry;
    while(ent.size())
    {
        auto c = ent.back();

        if( r.valid(c))
        {
            auto & M = registry.get<CMain>(c);

            // first remove the name if it has been registered
            if( M.name.size())
            {
                m_NameToEntity.erase(M.name);
                M.name.clear();
            }

            // remove the
            m_IdToEntity.erase( M.getId() );;
            m_EntityToName.erase(c);

            // make sure to clear the children and parent
            M.children.clear();
            M.parent = entt::null;

            M.m_throwError = false;
            r.destroy(c);
        }
        ent.pop_back();
    }

    auto & M = registry.get<CMain>(e);
    M.m_throwError = false;
    if( M.name.size())
    {
        m_NameToEntity.erase(M.name);
        M.name.clear();
    }

    // remove the
    m_IdToEntity.erase( M.getId() );;
    m_EntityToName.erase(e);

    // make sure to clear the children and parent
    M.children.clear();
    M.parent = entt::null;
    M.rootEntity = entt::null;

    r.destroy(e);

    EvtEntityDestroyed E;
    E.entity = e;
    dispatcher.trigger(E);
}
#endif

void SystemBus::onDestroyCMain(SystemBus::registry_type &r, SystemBus::entity_type e)
{
    //VKA_DEBUG("Destroying CMain {} ", to_integral(e) );

    auto & M2 = registry.get<CMain>(e);
    if( M2.m_throwError)
        throw std::runtime_error("This component cannot be destroyed by the registry. Must call SystembBus->destroyEntity(e) ");
    (void)r;
}


std::string SystemBus::getEntityName(entity_type id) const
{
    if( m_EntityToName.count(id))
    {
        return m_EntityToName.at(id);
    }
    return "entity_" + std::to_string(registry.get<CMain>(id).id);
}

EntityRef SystemBus::createEntity(const std::string &uniqueName, entt::entity parent)
{
    return createEntity(parent, uniqueName);
}

EntityRef vka::ecs::SystemBus::createEntity( entt::entity parentEntity, std::string const & name)
{
    auto e   = registry.create();
    auto & C = registry.emplace<CMain>(e, ++m_uniqueId);
    registry.emplace<tag_is_root>(e);

    C.entity     = e;
    C.parent     = entt::null;
    C.rootEntity = e;

    if( name == "")
    {
        C.name = "entity_" + std::to_string(m_uniqueId);
    }
    else
    {
        C.name = name;
    }

    m_NameToEntity[C.name] = e;
    m_EntityToName[e     ] = C.name;

    EvtEntityCreated ee;
    ee.entity = e;
    triggerEvent(ee);

    if(parentEntity != entt::null)
        linkToParent(e, parentEntity);

    return EntityRef{ e, &registry, this};
}

EntityRef EntityRef::createChild()
{
    return systemBus->createEntity(entity);
}


static std::vector<std::string> split(std::string str, std::string token)
{
    using namespace std;
    vector<string>result;
    while(str.size())
    {
        auto index = str.find(token);
        if(index!=string::npos){
            result.push_back(str.substr(0,index));
            str = str.substr(index+token.size());
            if(str.size()==0)result.push_back(str);
        }else{
            result.push_back(str);
            str = "";
        }
    }
    return result;
}
static nlohmann::json const * getRef( nlohmann::json const & rootDocument, std::string ref )
{
    // "$ref": "#/$defs/enabledToggle"
    auto path = split(ref, "/");
    if( path.size() < 2 ) return nullptr;

    nlohmann::json const * p = &rootDocument;
    for(size_t i=1;i<path.size();i++)
    {
        if( p->count(path[i]) ==0) return nullptr;
        p = &p->at( path[i] );
    }
    return p;
}



/**
 * @brief dereference
 * @param obj
 * @param rootDocument
 * @return
 *
 * Given a json object
 */
static void dereference(  nlohmann::json & obj, nlohmann::json const& rootDocument)
{
    if( obj.count("$ref"))
    {
        auto p = getRef( rootDocument, obj.at("$ref").get<std::string>() );

        if( p != nullptr) // we found the reference
        {
            // make a copy of the original object, but remove the "$ref" key
            nlohmann::json oldValue = obj;
            oldValue.erase("$ref");

            // copy the ref object over
            obj = *p;

            obj.merge_patch( oldValue );
        }
    }
    if( obj.is_object())
    {
        // now dereference any keys for hti sobject
        for(auto it = obj.begin(); it!=obj.end(); it++)
        {
            //std::cout << "Dereferenceing: " << it.key() << std::endl;
            dereference(it.value(), rootDocument);
        }
    }
    if( obj.is_array() )
    {
        for(auto & v : obj )
        {
            dereference(v, rootDocument);
        }
    }
}

static EntityRef _jsonCreate2(vka::ecs::SystemBus & S, json const & root, vka::ecs::SystemBus::entity_type parent)
{
    auto & J = root;
    if( J.count("detached") )
    {
        if( J.at("detached").get<bool>())
        {
            parent = entt::null;
        }
    }
    auto e = J.count("name") ? S.createEntity(J.at("name").get<std::string>(), parent) : S.createEntity(parent);

    S.buildEntity( e.entity, J);

    if( J.count("children"))
    {
        for(auto & C : J.at("children"))
        {
            _jsonCreate2(S, C, e.entity);
        }
    }

    return e;
}


void vka::ecs::SystemBus::buildFromJson(entity_type e, json const &root)
{
    auto J = root;

    dereference(J, root);

    for(auto & sy : m_systems2)
    {
        std::get<2>(sy)->onBuildComponents(e,J);
    }

    if( J.count("children"))
    {
        for(auto & C : J.at("children"))
        {
            _jsonCreate2(*this, C, e);
        }
    }
}



glm::mat4 SystemBus::calculateWorldSpaceMatrix(entity_type ec) const
{
    auto & r = registry;
    return traverseParents(ec,
                      [&r](entt::entity e, glm::mat4 childTransform)
    {
        glm::mat4 myTransform(1.0f);
        if( r.has<CScale>(e) )
        {
            auto & sc = r.get<CScale>(e);
            myTransform = glm::scale(myTransform, sc);
        }
        if( r.has<CRotation>(e) )
        {
            auto & rot = r.get<CRotation>(e);
            myTransform = glm::mat4_cast(rot) * myTransform;
        }
        if( r.has<CPosition>(e) )
        {
            auto & pos = r.get<CPosition>(e);
            myTransform = glm::translate( glm::mat4(1.0f), pos ) * myTransform;
        }
        return myTransform * childTransform;
    }, glm::mat4(1.0f));
}

glm::quat SystemBus::calculateWorldSpaceRotation(entity_type ec) const
{
    auto & r = registry;
    return traverseParents(ec,
                      [&r](entt::entity e, glm::quat childQuat)
    {
        if( r.has<CRotation>(e) )
        {
            auto & rot = r.get<CRotation>(e);
            return rot * childQuat;
        }
        else
        {
            return childQuat;
        }
    }, glm::quat(1,0,0,0));
}

vka::Transform SystemBus::calculateWorldSpaceTransform(entity_type ec) const
{
     auto & r = registry;
     return
     traverseParents(ec,
                     [&r](entt::entity e, vka::Transform parentTransform)
     {
         vka::Transform myTransform;
         if( r.has<CScale>(e) )
         {
             myTransform.scale = r.get<CScale>(e);
         }
         if( r.has<CRotation>(e) )
         {
             myTransform.rotation = r.get<CRotation>(e);
         }
         if( r.has<CPosition>(e) )
         {
             myTransform.position = r.get<CPosition>(e);
         }
         return myTransform * parentTransform;
     }, vka::Transform() );  
}


vka::HostImage loadSingleImage(std::string const& p)
{
    return vka::loadHostImage(p);
}





}
}


#define STB_IMAGE_IMPLEMENTATION
#define VKA_IMAGE_IMPLEMENTATION

#define VKAI_NO_STDIO
#define STBI_NO_STDIO
#define VKA_IMAGE_STATIC

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wunused-function"

#include <vka/utils/vka_image.h>

#pragma GCC diagnostic pop
