#include <vka/ecs/SystemBus.h>
#include <easy/profiler.h>

#include <vka/ecs/ResourceObjects/HostTexture.h>
#include <vka/ecs/ResourceObjects/GltfScene.h>
#include <vka/ecs/ResourceObjects/PBRMaterial.h>
#include <vka/ecs/ResourceObjects/Environment.h>
#include <vka/ecs/ResourceObjects/HostPrimitive.h>

#include <vka/ecs/Loaders/TextureLoaderGeneric.h>
#include <vka/ecs/Loaders/GLTFLoader.h>
#include <vka/ecs/Loaders/MaterialLoader.h>
#include <vka/ecs/Loaders/EnvironmentLoader.h>

#if 1
#include <spdlog/spdlog.h>

#define SB_TRACE(...) spdlog::trace( __VA_ARGS__ ) //if(m_trace ) m_trace( fmt::format(__VA_ARGS__) )
#define SB_INFO(...)  spdlog::info( __VA_ARGS__ ) //if(m_info  ) m_info ( fmt::format(__VA_ARGS__) )
#define SB_DEBUG(...) spdlog::debug( __VA_ARGS__ ) //if(false && m_debug ) m_debug( fmt::format(__VA_ARGS__) )
#define SB_WARN(...)  spdlog::warn( __VA_ARGS__ ) //if(m_warn  ) m_warn ( fmt::format(__VA_ARGS__) )
#define SB_ERROR(...) spdlog::error( __VA_ARGS__ ) //if(m_error ) m_error( fmt::format(__VA_ARGS__) )
#define SB_CRIT(...)  spdlog::crit( __VA_ARGS__ ) //if(m_critical  ) m_crit ( fmt::format(__VA_ARGS__) )
#else
#define SB_TRACE(...)
#define SB_INFO(...)
#define SB_DEBUG(...)
#define SB_WARN(...)
#define SB_ERROR(...)
#define SB_CRIT(...)

#endif

namespace vka
{
namespace ecs
{

template<typename ResourceType>
void _loadResource(std::shared_ptr<SystemBus> sb, vka::uri _uri, _ID<ResourceType> id);

template<>
void _loadResource<HostMeshPrimitive>(std::shared_ptr<SystemBus> sb, vka::uri _uri, MeshPrimitive_ID id)
{
   // HostTriPrimitives are never loaded from the filesystem
   (void)sb;
   (void)_uri;
   (void)id;
   return;
}

template<>
void _loadResource<Environment>(std::shared_ptr<SystemBus> sb, vka::uri _uri, Environment_ID id)
{
    using ResourceType = Environment;
    using id_type     = typename ResourceManager<ResourceType>::id_type;

    auto loaderLambda = [](std::shared_ptr<SystemBus> SB, id_type _id, vka::uri uu)
    {
        EnvironmentLoader Loader;

        // load the material, the textures may not be loaded yet.
        auto SS = std::make_shared<ResourceType>( Loader.load(*SB, uu) );

        // simple inplace lambda to load the textures
        auto _loadTexture = [SB](auto tex_id)
        {
            if( tex_id.valid() )
            {
                if( !SB->resourceIsHostLoaded(tex_id) )
                {
                    // schedule the texture to load in the background
                    SB->resourceHostLoadBackground(tex_id);
                }
            }
        };

        // schedule all the textures to load in teh background
        {
            _loadTexture(SS->irradiance);
            _loadTexture(SS->radiance);
            _loadTexture(SS->skybox);

            // The final Checker tasks will
            // be pushed onto the task pool after all the load texture tasks have been
            // schedule. This task will check that all the textures are loaded
            // before fully emplacing the material into the manater
            auto finalChecker = [SB, SS](id_type _id1)
            {
                auto & P = *SS;

                bool isLoaded=true;
                while( !isLoaded)
                {
                    isLoaded=true;

                    isLoaded &= P.irradiance.valid() ? SB->resourceIsHostLoaded( P.irradiance        ) : true;
                    isLoaded &= P.radiance.valid() ? SB->resourceIsHostLoaded( P.radiance) : true;
                    isLoaded &= P.skybox.valid() ? SB->resourceIsHostLoaded( P.skybox           ) : true;

                    std::this_thread::sleep_for( std::chrono::milliseconds(1));
                }

                SB->emplaceResourceLater<Environment>(_id1, SS);
            };

            // lock the systembus so that all
            // tasks get schedule at the same time
            auto l = SB->lockTaskPool();
            SB->pushTaskPool( finalChecker, _id);
        }
    };

    sb->pushTaskPool( loaderLambda, sb, id, _uri);
}


template<>
void _loadResource<PBRMaterial>(std::shared_ptr<SystemBus> sb, vka::uri _uri, PBRMaterial_ID id)
{
    using ResourceType = PBRMaterial;
    using id_type     = typename ResourceManager<ResourceType>::id_type;


    auto loaderLambda = [](std::shared_ptr<SystemBus> SB, id_type _id, vka::uri uu)
    {
        MaterialLoader Loader;

        // load the material, the textures may not be loaded yet.
        auto SS = std::make_shared<PBRMaterial>( Loader.load(*SB, uu) );

        // simple inplace lambda to load the textures
        auto _loadTexture = [SB](auto tex_id)
        {
            if( tex_id.valid() )
            {
                if( !SB->resourceIsHostLoaded(tex_id) )
                {
                    // schedule the texture to load in the background
                    SB->resourceHostLoadBackground(tex_id);
                }
            }
        };

        // schedule all the textures to load in teh background
        {
            // lock the systembus so that all
            // tasks get schedule at the same time
            _loadTexture(SS->baseColorTexture        );
            _loadTexture(SS->metallicRoughnessTexture);
            _loadTexture(SS->normalTexture           );
            _loadTexture(SS->occlusionTexture        );
            _loadTexture(SS->emissiveTexture         );


            // The final Checker tasks will
            // be pushed onto the task pool after all the load texture tasks have been
            // schedule. This task will check that all the textures are loaded
            // before fully emplacing the material into the manater
            auto finalChecker = [SB, SS](id_type _id1)
            {
                auto & P = *SS;

                bool isLoaded=true;
                while( !isLoaded)
                {
                    isLoaded=true;

                    isLoaded &= P.baseColorTexture        .valid() ? SB->resourceIsHostLoaded( P.baseColorTexture        ) : true;
                    isLoaded &= P.metallicRoughnessTexture.valid() ? SB->resourceIsHostLoaded( P.metallicRoughnessTexture) : true;
                    isLoaded &= P.normalTexture           .valid() ? SB->resourceIsHostLoaded( P.normalTexture           ) : true;
                    isLoaded &= P.occlusionTexture        .valid() ? SB->resourceIsHostLoaded( P.occlusionTexture        ) : true;
                    isLoaded &= P.emissiveTexture         .valid() ? SB->resourceIsHostLoaded( P.emissiveTexture         ) : true;

                    std::this_thread::sleep_for( std::chrono::milliseconds(1));
                }

                SB->emplaceResourceLater(_id1, SS);
            };

            SB->pushTaskPool( finalChecker, _id);
        }
    };

    sb->pushTaskPool( loaderLambda, sb, id, _uri);
}

template<>
void _loadResource<GLTFScene2>(std::shared_ptr<SystemBus> sb, vka::uri _uri, Scene2_ID id)
{
    auto loaderLambda = [](std::shared_ptr<SystemBus> SB, Scene2_ID _e1, vka::uri uu)
    {
        GLTFLoader2 Loader;

        EASY_BLOCK("Loading GLTF");
        auto SS = std::make_shared<GLTFScene2>(Loader.load(*SB, uu, 2048));
        EASY_END_BLOCK

        SB->emplaceResourceLater<GLTFScene2>(_e1, SS);
    };

    sb->pushTaskPool( loaderLambda, sb, id, _uri);
}


template<>
void _loadResource<HostTexture2D>(std::shared_ptr<SystemBus> sb, vka::uri _uri, Texture2D_ID id)
{
    auto loaderLambda = [](std::shared_ptr<SystemBus> SB, Texture2D_ID e1, vka::uri uu)
    {
        auto strr = fmt::format("Loading Texture: {}", SB->getResourceManager<HostTexture2D>().getResourceName(e1));
        EASY_BLOCK( strr);
        auto I = HostTextureArrayLoader::_loadSingleImage(SB.get(), uu);

        auto CC = std::make_shared<HostTexture2D>( std::move(I) );
        {
            auto l = SB->getResourceManager_p<HostTexture2D>()->acquireLock();
            // emplace the resource. This will set the
            // resource to be available for hostmemory related tasks
            // any calls to resourceIsHostLoaded( )  will return true
            SB->emplaceResourceLater(e1, CC);

        }
        EASY_END_BLOCK
    };

    sb->pushTaskPool( loaderLambda, sb, id, _uri);
}



template<>
void _loadResource<HostTextureCube>(std::shared_ptr<SystemBus> sb, vka::uri _uri, TextureCube_ID id)
{
    {
        auto    J = sb->readResourceJSON(_uri);

        auto _name = sb->getResourceManager<HostTextureCube>().getResourceName(id);

        // this section will try parallelize all loading of the cube faces, each mipmap and cube
        // face will be sent on its own task.
        if( J.at("resourceType").get<std::string>() == "cubeMap")
        {
            // list of all URIs for each cube miplevel
            auto mips = J.at("mipMaps").get< std::vector<std::string> >();

            auto cubeMapArray = std::make_shared<HostTextureCube>();
            cubeMapArray->layer.clear();
            cubeMapArray->layer.resize(6);
            for(auto & L : cubeMapArray->layer)
            {
                L.level.resize( mips.size() );
            }

            auto latLongMipChain = std::make_shared<HostImageArray>();
            latLongMipChain->layer.resize(mips.size());

            auto totalFacesLeft = std::make_shared<size_t>();
            *totalFacesLeft = 6 * mips.size();

            // Root path of the cubemap json file.
            std::string rootPath_j = vka::fs::parent_path( sb->getPath(_uri));

            auto loadFromURI = [id]( std::shared_ptr<HostTextureCube> fullCube,
                                   std::shared_ptr<HostImageArray>  mipChainLL,
                                   size_t mipIndex,
                                   vka::uri mipURI,
                                   std::string rootPath,
                                   std::shared_ptr<SystemBus> SB,
                                   std::shared_ptr<size_t> totalF)
            {
                SB_TRACE("Loading Mip Level: {} ", mipURI.toString());
                auto u = mipURI;
                std::string path;
                if( u.scheme == "rc")
                {
                    path = SB->getPath(u);
                }
                else if( u.scheme == "file")
                {
                    if( u.path.front() == '/')
                    {
                        path = u.path;
                    }
                    else
                    {
                        path = vka::fs::join( rootPath, u.path);
                    }
                }
                auto strr = fmt::format("LoadingFS: {}", u.path);

                EASY_BLOCK(strr)
                    vka::HostImage I = vka::loadHostImage_NoRGB(path);
                    if( !HostTextureArrayLoader::isPowerOfTwo( I.width() ))
                    {
                        throw std::runtime_error("Image is not a power of two");
                    }
                    if( I.width() != 2*I.height())
                    {
                        throw std::runtime_error("Image width must be 2*height");
                    }
                    // place the loaded image into the first slot
                    mipChainLL->layer.at(mipIndex).level.at(0) = std::move(I);
                EASY_END_BLOCK



                // This takes a particular mip level in teh LatLong chain
                // and converts it into a set of cube
                auto createCubeFace = [id, u]( std::shared_ptr<size_t> _totalF,
                        std::shared_ptr<HostTextureCube> fullCubeMap,
                        std::shared_ptr<HostImageArray> mipChainLL2,
                        size_t _mipIndex,
                        uint32_t faceIndex,
                        std::shared_ptr<SystemBus> _sb)
                {
                     auto name = _sb->getResourceManager<HostTextureCube>().getResourceName(id);

                     SB_TRACE("Creating Cube Face {} :    Face Index: {}   Mip: {} ", u.toString(), faceIndex, _mipIndex);

                     auto strrr = fmt::format("Creating Face: {}.{}", faceIndex, _mipIndex);
                     EASY_BLOCK( strrr);
                     assert( mipChainLL2->layer.at(_mipIndex).level.size() == 1); // there is only one image.
                     auto height = mipChainLL2->layer.at(_mipIndex).level.at(0).getHeight();
                     auto Cface = vka::HostCubeImage::fromLatLong( &mipChainLL2->layer.at(_mipIndex).level.at(0) , height/2, faceIndex);
                     EASY_END_BLOCK

                     assert( fullCubeMap->layer.size() == 6);
                     assert( _mipIndex < fullCubeMap->layer.at(faceIndex).level.size() );
                     // place the face into the appropriate spot
                     fullCubeMap->layer.at(faceIndex).level.at(_mipIndex) = std::move(Cface);
                     --(*_totalF);

                     // the task that is ordered to produce face index 5 of mip levle 0
                     // will be the one to combine everything
                     // into the final resource. Since this will likely be the
                     // last task to finish
                     if( _mipIndex == 0 && faceIndex==5)
                     {
                         while( *_totalF != 0 )
                         {
                             std::this_thread::sleep_for( std::chrono::milliseconds(1));
                         }
                         {
                             auto l = _sb->getResourceManager_p<HostTextureCube>()->acquireLock();

                             _sb->emplaceResourceLater<HostTextureCube>(id, fullCubeMap);

                         }
                     }
                };

                // If we are processing the first mipIndex
                // then when we are done, its most likely that the others are
                // so we should schedule the task to create the actual cubeMap chain
                if( mipIndex == 0)
                {
                    {
                        // lock the system bus so that all tasks get pushed in sequence
                        SB->pushTaskPool( createCubeFace, totalF, fullCube, mipChainLL, 0, 0, SB);
                        SB->pushTaskPool( createCubeFace, totalF, fullCube, mipChainLL, 0, 1, SB);
                        SB->pushTaskPool( createCubeFace, totalF, fullCube, mipChainLL, 0, 2, SB);
                        SB->pushTaskPool( createCubeFace, totalF, fullCube, mipChainLL, 0, 3, SB);
                        SB->pushTaskPool( createCubeFace, totalF, fullCube, mipChainLL, 0, 4, SB);
                        SB->pushTaskPool( createCubeFace, totalF, fullCube, mipChainLL, 0, 5, SB);
                    }
                }
                else
                {
                    EASY_BLOCK("Loading Mips 1-N")
                    // the remaining mip levels should be done
                    // on this task since all of them will finish
                    // before the mip0 even loads.
                    createCubeFace(totalF, fullCube, mipChainLL, mipIndex, 0, SB);
                    createCubeFace(totalF, fullCube, mipChainLL, mipIndex, 1, SB);
                    createCubeFace(totalF, fullCube, mipChainLL, mipIndex, 2, SB);
                    createCubeFace(totalF, fullCube, mipChainLL, mipIndex, 3, SB);
                    createCubeFace(totalF, fullCube, mipChainLL, mipIndex, 4, SB);
                    createCubeFace(totalF, fullCube, mipChainLL, mipIndex, 5, SB);
                    EASY_END_BLOCK
                }

                return 0;
            };
            (void)loadFromURI;
            //===============================================================================================
            // LAMBDA END
            //===============================================================================================

            // put the loading onto the taskpool
            // we probably only need to push the f
            {
                // lock the systembus so that all tasks are pushed in sequence.
                auto l = sb->lockTaskPool();


                for(size_t i=0;i< mips.size();i++)
                {
                    sb->pushTaskPool( loadFromURI, cubeMapArray, latLongMipChain, i, vka::uri(mips[i]), rootPath_j, sb, totalFacesLeft);
                }
            }
        }
    }
}


}
}

