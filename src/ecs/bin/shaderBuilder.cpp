#include <iostream>
#include <fstream>

#include <vka/ecs/ShaderBuilder/ShaderBuilder.h>

int main( int argc, char ** argv)
{
    (void)argc;
    (void)argv;

    if( argc == 3)
    {
        std::string type = argv[1];
        std::string filename = argv[2];

        std::ifstream t(filename);
        std::string str((std::istreambuf_iterator<char>(t)),
                         std::istreambuf_iterator<char>());

        vka::ecs::ShaderBuilder2 S1;


        if( type == "vert")
        {
            S1.setVertexShaderMain(str);
            auto vf = S1.buildVertexShader();
            std::cout << vf << std::endl;
        }
        else if( type == "frag")
        {
            S1.setFragmentShaderMain(str);
            auto vf = S1.buildFragmentShader(false);
            std::cout << vf << std::endl;
        }

    }
    else
    {
        std::cout << R"foo(
    This Program is used to test/build shaders for
    the vka::ecs::RenderSystem2::Renderer.

    All shaders must follow a specfic pattern.
    This program takes the input shader, using our
    custom shader format and produces the glsl
    equivelant that can be used for the Renderer.

    Usage:
        ./shaderBuilder [vert/frag] shaderfile

)foo";
    }


    return 0;
}
