#!/bin/bash

#
# Copyright 2014 Dario Manesku. All rights reserved.
# License: http://www.opensource.org/licenses/BSD-2-Clause
#

# This file requires the CMFT application to be able to create
# environment maps.
# down

CMFT=cmft

power2() { echo "x=l($1)/l(2); scale=0; 2^((x+0.5)/1)" | bc -l; }

#INPUT="okretnica.tga"
#INPUT="pano.tga"
#INPUT="papermill.tga"
INPUT=$1

if [[ "${INPUT}" == "" ]]; then
    echo Create an environment map using ./cmft.sh latlongfile.tga
    echo ""
    echo Input files musts be in tga format, dimensions power of two, and width = 2*height
    exit 1
fi


BASE_NAME=$(basename ${INPUT})

OUTPUT_NAME=$(echo "${BASE_NAME}" | cut -f 1 -d '.')

HEIGHT=$(identify -format '%h' ${INPUT})
WIDTH=$(identify -format '%w'  ${INPUT})

POW2H=$(power2 ${HEIGHT})
POW2W=$(power2 ${WIDTH})

if [[ "${POW2H}" != "HEIGHT" ]]; then
    echo not correct size
    convert -resize ${POW2W}x${POW2H}! ${INPUT}  ${INPUT}.resize.png
    convert ${INPUT}.resize.png  ${INPUT}.resize.tga
    rm ${INPUT}.resize.png
    INPUT=${INPUT}.resize.tga
    HEIGHT=$(identify -format '%h' ${INPUT})
    WIDTH=$(identify -format '%w'  ${INPUT})
    RM_FILE=${INPUT}
fi

# The actual size of the source image
# it is usually equal to 1/2 the size of the height of the image
SRC_FACE_SIZE=$(expr ${HEIGHT} / 2)

if [[ "${FACE_SIZE}"=="" ]]; then
    FACE_SIZE=${SRC_FACE_SIZE}
fi


RAD_FACE_SIZE=$(expr ${FACE_SIZE} / 2)

THREADS=8

BRDFTYPE=phongbrdf
#BRDFTYPE=blinnbrdf


OUTPUT=${OUTPUT_NAME}_$FACE_SIZE

rm -rf ${OUTPUT}
mkdir -p ${OUTPUT}/{none_latlong,radiance_latlong,irradiance_latlong}
echo ${INPUT}

$CMFT --input ${INPUT} \
         --filter irradiance\
         --srcFaceSize ${SRC_FACE_SIZE}\
         --dstFaceSize ${RAD_FACE_SIZE}\
         --numCpuProcessingThreads ${THREADS}\
         --outputNum 1\
         --output0 ${OUTPUT}/irradiance_latlong/${OUTPUT} \
         --output0params tga,bgra8,latlong

$CMFT --input ${INPUT} \
     --filter none \
     --dstFaceSize ${FACE_SIZE} \
     --excludeBase false\
     --glossScale 10\
     --glossBias 3 \
     --lightingModel ${BRDFTYPE}\
     --numCpuProcessingThreads ${THREADS}\
     --useOpenCL true \
     --clVendor anyGpuVendor\
     --deviceType gpu\
     --deviceIndex 0\
     --inputGammaNumerator 1.0\
     --inputGammaDenominator 1.0\
     --outputGammaNumerator 1.0\
     --outputGammaDenominator 1.0\
     --generateMipChain false \
     --outputNum 1\
     --output0 ${OUTPUT}/none_latlong/${OUTPUT} \
     --output0params tga,bgra8,latlong

$CMFT --input ${INPUT} \
     --filter radiance \
     --srcFaceSize ${RAD_FACE_SIZE}\
     --dstFaceSize ${RAD_FACE_SIZE}\
     --excludeBase false\
     --glossScale 10 \
     --glossBias 1 \
     --lightingModel ${BRDFTYPE}\
     --numCpuProcessingThreads ${THREADS}\
     --useOpenCL true \
     --clVendor anyGpuVendor\
     --deviceType gpu\
     --deviceIndex 0\
     --inputGammaNumerator 1.0\
     --inputGammaDenominator 1.0\
     --outputGammaNumerator 1.0\
     --outputGammaDenominator 1.0\
     --generateMipChain false \
     --outputNum 1\
     --output0 ${OUTPUT}/radiance_latlong/${OUTPUT} \
     --output0params tga,bgra8,latlong


cd ${OUTPUT}/radiance_latlong
a=1
for i in *.tga; do
  new=$(printf "radiance_%1d.jpg" "$a") #04 pad to length of 4
  convert "$i" "$new"
  let a=a+1
done
rm *.tga
convert ../none_latlong/${OUTPUT}.tga radiance_0.jpg
convert ../irradiance_latlong/${OUTPUT}.tga irradiance.jpg
rm -rf ../none_latlong
rm -rf ../irradiance_latlong
mv *.jpg ..
cd ..
rm -rf radiance_latlong

if [[ "${RM_FILE}" != "" ]]; then
    cd ..
   rm $RM_FILE
fi

