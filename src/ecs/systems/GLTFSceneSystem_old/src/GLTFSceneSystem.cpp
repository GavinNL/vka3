#include <vka/ecs/Systems/GLTFSceneSystem.h>
#include <vka/ecs/Components/Tags.h>

void vka::ecs::GLTFSceneSystem::clear()
{

}


void vka::ecs::GLTFSceneSystem::onSystemConnect()
{
    registry().on_construct<SceneComponent>().connect<   &GLTFSceneSystem::onConstructGLTFSceneComponent>(*this);
    registry().on_update<   SceneComponent>().connect<     &GLTFSceneSystem::onUpdateGLTFSceneComponent>(*this);
    registry().on_destroy<  SceneComponent>().connect<     &GLTFSceneSystem::onDestroyGLTFSceneComponent>(*this);

    registry().on_destroy<  SceneNodeComponent>().connect<     &GLTFSceneSystem::onDestroySceneNodeComponent>(*this);


    sceneComponentTransformsUpdated.connect( registry(), entt::collector.update<TransformComponent>().where<SceneComponent>() );

    m_systemBus->registerResourceLoader<vka::ecs::GLTFLoader>();
    m_systemBus->registerResourceLoader<vka::ecs::PrimitiveLoader>();
    m_systemBus->registerComponentSerializer<SceneComponent>();
    m_systemBus->registerComponent<SceneComponent>();
    m_systemBus->registerComponent<SceneNodeComponent>();

}

void vka::ecs::GLTFSceneSystem::onSystemDisconnect()
{
    registry().on_construct<SceneComponent>().disconnect<   &GLTFSceneSystem::onConstructGLTFSceneComponent>(*this);
    registry().on_update<   SceneComponent>().disconnect<     &GLTFSceneSystem::onUpdateGLTFSceneComponent>(*this);
    registry().on_destroy<  SceneComponent>().disconnect<     &GLTFSceneSystem::onDestroyGLTFSceneComponent>(*this);

    sceneComponentTransformsUpdated.disconnect();
}

void vka::ecs::GLTFSceneSystem::reset(vka::ecs::SystemBase::Registry &r, vka::ecs::SceneComponent &C)
{
    for(auto e : C.nodes)
    {
        r.destroy(e);
    }
}

void vka::ecs::GLTFSceneSystem::create(vka::ecs::SystemBase::Registry &r, vka::ecs::SceneComponent &G, vka::ecs::GLTFScene &S)
{
    (void)r;
    G.nodeSpaceTransforms = S.getNodeSpaceTransforms();
    for(auto & n : S.nodes)
    {
        if( n.isRenderable )
        {
            auto E = m_systemBus->create();

            E.create_and_update<TransformComponent>(
                        [&](auto & T)
            {
                T = n.transform;
            });
            G.nodes.push_back(E.entity);

            if( n.meshIndex.has_value())
            {
#if 1
                auto & M = S.meshes.at(*n.meshIndex);

                auto & GM = E.create<MeshComponent>();

                for(auto & pi :  M.meshPrimitives )
                {
                    auto _id   = S.rawPrimitiveId.at( S.primitives.at(pi.primitiveIndex).rawPrimitiveIndex );
                    auto & mat = S.pbrMaterials.at(pi.materialIndex);

                    auto dc = S.primitives.at(pi.primitiveIndex).drawCall;

                    GM.addMesh(_id, mat, dc);

                }
#endif
            }
        }
        else
        {
            G.nodes.push_back( entt::null );
        }
    }
}

void vka::ecs::GLTFSceneSystem::_checkScene(vka::ecs::SceneComponent &G)
{
    auto & RM = m_systemBus->getResourceManager<GLTFScene>();
    if( !G.sceneId.valid() )
    {
        // try to find the scene
        auto id = RM.findResource(G._uri.toString());

        // no scene found. create it
        if( !id.valid() )
        {
            // valid uri path
            if( G._uri.path.size() )
            {

                G.sceneId = m_systemBus->getOrCreateResource<GLTFScene>(G._uri);
            }
            else
            {
                return;
            }
        }
        else
        {
            G.sceneId = id;
        }
    }
}

entt::entity vka::ecs::GLTFSceneSystem::buildHierarchy(const vka::ecs::GLTFScene &S, uint32_t rootNode, std::vector<entt::entity> &nodes)
{
    auto nE = m_systemBus->create();

    auto & M = nE.get<MainComponent>();
    M.name = S.nodes[rootNode].name;
    nE.create<LocalTransformComponent>(S.nodes[rootNode].transform);
    nodes[rootNode] = nE.entity;

    for(auto c : S.nodes[rootNode].children)
    {
        if( S.nodes[c].isRenderable)
            m_systemBus->linkToParent( buildHierarchy(S, c, nodes), nE.entity);
    }

    return nE.entity;
}

void vka::ecs::GLTFSceneSystem::buildScene(const vka::ecs::GLTFScene &S, vka::ecs::SystemBase::Registry &r, entt::entity e, vka::ecs::SceneComponent &G)
{
    auto & Sc = S.scenes[ G.sceneIndex ];

    std::vector<entt::entity> nodes;
    nodes.insert( nodes.end(), S.nodes.size(), entt::null);

    for(auto rr : Sc.rootNodes)
    {
        m_systemBus->linkToParent(buildHierarchy(S,rr,nodes), e);
    }


    for(auto x : nodes)
    {
        if( r.valid(x))
            r.emplace<SceneNodeComponent>(x).rootSceneComponent = e;
    }

    G.nodeSpaceTransforms = S.getNodeSpaceTransforms();
    G.nodes = std::move(nodes);

    for(uint32_t i=0;i<S.nodes.size();i++)
    {
        auto & n = S.nodes[i];

        if( n.meshIndex.has_value() )
        {
            if( n.meshIndex.has_value())
            {
#if 1
                auto & M = S.meshes.at(*n.meshIndex);

                auto & GM = r.emplace<MeshComponent>( G.nodes[i] );// .create<MeshComponent>();

                for(auto & pi :  M.meshPrimitives )
                {
                    auto _id   = S.rawPrimitiveId.at( S.primitives.at(pi.primitiveIndex).rawPrimitiveIndex );
                    auto & mat = S.pbrMaterials.at(pi.materialIndex);

                    auto & PP    = GM.primitives.emplace_back();

                    PP.material  = mat;
                    PP.primitive = _id;


                    auto & dc = S.primitives.at(pi.primitiveIndex).drawCall;
                    PP.drawCall.firstIndex = dc.firstIndex;
                    PP.drawCall.indexCount = dc.indexCount;
                    PP.drawCall.vertexCount = dc.vertexCount;
                    PP.drawCall.vertexOffset = dc.vertexOffset;
                }
#endif
            }
        }
    }
}

void vka::ecs::GLTFSceneSystem::onConstructGLTFSceneComponent(vka::ecs::SystemBase::Registry &r, vka::ecs::SystemBase::Entity e)
{
    auto & G = r.get<SceneComponent>(e);

    _checkScene(G);
    if( !G.sceneId.valid() ) return;
    m_systemBus->forceLoadResource2(G.sceneId);


    auto & S = m_systemBus->getManagedResource<GLTFScene>(G.sceneId);
    buildScene(S,r,e, G);

}

void vka::ecs::GLTFSceneSystem::handleTransformUpdate()
{
    for(auto e : m_systemBus->registry.view<tag_animation_nodes_updated>() )
    {
        process(registry(), e);
        registry().remove<tag_animation_nodes_updated>(e);
    }
}

void vka::ecs::GLTFSceneSystem::process(vka::ecs::SystemBase::Registry &r, vka::ecs::SystemBase::Entity e)
{
    if( !r.has<SceneComponent>(e)) return;

    auto & G = r.get<SceneComponent>(e);
    auto const & S = m_systemBus->getManagedResource<GLTFScene>(G.sceneId);
    //  The model-space matrices for each node in the GLTF scene
    auto matrices = S.getModelSpaceMatrices( G.nodeSpaceTransforms );

    // Loop through all the nodes in the GLTF Scene, find their
    // respective entity and uptate the matrix
    uint32_t index=0;
    for(auto & n : S.nodes)
    {
        if( n.meshIndex.has_value() )
        {
            if( r.valid(G.nodes[index]))
            {
                if( r.has<LocalTransformComponent>(G.nodes[index]))
                {
                    r.patch<LocalTransformComponent>(G.nodes[index],
                                                     [&](auto & T)
                    {
                        T = G.nodeSpaceTransforms[index];
                    });
                }
            }

            if(  n.skinIndex.has_value() )
            {
                auto & M = r.get<MeshComponent>( G.nodes[index] );
                // copy the skin matrices to the mes
                M.boneMatrices.clear();
                appendBoneMatrices(M.boneMatrices, matrices, S.skins[ *n.skinIndex].inverseBindMatrices, S.skins[ *n.skinIndex].joints);
            }
        }
        ++index;
    }
}

void vka::ecs::GLTFSceneSystem::onUpdateGLTFSceneComponent(vka::ecs::SystemBase::Registry &r, vka::ecs::SystemBase::Entity e)
{
    // The GLTFScene component has been updated, so we should make sure

    //==================================================================================
    // Check if the GLTFScene is loaded
    //==================================================================================
    auto & G = r.get<SceneComponent>(e);

    _checkScene(G);
    if( G.sceneId.valid() )
    {
        m_systemBus->forceLoadResource2(G.sceneId);
    }
    else
    {
        return;
    }

    auto const & S = m_systemBus->getManagedResource<GLTFScene>(G.sceneId);

    if( G.nodes.size() == 0)
    {
        buildScene(S,r,e, G);
    }
    //==================================================================================

    process(r,e);
    return;
    //  The model-space matrices for each node in the GLTF scene
    auto matrices = S.getModelSpaceMatrices( G.nodeSpaceTransforms );

    // Loop through all the nodes in the GLTF Scene, find their
    // respective entity and uptate the matrix
    uint32_t index=0;
    for(auto & n : S.nodes)
    {
        if( n.meshIndex.has_value() )
        {
            if( r.valid(G.nodes[index]))
            {
                r.patch<LocalTransformComponent>(G.nodes[index],
                                                 [&](auto & T)
                {
                    T = G.nodeSpaceTransforms[index];
                });
            }

            if(  n.skinIndex.has_value() )
            {
                auto & M = r.get<MeshComponent>( G.nodes[index] );
                // copy the skin matrices to the mes
                M.boneMatrices.clear();
                appendBoneMatrices(M.boneMatrices, matrices, S.skins[ *n.skinIndex].inverseBindMatrices, S.skins[ *n.skinIndex].joints);
            }
        }
        ++index;
    }
}

void vka::ecs::GLTFSceneSystem::onDestroyGLTFSceneComponent(vka::ecs::SystemBase::Registry &r, vka::ecs::SystemBase::Entity e)
{
    (void)r;
    (void)e;
    auto & G = r.get<SceneComponent>(e);
    for(auto c : G.nodes)
    {
        if(c != entt::null)
        {
            if( r.valid(c))
                r.destroy(c);
        }
    }
    G = SceneComponent();
}

void vka::ecs::GLTFSceneSystem::onDestroySceneNodeComponent(vka::ecs::SystemBase::Registry &r, vka::ecs::SystemBase::Entity e)
{
    (void)r;
    (void)e;
    auto & G = r.get<SceneNodeComponent>(e);

    if( r.valid(G.rootSceneComponent) )
    {
        if( r.has<SceneComponent>(G.rootSceneComponent))
        {
            auto & R = r.get<SceneComponent>(G.rootSceneComponent);
            for(auto & x : R.nodes)
            {
                if( x==e)
                {
                    x = entt::null;
                    return;
                }
            }
        }
    }
}

void vka::ecs::GLTFSceneSystem::appendBoneMatrices(std::vector<glm::mat4> &nodePlusBoneMatrices, vk::ArrayProxy<const glm::mat4> modelSpaceBoneTransforms, vk::ArrayProxy<const glm::mat4> inverseBindMatrices, vk::ArrayProxy<const uint32_t> jointIndices)
{
    if( jointIndices.size() )
    {
        assert( jointIndices.size() == inverseBindMatrices.size() );

        if( modelSpaceBoneTransforms.size() )
        {
            // upload all bone matrices
            uint32_t b=0;
            for(auto j : jointIndices)
            {
                glm::mat4 bone = modelSpaceBoneTransforms.data()[j] * inverseBindMatrices.data()[b++];
                nodePlusBoneMatrices.push_back( bone );
            }
        }
    }
}
