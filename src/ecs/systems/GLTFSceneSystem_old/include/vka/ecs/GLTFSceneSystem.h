#ifndef VKA_ECS_GLTF_SCENE_SYSTEM_H
#define VKA_ECS_GLTF_SCENE_SYSTEM_H


#include <map>
#include <list>
#include <set>


#include <vka/ecs/ResourceObjects/HostPrimitive.h>

#include <vka/ecs/SystemBase.h>
//#include <vka/ecs/RenderSystem2/ResourceManagers.h>
#include <vka/ecs/Components/CScene.h>
#include <vka/ecs/Components/CMesh.h>

#include <vka/ecs/Loaders/GLTFLoader.h>
#include <vka/ecs/Components/CTransform.h>

namespace vka
{
namespace ecs
{

struct SceneNodeComponent
{
    static constexpr auto _type = "SceneNodeComponent";
    entt::entity rootSceneComponent = entt::null;

    //==============================================================
    // Required
    //==============================================================
    json to_json() const
    {
        json J;
        J["rootSceneComponent"] = to_integral(rootSceneComponent);
        return J;
    }
    void from_json(const json & J)
    {
        (void)J;
    }
    //==============================================================

};

struct GLTFSceneSystem : public vka::ecs::SystemBase
{
    entt::observer sceneComponentTransformsUpdated;

    void clear() override;

    void onSystemConnect() override;

    void onSystemDisconnect() override;


    void reset(Registry & r, SceneComponent &C);

    void create(Registry & r, SceneComponent &G, GLTFScene & S);

    void _checkScene(SceneComponent & G);


    /**
     * @brief buildHierarchy
     * @param S
     * @param rootNode
     * @param nodes
     * @return
     *
     * Recursively builds a node of a GLTFScene and all it's children.
     *
     * Places the entites that it crates into the nodes vector. The nodes vector must be
     * initialized to all entt::null.
     */
    entt::entity buildHierarchy(GLTFScene const & S, uint32_t rootNode, std::vector<entt::entity> & nodes);

    void buildScene(GLTFScene const & S, Registry & r, entt::entity e, SceneComponent & G);

    void onConstructGLTFSceneComponent( Registry &r, Entity e );

    /**
     * @brief handleTransformUpdate
     *
     * If any entity has a SceneSystem and an Updated transform
     * make sur eto call the onUpdateGLTFSceneComponent so
     * that it can update all it's children's transforms
     */
    void handleTransformUpdate();

    void   process(   Registry &r, Entity e );
    void   onUpdateGLTFSceneComponent(   Registry &r, Entity e );

    void   onDestroyGLTFSceneComponent(   Registry &r, Entity e );
    void   onDestroySceneNodeComponent(   Registry &r, Entity e );


    static void appendBoneMatrices(
            std::vector<glm::mat4> & nodePlusBoneMatrices,
            vk::ArrayProxy<glm::mat4 const> modelSpaceBoneTransforms,
            vk::ArrayProxy<glm::mat4 const> inverseBindMatrices,
            vk::ArrayProxy<uint32_t const > jointIndices
            );

};



}
}

#endif
