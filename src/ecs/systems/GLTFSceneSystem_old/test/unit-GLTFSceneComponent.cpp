#include <iostream>

#include <vka/ecs/GLTFSceneSystem.h>
#include <vka/ecs/Components/PrimitiveMaterialComponent.h>
#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

#include <vka/ecs/json.h>

#include "catch.hpp"
#include <vka/ecs/SystemBus.h>

using namespace vka::ecs;


SCENARIO("Create SceneComponent from JSON")
{
auto str_j =
R"foo(

{

    "components" : {
        "TransformComponent" : {
            "position" : [1,1,1],
            "rotation" : [0,0,0,1]
        },
        "SceneComponent" : {
            "uri" : "rc:/models/ybot_full.glb"
        }
    }
}


)foo";

    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");

    sb->registerComponentSerializer<vka::ecs::TransformComponent>();
    sb->registerComponentSerializer<vka::ecs::SceneComponent>();

    auto SS = std::make_shared<GLTFSceneSystem>();
    SS->connect(sb);


    auto id = sb->createResource<vka::ecs::GLTFScene>(vka::uri("rc:models/ybot_full.glb"));
    (void)id;
    WHEN("We create an entity with a scene component")
    {
        auto J = nlohmann::json::parse(str_j);
        auto E = sb->jsonCreate(J);
        (void)E;

        auto e = E.entity;


        REQUIRE( sb->registry.has<SceneComponent>(e) );
        REQUIRE( sb->registry.has<TransformComponent>(e) );
        REQUIRE( sb->registry.alive() == 4 );

        std::cout << sb->to_json(e).dump(4) << std::endl;

    }
}

SCENARIO("Test Relinking")
{

    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");



    auto e1 = sb->create();
    auto e2 = e1.createChild();
    auto e3 = e2.createChild();

    REQUIRE( e1.get<MainComponent>().rootEntity == e1.entity);
    REQUIRE( e2.get<MainComponent>().rootEntity == e1.entity);
    REQUIRE( e3.get<MainComponent>().rootEntity == e1.entity);

    auto e0 = sb->create();
    sb->linkToParent(e1.entity, e0.entity);

    REQUIRE( e0.get<MainComponent>().rootEntity == e0.entity);
    REQUIRE( e1.get<MainComponent>().rootEntity == e0.entity);
    REQUIRE( e2.get<MainComponent>().rootEntity == e0.entity);
    REQUIRE( e3.get<MainComponent>().rootEntity == e0.entity);

    REQUIRE( e0.get<MainComponent>().children.front() == e1.entity);

    sb->registry.destroy(e1.entity);
    REQUIRE( e0.get<MainComponent>().children.size() == 0);
}


#define STB_IMAGE_IMPLEMENTATION
#define VKA_IMAGE_IMPLEMENTATION

#define VKAI_NO_STDIO
#define STBI_NO_STDIO


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wunused-function"

#include <vka/utils/vka_image.h>

#pragma GCC diagnostic pop
