#ifndef VKA_ECS_SDL_WINDOW_SYSTEM_H
#define VKA_ECS_SDL_WINDOW_SYSTEM_H

#include <vka/ecs/SystemBase2.h>
#include <vkw/QtVulkanWidget.h>
#include <vka/ecs/Events/InputEvents.h>
#include <map>
#include <list>
#include <set>


namespace vka
{
namespace ecs
{

/**
 * @brief The QtWindowSystem class
 *
 * The QtWindowSystem can be added to the systembus to create
 * a widget for
 */
class QtWindowSystem : public vka::ecs::SystemBaseInternal,
                       public vkw::QtVulkanWidget,
                       public vkw::Application
{
public:
    QtWindowSystem(QVulkanInstance *inst) : vkw::QtVulkanWidget()
    {
        QtVulkanWidget::setVulkanInstance(inst);
        this->init(this);
        _initKeys();
    }
    ~QtWindowSystem()
    {

    }

    QWidget* getWrapper()
    {
        return QWidget::createWindowContainer(this);
    }

    //==========================================================================
    // From SystemBase
    //==========================================================================
    void onStart()    override;
    void onStop() override;
    void onUpdate() override;

    std::string name() const override
    {
        return "QtWindowSystem";
    }
    //==========================================================================


    //==========================================================================
    //
    //==========================================================================
    void mousePressEvent(QMouseEvent * e) override
    {
        using namespace vka;

        EvtInputMouseButton E;
        E.x      = e->x();//event.button.x;
        E.y      = e->y();//event.button.y;
        switch( e->button() )
        {
           case Qt::NoButton         : E.button = vka::MouseButton::NONE;  break;//= 0x00000000,
           case Qt::LeftButton       : E.button = vka::MouseButton::LEFT;  break;//= 0x00000001,
           case Qt::RightButton      : E.button = vka::MouseButton::RIGHT;  break;//= 0x00000002,
           //case Qt::MidButton      : E.button = ;  break;//= 0x00000004, // ### Qt 6: remove me
           case Qt::MiddleButton     : E.button = vka::MouseButton::MIDDLE;  break;//= MidButton,
           //case Qt::BackButton     : E.button = ;  break;//= 0x00000008,
           case Qt::XButton1         : E.button = vka::MouseButton::X1;  break;//= BackButton,
           //case Qt::ExtraButton1   : E.button = ;  break;//= XButton1,
           //case Qt::ForwardButton  : E.button = ;  break;//= 0x00000010,
           case Qt::XButton2         : E.button = vka::MouseButton::X2;  break;//= ForwardButton,
        default:
            E.button = vka::MouseButton::NONE;
         break;
        }
        E.state =  1;//e->buttons() & e->button() ? 1 : 0;

        E.clicks = 1;//event.button.clicks;
        getSystemBus()->triggerEvent(E);
    }

    void mouseReleaseEvent(QMouseEvent * e) override
    {
        using namespace vka;
        EvtInputMouseButton E;
        E.x      = e->x();//event.button.x;
        E.y      = e->y();//event.button.y;
        switch( e->button() )
        {
           case Qt::NoButton         : E.button = vka::MouseButton::NONE;  break;//= 0x00000000,
           case Qt::LeftButton       : E.button = vka::MouseButton::LEFT;  break;//= 0x00000001,
           case Qt::RightButton      : E.button = vka::MouseButton::RIGHT;  break;//= 0x00000002,
           //case Qt::MidButton      : E.button = ;  break;//= 0x00000004, // ### Qt 6: remove me
           case Qt::MiddleButton     : E.button = vka::MouseButton::MIDDLE;  break;//= MidButton,
           //case Qt::BackButton     : E.button = ;  break;//= 0x00000008,
           case Qt::XButton1         : E.button = vka::MouseButton::X1;  break;//= BackButton,
           //case Qt::ExtraButton1   : E.button = ;  break;//= XButton1,
           //case Qt::ForwardButton  : E.button = ;  break;//= 0x00000010,
           case Qt::XButton2         : E.button = vka::MouseButton::X2;  break;//= ForwardButton,
           default:
               E.button = vka::MouseButton::NONE;
            break;
        }
        E.state =  0;//e->buttons() & e->button() ? 1 : 0;

        E.clicks = 1;//event.button.clicks;

        getSystemBus()->triggerEvent(E);
    }

    void mouseDoubleClickEvent(QMouseEvent * e) override
    {
        using namespace vka;
        EvtInputMouseButton E;
        E.x      = e->x();//event.button.x;
        E.y      = e->y();//event.button.y;
        switch( e->button() )
        {
           case Qt::NoButton         : E.button = vka::MouseButton::NONE;  break;//= 0x00000000,
           case Qt::LeftButton       : E.button = vka::MouseButton::LEFT;  break;//= 0x00000001,
           case Qt::RightButton      : E.button = vka::MouseButton::RIGHT;  break;//= 0x00000002,
           //case Qt::MidButton      : E.button = ;  break;//= 0x00000004, // ### Qt 6: remove me
           case Qt::MiddleButton     : E.button = vka::MouseButton::MIDDLE;  break;//= MidButton,
           //case Qt::BackButton     : E.button = ;  break;//= 0x00000008,
           case Qt::XButton1         : E.button = vka::MouseButton::X1;  break;//= BackButton,
           //case Qt::ExtraButton1   : E.button = ;  break;//= XButton1,
           //case Qt::ForwardButton  : E.button = ;  break;//= 0x00000010,
           case Qt::XButton2         : E.button = vka::MouseButton::X2;  break;//= ForwardButton,
           default:
               E.button = vka::MouseButton::NONE;
            break;
        }
        E.state =  0;//e->buttons() & e->button() ? 1 : 0;

        E.clicks = 2;//event.button.clicks;

        getSystemBus()->triggerEvent(E);
    }

    void mouseMoveEvent(QMouseEvent * e) override
    {
        using namespace vka;
        EvtInputMouseMotion M;
        M.x    = e->x();//event.motion.x;
        M.y    = e->y();//event.motion.y;
        M.xrel = M.x - m_oldMouseX;//event.motion.xrel;
        M.yrel = M.y - m_oldMouseY;//event.motion.yrel;

        m_oldMouseX = M.x;
        m_oldMouseY = M.y;
        //
        getSystemBus()->triggerEvent(M);
    }

    void keyPressEvent(QKeyEvent * e)       override
    {
        using namespace vka;
        EvtInputKey M;

        M.timestamp      = std::chrono::system_clock::now();
        M.down           = true;
        M.repeat         = static_cast<uint8_t>(e->count());
        auto f    = m_QT_to_Key.find( e->key() );
        M.keycode = KeyCode::UNKNOWN;
        if( f != m_QT_to_Key.end() )
           M.keycode = f->second;

        M.windowKeyCode  = static_cast<uint32_t>( e->key() );
        M.windowScanCode = static_cast<uint32_t>( e->nativeScanCode() );

        M.windowEvent = e;

       // std::cout << "QtScan " << M.windowScanCode << " --> vkaScan " << to_string(M.scancode) << std::endl;
        //std::cout << "QtKey  " << M.windowKeyCode << " --> vkaKey  " << to_string(M.keycode) << std::endl;
        //std::cout << "VulkanWidgetQt: Key: " << e->nativeVirtualKey() << "  Scan  " << e->nativeScanCode() << std::endl;
        getSystemBus()->triggerEvent(M);

    }

    void keyReleaseEvent(QKeyEvent * e)     override
    {
        using namespace vka;
        EvtInputKey M;

        M.timestamp      = std::chrono::system_clock::now();
        M.down           = false;
        M.repeat         = static_cast<uint8_t>( e->count() );
        auto f    = m_QT_to_Key.find( e->key() );
        M.keycode = KeyCode::UNKNOWN;
        if( f != m_QT_to_Key.end() )
           M.keycode = f->second;

        M.windowKeyCode  = static_cast<uint32_t>( e->key() );
        M.windowScanCode = e->nativeScanCode();

        M.windowEvent = e;

        //std::cout << "QtScan " << M.windowScanCode << " --> vkaScan " << to_string(M.scancode) << std::endl;
        //std::cout << "QtKey  " << M.windowKeyCode << " --> vkaKey  " << to_string(M.keycode) << std::endl;
        //std::cout << "VulkanWidgetQt: Key: " << e->nativeVirtualKey() << "  Scan  " << e->nativeScanCode() << std::endl;
        getSystemBus()->triggerEvent(M);

    }

    void wheelEvent(QWheelEvent * e) override
    {
        using namespace vka;
        EvtInputMouseWheel E;
        E.x = e->pixelDelta().x();
        E.y = e->pixelDelta().y();
        getSystemBus()->triggerEvent(E);
    }

    void _initKeys()
    {
        m_QT_to_Key[Qt::Key_Escape]         = vka::KeyCode::ESCAPE  ;/*0x01000000*/ // misc keys
        m_QT_to_Key[Qt::Key_Tab]            = vka::KeyCode::TAB  ;/*0x01000001*/
        m_QT_to_Key[Qt::Key_Backtab]        = vka::KeyCode::UNKNOWN  ;/*0x01000002*/
        m_QT_to_Key[Qt::Key_Backspace]      = vka::KeyCode::BACKSPACE  ;/*0x01000003*/
        m_QT_to_Key[Qt::Key_Return]         = vka::KeyCode::RETURN  ;/*0x01000004*/
        m_QT_to_Key[Qt::Key_Enter]          = vka::KeyCode::KP_ENTER  ;/*0x01000005*/
        m_QT_to_Key[Qt::Key_Insert]         = vka::KeyCode::INSERT  ;/*0x01000006*/
        m_QT_to_Key[Qt::Key_Delete]         = vka::KeyCode::DELETE  ;/*0x01000007*/
        m_QT_to_Key[Qt::Key_Pause]          = vka::KeyCode::PAUSE  ;/*0x01000008*/
        m_QT_to_Key[Qt::Key_Print]          = vka::KeyCode::PRINTSCREEN  ;/*0x01000009*/ // print screen
        m_QT_to_Key[Qt::Key_SysReq]         = vka::KeyCode::SYSREQ  ;/*0x0100000a*/
        m_QT_to_Key[Qt::Key_Clear]          = vka::KeyCode::CLEAR  ;/*0x0100000b*/
        m_QT_to_Key[Qt::Key_Home]           = vka::KeyCode::HOME  ;/*0x01000010*/ // cursor movement
        m_QT_to_Key[Qt::Key_End]            = vka::KeyCode::END  ;/*0x01000011*/
        m_QT_to_Key[Qt::Key_Left]           = vka::KeyCode::LEFT  ;/*0x01000012*/
        m_QT_to_Key[Qt::Key_Up]             = vka::KeyCode::UP  ;/*0x01000013*/
        m_QT_to_Key[Qt::Key_Right]          = vka::KeyCode::RIGHT  ;/*0x01000014*/
        m_QT_to_Key[Qt::Key_Down]           = vka::KeyCode::DOWN  ;/*0x01000015*/
        m_QT_to_Key[Qt::Key_PageUp]         = vka::KeyCode::PAGEUP  ;/*0x01000016*/
        m_QT_to_Key[Qt::Key_PageDown]       = vka::KeyCode::PAGEDOWN  ;/*0x01000017*/
 /**/    m_QT_to_Key[Qt::Key_Shift]          = vka::KeyCode::LSHIFT  ;/*0x01000020*/ // modifiers
 /**/    m_QT_to_Key[Qt::Key_Control]        = vka::KeyCode::LCTRL  ;/*0x01000021*/
 /**/    m_QT_to_Key[Qt::Key_Meta]           = vka::KeyCode::LGUI  ;/*0x01000022*/
        m_QT_to_Key[Qt::Key_Alt]            = vka::KeyCode::LALT  ;/*0x01000023*/
        m_QT_to_Key[Qt::Key_CapsLock]       = vka::KeyCode::CAPSLOCK  ;/*0x01000024*/
        m_QT_to_Key[Qt::Key_NumLock]        = vka::KeyCode::NUMLOCKCLEAR  ;/*0x01000025*/
        m_QT_to_Key[Qt::Key_ScrollLock]     = vka::KeyCode::SCROLLLOCK  ;/*0x01000026*/
        m_QT_to_Key[Qt::Key_F1]             = vka::KeyCode::F1  ;/*0x01000030*/ // function keys
        m_QT_to_Key[Qt::Key_F2]             = vka::KeyCode::F2 ;/*0x01000031*/
        m_QT_to_Key[Qt::Key_F3]             = vka::KeyCode::F3 ;/*0x01000032*/
        m_QT_to_Key[Qt::Key_F4]             = vka::KeyCode::F4  ;/*0x01000033*/
        m_QT_to_Key[Qt::Key_F5]             = vka::KeyCode::F5  ;/*0x01000034*/
        m_QT_to_Key[Qt::Key_F6]             = vka::KeyCode::F6  ;/*0x01000035*/
        m_QT_to_Key[Qt::Key_F7]             = vka::KeyCode::F7  ;/*0x01000036*/
        m_QT_to_Key[Qt::Key_F8]             = vka::KeyCode::F8  ;/*0x01000037*/
        m_QT_to_Key[Qt::Key_F9]             = vka::KeyCode::F9  ;/*0x01000038*/
        m_QT_to_Key[Qt::Key_F10]            = vka::KeyCode::F10  ;/*0x01000039*/
        m_QT_to_Key[Qt::Key_F11]            = vka::KeyCode::F11  ;/*0x0100003a*/
        m_QT_to_Key[Qt::Key_F12]            = vka::KeyCode::F12  ;/*0x0100003b*/
        m_QT_to_Key[Qt::Key_F13]            = vka::KeyCode::F13  ;/*0x0100003c*/
        m_QT_to_Key[Qt::Key_F14]            = vka::KeyCode::F14  ;/*0x0100003d*/
        m_QT_to_Key[Qt::Key_F15]            = vka::KeyCode::F15  ;/*0x0100003e*/
        m_QT_to_Key[Qt::Key_F16]            = vka::KeyCode::F16  ;/*0x0100003f*/
        m_QT_to_Key[Qt::Key_F17]            = vka::KeyCode::F17  ;/*0x01000040*/
        m_QT_to_Key[Qt::Key_F18]            = vka::KeyCode::F18  ;/*0x01000041*/
        m_QT_to_Key[Qt::Key_F19]            = vka::KeyCode::F19  ;/*0x01000042*/
        m_QT_to_Key[Qt::Key_F20]            = vka::KeyCode::F20  ;/*0x01000043*/
        m_QT_to_Key[Qt::Key_F21]            = vka::KeyCode::F21  ;/*0x01000044*/
        m_QT_to_Key[Qt::Key_F22]            = vka::KeyCode::F22  ;/*0x01000045*/
        m_QT_to_Key[Qt::Key_F23]            = vka::KeyCode::F23  ;/*0x01000046*/
        m_QT_to_Key[Qt::Key_F24]            = vka::KeyCode::F24  ;/*0x01000047*/
        m_QT_to_Key[Qt::Key_F25]            = vka::KeyCode::UNKNOWN  ;/*0x01000048*/ // F25 .. F35 only on X11
        m_QT_to_Key[Qt::Key_F26]            = vka::KeyCode::UNKNOWN  ;/*0x01000049*/
        m_QT_to_Key[Qt::Key_F27]            = vka::KeyCode::UNKNOWN  ;/*0x0100004a*/
        m_QT_to_Key[Qt::Key_F28]            = vka::KeyCode::UNKNOWN;/*0x0100004b*/
        m_QT_to_Key[Qt::Key_F29]            = vka::KeyCode::UNKNOWN;/*0x0100004c*/
        m_QT_to_Key[Qt::Key_F30]            = vka::KeyCode::UNKNOWN;/*0x0100004d*/
        m_QT_to_Key[Qt::Key_F31]            = vka::KeyCode::UNKNOWN;/*0x0100004e*/
        m_QT_to_Key[Qt::Key_F32]            = vka::KeyCode::UNKNOWN;/*0x0100004f*/
        m_QT_to_Key[Qt::Key_F33]            = vka::KeyCode::UNKNOWN;/*0x01000050*/
        m_QT_to_Key[Qt::Key_F34]            = vka::KeyCode::UNKNOWN;/*0x01000051*/
        m_QT_to_Key[Qt::Key_F35]            = vka::KeyCode::UNKNOWN;/*0x01000052*/
        m_QT_to_Key[Qt::Key_Super_L]        = vka::KeyCode::LGUI  ;/*0x01000053*/ // extra keys
        m_QT_to_Key[Qt::Key_Super_R]        = vka::KeyCode::RGUI  ;/*0x01000054*/
        m_QT_to_Key[Qt::Key_Menu]           = vka::KeyCode::MENU  ;/*0x01000055*/
        m_QT_to_Key[Qt::Key_Hyper_L]        = vka::KeyCode::UNKNOWN  ;/*0x01000056*/
        m_QT_to_Key[Qt::Key_Hyper_R]        = vka::KeyCode::UNKNOWN  ;/*0x01000057*/
        m_QT_to_Key[Qt::Key_Help]           = vka::KeyCode::HELP  ;/*0x01000058*/
        m_QT_to_Key[Qt::Key_Direction_L]    = vka::KeyCode::UNKNOWN  ;/*0x01000059*/
        m_QT_to_Key[Qt::Key_Direction_R]    = vka::KeyCode::UNKNOWN  ;/*0x01000060*/
        m_QT_to_Key[Qt::Key_Space]          = vka::KeyCode::SPACE  ;/*0x20*/ // 7 bit printable ASCII
        m_QT_to_Key[Qt::Key_Any]            = vka::KeyCode::SPACE  ;//Key_Space,
        m_QT_to_Key[Qt::Key_Exclam]         = vka::KeyCode::EXCLAIM  ;/*0x21*/
        m_QT_to_Key[Qt::Key_QuoteDbl]       = vka::KeyCode::QUOTEDBL  ;/*0x22*/
        m_QT_to_Key[Qt::Key_NumberSign]     = vka::KeyCode::HASH  ;/*0x23*/
        m_QT_to_Key[Qt::Key_Dollar]         = vka::KeyCode::DOLLAR  ;/*0x24*/
        m_QT_to_Key[Qt::Key_Percent]        = vka::KeyCode::PERCENT  ;/*0x25*/
        m_QT_to_Key[Qt::Key_Ampersand]      = vka::KeyCode::AMPERSAND  ;/*0x26*/
        m_QT_to_Key[Qt::Key_Apostrophe]     = vka::KeyCode::QUOTE  ;/*0x27*/
        m_QT_to_Key[Qt::Key_ParenLeft]      = vka::KeyCode::LEFTPAREN  ;/*0x28*/
        m_QT_to_Key[Qt::Key_ParenRight]     = vka::KeyCode::RIGHTPAREN  ;/*0x29*/
        m_QT_to_Key[Qt::Key_Asterisk]       = vka::KeyCode::ASTERISK  ;/*0x2a*/
        m_QT_to_Key[Qt::Key_Plus]           = vka::KeyCode::PLUS  ;/*0x2b*/
        m_QT_to_Key[Qt::Key_Comma]          = vka::KeyCode::COMMA  ;/*0x2c*/
        m_QT_to_Key[Qt::Key_Minus]          = vka::KeyCode::MINUS  ;/*0x2d*/
        m_QT_to_Key[Qt::Key_Period]         = vka::KeyCode::PERIOD  ;/*0x2e*/
        m_QT_to_Key[Qt::Key_Slash]          = vka::KeyCode::SLASH  ;/*0x2f*/
        m_QT_to_Key[Qt::Key_0]              = vka::KeyCode::_0  ;/*0x30*/
        m_QT_to_Key[Qt::Key_1]              = vka::KeyCode::_1 ;/*0x31*/
        m_QT_to_Key[Qt::Key_2]              = vka::KeyCode::_2 ;/*0x32*/
        m_QT_to_Key[Qt::Key_3]              = vka::KeyCode::_3 ;/*0x33*/
        m_QT_to_Key[Qt::Key_4]              = vka::KeyCode::_4 ;/*0x34*/
        m_QT_to_Key[Qt::Key_5]              = vka::KeyCode::_5 ;/*0x35*/
        m_QT_to_Key[Qt::Key_6]              = vka::KeyCode::_6 ;/*0x36*/
        m_QT_to_Key[Qt::Key_7]              = vka::KeyCode::_7 ;/*0x37*/
        m_QT_to_Key[Qt::Key_8]              = vka::KeyCode::_8 ;/*0x38*/
        m_QT_to_Key[Qt::Key_9]              = vka::KeyCode::_9 ;/*0x39*/
        m_QT_to_Key[Qt::Key_Colon]          = vka::KeyCode::COLON  ;/*0x3a*/
        m_QT_to_Key[Qt::Key_Semicolon]      = vka::KeyCode::SEMICOLON  ;/*0x3b*/
        m_QT_to_Key[Qt::Key_Less]           = vka::KeyCode::LESS  ;/*0x3c*/
        m_QT_to_Key[Qt::Key_Equal]          = vka::KeyCode::EQUALS  ;/*0x3d*/
        m_QT_to_Key[Qt::Key_Greater]        = vka::KeyCode::GREATER  ;/*0x3e*/
        m_QT_to_Key[Qt::Key_Question]       = vka::KeyCode::QUESTION  ;/*0x3f*/
        m_QT_to_Key[Qt::Key_Alt]            = vka::KeyCode::LALT  ;/*0x40*/
        m_QT_to_Key[Qt::Key_A]              = vka::KeyCode::a  ;/*0x41*/
        m_QT_to_Key[Qt::Key_B]              = vka::KeyCode::b  ;/*0x42*/
        m_QT_to_Key[Qt::Key_C]              = vka::KeyCode::c  ;/*0x43*/
        m_QT_to_Key[Qt::Key_D]              = vka::KeyCode::d  ;/*0x44*/
        m_QT_to_Key[Qt::Key_E]              = vka::KeyCode::e  ;/*0x45*/
        m_QT_to_Key[Qt::Key_F]              = vka::KeyCode::f  ;/*0x46*/
        m_QT_to_Key[Qt::Key_G]              = vka::KeyCode::g  ;/*0x47*/
        m_QT_to_Key[Qt::Key_H]              = vka::KeyCode::h  ;/*0x48*/
        m_QT_to_Key[Qt::Key_I]              = vka::KeyCode::i  ;/*0x49*/
        m_QT_to_Key[Qt::Key_J]              = vka::KeyCode::j  ;/*0x4a*/
        m_QT_to_Key[Qt::Key_K]              = vka::KeyCode::k  ;/*0x4b*/
        m_QT_to_Key[Qt::Key_L]              = vka::KeyCode::l  ;/*0x4c*/
        m_QT_to_Key[Qt::Key_M]              = vka::KeyCode::m  ;/*0x4d*/
        m_QT_to_Key[Qt::Key_N]              = vka::KeyCode::n  ;/*0x4e*/
        m_QT_to_Key[Qt::Key_O]              = vka::KeyCode::o  ;/*0x4f*/
        m_QT_to_Key[Qt::Key_P]              = vka::KeyCode::p  ;/*0x50*/
        m_QT_to_Key[Qt::Key_Q]              = vka::KeyCode::q  ;/*0x51*/
        m_QT_to_Key[Qt::Key_R]              = vka::KeyCode::r  ;/*0x52*/
        m_QT_to_Key[Qt::Key_S]              = vka::KeyCode::s  ;/*0x53*/
        m_QT_to_Key[Qt::Key_T]              = vka::KeyCode::t  ;/*0x54*/
        m_QT_to_Key[Qt::Key_U]              = vka::KeyCode::u  ;/*0x55*/
        m_QT_to_Key[Qt::Key_V]              = vka::KeyCode::v  ;/*0x56*/
        m_QT_to_Key[Qt::Key_W]              = vka::KeyCode::w  ;/*0x57*/
        m_QT_to_Key[Qt::Key_X]              = vka::KeyCode::x  ;/*0x58*/
        m_QT_to_Key[Qt::Key_Y]              = vka::KeyCode::y  ;/*0x59*/
        m_QT_to_Key[Qt::Key_Z]              = vka::KeyCode::z  ;/*0x5a*/
        m_QT_to_Key[Qt::Key_BracketLeft]    = vka::KeyCode::LEFTBRACKET  ;/*0x5b*/
        m_QT_to_Key[Qt::Key_Backslash]      = vka::KeyCode::BACKSLASH  ;/*0x5c*/
        m_QT_to_Key[Qt::Key_BracketRight]   = vka::KeyCode::RIGHTBRACKET  ;/*0x5d*/
        m_QT_to_Key[Qt::Key_AsciiCircum]    = vka::KeyCode::CARET  ;/*0x5e*/
        m_QT_to_Key[Qt::Key_Underscore]     = vka::KeyCode::UNDERSCORE  ;/*0x5f*/
        m_QT_to_Key[Qt::Key_BraceLeft]      = vka::KeyCode::KP_LEFTBRACE  ;/*0x7b*/
        m_QT_to_Key[Qt::Key_BraceRight]     = vka::KeyCode::KP_RIGHTBRACE  ;/*0x7d*/
        m_QT_to_Key[Qt::Key_AsciiTilde]     = vka::KeyCode::BACKQUOTE  ;/*0x7e*/
 #if 0
        m_QT_to_Key[Qt::Key_QuoteLeft]      = vka::KeyCode::QUO  ;/*0x60*/
        m_QT_to_Key[Qt::Key_Bar]            = vka::KeyCode::  ;/*0x7c*/
        m_QT_to_Key[Qt::Key_nobreakspace]   = vka::KeyCode::  ;/*0x0a0*/
        m_QT_to_Key[Qt::Key_exclamdown]     = vka::KeyCode::  ;/*0x0a1*/
        m_QT_to_Key[Qt::Key_cent]           = vka::KeyCode::  ;/*0x0a2*/
        m_QT_to_Key[Qt::Key_sterling]       = vka::KeyCode::  ;/*0x0a3*/
        m_QT_to_Key[Qt::Key_currency]       = vka::KeyCode::  ;/*0x0a4*/
        m_QT_to_Key[Qt::Key_yen]            = vka::KeyCode::  ;/*0x0a5*/
        m_QT_to_Key[Qt::Key_brokenbar]      = vka::KeyCode::  ;/*0x0a6*/
        m_QT_to_Key[Qt::Key_section]        = vka::KeyCode::  ;/*0x0a7*/
        m_QT_to_Key[Qt::Key_diaeresis]      = vka::KeyCode::  ;/*0x0a8*/
        m_QT_to_Key[Qt::Key_copyright]      = vka::KeyCode::  ;/*0x0a9*/
        m_QT_to_Key[Qt::Key_ordfeminine]    = vka::KeyCode::  ;/*0x0aa*/
        m_QT_to_Key[Qt::Key_guillemotleft]  = vka::KeyCode::  ;/*0x0ab*/ // left angle quotation mark
        m_QT_to_Key[Qt::Key_notsign]        = vka::KeyCode::  ;/*0x0ac*/
        m_QT_to_Key[Qt::Key_hyphen]         = vka::KeyCode::  ;/*0x0ad*/
        m_QT_to_Key[Qt::Key_registered]     = vka::KeyCode::  ;/*0x0ae*/
        m_QT_to_Key[Qt::Key_macron]         = vka::KeyCode::  ;/*0x0af*/
        m_QT_to_Key[Qt::Key_degree]         = vka::KeyCode::  ;/*0x0b0*/
        m_QT_to_Key[Qt::Key_plusminus]      = vka::KeyCode::  ;/*0x0b1*/
        m_QT_to_Key[Qt::Key_twosuperior]    = vka::KeyCode::  ;/*0x0b2*/
        m_QT_to_Key[Qt::Key_threesuperior]  = vka::KeyCode::  ;/*0x0b3*/
        m_QT_to_Key[Qt::Key_acute]          = vka::KeyCode::  ;/*0x0b4*/
        m_QT_to_Key[Qt::Key_mu]             = vka::KeyCode::  ;/*0x0b5*/
        m_QT_to_Key[Qt::Key_paragraph]      = vka::KeyCode::  ;/*0x0b6*/
        m_QT_to_Key[Qt::Key_periodcentered] = vka::KeyCode::  ;/*0x0b7*/
        m_QT_to_Key[Qt::Key_cedilla]        = vka::KeyCode::  ;/*0x0b8*/
        m_QT_to_Key[Qt::Key_onesuperior]    = vka::KeyCode::  ;/*0x0b9*/
        m_QT_to_Key[Qt::Key_masculine]      = vka::KeyCode::  ;/*0x0ba*/
        m_QT_to_Key[Qt::Key_guillemotright] = vka::KeyCode::  ;/*0x0bb*/ // right angle quotation mark
        m_QT_to_Key[Qt::Key_onequarter]     = vka::KeyCode::  ;/*0x0bc*/
        m_QT_to_Key[Qt::Key_onehalf]        = vka::KeyCode::  ;/*0x0bd*/
        m_QT_to_Key[Qt::Key_threequarters]  = vka::KeyCode::  ;/*0x0be*/
        m_QT_to_Key[Qt::Key_questiondown]   = vka::KeyCode::  ;/*0x0bf*/
        m_QT_to_Key[Qt::Key_Agrave]         = vka::KeyCode::  ;/*0x0c0*/
        m_QT_to_Key[Qt::Key_Aacute]         = vka::KeyCode::  ;/*0x0c1*/
        m_QT_to_Key[Qt::Key_Acircumflex]    = vka::KeyCode::  ;/*0x0c2*/
        m_QT_to_Key[Qt::Key_Atilde]         = vka::KeyCode::BACKQUOTE  ;/*0x0c3*/
        m_QT_to_Key[Qt::Key_Adiaeresis]     = vka::KeyCode::  ;/*0x0c4*/
        m_QT_to_Key[Qt::Key_Aring]          = vka::KeyCode::  ;/*0x0c5*/
        m_QT_to_Key[Qt::Key_AE]             = vka::KeyCode::  ;/*0x0c6*/
        m_QT_to_Key[Qt::Key_Ccedilla]       = vka::KeyCode::  ;/*0x0c7*/
        m_QT_to_Key[Qt::Key_Egrave]         = vka::KeyCode::  ;/*0x0c8*/
        m_QT_to_Key[Qt::Key_Eacute]         = vka::KeyCode::  ;/*0x0c9*/
        m_QT_to_Key[Qt::Key_Ecircumflex]    = vka::KeyCode::  ;/*0x0ca*/
        m_QT_to_Key[Qt::Key_Ediaeresis]     = vka::KeyCode::  ;/*0x0cb*/
        m_QT_to_Key[Qt::Key_Igrave]         = vka::KeyCode::  ;/*0x0cc*/
        m_QT_to_Key[Qt::Key_Iacute]         = vka::KeyCode::  ;/*0x0cd*/
        m_QT_to_Key[Qt::Key_Icircumflex]    = vka::KeyCode::  ;/*0x0ce*/
        m_QT_to_Key[Qt::Key_Idiaeresis]     = vka::KeyCode::  ;/*0x0cf*/
        m_QT_to_Key[Qt::Key_ETH]            = vka::KeyCode::  ;/*0x0d0*/
        m_QT_to_Key[Qt::Key_Ntilde]         = vka::KeyCode::  ;/*0x0d1*/
        m_QT_to_Key[Qt::Key_Ograve]         = vka::KeyCode::  ;/*0x0d2*/
        m_QT_to_Key[Qt::Key_Oacute]         = vka::KeyCode::  ;/*0x0d3*/
        m_QT_to_Key[Qt::Key_Ocircumflex]    = vka::KeyCode::  ;/*0x0d4*/
        m_QT_to_Key[Qt::Key_Otilde]         = vka::KeyCode::  ;/*0x0d5*/
        m_QT_to_Key[Qt::Key_Odiaeresis]     = vka::KeyCode::  ;/*0x0d6*/
        m_QT_to_Key[Qt::Key_multiply]       = vka::KeyCode::  ;/*0x0d7*/
        m_QT_to_Key[Qt::Key_Ooblique]       = vka::KeyCode::  ;/*0x0d8*/
        m_QT_to_Key[Qt::Key_Ugrave]         = vka::KeyCode::  ;/*0x0d9*/
        m_QT_to_Key[Qt::Key_Uacute]         = vka::KeyCode::  ;/*0x0da*/
        m_QT_to_Key[Qt::Key_Ucircumflex]    = vka::KeyCode::  ;/*0x0db*/
        m_QT_to_Key[Qt::Key_Udiaeresis]     = vka::KeyCode::  ;/*0x0dc*/
        m_QT_to_Key[Qt::Key_Yacute]         = vka::KeyCode::  ;/*0x0dd*/
        m_QT_to_Key[Qt::Key_THORN]          = vka::KeyCode::  ;/*0x0de*/
        m_QT_to_Key[Qt::Key_ssharp]         = vka::KeyCode::  ;/*0x0df*/
        m_QT_to_Key[Qt::Key_division]       = vka::KeyCode::KP_DIVIDE  ;/*0x0f7*/
        m_QT_to_Key[Qt::Key_ydiaeresis]     = vka::KeyCode::  ;/*0x0ff*/
 #endif
    }

    int32_t m_oldMouseX;
    int32_t m_oldMouseY;
    std::map< int32_t, vka::KeyCode > m_QT_to_Key;
    //==========================================================================
public:
    // Application interface
    void initResources() override;
    void releaseResources()  override;
    void initSwapChainResources()  override;
    void releaseSwapChainResources()  override;
    void render(vkw::Frame &frame)  override;


};



}
}

#endif


