#ifndef VAK_ECS_UI_QTWINDOWSYSTEM_PROJECTTREEWIDGET_H
#define VAK_ECS_UI_QTWINDOWSYSTEM_PROJECTTREEWIDGET_H

#include <QTreeWidget>
#include <QDropEvent>

#include <optional>
#include <iostream>

#include <vka/ecs/SystemBus.h>
#include <vka/ecs/Events/EntityEvents.h>
#include <vka/ecs/Events/ResourceEvents.h>

/**
 * @brief The vkaProjectWidget class
 *
 * The widget for
 */
class vkaProjectWidget : public QTreeWidget
{
    Q_OBJECT

    QTreeWidgetItem * m_objectsRoot = nullptr;

    QTreeWidgetItem * m_assetsRoot  = nullptr;
    //QTreeWidgetItem * m_textures  = nullptr;
    //QTreeWidgetItem * m_textureCubes  = nullptr;
    //QTreeWidgetItem * m_materials  = nullptr;
    //QTreeWidgetItem * m_envs  = nullptr;
    //QTreeWidgetItem * m_scenes  = nullptr;
    //QTreeWidgetItem * m_primitives  = nullptr;

    std::map<QString, QTreeWidgetItem*> m_resourceItems;

    QMenu * m_menu = nullptr;
    using entity_type = vka::ecs::SystemBus::entity_type;

public:

    vkaProjectWidget(std::shared_ptr<vka::ecs::SystemBus> proj, QWidget* parent);

    void init()
    {
        setSelectionMode(QAbstractItemView::SingleSelection);
        setDragEnabled(false);
        viewport()->setAcceptDrops(true);
        setDropIndicatorShown(true);
        setDragDropMode(QAbstractItemView::InternalMove);

        if( m_objectsRoot) delete m_objectsRoot;
        if( m_assetsRoot)  delete m_assetsRoot;

        m_idToItem.clear();
        m_itemToId.clear();

        {
            m_objectsRoot = new QTreeWidgetItem(this);
            m_objectsRoot->setText(0, "Objects");
            m_objectsRoot->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDropEnabled);
            m_idToItem[entt::null] = m_objectsRoot;
        }
        {
            m_assetsRoot = new QTreeWidgetItem(this);
            m_assetsRoot->setText(0, "Assets");
            m_assetsRoot->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
        }
    }

    void onMenuAction(QString const action, entity_type id);

    void addItem(QString const & label, entity_type id);

    void onNewEntity(const vka::ecs::EvtEntityCreated &r);
    void onParentChanged(const vka::ecs::EvtParentChanged &r);

    void setParent(entity_type id, entity_type parent=entt::null);
    void onDestroyEntity(const vka::ecs::EvtEntityDestroyed &r);

signals:
    void entitySelected(entity_type e);
    void resourceSelected(uint32_t resourceId, QString resourceName);

private:
    std::map< entity_type, QTreeWidgetItem*> m_idToItem;
    std::map< QTreeWidgetItem*, entity_type> m_itemToId;

    using resource_pair = std::pair< uint32_t, QString>;
    std::map< resource_pair, QTreeWidgetItem*>    m_Resource_idToItem;
    std::map< QTreeWidgetItem*, resource_pair>    m_Resource_itemToId;

    std::shared_ptr< vka::ecs::SystemBus > m_systemBus;
    entity_type m_menuEntity= entt::null;
    virtual void  dropEvent(QDropEvent * event) override;

    QTreeWidgetItem *m_currentDrag=nullptr;
    QTreeWidgetItem *m_currentDragParent=nullptr;

    void dragEnterEvent(QDragEnterEvent *event) override
    {
        auto draggedItem = currentItem();
        std::cout <<  "Dragging: " << draggedItem->text(0).toStdString() << std::endl;
        m_currentDrag = draggedItem;
        m_currentDragParent=m_currentDrag->parent();
        QTreeWidget::dragEnterEvent(event);
    }
    //void dropEvent(QDropEvent *event) override
    //{
    //    QModelIndex droppedIndex = indexAt(event->pos());
    //    if( !droppedIndex.isValid() )
    //        return;
    //
    //    if(draggedItem){
    //        QTreeWidgetItem* dParent = draggedItem->parent();
    //        if(dParent){
    //            if(itemFromIndex(droppedIndex.parent()) != dParent)
    //                return;
    //            dParent->removeChild(draggedItem);
    //            dParent->insertChild(droppedIndex.row(), draggedItem);
    //        }
    //    }
    //}
    //QTreeWidgetItem* draggedItem=nullptr;
};


#endif
