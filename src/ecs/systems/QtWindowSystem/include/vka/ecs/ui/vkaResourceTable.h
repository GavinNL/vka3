#ifndef VAK_ECS_UI_QTWINDOWSYSTEM_RESOURCE_TABLE_H
#define VAK_ECS_UI_QTWINDOWSYSTEM_RESOURCE_TABLE_H

#include <QTableWidget>
#include <QTreeWidget>
#include <QDropEvent>
#include <QPushButton>

#include <optional>
#include <iostream>

#include <vka/ecs/SystemBus.h>
#include <vka/ecs/Events/EntityEvents.h>
#include <vka/ecs/Events/ResourceEvents.h>
#include <vka/ecs/ResourceObjects/GltfScene.h>
#include <vka/ecs/ResourceObjects/Environment.h>

/**
 * @brief The vkaProjectWidget class
 *
 * The widget for
 */
class vkaResourceTable : public QTreeWidget
{
    Q_OBJECT

    using entity_type = vka::ecs::SystemBus::entity_type;

public:

    vkaResourceTable(std::shared_ptr<vka::ecs::SystemBus> proj, QWidget* parent) : QTreeWidget(parent), m_systemBus(proj)
    {
        m_checkTimer = startTimer( 1000 );

//        setColumnCount(3);
//
//        setHorizontalHeaderLabels( {
//                                       "ID",
//                                       "Host",
//                                       "Device"
//                                   });
//        setRowCount(10);

        this->setRootIsDecorated(false);

        // column    info
        //  0        id
        //  1        type (material)
        //  2        in host
        //  3        in gpu
        //  4        path
        this->setColumnCount(5);

        //auto item = new QTreeWidgetItem(this);
        //item->setText(0, "name");
        //item->setText(1, "true");
        //item->setText(2, "false");
        //this->insertTopLevelItem( 0, item);

        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::HostTexture2D>     >().connect< &vkaResourceTable::onResourceEvent<vka::ecs::HostTexture2D>     >(*this);
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::HostTextureCube>   >().connect< &vkaResourceTable::onResourceEvent<vka::ecs::HostTextureCube>   >(*this);
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::HostMeshPrimitive> >().connect< &vkaResourceTable::onResourceEvent<vka::ecs::HostMeshPrimitive> >(*this);

        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::PBRMaterial> >().connect< &vkaResourceTable::onResourceEvent<vka::ecs::PBRMaterial> >(*this);
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::GLTFScene2> >().connect< &vkaResourceTable::onResourceEvent<vka::ecs::GLTFScene2> >(*this);
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::Environment> >().connect< &vkaResourceTable::onResourceEvent<vka::ecs::Environment> >(*this);
        //proj->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::HostTexture2D> >().connect< &vkaResourceTable::onHostLoadedTex >(*this);
        //proj->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::HostTextureCube> >().connect< &vkaResourceTable::onHostLoadedCube >(*this);
        //proj->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::HostMeshPrimitive> >().connect< &vkaResourceTable::onHostLoadedPrim >(*this);

    }

    std::map< vka::ecs::MeshPrimitive_ID, QTreeWidgetItem*> m_meshToItem;
    std::map< vka::ecs::Texture2D_ID, QTreeWidgetItem*> m_textureToItem;
    std::map< vka::ecs::TextureCube_ID, QTreeWidgetItem*> m_cubeToItem;
    std::map< vka::ecs::Scene2_ID, QTreeWidgetItem*> m_sceneToItem;
    std::map< vka::ecs::Environment_ID, QTreeWidgetItem*> m_envToItem;
    std::map< vka::ecs::PBRMaterial_ID, QTreeWidgetItem*> m_matToItem;

    template<typename RTYPE>
    std::map< vka::ecs::_ID<RTYPE>, QTreeWidgetItem*> & _getMap()
    {
        if constexpr ( std::is_same_v<RTYPE, vka::ecs::HostMeshPrimitive>)
        {
            return m_meshToItem;
        }
        else if constexpr ( std::is_same_v<RTYPE, vka::ecs::HostTextureCube>)
        {
                return m_cubeToItem;
        }
        else if constexpr ( std::is_same_v<RTYPE, vka::ecs::HostTexture2D>)
        {
                return m_textureToItem;
        }
        else if constexpr ( std::is_same_v<RTYPE, vka::ecs::GLTFScene2>)
        {
                return m_sceneToItem;
        }
        else if constexpr ( std::is_same_v<RTYPE, vka::ecs::Environment>)
        {
                return m_envToItem;
        }
        else if constexpr ( std::is_same_v<RTYPE, vka::ecs::PBRMaterial>)
        {
                return m_matToItem;
        }
        throw std::runtime_error("invlid");
    }


    ~vkaResourceTable()
    {
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::HostTexture2D>     >().disconnect< &vkaResourceTable::onResourceEvent<vka::ecs::HostTexture2D>     >(*this);
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::HostTextureCube>   >().disconnect< &vkaResourceTable::onResourceEvent<vka::ecs::HostTextureCube>   >(*this);
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::HostMeshPrimitive> >().disconnect< &vkaResourceTable::onResourceEvent<vka::ecs::HostMeshPrimitive> >(*this);
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::PBRMaterial>       >().disconnect< &vkaResourceTable::onResourceEvent<vka::ecs::PBRMaterial> >(*this);
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::GLTFScene2>        >().disconnect< &vkaResourceTable::onResourceEvent<vka::ecs::GLTFScene2> >(*this);
        m_systemBus->dispatcher.sink< vka::ecs::EvtResourceEvent_t<vka::ecs::Environment>       >().disconnect< &vkaResourceTable::onResourceEvent<vka::ecs::Environment> >(*this);
    }

    template<typename T>
    void onResourceEvent( vka::ecs::EvtResourceEvent_t<T> x)
    {
        using RTYPE  = T;
        using IDTYPE = vka::ecs::_ID<RTYPE>;

        auto getItem = [this]( IDTYPE id)
        {
            auto & _map = _getMap<RTYPE>();
            if( _map.count(id))
                return _map[id];

            auto item = new QTreeWidgetItem(this);

            auto & M = m_systemBus->getResourceManager<RTYPE>();
            auto name = QString(M.getResourceName(id).c_str());
            item->setText(0,  QString( std::to_string(id.value()).c_str() ) );
            item->setText(1,  RTYPE::_subType);
            item->setText(2, "false");
            item->setText(3, "false");
            item->setText(4, name);
            this->insertTopLevelItem( 0, item);

            _map[id] = item;
            return item;
        };

        auto item = getItem(x.id);
        if( x.type == vka::ecs::ResourceEvtType::HOST_LOADED)
        {
            item->setText(2, "true");
        }
        if( x.type == vka::ecs::ResourceEvtType::HOST_UNLOADED)
        {
            item->setText(2, "false");
        }
        if( x.type == vka::ecs::ResourceEvtType::DEVICE_LOADED)
        {
            item->setText(3, "true");
        }
        if( x.type == vka::ecs::ResourceEvtType::DEVICE_UNLOADED)
        {
            item->setText(3, "false");
        }
        this->repaint();
    }

    void init()
    {

    }
    int m_checkTimer=-1;


private:
    std::shared_ptr< vka::ecs::SystemBus > m_systemBus;



    // QObject interface
protected:
    void timerEvent(QTimerEvent *event)
    {
        if( m_checkTimer == event->timerId() )
        {
            //this->repaint();
            //std::cout << "tick" << std::endl;
        }
    }
};


#endif
