#ifndef VAK_ECS_UI_QTWINDOWSYSTEM_COMPONENTINSPECTOR_H
#define VAK_ECS_UI_QTWINDOWSYSTEM_COMPONENTINSPECTOR_H

#include <QTreeWidget>
#include <QDropEvent>
#include <QJsonObject>
#include <QJsonDocument>

#include <optional>
#include <iostream>

#include <vka/ecs/SystemBus.h>
#include <vka/ecs/Events/EntityEvents.h>
#include <vka/ecs/Events/ResourceEvents.h>

/**
 * @brief The vkaProjectWidget class
 *
 * The widget for
 */
class vkaComponenInspector : public QWidget
{
    using entity_type = vka::ecs::SystemBus::entity_type;

public:

    vkaComponenInspector(std::shared_ptr<vka::ecs::SystemBus> proj, QWidget* parent);

    void init()
    {
    }

    void showEntity(entity_type id);

    template<typename R>
    void showResource( vka::ecs::_ID<R> v);

    static nlohmann::json convertJson(const QJsonObject & J)
    {
        auto ba = QJsonDocument(J).toJson().toStdString();
        return nlohmann::json::parse(ba);
    }
    static QJsonObject convertJson(const nlohmann::json &in)
    {
        QJsonObject obj;
        std::ostringstream SS;
        SS << in.dump();
        auto str = SS.str();
        QByteArray ba(str.data(), static_cast<int>(str.size()));
        QJsonDocument doc = QJsonDocument::fromJson(ba);

        // check validity of the document
        if(!doc.isNull())
        {
            if(doc.isObject())
            {
                obj = doc.object();
            }
            else
            {
                return {};
            }
        }
        else
        {
            return {};
        }

        return obj;
    }

    void clearWidgets();

private:
    std::shared_ptr< vka::ecs::SystemBus > m_systemBus;
};


#endif
