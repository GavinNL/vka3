#include <vka/ecs/ui/vkaComponentInspector.h>

#include <QStyle>
#include <QAction>
#include <QMenu>
#include <QVBoxLayout>
#include <typeindex>
#include <type_traits>

#include <vka/ecs/ResourceObjects/HostTexture.h>
#include <vka/ecs/ResourceObjects/GltfScene.h>
#include <vka/ecs/ResourceObjects/Environment.h>
#include <vka/ecs/ResourceObjects/PBRMaterial.h>

#include <vka/ecs/Components/CTransform.h>

#include <vka/ecs/Components/Schemas.h>

#include <QJForm/qjform.h>
#include <QJForm/CollapsableWidget.h>

vkaComponenInspector::vkaComponenInspector(std::shared_ptr<vka::ecs::SystemBus> proj, QWidget *parent) : QWidget(parent), m_systemBus(proj)
{
    resize(200, 300);

    auto L = new QVBoxLayout();
    this->setLayout( L );
}

template<typename Comp>
static void _addwidget(QVBoxLayout * l, entt::entity id, std::shared_ptr<vka::ecs::SystemBus> sb, QWidget* parent)
{
    auto & r = sb->registry;


    if( r.has<Comp>(id) )
    {
        auto & C = r.get<Comp>(id);

        auto lay = new QVBoxLayout() ;
        auto cent = new QFrame();
        cent->setFrameShape(QFrame::StyledPanel);
        lay->setMargin(0);
        cent->setLayout(lay);




        QJForm::CollapsableWidget * coll = new QJForm::CollapsableWidget(C._type, parent);
        QJForm::QJForm * F = new QJForm::QJForm(parent);

        F->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
        cent->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));

        //auto x = new QWidget();
        //x->setMinimumHeight(250);
        lay->addWidget(F);

        lay->addWidget(new QPushButton("Hello"));

        coll->setWidget(cent);
        coll->setSizePolicy(QSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding));
        F->setSchema( vkaComponenInspector::convertJson(vka::ecs::toSchema(C, sb)));

        F->connect( F, &QJForm::QJForm::changed,[F, sb, id]()
        {
            auto j = vkaComponenInspector::convertJson( F->get() );
            std::cout << j.dump(4) << std::endl;
            Comp value;
            try {
                vka::ecs::fromJson(value, j, sb);
                sb->registry.emplace_or_replace<Comp>(id, std::move(value));
            } catch (...) {

            }
        });

        coll->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding));
        //l->addRow(coll);
        l->addWidget(coll, 0);
    }
}

void vkaComponenInspector::clearWidgets()
{
    if ( layout() != NULL )
    {
        QLayoutItem* item;
        while ( ( item = layout()->takeAt( 0 ) ) != NULL )
        {
            delete item->widget();
            delete item;
        }
        //delete layout();
    }
}


template<typename RTYPE>
QJForm::CollapsableWidget* createResourceWidget(QWidget* parent, vka::ecs::_ID<RTYPE> v,std::shared_ptr<vka::ecs::SystemBus> m_systemBus)
{
    auto & M = m_systemBus->getResourceManager<RTYPE>();
    auto & C = M.getResource(v);

    auto j = vka::ecs::toSchema(C, m_systemBus);


    auto name = M.getResourceName(v);

    {
        QJForm::CollapsableWidget * coll = new QJForm::CollapsableWidget( RTYPE::_subType , parent);

        QJForm::QJForm * F = new QJForm::QJForm(parent);

        F->setSchema( vkaComponenInspector::convertJson(j));

        coll->setWidget(F);

        auto sb = m_systemBus;
        F->connect( F, &QJForm::QJForm::changed,[F, sb, name]()
        {
            auto j1 = vkaComponenInspector::convertJson( F->get() );

//            vka::ecs::fromJson(value, j, sb);
  //          sb->registry.emplace_or_replace<Comp>(id, std::move(value));
            std::cout << j1.dump(4) << std::endl;
        });

        coll->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding));
        //l->addRow(coll);
        //l->addWidget(coll, 0);
        return coll;
    }
}

template<>
void vkaComponenInspector::showResource<vka::ecs::Environment>( vka::ecs::Environment_ID v)
{
    using RTYPE = vka::ecs::Environment;
    clearWidgets();

    auto l = dynamic_cast<QVBoxLayout*>(layout());
    {
        auto coll = createResourceWidget<RTYPE>(this, v, m_systemBus);
        coll->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding));
        l->addWidget(coll, 0);
    }
}

template<>
void vkaComponenInspector::showResource<vka::ecs::PBRMaterial>( vka::ecs::PBRMaterial_ID v)
{
    using RTYPE = vka::ecs::PBRMaterial;
    clearWidgets();

    auto l = dynamic_cast<QVBoxLayout*>(layout());
    {
        auto coll = createResourceWidget<RTYPE>(this, v, m_systemBus);
        coll->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding));
        l->addWidget(coll, 0);
    }
}

template<>
void vkaComponenInspector::showResource<vka::ecs::GLTFScene2>( vka::ecs::Scene2_ID v)
{
    using RTYPE = vka::ecs::GLTFScene2;
    clearWidgets();

    auto l = dynamic_cast<QVBoxLayout*>(layout());
    {
        auto coll = createResourceWidget<RTYPE>(this, v, m_systemBus);
        coll->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding));
        l->addWidget(coll, 0);
    }
}

template<>
void vkaComponenInspector::showResource<vka::ecs::HostMeshPrimitive>( vka::ecs::MeshPrimitive_ID v)
{
    using RTYPE = vka::ecs::HostMeshPrimitive;;
    clearWidgets();

    auto l = dynamic_cast<QVBoxLayout*>(layout());
    {
        auto coll = createResourceWidget<RTYPE>(this, v, m_systemBus);
        coll->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding,QSizePolicy::MinimumExpanding));
        l->addWidget(coll, 0);
    }
}


void vkaComponenInspector::showEntity(entity_type id)
{
    if ( layout() != NULL )
    {
        QLayoutItem* item;
        while ( ( item = layout()->takeAt( 0 ) ) != NULL )
        {
            delete item->widget();
            delete item;
        }
        //delete layout();
    }

    auto l = dynamic_cast<QVBoxLayout*>(layout());

    _addwidget<vka::ecs::CPosition>(l, id, m_systemBus, this);
    _addwidget<vka::ecs::CRotation>(l, id, m_systemBus, this);
    _addwidget<vka::ecs::CScale>(l, id, m_systemBus, this);
    _addwidget<vka::ecs::CCamera>(l, id, m_systemBus, this);
    _addwidget<vka::ecs::CScene>(l, id, m_systemBus, this);
    _addwidget<vka::ecs::CMesh>(l, id, m_systemBus, this);
    _addwidget<vka::ecs::CCollider>(l, id, m_systemBus, this);
    _addwidget<vka::ecs::CWSADMovement>(l, id, m_systemBus, this);
    _addwidget<vka::ecs::CMouseLook>(l, id, m_systemBus, this);
        //coll->setSizePolicy(QSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum));
    //l->addWidget(new QWidget(), 1);

}
