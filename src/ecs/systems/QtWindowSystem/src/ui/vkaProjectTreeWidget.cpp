#include <vka/ecs/ui/vkaProjectTreeWidget.h>

#include <QAction>
#include <QMenu>

#include <typeindex>
#include <type_traits>

#include <vka/ecs/ResourceObjects/HostTexture.h>
#include <vka/ecs/ResourceObjects/GltfScene.h>
#include <vka/ecs/ResourceObjects/Environment.h>
#include <vka/ecs/ResourceObjects/PBRMaterial.h>
#include <vka/ecs/ResourceObjects/Schemas.h>

vkaProjectWidget::vkaProjectWidget(std::shared_ptr<vka::ecs::SystemBus> proj, QWidget *parent) : QTreeWidget(parent), m_systemBus(proj)
{
    resize(200, 300);

    init();



    proj->dispatcher.sink<vka::ecs::EvtEntityDestroyed>().connect<&vkaProjectWidget::onDestroyEntity>(*this);
    proj->dispatcher.sink<vka::ecs::EvtEntityCreated>().connect<&vkaProjectWidget::onNewEntity>(*this);
    proj->dispatcher.sink<vka::ecs::EvtParentChanged>().connect<&vkaProjectWidget::onParentChanged>(*this);

    //============================================
    m_menu = new QMenu(this);

    {
        QAction *newAct = new QAction(QIcon(":/Resource/warning32.ico"), tr("&New"), this);
        newAct->setStatusTip(tr("new sth"));
        connect(newAct, &QAction::triggered, [this](bool b)
        {
            (void)b;
            if( m_menuEntity != entt::null)
            {
                this->m_systemBus->createEntity(m_menuEntity);
            }
            else
            {
                this->m_systemBus->createEntity();
            }
        });
        m_menu->addAction(newAct);
    }
    {
        QAction *newAct = new QAction(QIcon(":/Resource/warning32.ico"), tr("&Destroy"), this);
        newAct->setStatusTip(tr("new sth"));
        connect(newAct, &QAction::triggered, [this](bool b)
        {
            (void)b;
            if( m_menuEntity != entt::null)
            {
                this->m_systemBus->destroyEntityLater(this->m_menuEntity);
            }
         //   std::cout << "Pressed " << b << std::endl;
        });
        m_menu->addAction(newAct);
    }
    //QPoint pt(pos);
    //menu.exec( tree->mapToGlobal(pos) );
    //============================================
    setContextMenuPolicy(Qt::CustomContextMenu);

    auto prepareMenu = [this]( const QPoint & pos )
    {
        QTreeWidget *tree = this;
        QTreeWidgetItem *nd = tree->itemAt( pos );
        if( m_itemToId.count(nd))
        {
            this->m_menuEntity = this->m_itemToId.at(nd);
        }
        else
        {
            this->m_menuEntity = entt::null;
        }
        m_menu->exec( tree->mapToGlobal(pos));
    };

    connect(this,&QTreeWidget::customContextMenuRequested,prepareMenu);

    connect(this,&QTreeWidget::itemClicked, [this](QTreeWidgetItem* i, int ccol)
    {
        if( i == this->m_assetsRoot)
            return;

        if( m_itemToId.count(i))
        {
            auto selEntity = this->m_itemToId.at(i);

            emit this->entitySelected(selEntity);
            (void)selEntity;
        }

        if( m_Resource_itemToId.count(i))
        {
            auto par = m_Resource_itemToId.at(i);
            emit this->resourceSelected(par.first, par.second);
        }

        (void)ccol;

    });
}

void vkaProjectWidget::onMenuAction(const QString action, entity_type id)
{
    (void)action;
    (void)id;
}

void vkaProjectWidget::addItem(const QString &label, entity_type id)
{
    QTreeWidgetItem* childItem  = nullptr;
    QTreeWidgetItem* parentItem = m_objectsRoot;

    childItem = new QTreeWidgetItem();
    childItem->setText(0, label);
    childItem->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDropEnabled | Qt::ItemIsDragEnabled );

    m_idToItem[id] = childItem;
    m_itemToId[childItem] = id;

    parentItem->addChild(childItem);
}

void vkaProjectWidget::setParent(entity_type id, entity_type parent)
{
    QTreeWidgetItem* childItem  = m_idToItem.at(id);
    QTreeWidgetItem* parentItem = m_idToItem.at(parent);;

    parentItem->addChild(childItem);
}


void vkaProjectWidget::onDestroyEntity( vka::ecs::EvtEntityDestroyed const & r )
{
    auto id = r.entity;

    auto childItem = m_idToItem.at(id);
    m_idToItem.erase(id);
    m_itemToId.erase(childItem);

    delete childItem;



}


void vkaProjectWidget::onNewEntity( vka::ecs::EvtEntityCreated const & r )
{
    auto name = m_systemBus->getEntityName(r.entity);

    addItem( QString(name.c_str()), r.entity);
}

void vkaProjectWidget::onParentChanged( vka::ecs::EvtParentChanged const & r )
{
    auto name = m_systemBus->getEntityName(r.entity);

    QTreeWidgetItem* childItem  = m_idToItem.at(r.entity);
    QTreeWidgetItem* parentItem = m_idToItem.at(r.oldParent);

    parentItem->removeChild(childItem);

    setParent(r.entity, r.newParent);
}

void vkaProjectWidget::dropEvent(QDropEvent *event)
{
    QModelIndex droppedIndex = indexAt( event->pos() );

    if( !droppedIndex.isValid() )
    {
        event->ignore();
        return;
    }

    auto parentDropItem = this->itemFromIndex(droppedIndex);


    if( parentDropItem == m_objectsRoot || m_itemToId.count(parentDropItem) )
    {
    event->ignore();
        if( m_itemToId.count(parentDropItem) )
        {
            m_systemBus->linkToParent( m_itemToId.at(m_currentDrag), m_itemToId.at(parentDropItem));
        }
        else
        {
            m_systemBus->unlinkFromParent( m_itemToId.at(m_currentDrag));
        }
    }
}
