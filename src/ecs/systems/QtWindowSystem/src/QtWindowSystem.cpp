#include <vka/ecs/Events/InputEvents.h>
#include <vka/ecs/RenderSystem3.h>

#include <vkff/framegraph.h>

#include <glslang/Public/ShaderLang.h>
#include <vka/ecs/QtWindowSystem.h>

namespace vka
{

namespace ecs
{


VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;
    #define VKA_INFO(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_WARN(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_ERROR(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_DEBUG(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}


void QtWindowSystem::onStart()
{
    requestNextFrame();
}

void QtWindowSystem::onStop()
{
}

void QtWindowSystem::onUpdate()
{
}

void QtWindowSystem::initResources()
{
    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    assert(rs);
    vka::ecs::RenderSystem3CreateInfo ii;

    ii.device         = getDevice();
    ii.physicalDevice = getPhysicalDevice();
    ii.instance       = getInstance();

    rs->init(ii);
}

void QtWindowSystem::releaseResources()
{
    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    assert(rs);
    rs->releaseResources();
}

void QtWindowSystem::initSwapChainResources()
{
    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    assert(rs);
    rs->initSwapchainResources(swapchainImageSize(),
                               static_cast<uint32_t>(QtVulkanWidget::concurrentFrameCount()),
                               getDefaultRenderPass());
}

void QtWindowSystem::releaseSwapChainResources()
{
    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    assert(rs);
    rs->releaseSwapchainResources();
}

void QtWindowSystem::render(vkw::Frame &frame)
{
    vkff::FrameGraphExecuteInfo info;
    info.commandBuffer        = frame.commandBuffer;
    info.swapchainImage       = frame.swapchainImage;
    info.swapchainExtent      = frame.swapchainSize;
    info.swapchainImageView   = frame.swapchainImageView;
    info.swapchainFrameIndex  = frame.swapchainIndex;
    info.swapchainFormat      = vk::Format(frame.swapchainFormat);
    info.swapchainRenderPass  = frame.renderPass;
    info.swapchainFramebuffer = frame.framebuffer;
    info.swapchainDepthFormat = vk::Format(frame.depthFormat);

    double dt = 0.016;//frameTime();
    getSystemBus()->step(dt);

    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    if( rs )
    {
        // if the rendersystem exists
        // then pass it to the rendersystem
        rs->render(info);
    }
    else
    {
        // otherwise do basic pass
        frame.beginRenderPass( frame.commandBuffer );
        frame.endRenderPass(frame.commandBuffer);
    }
    requestNextFrame();
}

}
}


