#ifndef VKA_ECS_TRANSFORMSYSTEM_H
#define VKA_ECS_TRANSFORMSYSTEM_H

#include <vka/ecs/SystemBase2.h>

namespace vka
{
namespace ecs
{

struct TransformSystem : public vka::ecs::SystemBaseInternal
{
    ~TransformSystem()
    {
        S_INFO("TransformSystem Destroyed");
    }


    void onConstruct() override;
    void onStart() override;
    void onStop() override;
    void onUpdate() override;

    std::string name() const override
    {
        return "TransformSystem";
    }

    void   onUpdateTransformComponent(   registry_type &r, entity_type e );
    void   onConstructTransformComponent(   registry_type &r, entity_type e );
    void   onDestroyTransformComponent(   registry_type &r, entity_type e );



    void      onUpdateCPosition(   registry_type &r, entity_type e );
    void   onConstructCPosition(   registry_type &r, entity_type e );
    void     onDestroyCPosition(   registry_type &r, entity_type e );

    void      onUpdateCRotation(   registry_type &r, entity_type e );
    void   onConstructCRotation(   registry_type &r, entity_type e );
    void     onDestroyCRotation(   registry_type &r, entity_type e );

    void      onUpdateCScale(   registry_type &r, entity_type e );
    void   onConstructCScale(   registry_type &r, entity_type e );
    void     onDestroyCScale(   registry_type &r, entity_type e );

    // SystemBase interface
public:
    void onBuildComponents(entt::entity e, const nlohmann::json &components) override;
};



}
}


#endif
