#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/TransformSystem.h>
#include <vka/ecs/Components/Tags.h>
#include <vka/ecs/Components/CCamera.h>
#include <easy/profiler.h>

namespace vka
{
namespace ecs
{


template<>
json serialize<CPosition>(SystemBus const & S, CPosition const & P)
{
    (void)S;
    json j;
    j = { P.x, P.y, P.z};
    return j;
}

template<>
json serialize<CScale>(SystemBus const & S, CScale const & P)
{
    (void)S;
    json j;
    j = { P.x, P.y, P.z};
    return j;
}

template<>
json serialize<CRotation>(SystemBus const & S, CRotation const & P)
{
    (void)S;
    json j;
    j = { P.x, P.y, P.z, P.w};
    return j;
}

template<>
json serialize<CCamera>(SystemBus const & S, CCamera const & P)
{
    (void)S;
    json j;

    j["fieldOfViewDegrees"] =  P.fieldOfViewDegrees;
    j["nearPlane"]          =  P.nearPlane;
    j["farPlane"]           =  P.farPlane ;

    auto & M = S.getResourceManager<Environment>();
    if( P.environment.valid() )
    {
        j["environment"] = M.getUri(P.environment).toString();
    }
    return j;
}

void TransformSystem::onConstruct()
{
    getSystemBus()->registerSerializer<CPosition>();
    getSystemBus()->registerSerializer<CRotation>();
    getSystemBus()->registerSerializer<CScale>();
    getSystemBus()->registerSerializer<CCamera>();
}


void TransformSystem::onStart()
{
    on_construct<CPosition>().connect<&entt::registry::emplace_or_replace<CGlobalPosition>>();
    on_construct<CRotation>().connect<&entt::registry::emplace_or_replace<CGlobalRotation>>();


    on_construct<CGlobalPosition>().connect<&entt::registry::emplace_or_replace<tag_global_transform_update>>();
    on_construct<CGlobalRotation>().connect<&entt::registry::emplace_or_replace<tag_global_transform_update>>();
    on_update<CGlobalPosition>().connect<&entt::registry::emplace_or_replace<tag_global_transform_update>>();
    on_update<CGlobalRotation>().connect<&entt::registry::emplace_or_replace<tag_global_transform_update>>();


    on_construct<CPosition>().connect<&entt::registry::emplace_or_replace<tag_transform_update>>();
    on_construct<CRotation>().connect<&entt::registry::emplace_or_replace<tag_transform_update>>();
    on_construct<CScale   >().connect<&entt::registry::emplace_or_replace<tag_transform_update>>();

    on_update<CPosition>().connect<&entt::registry::emplace_or_replace<tag_transform_update>>();
    on_update<CRotation>().connect<&entt::registry::emplace_or_replace<tag_transform_update>>();
    on_update<CScale   >().connect<&entt::registry::emplace_or_replace<tag_transform_update>>();

    on_destroy<CPosition>().connect<&entt::registry::emplace_or_replace<tag_transform_update>>();
    on_destroy<CRotation>().connect<&entt::registry::emplace_or_replace<tag_transform_update>>();
    on_destroy<CScale   >().connect<&entt::registry::emplace_or_replace<tag_transform_update>>();

    on_construct<tag_transform_update >().connect<&entt::registry::get_or_emplace<CWorldTransform>>();

    for(auto e : view<CPosition>())
    {
        emplace_or_replace<CGlobalPosition>(e);
        emplace_or_replace<tag_transform_update>(e);
    }
    for(auto e : view<CRotation>())
    {
        emplace_or_replace<CGlobalPosition>(e);
        emplace_or_replace<tag_transform_update>(e);
    }
    for(auto e : view<CScale>())
    {
        emplace_or_replace<CGlobalPosition>(e);
        emplace_or_replace<tag_transform_update>(e);
    }
    S_INFO("TransformSystem Connected Successfully");

}

void TransformSystem::onStop()
{
    on_construct<CPosition>().disconnect<&entt::registry::emplace_or_replace<CGlobalPosition>>();
    on_construct<CRotation>().disconnect<&entt::registry::emplace_or_replace<CGlobalRotation>>();

    on_construct<CGlobalPosition>().disconnect<&entt::registry::emplace_or_replace<tag_global_transform_update>>();
    on_construct<CGlobalRotation>().disconnect<&entt::registry::emplace_or_replace<tag_global_transform_update>>();
    on_update<CGlobalPosition>().disconnect<&entt::registry::emplace_or_replace<tag_global_transform_update>>();
    on_update<CGlobalRotation>().disconnect<&entt::registry::emplace_or_replace<tag_global_transform_update>>();

   on_construct<CPosition>().disconnect<&entt::registry::emplace_or_replace<tag_transform_update>>();
   on_construct<CRotation>().disconnect<&entt::registry::emplace_or_replace<tag_transform_update>>();
   on_construct<CScale   >().disconnect<&entt::registry::emplace_or_replace<tag_transform_update>>();

   on_update<CPosition>().disconnect<&entt::registry::emplace_or_replace<tag_transform_update>>();
   on_update<CRotation>().disconnect<&entt::registry::emplace_or_replace<tag_transform_update>>();
   on_update<CScale   >().disconnect<&entt::registry::emplace_or_replace<tag_transform_update>>();

   on_destroy<CPosition>().disconnect<&entt::registry::emplace_or_replace<tag_transform_update>>();
   on_destroy<CRotation>().disconnect<&entt::registry::emplace_or_replace<tag_transform_update>>();
   on_destroy<CScale   >().disconnect<&entt::registry::emplace_or_replace<tag_transform_update>>();

   on_construct<tag_transform_update >().disconnect<&entt::registry::get_or_emplace<CWorldTransform>>();

   S_INFO("TransformSystem Disconnected Successfully");
}

static glm::mat4 computeModelMatrix(TransformSystem::registry_type &r, entt::entity e);


void TransformSystem::onUpdate()
{
    auto & r = getSystemBus()->registry;
    uint32_t count=0;
    (void)count;
    // Loop through all entities that have their transforms updated
    // set the world-space matrix.

    uint32_t ra=0;
    (void)ra;
    // first loop through the root entities.
    // - if they have children, update the children as well
    // - remove the tag_transform_updated tag from each of the
    // entities that have been updated.
    // if the root entity was updated, that means that ALL
    // children would have to be updated.
    //
    // Note that root entities don't have parents, so that we have to use
    // the identity matrix
    EASY_BLOCK("Updating Root Global Transforms")
    for(auto en : view< tag_transform_update, tag_is_root , CMain>())
    {
        if(!valid(en) )
            continue;

        getSystemBus()->traverseChildren(en,
        [&r](auto e, auto const & parentTransform)
        {
            vka::Transform myTransform;
            if( r.has<CScale>(e) )
            {
                myTransform.scale = r.get<CScale>(e);
            }
            if( r.has<CRotation>(e) )
            {
                myTransform.rotation = r.get<CRotation>(e);
            }
            if( r.has<CPosition>(e) )
            {
                myTransform.position = r.get<CPosition>(e);
            }
            myTransform = parentTransform * myTransform;
            r.emplace_or_replace<CGlobalScale>(e, myTransform.scale);
            r.emplace_or_replace<CGlobalRotation>(e, myTransform.rotation);
            r.emplace_or_replace<CGlobalPosition>(e, myTransform.position);
            r.emplace_or_replace<tag_global_transform_update>(e);

            r.remove_if_exists<tag_transform_update>(e);

            return myTransform;

        }, vka::Transform() );

        //ra += updateGlobalTransforms(r, e, glm::mat4(1.0f) );
        //updateGlobalComponents(*getSystemBus(), e);
        ++count;
    }
    EASY_END_BLOCK

    // now loop through all remaining entities
    // which updated transforms that also have parents.
    // ie: does not contain the tag_is_root tag.
    // update their worldspace matrix and then recurisvely
    // update all their childrens
    EASY_BLOCK("Updating Non-Root Global Transforms")
    for(auto en : view<tag_transform_update, CMain>( entt::exclude<tag_is_root>) )
    {
        if( !valid(en) )
            continue;

        auto & M  = get<CMain>(en);

        if( !has<CWorldTransform>(M.parent))
        {
            emplace<CWorldTransform>(M.parent, glm::mat4(1.0f) );
        }

        vka::Transform t;
        if( r.has<CGlobalScale>(M.parent))
            t.scale          = r.get<CGlobalScale   >( M.parent );
        if( r.has<CGlobalRotation>(M.parent))
            t.rotation    = r.get<CGlobalRotation>( M.parent );
        if( r.has<CGlobalPosition>(M.parent))
            t.position       = r.get<CGlobalPosition>( M.parent );

        getSystemBus()->traverseChildren(en,
        [&r](auto e, auto const & parentTransform)
        {
            vka::Transform myTransform;
            if( r.has<CScale>(e) )
            {
                myTransform.scale = r.get<CScale>(e);
            }
            if( r.has<CRotation>(e) )
            {
                myTransform.rotation = r.get<CRotation>(e);
            }
            if( r.has<CPosition>(e) )
            {
                myTransform.position = r.get<CPosition>(e);
            }
            myTransform = parentTransform * myTransform;
            r.emplace_or_replace<CGlobalScale>(e, myTransform.scale);
            r.emplace_or_replace<CGlobalRotation>(e, myTransform.rotation);
            r.emplace_or_replace<CGlobalPosition>(e, myTransform.position);
            r.emplace_or_replace<tag_global_transform_update>(e);

            r.remove_if_exists<tag_transform_update>(e);

            return myTransform;

        }, t );
    }
    EASY_END_BLOCK

    EASY_BLOCK("Calculating WorldTransform")
    for(auto e : view<tag_global_transform_update, CMain, CGlobalPosition, CGlobalRotation>() )
    {
        if( !valid(e) )
            continue;

        auto worldTransform = glm::mat4_cast( r.get<vka::ecs::CGlobalRotation>(e) ); //glm::translate(   r.get<vka::ecs::CGlobalPosition>(e) );
        worldTransform[3] = vec4(r.get<CGlobalPosition>(e),1.0f);

        if( r.has<CGlobalScale>(e))
            worldTransform = worldTransform * glm::scale( r.get<CGlobalScale>(e));

        emplace_or_replace<CWorldTransform>(e, worldTransform );
        remove_if_exists<tag_global_transform_update>(e);
    }
    EASY_END_BLOCK
}

// gets the transformation matrix based on the Postiion/Rotation/Scale matrix
static glm::mat4 computeModelMatrix(TransformSystem::registry_type &r, entt::entity e)
{
    // 000 - none
    // 001 - position only
    // 010 - rotation only
    // 100 - scale only;
    int v = r.has<vka::ecs::CPosition>(e);
    v |= r.has<vka::ecs::CRotation>(e) << 1;
    v |= r.has<vka::ecs::CScale>(e) << 2;

    switch(v)
    {
        case 0:
            return glm::mat4(1.0f);
        case 1:
            return glm::translate(  glm::mat4(1.0f), r.get<vka::ecs::CPosition>(e) );
        case 2:
            return glm::mat4_cast( r.get<vka::ecs::CRotation>(e) );
        case 3:
            return glm::translate(  glm::mat4(1.0f), r.get<vka::ecs::CPosition>(e) ) * glm::mat4_cast( r.get<vka::ecs::CRotation>(e) );
        case 4:
            return glm::scale( glm::mat4(1.0),  r.get<vka::ecs::CScale>(e));
        case 5:
            return glm::translate(  glm::mat4(1.0f), r.get<vka::ecs::CPosition>(e) ) *  glm::scale( glm::mat4(1.0),  r.get<vka::ecs::CScale>(e));
        case 6:
            return glm::mat4_cast( r.get<vka::ecs::CRotation>(e) ) * glm::scale( glm::mat4(1.0),  r.get<vka::ecs::CScale>(e));
        case 7:
            return glm::translate(  glm::mat4(1.0f), r.get<vka::ecs::CPosition>(e) ) * glm::mat4_cast( r.get<vka::ecs::CRotation>(e) ) * glm::scale( glm::mat4(1.0),  r.get<vka::ecs::CScale>(e));
    }
    throw std::runtime_error("invalid");
}

void TransformSystem::onBuildComponents(entt::entity e, const nlohmann::json &components)
{
    (void)computeModelMatrix;

    if( components.count("CPosition") )
    {
        glm::vec3 x;
        auto it = components.at("CPosition");
        if( it.is_object() )
        {
            x.x = it.at("x").get<float>();
            x.y = it.at("y").get<float>();
            x.z = it.at("z").get<float>();
        }
        if( it.is_array() )
        {
            auto ar = it.get< std::array<float,3> >();
            x.x = ar[0];
            x.y = ar[1];
            x.z = ar[2];
        }
        emplace_or_replace<CPosition>(e, x);
    }
    if( components.count("CScale") )
    {
        glm::vec3 x(1.0f);
        auto it = components.at("CScale");
        if( it.is_object() )
        {
            x.x = it.at("x").get<float>();
            x.y = it.at("y").get<float>();
            x.z = it.at("z").get<float>();
        }
        if( it.is_array() )
        {
            auto ar = it.get< std::array<float,3> >();
            x.x = ar[0];
            x.y = ar[1];
            x.z = ar[2];
        }
        emplace_or_replace<CScale>(e, x);
    }
    if( components.count("CRotation") )
    {
        glm::quat x;
        auto it = components.at("CRotation");
        if( it.is_object() )
        {
            x.x = it.at("x").get<float>();
            x.y = it.at("y").get<float>();
            x.z = it.at("z").get<float>();
            x.w = it.at("w").get<float>();
        }
        if( it.is_array() )
        {
            auto ar = it.get< std::array<float,4> >();
            x.x = ar[0];
            x.y = ar[1];
            x.z = ar[2];
            x.w = ar[3];
        }
        emplace_or_replace<CRotation>(e, x);
    }

    if( components.count("detach"))
    {
        auto T = getSystemBus()->calculateWorldSpaceTransform(e);
        auto & newPosition = T.position;
        auto & Q           = T.rotation;//m_systemBus->calculateWorldSpaceRotation(e);

        getSystemBus()->unlinkFromParent(e);
        emplace_or_replace<CPosition>(e, newPosition);
        emplace_or_replace<CRotation>(e, Q);
    }
}

}
}

