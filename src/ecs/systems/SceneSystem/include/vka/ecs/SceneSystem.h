#ifndef VKA_ECS_SCENE_SYSTEM_2_H
#define VKA_ECS_SCENE_SYSTEM_2_H


#include <map>
#include <list>
#include <set>


#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/Components/CMesh.h>

namespace vka
{
namespace ecs
{

// this tag will be applied to an entity with a CScene2
// to schedule a construction of all the
// sub nodes required by the scene
using  tag_scene_system_schedule_build = entt::tag<"tag_scene_system_schedule_build"_hs>;

struct SceneNodeComponent
{
    static constexpr auto _type = "SceneNodeComponent";
    entt::entity rootSceneComponent = entt::null;

    //==============================================================
    // Required
    //==============================================================
    json to_json() const
    {
        json J;
        J["rootSceneComponent"] = to_integral(rootSceneComponent);
        return J;
    }
    void from_json(const json & J)
    {
        (void)J;
    }
    //==============================================================

};

struct SceneSystem : public vka::ecs::SystemBaseInternal
{
    using scene_component_type = CScene;

    entt::observer sceneComponentTransformsUpdated;

    ~SceneSystem()
    {
        //S_INFO("SceneSystem Destroyed");
    }


    void onConstruct() override;
    void onStop() override;
    void onStart() override;
    void onUpdate() override;



    void clear();

    std::string name() const override
    {
        return "SceneSystem";
    }


    void reset(registry_type & r, scene_component_type &C);


public:
    void onBuildComponents(entity_type e, const nlohmann::json &components) override;
};



}
}


#endif
