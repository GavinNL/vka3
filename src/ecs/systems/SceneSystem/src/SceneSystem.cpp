#include <vka/ecs/SceneSystem.h>
#include <vka/ecs/Components/Tags.h>
#include <vka/ecs/Components/CPose.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Loaders/GLTFLoader.h>
#include <easy/profiler.h>


namespace vka
{
namespace ecs
{

struct SceneBuilderSystem : public SystemBaseInternal
{
    using update_scene = entt::tag<"SceneBuilder::update_scene"_hs>;

    void onStart() override
    {
        on_construct<CScene>().connect<&entt::registry::emplace_or_replace<update_scene>>();
        on_update<CScene>().connect<&entt::registry::emplace_or_replace<update_scene>>();
        on_destroy<CScene>().connect<&SceneBuilderSystem::onDestroySceneComponent>(*this);

        for(auto e : view<CScene>())
        {
            emplace_or_replace<update_scene>(e);
        }
    }
    void onStop() override
    {
        on_construct<CScene>().disconnect<&entt::registry::emplace_or_replace<update_scene>>();
        on_update<CScene>().disconnect<&entt::registry::emplace_or_replace<update_scene>>();
        on_destroy<CScene>().disconnect<&SceneBuilderSystem::onDestroySceneComponent>(*this);
        std::cout <<  "BuildColliders::onStop" << std::endl;
    }

    void onUpdate() override
    {
        auto sb = getSystemBus();
        sb->forEachResourceToHostUnload<GLTFScene2>( [sb](GLTFScene2 & P)
        {
            // remove any references to _ID< > so that
            // any refernces to primitives or textures gets decremnted
            // they can then be
            for(auto const & m : P.materials)
            {
                sb->resourceScheduleHostUnload(m);
            }
            for(auto const & m : P.meshes)
            {
                for(auto & p : m.primitives)
                {
                    sb->resourceScheduleHostUnload(p.mesh);
                }
            }
            P = GLTFScene2();
        });

        for(auto e : view<CScene, update_scene>() )
        {
            auto & G = get<CScene>(e);

            if( !G.sceneId.valid() )
                continue;

            // If the resource has been loaded, we can then build the scene
            if( resourceIsHostLoaded(G.sceneId) )
            {
                auto & S = getManagedResource<GLTFScene2>(G.sceneId);
                buildScene(S,e,G);
                remove_if_exists<update_scene>(e);
            }
            else
            {
                getSystemBus()->stageResource(G.sceneId);
            }
        }
    }

    void onDestroySceneComponent(registry_type & r, entity_type e)
    {
        (void)r;
        (void)e;
        if( !has<SceneNodeComponent>(e))
            return;
        auto & G = get<SceneNodeComponent>(e);

        if( r.valid(G.rootSceneComponent) )
        {
            if( r.has<CScene>(G.rootSceneComponent))
            {
                auto & R = get<CScene>(G.rootSceneComponent);
                for(auto & x : R.nodes)
                {
                    if( x==e)
                    {
                        x = entt::null;
                        return;
                    }
                }
            }
        }
    }

private:
#if 0
    entt::entity buildHierarchy(const vka::ecs::GLTFScene &S, uint32_t rootNode, std::vector<entt::entity> &nodes)
    {
        auto nE = createEntity();

        auto & M = get<CMain>(nE);
               M.name = S.nodes[rootNode].name;

        emplace_or_replace<tag_entity_dont_serialize>(nE);
        emplace_or_replace<CWorldTransform>(nE);
        emplace_or_replace<CPosition>(      nE, S.nodes[rootNode].transform.position);
        emplace_or_replace<CRotation>(      nE, S.nodes[rootNode].transform.rotation);
        emplace_or_replace<CScale>(         nE, S.nodes[rootNode].transform.scale);

        nodes[rootNode] = nE;

        for(auto c : S.nodes[rootNode].children)
        {
            // if this node is renderable (it has a mesh, or one of its descendents have a mesh)
            if( S.nodes[c].isRenderable)
            {
                auto childEntity = buildHierarchy(S, c, nodes);
                linkToParent( childEntity, nE);
            }
        }

        return nE;
    }

    void buildScene(const vka::ecs::GLTFScene &S,
                    entt::entity e,
                    CScene &G)
    {
        auto & Po = emplace_or_replace<CPose>(e);

        auto & Sc = S.scenes[ G.sceneIndex ];

        std::vector<entt::entity> nodes;
        nodes.insert( nodes.end(), S.nodes.size(), entt::null);

        for(auto rr : Sc.rootNodes)
        {
            // build the hierarchy for each root scene node
            // in the gltf
            auto sceneRoot = buildHierarchy(S,rr,nodes);
            linkToParent(sceneRoot, e);
        }

        // make sure that all the SceneNodes
        // know which entity is the real parent
        for(auto x : nodes)
        {
            if( valid(x))
                emplace<SceneNodeComponent>(x).rootSceneComponent = e;
        }

        Po.nodeSpaceTransforms = S.getNodeSpaceTransforms();
        G.nodes = std::move(nodes);

        for(uint32_t i=0;i<S.nodes.size();i++)
        {
            auto & n = S.nodes[i];

            if( n.skinIndex.has_value() )
            {
                auto & Sk = emplace<CSkeleton>( G.nodes[i] );
                Sk.boneMatrices.insert( Sk.boneMatrices.begin(), S.skins.at(*n.skinIndex).joints.size() , glm::mat4(1.0f));
            }

            if( n.meshIndex.has_value() )
            {
    #if 1
                auto & M = S.meshes.at(*n.meshIndex);

                // add the mesh component to the entity
                auto & GM = emplace<CMesh>( G.nodes[i] );

                emplace_or_replace<CPosition>( G.nodes[i], S.nodes[i].transform.position );//.position);
                emplace_or_replace<CRotation>( G.nodes[i], S.nodes[i].transform.rotation );//.rotation);
                emplace_or_replace<CScale>(    G.nodes[i], S.nodes[i].transform.scale );//.scale);

                for(auto & pi :  M.meshPrimitives )
                {
                    auto _id   = S.rawPrimitiveId.at( S.primitives.at(pi.primitiveIndex).rawPrimitiveIndex );
                    auto & mat = S.pbrMaterials.at(pi.materialIndex);

                    auto & PP    = GM.primitives.emplace_back();

                    PP.material  = mat;
                    PP.primitive = _id;


                    auto & dc = S.primitives.at(pi.primitiveIndex).drawCall;
                    PP.drawCall.firstIndex = dc.firstIndex;
                    PP.drawCall.indexCount = dc.indexCount;
                    PP.drawCall.vertexCount = dc.vertexCount;
                    PP.drawCall.vertexOffset = dc.vertexOffset;
                }
    #endif

            }
        }
    }

#endif

    entt::entity buildHierarchy(const vka::ecs::GLTFScene2 &S, uint32_t rootNode, std::vector<entt::entity> &nodes)
    {
        auto nE = createEntity();

        auto & M = get<CMain>(nE);
               M.name = S.nodes[rootNode].name;

        emplace_or_replace<tag_entity_dont_serialize>(nE);
        emplace_or_replace<CWorldTransform>(nE);
        emplace_or_replace<CPosition>(      nE, S.nodes[rootNode].transform.position);
        emplace_or_replace<CRotation>(      nE, S.nodes[rootNode].transform.rotation);
        emplace_or_replace<CScale>(         nE, S.nodes[rootNode].transform.scale);

        nodes[rootNode] = nE;

        for(auto c : S.nodes[rootNode].children)
        {
            // if this node is renderable (it has a mesh, or one of its descendents have a mesh)
            if( S.nodes[c].isRenderable)
            {
                auto childEntity = buildHierarchy(S, c, nodes);
                linkToParent( childEntity, nE);
            }
        }

        return nE;
    }

    void buildScene(const vka::ecs::GLTFScene2 &S,
                    entt::entity e,
                    CScene &G)
    {
        auto & Po = emplace_or_replace<CPose>(e);

        G.sceneIndex = std::clamp<uint32_t>( G.sceneIndex, 0, static_cast<uint32_t>(S.scenes.size()) );
        auto & Sc = S.scenes[ G.sceneIndex ];

        std::vector<entt::entity> nodes;
        nodes.insert( nodes.end(), S.nodes.size(), entt::null);

        for(auto rr : Sc.rootNodes)
        {
            // build the hierarchy for each root scene node
            // in the gltf
            auto sceneRoot = buildHierarchy(S,rr,nodes);
            linkToParent(sceneRoot, e);
        }

        // make sure that all the SceneNodes
        // know which entity is the real parent
        for(auto x : nodes)
        {
            if( valid(x))
                emplace<SceneNodeComponent>(x).rootSceneComponent = e;
        }

        Po.nodeSpaceTransforms = S.getNodeSpaceTransforms();
        G.nodes = std::move(nodes);

        for(uint32_t i=0;i<S.nodes.size();i++)
        {
            auto & n = S.nodes[i];

            if( n.skinIndex.has_value() )
            {
                auto & Sk = emplace<CSkeleton>( G.nodes[i] );
                Sk.boneMatrices.insert( Sk.boneMatrices.begin(), S.skins.at(*n.skinIndex).joints.size() , glm::mat4(1.0f));
            }

            if( n.meshIndex.has_value() )
            {
                auto & M = S.meshes.at(*n.meshIndex);

                // add the mesh component to the entity
                auto & GM = emplace<CMesh>( G.nodes[i] );

                emplace_or_replace<CPosition>( G.nodes[i], S.nodes[i].transform.position );//.position);
                emplace_or_replace<CRotation>( G.nodes[i], S.nodes[i].transform.rotation );//.rotation);
                emplace_or_replace<CScale>(    G.nodes[i], S.nodes[i].transform.scale );//.scale);

                for(auto & pi : M.primitives)
                {
                    auto & pp = GM.primitives.emplace_back( CPrimitive(pi.mesh, pi.material, vk::PrimitiveTopology::eTriangleList));
                    pp.subMeshIndex = pi.subMeshIndex;
                }
            }
        }
    }




};

}
}


void vka::ecs::SceneSystem::clear()
{

}


void vka::ecs::SceneSystem::onConstruct()
{
    getSystemBus()->registerSerializer<CScene>();
}

void vka::ecs::SceneSystem::onStart()
{
    addPostSubSystem<SceneBuilderSystem>();

    S_INFO("SceneSystem Connected Successfully");
}

void vka::ecs::SceneSystem::onStop()
{
    S_INFO("SceneSystem Disconnected Successfully");
}


void vka::ecs::SceneSystem::onUpdate()
{
}



void vka::ecs::SceneSystem::reset(registry_type &r, scene_component_type &C)
{
    for(auto e : C.nodes)
    {
        r.destroy(e);
    }
}


void vka::ecs::SceneSystem::onBuildComponents(entt::entity e, const nlohmann::json &components)
{
    if( components.count("SceneComponent") )
    {
        auto it = components.at("SceneComponent");
        if( it.is_string() )
        {
            auto uri = it.get<std::string>();
            vka::uri u(uri);
            auto uniqueName = std::to_string( std::hash<std::string>()(u.toString()) );
            auto & M = getResourceManager<vka::ecs::GLTFScene2>();

            auto id = M.findResource(uniqueName);

            if( !id.valid() )
            {
                id = getSystemBus()->resourceCreate<vka::ecs::GLTFScene2>( u );
            }
            emplace_or_replace<CScene>(e, id);
        }
    }

}

namespace vka
{
namespace ecs
{

template<>
json serialize<CScene>(SystemBus const & S, CScene const & P)
{
    (void)S;
    json j;

    if( P.sceneId.valid() )
    {
        auto & M = S.getResourceManager<GLTFScene2>();
        j["uri"]  = M.getUri(P.sceneId).toString();
        j["sceneIndex"] = P.sceneIndex;
    }
    return j;
}




}
}
