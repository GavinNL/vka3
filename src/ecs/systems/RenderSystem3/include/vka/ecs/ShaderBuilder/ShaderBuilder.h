#ifndef ECS_RENDERSYSTEM_FG_EXEUCTOR_SHADER_BUILDER_H
#define ECS_RENDERSYSTEM_FG_EXEUCTOR_SHADER_BUILDER_H

#include <vulkan/vulkan.hpp>
#include <string>
#include <sstream>
#include <fmt/format.h>
#include <regex>
#include <vka/math/linalg.h>

namespace vka
{
namespace ecs {

struct ShaderBuilder2
{
    using vec2 = glm::vec2;
    using vec3 = glm::vec3;
    using vec4 = glm::vec4;
    using mat4 = glm::mat4;

    struct GlobalUniform_t
    {
        #define RENDERER_GLOBAL_UNIFORM_VARS\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , SCREEN_WIDTH      , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , SCREEN_HEIGHT     , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , MOUSE_X           , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , MOUSE_Y           , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , MOUSE_Z           , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , TIME_INT          , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , TIME_FRAC         , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , UNUSED1           , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(vec3  , MOUSE_NEAR        , vec3(0.0f)    )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , UNUSED2           , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(vec3  , MOUSE_FAR         , vec3(0.0f)    )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , UNUSED3           , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(vec3  , CAMERA_POSITION   , vec3(0.0f)    )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(float , UNUSED4           , 0.0           )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(mat4  , VIEW              , mat4(1.0f)    )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(mat4  , PROJECTION        , mat4(1.0f)    )\
            RENDERER_GLOBAL_UNIFORM_X_MACRO(mat4  , PROJECTION_VIEW   , mat4(1.0f)    )

        #define RENDERER_GLOBAL_UNIFORM_X_MACRO(TYPE, VAR, INIT_VALUE) TYPE VAR = INIT_VALUE;
        RENDERER_GLOBAL_UNIFORM_VARS
        #undef RENDERER_GLOBAL_UNIFORM_X_MACRO

        static std::string to_string()
        {
            std::string str;
            #define RENDERER_GLOBAL_UNIFORM_X_MACRO(TYPE, VAR, INIT_VALUE) str += #TYPE " " #VAR  ";\n";
                RENDERER_GLOBAL_UNIFORM_VARS
            #undef RENDERER_GLOBAL_UNIFORM_X_MACRO
            return str;
        }
    };

    struct PushConsts_t
    {

        #define RENDERER_PUSH_CONSTS_VARS\
            RENDERER_PUSH_CONSTS_X_MACRO(int, attributeFlags       , 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, vertexUniformIndex   , 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, fragmentUniformIndex , 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, userValue0           , 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, vertexStorageIndex0  , 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, vertexStorageIndex1  , 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, vertexStorageIndex2  , 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, vertexStorageIndex3  , 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, fragmentStorageIndex0, 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, fragmentStorageIndex1, 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, fragmentStorageIndex2, 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, fragmentStorageIndex3, 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, unused0, 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, unused1, 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, unused2, 0)\
            RENDERER_PUSH_CONSTS_X_MACRO(int, unused3, 0)

        #define RENDERER_PUSH_CONSTS_X_MACRO(TYPE, VAR, INIT_VALUE) TYPE VAR = INIT_VALUE;
        RENDERER_PUSH_CONSTS_VARS
        #undef RENDERER_PUSH_CONSTS_X_MACRO

        static std::string to_string()
        {
            static_assert( sizeof(PushConsts_t) == 64, "" );
            std::string str;
            #define RENDERER_PUSH_CONSTS_X_MACRO(TYPE, VAR, INIT_VALUE) str += #TYPE " " #VAR  ";\n";
                RENDERER_PUSH_CONSTS_VARS
            #undef RENDERER_PUSH_CONSTS_X_MACRO

            return str;
        }
    };

    void setFragmentShaderMain(std::string s)
    {
        s = parse(s, vk::PipelineStageFlagBits::eFragmentShader);
        fragmentMainSourceCode = s;
    }
    void setVertexShaderMain(std::string s)
    {
        s = parse(s, vk::PipelineStageFlagBits::eVertexShader);
        vertexMainSourceCode = s;
    }

    static std::string getGlobalStructDefinitions()
    {
        return
        R"foo(

        struct sampler2D_id
        {
            int index;
        };

        struct samplerCube_id
        {
            int index;
        };

        )foo";
    }
    static std::string getConstantFunctions(bool enableTextures=false, bool enableCubeTextures=false)
    {
        std::string out;
        out +=
        R"foo(
        bool valid( sampler2D_id d)
        {
          return d.index!=-1;
        }
        bool valid( samplerCube_id d)
        {
          return d.index!=-1;
        }
        )foo";

        if( enableTextures)
        {
            out +=
            R"foo(
            vec4 texture( sampler2D_id s, vec2 uv)
            {
               if( !valid(s) ) return vec4(1,1,1,1);
               return texture( inputTextures[ s.index ], uv );
            }
            vec4 textureLod( sampler2D_id s, vec2 uv, float lod)
            {
               return textureLod( inputTextures[s.index], uv, lod );
            }
            float textureQueryLevels(sampler2D_id s)
            {
                if( !valid(s) ) return 0.f;
                return textureQueryLevels( inputTextures[s.index] );
            }
            )foo";
        }
        else
        {
            out +=
            R"foo(
            vec4 texture( sampler2D_id s, vec2 uv)
            {
               return vec4(1,1,1,1);
            }
            vec4 textureLod( sampler2D_id s, vec2 uv, float lod)
            {
               return vec4(1,1,1,1);
            }
            float textureQueryLevels(sampler2D_id s)
            {
                return 1.f;
            }

            )foo";
        }

        if( enableCubeTextures)
        {
            out +=
            R"foo(
            vec4 texture( samplerCube_id s, vec3 uv)
            {
                if( !valid(s) ) return vec4(1,1,1,1);
               return texture( inputCubeTextures[s.index], uv );
            }
            vec4 textureLod( samplerCube_id s, vec3 uv, float lod)
            {
               return textureLod( inputCubeTextures[s.index], uv, lod );
            }
            float textureQueryLevels(samplerCube_id s)
            {
                if( !valid(s) ) return 0.f;
                return textureQueryLevels( inputCubeTextures[s.index] );
            }
            )foo";
        }
        else
        {
            out +=
            R"foo(
            vec4 texture( samplerCube_id s, vec3 uv)
            {
                return vec4(1,1,1,1);
            }
            vec4 textureLod( samplerCube_id s, vec3 uv, float lod)
            {
               return vec4(1,1,1,1);
            }
            float textureQueryLevels(samplerCube_id s)
            {
                return 1.0f;
            }

            )foo";
        }
        return out;
    }

    std::string buildFragmentShader(uint32_t maxTexture2D=0, uint32_t maxCubeTexture=0) const
    {
        std::ostringstream out;

        auto & uniformStructMembers = fragmentUniformStructMembers;
        auto & storageArrays        = fragmentStorageArrays;
        auto & mainSourceCode       = fragmentMainSourceCode;

        //auto & srcInput  = srcFragmentInput;
        //auto & srcOutput = srcFragmentOutput;

        std::string srcOutType = "FSOut";

        out << "#version 450\n";
        out << "#extension GL_ARB_separate_shader_objects : enable\n";

        out << "\n\n\n";

        if(maxTexture2D)
        {
            #define VKA_MAX_TEXTURES 32
            out << fmt::format("#define VKA_MAX_TEXTURES {}\n", maxTexture2D);
            out << fmt::format("layout(set = {}, binding = {}) uniform sampler2D inputTextures[VKA_MAX_TEXTURES];\n", texturesSet, texturesBinding);
        }
        if(maxCubeTexture)
        {
            #define VKA_MAX_TEXTURES 32
            out << fmt::format("#define VKA_MAX_CUBE_TEXTURES {}\n", maxCubeTexture);
            out << fmt::format("layout(set = {}, binding = {}) uniform samplerCube inputCubeTextures[VKA_MAX_CUBE_TEXTURES];\n", texturesSet, texturesCubeBinding);
        }

        out << "\n\n\n";
out << R"foo(
        layout( location = 0) out vec4 out_COLOR;
        //layout( location = 1) out vec4 out_POSITION;
        //layout( location = 2) out vec4 out_NORMAL;
)foo" "\n\n\n";


        out << "\n\n\n";

        out << getGlobalStructDefinitions() << "\n\n";

        //if( _enableTextures1 )
        out << getConstantFunctions(maxTexture2D>0, maxCubeTexture>0) << "\n\n";

out << R"foo(
        struct FSOut
        {
            vec3  position;
            vec3  normal;
            vec3  albedo;
            float metallic;
            float roughness;
        };
)foo" "\n\n\n";


        // Write out the VertexOut Definition
        out << "layout(push_constant) uniform PushConsts \n";
        out << "{                \n";
        out << PushConsts_t::to_string();
        out << "} pushConsts;               \n";
        out << "\n\n\n\n";


        // Write out the Global Uniform
        out << fmt::format("layout(set={}, binding={}) uniform GLOBAL_UNIFORM_t \n", globalUniformDescriptorSetNumber, globalUniformDescriptorSetBinding);
        out << "{                \n";

        out << GlobalUniform_t::to_string();

        out << "} GLOBAL_UNIFORM;               \n";
        out << "#define GLOBAL GLOBAL_UNIFORM\n";
        out << "\n\n\n\n";


        out << "#define VKA_UNIFORM_PUSHCONSTS_INDEX pushConsts.fragmentUniformIndex \n";

        out << mainSourceCode << "\n\n";

        out << R"foo(
        void main()
        {

          FSOut OUT = MAIN();


          //====================== DO NOT EDIT =====================
          out_COLOR      = vec4(OUT.albedo, 1.0f);
          //out_POSITION = vec4(OUT.position, 1.0f);
          //out_NORMAL   = vec4(OUT.normal, OUT.roughness);

        } )foo" "\n\n";

        auto s = out.str();
        size_t i=0;
        for(auto & storageArray : fragmentStorageArrays)
        {
            auto label = storageArray.second;
            std::string storageIndex = "pushConsts.fragmentStorageIndex" + std::to_string(i++);
            std::string rxs = label+" *\\[";
            std::regex vowel_re(rxs);

            s = std::regex_replace(s, vowel_re, "$&"+storageIndex+"+");;
        }

        s = ReplaceString(s, "#define VKA_DEFINITIONS", generateUniformDeclarations(uniformStructMembers, storageArrays, fragmentStorageDescriptorSetNumber, fragmentStorageDescriptorSetBinding) );
        return s;
    }

    std::string buildVertexShader()
    {
        std::ostringstream out;

        auto & uniformStructMembers = vertexUniformStructMembers;
        auto & storageArrays = vertexStorageArrays;
        auto & mainSourceCode = vertexMainSourceCode;

        //auto & srcInput  = srcVertexInput;
        //auto & srcOutput = srcVertexOutput;

        std::string srcOutType = "VSOut";

        out << "#version 450\n";
        out << "#extension GL_ARB_separate_shader_objects : enable\n";

        // Write out the VertexOut Definition
        out << R"foo(
        layout( location = 0) in vec3 in_POSITION;
        layout( location = 1) in vec3 in_NORMAL;
        layout( location = 2) in vec3 in_TANGENT;
        layout( location = 3) in vec2 in_TEXCOORD_0;
        layout( location = 4) in vec2 in_TEXCOORD_1;
        layout( location = 5) in vec4 in_COLOR_0;
        layout( location = 6) in uvec4 in_JOINTS_0;
        layout( location = 7) in vec4 in_WEIGHTS_0;

        out gl_PerVertex
        {
             vec4 gl_Position;
        };
        )foo" "\n\n";


        // Write out the VertexOut Definition
        out << "layout(push_constant) uniform PushConsts \n";
        out << "{                \n";
        out << PushConsts_t::to_string();
        out << "} pushConsts;               \n";
        out << "\n\n\n\n";

        out << "#define VKA_ATTRIBUTES_FLAGS         pushConsts.attributeFlags \n";
        out << "#define VKA_HAS_POSITION    (( VKA_ATTRIBUTES_FLAGS & 0x01) == 0x01) \n";
        out << "#define VKA_HAS_NORMAL      (( VKA_ATTRIBUTES_FLAGS & 0x02) == 0x02) \n";
        out << "#define VKA_HAS_TANGENT     (( VKA_ATTRIBUTES_FLAGS & 0x04) == 0x04) \n";
        out << "#define VKA_HAS_TEXCOORD_0  (( VKA_ATTRIBUTES_FLAGS & 0x08) == 0x08) \n";
        out << "#define VKA_HAS_TEXCOORD_1  (( VKA_ATTRIBUTES_FLAGS & 0x10) == 0x10) \n";
        out << "#define VKA_HAS_COLOR_0     (( VKA_ATTRIBUTES_FLAGS & 0x20) == 0x20) \n";
        out << "#define VKA_HAS_JOINTS_0    (( VKA_ATTRIBUTES_FLAGS & 0x40) == 0x40) \n";
        out << "#define VKA_HAS_WEIGHTS_0   (( VKA_ATTRIBUTES_FLAGS & 0x80) == 0x80) \n";

        // Write out the Global Uniform
        out << fmt::format("layout(set={}, binding={}) uniform GLOBAL_UNIFORM_t \n", globalUniformDescriptorSetNumber, globalUniformDescriptorSetBinding);
        out << "{                \n";
        out << GlobalUniform_t::to_string();
        out << "} GLOBAL_UNIFORM;               \n";
        out << "#define GLOBAL GLOBAL_UNIFORM\n";
        out << "\n\n\n\n";

        out << "#define VKA_UNIFORM_PUSHCONSTS_INDEX pushConsts.vertexUniformIndex \n";
        //out << generateUniformDeclarations(uniformStructMembers, storageArrays, vertexStorageDescriptorSetNumber, vertexStorageDescriptorSetBinding) << "\n\n";

        out << mainSourceCode << "\n\n";



        out << "\n";

        out << "void main()                                                                                               \n";
        out << "{                                                                                                         \n";
        out << "                                                                                                          \n";
        out << "  MAIN();                                                                                     \n";
        out << "                                                                                                          \n";
        out << "                                                                                                          \n";
        //out << "  //====================== DO NOT EDIT =====================                                              \n";
        //out << "  f_POSITION   = OUT.position;                                                                            \n";
        //out << "  f_NORMAL     = OUT.normal;                                                                              \n";
        //out << "  f_TEXCOORD_0 = OUT.texCoord_0;                                                                          \n";
        //out << "  f_COLOR_0    = OUT.color;                                                                               \n";
        //out << "                                                                                                          \n";
        //out << "  gl_Position  = OUT.frag_position;// VKA_PROJECTION_MATRIX * (VKA_VIEW_MATRIX * vec4( OUT.position, 1.0) ); \n";
        out << "}                                                                                                         \n";


        auto s = out.str();
        size_t i=0;
        for(auto & storageArray : vertexStorageArrays)
        {
            auto label = storageArray.second;
            std::string storageIndex = "pushConsts.vertexStorageIndex" + std::to_string(i++);
            std::string rxs = label+" *\\[";
            std::regex vowel_re(rxs);

            s = std::regex_replace(s, vowel_re, "$&"+storageIndex+"+");;
        }


        s = ReplaceString(s, "#define VKA_DEFINITIONS", generateUniformDeclarations(uniformStructMembers, storageArrays, vertexStorageDescriptorSetNumber, vertexStorageDescriptorSetBinding) );
        return s;
    }

    std::string generateUniformDeclarations(std::vector< std::pair<std::string, std::string> > const &uniformStructMembers,
                                            std::vector< std::pair<std::string, std::string> > const &storageArrays,
                                            uint32_t set, uint32_t binding) const
    {

        std::ostringstream out;
        if( uniformStructMembers.size() )
        {
            // Write out the VertexOut Definition
            out << "struct Uniform_t \n";
            out << "{                \n";
            for(auto & l : uniformStructMembers)
            {
                out << fmt::format("   {} m_{};\n", l.first, l.second);
            }
            out << "};               \n";

            out << "\n\n\n\n";

            out << fmt::format("layout(set = {}, binding = {}) buffer readonly STORAGE_Uniform_t\n", set, binding);
            out << "{\n";
            out << "    " << fmt::format("{} value[];\n", "Uniform_t");//mat4 transform[];\n";
            out << "} " << fmt::format(" STORAGE_{};\n" , "Uniform");// "})}STORAGE_MATRIX;\n";
            for(auto & l : uniformStructMembers)
            {
                out << fmt::format("#define {} STORAGE_{}.value[ VKA_UNIFORM_PUSHCONSTS_INDEX ].m_{}\n", l.second, "Uniform", l.second);
            }
            out << "\n\n\n";
        }

        for(auto & arr : storageArrays)
        {
            out << fmt::format("layout(set = {}, binding = {}) buffer readonly STORAGE_{}_t\n", set, binding, arr.second);
            out << "{\n";
            out << "    " << fmt::format("{} value[];\n", arr.first);//mat4 transform[];\n";
            out << "} " << fmt::format(" STORAGE_{};\n", arr.second);// "})}STORAGE_MATRIX;\n";
            out << fmt::format("#define {} STORAGE_{}.value \n", arr.second, arr.second);
            out << "\n\n\n";
        }
        return out.str();
    }

    static std::string ReplaceString(std::string subject, const std::string& search,
                              const std::string& replace) {
        size_t pos = 0;
        while ((pos = subject.find(search, pos)) != std::string::npos) {
             subject.replace(pos, search.length(), replace);
             pos += replace.length();
        }
        return subject;
    }
protected:
    uint32_t globalUniformDescriptorSetNumber  = 0;
    uint32_t globalUniformDescriptorSetBinding = 0;

    uint32_t vertexStorageDescriptorSetNumber  = 0;
    uint32_t vertexStorageDescriptorSetBinding = 1;

    uint32_t fragmentStorageDescriptorSetNumber  = 0;
    uint32_t fragmentStorageDescriptorSetBinding = 1;

    uint32_t maxTextures     = 32;
    uint32_t texturesSet     = 1;
    uint32_t texturesBinding = 0;
    uint32_t texturesCubeBinding = 1;

    std::vector< std::pair<std::string, std::string> > vertexUniformStructMembers;
    std::vector< std::pair<std::string, std::string> > vertexStorageArrays;

    std::vector< std::pair<std::string, std::string> > fragmentUniformStructMembers;
    std::vector< std::pair<std::string, std::string> > fragmentStorageArrays;

    std::string vertexMainSourceCode;
    std::string fragmentMainSourceCode;

    void addVertexStorageArray(std::string type, std::string label)
    {
        if( !(label[0] == 's' && label[1] == '_') )
            throw std::runtime_error("Storage array labels must start with s_");
        vertexStorageArrays.push_back( std::make_pair(type,label));
    }
    void addVertexUniform(std::string type, std::string label)
    {
        if( !(label[0] == 'u' && label[1] == '_') )
            throw std::runtime_error("Uniform variables must start with u_");
        vertexUniformStructMembers.push_back( std::make_pair(type,label));
    }
    void addFragmentStorageArray(std::string type, std::string label)
    {
        if( !(label[0] == 's' && label[1] == '_') )
            throw std::runtime_error("Uniform variables must start with u_");
        fragmentStorageArrays.push_back( std::make_pair(type,label));
    }
    void addFragmentUniform(std::string type, std::string label)
    {
        if( !(label[0] == 'u' && label[1] == '_') )
            throw std::runtime_error("Uniform variables must start with u_");
        fragmentUniformStructMembers.push_back( std::make_pair(type,label));
    }
public:
    std::string parse(std::string n, vk::PipelineStageFlagBits shader)
    {
        n = removeComments(n);
        std::string newShader;
        std::istringstream ss(n);

        uint32_t count=0;
        while( ss )
        {
            std::string line;
            std::getline( ss, line);

            const std::regex uniform_regex(" *uniform ([\\w\\d]+) *([\\w\\d]+) *;.*");
            const std::regex storageArray_regex(" *storageArray ([\\w\\d]+) *([\\w\\d]+) *;.*");
            std::smatch pieces_match;

            if( 1 )
            {
                auto & fname = line;

                if (std::regex_match(fname, pieces_match, uniform_regex))
                {
                    if(count==0)
                    {
                        count++;
                        newShader += "#define VKA_DEFINITIONS\n\n";
                    }
                    if( shader == vk::PipelineStageFlagBits::eVertexShader)
                        addVertexUniform( pieces_match[1], pieces_match[2]);
                    else if( shader == vk::PipelineStageFlagBits::eFragmentShader)
                        addFragmentUniform( pieces_match[1], pieces_match[2]);
                }
                else if (std::regex_match(fname, pieces_match, storageArray_regex))
                {
                    if( shader == vk::PipelineStageFlagBits::eVertexShader)
                        addVertexStorageArray( pieces_match[1], pieces_match[2]);
                    else if( shader == vk::PipelineStageFlagBits::eFragmentShader)
                        addFragmentStorageArray( pieces_match[1], pieces_match[2]);

                }
                else
                {
                    newShader += line +"\n";
                }
            }
        }
        return newShader;
    }

    static std::string removeComments(std::string prgm)
    {
        auto n = prgm.length();
        std::string res;

        // Flags to indicate that single line and multpile line comments
        // have started or not.
        bool s_cmt = false;
        bool m_cmt = false;


        // Traverse the given program
        for (size_t i=0; i<n; i++)
        {
            // If single line comment flag is on, then check for end of it
            if (s_cmt == true && prgm[i] == '\n')
                s_cmt = false;

            // If multiple line comment is on, then check for end of it
            else if  (m_cmt == true && prgm[i] == '*' && prgm[i+1] == '/')
                m_cmt = false,  i++;

            // If this character is in a comment, ignore it
            else if (s_cmt || m_cmt)
                continue;

            // Check for beginning of comments and set the approproate flags
            else if (prgm[i] == '/' && prgm[i+1] == '/')
                s_cmt = true, i++;
            else if (prgm[i] == '/' && prgm[i+1] == '*')
                m_cmt = true,  i++;

            // If current character is a non-comment character, append it to res
            else  res += prgm[i];
        }
        return res;
    }
};

}
}

#endif
