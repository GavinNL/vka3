#ifndef ECS_RENDERSYSTEM3_PUNCTUAL_LIGHT_H
#define ECS_RENDERSYSTEM3_PUNCTUAL_LIGHT_H

#include <vka/math/linalg.h>
#include <vka/ecs/Components/CLight.h>

namespace vka {
namespace ecs {

struct sampler2D_id
{
    int32_t index = -1;
};

struct samplerCube_id
{
    int32_t index = -1;
};

}
}

#endif
