#ifndef VKA_ECS_RENDER_SYSTEM_3_H
#define VKA_ECS_RENDER_SYSTEM_3_H

#include <vka/ecs/SystemBase2.h>

#include <map>
#include <list>
#include <set>


#include <vka/utils/HostCubeImage.h>
#include <vka/ecs/ResourceObjects/HostPrimitive.h>

#include <vka/ecs/ResourceObjects/PBRMaterial.h>

#include <vkff/framegraph.h>
#include "SceneView.h"
#include "ShaderBuilder/ShaderBuilder.h"

namespace vka
{
namespace ecs
{

using tag_resource_device_copy_from_host = entt::tag<"tag_resource_device_copy_from_host"_hs>;



struct RenderSystem3Resources;

struct RenderSystem3CreateInfo
{
    vk::Instance instance;
    vk::Device device;
    vk::PhysicalDevice physicalDevice;
};

/**
 * @brief The RenderSystem3 class
 *
 * This is a re-imagined version of the rendersystem./
 *
 * This system will ONLY perform rendering. It will not perform
 * frustum culling, or managing of entities.
 *
 * This system only render MaterialPrimitiveComponents
 * and MeshComponents.
 */
class RenderSystem3 : public vka::ecs::SystemBaseInternal,
                      public vkff::FrameGraphExecutor
{
    vk::Instance            m_instance;
    vk::Device              m_device;
    vk::PhysicalDevice      m_physicalDevice;
    RenderSystem3Resources* m_resources=nullptr;
public:

    std::shared_ptr<SystemBaseInternal> m_PrimitiveManagementSystem;
    std::shared_ptr<SystemBaseInternal> m_TextureManagementSystem;
    std::shared_ptr<SystemBaseInternal> m_TextureCubeManagementSystem;

    std::shared_ptr<SystemBaseInternal> m_ArrayOfTexturesManagementSystem;

    void init(RenderSystem3CreateInfo const & R)
    {
        m_instance = R.instance;
        m_device = R.device;
        m_physicalDevice = R.physicalDevice;

        initResources();
    }

    template<typename RES>
    bool resourceIsDeviceLoaded( _ID<RES> id)
    {
        auto & M = getSystemBus()->getResourceManager< RES >();
        return M.registry().template has<tag_resource_device_fully_loaded>( M.entity(id) );
    }
    ~RenderSystem3();

    //==========================================================================
    // From SystemBase
    //==========================================================================
    void onStart()    override;
    void onStop() override;
    void onUpdate() override;

    std::string name() const override
    {
        return "RenderSystem3";
    }
    //==========================================================================

    //==========================================================================

    void releaseResources();
    void initSwapchainResources(vk::Extent2D swapchainImageSize,
                                uint32_t     concurrentFrameCount,
                                vk::RenderPass swapchainRenderPass);
    void releaseSwapchainResources();
    //==========================================================================


    void clear();


    /**
     * @brief initFrameGraph
     *
     * Initialize the framegraph
     */
    void initFrameGraph();



    /**
     * @brief render
     * @param frame
     *
     * The main render function that will render
     * the frame.
     */
    void render(vkff::FrameGraphExecuteInfo &info);

    std::mutex m_mutex;
    //==========================================================================

public:
    //==========================================================================
    // Image Management
    //==========================================================================
    /**
     * @brief generateMipmaps
     * @param id
     *
     * Schedule this id to have all its mipmaps generated.
     * This does not happen right away and it can be set
     * for images that haven't been loaded into memory yet
     */
    void generateMipmaps( Texture2D_ID id );



public:
    //==========================================================================
    // FrameGraph executor interface.
    //==========================================================================
    void init(RenderPassInit &I) override;
    void destroy(RenderPassDestroy &D) override;
    void write(RenderPassWrite &C) override;
    void update(RenderPassUpdate &C) override;


protected:
    void initResources();

    // free all staging buffers that have been allocated
    // for the last frame's transfer phase.
    // this should be called at the start of the framegraph
    size_t _freeStagingBuffers();


    /**
     * @brief _onDestroyDeviceTexture
     * @param r
     * @param e
     *
     * Callback function for when 2d textures and texture cubes
     * are destroyed
     */
    void   _onDestroyDeviceTexture2D(entt::registry & r, entt::entity e);
    void   _onDestroyDeviceTextureCube(entt::registry & r, entt::entity e);

    /**
     * @brief _onDestroyPrimitive
     * @param r
     * @param e
     *
     * callback function for when primitives are destroyed.
     */
    void   _onDestroyPrimitive(entt::registry & r, entt::entity e);

    //==========================================================================
    // Initializiations
    //==========================================================================
    SceneView3 m_sceneView;


    std::shared_ptr<SystemBaseInternal> m_MeshRendererSystem;
};



}
}

#endif


