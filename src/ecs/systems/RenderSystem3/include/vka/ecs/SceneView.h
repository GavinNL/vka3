#ifndef ECS_RENDERSYSTEM3_SCENEVIEW_H
#define ECS_RENDERSYSTEM3_SCENEVIEW_H

#include <vulkan/vulkan.hpp>

#include <vka/math/linalg.h>
#include <vka/math/transform.h>
#include <vka/math/geometry/frustum.h>

#include <vector>
#include <vka/ecs/ECS.h>

#include <vka/ecs/ID.h>
#include <vka/ecs/Components/CLight.h>

#include <vka/ecs/ResourceObjects/HostTexture.h>
#include <vka/ecs/ResourceObjects/Environment.h>

#include "PunctualLight.h"

namespace vka
{
namespace ecs
{

struct SceneView3
{
    // Screen Space Information
    vk::Extent2D screenDimensions; // how large the screen is in pixels
    vk::Rect2D   renderArea;       // the portion of the screen to actaully render (eg: the viewport)

    // Camera Information
    glm::vec3       cameraPosition;
    vka::Transform  cameraTransform; // use this to calculate the view matrix
    glm::mat4       projMatrix;
    float           nearPlane;
    float           farPlane;

    // Mouse information
    glm::vec2     mouseCoords;     // the mouse coordinates [0,0]-[screenDimensions]
    glm::vec3     mouseNear;       // position of the mouse on the near plane in worldspace
    glm::vec3     mouseFar;        // position of the mouse on the far plane in worldspace


    // Entities to draw.
    std::vector<entt::entity>          visibleEntities;


    // Current Lights
    std::vector<entt::entity>    lightEntities;

    // the environment we went to render with
    Environment_ID  environment;

};

}
}

#endif
