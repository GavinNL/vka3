#ifndef VKA_ECS_RENDERSYSTEM3_VULKANBUFFERS_H
#define VKA_ECS_RENDERSYSTEM3_VULKANBUFFERS_H

#include <vka/ecs/SystemBase2.h>
#include "vk_mem_alloc.h"
#include <vulkan/vulkan.hpp>

namespace vka
{

namespace ecs
{

/**
 * @brief The MultiStorageBuffer struct
 *
 * A multistorage buffer is a Storage Buffer which can
 * hold different types of data and keep them aligned.
 *
 * For example:
 *
 * MultiStorageBuffer B;
 *
 * auto index = B.push( mat4() );
 * assert( index == 0);
 *
 * auto index2 = B.push( vec4() );
 * assert( index == 5);
 *
 * In the shader, you would have the following definitions:
 * Both storage buffers are using teh same set/binding number
 *
 * layout(set = 0, binding = 1) buffer STORAGE_MATRIX_t
 * {
 *     mat4 value[];
 * } STORAGE_MATRIX;
 *
 *  * layout(set = 0, binding = 1) buffer STORAGE_MATRIX_t
 * {
 *     vec4 value[];
 * } STORAGE_VEC;
 *
 * // access teh values like this
 * STORAGE_MATRIX[0];
 * STORAGE_VEC[5];
 *
 */
struct MultiStorageBuffer
{
protected:
    uint8_t *  m_first = nullptr;
    uint8_t *  m_last  = nullptr;
    uint8_t *  m_end  = nullptr;
public:
    std::pair<vk::Buffer,VmaAllocation> m_buffer;

    MultiStorageBuffer()
    {
    }

    vk::Buffer buffer() const
    {
        return std::get<0>(m_buffer);
    }

    void init(std::pair<vk::Buffer,VmaAllocation> buff, void * data, size_t byteSize)
    {
        m_buffer= buff;
        m_first = static_cast<uint8_t*>(data);
        m_end   = m_first;
        m_last  = m_first + byteSize;
    }

    void reset()
    {
        m_end = m_first;
    }

    // returns the number of bytes available until the buffer
    // rolls over.
    size_t available() const
    {
        return static_cast<size_t>( std::distance(m_end, m_last) );
    }
    size_t capacity() const
    {
        return static_cast<size_t>( std::distance(m_first, m_last) );
    }
    size_t size() const
    {
        return static_cast<size_t>( std::distance(m_first, m_end) );
    }
    vk::DeviceSize byteSize() const
    {
        return static_cast<vk::DeviceSize>(capacity());
    }

    /**
     * @brief push
     * @param v
     * @return
     *
     * Push data into the buffer. The value will always be aligned to
     * sizeof(T).
     *
     * Ths function runs the array offset if the buffer was
     * representing a an array of T[x].
     *
     *  For example, given an empty storage, M:
     *
     * M.push(glm::mat4() ) == 0 (since it is the first element)
     * M.push(glm::mat4() ) == 1 (since it is now the second element in mat4[]
     * M.push( float() )    == 32 (since the float was pushed after the 2 mat4(), it exists at
     *                             byte offset 2*sizeof(mat4)==128.
     *                             If the underlying buffer was an array of float[], then byte 128
     *                             would be at array index 32.
     *
     *
     */
    template<typename T>
    int32_t push(T const & v)
    {
        static_assert(  !std::is_pointer<T>::value, "Cannot use a pointer");
        return push(&v, 1);
    }
    template<typename T>
    int32_t push(T const * v, size_t count)
    {
        auto alignment = sizeof(T);
        auto totalBytes= sizeof(T)*count;

        return push(v, totalBytes, alignment);
    }

    int32_t push(void const * v, size_t totalBytes, size_t alignment)
    {
        if( available() < totalBytes )
        {
            reset();
        }
        // move the current end point to a byte offset that aligns with alignment
        auto i = roundUp(alignment);

        std::memcpy( m_end, v, totalBytes );
        m_end += totalBytes;

        return static_cast<int32_t>( i / alignment );
    }

private:
    // round up the current offset to the next byte alignment
    size_t roundUp(size_t alignment)
    {
        auto _roundUp = [](auto N, auto S)
        {
            return ((((N) + (S) - 1) / (S)) * (S));
        };
        m_end = m_first + _roundUp( size(), alignment);

        if(m_end >= m_last)
        {
            reset();
        }

        return size();
    }

};

template<typename T>
struct StorageBuffer
{
    std::pair<vk::Buffer,VmaAllocation> m_buffer;
    T *                                 memMap = nullptr;

    vk::DeviceSize                      m_byteSize = 0;
    size_t                              m_size = 0;
    size_t                              m_capacity = 0;

    T* begin()
    {
        return memMap;
    }
    T* end()
    {
        return memMap+m_size;
    }

    T& operator[](size_t i)
    {
        return memMap[i];
    }

    T* push_back( T const & V)
    {
        this->operator[]( size() ) = V;
        m_size++;
        auto t = end()-1;
        return t;
    }

    T* insert_back( vk::ArrayProxy<T> V)
    {
        std::memcpy( end(), V.data(), sizeof(T)*V.size() );
        m_size += V.size();
        auto t = end()-1;
        return t;
    }

    size_t size() const
    {
        return m_size;
    }

    size_t available() const
    {
        return m_capacity-m_size;
    }

    void clear()
    {
        m_size=0;
    }
    size_t capacity() const
    {
        return m_capacity;
    }

    vk::DeviceSize byteSize() const
    {
        return m_byteSize;
    }
    vk::Buffer buffer() const
    {
        return m_buffer.first;
    }

};

/**
 * @brief The StorageBufferSet struct
 *
 * The StorageBufferSet is meant to
 * be used similar to a std::set in that
 * it stores a single copy of a particular piece of data
 * that is to be referenced within a vulkan storage buffer
 * object.
 *
 */
template<typename keyType, typename T>
struct StorageBufferMap : public StorageBuffer<T>
{
    using key_type   = keyType;
    using value_type = T;

    StorageBufferMap()
    {
    }

    StorageBufferMap(StorageBuffer<T> const & v) : StorageBuffer<T>(v)
    {

    }

    /**
     * @brief insert
     * @param id
     * @param v
     * @return
     *
     * Inserts a id/value into the storage buffer,
     * returns the index into the buffer where this
     * id is stored.
     */
    size_t insert( key_type id, value_type const & v)
    {
        auto it= m_map.find(id);
        if( it != m_map.end())
        {
            _cpy(it->second, v);
            return it->second;
        }

        size_t ind=this->size()-1;
        if( m_freeIndex.size() )
        {
            ind = m_freeIndex.back();
            m_freeIndex.pop_back();
        }
        else
        {
            this->push_back(v);
            ind = this->size() -1;
        }
        _cpy(ind, v);
        m_map[id] = ind;
        return ind;
    }

    int32_t find( key_type id) const
    {
        auto it = m_map.find(id);
        if( it != m_map.end())
        {
            return static_cast<int32_t>(it->second);
        }
        return -1;
    }

    /**
     * @brief erase
     * @param id
     *
     * Erase the object identified by id
     */
    void erase(key_type const& id)
    {
        auto it = m_map.find(id);
        if(it!=m_map.end())
        {
            m_freeIndex.push_back(it->second);
            m_map.erase(it);
        }
    }

protected:

    void _cpy(size_t index, value_type const & v)
    {
        auto * b = this->begin();
        b = b+index;
        std::memcpy(b, &v, sizeof(v));
    }
    std::map< key_type, size_t> m_map;
    std::vector< size_t> m_freeIndex;
};

template<typename T>
struct UniformBuffer
{
    std::pair<vk::Buffer,VmaAllocation> m_buffer;
    T *                                 memMap = nullptr;
    vk::DeviceSize                      m_byteSize = 0;

    vk::Buffer buffer() const
    {
        return m_buffer.first;
    }
    vk::DeviceSize byteSize() const
    {
        return m_byteSize;
    }

    void operator << (T const &v)
    {
        std::memcpy( memMap, &v, sizeof(v) );
    }
};


struct StagingBufferData
{
    vk::Buffer     buffer=vk::Buffer();
    uint32_t       offset=0;
    uint32_t       size  =0;
    void*          mappedData=nullptr;
};

struct TransferBuffer
{
    std::pair<vk::Buffer,VmaAllocation> m_buffer;
    uint8_t *                           memMap = nullptr;

    vk::DeviceSize                      m_byteSize = 0;
    size_t                              m_size = 0;
    size_t                              m_capacity = 0;

    uint8_t* begin()
    {
        return memMap;
    }
    uint8_t* end()
    {
        return memMap+m_size;
    }

    size_t size() const
    {
        return m_size;
    }

    size_t available() const
    {
        return m_capacity-m_size;
    }

    void clear()
    {
        m_size=0;
    }

    size_t capacity() const
    {
        return m_capacity;
    }

    vk::DeviceSize byteSize() const
    {
        return m_byteSize;
    }
    vk::Buffer buffer() const
    {
        return m_buffer.first;
    }

    /**
     * @brief reserve
     * @param sizeToReserve
     * @return
     *
     * Reserve a block of memory that can be used for copying.
     * will return a reference to where the data can be copied to
     * If the availble bytes is less than the sizeToReserve,
     * the transfer buffer is reset
     */
    StagingBufferData reserve(uint32_t sizeToReserve)
    {
        if( available() < sizeToReserve)
            clear();

        StagingBufferData D;
        D.size       = sizeToReserve;
        D.buffer     = m_buffer.first;
        D.offset     = static_cast<uint32_t>(m_size);
        D.mappedData = memMap + m_size;
        m_size += sizeToReserve;

        return D;
    }
};


}
}

#endif
