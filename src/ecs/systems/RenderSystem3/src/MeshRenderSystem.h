#ifndef VKA_RENDERSYSTEM3_MESH_RENDERER_H
#define VKA_RENDERSYSTEM3_MESH_RENDERER_H

#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/Components/CMesh.h>

#include <vka/ecs/ResourceObjects/Environment.h>
#include <vka/ecs/ResourceObjects/Pipeline.h>

#include <vka/utils/GLSLCompiler.h>
#include <vka/ecs/SceneView.h>
#include <vka/ecs/PunctualLight.h>

#include <vkff/framegraph.h>
#include <vka/ecs/Components/Tags.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CPose.h>
#include <vka/utils/filesystem.h>
#include <vka/ecs/VulkanBuffers.h>

#include "commandbuffer_helpers.inl"
#include "RenderSystem3_Resources.h"
#include "device_objects.h"

namespace vka
{

namespace ecs
{


/**
 * @brief The MeshRendererSystem struct
 *
 * The MeshRenderingSystem is a system which renders the basic
 * CMesh component.
 *
 * The shaders it uses is very specific. Some off-shoot shaders can be
 * used but they must have the same pipeline layout.
 *
 */
struct MeshRendererSystem : public SystemBaseInternal,
                            public vkff::FrameGraphExecutor
{
    RenderSystem3Resources * m_resources = nullptr;

    vk::RenderPass m_renderPass;

    struct
    {
        PBRPipeline_ID skybox;
        PBRPipeline_ID simple;
    } m_pipelines;
    //=========================================================================================
    // The descriptor set layouts for the pipelines
    //=========================================================================================
    vk::DescriptorSet       m_DSet_GlobalUniformMultiStorageBuffer;
    vk::DescriptorSetLayout       m_DSetLayout_GlobalUniformMultiStorageBuffer;

    vk::PipelineLayout      m_PipelineLayout;
    //=========================================================================================

    //=========================================================================================
    ResourceManager_p<PBRMaterial>       m_materialManager;
    ResourceManager_p<Environment>       m_envManager;
    ResourceManager_p<HostTexture2D>     m_textureManager;
    ResourceManager_p<HostTextureCube>   m_cubeManager;
    ResourceManager_p<HostMeshPrimitive> m_primitiveManager;
    ResourceManager_p<PBRPipeline>       m_pbrPipelineManager;
    //=========================================================================================

    vk::DescriptorPool m_DescriptorPool;


    vk::Device m_device;

    PBRMaterial_ID  m_defaultMaterial;
    MeshPrimitive_ID m_skyboxMesh;


    Texture2D_ID   m_brdf;
    TextureCube_ID m_defaultCubeMap;
    Environment_ID m_defaultEnv;

    bool m_initialized=false;

    SceneView3 * m_sceneView_ptr = nullptr;

    //=====================

    struct GlobalUniform_t
    {
        vec2  WINDOW;
        float TIME_DELTA;
        float UNUSED0;

        vec3  MOUSE;
        int   FRAME_NUMBER;

        vec3 MOUSE_NEAR;
        float TIME_INT;

        vec3 MOUSE_FAR;
        float TIME_FRAC;

        vec3 CAMERA_POSITION;
        float UNUSED4;

        mat4 VIEW;
        mat4 PROJECTION;
        mat4 PROJECTION_VIEW;
    };

    struct push_constants_type
    {
        int32_t    projMatrixIndex;
        int32_t    viewMatrixIndex;
        int32_t    transformIndex;
        int32_t    projViewMatrixIndex;  // index into the lights uniform buffer

        int32_t    materialIndex0; // index into the materials uniform buffer
        int32_t    materialIndex1;
        int32_t    unused2;
        int32_t    attributeFlags;

        float      lod; // index into the materials uniform buffer
        int32_t    environmentIndex1;
        int32_t    environmentIndex2;
        int32_t    unused1;

        int32_t    lightIndex;
        int32_t    lightCount;
        int32_t    unused4[2];
    };


    static_assert( sizeof(push_constants_type) == 64, "Push consts must be exactly 128 bytes");


    struct Light_t
    {
        vec3 direction;
        float range;

        vec3 color;
        float intensity;

        vec3 position;
        float innerConeCos;

        float   outerConeCos;
        int32_t type;
        int32_t unused1;
        int32_t unused2;
    };

    struct Environment_t
    {
        samplerCube_id skybox;
        samplerCube_id radiance;
        samplerCube_id irradiance;
        sampler2D_id   brdf;
    };

    struct Material_t
    {
        int   LightCount = 0;
        float Exposure = 2.0f;
        float AlphaCutoff = 0.5f;
        int   unlit = 0;

        int   NormalUVSet=0;
        float NormalScale=1.0f;
        float MetallicFactor  = 0.0f;
        float RoughnessFactor = 1.0f;

        vec4  BaseColorFactor = vec4(1,1,1,1);

        int   BaseColorUVSet = 0;
        int   MetallicRoughnessUVSet = 0;
        float Thickness = 0.f;
        float Transmission  = 0.f;

        vec3  SheenColorFactor = vec3(1,1,1);
        float SheenIntensityFactor = 0.0f;

        vec3  EmissiveFactor = vec3(0,0,0);
        int   EmissiveUVSet = 0;

        float SheenRoughness  = 0.0f;
        float ClearcoatFactor = 0.0f;
        float ClearcoatRoughnessFactor = 0.0f;
        float Anisotropy = 0.0f;

        vec2  IOR_and_f0 = {1.5f, 0.04f};
        float vertexOffsetDistance = 0.0f;
        float secondsSinceLoaded = 0.0f;

        vec3  AbsorptionColor = vec3(0,0,0);
        int   DebugOutput = 0;

        sampler2D_id BaseColorSampler;
        sampler2D_id NormalSampler;
        sampler2D_id MetallicRoughnessSampler;
        sampler2D_id EmissiveSampler;

        sampler2D_id  OcclusionSampler;
        sampler2D_id  ExtraSampler1;
        sampler2D_id  ExtraSampler2;
        sampler2D_id  ExtraSampler3;
    };
    static_assert( sizeof(Material_t) == 176, "Material_t must be exactly 192 bytes");

    //=====================


    //===============================================================================================
    // Subsystem Implementations
    //===============================================================================================
    void onStart() override;
    void onStop() override;

    void onUpdate() override;

    //===============================================================================================







    //===============================================================================================
    // Framegraph Executor Implementations
    //===============================================================================================
    void init(RenderPassInit & I) override;
    void init(vk::Device device, vk::RenderPass renderPass, bool enableDepth);

    void update(RenderPassUpdate &I) override;

    void destroy(RenderPassDestroy &D) override;


    bool m_writeSuccess         = false;
    bool m_pipelinesInitialized = false;

    void write(RenderPassWrite & C) override;

    //===============================================================================================================================

    bool _drawMaterialPrimitive2(RenderPassWrite & C,
                                 CPrimitive & x,
                                 size_t drawReqHash);

    bool _drawMaterialPrimitive2(RenderPassWrite & C,
                                 CPrimitive & x,
                                 vk::PrimitiveTopology selectedTopology);

    /**
     * @brief _draw_CMesh
     * @param C
     * @param Cr
     * @param PrimitiveManager
     * @param MaterialManager
     * @param selectedTopology
     * @return
     *
     * Draw a CMesh including all its primitives.
     * The selectedToplogy must match the topology set in the
     * primitive. It will return true if the mesh was able to be
     * fully drawn, and false if there were some primitives that
     * weren't drawn due to miss-matched topology
     */
    bool _draw_CMesh(RenderPassWrite       &C,
                     CMesh                 &Cr,
                     vk::PrimitiveTopology  selectedTopology
                  );
    bool _draw_CMesh(RenderPassWrite       &C,
                     CMesh                 &Cr,
                     size_t                 drawReqHash
                  );

    void setLightCount(vk::CommandBuffer cmd, uint32_t count);
    void setLights(vk::CommandBuffer cmd, vk::ArrayProxy<Light_t const> lights);

    /**
     * @brief bindMaterial
     * @param cmd
     * @param id
     * @param forceReload
     * @return
     *
     * Binds a material to the current buffer. returns false if it wasn't able to bind
     * and therefore binds the default material.
     *
     * To successfully be able to bind a material. you must first call:
     *
     * stageMaterialTextures(...)
     *
     * If that returns true, you must call:
     * stageMaterial( )
     *
     * After which bindMaterial will return true.
     * Calling bindMaterial again will update the material in teh buffer
     * if anything has changed.
     *
     */
    std::map<PBRMaterial_ID, int32_t> m_boundMaterials;
    bool bindMaterial(PBRMaterial_ID id, uint32_t materialNumber=0);

    bool bindEnvironment(Environment_ID x);



    /**
     * @brief _pushMultiStorageData
     * @param v
     * @return
     *
     * Push a struct of data into the multistoragebuffer
     * and return the index it was placed it.
     */
    template<typename T>
    int32_t _pushMultiStorageData( T const &v)
    {
        static_assert( sizeof(T)%16 == 0, "T must be aligned to a 16 byte boundary");
        return m_MultiStorageBuffer.push(v);
    }

    template<typename T>
    int32_t _pushMultiStorageData( T  * v, size_t count)
    {
        static_assert( sizeof(T)%16 == 0, "T must be aligned to a 16 byte boundary");
        return m_MultiStorageBuffer.push(v, count*sizeof(T), sizeof(T));
    }
    //===============================================================================================

    /**
     * @brief _pushMatrices
     * @param cmd
     * @param woldMatrix
     * @param boneMatrices
     * @return
     *
     * Push matrices into the storage buffer and return the
     * index into the buffer where the first matrix is stored
     */
    int32_t _pushMatrices(vk::CommandBuffer cmd, glm::mat4 const & woldMatrix, vk::ArrayProxy<glm::mat4 const> boneMatrices=nullptr);


    MeshPrimitive_ID m_lastBoundPrimitive;
    DevicePrimitive* m_lastBoundDevicePrimitive=nullptr;
    // Bind a primitive and set the attribute flag value
    // in the push constants
    DevicePrimitive const& bindPrimitive(vk::CommandBuffer cmd, MeshPrimitive_ID id);
    //===============================================================================================

    /**
     * @brief findOrLoadTexture
     * @param id
     * @return
     *
     * Finds a texture index if it has been already placed
     * in the array, if not, it will attempt to stage
     * the resource for loading. It will return
     * a null id, which can still be used in the shader.
     */
    sampler2D_id findOrLoadTexture(Texture2D_ID id);
    samplerCube_id findOrLoadTextureCube(TextureCube_ID id);

    /**
     * @brief findTexture
     * @param id
     * @return
     *
     * Finds the index of a texture within the Global Texture Array.
     * If the texture does not exist, returns a an index of -1.
     */
    sampler2D_id findTexture(Texture2D_ID id) const;
    samplerCube_id findTexture(TextureCube_ID id) const;

    /**
     * @brief findMaterial
     * @param id
     * @return
     *
     * Find a material that was added to the storage buffer.
     * returns -1 if it doesn't exist
     */
    int32_t findMaterial(PBRMaterial_ID id) const;


    /**
     * @brief bindTextureArray
     * @param cb
     * @param layout
     * @param set
     *
     * Binds the Global texture array descriptor set.
     */
    void bindGlobalTextureArray(vk::CommandBuffer cb, vk::PipelineLayout layout, uint32_t set);
    void bindGlobalStorage(vk::CommandBuffer cb, vk::PipelineLayout layout, uint32_t set);

    MultiStorageBuffer                                m_MultiStorageBuffer;
    UniformBuffer<GlobalUniform_t>                    m_GlobalUniformBuffer;

    // We should somehow move this into the "Painter" class
    std::function< void(vk::CommandBuffer, vk::PipelineLayout, uint32_t) > _bindTextureArray;
    std::function< int32_t(Texture2D_ID v) >                               _findTexture;
    std::function< int32_t(TextureCube_ID v) >                             _findTextureCube;
    std::function< vk::DescriptorSetLayout() >                             _getTextureDescriptorSetLayout;
    std::function< uint32_t() >                                            _getMaxTextures;
    std::function< uint32_t() >                                            _getMaxTextureCubes;

public:

    // Initialize a pipline that doesn't
    // already have the DynamicPipline Object created
    void initializePipeline(PBRPipeline_ID const & id);

    vkb::DynamicPipeline &setActivePipeline(PBRPipeline_ID const &id);
    void bindActivePipeline();

    void beginCommandBuffer(vk::CommandBuffer cmd);
    void endCommandBuffer();
    void pushConstants(const void *data, uint32_t offset, uint32_t size);
};

}

}

#endif


