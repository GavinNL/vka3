#include <vka/ecs/RenderSystem3.h>

#include <vka/ecs/Components/Tags.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CCamera.h>
#include <vka/ecs/Components/CAnimator2.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CLight.h>

#include <vkff/framegraph.h>

#include <vkb/utils/TextureArrayDescriptorSet.h>

#include <easy/profiler.h>
#include <easy/arbitrary_value.h> // EASY_VALUE, EASY_ARRAY are defined here

#include <glm/gtx/io.hpp>


#include "RenderSystem3_Resources.h"
#include "MeshRenderSystem.h"
#include "device_objects.h"
#include "commandbuffer_helpers.inl"

#include "RenderSystem_PrimitiveManagementSystem.h"

namespace vka
{

namespace ecs
{

static uint64_t _frameCount=0;

void RenderSystemPrimitiveManagementSystem::onStart()
{
    auto & rM = getResourceManager<HostMeshPrimitive>().registry();

    m_hostVisibleUpdated.connect( rM,
                                  entt::collector.update<HostMeshPrimitive>()
                                  .where<DevicePrimitive>()
                                  .where<tag_resource_host_visible>()
                                  );

}

void RenderSystemPrimitiveManagementSystem::onStop()
{
    m_hostVisibleUpdated.disconnect();
}

void RenderSystemPrimitiveManagementSystem::onUpdate()
{
    _frameCount++;
    // temporary for now.
    for(auto e : view<CMesh>())
    {
#pragma message("Physics system already updates this. But problems are occuring for GLTF Scenes")
        emplace_or_replace<tag_render_in_frustum>(e);
    }
}

void RenderSystemPrimitiveManagementSystem::writeCommandBuffer(vk::CommandBuffer m_commandBuffer)
{
    auto PrimManager = getSystemBus()->getResourceManager_p< HostMeshPrimitive >();
    auto llll = PrimManager->acquireLock();

    EASY_BLOCK("PrimitiveManagmenet")
            assert(m_resources);

    // unload any primitives that need to be
    // unloaded.
    _deviceUnloadPrimitives();

    // this system should loop through all the
    // entities that are flaged to be rendered
    //  1. check if their primitive is loaded
    //     if it is not, flag this object as cannot be drawn
    //  2. check if the material is loaded
    //     if it is, place it in the vector of staged materials
    //     this vector is what is copied to the storage buffer
    //     - If the material is not loaded, either flag the object as
    //       cannot be drawn, or create a blank material for it.

    auto & Mp = getSystemBus()->getResourceManager< vka::ecs::HostMeshPrimitive >();
    auto & Mm = getSystemBus()->getResourceManager< PBRMaterial >();
    //auto & Mi = getSystemBus()->getResourceManager< HostTexture2D >();

    // Clear any previous staging data
    // from the last frame.
    Mp.registry().clear<StagingBufferData>();

    (void)Mm;
    auto sb = getSystemBus();

    // loop through all the staged primitives
    // that do not have a DevicePrimitive and Do not have a StagedData
    // allocate a Staged Data struct for it
    //
    // This needs to be done after the previous two for-loops are completed
    size_t alignment = 16;
    uint32_t devicePrimitivesAllocated=0;

    auto vv = Mp.view<tag_resource_device_load, vka::ecs::HostMeshPrimitive>( entt::exclude<DevicePrimitive, StagingBufferData>);

    bool first=false;
    for(auto e : vv)
    {
        auto & Hp     = Mp.get<HostMeshPrimitive>(e);
        auto byteSize = Hp.requiredByteSize(alignment);

        if( byteSize == 0)
            continue;

        if(!first)
        {
            S_INFO(" ====== Primitives performCopies: Frame {} ================", _frameCount);
            first=true;
        }
        // Make sure that if there are no sub meshes
        // we create one that contains the entire
        // hostmeshprimitive.
        if( Hp.subMeshes.size() == 0)
        {
            Hp.addSubMesh( Hp.getDrawCall() );
        }
        auto & res = *m_resources;


        // out of memory, we'll need to copy on the
        // next run
        if( res.transferBufferAvailable() <  byteSize)
            break;

        if( Mp.has<tag_resource_host_visible>(e))
        {
            auto D = _makeHostVisibleDevicePrimitive(Hp,m_resources, {} );

            Mp.registry().emplace<DevicePrimitive>(   e, std::move(D) );
            sb->setDeviceResourceValid(Mp, e, true);

            continue;
        }
        else
        {
            S_INFO("Copying To Device {}", Mp.getResourceName(e));
            if( res.transferBufferAvailable() <= byteSize+1024)
            {
                S_WARN("   Out of Staging Memory. Required: {},   Available: {}", byteSize+1024, res.transferBufferAvailable() );
                continue;
            }

            StagingBufferData sD = res.reserveTransfer( static_cast<uint32_t>(byteSize) );
            S_DEBUG("   Staging Allocated.  Offset {}   Size {} ", sD.offset, sD.size);

            auto D = _makeDeviceOnlyPrimitive( Hp, sD.size, m_resources );

            auto offsets = Hp.getOffets(alignment);
            auto bytes   = Hp.copyData(sD.mappedData, offsets);

            (void)bytes;
            (void)offsets;
            {
                vk::BufferCopy region;
                region.srcOffset = sD.offset;
                region.dstOffset = 0;
                region.size      = D.byteSize;

                auto srcBuffer = sD.buffer;
                auto dstBuffer = D.buffer;

                m_commandBuffer.copyBuffer( srcBuffer, dstBuffer, region);
                S_DEBUG("   Copied to Device. Total Bytes: {}", Mp.getResourceName(e), region.size);
                // the primitive is now in the device
                // and can be used to render.
            }

            Mp.registry().emplace<DevicePrimitive>(   e, std::move(D) );
            getSystemBus()->setDeviceResourceValid(Mp, e, true);
        }

        devicePrimitivesAllocated++;
    }

    // check if any of the host-visible buffers
    // need to be reloaded

    for(auto e : m_hostVisibleUpdated)
    {
        auto & Hp     = Mp.get<HostMeshPrimitive>(e);
        auto byteSize = Hp.requiredByteSize(alignment);

        if( byteSize == 0)
            return;

        // Make sure that if there are no sub meshes
        // we create one that contains the entire
        // hostmeshprimitive.
        if( Hp.subMeshes.size() == 0)
        {
            Hp.addSubMesh( Hp.getDrawCall() );
        }

        auto D = Mp.get<DevicePrimitive>(e);
        D = _makeHostVisibleDevicePrimitive(Hp,m_resources, D );

        Mp.registry().replace<DevicePrimitive>(   e, std::move(D) );
        //sb->setDeviceResourceValid(Mp, e, true);
    }
    m_hostVisibleUpdated.clear();

    EASY_END_BLOCK
            //===============================================================================================
}

void RenderSystemPrimitiveManagementSystem::_deviceUnloadPrimitives()
{
    auto sb = getSystemBus();

    sb->forEachResourceToDeviceUnload<HostMeshPrimitive,DevicePrimitive>(
                [this](DevicePrimitive & D)
    {
        if( D.buffer )
        {
            m_resources->freeBuffer( D.buffer, D.allocation );
            D.buffer = vk::Buffer();
            D.allocation = nullptr;
            D.buffers.fill(D.buffer);
            D.attributeOffsets = {0};
            D.attributeFlags = 0;
            D.allocation = nullptr;
            D.byteSize = 0;
            D.drawCall = PrimitiveDrawCall();
            S_INFO("DevicePrimitive destroyed");
        }
        // Remove the references so their useCount() will
        // be decreased. This is so that the texture manager can
        // handle unload the textures if they haven't been used in a while.
    });
}

DevicePrimitive RenderSystemPrimitiveManagementSystem::_makeDeviceOnlyPrimitive(const HostMeshPrimitive &hP, uint32_t byteSize, RenderSystem3Resources *m_resources)
{
    DevicePrimitive D;

    auto bb = m_resources->allocateBuffer(byteSize,
                                          vk::BufferUsageFlagBits::eVertexBuffer
                                          | vk::BufferUsageFlagBits::eIndexBuffer
                                          | vk::BufferUsageFlagBits::eTransferDst,
                                          VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_ONLY);

    auto offsets = hP.getOffets(16);
    D.buffers.fill(D.buffer);

    // and set the new device-only buffer as the
    // main buffer
    D.buffer           = bb.first;
    D.allocation       = bb.second;
    D.attributeOffsets = offsets;
    D.hostVisible      = false;
    D.mappedData       = nullptr;
    D.byteSize         = byteSize;
    D.buffers.fill( D.buffer );


    S_DEBUG("  POSITION   : {}    byteLength: {} ", hP.POSITION  .count() , hP.POSITION  .byteLength());
    S_DEBUG("  NORMAL     : {}    byteLength: {} ", hP.NORMAL    .count() , hP.NORMAL    .byteLength());
    S_DEBUG("  TANGENT    : {}    byteLength: {} ", hP.TANGENT   .count() , hP.TANGENT   .byteLength());
    S_DEBUG("  TEXCOORD_0 : {}    byteLength: {} ", hP.TEXCOORD_0.count() , hP.TEXCOORD_0.byteLength());
    S_DEBUG("  TEXCOORD_1 : {}    byteLength: {} ", hP.TEXCOORD_1.count() , hP.TEXCOORD_1.byteLength());
    S_DEBUG("  COLOR_0    : {}    byteLength: {} ", hP.COLOR_0   .count() , hP.COLOR_0   .byteLength());
    S_DEBUG("  JOINTS_0   : {}    byteLength: {} ", hP.JOINTS_0  .count() , hP.JOINTS_0  .byteLength());
    S_DEBUG("  WEIGHTS_0  : {}    byteLength: {} ", hP.WEIGHTS_0 .count() , hP.WEIGHTS_0 .byteLength());
    S_DEBUG("  INDEX      : {}    byteLength: {} ", hP.INDEX     .count() , hP.INDEX     .byteLength());

    assert( hP.POSITION.count() != 0);
    S_DEBUG("  HostPrimitve IndexCount: {}   VertexCount: {} ", hP.indexCount() , hP.vertexCount());
    if( hP.indexCount() )
    {
        if( hP.INDEX.attributeSize() == 2 )
            D.indexType = vk::IndexType::eUint16;
        else
            D.indexType = vk::IndexType::eUint32;
    }

    auto dc = hP.getDrawCall();
    D.drawCall.firstIndex   =  dc.firstIndex;
    D.drawCall.indexCount   =  dc.indexCount;
    D.drawCall.vertexCount  =  dc.vertexCount;
    D.drawCall.vertexOffset =  dc.vertexOffset;

    int32_t attributeFlags = 0;
    for(auto a : {PrimitiveAttribute::POSITION
        , PrimitiveAttribute::NORMAL
        , PrimitiveAttribute::TANGENT
        , PrimitiveAttribute::TEXCOORD_0
        , PrimitiveAttribute::TEXCOORD_1
        , PrimitiveAttribute::COLOR_0
        , PrimitiveAttribute::JOINTS_0
        , PrimitiveAttribute::WEIGHTS_0
        , PrimitiveAttribute::__LAST} /*__LAST == index*/ )
    {
        if( offsets[ static_cast<size_t>(a)] != std::numeric_limits<vk::DeviceSize>::max())
        {
            attributeFlags |= ( int32_t(1) << static_cast<int32_t>(a) );
        }
    }
    D.attributeFlags = attributeFlags;

    for(auto & off : D.attributeOffsets)
        off = off == std::numeric_limits<vk::DeviceSize>::max() ? 0 : off;

    return D;
}

DevicePrimitive RenderSystemPrimitiveManagementSystem::_makeHostVisibleDevicePrimitive(const HostMeshPrimitive &hP, RenderSystem3Resources *m_resources, DevicePrimitive dP)
{
    size_t alignment=16;

    // when we first stage the primitive, we will allocate
    // the buffer as host visible
    //auto e = Host.entity(id);
    //auto & hP = Host.registry().get<vka::ecs::HostMeshPrimitive>(e);
    auto byteSize = hP.requiredByteSize(alignment);
    if( byteSize == 0)
        return {};

    auto & res = *m_resources;

    if( dP.byteSize < byteSize)
    {
        if( dP.allocation )
        {
            res.freeBuffer(dP.buffer, dP.allocation);
        }
        auto bb = res.allocateBuffer(byteSize,
                                     vk::BufferUsageFlagBits::eVertexBuffer
                                     | vk::BufferUsageFlagBits::eIndexBuffer
                                     | vk::BufferUsageFlagBits::eTransferSrc ,
                                     VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_TO_GPU);

        void * mappedMem=nullptr;
        vmaMapMemory(res.allocator, bb.second, &mappedMem);

        dP.buffer           = bb.first;
        dP.allocation       = bb.second;
        dP.mappedData       = mappedMem;
    }


    auto offsets = hP.getOffets(alignment);

    hP.copyData(dP.mappedData, offsets);

    dP.attributeOffsets = offsets;
    dP.hostVisible      = true;
    dP.byteSize         = byteSize;
    dP.buffers.fill( dP.buffer );

    if( hP.indexCount() )
    {
        if( hP.INDEX.attributeSize() == 2 )
            dP.indexType = vk::IndexType::eUint16;
        else
            dP.indexType = vk::IndexType::eUint32;
    }

    auto dc = hP.getDrawCall();
    dP.drawCall.firstIndex   =  dc.firstIndex;
    dP.drawCall.indexCount   =  dc.indexCount;
    dP.drawCall.vertexCount  =  dc.vertexCount;
    dP.drawCall.vertexOffset =  dc.vertexOffset;

    int32_t attributeFlags = 0;
    for(auto a : {PrimitiveAttribute::POSITION
        , PrimitiveAttribute::NORMAL
        , PrimitiveAttribute::TANGENT
        , PrimitiveAttribute::TEXCOORD_0
        , PrimitiveAttribute::TEXCOORD_1
        , PrimitiveAttribute::COLOR_0
        , PrimitiveAttribute::JOINTS_0
        , PrimitiveAttribute::WEIGHTS_0
        , PrimitiveAttribute::__LAST} /*__LAST == index*/ )
    {
        if( offsets[ static_cast<size_t>(a)] != std::numeric_limits<vk::DeviceSize>::max())
        {
            attributeFlags |= ( int32_t(1) << static_cast<int32_t>(a) );
        }
    }
    dP.attributeFlags = attributeFlags;

    for(auto & off : dP.attributeOffsets)
        off = off == std::numeric_limits<vk::DeviceSize>::max() ? 0 : off;

    return dP;
}


}
}
