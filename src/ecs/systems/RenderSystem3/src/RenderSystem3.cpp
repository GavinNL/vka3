#include <vka/ecs/RenderSystem3.h>

#include <vka/ecs/Components/Tags.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CCamera.h>
#include <vka/ecs/Components/CAnimator2.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CLight.h>

#include <vkff/framegraph.h>

#include <easy/profiler.h>
#include <easy/arbitrary_value.h> // EASY_VALUE, EASY_ARRAY are defined here

#include <glm/gtx/io.hpp>


#include "RenderSystem3_Resources.h"
#include "MeshRenderSystem.h"
#include "device_objects.h"
#include "commandbuffer_helpers.inl"

#include "RenderSystem_PrimitiveManagementSystem.h"
#include "RenderSystem_TextureManagementSystem.h"
#include "RenderSystem_ArrayOfTexturesSystem.h"

namespace vka
{

namespace ecs
{

static uint64_t _frameCount=0;

struct MipMapGeneratorComponent
{
    std::set<uint32_t> layers;
};

// this will be set by th efrustum culling system
//using tag_render_in_frustum = entt::tag<"RenderSystemPreStep::tag_render_in_frustum"_hs>;


#define MAIN_OBJECT_PASS_NAME "objectPass"
#define MAIN_FLOW_FILE "rc:///framegraph/swapchainPresent.flow"

void RenderSystem3::onStart()
{   
    S_INFO("RenderSystem3 System Connected");

    assert(m_resources);
    assert(m_device);

    {
        auto PrimitivePrepSystem         = getSystemBus()->createSystem<RenderSystemPrimitiveManagementSystem>();
        PrimitivePrepSystem->m_resources = m_resources;
        PrimitivePrepSystem->m_renderSystem = this;
        m_PrimitiveManagementSystem     = PrimitivePrepSystem;
    }

    {
        auto imagePrep         = getSystemBus()->createSystem<RenderSystemTextureManagementSystem>();
        imagePrep->m_device    = m_device;
        imagePrep->m_resources = m_resources;

        m_TextureManagementSystem      = imagePrep;
    }

    {
        auto imagePrep         = getSystemBus()->createSystem<RenderSystemArrayOfTextureSystem>();
        imagePrep->m_device    = m_device;
        imagePrep->m_resources = m_resources;

        m_ArrayOfTexturesManagementSystem      = imagePrep;
    }


    auto & Mi = getSystemBus()->getResourceManager<HostTexture2D>();
    auto & Mc = getSystemBus()->getResourceManager<HostTextureCube>();
    auto & Mp = getSystemBus()->getResourceManager<vka::ecs::HostMeshPrimitive>();

    Mi.registry().on_destroy<DeviceTexture>().connect<&RenderSystem3::_onDestroyDeviceTexture2D>(*this);
    Mc.registry().on_destroy<DeviceTexture>().connect<&RenderSystem3::_onDestroyDeviceTextureCube>(*this);
    Mp.registry().on_destroy<DevicePrimitive>().connect<&RenderSystem3::_onDestroyPrimitive>(*this);

    {
        // add the mesh renderer to the post subsystem list
        // this will call the onUpdate() method
        // after the rendersystem's onUPdate method is called.
        auto meshRenderer = getSystemBus()->createSystem<MeshRendererSystem>();
        meshRenderer->m_resources = m_resources;
        m_MeshRendererSystem = meshRenderer;

        auto aot = std::dynamic_pointer_cast<RenderSystemArrayOfTextureSystem>(m_ArrayOfTexturesManagementSystem);
        meshRenderer->_findTexture     = std::bind( &RenderSystemArrayOfTextureSystem::findTexture, aot.get(), std::placeholders::_1);
        meshRenderer->_findTextureCube = std::bind( &RenderSystemArrayOfTextureSystem::findTextureCube, aot.get(), std::placeholders::_1);

        meshRenderer->_bindTextureArray = std::bind( &RenderSystemArrayOfTextureSystem::bindTextureArray, aot.get(), std::placeholders::_1, std::placeholders::_2, std::placeholders::_3);


    }

    getSystemBus()->registerResourceLoader<PBRPipeline>(
                [](std::shared_ptr<SystemBus> sb, vka::uri u)
    {
        auto M  = sb->getResourceManager_p<PBRPipeline>();
        auto id = M->getIdFromUri(u);

        auto J = sb->readResourceJSON(u);

        PBRPipeline pb;
        pb.vertexShader   = vka::uri( J.at("vertexShader"  ).get<std::string>());
        pb.fragmentShader = vka::uri( J.at("fragmentShader").get<std::string>());

        if( J.count("compileTimeDefinitions"))
        {
            auto & compileTimeDefinitions = J.at("compileTimeDefinitions");
            for(auto it=compileTimeDefinitions.begin(); it!= compileTimeDefinitions.end();++it)
            {
                auto & key   = it.key();
                auto & value = it.value();

                pb.compileTimeDefinitions[key] = value.get<std::string>();
            }
        }
        M->emplaceData(id, std::move(pb));
    });

    // Start all the subsystems
    m_PrimitiveManagementSystem->start();
    m_TextureManagementSystem->start();
    m_ArrayOfTexturesManagementSystem->start();
    m_MeshRendererSystem->start();
    S_INFO("RenderSystem3 Connected Successfully");

}

void RenderSystem3::onStop()
{
    // Stop all the sybsystems
    m_MeshRendererSystem->stop();
    m_ArrayOfTexturesManagementSystem->stop();
    m_PrimitiveManagementSystem->stop();
    m_TextureManagementSystem->stop();

    getSystemBus()->getResourceManager<HostMeshPrimitive>().registry().clear<DevicePrimitive>();
    getSystemBus()->getResourceManager<HostTexture2D>().registry().clear<DeviceTexture>();
    getSystemBus()->getResourceManager<HostTextureCube>().registry().clear<DeviceTexture>();

    auto & Mi = getSystemBus()->getResourceManager<HostTexture2D>();
    auto & Mp = getSystemBus()->getResourceManager<vka::ecs::HostMeshPrimitive>();
    auto & Mc = getSystemBus()->getResourceManager<HostTextureCube>();

    Mi.registry().on_destroy<DeviceTexture>().disconnect<&RenderSystem3::_onDestroyDeviceTexture2D>(*this);
    Mc.registry().on_destroy<DeviceTexture>().disconnect<&RenderSystem3::_onDestroyDeviceTextureCube>(*this);
    Mp.registry().on_destroy<DevicePrimitive>().disconnect<&RenderSystem3::_onDestroyPrimitive>(*this);


    S_INFO("RenderSystem3 Disconnected Successfully");
}

void RenderSystem3::onUpdate()
{
    auto atms = std::dynamic_pointer_cast< RenderSystemArrayOfTextureSystem >( m_ArrayOfTexturesManagementSystem);
    if( atms->m_textureArrayInitialized )
    {
        auto mrs = std::dynamic_pointer_cast< MeshRendererSystem >( m_MeshRendererSystem);
        mrs->_getTextureDescriptorSetLayout = std::bind( &RenderSystemArrayOfTextureSystem::getLayout, atms.get() );
        mrs->_getMaxTextures                = std::bind( &RenderSystemArrayOfTextureSystem::getMaxTextures, atms.get() );
        mrs->_getMaxTextureCubes            = std::bind( &RenderSystemArrayOfTextureSystem::getMaxTextureCubes, atms.get() );
    }
    //uint32_t count = static_cast<uint32_t>(m_resources->queryData.size());
    //m_device.getQueryPoolResults(m_resources->queryPool,0,1,count*size)
    //m_device.getQueryPoolResults(
    //    m_resources->queryPool,
    //    0,
    //    1,
    //    count * sizeof(uint64_t),
    //    m_resources->queryData.data(),
    //    sizeof(uint64_t),
    //    vk::QueryResultFlagBits::e64);

    reportStat("input_assembly_vertex_count",         m_resources->queryData[0] );
    reportStat("input_assembly_primitives count",     m_resources->queryData[1] );
    reportStat("vertex_shader_invocations",           m_resources->queryData[2] );
    reportStat("clipping_stage_primitives_processed", m_resources->queryData[3] );
    reportStat("clipping_stage_primitives_output",    m_resources->queryData[4] );
    reportStat("fragment_shader_invocations",         m_resources->queryData[5] );
        //reportStat("tess_control_shader_patches" , m_resources->queryData[6]);
        //reportStat("tess_eval_shader_invocations", m_resources->queryData[6]);

    m_resources->nextTransferBuffer();

    m_PrimitiveManagementSystem->execute();
    m_TextureManagementSystem->execute();

    EASY_BLOCK("SceneViewSetup");

    auto & reg     = getSystemBus()->registry;

    vka::Transform Ct;
    CCamera Cc;
    if( reg.size<CCamera>() == 0)
    {
        getSystemBus()->variables.currentCameraEntity = entt::null;
    }
    else
    {
        auto camEntity = reg.view<CCamera>().front();
             Cc        = reg.get<CCamera>( camEntity );

         // calculate the world space transformation of the camera
         Ct = getSystemBus()->calculateWorldSpaceTransform(camEntity);

         getSystemBus()->variables.currentCameraEntity = camEntity;
    }


    getSystemBus()->variables.mouseRay = Cc.projectMouseRay( getSystemBus()->variables.mouseScreenPosition, Ct, getSystemBus()->variables.screenSize);


    // Set up the scene view for rendering
    {
        auto sb = getSystemBus();

        m_sceneView.visibleEntities.clear();

        // this value is actually filled in by the first render pass
        // it may not be set the first around.
        auto screenSize = getSystemBus()->variables.screenSize;

        glm::vec2 viewPortSize = screenSize * Cc.viewPortSizeNormalized;
        glm::vec2 viewPortPos  = screenSize * Cc.viewPortOffsetNormalized;

        m_sceneView.environment     = Cc.environment;
        m_sceneView.cameraPosition  = Ct.position;
        m_sceneView.cameraTransform = Ct;
        m_sceneView.screenDimensions= vk::Extent2D( static_cast<uint32_t>(screenSize.x), static_cast<uint32_t>(screenSize.y));

        m_sceneView.renderArea      = vk::Rect2D( vk::Offset2D( static_cast< int32_t>(viewPortPos.x) , static_cast< int32_t>(viewPortPos.y)),
                                                  vk::Extent2D( static_cast<uint32_t>(viewPortSize.x), static_cast<uint32_t>(viewPortSize.y))
                                                );

        m_sceneView.projMatrix      = Cc.getProjectionMatrix( screenSize );
        m_sceneView.nearPlane       = Cc.nearPlane;
        m_sceneView.farPlane        = Cc.farPlane;
        m_sceneView.visibleEntities.clear();

        EASY_BLOCK("Appending Visible Entities");
        for(auto e : getSystemBus()->registry.view<tag_render_in_frustum, CMesh, CWorldTransform>() )
        {
            m_sceneView.visibleEntities.push_back(e);
            auto & M = get<CMesh>(e);
            for(auto & x : M.primitives)
            {
                if( x.mesh.valid())
                    stageResource(x.mesh);
                if( x.material.valid())
                    stageResource(x.material);
            }
            remove<tag_render_in_frustum>(e);
        }
        EASY_END_BLOCK

        auto mrs = std::dynamic_pointer_cast<MeshRendererSystem>(m_MeshRendererSystem);
        if( mrs)
            mrs->m_sceneView_ptr = &m_sceneView;

        {
            m_sceneView.lightEntities.clear();
            // copy all the lights. but really we should only be using the lights
            // which intersect the view frustum.
            auto _view = view<CLight, CPosition>();
            for(auto e : _view)
            {
                m_sceneView.lightEntities.push_back(e);
            }
        }
    }
    EASY_END_BLOCK

    m_ArrayOfTexturesManagementSystem->execute();

    // Execute the mesh render system at this point
    // calling its onUpdate() method
    if( m_MeshRendererSystem)
        m_MeshRendererSystem->execute();
}

void RenderSystem3::clear()
{

}

RenderSystem3::~RenderSystem3()
{
    S_INFO("RenderSystem3 destroyed");
}

void RenderSystem3::initResources()
{
    m_resources = new RenderSystem3Resources();
    auto & res  = *m_resources;

    //=========================================================
    // initialize the vulkan memory allocator
    //=========================================================
    VmaAllocatorCreateInfo allocatorInfo = {};
    allocatorInfo.physicalDevice = m_physicalDevice;
    allocatorInfo.device = m_device;
    allocatorInfo.instance = m_instance;
    vmaCreateAllocator(&allocatorInfo, &res.allocator);

    //=========================================================

    {
        // Temporary 1-mipmap sampler
        vkb::SamplerCreateInfo2 ci;
        ci.magFilter               =  vk::Filter::eNearest;
        ci.minFilter               =  vk::Filter::eNearest;
        ci.mipmapMode              =  vk::SamplerMipmapMode::eLinear ;
        ci.addressModeU            =  vk::SamplerAddressMode::eRepeat ;
        ci.addressModeV            =  vk::SamplerAddressMode::eRepeat ;
        ci.addressModeW            =  vk::SamplerAddressMode::eRepeat ;
        ci.mipLodBias              =  0.0f  ;
        ci.anisotropyEnable        =  VK_FALSE;
        ci.maxAnisotropy           =  1 ;
        ci.compareEnable           =  VK_FALSE ;
        ci.compareOp               =  vk::CompareOp::eAlways ;
        ci.minLod                  =  0 ;
        ci.maxLod                  =  static_cast<float>(1.0) ;
        ci.borderColor             =  vk::BorderColor::eIntOpaqueBlack ;
        ci.unnormalizedCoordinates =  VK_FALSE ;

        res.samplerNearest1Mip     = ci.create(res.storage,m_device);

        ci.magFilter               = vk::Filter::eLinear;
        ci.minFilter               = vk::Filter::eLinear;

        res.samplerLinear1Mip      = ci.create(res.storage,m_device);
    }

    {
        //-------------------------------------------------
        // copy the data to the staging buffer
        vk::DeviceSize byteSize = 5*1024*1024*4;
        res.stagingBuffers.push( res.allocateTransferBuffer(byteSize) );
        res.stagingBuffers.push( res.allocateTransferBuffer(byteSize) );
        //-------------------------------------------------
    }


    {
       // pipelineStatNames = {
       //     "Input assembly vertex count        ",
       //     "Input assembly primitives count    ",
       //     "Vertex shader invocations          ",
       //     "Clipping stage primitives processed",
       //     "Clipping stage primitives output    ",
       //     "Fragment shader invocations        "
       // };
       // if (deviceFeatures.tessellationShader) {
       //     pipelineStatNames.push_back("Tess. control shader patches       ");
       //     pipelineStatNames.push_back("Tess. eval. shader invocations     ");
       // }
       // pipelineStats.resize(pipelineStatNames.size());

        vk::QueryPoolCreateInfo queryPoolInfo;

        // This query pool will store pipeline statistics
        queryPoolInfo.queryType = vk::QueryType::ePipelineStatistics;
        // Pipeline counters to be returned for this pool
        queryPoolInfo.pipelineStatistics =
                vk::QueryPipelineStatisticFlagBits::eInputAssemblyVertices |
                vk::QueryPipelineStatisticFlagBits::eInputAssemblyPrimitives |
                vk::QueryPipelineStatisticFlagBits::eVertexShaderInvocations |
                vk::QueryPipelineStatisticFlagBits::eClippingInvocations |
                vk::QueryPipelineStatisticFlagBits::eClippingPrimitives |
                vk::QueryPipelineStatisticFlagBits::eFragmentShaderInvocations;
        queryPoolInfo.queryCount = 6;
        if (0)
        {
            queryPoolInfo.pipelineStatistics |=
                vk::QueryPipelineStatisticFlagBits::eTessellationControlShaderPatches |
                vk::QueryPipelineStatisticFlagBits::eTessellationEvaluationShaderInvocations;
            queryPoolInfo.queryCount = 8;
        }
        m_resources->queryData.resize(queryPoolInfo.queryCount);
        auto queryPool = m_device.createQueryPool(queryPoolInfo);
        m_resources->queryPool = queryPool;

    }

    S_INFO("RenderSystem3 initResources");
}

void RenderSystem3::releaseResources()
{
    // This method is called just before the vulkan instance and device is destroyed
    // so you MUST release all resources that have ever been created
    auto x = std::dynamic_pointer_cast<RenderSystemArrayOfTextureSystem>(m_ArrayOfTexturesManagementSystem);
    x->releaseResources();


    if( getSystemBus() )
    {
        auto & Mt = getResourceManager<HostTexture2D>();
        auto & Mc = getResourceManager<HostTextureCube>();
        auto & Mp = getResourceManager<HostMeshPrimitive>();

        Mt.registry().clear<DeviceTexture>();
        Mc.registry().clear<DeviceTexture>();
        Mp.registry().clear<DevicePrimitive>();
    }

    if( m_resources->framegraph)
    {
        m_resources->framegraph->destroy(*this);
        m_resources->framegraph->free();
        delete m_resources->framegraph;
        m_resources->framegraph = nullptr;
    }

    auto & res = *m_resources;

    res.storage.destroy( res.samplerLinear1Mip , m_device);
    res.storage.destroy( res.samplerNearest1Mip, m_device);

    res.storage.destroyAll(m_device);

    m_device.destroyQueryPool(m_resources->queryPool);

    while( !res.stagingBuffers.empty())
    {
        res.freeBuffer( res.stagingBuffers.front().m_buffer );
        res.stagingBuffers.pop();
    }

    //=========================================================
    // initialize the vulkan memory allocator
    //=========================================================
    vmaDestroyAllocator(res.allocator);
    //=========================================================

    delete m_resources;
    m_resources = nullptr;
    S_INFO("RenderSystem3 releaseResources");
}

void RenderSystem3::initSwapchainResources(vk::Extent2D   swapchainImageSize,
                                           uint32_t       concurrentFrameCount,
                                           vk::RenderPass swapchainRenderPass)
{
    (void)concurrentFrameCount;
    (void)swapchainRenderPass;

    S_INFO("renderSystem3::initSwapchainResources()");
    if( !m_resources->framegraph )
    {
        initFrameGraph();
    }

    auto & fg = *m_resources->framegraph;

    vkff::FrameGraphResizeInfo info;
    info.width  = swapchainImageSize.width;
    info.height = swapchainImageSize.height;

    getSystemBus()->variables.screenSize = glm::vec2(info.width,info.height);

    fg.resize( info );
}

void RenderSystem3::releaseSwapchainResources()
{
}

void RenderSystem3::initFrameGraph()
{
    m_resources->framegraph = new vkff::FrameGraph();

    auto & fg = *m_resources->framegraph;
    vka::json J;
    //
    std::ifstream in(getSystemBus()->getPath( vka::uri(MAIN_FLOW_FILE) ));
    if( !in )
        throw std::runtime_error("could not open the framegraph flow file");

    in >> J;

    J = vkff::fromJsonFlow2(J);
    std::cout << J.dump(4) << std::endl;



    fg.shaderPaths->push_back(VKA_CMAKE_BINARY_DIR "/third_party/vkff/shaders");
    fg.fromJson(J);

    vkff::FrameGraphInitInfo ii;
    ii.device         = m_device;
    ii.physicalDevice = m_physicalDevice;

    fg.initialize(ii);
}

void RenderSystem3::render(vkff::FrameGraphExecuteInfo &info)
{
    glm::vec2 screenSize( info.swapchainExtent.width , info.swapchainExtent.height );
    getSystemBus()->variables.screenSize = screenSize;

    auto & fg = *m_resources->framegraph;


    // free all the staging buffers what were
    // allocated during the staging process
    _freeStagingBuffers();


    EASY_BLOCK("FrameGraph Execute");
    // execute the framegraph
    fg.execute(*this, info);
    EASY_END_BLOCK
    // free any staging buffers that were created

}


//=================================================================================
//=================================================================================

void RenderSystem3::init(RenderPassInit &I)
{
    if( I.renderPassName == MAIN_OBJECT_PASS_NAME || I.renderPassName == "SWAPCHAIN")
    {
        auto mrs = std::dynamic_pointer_cast<MeshRendererSystem>(m_MeshRendererSystem);
        mrs->init(I);
    }
}

void RenderSystem3::destroy(RenderPassDestroy &D)
{
    if( D.renderPassName == MAIN_OBJECT_PASS_NAME || D.renderPassName == "SWAPCHAIN")
    {
        auto mrs = std::dynamic_pointer_cast<MeshRendererSystem>(m_MeshRendererSystem);
        mrs->destroy(D);
    }
}

void RenderSystem3::update(RenderPassUpdate &D)
{
    if( D.renderPassName == MAIN_OBJECT_PASS_NAME || D.renderPassName == "SWAPCHAIN" )
    {
        auto mrs = std::dynamic_pointer_cast<MeshRendererSystem>(m_MeshRendererSystem);
        mrs->update(D);
    }
}

void RenderSystem3::write(RenderPassWrite &C)
{
    //SceneView m_sceneView;
    auto & reg = getSystemBus()->registry;

    (void)reg;
//    R.uniformStorageBuffer.reset();

    _frameCount++;
    if( C.renderPassName == MAIN_OBJECT_PASS_NAME || C.renderPassName == "SWAPCHAIN")
    {
        C.cmd.resetQueryPool(m_resources->queryPool,0, static_cast<uint32_t>(m_resources->queryData.size()) );

        //bool passSuccess=true;

        //=================================================
        // execute the transfers for all the textures
        // that havent been uploaded yet
        //=================================================

        EASY_BLOCK("PrimitiveTransfer");
        {
            auto sys         = std::dynamic_pointer_cast<RenderSystemPrimitiveManagementSystem>(m_PrimitiveManagementSystem);
            sys->writeCommandBuffer(C.cmd);
        }
        EASY_END_BLOCK

        EASY_BLOCK("ImageTransfer");
        {
            auto sys             = std::dynamic_pointer_cast<RenderSystemTextureManagementSystem>(m_TextureManagementSystem);
            sys->writerCommandBuffer(C.cmd);
        }
        EASY_END_BLOCK
        //=================================================


        //=================================================
        // Main Pipline Barrer. Don't do any
        // rendering unless all the transfers have
        // completed
        //=================================================
        {
            vk::MemoryBarrier memoryBarrier;
            memoryBarrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite; //vk::AccessFlags::eTra  eT VK_ACCESS_TRANSFER_WRITE_BIT;
            memoryBarrier.dstAccessMask = vk::AccessFlagBits::eVertexAttributeRead ;//vk::Access:e VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;

            C.cmd.pipelineBarrier( vk::PipelineStageFlagBits::eTransfer,
                             vk::PipelineStageFlagBits::eVertexInput,
                             vk::DependencyFlags(),
                             memoryBarrier,
                             nullptr,
                             nullptr);
        }
        //=================================================



        //===================================================================
        // BEGIN RENDERING
        //===================================================================
        EASY_BLOCK("Begin RenderPass");
        // The object pass is a forward rendering
        // pass that renders all objects using
        // the Khronous PBR shader. It renders
        // objects to the an offscreen buffer
        C.cmd.beginRenderPass( C.renderPassBeginInfo, vk::SubpassContents::eInline);

            auto meshRenderer = std::dynamic_pointer_cast<MeshRendererSystem>(m_MeshRendererSystem);
            meshRenderer->write(C);


        C.cmd.endRenderPass();

        assert( C.outputImages.size() >= 1);

        //===================================================================
        // END RENDERING
        //===================================================================
        {
            C.cmd.pipelineBarrier( vk::PipelineStageFlagBits::eColorAttachmentOutput,
                             vk::PipelineStageFlagBits::eFragmentShader,
                             vk::DependencyFlags(),
                             nullptr,
                             nullptr,
                             nullptr);
        }

    }
}


}
}







