#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/Components/CMesh.h>

#include <vka/utils/GLSLCompiler.h>
#include <vka/ecs/ResourceObjects/Environment.h>
#include <vka/ecs/SceneView.h>
#include "device_objects.h"
#include "commandbuffer_helpers.inl"
#include "RenderSystem3_Resources.h"
#include <vka/ecs/PunctualLight.h>

#include <vkff/framegraph.h>
#include <vka/ecs/Components/Tags.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CPose.h>
#include <vka/utils/filesystem.h>
#include <vka/ecs/VulkanBuffers.h>

#include "MeshRenderSystem.h"

namespace vka
{

namespace ecs
{

void MeshRendererSystem::onStart()
{
    m_materialManager  = getSystemBus()->getResourceManager_p<PBRMaterial>    ();
    m_envManager       = getSystemBus()->getResourceManager_p<Environment>    ();
    m_textureManager   = getSystemBus()->getResourceManager_p<HostTexture2D>  ();
    m_cubeManager      = getSystemBus()->getResourceManager_p<HostTextureCube>();
    m_primitiveManager = getSystemBus()->getResourceManager_p<HostMeshPrimitive>();
    m_pbrPipelineManager = getSystemBus()->getResourceManager_p<PBRPipeline>();

    auto skyboxMesh = vka::ecs::HostMeshPrimitive::Sphere(1.0f,32,32);
    m_skyboxMesh = getSystemBus()->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("/MRS/skyBoxMesh", std::move(skyboxMesh) );
    stageResource(m_skyboxMesh);


    PBRMaterial defaultMaterial;
    m_defaultMaterial = getSystemBus()->resourceCreateAndEmplace<PBRMaterial>("/MRS/defaultMaterial", std::move(defaultMaterial) );

    stageResource(m_defaultMaterial);

    if( !m_brdf.valid() )
    {
        m_brdf = getSystemBus()->resourceGetOrCreate<HostTexture2D>(vka::uri("rc:LUT/brdf.png"));
        getSystemBus()->resourceHostLoadBackground(m_brdf);
    }

    if( !m_defaultCubeMap.valid())
    {
        HostTextureCube H;
        H.resize(32);
        H.apply(0,
                [](auto x, auto y, auto z)
        {
            (void)x;
            (void)y;
            (void)z;
            return vka::vec4(1,1,1,1);
        });
        m_defaultCubeMap = getSystemBus()->resourceGetOrEmplace<HostTextureCube>("/MRS/defaultCube", std::move(H));

        getSystemBus()->resourceHostLoadBackground(m_defaultCubeMap);
    }

    Environment defaultEnv;
    defaultEnv.skybox     = m_defaultCubeMap;
    defaultEnv.radiance   = m_defaultCubeMap;
    defaultEnv.irradiance = m_defaultCubeMap;
    m_defaultEnv          = getSystemBus()->resourceGetOrEmplace<Environment>("/MRS/defaultEnv", std::move(defaultEnv));
    stageResource(m_defaultEnv);



    m_pipelines.simple   = getSystemBus()->resourceGetOrCreate<PBRPipeline>( vka::uri("rc:shaders/RenderSystem3/PBR_KRH.pipeline") );
    m_pipelines.skybox   = getSystemBus()->resourceGetOrCreate<PBRPipeline>( vka::uri("rc:shaders/RenderSystem3/PBR_KRH_skybox.pipeline") );

}

void MeshRendererSystem::onStop()
{
}

void MeshRendererSystem::onUpdate()
{
    if( !m_initialized)
        return;

    if( !m_sceneView_ptr)
        return;

    if( !m_device )
        return;

    if( !m_resources )
        return;

    auto & sceneV = *m_sceneView_ptr;

    // always stage the default environment
    stageResource(m_defaultEnv);

    // always stage the default material
    stageResource(m_defaultMaterial);

    // always stage the skybox mesh
    stageResource(m_skyboxMesh);

    // Stage the brdf resource so that
    // it will always be loaded into the GPU.
    stageResource(m_brdf);

    // set the environment id to the default one
    auto envID = m_defaultEnv;//sceneV.environment;

    envID = sceneV.environment;

    // If the envID is invalid, it means
    // the user did not specifcy an environmetn and the
    // default one has not been set.
    //if( !envID.valid() )
    //    return;

    // if the radiance/irradiance texture is different
    // from the last time, then we'll need to update
    // the descriptor set.
    auto & MaterialManager = getSystemBus()->getResourceManager<PBRMaterial>();
    auto & EnvManager = getSystemBus()->getResourceManager<Environment>();

    (void)MaterialManager;
    (void)EnvManager;
}

void MeshRendererSystem::init(vkff::FrameGraphExecutor::RenderPassInit &I)
{
    (void)I;
}

void MeshRendererSystem::init(vk::Device device, vk::RenderPass renderPass, bool enableDepth)
{
    (void)renderPass;
    (void)enableDepth;
    assert( m_resources );
    m_device = device;
    {
        // Initialize the descriptor pool
        vkb::DescriptorPoolCreateInfo2 pI;

        pI.setPoolSize( vk::DescriptorType::eCombinedImageSampler, 30 );
        pI.setPoolSize( vk::DescriptorType::eUniformBuffer       , 30 );
        pI.setPoolSize( vk::DescriptorType::eStorageBuffer       , 40 );
        pI.setMaxSets(500);
        m_DescriptorPool = pI.create( m_resources->storage, m_device);
    }

    // =============== Allocate the Uniform Buffer for Materials.
    {
        m_MultiStorageBuffer  = m_resources->allocateMultiStorageBuffer(1024*1024*10);
        m_GlobalUniformBuffer = m_resources->allocateUniformBuffer<GlobalUniform_t>();
    }


    //===============================================================================================================================
    // Descriptor Set 0 contains only the Global uniform buffer
    // and potentially any other buffers that are global to the Mesh Shader
    // this includes:
    //       0. the global uniform buffer
    //       1. the multistorage buffer
    //       5. the matrix storage buffer
    //===============================================================================================================================
    {
        vkb::DescriptorSetLayoutCreateInfo2 L;
        // Descriptor Set 0:  Global Uniform Buffer and the Fragment Materials buffer
        L.addDescriptor(0, vk::DescriptorType::eUniformBuffer       , 1, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment)  // materials buffer
                .addDescriptor(1, vk::DescriptorType::eStorageBuffer       , 1, vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment);

        m_DSetLayout_GlobalUniformMultiStorageBuffer = L.create(m_resources->storage,m_device);

        vk::DescriptorSetAllocateInfo a;
        a.setPSetLayouts( &m_DSetLayout_GlobalUniformMultiStorageBuffer);
        a.setDescriptorSetCount(1);
        a.setDescriptorPool(m_DescriptorPool);

        m_DSet_GlobalUniformMultiStorageBuffer = device.allocateDescriptorSets(a).front();


        {
            auto dset = m_DSet_GlobalUniformMultiStorageBuffer;
            vkb::DescriptorSetUpdater updater;
            updater.updateBufferDescriptor(dset, 0, 0, vk::DescriptorType::eUniformBuffer, {{m_GlobalUniformBuffer.buffer()     , 0, m_GlobalUniformBuffer.byteSize()}});
            updater.updateBufferDescriptor(dset, 1, 0, vk::DescriptorType::eStorageBuffer, {{m_MultiStorageBuffer.buffer()     , 0, m_MultiStorageBuffer.byteSize()}});
            updater.update( m_device );
        }

    }
    //===============================================================================================================================
    // We're going to use this descriptor pool
    // to a texture array of size MAX_TEXTURES_BOUND
    // we will allocate one texture array per swapchain.
    //===============================================================================================================================
    {
        vkb::PipelineLayoutCreateInfo2 L;

        L.setLayouts.push_back(m_DSetLayout_GlobalUniformMultiStorageBuffer);

        if( _getMaxTextures() > 0 )
            L.setLayouts.push_back( _getTextureDescriptorSetLayout() );

        L.addPushConstantRange( vk::ShaderStageFlagBits::eVertex | vk::ShaderStageFlagBits::eFragment, 0, sizeof(push_constants_type));

        m_PipelineLayout = L.create( m_resources->storage, m_device);
    }


    m_initialized=true;
}

void MeshRendererSystem::update(vkff::FrameGraphExecutor::RenderPassUpdate &I)
{
    // Do nothing in this point
    (void)I;
}

void MeshRendererSystem::destroy(vkff::FrameGraphExecutor::RenderPassDestroy &D)
{
    (void)D;

    for(auto e : m_pbrPipelineManager->view<vkb::DynamicPipeline>())
    {
        auto & d = m_pbrPipelineManager->get<vkb::DynamicPipeline>(e);
        d.destroy();
    }

    m_resources->freeBuffer(m_MultiStorageBuffer.m_buffer);
    m_resources->freeBuffer(m_GlobalUniformBuffer.m_buffer);

    D.device.destroyDescriptorPool(m_DescriptorPool);
}

void MeshRendererSystem::write(vkff::FrameGraphExecutor::RenderPassWrite &C)
{
    m_renderPass = C.renderPass;
    // Clear all the materials we used from the last frame
    m_boundMaterials.clear();

    // This functional is set by the RenderSystem3
    // it is used to get the DescriptorSetLayout which
    // holds the global Texture array
    // This is NEEDED for being able to render anything.
    // The layout is only possible once the
    // default texture and texture cube have been
    // created and copied into the GPU
    if( !_getTextureDescriptorSetLayout )
    {
        return;
    }
    if( !m_pipelinesInitialized )
    {
        init(C.device, C.renderPass, C.outputFormats.size()==2);
        m_pipelinesInitialized = true;
    }

    m_lastBoundPrimitive = MeshPrimitive_ID();

    m_writeSuccess = false;
    if( !m_sceneView_ptr)
        return;

    auto & m_sceneView = *m_sceneView_ptr;

    auto envID = m_sceneView_ptr->environment;


    //====================================================================================
    // Initial check to determine if we can render or now
    // environment must be fully loaded
    // before we can render anything
    if( !resourceIsDeviceLoaded(m_brdf))
        return;
    //====================================================================================



    //====================================================================================
    // update the global uniform buffers
    //====================================================================================
    GlobalUniform_t uniform;
    {
        auto & V = m_sceneView;

        uniform.CAMERA_POSITION = V.cameraPosition;
        uniform.VIEW            = V.cameraTransform.getViewMatrix();
        uniform.PROJECTION      = V.projMatrix;
        uniform.PROJECTION_VIEW = uniform.PROJECTION * uniform.VIEW;
        uniform.WINDOW          = {static_cast<float>(V.screenDimensions.width )
                                  ,static_cast<float>(V.screenDimensions.height)};

        static double t = 0.0f;
        t += 0.016;

        uniform.TIME_INT        = std::floor(static_cast<float>(t));
        uniform.TIME_FRAC       = static_cast<float>( t-std::floor(t) );
        uniform.TIME_DELTA      = 0.016f;

        m_GlobalUniformBuffer << uniform;
    }

    //==================================================================================================
    // begin teh command buffer by
    // setting the m_activeCommandBuffer variable
    // and binding the global storage and global texture arary
    m_renderPass = C.renderPass;
    beginCommandBuffer(C.cmd);
    //==================================================================================================

    //==================================================================================================
    // Push the View and Projection and view proj matrices
    //==================================================================================================
    {
        auto index = _pushMultiStorageData( uniform.VIEW );
        pushConstants(&index, offsetof(push_constants_type,viewMatrixIndex), sizeof(index));
    }
    {
        auto index = _pushMultiStorageData( uniform.PROJECTION );
        pushConstants(&index, offsetof(push_constants_type,projMatrixIndex), sizeof(index));
    }
    {
        auto index = _pushMultiStorageData( uniform.PROJECTION_VIEW );
        pushConstants(&index, offsetof(push_constants_type,projViewMatrixIndex), sizeof(index));
    }

    vk::Viewport vp(static_cast<float>(m_sceneView.renderArea.offset.x),
                    static_cast<float>(m_sceneView.renderArea.offset.y),
                    static_cast<float>(m_sceneView.renderArea.extent.width),
                    static_cast<float>(m_sceneView.renderArea.extent.height),0,1);
    C.cmd.setScissor(  0, m_sceneView.renderArea);
    C.cmd.setViewport( 0, vp);
    C.cmd.setLineWidth(1.0f);


    {
        auto & PL = setActivePipeline(m_pipelines.simple);

        PL.setPolygonMode(vk::PolygonMode::eFill);
        PL.setFrontFace(  vk::FrontFace::eCounterClockwise);
        PL.setCullMode(   vk::CullModeFlagBits::eBack);
        PL.setTopology(   vk::PrimitiveTopology::eTriangleList);

        bindActivePipeline();
    }


    //==================================================================================================
    // bind the default environment
    bindEnvironment(envID);
    //==================================================================================================


    // bind the default material just in cases
    bindMaterial(m_defaultMaterial);


    std::vector<entity_type> unDrawn;
    std::vector<entity_type> withBones;

    {
        int32_t lightCount=0;
        pushConstants(&lightCount, offsetof(push_constants_type, lightCount), sizeof(push_constants_type::lightCount));
    }

    auto & reg = getSystemBus()->registry;

    std::vector<Light_t> lightSources;
    for(auto e : m_sceneView.lightEntities)
    {
        auto & L    = reg.get<CLight>(e);
        auto & P    = reg.get<CPosition>(e);

        auto & l    = lightSources.emplace_back();
        l.color     = L.color;
        l.intensity = L.intensity;
        l.type      = static_cast<int32_t>(L.type);
        l.position  = P;
        l.direction = has<CRotation>(e) ? get<CRotation>(e) * glm::vec3(0,0,1) : glm::vec3(0,0,1);
        l.range     = L.range; // temporary
        l.intensity = L.intensity; //temporary
        l.outerConeCos = std::cos(L.outer_angle_rad);
        l.innerConeCos = std::cos(L.inner_angle_rad);
    }
    if( lightSources.size())
    {
        auto lightIndex = _pushMultiStorageData(lightSources.data(), lightSources.size());
        auto lightCount = static_cast<int32_t>( lightSources.size() );
        pushConstants(&lightIndex, offsetof(push_constants_type, lightIndex), sizeof(push_constants_type::lightIndex));
        pushConstants(&lightCount, offsetof(push_constants_type, lightCount), sizeof(push_constants_type::lightCount));
    }
    //==================================================================================================
    // Draw all the meshes that do not have bones
    // these are likely static meshes or dynamic meshes
    // we may separate these into their own loops in the future
    //==================================================================================================

    DrawReq drawReq;
    drawReq.polygonMode = vk::PolygonMode::eFill;
    drawReq.topology    = vk::PrimitiveTopology::eTriangleList;
    drawReq.cullMode    = vk::CullModeFlagBits::eBack;
    drawReq.wireframe   = false;

    size_t drawReqHash = drawReq.hash();
    (void)drawReqHash;

    {
        auto & PL = setActivePipeline(m_pipelines.simple);

        PL.setPolygonMode(vk::PolygonMode::eFill);
        PL.setFrontFace(  vk::FrontFace::eCounterClockwise);
        PL.setCullMode(   vk::CullModeFlagBits::eBack);
        PL.setTopology(   vk::PrimitiveTopology::eTriangleList);

        bindActivePipeline();
    }

    // Draw all visible entities using teh standard
    // draw requirements: solid objects
    // with the base shader
    for(auto e : m_sceneView.visibleEntities)
    {
        auto & Cr =  get<CMesh>(e);
        auto & Wr =  get<CWorldTransform>(e);

        // Push the model matrix
        auto index = _pushMultiStorageData(Wr.worldMatrix);
        pushConstants(&index, offsetof(push_constants_type,transformIndex), sizeof(index));

        // if it has a skeleton add the
        // skeleton matrices right after
        if( has<CSkeleton>(e) )
        {
            auto & Br =  get<CSkeleton>(e);
            _pushMultiStorageData(Br.boneMatrices.data(), Br.boneMatrices.size() );
        }

        if( !_draw_CMesh( C, Cr, drawReqHash) )
        {
            unDrawn.push_back(e);
        }
    }
    //==================================================================================================

    std::vector<entity_type> unDrawn2;


    for(auto const & dq  : {
                              DrawReq{ vk::PrimitiveTopology::eLineList    , vk::PolygonMode::eFill, vk::CullModeFlagBits::eBack, false},
                              DrawReq{ vk::PrimitiveTopology::eTriangleList, vk::PolygonMode::eLine, vk::CullModeFlagBits::eBack, false},
                              DrawReq{ vk::PrimitiveTopology::eTriangleList, vk::PolygonMode::eFill, vk::CullModeFlagBits::eFront, false}
                           }
        )
    {
        unDrawn2.clear();
        if( unDrawn.empty() )
            break;

        auto hh = dq.hash();
        {
            auto & PLA = setActivePipeline(m_pipelines.simple);
            PLA.setPolygonMode(dq.polygonMode);
            PLA.setFrontFace(  vk::FrontFace::eCounterClockwise);
            PLA.setCullMode(   dq.cullMode);
            PLA.setTopology(   dq.topology);

            bindActivePipeline();
        }

        for(auto e : unDrawn)
        {
            auto & Cr =  get<CMesh>(e);
            auto & Wr =  get<CWorldTransform>(e);

            // Push the model matrix
            auto index = _pushMultiStorageData(Wr.worldMatrix);
            pushConstants(&index, offsetof(push_constants_type,transformIndex), sizeof(index));

            // if it has a skeleton add the
            // skeleton matrices right after
            if( has<CSkeleton>(e) )
            {
                auto & Br =  get<CSkeleton>(e);
                _pushMultiStorageData(Br.boneMatrices.data(), Br.boneMatrices.size() );
            }

            if( !_draw_CMesh( C, Cr, hh) )
            {
                unDrawn2.push_back(e);
            }
        }
        std::swap( unDrawn2, unDrawn);


    }
    //=======================================================================
    // Draw all the object that are in wireframe mode
    //=======================================================================
#if 0
    if( unDrawn.size() )
    {
        {
            auto & PLA = setActivePipeline(m_pipelines.simple);
            PLA.setPolygonMode(vk::PolygonMode::eFill);
            PLA.setFrontFace(  vk::FrontFace::eCounterClockwise);
            PLA.setCullMode(   vk::CullModeFlagBits::eBack);
            PLA.setTopology(   vk::PrimitiveTopology::eLineList);

            bindActivePipeline();
        }

        for(auto e : unDrawn)
        {
            auto & Cr = get<CMesh>(e);
            auto & Wr = get<CWorldTransform>(e);

            // Set the world space matrix for
            // this mesh
            auto index = _pushMultiStorageData(Wr.worldMatrix);
            pushConstants(&index, offsetof(push_constants_type,transformIndex), sizeof(index));

            _draw_CMesh( C, Cr, vk::PrimitiveTopology::eLineList);
        }
    }
#endif
    //=======================================================================



    //==================================================================================================
    // Draw the skybox.
    // The sky box is drawn manually in this phase. we may update this later
    // to use a material
    //==================================================================================================
    if( resourceIsDeviceLoaded(m_skyboxMesh) )
    {
        {
            auto & PLA = setActivePipeline(m_pipelines.skybox);
            PLA.setPolygonMode(vk::PolygonMode::eFill);
            PLA.setFrontFace(  vk::FrontFace::eCounterClockwise);
            PLA.setCullMode(   vk::CullModeFlagBits::eFront);
            PLA.setTopology(   vk::PrimitiveTopology::eTriangleList);

            bindActivePipeline();
        }

        vka::Transform t;// = Ct;
        t.position = m_sceneView.cameraPosition;
        t.scale    = vec3( m_sceneView.farPlane * 0.999f);
        auto M       = t.getMatrix();

        float lod = 0.0f;

        auto index = _pushMultiStorageData(M);
        pushConstants(&index, offsetof(push_constants_type,transformIndex), sizeof(index));

        auto & H = m_primitiveManager->getResource(m_skyboxMesh);

        assert(H.subMeshes.size());
        auto & drawCall = H.subMeshes.front();
        bindPrimitive(C.cmd, m_skyboxMesh);

        pushConstants(&lod, offsetof(push_constants_type,lod), sizeof(push_constants_type::lod));

        if( drawCall.indexCount > 0)
        {
            C.cmd.drawIndexed( drawCall.indexCount, 1, drawCall.firstIndex, drawCall.vertexOffset, 0);
        }
        else
        {
        }
    }

    m_writeSuccess = true;

    endCommandBuffer();

}

/**
 * @brief MeshRendererSystem::_drawMaterialPrimitive2
 * @param C
 * @param x
 * @param drawReqHash
 * @return
 *
 * Attempt to draw a CPrimitive. if the CPrimitive's drawReq's hash is different
 * from the one provided as an input parameter, it will not be drawn
 * and the function will return false.
 */
bool MeshRendererSystem::_drawMaterialPrimitive2(vkff::FrameGraphExecutor::RenderPassWrite &C,
                                                 CPrimitive &x,
                                                 size_t drawReqHash)
{
    {
        if( x.m_drawReqHash != drawReqHash)
        {
            return false;
        }

        auto & PrimitiveManager = *m_primitiveManager;

        if( !resourceIsDeviceLoaded(x.mesh, PrimitiveManager) )
            return false;

        auto & Mesh = PrimitiveManager.getResource(x.mesh);
        auto & drawCall = Mesh.subMeshes.at( std::min<size_t>( x.subMeshIndex, Mesh.subMeshes.size()-1));

        if( !drawCall.indexCount && !drawCall.vertexCount)
        {
            return false;
        }

        bindPrimitive(C.cmd, x.mesh);

        if( x.material.valid() )
        {
            bindMaterial(x.material);
        }
        else
        {
            bindMaterial(m_defaultMaterial);
        }

        uint32_t INSTANCES = 1;
        uint32_t FIRST_INSTANCE = 0;

        if( drawCall.indexCount)
        {
            C.cmd.drawIndexed( drawCall.indexCount, INSTANCES, drawCall.firstIndex, drawCall.vertexOffset, FIRST_INSTANCE);
        }
        else
        {
            C.cmd.draw( drawCall.vertexCount, INSTANCES, static_cast<uint32_t>(drawCall.vertexOffset), FIRST_INSTANCE);
        }
    }
    return true;
}

/**
 * @brief MeshRendererSystem::_draw_CMesh
 * @param C
 * @param Cr
 * @param drawReqHash
 * @return
 *
 * Attempt to draw a mesh and returns true if it managed to draw all primitives.
 *
 * The drawReqHash is the has of the DrawReq struct. Only CPrimitives with the
 * same hash will be drawn.
 */
bool MeshRendererSystem::_draw_CMesh(vkff::FrameGraphExecutor::RenderPassWrite &C,
                                     CMesh &Cr,
                                     size_t drawReqHash)
{
    bool isFullyDrawn = true;
    for(auto & x : Cr.primitives)
    {
        isFullyDrawn = _drawMaterialPrimitive2(C, x, drawReqHash);
    }
    return isFullyDrawn;
}


bool MeshRendererSystem::_drawMaterialPrimitive2(vkff::FrameGraphExecutor::RenderPassWrite &C, CPrimitive &x, vk::PrimitiveTopology selectedTopology)
{
    {
        //auto & MaterialManager = *m_materialManager;
        auto & PrimitiveManager = *m_primitiveManager;

        if( !resourceIsDeviceLoaded(x.mesh, PrimitiveManager) )
            return false;

        if( x.drawReq.topology!=selectedTopology)
        {
            return false;
        }
        auto & Mesh = PrimitiveManager.getResource(x.mesh);
        auto & drawCall = Mesh.subMeshes.at( std::min<size_t>( x.subMeshIndex, Mesh.subMeshes.size()-1));

        if( !drawCall.indexCount && !drawCall.vertexCount)
        {
            return false;
        }

        bindPrimitive(C.cmd, x.mesh);

        if( x.material.valid() )
        {
            bindMaterial(x.material);
        }
        else
        {
            bindMaterial(m_defaultMaterial);
        }

        uint32_t INSTANCES = 1;
        uint32_t FIRST_INSTANCE = 0;

        if( drawCall.indexCount)
        {
            C.cmd.drawIndexed( drawCall.indexCount, INSTANCES, drawCall.firstIndex, drawCall.vertexOffset, FIRST_INSTANCE);
        }
        else
        {
            C.cmd.draw( drawCall.vertexCount, INSTANCES, static_cast<uint32_t>(drawCall.vertexOffset), FIRST_INSTANCE);
        }
    }
    return true;
}




bool MeshRendererSystem::_draw_CMesh(vkff::FrameGraphExecutor::RenderPassWrite &C,
                                     CMesh &Cr,
                                     vk::PrimitiveTopology selectedTopology)
{
    bool isFullyDrawn = true;
    for(auto & x : Cr.primitives)
    {
        isFullyDrawn = _drawMaterialPrimitive2(C, x, selectedTopology);
    }
    return isFullyDrawn;
}


bool MeshRendererSystem::bindMaterial(PBRMaterial_ID id, uint32_t materialNumber)
{
    int32_t materialIndex = 0;

    // check if the material is already bound
    // if it is, we can return that instead
    auto it = m_boundMaterials.find(id);

    // its not bound, so place it in
    // the MultiStorageBuffer
    if( it == m_boundMaterials.end() )
    {
        auto & x = m_materialManager->get<PBRMaterial>(id);

        Material_t mat;

        mat.BaseColorFactor          = x.baseColorFactor;
        mat.RoughnessFactor          = x.roughnessFactor;
        mat.MetallicFactor           = x.metallicFactor;
        mat.EmissiveFactor           = x.emissiveFactor;
        mat.unlit                    = x.unlit;
        mat.DebugOutput              = static_cast<int>(x.debugMode);

        mat.BaseColorSampler         = findOrLoadTexture( x.baseColorTexture        );
        mat.NormalSampler            = findOrLoadTexture( x.normalTexture           );
        mat.MetallicRoughnessSampler = findOrLoadTexture( x.metallicRoughnessTexture);
        mat.EmissiveSampler          = findOrLoadTexture( x.emissiveTexture);
        mat.OcclusionSampler         = findOrLoadTexture( x.occlusionTexture);

        auto index = _pushMultiStorageData( mat );
        m_boundMaterials[id] = index;

        materialIndex = index;
    }
    else
    {
        materialIndex = it->second;
    }

    if( materialNumber == 0)
    {
        pushConstants(&materialIndex, offsetof(push_constants_type, materialIndex0), sizeof(materialIndex));
    }
    else
    {
        pushConstants(&materialIndex, offsetof(push_constants_type, materialIndex1), sizeof(materialIndex));
    }
    return materialIndex!=-1;
}

bool MeshRendererSystem::bindEnvironment(Environment_ID x)
{
    Environment_t Es;
    int32_t index = -1;

    if( x.valid() )
    {
        if( m_envManager->has<Environment>(x))
        {
            // Bind the environment by creating the
            // struct (Environemnt_t) that will be
            // sent to the shader. It contains
            // the indices into the Global Texture Cube
            // array at which to find each of the
            // radiance/irradiance/brdf textures
            auto & ec = m_envManager->get<Environment>(x);

            Es.irradiance = findOrLoadTextureCube( ec.irradiance );
            Es.radiance   = findOrLoadTextureCube( ec.radiance );
        }
        else
        {
            stageResource(x);
        }

    }
    Es.brdf       = findTexture( m_brdf );

    index = _pushMultiStorageData(Es);
    pushConstants(&index, offsetof(push_constants_type,environmentIndex1), sizeof(index) );

    return false;
}

int32_t MeshRendererSystem::_pushMatrices(vk::CommandBuffer cmd, const glm::mat4 &woldMatrix, vk::ArrayProxy<const glm::mat4> boneMatrices)
{
    auto index = _pushMultiStorageData(woldMatrix);

    cmd.pushConstants( m_PipelineLayout,
                       vk::ShaderStageFlagBits::eVertex|vk::ShaderStageFlagBits::eFragment,
                       offsetof(push_constants_type,transformIndex),
                       sizeof(push_constants_type::transformIndex),
                       &index);

    if( boneMatrices.size() )
    {
        _pushMultiStorageData(boneMatrices.data(), boneMatrices.size());
    }
    return index;
}

const DevicePrimitive &MeshRendererSystem::bindPrimitive(vk::CommandBuffer cmd, MeshPrimitive_ID id)
{
    auto & PrimitiveManager = *m_primitiveManager;
    if( m_lastBoundPrimitive == id) return *m_lastBoundDevicePrimitive;

    auto & dev = PrimitiveManager.get<DevicePrimitive>( id );

    uint32_t totalAttributes=8;
    cmd.bindVertexBuffers(0, totalAttributes, dev.buffers.data(), dev.attributeOffsets.data());
    if( dev.indexType.has_value() )
    {
        cmd.bindIndexBuffer(dev.buffer, dev.attributeOffsets.back(), *dev.indexType);
    }


    // set the attribute flags which indicates
    // which primitive attributes this primitive contains
    int32_t flags = dev.attributeFlags;
    cmd.pushConstants( m_PipelineLayout,
                       vk::ShaderStageFlagBits::eVertex|vk::ShaderStageFlagBits::eFragment,
                       offsetof(push_constants_type,attributeFlags),
                       sizeof(push_constants_type::attributeFlags),
                       &flags);

    m_lastBoundPrimitive = id;
    m_lastBoundDevicePrimitive = &dev;
    return dev;
}

sampler2D_id MeshRendererSystem::findOrLoadTexture(Texture2D_ID id)
{
    sampler2D_id i;
    if( !id.valid() ) return i;

    if( stageResource(id) ) // stage the resource
    {
        return findTexture(id);
    }
    else
    {
        return i;
    }

    return i;
}

samplerCube_id MeshRendererSystem::findOrLoadTextureCube(TextureCube_ID id)
{
    samplerCube_id i;
    if( !id.valid() ) return i;

    if( stageResource(id) ) // stage the resource
    {
        return findTexture(id);
    }
    else
    {
        return i;
    }

    return i;
}

sampler2D_id MeshRendererSystem::findTexture(Texture2D_ID id) const
{
    return sampler2D_id{_findTexture( id) };
}

samplerCube_id MeshRendererSystem::findTexture(TextureCube_ID id) const
{
    return samplerCube_id{_findTextureCube( id) };
}

int32_t MeshRendererSystem::findMaterial(PBRMaterial_ID id) const
{
    auto it = m_boundMaterials.find(id);

    if( it == m_boundMaterials.end() )
        return -1;

    return it->second;
}

void MeshRendererSystem::bindGlobalTextureArray(vk::CommandBuffer cb, vk::PipelineLayout layout, uint32_t set)
{
    if(_bindTextureArray)
        _bindTextureArray(cb, layout, set);
}

void MeshRendererSystem::bindGlobalStorage(vk::CommandBuffer cb, vk::PipelineLayout layout, uint32_t set)
{
    cb.bindDescriptorSets(vk::PipelineBindPoint::eGraphics, layout, set, m_DSet_GlobalUniformMultiStorageBuffer, nullptr);
}








/**
 * @brief MeshRendererSystem::initializePipeline
 * @param id
 *
 * This function initializes the pipeline id.
 */
void MeshRendererSystem::initializePipeline( PBRPipeline_ID const & id)
{
    auto sb = getSystemBus();

    auto _compileShader = [sb](const uri &U, GLSLCompiler comp, std::string entryPoint)
    {
        try
        {
            vkb::PipelineShaderStageCreateInfo2 ci;
            ci.stage = vk::ShaderStageFlagBits::eVertex;

            auto shaderPath   = sb->getPath( U );
            auto shaderFolder = vka::fs::parent_path(shaderPath);
            auto glslSource   = sb->readResourceASCII( U );
            auto ext          = vka::fs::extension(shaderPath);
            auto shaderType   = EShLangVertex;

            if( ext == "frag")
            {
                shaderType = EShLangFragment;
                ci.stage   = vk::ShaderStageFlagBits::eFragment;
            }

            comp.addIncludePath(shaderFolder);

            ci.code = comp.compile(glslSource, shaderType);
            ci.name = entryPoint;
            return ci;
        }
        catch (std::exception & e)
        {
            spdlog::critical("Error compiling Shader: {}", U.toString() );
            spdlog::critical(" {} ", comp.getLog() );
            throw e;
        }
    };


    auto PBRManager = getSystemBus()->getResourceManager_p<PBRPipeline>();

    if( !getSystemBus()->resourceIsHostLoaded(id) )
    {
        getSystemBus()->resourceHostLoadBackground(id);
    }
    auto & pd = PBRManager->getResource(id);

    vkb::GraphicsPipelineCreateInfo2 P;

    P.setVertexInputAttribute(0, 0, vk::Format::eR32G32B32Sfloat   , 0);
    P.setVertexInputAttribute(1, 1, vk::Format::eR32G32B32Sfloat   , 0);
    P.setVertexInputAttribute(2, 2, vk::Format::eR32G32B32Sfloat   , 0);
    P.setVertexInputAttribute(3, 3, vk::Format::eR32G32Sfloat      , 0);
    P.setVertexInputAttribute(4, 4, vk::Format::eR32G32Sfloat      , 0);
    P.setVertexInputAttribute(5, 5, vk::Format::eR8G8B8A8Unorm     , 0);
    P.setVertexInputAttribute(6, 6, vk::Format::eR16G16B16A16Uint  , 0);
    P.setVertexInputAttribute(7, 7, vk::Format::eR32G32B32A32Sfloat, 0);

    P.setVertexInputBinding(0, sizeof(glm::vec3)    , vk::VertexInputRate::eVertex);
    P.setVertexInputBinding(1, sizeof(glm::vec3)    , vk::VertexInputRate::eVertex);
    P.setVertexInputBinding(2, sizeof(glm::vec3)    , vk::VertexInputRate::eVertex);
    P.setVertexInputBinding(3, sizeof(glm::vec2)    , vk::VertexInputRate::eVertex);
    P.setVertexInputBinding(4, sizeof(glm::vec2)    , vk::VertexInputRate::eVertex);
    P.setVertexInputBinding(5, sizeof(glm::u8vec4)  , vk::VertexInputRate::eVertex);
    P.setVertexInputBinding(6, sizeof(glm::u16vec4) , vk::VertexInputRate::eVertex);
    P.setVertexInputBinding(7, sizeof(glm::vec4)    , vk::VertexInputRate::eVertex);

    P.layout     = m_PipelineLayout;
    P.renderPass = m_renderPass;

    //================================================================
    // Output attachments
    //================================================================
    P.addBlendStateAttachment().setColorBlendOp(vk::BlendOp::eAdd)
            .setBlendEnable(false);
    //================================================================


    P.viewportState.viewports.emplace_back();
    P.viewportState.scissors.emplace_back();
    P.dynamicStates = {{
                           vk::DynamicState::eViewport,
                           vk::DynamicState::eScissor,
                           vk::DynamicState::eLineWidth
                       }};

    if( true )
    {
        P.depthStencilState.setDepthTestEnable(true)
                .setDepthWriteEnable(true);
    }

    {
        GLSLCompiler compiler;

        if( _getMaxTextures() > 0)
            compiler.addCompleTimeDefinition("MAX_TEXTURES_BOUND"       , std::to_string(_getMaxTextures()) );

        if( _getMaxTextureCubes() > 0)
            compiler.addCompleTimeDefinition("MAX_CUBES_BOUND"       , std::to_string(_getMaxTextureCubes()) );


        compiler.addCompleTimeDefinition("VKA_USE_STORAGE_TRANSFORMS"  , "" );
        for(auto & cd : pd.compileTimeDefinitions)
        {
            compiler.addCompleTimeDefinition(cd.first  , cd.second );
        }

        //================================================================
        // Shaders: default PBR pipeline
        //================================================================
        P.stages.push_back( _compileShader( pd.vertexShader  , compiler, "main" ) );
        P.stages.push_back( _compileShader( pd.fragmentShader, compiler, "main" ) );


        vkb::DynamicPipeline dynamicPipeline;

        dynamicPipeline.init( &m_resources->storage, P, m_device);
        PBRManager->emplaceData(id, std::move(dynamicPipeline) );
    }
}



//==================================================================================================
// These functions will eventually be moved into the
// Painter class
//==================================================================================================
PBRPipeline_ID m_activePipeline;
vk::CommandBuffer m_currentCommandBuffer;

void MeshRendererSystem::endCommandBuffer()
{
    m_activePipeline = {};
    m_currentCommandBuffer = vk::CommandBuffer();
}

void MeshRendererSystem::beginCommandBuffer(vk::CommandBuffer cmd)
{
    m_currentCommandBuffer = cmd;

    //==================================================================================================
    // Bind the global storage buffers
    // the global store buffers contain:
    // 0. The Global Uniform Buffer (all stages)
    // 1. The MultiStorageBuffer (all stages)
    //==================================================================================================
    bindGlobalStorage(m_currentCommandBuffer, m_PipelineLayout, 0);

    //==================================================================================================
    // Bind the texture array. The texture array
    // is a descriptor set which contains an array of textures
    // all textures are accessable.
    //==================================================================================================
    bindGlobalTextureArray(m_currentCommandBuffer, m_PipelineLayout, 1); // descriptor set 1
    //==================================================================================================
}

vkb::DynamicPipeline& MeshRendererSystem::setActivePipeline( PBRPipeline_ID const & id )
{
    m_activePipeline = id;
    if( m_pbrPipelineManager->has<vkb::DynamicPipeline>(m_activePipeline) )
    {
        return m_pbrPipelineManager->get<vkb::DynamicPipeline>(m_activePipeline);
    }
    else
    {
        // initialize the pipeline.
        // Note: the renderpass and pipeline layout must be
        // created already
        initializePipeline(m_activePipeline);
        return setActivePipeline(id);
    }
}

void MeshRendererSystem::bindActivePipeline( )
{
    auto & d = m_pbrPipelineManager->get<vkb::DynamicPipeline>(m_activePipeline);
    auto  ps = d.get();

    m_currentCommandBuffer.bindPipeline(vk::PipelineBindPoint::eGraphics, std::get<0>(ps) );
}

void MeshRendererSystem::pushConstants( void const * data, uint32_t offset, uint32_t size)
{
    m_currentCommandBuffer.pushConstants( m_PipelineLayout,
                                          vk::ShaderStageFlagBits::eVertex|vk::ShaderStageFlagBits::eFragment,
                                          offset,
                                          size,
                                          data);
}

}

}

