#include <vka/ecs/RenderSystem3.h>

#include <vka/ecs/Components/Tags.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CCamera.h>
#include <vka/ecs/Components/CAnimator2.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CLight.h>

#include <vkff/framegraph.h>

#include <vkb/utils/TextureArrayDescriptorSet.h>

#include <easy/profiler.h>
#include <easy/arbitrary_value.h> // EASY_VALUE, EASY_ARRAY are defined here

#include <glm/gtx/io.hpp>


#include "RenderSystem3_Resources.h"
#include "MeshRenderSystem.h"
#include "device_objects.h"
#include "commandbuffer_helpers.inl"

#include "RenderSystem_ArrayOfTexturesSystem.h"

namespace vka
{

namespace ecs
{

static uint64_t _frameCount=0;

uint32_t RenderSystemArrayOfTextureSystem::getMaxTextures() const
{
    return MAX_TEXTURES_BOUND;
}

uint32_t RenderSystemArrayOfTextureSystem::getMaxTextureCubes() const
{
    return MAX_TEXTURES_CUBES_BOUND;
}

vk::DescriptorSetLayout RenderSystemArrayOfTextureSystem::getLayout() const
{
    return m_textureArrayChain.getDescriptorSetLayout();
}

int32_t RenderSystemArrayOfTextureSystem::findTexture(Texture2D_ID v)
{
    if( !v.valid() )
        return -1;

    if( m_texManager->has<DeviceTexture>(v) )
    {
        auto i = m_textureArrayChain.findTexture( m_texManager->get<DeviceTexture>(v).view , 0);

        // update the the last time this id was found
        // in the map
        m_lastFoundT[v] = m_frameNumber;
        m_inactiveTextures.erase(v);
        return i;
    }
    return -1;
}

int32_t RenderSystemArrayOfTextureSystem::findTextureCube(TextureCube_ID v)
{
    if( !v.valid() )
        return -1;
    if( m_cubeManager->has<DeviceTexture>(v))
    {
        auto i = m_textureArrayChain.findTexture( m_cubeManager->get<DeviceTexture>(v).view , 1);
        // update the the last time this id was found
        // in the map
        m_lastFoundC[v] = m_frameNumber;
        m_inactiveCubes.erase(v);
        return i;
    }
    return -1;
}

void RenderSystemArrayOfTextureSystem::checkUnload()
{
    //return;
    for(auto it = m_lastFoundT.begin(); it!=m_lastFoundT.end();)
    {
        auto & v = *it;
        if( m_frameNumber - v.second > 100)
        {
            // Send the DEVICE_WILL_UNLOAD event
            // through the systembus so that
            // so that we know to remove the image view from
            // any descriptor sets
            EvtResourceEvent_t<HostTexture2D> E;
            E.type = ResourceEvtType::DEVICE_INACTIVE_STATE;
            E.id   = v.first;
            getSystemBus()->dispatcher.trigger(E);

            // place the resource in the inactive
            // textures set, so that
            // we can keep track of it
            // if we run out of memory
            // we can start removing textures
            m_inactiveTextures.insert(v.first);

            it = m_lastFoundT.erase(it);
            continue;
        }
        it++;
    }

    for(auto it = m_lastFoundC.begin(); it!=m_lastFoundC.end();)
    {
        auto & v = *it;
        if( m_frameNumber - v.second > 100)
        {
            // Send the DEVICE_WILL_UNLOAD event
            // through the systembus so that
            // so that we know to remove the image view from
            // any descriptor sets
            EvtResourceEvent_t<HostTextureCube> E;
            E.type = ResourceEvtType::DEVICE_INACTIVE_STATE;
            E.id   = v.first;
            getSystemBus()->dispatcher.trigger(E);

            // place the resource in the inactive
            // textures set, so that
            // we can keep track of it
            // if we run out of memory
            // we can start removing textures
            m_inactiveCubes.insert(v.first);

            it = m_lastFoundC.erase(it);

            continue;
        }
        it++;
    }

    if( m_frameNumber%25==24)
    {
        for(auto i : m_inactiveCubes)
        {
            //=====================================================
            // after 1 second, schedule the Cube to unload
            // from the device, thereby completely destroying the
            // vk::imageView and vk::image
            //=====================================================
            auto sb = getSystemBus();
            auto id = i;
            getSystemBus()->executeLater(
                        [sb,id]()
            {
                sb->resourceDeviceScheduleUnload( id );
            }, 1.0);

            //=====================================================
        }
        for(auto i : m_inactiveTextures)
        {
            //=====================================================
            // after 1 second, schedule the Cube to unload
            // from the device, thereby completely destroying the
            // vk::imageView and vk::image
            //=====================================================
            auto sb = getSystemBus();
            auto id = i;
            getSystemBus()->executeLater(
                        [sb,id]()
            {
                sb->resourceDeviceScheduleUnload( id );
            }, 1.0);

            //=====================================================
        }
        m_inactiveCubes.clear();
        m_inactiveTextures.clear();
    }
}

void RenderSystemArrayOfTextureSystem::bindTextureArray(vk::CommandBuffer cmd, vk::PipelineLayout layout, uint32_t set)
{
    cmd.bindDescriptorSets( vk::PipelineBindPoint::eGraphics,
                            layout, set, m_textureArrayChain.getCurrentDescriptorSet(), nullptr );
}

void RenderSystemArrayOfTextureSystem::onStart()
{
    assert(m_resources);

    m_texManager = getSystemBus()->getResourceManager_p<HostTexture2D>();
    m_cubeManager = getSystemBus()->getResourceManager_p<HostTextureCube>();

    // This event listener will be used to
    // check when a texture has been copied to the GPU
    // or when it is about to be removed from the GPU.
    getSystemBus()->dispatcher.sink< EvtResourceEvent_t<HostTexture2D> >().connect< &RenderSystemArrayOfTextureSystem::_onHostTexture2DEvent >(*this);
    getSystemBus()->dispatcher.sink< EvtResourceEvent_t<HostTextureCube> >().connect< &RenderSystemArrayOfTextureSystem::_onHostCubeEvent >(*this);

    // Create a default texture which will be used
    // to fill the texture arrays
    if( !m_defaultTexture.valid() )
    {
        HostTexture2D H;
        H.resize(8,8);
        H.getLayer(0).getLevel(0).r = 255;
        H.getLayer(0).getLevel(0).g = 0;
        H.getLayer(0).getLevel(0).b = 0;
        H.getLayer(0).getLevel(0).a = 255;

        m_defaultTexture = getSystemBus()->resourceGetOrEmplace<HostTexture2D>("/RenderSystemArrayOfTextureSystem/defaultTexture", std::move(H));

        getSystemBus()->stageResource(m_defaultTexture);
    }
    if( !m_defaultCubeMap.valid())
    {
        HostTextureCube H;
        H.resize(32);
        H.apply(0,
                [](auto x, auto y, auto z)
        {
            (void)x;
            (void)y;
            (void)z;
            return vka::vec4(1,1,1,1);
        });
        m_defaultCubeMap = getSystemBus()->resourceGetOrEmplace<HostTextureCube>("/RenderSystemArrayOfTextureSystem/defaultCube", std::move(H));

        getSystemBus()->stageResource(m_defaultCubeMap);
    }
}

void RenderSystemArrayOfTextureSystem::onStop()
{
    getSystemBus()->dispatcher.sink< EvtResourceEvent_t<HostTexture2D>   >().disconnect< &RenderSystemArrayOfTextureSystem::_onHostTexture2DEvent >(*this);
    getSystemBus()->dispatcher.sink< EvtResourceEvent_t<HostTextureCube> >().disconnect< &RenderSystemArrayOfTextureSystem::_onHostCubeEvent >(*this);
}

void RenderSystemArrayOfTextureSystem::releaseResources()
{
    m_textureArrayChain.destroy(m_device, m_resources->storage);
}

void RenderSystemArrayOfTextureSystem::onUpdate()
{
    checkUnload();
    m_frameNumber++;
    _frameCount++;
    if( !m_textureArrayInitialized )
        return;

    m_textureArrayChain.nextArray();
    vkb::DescriptorSetUpdater updater;

    auto c = m_textureArrayChain.update(updater);
    if( c )
    {
        //S_INFO("Textures Updated: {} ", c );
        updater.update( m_device );
    }
}

void RenderSystemArrayOfTextureSystem::_onHostTexture2DEvent(EvtResourceEvent_t<HostTexture2D> &E)
{
    auto sb = getSystemBus();

    switch(E.type)
    {
    // Called when the texture has been loaded into
    // gpu memory and is ready to be used.
    case ResourceEvtType::DEVICE_LOADED:
    {
        if( !m_textureArrayInitialized)
        {
            initilizeTextureArraySet();
            m_texturesToAdd.insert( E.id );
            S_INFO("Storing Texture ({}) for later", sb->getResourceManager_p<HostTexture2D>()->getResourceName(E.id) );
        }
        else
        {
            // the texture array descriptor has been initialized
            // so make sure to add all the textures that were saved
            for(auto &x : m_texturesToAdd)
            {
                auto & D = sb->getResourceManager_p<HostTexture2D>()->get<DeviceTexture>(x);
                auto ind = _insertTexture( D.view, 0);
                S_INFO("Inserting Texture ({}) at index {}", sb->getResourceManager_p<HostTexture2D>()->getResourceName(x), ind );
            }
            m_texturesToAdd.clear();
            auto & D = sb->getResourceManager_p<HostTexture2D>()->get<DeviceTexture>(E.id);
            auto ind = _insertTexture( D.view, 0);
            S_INFO("Inserting Texture ({}) at index {}", sb->getResourceManager_p<HostTexture2D>()->getResourceName(E.id), ind );
        }
        break;
    }
    case ResourceEvtType::DEVICE_INACTIVE_STATE:
    {
        auto & D = sb->getResourceManager_p<HostTexture2D>()->get<DeviceTexture>(E.id);
        auto ind = _removeTexture(D.view, 0);
        S_INFO("Removing Texture ({}) from index {}", sb->getResourceManager_p<HostTexture2D>()->getResourceName(E.id), ind );
        break;
    }
    default:
        break;
    }
}

void RenderSystemArrayOfTextureSystem::_onHostCubeEvent(EvtResourceEvent_t<HostTextureCube> &E)
{
    auto sb = getSystemBus();

    switch(E.type)
    {
    // Called when the texture has been loaded into
    // gpu memory and is ready to be used.
    case ResourceEvtType::DEVICE_LOADED:
    {
        if( !m_textureArrayInitialized)
        {
            initilizeTextureArraySet();
            m_cubersToAdd.insert( E.id );
        }
        else
        {
            // the texture array descriptor has been initialized
            // so make sure to add all the textures that were saved
            for(auto &x : m_cubersToAdd)
            {
                auto & D = sb->getResourceManager_p<HostTextureCube>()->get<DeviceTexture>(x);
                auto ind = _insertTexture( D.view, 1);
                S_INFO("Inserting Cube ({}) at index {}", sb->getResourceManager_p<HostTextureCube>()->getResourceName(x), ind );
            }
            m_cubersToAdd.clear();
            auto & D = sb->getResourceManager_p<HostTextureCube>()->get<DeviceTexture>(E.id);
            auto ind = _insertTexture( D.view, 1);
            S_INFO("Inserting Cube ({}) at index {}", sb->getResourceManager_p<HostTextureCube>()->getResourceName(E.id), ind );
        }
        break;
    }
    case ResourceEvtType::DEVICE_INACTIVE_STATE:
    {
        auto & D = sb->getResourceManager_p<HostTextureCube>()->get<DeviceTexture>(E.id);
        S_INFO("Removing Cube {}", sb->getResourceManager_p<HostTextureCube>()->getResourceName(E.id) );
        _removeTexture(D.view, 1);
        break;
    }
    default:
        break;
    }
}

void RenderSystemArrayOfTextureSystem::initilizeTextureArraySet()
{

    auto defaultTex  = m_defaultTexture;
    auto defaultCube = m_defaultCubeMap;

    if( m_textureArrayInitialized)
        return;

    if(    getSystemBus()->resourceIsDeviceLoaded(defaultTex)
           && getSystemBus()->resourceIsDeviceLoaded(defaultCube)     )
    {
        vkb::TextureArrayDescriptorSetChainCreateInfo ci;
        ci.chainSize          = 5;

        {
            DeviceTexture const & defaultDeviceTex = getSystemBus()->getResourceManager_p<HostTexture2D>()->get<DeviceTexture>(defaultTex);
            auto & bi = ci.bindings.emplace_back();
            bi.descriptorSetBinding = 0;
            bi.textureCount         = MAX_TEXTURES_BOUND;
            bi.defaultImageLayout   = vk::ImageLayout::eShaderReadOnlyOptimal;
            bi.defaultSampler       = defaultDeviceTex.sampler;
            bi.defaultImageView     = defaultDeviceTex.view;
        }

        if( defaultCube.valid() )
        {
            DeviceTexture const & defaultDeviceTex = getSystemBus()->getResourceManager_p<HostTextureCube>()->get<DeviceTexture>(defaultCube);

            auto & bi = ci.bindings.emplace_back();
            bi.descriptorSetBinding = 1;
            bi.textureCount         = MAX_TEXTURES_CUBES_BOUND;
            bi.defaultImageLayout   = vk::ImageLayout::eShaderReadOnlyOptimal;
            bi.defaultSampler       = defaultDeviceTex.sampler;
            bi.defaultImageView     = defaultDeviceTex.view;
        }
        m_textureArrayChain.create(m_device, m_resources->storage, ci);
        m_textureArrayInitialized = true;
    }


}

int32_t RenderSystemArrayOfTextureSystem::_removeTexture(vk::ImageView v, uint32_t binding)
{
    auto s = m_textureArrayChain.removeTexture(v, binding);
    return s;
}

int32_t RenderSystemArrayOfTextureSystem::_insertTexture(vk::ImageView v, uint32_t binding)
{
    auto s = m_textureArrayChain.insertTexture(v, binding);
    return s;
}


}
}
