#define STB_IMAGE_IMPLEMENTATION
#define VKA_IMAGE_IMPLEMENTATION

#define VKAI_NO_STDIO
#define STBI_NO_STDIO


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wunused-function"

//#include <vka/utils/vka_image.h>

#pragma GCC diagnostic pop
