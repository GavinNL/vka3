#include <vka/ecs/RenderSystem3.h>
#include <vka/ecs/SceneView.h>

#include <vkb/vkb.h>

#include "RenderSystem3_Resources.h"

#include "vk_mem_alloc.h"
#include "commandbuffer_helpers.inl"
#include "device_objects.h"

#include <easy/profiler.h>

namespace vka {
namespace ecs {

void RenderSystem3::_onDestroyDeviceTexture2D(entt::registry & r, entt::entity e)
{
    auto &D = r.get<DeviceTexture>(e);

    if( D.view )
    {
        EvtResourceEvent_t<HostTexture2D> E;
        E.type = ResourceEvtType::DEVICE_WILL_DESTROY;
        E.id   = getSystemBus()->getResourceManager_p<HostTexture2D>()->getID(e);
        getSystemBus()->dispatcher.trigger(E);

        m_device.destroyImageView(D.view);
        D.view = vk::ImageView();
    }
    if( D.image )
    {
        m_resources->freeImage( D.image, D.allocation );
        D.image = vk::Image();
        D.allocation = nullptr;
    }
    S_INFO("DeviceTexture (HostTexture2D) destroyed");
}

void RenderSystem3::_onDestroyDeviceTextureCube(entt::registry & r, entt::entity e)
{
    auto &D = r.get<DeviceTexture>(e);
    if( D.view )
    {
        EvtResourceEvent_t<HostTextureCube> E;
        E.type = ResourceEvtType::DEVICE_WILL_DESTROY;
        E.id   = getSystemBus()->getResourceManager_p<HostTextureCube>()->getID(e);
        getSystemBus()->dispatcher.trigger(E);

        m_device.destroyImageView(D.view);
        D.view = vk::ImageView();
    }
    if( D.image )
    {
        m_resources->freeImage( D.image, D.allocation );
        D.image = vk::Image();
        D.allocation = nullptr;
    }
    S_INFO("DeviceTexture (HostTextureCube) destroyed");
}

size_t RenderSystem3::_freeStagingBuffers()
{
    return 0;
    EASY_FUNCTION(profiler::colors::Magenta);
    auto & R   = *m_resources;
    if( R._toFree.size() )
    {
        for(auto & e : R._toFree)
        {
            vmaDestroyBuffer( R.allocator, e.first, e.second);
        }
        R._toFree.clear();
    }
    return 0;
}


void RenderSystem3::_onDestroyPrimitive(entt::registry & r, entt::entity e)
{
    (void)r;
    (void)e;
    auto & D = r.get<DevicePrimitive>(e);
    if( D.buffer )
    {
        EvtResourceEvent_t<HostMeshPrimitive> E;
        E.type = ResourceEvtType::DEVICE_WILL_DESTROY;
        E.id   = getSystemBus()->getResourceManager_p<HostMeshPrimitive>()->getID(e);
        getSystemBus()->dispatcher.trigger(E);

        m_resources->freeBuffer( D.buffer, D.allocation );
        D.buffer = vk::Buffer();
        D.allocation = nullptr;
        D.buffers.fill(D.buffer);
        D.attributeOffsets = {0};
        D.attributeFlags = 0;
        D.allocation = nullptr;
        D.byteSize = 0;
        D.drawCall = PrimitiveDrawCall();
        S_INFO("DevicePrimitive destroyed");
    }
}


}
}
