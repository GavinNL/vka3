#ifndef VKA_ECS_RENDERSYSTEM3_RENDERRESOURCES_H
#define VKA_ECS_RENDERSYSTEM3_RENDERRESOURCES_H

#include <vulkan/vulkan.hpp>
#include <vkb/vkb.h>
#include <vkb/utils/multistoragebuffer.h>
#include <vkb/utils/DynamicPipeline.h>
#include "vk_mem_alloc.h"
#include <vkff/framegraph.h>
#include <vka/utils/HostCubeImage.h>
#include <vka/ecs/ResourceObjects/HostPrimitive.h>
#include <vka/ecs/ID.h>
#include <vka/ecs/ResourceObjects/PBRMaterial.h>
#include <mutex>
#include <easy/profiler.h>

#include <vka/ecs/VulkanBuffers.h>

namespace vka
{
namespace ecs
{

struct RenderSystem3Resources
{
    VmaAllocator            allocator;
    vkb::Storage            storage;

    vk::QueryPool queryPool;
    std::vector<uint64_t> queryData;
    std::vector< std::pair<vk::Buffer,VmaAllocation> > _toFree;

    //TransferBuffer staging2;
    std::queue<TransferBuffer> stagingBuffers;

    //======================
    vk::Sampler samplerNearest1Mip;
    vk::Sampler samplerLinear1Mip;

    vkff::FrameGraph * framegraph = nullptr;

    void freeBuffer(vk::Buffer b, VmaAllocation a)
    {
        vmaDestroyBuffer(allocator, b, a);
    }
    void freeBuffer( std::pair<vk::Buffer,VmaAllocation> ba )
    {
        freeBuffer( ba.first, ba.second);
    }
    void freeImage(vk::Image i, VmaAllocation a)
    {
        vmaDestroyImage(allocator, i, a);
    }

    static uint32_t calcMipmaps(uint32_t width, uint32_t height)
    {
        auto l = std::min(width, height);
        return static_cast<uint32_t>( std::log2( static_cast<double>(l)) );

    }
    std::pair<vk::Image,VmaAllocation> allocateImage( vk::Extent3D extent,
                                                      vk::Format format,
                                                      uint32_t arrayLayers,
                                                      uint32_t miplevels
                                                         )
    {
        EASY_FUNCTION(profiler::colors::Magenta);

        vk::ImageCreateInfo imageInfo;

        imageInfo.imageType     = extent.depth==1 ? vk::ImageType::e2D : vk::ImageType::e3D;
        imageInfo.format        = format;
        imageInfo.extent        = extent;
        imageInfo.mipLevels     = miplevels;
        imageInfo.arrayLayers   = arrayLayers;
        imageInfo.samples       = vk::SampleCountFlagBits::e1;
        imageInfo.tiling        = vk::ImageTiling::eOptimal;
        imageInfo.usage         = vk::ImageUsageFlagBits::eSampled | vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst;
        imageInfo.sharingMode   = vk::SharingMode::eExclusive;
        imageInfo.initialLayout = vk::ImageLayout::eUndefined;

        if( arrayLayers == 6)
            imageInfo.flags |=  vk::ImageCreateFlagBits::eCubeCompatible;

        VmaAllocationCreateInfo allocCInfo = {};
        allocCInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

        VkImage image;
        VmaAllocation allocation;
        VmaAllocationInfo allocInfo;

        VkImageCreateInfo & imageInfo_c = imageInfo;
        vmaCreateImage(allocator,  &imageInfo_c,  &allocCInfo, &image, &allocation, &allocInfo);

        return { vk::Image(image), allocation};
    }


    std::pair<vk::Buffer,VmaAllocation> allocateBuffer(vk::DeviceSize size,
                                               vk::BufferUsageFlags usage,
                                               VmaMemoryUsage memUsage)
    {
        std::lock_guard<std::mutex> L(m_mutex);

        VkBufferCreateInfo bufferInfo = {};
        bufferInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        bufferInfo.size  = size;
        bufferInfo.usage = static_cast<decltype(bufferInfo.usage)>(usage);

        VmaAllocationCreateInfo allocCInfo = {};
        allocCInfo.usage = memUsage;
        allocCInfo.flags = VMA_ALLOCATION_CREATE_MAPPED_BIT; // can always set this bit,
                                                            // vma will not allow device local
                                                            // memory to be mapped

        VkBuffer buffer;
        VmaAllocation allocation=nullptr;
        VmaAllocationInfo allocInfo;

        auto result = vmaCreateBuffer(allocator, &bufferInfo, &allocCInfo, &buffer, &allocation, &allocInfo);

        if( result != VK_SUCCESS)
            vk::throwResultException( vk::Result(result), "Error allocating VMA Buffer");

        return { vk::Buffer(buffer), allocation};
    }




    MultiStorageBuffer allocateMultiStorageBuffer(size_t byteSize)
    {
        MultiStorageBuffer B;
        vk::DeviceSize size = byteSize;

        auto buff = allocateBuffer(size, vk::BufferUsageFlagBits::eStorageBuffer, VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_TO_GPU);
        void *mappedMem = nullptr;

        vmaMapMemory(allocator, buff.second, &mappedMem);
        assert(mappedMem);

        B.init( buff, mappedMem, byteSize );

        return B;
    }

    template<typename T>
    StorageBuffer<T> allocateStorageBuffer(size_t byteSize)
    {
        StorageBuffer<T> B;
        vk::DeviceSize size = byteSize;

        B.m_capacity = byteSize / sizeof(T);
        B.m_byteSize = byteSize;

        B.m_buffer = allocateBuffer(size, vk::BufferUsageFlagBits::eStorageBuffer, VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_TO_GPU);

        void *mappedMem = nullptr;
        vmaMapMemory(allocator, B.m_buffer.second, &mappedMem);

        assert(mappedMem);
        B.memMap = static_cast<T*>(mappedMem);

        return B;
    }

    template<typename T>
    UniformBuffer<T> allocateUniformBuffer()
    {
        UniformBuffer<T> B;
        vk::DeviceSize size = sizeof(T);

        B.m_buffer = allocateBuffer(size, vk::BufferUsageFlagBits::eUniformBuffer, VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_TO_GPU);

        void *mappedMem = nullptr;
        vmaMapMemory(allocator, B.m_buffer.second, &mappedMem);

        assert(mappedMem);
        B.memMap = static_cast<T*>(mappedMem);
        B.m_byteSize = size;
        return B;
    }


    TransferBuffer allocateTransferBuffer(size_t byteSize)
    {
        TransferBuffer B;
        vk::DeviceSize size = byteSize;

        B.m_capacity = byteSize;
        B.m_byteSize = byteSize;

        B.m_buffer = allocateBuffer(size, vk::BufferUsageFlagBits::eTransferSrc, VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_ONLY);

        void *mappedMem = nullptr;
        vmaMapMemory(allocator, B.m_buffer.second, &mappedMem);

        assert(mappedMem);
        B.memMap = static_cast<uint8_t*>(mappedMem);

        return B;
    }

    void nextTransferBuffer()
    {
        if( stagingBuffers.size() )
        {
            TransferBuffer f = stagingBuffers.front();
            stagingBuffers.pop();
            f.clear();
            stagingBuffers.push(f);
        }
    }
    size_t transferBufferAvailable()
    {
        return stagingBuffers.front().available();
    }
    StagingBufferData reserveTransfer(uint32_t byteSize)
    {
        return stagingBuffers.front().reserve(byteSize);
    }

    // texture for the BRDF look-up texture
    TextureCube_ID m_testCube;

    std::mutex m_mutex;

};

}
}

#endif
