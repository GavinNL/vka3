#ifndef VKA_ECS_RENDERSYSTEM_PIPELINEMANAGEMENTSYSTEM_H
#define VKA_ECS_RENDERSYSTEM_PIPELINEMANAGEMENTSYSTEM_H

#include <fstream>
#include "RenderSystem3_Resources.h"
#include "device_objects.h"

namespace vka {

namespace ecs
{

class RenderSystem3;

struct RenderSystemPipelineManagementSystem : public SystemBaseInternal
{
    RenderSystem3Resources * m_resources = nullptr;
    RenderSystem3          * m_renderSystem = nullptr;
    entt::observer m_hostVisibleUpdated;

    void onStart() override;
    void onStop() override;
    void onUpdate() override;


private:
};

}

}

#endif
