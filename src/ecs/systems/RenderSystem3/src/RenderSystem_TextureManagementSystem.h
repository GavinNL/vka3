#ifndef VKA_ECS_RENDERSYSTEM_TEXTUREMANAGEMENTSYSTEM_H
#define VKA_ECS_RENDERSYSTEM_TEXTUREMANAGEMENTSYSTEM_H

#include "RenderSystem3_Resources.h"
#include "device_objects.h"

namespace vka {

namespace ecs
{

class RenderSystem3;


struct RenderSystemTextureManagementSystem : public SystemBaseInternal
{
    RenderSystem3Resources * m_resources = nullptr;
    vk::Device m_device = {};

    void onStart() override;
    void onStop() override;

    void onUpdate() override;


    void writerCommandBuffer(vk::CommandBuffer m_commandBuffer);

    void _copyImage(vk::CommandBuffer m_commandBuffer,
                    vk::Buffer buffer,
                    uint32_t bufferOffset,
                    uint32_t bufferImageWidth,
                    uint32_t bufferImageHeight,
                    vk::Image image,
                    vk::Extent2D imageExtent,
                    vk::Format imageFormat,
                    uint32_t layer,
                    uint32_t mip);

private:
    std::mutex m_mutex;

    static DeviceTexture _createDeviceTexture( HostImageArray const & hP, uint32_t mipLevels, vk::ImageViewType viewType, vk::Device m_device, RenderSystem3Resources *m_resources);

    template<typename HostImageType>
    void _unloadTextures()
    {
        auto manager_p = getSystemBus()->getResourceManager_p< HostImageType >();
        auto llll = manager_p->acquireLock();

        auto sb = getSystemBus();
        sb->forEachResourceToDeviceUnload<HostImageType,DeviceTexture>(
        [this](DeviceTexture & D)
        {
            if( D.view )
            {
                m_device.destroyImageView(D.view);
                D.view = vk::ImageView();
            }
            if( D.image )
            {
                m_resources->freeImage( D.image, D.allocation );
                D.image      = vk::Image();
                D.allocation = nullptr;
            }
        });

        // no need to do anything extra for
        // unloading HostTextures since they do not
        // reference other resources
        getSystemBus()->forEachResourceToHostUnload<HostImageType>( [sb](HostImageType & P)
        {
            (void)P;
        });
    }

    void _performCopies(vk::CommandBuffer cmd);

    struct ToTransfer_t
    {
        entity_type e;
        vk::ImageViewType viewType;
        uint32_t layer=0;
        uint32_t mip=0;
    };

    std::list<ToTransfer_t> m_imagesToTransfer;

    template<typename TextureType>
    void _allocateDeviceTextures(vk::ImageViewType viewType)
    {
        auto manager_p = getSystemBus()->getResourceManager_p< TextureType >();
        auto llll = manager_p->acquireLock();

        auto & Mp = getSystemBus()->getResourceManager< TextureType >();

        for(auto e : Mp.template view<tag_resource_host_fully_loaded, tag_resource_device_load, TextureType>( entt::exclude<DeviceTexture>))
        {
            auto & Hp = Mp.template get<TextureType>(e);
            assert(m_device);

            uint32_t width  = Hp.getWidth();
            uint32_t height = Hp.getHeight();

            uint32_t mipLevels  = static_cast<uint32_t>( std::log2( static_cast<double>(std::min( width, height )) ) );

            if constexpr ( std::is_same<TextureType, HostTextureCube>::value )
            {
                mipLevels = Hp.getLevelCount();
            }

            // Create the device texture object
            // all layers/mips should be flagged as
            // dirty
            auto D = _createDeviceTexture(Hp, mipLevels, viewType ,m_device, m_resources );

            // loop through all the layers/mips that have been flagged
            // as dirty. it should be all of them.
            // and place them into a queue
            auto id = Mp.getID(e);
            for(auto & lm : D.m_dirty)
            {
                ToTransfer_t T;

                T.e = getSystemBus()->getResourceManager_p<TextureType>()->entity(id);
                T.layer = std::get<0>(lm.first);
                T.mip   = std::get<1>(lm.first);
                T.viewType = vk::ImageViewType::e2D;
                if constexpr ( std::is_same<TextureType, HostTextureCube>::value )
                {
                    T.viewType = vk::ImageViewType::eCube;
                }
                m_imagesToTransfer.push_back(T);
            }

            Mp.registry().template emplace<DeviceTexture>( e, std::move(D) );
        }
    }
};


}

}

#endif
