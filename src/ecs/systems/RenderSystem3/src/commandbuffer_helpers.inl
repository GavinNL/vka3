#ifndef VKA_ECS_RENDERSYSTEM3_COMMANDBUFFERHELPER_H
#define VKA_ECS_RENDERSYSTEM3_COMMANDBUFFERHELPER_H

#include <set>
#include <vector>
#include <vulkan/vulkan.hpp>
#include <vka/utils/VulkanHelperFunctions.h>

namespace vka
{
namespace ecs
{

struct BufferSubImageCopyToImage
{
    vk::Extent2D  srcBufferImageExtent;      // the full width/height of the image in the buffer

    vk::Rect2D    srcBufferRegion;           // the region within the srcBuffer image to copy from

    vk::Offset2D  dstImageOffset;            // the offset in the vk::image to copy the src to
    uint32_t      layer;                     // the layer to copy the src to
    uint32_t      mip;                       // the mip level to copy the src to
};

inline
void setImageLayout( vk::CommandBuffer cmd,
                     vk::Image img,
                     vk::ImageSubresourceRange const & range,
                     vk::ImageLayout _oldLayout,
                     vk::ImageLayout _newLayout,
                     vk::AccessFlags srcAccess,
                     vk::AccessFlags dstAccess,
                     vk::PipelineStageFlags srcStageFlags,
                     vk::PipelineStageFlags dstStageFlags)
{
    vk::ImageMemoryBarrier imageBarrier;

    imageBarrier.oldLayout = _oldLayout;
    imageBarrier.newLayout = _newLayout;
    imageBarrier.image     = img;
    imageBarrier.subresourceRange = range;

    imageBarrier.srcAccessMask = srcAccess;
    imageBarrier.dstAccessMask = dstAccess;

    cmd.pipelineBarrier( srcStageFlags, dstStageFlags, vk::DependencyFlags(), nullptr,nullptr,imageBarrier);
}

inline
void copyBufferToImageAndConvert(vk::CommandBuffer cmd,
                                 vk::Buffer srcBuffer,
                                 vk::DeviceSize bufferOffset,
                                 vk::Image  dstImage,
                                 vk::Extent2D imageExtent,
                                 vk::Format format,
                                 vk::ArrayProxy<BufferSubImageCopyToImage const > subImageCopies)
{
    //auto & info  = GLOBAL.imageViewsContainer.info(dstImageView);
    auto img     = dstImage;
    //auto & infoI = GLOBAL.imagesContainer.info(img);
    //auto format  = infoI.createInfo.format;
    auto formatByteSize = static_cast<uint32_t>(vka::formatSize(format));
    // Size of the image
    uint32_t imgWidth  = imageExtent.width;
    uint32_t imgHeight = imageExtent.height;


    // figure out which layers/mips need to be converted into
    // TransferDstOptimal
    std::set< std::pair<uint32_t, uint32_t> > layerAndMip;
    for(auto & s : subImageCopies)
    {
        layerAndMip.insert( std::make_pair(s.layer, s.mip) );
    }

    //===============================================================================
    // Convert all the layers/mip levels into a layout that allows transfering to
    //===============================================================================
    for(auto & s : layerAndMip)
    {
        vk::ImageSubresourceRange mipSubRange;// = {};
        mipSubRange.aspectMask   = vk::ImageAspectFlagBits::eColor;//
        mipSubRange.baseMipLevel = s.second;
        mipSubRange.levelCount   = 1;

        mipSubRange.baseArrayLayer = s.first;
        mipSubRange.layerCount     = 1;

        // convert the image layer into TransferDstOptimal
        setImageLayout(cmd,
                    img, mipSubRange,
                    vk::ImageLayout::eUndefined,
                    vk::ImageLayout::eTransferDstOptimal,
                    vk::AccessFlags(),
                    vk::AccessFlagBits::eTransferWrite,
                    vk::PipelineStageFlagBits::eHost,
                    vk::PipelineStageFlagBits::eTransfer);
    }
    //===============================================================================



    std::vector<vk::BufferImageCopy> BICv;
    BICv.reserve(subImageCopies.size());
    for(auto & s : subImageCopies)
    {
        // Want to Copy this sub image which is contained in the buffer
        vk::Offset3D srcBufferSubImageOffset(  s.srcBufferRegion.offset.x,      s.srcBufferRegion.offset.y,       0);
       // vk::Extent3D srcBufferSubImageExtent(  s.srcBufferImageExtent.width,  s.srcBufferImageExtent.height,  1);

        auto & BIC = BICv.emplace_back();

        uint32_t byteOffsetOfFirstPixelInSubBufferImage = static_cast<uint32_t>(bufferOffset) + ( static_cast<uint32_t>( srcBufferSubImageOffset.x) + static_cast<uint32_t>(srcBufferSubImageOffset.y) * s.srcBufferImageExtent.width ) * formatByteSize;
        (void)byteOffsetOfFirstPixelInSubBufferImage;

        if( vka::formatIsCompressedBC(format) )
        {
            throw std::runtime_error("Cannot do compressed images right now");
            imgWidth=0;
            imgHeight=0;
            (void)imgWidth;
            (void)imgHeight;
        }

        //  https://arm-software.github.io/vulkan-sdk/mipmapping.html

        BIC.bufferOffset      = byteOffsetOfFirstPixelInSubBufferImage;
        BIC.bufferRowLength   = s.srcBufferImageExtent.width;
        BIC.bufferImageHeight = s.srcBufferImageExtent.height;
        BIC.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
        BIC.imageSubresource.mipLevel = s.mip;
        BIC.imageSubresource.baseArrayLayer = s.layer;
        BIC.imageSubresource.layerCount = 1;
        BIC.imageOffset        = vk::Offset3D(s.dstImageOffset.x, s.dstImageOffset.y, 0);
        BIC.imageExtent.width  = s.srcBufferRegion.extent.width;
        BIC.imageExtent.height = s.srcBufferRegion.extent.height;
        BIC.imageExtent.depth = 1;
    }

    cmd.copyBufferToImage(
                srcBuffer,
                img,
                vk::ImageLayout::eTransferDstOptimal,
                BICv
                );


    //==============================================================
    // Convert back into shaderReadOnlyOptimal
    //==============================================================
    for(auto & s : layerAndMip)
    {
        vk::ImageSubresourceRange mipSubRange;// = {};
        mipSubRange.aspectMask   = vk::ImageAspectFlagBits::eColor;//
        mipSubRange.baseMipLevel = s.second;
        mipSubRange.levelCount   = 1;

        mipSubRange.baseArrayLayer = s.first;
        mipSubRange.layerCount     = 1;

        // convert the image layer into TransferDstOptimal
        setImageLayout( cmd,
                    img, mipSubRange,
                    vk::ImageLayout::eTransferDstOptimal,
                    vk::ImageLayout::eShaderReadOnlyOptimal,
                    vk::AccessFlagBits::eTransferWrite,
                    vk::AccessFlagBits::eShaderRead,
                    vk::PipelineStageFlagBits::eTransfer,
                    vk::PipelineStageFlagBits::eFragmentShader);
    }
    //==============================================================
}



/**
 * @brief generateMipMaps
 * @param cmd
 * @param img
 * @param extent
 * @param layer
 * @param initialMipLevel
 * @param mipLevelsToGenerate
 *
 * Generate the mipmap levels from
 * initialMipLevel to initialMipLevel+mipLevelsToGenerate
 *
 * initialMipLevel-1 needs to be in shaderReadOnlyOptimal mode
 */
inline
void generateMipMaps( vk::CommandBuffer cmd,
                      vk::Image img,
                      vk::Extent2D imageExtent,
                      uint32_t layer,
                      vk::ImageLayout sourceMipLevelLayout,
                      uint32_t firstMipLevelToGenerate,
                      uint32_t mipLevelsToGenerate,
                      vk::ImageLayout mipLevelsCurrentLayout,
                      vk::ImageLayout finalMipLevelsLayout)
{
    vk::ImageSubresourceRange range;
    range.setAspectMask( vk::ImageAspectFlagBits::eColor);
    range.layerCount     = 1;
    range.baseArrayLayer = layer;
    range.baseMipLevel   = firstMipLevelToGenerate-1;
    range.levelCount     = 1;

    auto width  = imageExtent.width;
    auto height = imageExtent.height;
    //auto depth  = extent.depth;


    // Convert the source MipMap level to TransferSrc
    // so that it can be copied to the next level.
    setImageLayout( cmd,
                img, range,
                sourceMipLevelLayout,
                vk::ImageLayout::eTransferSrcOptimal,
                vk::AccessFlagBits::eShaderRead,
                vk::AccessFlagBits::eTransferRead,
                vk::PipelineStageFlagBits::eAllCommands,
                vk::PipelineStageFlagBits::eAllCommands);

    auto mipLevels = mipLevelsToGenerate;

    for (uint32_t i = 0; i < mipLevels; i++)
    {
        vk::ImageBlit imageBlit;

        uint32_t levelDst = firstMipLevelToGenerate+i;
        uint32_t levelSrc = levelDst-1;

        // Source
        imageBlit.srcSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;//VK_IMAGE_ASPECT_COLOR_BIT;
        imageBlit.srcSubresource.layerCount = 1;
        imageBlit.srcSubresource.baseArrayLayer = layer;
        imageBlit.srcSubresource.mipLevel = levelSrc;
        imageBlit.srcOffsets[1].x = int32_t( width  >> (levelSrc));
        imageBlit.srcOffsets[1].y = int32_t( height >> (levelSrc));
        imageBlit.srcOffsets[1].z = 1;

        // Destination
        imageBlit.dstSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
        imageBlit.dstSubresource.layerCount = 1;
        imageBlit.dstSubresource.mipLevel = levelDst;
        imageBlit.dstSubresource.baseArrayLayer = layer;
        imageBlit.dstOffsets[1].x = int32_t(width  >> levelDst);
        imageBlit.dstOffsets[1].y = int32_t(height >> levelDst);
        imageBlit.dstOffsets[1].z = 1;


        // Transiton destination mip level to transferDst so
        // that it can be copied to from the previous
        // level
        vk::ImageSubresourceRange mipSubRange;// = {};
        mipSubRange.aspectMask   = vk::ImageAspectFlagBits::eColor;//
        mipSubRange.baseMipLevel = levelDst;
        mipSubRange.levelCount   = 1;
        mipSubRange.layerCount   = 1;
        mipSubRange.baseArrayLayer = layer;

        setImageLayout( cmd,
                    img, mipSubRange,
                    mipLevelsCurrentLayout,  //vk::ImageLayout::eUndefined,
                    vk::ImageLayout::eTransferDstOptimal,
                    vk::AccessFlags(),
                    vk::AccessFlagBits::eTransferWrite,
                    vk::PipelineStageFlagBits::eAllCommands,
                    vk::PipelineStageFlagBits::eAllCommands);


        // Blit from previous level
        cmd.
        blitImage( img, vk::ImageLayout::eTransferSrcOptimal,
                   img, vk::ImageLayout::eTransferDstOptimal,
                   imageBlit, vk::Filter::eLinear);

        // convert the dest level into TransferSrcOptimal
        // so that on the next run, it's data
        // can be copied to the next level
        setImageLayout(cmd,
                    img, mipSubRange,
                    vk::ImageLayout::eTransferDstOptimal,
                    vk::ImageLayout::eTransferSrcOptimal,
                    vk::AccessFlags(),
                    vk::AccessFlagBits::eTransferWrite,
                    vk::PipelineStageFlagBits::eHost,
                    vk::PipelineStageFlagBits::eTransfer);

    }

    // at this stage, all mip level sfrom
    // initialMipLevel to initialMipLevel+mipLevels
    // should be in TransferSrcOptimal
    // we should now convert them all into shaderReadOnlyOptimal
    range.baseMipLevel = firstMipLevelToGenerate-1;
    range.levelCount   = mipLevels+1;
    setImageLayout( cmd,
                img, range,
                vk::ImageLayout::eTransferSrcOptimal,
                finalMipLevelsLayout,
                vk::AccessFlagBits::eHostWrite | vk::AccessFlagBits::eTransferWrite,
                vk::AccessFlagBits::eShaderRead,
                vk::PipelineStageFlagBits::eAllCommands,
                vk::PipelineStageFlagBits::eAllCommands);

}


inline void fullBarrier(vk::CommandBuffer cmd)
{
    vk::MemoryBarrier memoryBarrier;
    memoryBarrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite; //vk::AccessFlags::eTra  eT VK_ACCESS_TRANSFER_WRITE_BIT;
    memoryBarrier.dstAccessMask = vk::AccessFlagBits::eVertexAttributeRead ;//vk::Access:e VK_ACCESS_VERTEX_ATTRIBUTE_READ_BIT;

    memoryBarrier.srcAccessMask =
    memoryBarrier.dstAccessMask =
    vk::AccessFlagBits::eIndirectCommandRead
    | vk::AccessFlagBits::eIndexRead
    | vk::AccessFlagBits::eVertexAttributeRead
    | vk::AccessFlagBits::eUniformRead
    | vk::AccessFlagBits::eInputAttachmentRead
    | vk::AccessFlagBits::eShaderRead
    | vk::AccessFlagBits::eShaderWrite
    | vk::AccessFlagBits::eColorAttachmentRead
    | vk::AccessFlagBits::eColorAttachmentWrite
    | vk::AccessFlagBits::eDepthStencilAttachmentRead
    | vk::AccessFlagBits::eDepthStencilAttachmentWrite
    | vk::AccessFlagBits::eTransferRead
    | vk::AccessFlagBits::eTransferWrite
    | vk::AccessFlagBits::eHostRead
    | vk::AccessFlagBits::eHostWrite
    | vk::AccessFlagBits::eMemoryRead
    | vk::AccessFlagBits::eMemoryWrite
    | vk::AccessFlagBits::eTransformFeedbackWriteEXT
    | vk::AccessFlagBits::eTransformFeedbackCounterReadEXT
    | vk::AccessFlagBits::eTransformFeedbackCounterWriteEXT
    | vk::AccessFlagBits::eConditionalRenderingReadEXT
    | vk::AccessFlagBits::eCommandProcessReadNVX
    | vk::AccessFlagBits::eCommandProcessWriteNVX
    | vk::AccessFlagBits::eColorAttachmentReadNoncoherentEXT
    | vk::AccessFlagBits::eShadingRateImageReadNV
    | vk::AccessFlagBits::eAccelerationStructureReadNV
    | vk::AccessFlagBits::eAccelerationStructureWriteNV
    | vk::AccessFlagBits::eFragmentDensityMapReadEXT;

    cmd.pipelineBarrier( vk::PipelineStageFlagBits::eAllCommands,
                     vk::PipelineStageFlagBits::eAllCommands,
                     vk::DependencyFlags(),
                     memoryBarrier,
                     nullptr,
                     nullptr);
}


inline void waitForImage(vk::CommandBuffer cmd, vk::Image img)
{
    vk::ImageMemoryBarrier imageBarrier;

    vk::ImageSubresourceRange range;// = {};
    range.aspectMask   = vk::ImageAspectFlagBits::eColor;//
    range.baseMipLevel = 0;
    range.levelCount   = 1;
    range.baseArrayLayer = 0;
    range.layerCount     = 1;



    imageBarrier.oldLayout        = vk::ImageLayout::eColorAttachmentOptimal;
    imageBarrier.newLayout        = vk::ImageLayout::eShaderReadOnlyOptimal;
    imageBarrier.image            = img;
    imageBarrier.subresourceRange = range;

    imageBarrier.srcAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
    imageBarrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;


    cmd.pipelineBarrier( vk::PipelineStageFlagBits::eColorAttachmentOutput, vk::PipelineStageFlagBits::eFragmentShader, vk::DependencyFlags(), nullptr,nullptr,imageBarrier);

}


}
}
#endif
