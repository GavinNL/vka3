#ifndef VKA_ECS_RENDERSYSTEM3_DEVICE_OBJECTS_H
#define VKA_ECS_RENDERSYSTEM3_DEVICE_OBJECTS_H

#include <vulkan/vulkan.hpp>
#include "vk_mem_alloc.h"
#include <vka/utils/HostImage.h>
#include <vka/ecs/ResourceObjects/HostPrimitive.h>
#include <vka/ecs/ResourceObjects/PBRMaterial.h>
#include <vka/ecs/ResourceObjects/Environment.h>
#include <chrono>


namespace vka {
namespace ecs {

struct ResourceTimeStamp
{
    std::chrono::system_clock::time_point lastUsed = std::chrono::system_clock::now();

    void touch()
    {
        lastUsed = std::chrono::system_clock::now();
    }
};

//======================================

struct DeviceTexture
{
    vk::Image       image;
    vk::ImageView   view;
    vk::Sampler     sampler;
    vk::Sampler     samplerNearest;
    VmaAllocation   allocation=nullptr;
    vk::DeviceSize  byteSize=0;
    vk::Format      format;
    vk::Extent2D    extent;
    uint32_t        arrayLayers=1;
    uint32_t        mipmaps=1;

    //bool clean =false;
    //std::vector< std::vector<bool> > dirty; // which layer/mipmap is dirty

    // layer,mip
    std::map< std::tuple<uint32_t, uint32_t>, bool > m_dirty;

    bool isDirty(uint32_t layer, uint32_t mip) const
    {
        return m_dirty.at( std::make_tuple(layer, mip));
    }
    bool isClean(uint32_t layer, uint32_t mip) const
    {
        return !isDirty(layer,mip);
    }
    void setClean(uint32_t layer, uint32_t mip)
    {
        m_dirty.at( std::make_tuple(layer, mip)) = false;
    }
    void setDirty(uint32_t layer, uint32_t mip)
    {
        m_dirty.at( std::make_tuple(layer, mip)) = true;
    }

    bool isDirty() const
    {
        bool areAnyDirty =
        std::any_of( m_dirty.begin(), m_dirty.end(),
                            [](auto & v)
        {
            return v.second == true;
        });
        return areAnyDirty;
    }

    void setAllDirty()
    {
        m_dirty.clear();
        for(uint32_t l=0;l<arrayLayers;l++)
        {
            for(uint32_t m=0;m<mipmaps;m++)
            {
                m_dirty[ std::make_tuple(l,m) ] = true;
            }
        }
    }
    void setAllClean()
    {
        m_dirty.clear();
        for(uint32_t l=0;l<arrayLayers;l++)
        {
            for(uint32_t m=0;m<mipmaps;m++)
            {
                m_dirty[ std::make_tuple(l,m) ] = false;
            }
        }
    }

};


struct StagingImageData
{
    vk::Buffer     buffer=vk::Buffer();
    struct ttt
    {
        uint32_t bufferOffset = 0;
        uint32_t byteSize     = 0;
        uint32_t mipLevel     = 0;
        uint32_t layer        = 0;
        uint32_t bufferImageWidth = 0;
        uint32_t bufferImageHeight = 0;
        void  * mappedData = nullptr;
    };

    std::vector<ttt> imageLocations;

};

struct DevicePrimitive
{
    vk::Buffer                     buffer;
    VmaAllocation                  allocation=nullptr;
    bool                           hostVisible=false;
    vk::DeviceSize                 byteSize=0;
    int32_t                        attributeFlags=0;
    PrimitiveDrawCall              drawCall;
    std::optional<vk::IndexType>   indexType;
    void *                         mappedData=nullptr;

    // these are for binding, buffers[..] should be same as bufer
    std::array< vk::DeviceSize, 9> attributeOffsets;
    std::array< vk::Buffer    , 9> buffers;
};


}
}

#endif
