#ifndef VKA_ECS_RENDERSYSTEM_ARRAYOFTEXTURES_H
#define VKA_ECS_RENDERSYSTEM_ARRAYOFTEXTURES_H

#include "RenderSystem3_Resources.h"
#include "device_objects.h"

#include <vkb/utils/TextureArrayDescriptorSet.h>

namespace vka {

namespace ecs
{

class RenderSystem3;

/**
 * @brief The RenderSystemArrayOfTextureSystem struct
 *
 * This system's sole responsibility is to keep track
 * of a ArrayOfTextures descriptor set queue for
 * keeping ALL gpu textures bound at once.
 *
 * As GPU textures get created, they will be placed
 * somewhere in the ArrayOfTextures.
 *
 * When the GPU texture is destroyed, its imageview
 * will be removed from the ArrayOfTextures
 */
struct RenderSystemArrayOfTextureSystem : public SystemBaseInternal
{
    vk::Device                m_device;
    RenderSystem3Resources * m_resources = nullptr;

    static constexpr uint32_t MAX_TEXTURES_BOUND = 64;
    static constexpr uint32_t MAX_TEXTURES_CUBES_BOUND = 16;

    vkb::TextureArrayDescriptorSetChain m_textureArrayChain;

    Texture2D_ID   m_defaultTexture;
    TextureCube_ID m_defaultCubeMap;

    std::shared_ptr< ResourceManager<HostTexture2D> >   m_texManager;
    std::shared_ptr< ResourceManager<HostTextureCube> > m_cubeManager;

    std::map<Texture2D_ID   , uint64_t> m_lastFoundT;
    std::map<TextureCube_ID, uint64_t>  m_lastFoundC;

    std::set<Texture2D_ID> m_inactiveTextures;
    std::set<TextureCube_ID> m_inactiveCubes;

    uint64_t m_frameNumber=0;
public:
    uint32_t getMaxTextures() const;
    uint32_t getMaxTextureCubes() const;
    vk::DescriptorSetLayout getLayout() const;

    int32_t findTexture( Texture2D_ID v);

    int32_t findTextureCube( TextureCube_ID v);

    // placeholder function
    // checks all the textures that have been added to the
    // map and see how long it has been since the findTexture or
    // findTextureCube was called with that id
    // if it is greater than some value, schedule it to unload
    void checkUnload();


    void bindTextureArray(vk::CommandBuffer cmd, vk::PipelineLayout layout, uint32_t set);

    void onStart();

    void onStop();

    void onUpdate();

    void releaseResources();

    void _onHostTexture2DEvent( EvtResourceEvent_t<HostTexture2D> & E);

    void _onHostCubeEvent( EvtResourceEvent_t<HostTextureCube> & E);


    void initilizeTextureArraySet();

    int32_t _removeTexture( vk::ImageView v, uint32_t binding);
    int32_t _insertTexture( vk::ImageView v, uint32_t binding);
    bool m_textureArrayInitialized = false;
    std::set< _ID<HostTexture2D> > m_texturesToAdd;
    std::set< _ID<HostTextureCube> > m_cubersToAdd;
};



}

}

#endif
