#include <vka/ecs/RenderSystem3.h>

#include <vka/ecs/Components/Tags.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CCamera.h>
#include <vka/ecs/Components/CAnimator2.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CLight.h>

#include <vkff/framegraph.h>

#include <vkb/utils/TextureArrayDescriptorSet.h>

#include <easy/profiler.h>
#include <easy/arbitrary_value.h> // EASY_VALUE, EASY_ARRAY are defined here

#include <glm/gtx/io.hpp>


#include "RenderSystem3_Resources.h"
#include "MeshRenderSystem.h"
#include "device_objects.h"
#include "commandbuffer_helpers.inl"

#include "RenderSystem_TextureManagementSystem.h"

namespace vka
{

namespace ecs
{

static uint64_t _frameCount=0;

void RenderSystemTextureManagementSystem::onStart()
{
    {
        auto & rM = getResourceManager<HostTexture2D>().registry();
        rM.on_update<HostTexture2D>().connect <&entt::registry::emplace_or_replace<tag_resource_host_updated> >();
    }

    {
        auto & rM = getResourceManager<HostTextureCube>().registry();
        rM.on_update<HostTextureCube>().connect <&entt::registry::emplace_or_replace<tag_resource_host_updated> >();
    }
}

void RenderSystemTextureManagementSystem::onStop()
{
    {
        auto & rM = getResourceManager<HostTexture2D>().registry();
        rM.on_update<HostTexture2D>().disconnect <&entt::registry::emplace_or_replace<tag_resource_host_updated> >();
    }
    {
        auto & rM = getResourceManager<HostTextureCube>().registry();
        rM.on_update<HostTextureCube>().disconnect <&entt::registry::emplace_or_replace<tag_resource_host_updated> >();
    }
}

void RenderSystemTextureManagementSystem::onUpdate()
{
    EASY_BLOCK("TextureManagement");
    assert(m_resources);
    if(!m_device) return;

    // this system should loop through all the
    // entities that are flaged to be rendered
    //  1. check if their primitive is loaded
    //     if it is not, flag this object as cannot be drawn
    //  2. check if the material is loaded
    //     if it is, place it in the vector of staged materials
    //     this vector is what is copied to the storage buffer
    //     - If the material is not loaded, either flag the object as
    //       cannot be drawn, or create a blank material for it.
    _unloadTextures<HostTextureCube>();
    _unloadTextures<HostTexture2D>();


    // loop through all the staged primitives
    // that do not have a DevicePrimitive and Do not have a StagedData
    // allocate a Staged Data struct for it
    //
    // This needs to be done after the previous two for-loops are completed

    std::vector< std::future<int> > futures;

    //_allocateDeviceTextures<HostTexture2D>(vk::ImageViewType::e2D);
    //_allocateDeviceTextures<HostTextureCube>(vk::ImageViewType::eCube);
    //_scheduleStagingCopies<HostTexture2D>(futures);
    //_scheduleStagingCopies<HostTextureCube>(futures);


    EASY_BLOCK("TextureManagement:waiting");
    for(auto & x : futures)
        x.get();
    EASY_END_BLOCK
            //===============================================================================================
            EASY_END_BLOCK
}

void RenderSystemTextureManagementSystem::writerCommandBuffer(vk::CommandBuffer m_commandBuffer)
{
    _allocateDeviceTextures<HostTexture2D>(vk::ImageViewType::e2D);
    _allocateDeviceTextures<HostTextureCube>(vk::ImageViewType::eCube);

    _performCopies(m_commandBuffer);
}

void RenderSystemTextureManagementSystem::_copyImage(vk::CommandBuffer m_commandBuffer, vk::Buffer buffer, uint32_t bufferOffset, uint32_t bufferImageWidth, uint32_t bufferImageHeight, vk::Image image, vk::Extent2D imageExtent, vk::Format imageFormat, uint32_t layer, uint32_t mip)
{
    vka::ecs::BufferSubImageCopyToImage bic;
    bic.srcBufferRegion.offset = vk::Offset2D(0,0);
    bic.srcBufferRegion.extent = vk::Extent2D(bufferImageWidth,bufferImageHeight);
    bic.srcBufferImageExtent   = vk::Extent2D(bufferImageWidth,bufferImageHeight);
    bic.dstImageOffset         = vk::Offset2D(0,0);
    bic.layer                  = layer;
    bic.mip                    = mip;

    // copy the image to the first mipmap level
    // and convert that level to shaderReadOnlyOptimal
    copyBufferToImageAndConvert( m_commandBuffer,
                                 buffer,
                                 bufferOffset,
                                 image,
                                 imageExtent,
                                 imageFormat,
                                 bic);

    // S_INFO("Transferring Image {}.{} to Device Memory: {}", layer, mip, Host.getResourceName(e)  );
}

DeviceTexture RenderSystemTextureManagementSystem::_createDeviceTexture(const HostImageArray &hP, uint32_t mipLevels, vk::ImageViewType viewType, vk::Device m_device, RenderSystem3Resources *m_resources)
{
    assert( m_device );

    //  auto byteSize = hP.size();

    auto width = hP.getWidth();
    auto height= hP.getHeight();

    auto format          = vk::Format::eR8G8B8A8Unorm;

    switch( hP.getChannels() )
    {
    case 1:
        format = vk::Format::eR8Unorm; break;
    case 2:
        format = vk::Format::eR8G8Unorm; break;
    case 3:
        format = vk::Format::eR8G8B8Unorm; break;
    case 4:
        format = vk::Format::eR8G8B8A8Unorm; break;
    }


    //uint32_t mipLevels  = static_cast<uint32_t>( std::log2( static_cast<double>(std::min( width, height )) ) );
    //        mipLevels   = std::min( mipLevels, hP.getLevelCount() );

    uint32_t arrayLayers = hP.getLayerCount();
    vk::Extent3D extent = { width, height, 1};

    DeviceTexture dP;

    dP.extent  = vk::Extent2D(hP.getWidth(), hP.getHeight());
    auto & res = *m_resources;

    // allocate the actual image
    auto ii        = res.allocateImage( extent, format, arrayLayers, mipLevels);
    dP.image       = ii.first;
    dP.allocation  = ii.second;
    dP.arrayLayers = arrayLayers;
    dP.mipmaps     = mipLevels;
    dP.format      = format;

    dP.setAllDirty();

    // create the image view
    {
        vk::ImageViewCreateInfo ci;
        ci.image = ii.first;

        ci.viewType = viewType;

        ci.format = format;
        ci.subresourceRange.aspectMask     = vk::ImageAspectFlagBits::eColor;
        switch(ci.format)
        {
        case vk::Format::eD16Unorm:
        case vk::Format::eD32Sfloat:
        case vk::Format::eD16UnormS8Uint:
        case vk::Format::eD24UnormS8Uint:
        case vk::Format::eD32SfloatS8Uint:
            ci.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
            break;
        default:
            ci.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
            break;
        }
        ci.subresourceRange.baseMipLevel   = 0;
        ci.subresourceRange.levelCount     = mipLevels;
        ci.subresourceRange.baseArrayLayer = 0;
        ci.subresourceRange.layerCount     = arrayLayers;

        dP.view = m_device.createImageView(ci);
    }

    // create a sampler
    {
        // Temporary 1-mipmap sampler
        vkb::SamplerCreateInfo2 ci;
        ci.magFilter               =  vk::Filter::eLinear;
        ci.minFilter               =  vk::Filter::eLinear;
        ci.mipmapMode              =  vk::SamplerMipmapMode::eLinear ;
        ci.addressModeU            =  vk::SamplerAddressMode::eRepeat ;
        ci.addressModeV            =  vk::SamplerAddressMode::eRepeat ;
        ci.addressModeW            =  vk::SamplerAddressMode::eRepeat ;
        ci.mipLodBias              =  0.0f  ;
        ci.anisotropyEnable        =  VK_FALSE;
        ci.maxAnisotropy           =  1 ;
        ci.compareEnable           =  VK_FALSE ;
        ci.compareOp               =  vk::CompareOp::eAlways ;
        ci.minLod                  =  0 ;
        ci.maxLod                  =  static_cast<float>(mipLevels) ;
        ci.borderColor             =  vk::BorderColor::eIntOpaqueBlack ;
        ci.unnormalizedCoordinates =  VK_FALSE ;

        ci.magFilter               =  vk::Filter::eLinear;
        ci.minFilter               =  vk::Filter::eLinear;

        dP.sampler = ci.create( res.storage, m_device);


        ci.magFilter               =  vk::Filter::eNearest;
        ci.minFilter               =  vk::Filter::eNearest;
        dP.samplerNearest = ci.create( res.storage, m_device);
    }

    return dP;
}

void RenderSystemTextureManagementSystem::_performCopies(vk::CommandBuffer cmd)
{
    //        auto manager_p = getSystemBus()->getResourceManager_p< TextureType >();
    //        auto llll = manager_p->acquireLock();

    auto & res = *m_resources;

    if( !m_imagesToTransfer.empty() )
    {
        S_INFO(" ====== performCopies: Frame {} ================", _frameCount);
    }
    entity_type e = entt::null;
    // now loop through all the ones that have
    // the DeviceTexture and the HostTextureType
    // this is where we will do the copying from host->staging->device
    auto it = m_imagesToTransfer.begin();
    while(it!=m_imagesToTransfer.end())
    {
        uint32_t byteSize;
        HostImage * Hi = nullptr;
        DeviceTexture * D = nullptr;
        auto layer = it->layer;
        auto mip   = it->mip;
        auto viewType = it->viewType;

        uint32_t totalHostMips = 0;
        uint32_t totalDeviceMips=0;

        Texture2D_ID texId;
        TextureCube_ID cubeId;

        if( viewType == vk::ImageViewType::e2D)
        {
            auto & Mp = getSystemBus()->getResourceManager< HostTexture2D >();
            auto & Hp = Mp.template get<HostTexture2D>(it->e);
            D         = &Mp.template get<DeviceTexture>(it->e);

            if( layer >= Hp.getLayerCount() )
            {
                it = m_imagesToTransfer.erase(it);
                continue;
            }
            if( mip >= Hp.getLevelCount() )
            {
                it = m_imagesToTransfer.erase(it);
                continue;
            }

            totalHostMips = Hp.getLevelCount();
            Hi        = &Hp.getLayer(layer).getLevel(mip);
            texId = Mp.getID(it->e);

            if( e != it->e)
            {
                e = it->e;
                S_INFO("Copying To Device {}", Mp.getResourceName(it->e));
            }
            S_INFO("   Layer: {}, Mip: {}", layer, mip);
        }
        if( viewType == vk::ImageViewType::eCube)
        {
            auto & Mp = getSystemBus()->getResourceManager< HostTextureCube >();
            auto & Hp = Mp.template get<HostTextureCube>(it->e);
            D         = &Mp.template get<DeviceTexture>(it->e);

            totalHostMips = Hp.getLevelCount();
            Hi        = &Hp.getLayer(layer).getLevel(mip);
            cubeId = Mp.getID(it->e);

            if( e != it->e)
            {
                e = it->e;
                S_INFO("Copying To Device {}", Mp.getResourceName(it->e));
            }
            S_INFO("   Layer: {}, Mip: {}", layer, mip);
        }
        totalDeviceMips = D->mipmaps;

        byteSize = static_cast<uint32_t>( Hi->byteSize() );
        if( res.transferBufferAvailable() < byteSize+1024)
        {
            S_WARN("   Not Enough Staging Room ({})  Required: {} ", res.transferBufferAvailable(), byteSize+1024);
            break;
        }
        auto sBD = res.reserveTransfer(byteSize);
        S_DEBUG("   Staging Allocated.  Offset {}   Size {} ", sBD.offset, sBD.size);
        // Copy the data to the store buffer
        std::memcpy( sBD.mappedData, Hi->data(), byteSize);

        uint32_t width  = Hi->getWidth();
        uint32_t height = Hi->getHeight();

        // write the copy command from staging->device
        _copyImage( cmd, sBD.buffer, sBD.offset, width, height, D->image, D->extent,  D->format, layer, mip);

        if( totalHostMips == 1 && totalDeviceMips>1 && mip==0)
        {
            S_INFO("   Generating MipMaps");
            generateMipMaps( cmd,
                             D->image,
                             vk::Extent2D(D->extent.width,D->extent.height),
                             layer,
                             vk::ImageLayout::eShaderReadOnlyOptimal, // level 0' is the source 's current layout
                             1,                                       // level 1 is the first level
                             totalDeviceMips-1,                              // total mipmaps to generate
                             vk::ImageLayout::eUndefined,             // the mipmaps we want to generate are currently in this layout
                             vk::ImageLayout::eShaderReadOnlyOptimal);
            D->setAllClean();
        }
        D->setClean(layer,mip);

        it = m_imagesToTransfer.erase(it);

        if(texId.valid())
        {
            if( !D->isDirty() )
                getSystemBus()->setDeviceResourceValid( texId, true);
        }
        if(cubeId.valid())
        {
            if( !D->isDirty() )
            {
                getSystemBus()->setDeviceResourceValid( cubeId, true);
            }
        }
    }


}


}
}
