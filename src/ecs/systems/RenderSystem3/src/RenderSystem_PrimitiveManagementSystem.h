#ifndef VKA_ECS_RENDERSYSTEM_PRIMITIVEMANAGEMENTSYSTEM_H
#define VKA_ECS_RENDERSYSTEM_PRIMITIVEMANAGEMENTSYSTEM_H

#include "RenderSystem3_Resources.h"
#include "device_objects.h"

namespace vka {

namespace ecs
{

class RenderSystem3;

struct RenderSystemPrimitiveManagementSystem : public SystemBaseInternal
{
    RenderSystem3Resources * m_resources = nullptr;
    RenderSystem3          * m_renderSystem = nullptr;
    entt::observer m_hostVisibleUpdated;

    void onStart() override;
    void onStop() override;

    void onUpdate() override;
    void writeCommandBuffer(vk::CommandBuffer m_commandBuffer);

    void _deviceUnloadPrimitives();

private:
    // allocate data from the device primitive
    // and make sure all the properties are set
    // this does not copy actual data over.
    static DevicePrimitive _makeDeviceOnlyPrimitive(vka::ecs::HostMeshPrimitive const & hP, uint32_t byteSize, RenderSystem3Resources *m_resources);


    /**
     * @brief _makeHostVisibleDevicePrimitive
     * @param hP
     * @param m_resources
     * @return
     *
     * Create a DevicePrimitive structure from the HostMeshPrimitive.
     */
    static DevicePrimitive _makeHostVisibleDevicePrimitive(vka::ecs::HostMeshPrimitive const & hP, RenderSystem3Resources *m_resources, DevicePrimitive dP);



};

}

}

#endif
