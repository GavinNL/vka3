#ifndef VKA_ECS_RESOURCEMANAGEMENTSYSTEM_H
#define VKA_ECS_RESOURCEMANAGEMENTSYSTEM_H

#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/Components/CTransform.h>

namespace vka
{
namespace ecs
{

/**
 * @brief The ResourceManagementSystem struct
 *
 * The resource management ssytem is responsible for keeping track
 * of unused resources and scheduling them to be unloaded after
 * a set amount of time.
 */
struct ResourceManagementSystem : public vka::ecs::SystemBaseInternal
{

    ~ResourceManagementSystem()
    {
        S_INFO("ResourceManagementSystem destroyed");
    }

    void onStart() override;
    void onStop() override;
    void onUpdate() override;

    std::string name() const override
    {
        return "ResourceManagementSystem";
    }


public:
    void onDestroyCMesh(registry_type &r, entity_type e);
};



}
}


#endif
