#include <vka/ecs/ResourceManagementSystem.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/ResourceObjects/Environment.h>
#include <vka/ecs/ResourceObjects/PBRMaterial.h>

namespace vka
{
namespace ecs
{

void ResourceManagementSystem::onStart()
{
    S_INFO("ResourceManagementSystem Connected Successfully");
}

void ResourceManagementSystem::onStop()
{
    S_INFO("ResourceManagementSystem Disconnected Successfully");
}


void ResourceManagementSystem::onUpdate()
{
#if 0
    static int ii=0;
    ii++;
    if( ii%60 == 0)
    {
        {
            auto & M = getResourceManager<PBRMaterial>();

            // loop through all the primitives that are
            // loaded into the device and check if their
            // useCount is less than the treshold
            for(auto e : M.registry().view<tag_resource_device_fully_loaded>() )
            {
                auto & id = M.getID_cr(e);
                auto uc   = id.actualUseCount();
                auto name = M.getName(id);
                std::cout << name << ",  Use Count: " << uc << std::endl;
            }
        }
        {
            auto & M = getResourceManager<HostTexture2D>();

            // loop through all the primitives that are
            // loaded into the device and check if their
            // useCount is less than the treshold
            for(auto e : M.registry().view<tag_resource_device_fully_loaded>() )
            {
                auto & id = M.getID_cr(e);
                auto uc   = id.actualUseCount();
                auto name = M.getName(id);

                std::cout << name << ",  Use Count: " << uc << std::endl;
            }
        }
    }
    //================================================
    {
        auto & M = getResourceManager<PBRMaterial>();

        // loop through all the primitives that are
        // loaded into the device and check if their
        // useCount is less than the treshold
        for(auto e : M.registry().view<tag_resource_device_fully_loaded>() )
        {
            auto & id = M.getID_cr(e);
            auto uc   = id.useCount();
            auto name = M.getName(id);

            if( uc == 0)
            {
                auto t = getSystemBus()->resourceGetTimeSinceLastStaged(id);
                if( t > 10.0)
                    getSystemBus()->resourceDeviceScheduleUnload(id);
            }
        }
    }

    {
        auto & M = getResourceManager<HostMeshPrimitive>();

        // loop through all the primitives that are
        // loaded into the device and check if their
        // useCount is less than the treshold
        for(auto e : M.registry().view<tag_resource_device_fully_loaded>() )
        {
            auto & id = M.getID_cr(e);
            auto uc   = id.useCount();
            auto name = M.getName(id);

            if( uc == 0)
            {
                auto t = getSystemBus()->resourceGetTimeSinceLastStaged(id);
                if( t > 10.0)
                    getSystemBus()->resourceDeviceScheduleUnload(id);
            }
        }
    }

    {
        auto & M = getResourceManager<HostTexture2D>();

        // loop through all the primitives that are
        // loaded into the device and check if their
        // useCount is less than the treshold
        for(auto e : M.registry().view<tag_resource_device_fully_loaded>() )
        {
            auto & id = M.getID_cr(e);
            auto uc   = id.useCount();
            auto name = M.getName(id);

            auto t = getSystemBus()->resourceGetTimeSinceLastStaged(id);
            //std::cout << name << ": " << t << std::endl;
            //std::cout << name << ": " << uc << std::endl;
            if( uc == 0)
            {
                //auto t = getSystemBus()->resourceGetTimeSinceLastStaged(id);
                if( t > 10.0)
                    getSystemBus()->resourceDeviceScheduleUnload(id);
            }
        }
    }

    {
        auto & M = getResourceManager<HostTextureCube>();

        // loop through all the primitives that are
        // loaded into the device and check if their
        // useCount is less than the treshold
        for(auto e : M.registry().view<tag_resource_device_fully_loaded>() )
        {
            auto & id = M.getID_cr(e);
            auto uc   = id.useCount();
            auto name = M.getName(id);

            if( uc == 0)
            {
                getSystemBus()->resourceDeviceScheduleUnload(id);
            }
        }
    }
#endif
}

}
}
