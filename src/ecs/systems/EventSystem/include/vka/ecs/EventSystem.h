#ifndef VKA_ECS_EVENT_SYSTEM_H
#define VKA_ECS_EVENT_SYSTEM_H

#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/Components/CEvent.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Events/InputEvents.h>

#include <map>
#include <list>
#include <set>

namespace vka
{
namespace ecs
{

/**
 * @brief The EventSystem class
 *
 * The EventSystem intercepts various events and
 * passes them to the CEvent. The Event Component
 */
class EventSystem : public vka::ecs::SystemBaseInternal
{
public:

    void onStart() override;
    void onStop() override;
    void onUpdate() override;


    //==========================================================================
    // Application/Window Handling Events
    //   - these events will be translated into  Game Events.
    //==========================================================================
    void onMouseMove(vka::EvtInputMouseMotion m);

    void onMouseButton(vka::EvtInputMouseButton m);

    void onMouseWheel(vka::EvtInputMouseWheel m);

    void onKey(vka::EvtInputKey m);



    std::string name() const override
    {
        return "EventSystem";
    }


    void clear();
};

}
}

#endif
