#include <vka/ecs/EventSystem.h>

void vka::ecs::EventSystem::onUpdate()
{
    auto & V = getSystemBus()->variables;

    V.INPUT.MOUSE.xrel = 0;
    V.INPUT.MOUSE.yrel = 0;

    auto now = std::chrono::system_clock::now();
    for(auto e : view<CEvent>())
    {
        auto & E = get<CEvent>(e);


        E.callbacks.erase(
        std::remove_if(E.callbacks.begin(),E.callbacks.end(),
                        [&](auto & r)
        {
            r.first.entity.entity    = e;
            r.first.entity.registry  = &getSystemBus()->registry;
            r.first.entity.systemBus = getSystemBus().get();
            r.first.count++;

            if( now - r.first.m_lastExecuted >= r.first.m_interval)
            {
                r.second(r.first);
                r.first.m_lastExecuted = now;
            }

            return r.first.m_exit;
        }), E.callbacks.end());
    }
}

void vka::ecs::EventSystem::onMouseMove(vka::EvtInputMouseMotion m)
{
    auto & V = getSystemBus()->variables;

    V.INPUT.MOUSE.x    = m.x;
    V.INPUT.MOUSE.y    = m.y;
    V.INPUT.MOUSE.xrel += m.xrel;
    V.INPUT.MOUSE.yrel += m.yrel;

    V.mouseScreenPosition = glm::vec2(m.x,m.y);
    V.mouseScreenPositionDelta += glm::vec2(m.xrel, m.yrel);
}

void vka::ecs::EventSystem::onMouseButton(vka::EvtInputMouseButton m)
{
    getSystemBus()->variables.mouseButtonState[m.button] = m.state;

    auto & V = getSystemBus()->variables;

    switch( m.button )
    {
        case MouseButton::LEFT  : V.INPUT.MOUSE.BUTTON.LEFT = m.state;   break;
        case MouseButton::MIDDLE: V.INPUT.MOUSE.BUTTON.MIDDLE = m.state; break;
        case MouseButton::RIGHT : V.INPUT.MOUSE.BUTTON.RIGHT = m.state;  break;
        case MouseButton::X1    : V.INPUT.MOUSE.BUTTON.X1 = m.state;     break;
        case MouseButton::X2    : V.INPUT.MOUSE.BUTTON.X2 = m.state;     break;
    default:
        break;
    }
}

void vka::ecs::EventSystem::onMouseWheel(vka::EvtInputMouseWheel m)
{
    (void)m;
}

void vka::ecs::EventSystem::onKey(vka::EvtInputKey m)
{
    getSystemBus()->variables.keyState[m.keycode] = m.down;
    getSystemBus()->variables.INPUT.KEY._keymap[m.keycode] = m.down;
}

void vka::ecs::EventSystem::onStart()
{
    sink<vka::EvtInputMouseMotion>().connect<&EventSystem::onMouseMove>(*this);
    sink<vka::EvtInputMouseButton>().connect<&EventSystem::onMouseButton>(*this);
    sink<vka::EvtInputMouseWheel>() .connect<&EventSystem::onMouseWheel>(*this);
    sink<vka::EvtInputKey>()        .connect<&EventSystem::onKey>(*this);

    S_INFO("EventSystem Connected Successfully");
}

void vka::ecs::EventSystem::onStop()
{
    sink<vka::EvtInputMouseMotion>().disconnect<&EventSystem::onMouseMove>(*this);
    sink<vka::EvtInputMouseButton>().disconnect<&EventSystem::onMouseButton>(*this);
    sink<vka::EvtInputMouseWheel>() .disconnect<&EventSystem::onMouseWheel>(*this);
    sink<vka::EvtInputKey>()        .disconnect<&EventSystem::onKey>(*this);

    S_INFO("EventSystem Disconnected Successfully");
}

void vka::ecs::EventSystem::clear()
{
    getSystemBus()->registry.clear<CEvent>();
}

void vka::ecs::CEvent::addEvent(vka::ecs::CEvent::CallbackType f, std::chrono::nanoseconds interval)
{
    callbacks.push_back( { {}, f });
    callbacks.back().first.setIntervalC(interval);
}


void vka::ecs::CEvent::addEvent(vka::ecs::EventType type, vka::ecs::CEvent::CallbackType f, std::chrono::nanoseconds interval)
{
    (void)interval;
    (void)type;
    callbacks.push_back( { {}, f });
}
