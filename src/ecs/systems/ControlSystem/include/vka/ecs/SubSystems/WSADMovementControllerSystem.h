#ifndef VKA_ECS_WSADMOVEMENTCONTROLSYSTEM_H
#define VKA_ECS_WSADMOVEMENTCONTROLSYSTEM_H

#include <vka/ecs/SystemBase2.h>

#include <vka/ecs/Components/CWSADMovement.h>
#include <vka/ecs/Components/CTransform.h>

namespace vka
{
namespace ecs
{

struct WSADMovementControllerSystem : public vka::ecs::SystemBaseInternal
{
    struct CPrivateWSADMovementComponent
    {
        glm::vec3 speed;
    };
    void onStart() override
    {
        on_construct<CWSADMovement>().connect<&entt::registry::emplace<CPrivateWSADMovementComponent>>();
        on_destroy<CWSADMovement>().connect<&entt::registry::on_destroy<CPrivateWSADMovementComponent>>();

        for(auto e : view<CWSADMovement>())
        {
            emplace_or_replace<CPrivateWSADMovementComponent>(e);
        }
    }
    void onStop() override
    {
        on_construct<CWSADMovement>().disconnect<&entt::registry::emplace<CPrivateWSADMovementComponent>>();
        on_destroy<CWSADMovement>().disconnect<&entt::registry::on_destroy<CPrivateWSADMovementComponent>>();
    }

    void onUpdate() override
    {
        using namespace vka;
        using namespace vka::ecs;

        float dt = static_cast<float>(DeltaTime());
        auto _v  = view<CWSADMovement, CPrivateWSADMovementComponent, CPosition, CRotation>();

        _v.each(
            [dt,this](auto e, auto & L, auto & Lp, auto & P, auto & R)
            {
                glm::vec3 dV;
                float forward = static_cast<float>(getInput().KEY[ L.forward ]);
                float right   = static_cast<float>(getInput().KEY[ L.right ]);
                float down    = static_cast<float>(getInput().KEY[ L.down ]);
                float left    = static_cast<float>(getInput().KEY[ L.left ]);

                dV.z = forward - down;
                dV.x = left - right;

                const auto desiredLinearSpeed = ( getInput().KEY[ L.slow ] ? L.slowSpeed : L.maxSpeed  ) * dV;

                Lp.speed = glm::mix( Lp.speed, desiredLinearSpeed, L.easing);

                //L.speed += dV * ;
                glm::vec3 newP = P + R * Lp.speed * dt;
                emplace_or_replace<CPosition>(e, newP);
            }
        );
    }
};


}
}

#endif
