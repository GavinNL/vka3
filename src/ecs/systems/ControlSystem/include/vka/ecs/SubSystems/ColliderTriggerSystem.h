#ifndef VKA_ECS_COLLIDERTRIGGERSYSTEM_H
#define VKA_ECS_COLLIDERTRIGGERSYSTEM_H

#include <vka/ecs/SystemBase2.h>

#include <vka/ecs/Components/CPLerp.h>
#include <vka/ecs/Components/CColliderTrigger.h>

namespace vka
{
namespace ecs
{

template<typename T>
struct ChangeDetector
{
    ChangeDetector& operator=(T const v)
    {
        m_previous = m_current;
        m_current = v;
        return *this;
    }
    bool changed() const
    {
        return m_previous != m_current;
    }
    bool changed(T const & v) const
    {
        return (v == m_current) && changed();
    };
    T current() const
    {
        return m_current;
    }
    T previous() const
    {
        return m_previous;
    }

    T m_current;
    T m_previous;
};

/**
 * @brief The PLERPControllerSystem struct
 *
 * The PLERPControllerSystem (Position Lerp) keeps
 * an entity at a particular lerped position between two others
 */
struct ColliderTriggerSystem : public vka::ecs::SystemBaseInternal
{
    entity_type m_lastHit = entt::null;

    entity_type m_currentPressed = entt::null;

    std::optional<vec2> m_mouseLastPos;

    vec3 m_lastPressedMousePosition;

    bool m_isDragging=false;

    ChangeDetector<entt::entity> m_hover;
    ChangeDetector<bool>         m_pressed;
    ChangeDetector<glm::vec2>    m_MousePosition;
    entity_type                  m_lastPressedEntity = entt::null;

    void onStart() override
    {
        m_hover = entt::entity(entt::null);
        m_hover = entt::entity(entt::null);

        m_pressed = false;
        m_pressed = false;
    }
    void onStop() override
    {
    }

    void onUpdate() override
    {
        using namespace vka;
        using namespace vka::ecs;

        auto & r = getSystemBus()->registry;

        ColliderTriggerEvent event;

        auto mouseHit = getSystemBus()->variables.mouseEntityHit;

        m_hover         = mouseHit;
        m_pressed       = getInput().MOUSE.BUTTON.LEFT;
        m_MousePosition = getSystemBus()->variables.mouseWorldHitPosition;

        if( r.valid(mouseHit) )
        {
            event.position = getSystemBus()->variables.mouseWorldHitPosition;
            event.normal   = getSystemBus()->variables.mouseWorldHitNormal;
        }
        event.lastPressedPosition =  m_lastPressedMousePosition;
        event.mouseRay = getSystemBus()->variables.mouseRay;


        if( m_hover.changed() )
        {
            if( m_hover.previous() != entt::null )
            {
                auto e = m_hover.previous();
                if( r.valid(e) && r.has<CColliderTrigger>(e))
                {
                     auto & last  = get<CColliderTrigger>(e);
                     event.type   = ColliderEventType::MOUSE_EXIT;
                     event.entity = e;
                     last.callback( event );
                }
            }
            if( m_hover.current() != entt::null )
            {
                auto e = m_hover.current();
                if( r.valid(e) && r.has<CColliderTrigger>(e))
                {
                     auto & last  = get<CColliderTrigger>(e);
                     event.type   = ColliderEventType::MOUSE_ENTER;
                     event.entity = e;
                     last.callback( event );
                }
                if( m_pressed.current() && !m_pressed.previous()) // clicked
                {
                    auto & last  = get<CColliderTrigger>(e);
                    event.type   = ColliderEventType::MOUSE_PRESS;
                    event.entity = e;
                    last.callback( event );
                    m_lastPressedMousePosition = event.position;
                }
            }
        }
        if( m_pressed.changed(true) )
        {
            auto e = m_hover.current();
            if( m_hover.current() != entt::null)
            {
                if( r.valid(e) && r.has<CColliderTrigger>(e))
                {
                     auto & last  = get<CColliderTrigger>(e);
                     event.type   = ColliderEventType::MOUSE_PRESS;
                     event.entity = e;
                     last.callback( event );
                     m_lastPressedEntity = e;
                }
            }
        }
        if( m_pressed.changed(false))
        {
            auto e = m_hover.current();
            if( m_hover.current() != entt::null)
            {
                if( r.valid(e) && r.has<CColliderTrigger>(e))
                {
                     auto & last  = get<CColliderTrigger>(e);
                     event.type   = ColliderEventType::MOUSE_RELEASE;
                     event.entity = e;
                     last.callback( event );
                }
            }
            m_lastPressedEntity = entt::null;
        }

        if( r.valid(m_lastPressedEntity) )
        {
            auto e = m_lastPressedEntity;
            if( r.valid(e) && r.has<CColliderTrigger>(e))
            {
                 auto & last  = get<CColliderTrigger>(e);
                 event.type   = ColliderEventType::MOUSE_DRAG;
                 event.entity = e;
                 last.callback( event );
            }

        }

        return;
    }
};


}
}

#endif
