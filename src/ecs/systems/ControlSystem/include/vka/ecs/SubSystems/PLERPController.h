#ifndef VKA_ECS_PLERPCONTROLLERCONTROLSYSTEM_H
#define VKA_ECS_PLERPCONTROLLERCONTROLSYSTEM_H

#include <vka/ecs/SystemBase2.h>

#include <vka/ecs/Components/CPLerp.h>
#include <vka/ecs/Components/CTransform.h>

namespace vka
{
namespace ecs
{

/**
 * @brief The PLERPControllerSystem struct
 *
 * The PLERPControllerSystem (Position Lerp) keeps
 * an entity at a particular lerped position between two others
 */
struct PLERPControllerSystem : public vka::ecs::SystemBaseInternal
{
    void onStart() override
    {

    }
    void onStop() override
    {
    }

    void onUpdate() override
    {
        using namespace vka;
        using namespace vka::ecs;

        auto _v  = view<CPLerp, CPosition>();

        _v.each(
            [this](auto e, auto & L, auto & P)
            {
                (void)P;
                if( has<CPosition>(L.e1) && has<CPosition>(L.e2) )
                {
                    glm::vec3 const & p1 = get<CPosition>(L.e1);
                    glm::vec3 const & p2 = get<CPosition>(L.e2);

                    glm::vec3 newP = glm::mix(p1,p2, L.lerpFactor);
                    emplace_or_replace<CPosition>(e, newP);
                }
            }
        );
    }
};


}
}

#endif
