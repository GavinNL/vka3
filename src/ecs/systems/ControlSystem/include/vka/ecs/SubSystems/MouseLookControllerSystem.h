#ifndef VKA_ECS_MOUSELOOKCONTROLSYSTEM_H
#define VKA_ECS_MOUSELOOKCONTROLSYSTEM_H

#include <vka/ecs/SystemBase2.h>

#include <vka/ecs/Components/CMouseLook.h>
#include <vka/ecs/Components/CTransform.h>

namespace vka
{
namespace ecs
{

struct MouseLookControllerSystem : public vka::ecs::SystemBaseInternal
{
    struct CPrivateMouseLook
    {
        glm::vec2 speed;
    };
    void onStart() override
    {
        on_construct<CMouseLook>().connect<&entt::registry::emplace<CPrivateMouseLook>>();
        on_destroy<CMouseLook>().connect<&entt::registry::on_destroy<CPrivateMouseLook>>();
        for(auto e : view<CMouseLook>())
        {
            emplace_or_replace<CPrivateMouseLook>(e);
        }
    }
    void onStop() override
    {
        on_construct<CMouseLook>().disconnect<&entt::registry::emplace<CPrivateMouseLook>>();
        on_destroy<CMouseLook>().disconnect<&entt::registry::on_destroy<CPrivateMouseLook>>();
    }


    void onUpdate() override
    {
        using namespace vka::ecs;

        float dt = static_cast<float>(DeltaTime());
        auto _v  = view<CMouseLook,CPrivateMouseLook,CRotation>();

        glm::vec2 dV(0.0f);
        if( getInput().MOUSE.BUTTON.RIGHT )
            dV = glm::vec2(getInput().MOUSE.xrel, getInput().MOUSE.yrel);

        _v.each(
            [dt,dV, this](auto e, auto & L, auto & Lp, auto & R)
            {
                const auto desiredRotationSpeed = dV*L.sensitivity;

                // set the speed of the rotation/second to be equal to
                Lp.speed = glm::mix( Lp.speed, desiredRotationSpeed, glm::clamp(L.easing * dt, 0.f, 1.0f) );

                auto q = _step(dt, R, Lp.speed);
                emplace_or_replace<CRotation>(e, q);
            }
        );
    }
    static glm::quat _step(float dt, glm::quat currentOrientation, glm::vec2 hv_speed)
    {
        auto yAxisInLocalSpace = glm::conjugate(currentOrientation)*glm::vec3(0,1,0);
        currentOrientation     = glm::rotate( currentOrientation, hv_speed.x * dt, yAxisInLocalSpace);
        currentOrientation     = glm::rotate( currentOrientation, hv_speed.y * dt, {1,0,0});
        return  currentOrientation;
    }
};

}
}

#endif
