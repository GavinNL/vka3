#ifndef VKA_ECS_CONTROL_SYSTEM2_H
#define VKA_ECS_CONTROL_SYSTEM2_H

#include <vka/ecs/SystemBase2.h>

#include <vka/ecs/Components/CControl.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CCamera.h>

#include <vka/ecs/Events/InputEvents.h>

namespace vka
{
namespace ecs
{

class ControlSystem : public vka::ecs::SystemBaseInternal
{
public:



    //==========================================================================
    // System level callbacks.
    //==========================================================================
    void onConstruct() override;
    void onStart() override;
    void onStop() override;
    void onUpdate() override;

    std::string name() const override
    {
        return "ControlSystem";
    }
    void clear();

    void onMouseMove(vka::EvtInputMouseMotion m);
    void onMouseButton(vka::EvtInputMouseButton m);
    void onMouseWheel(vka::EvtInputMouseWheel m);
    void onKey(vka::EvtInputKey m);


    void onConstructCControl( registry_type &r, entity_type e);
    void onUpdateCControl(registry_type &r, entity_type e );
    void onDestroyCControl(registry_type &r, entity_type e);

private:
    EvtTick m_onTick;
    bool _checkDestroy(std::shared_ptr<ControlObjectBase> & x);

    std::set< std::shared_ptr< ControlObjectEventMouse > > m_mouseEvents;
    std::set< std::shared_ptr< ControlObjectEventKey   > > m_keyEvents;
    friend struct ControlObjectBase;
};


}
}

#endif
