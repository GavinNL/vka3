#pragma once

#ifndef VKA_ECS_P_LERP_COMPONENT_H
#define VKA_ECS_P_LERP_COMPONENT_H

#include<vka/ecs/ECS.h>

namespace vka
{

namespace ecs
{

struct CPLerp
{
    entt::entity e1 = entt::null;
    entt::entity e2 = entt::null;
    float        lerpFactor = 0.5f;
};

}


}

#endif

