#pragma once

#ifndef VKA_ECS_WSAD_MOVEMENT_COMPONENT_H
#define VKA_ECS_WSAD_MOVEMENT_COMPONENT_H


#include <vka/math/linalg.h>
#include <vka/KeyCode.h>

namespace vka
{

namespace ecs
{

struct CWSADMovement
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CWSADMovement";

    glm::vec3 maxSpeed  = vec3(10.0f);
    glm::vec3 slowSpeed = vec3(1.0f);
    glm::vec3 easing    = glm::vec3(0.3f);

    KeyCode forward  = KeyCode::w;
    KeyCode right    = KeyCode::d;
    KeyCode down     = KeyCode::s;
    KeyCode left     = KeyCode::a;
    KeyCode slow     = KeyCode::LSHIFT;

};

}


}

#endif
