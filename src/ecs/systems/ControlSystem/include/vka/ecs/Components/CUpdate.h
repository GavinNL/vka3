#ifndef VKA_ECS_CUPDATE_COMPONENT_H
#define VKA_ECS_CUPDATE_COMPONENT_H

#include <vka/ecs/SystemBase.h>
#include <map>
#include <set>

namespace vka
{
namespace ecs
{


struct CUpdateCallbackInfo
{
    entt::registry & registry;
    SystemBus      & systemBus;
    decltype(CMain::m_variables) &variables;
    entt::entity     entity;

    void setVariable(std::string const& varName, std::any value)
    {
        variables[varName] = value;
    }

    template<typename T>
    T getVariable(std::string const& varName, T const & def) const
    {
        auto f = variables.find(varName);
        if( f == variables.end())
            return def;
        return std::any_cast<T>(  f->second );
    }
};

/**
 * @brief The CUpdate struct
 *
 * An update component which is used to register callbacks
 * that are called during the onUpdate call of teh Control system
 *
 * The callback function should return a bool. If true, the callback
 * will be kept and it will be called again in the next update.
 * If it returns false, the callback will be removed.
 */
struct CUpdate
{
    using CallbackType = std::function<bool(CUpdateCallbackInfo&)>;

    std::vector< CallbackType > callbacks;

    CUpdate()
    {

    }

    CUpdate(CallbackType c)
    {
        callbacks.push_back(c);
    }
};



}
}

#endif
