#pragma once

#ifndef VKA_ECS_LOOK_COMPONENT_H
#define VKA_ECS_LOOK_COMPONENT_H


#include <vka/math/linalg.h>

namespace vka
{

namespace ecs
{

struct CMouseLook
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CMouseLook";

    glm::vec2 sensitivity = {-0.15f, -0.15f};
    float easing = 30.0f;
};

}


}

#endif
