#pragma once

#ifndef VKA_ECS_COLLIDER_TRIGGER_H
#define VKA_ECS_COLLIDER_TRIGGER_H

#include <functional>
#include <vka/math/linalg.h>
#include <vka/math/geometry.h>
#include <vka/ecs/Events/InputEvents.h>
#include <vka/ecs/ECS.h>

namespace vka
{

namespace ecs
{

enum class ColliderEventType
{
    UNKNOWN_EVENT,
    MOUSE_ENTER,
    MOUSE_EXIT,
    MOUSE_PRESS,
    MOUSE_RELEASE,
    MOUSE_DRAG
};

struct ColliderTriggerEvent
{
    ColliderEventType type;
    glm::vec3         position = glm::vec3(NAN);
    glm::vec3         normal   = glm::vec3(NAN);
    vka::MouseButton  button   = vka::MouseButton::NONE;
    entt::entity      entity = entt::null;
    vka::line3        mouseRay;

    glm::vec3         lastPressedPosition;
};

/**
 * @brief The CColliderTrigger struct
 *
 * The collider trigger comonent is used to react to
 * trigger which happen in the Collider System (physics system).
 *
 * Callbacks occur when the mouse interacts with the collider object.
 */
struct CColliderTrigger
{
    using callback_type = std::function< void(ColliderTriggerEvent) >;

    // called when the mouse enters the collider object
    callback_type callback;

    CColliderTrigger()
    {

    }

    CColliderTrigger(callback_type && c) : callback( std::move(c))
    {
    }
};

}


}

#endif
