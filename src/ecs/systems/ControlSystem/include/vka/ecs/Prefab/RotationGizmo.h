#ifndef VKA_ECS_PREFAB_ROTATION_GIZMO
#define VKA_ECS_PREFAB_ROTATION_GIZMO

#include <vka/ecs/SystemBus.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CTransform.h>

#include <vka/ecs/Components/CUpdate.h>

// provided by physics system
#include <vka/ecs/Components/CColliderTrigger.h>
#include <vka/ecs/Components/CCollider.h>
#include <vka/ecs/Components/CRigidBody.h>

namespace vka {

namespace ecs {


inline entt::entity createRotationGizmo(vka::ecs::SystemBus & systemBusR)
{
    auto systemBus = &systemBusR;
    using namespace vka;
    using namespace vka::ecs;

    uint32_t segments=16;
    float inner = 1.0f;
    float outer = 1.1f;

    auto getSystemBus = [systemBus]()
    {
      return systemBus;
    };

    auto m_SystemBus=systemBus;
    auto ringId = m_SystemBus->resourceGetOrGenerate<vka::ecs::HostMeshPrimitive>("prim:gizmo/rotation/ring",
    [=]() -> HostMeshPrimitive
    {
        std::vector<glm::vec2> ring_points    = { { +0.025f, inner },{ -0.025f, inner },{ -0.025f, inner },{ -0.025f, outer },{ -0.025f, outer },{ +0.025f, outer },{ +0.025f, outer },{ +0.025f, inner } };
        return vka::ecs::HostMeshPrimitive::revolvePolygon({ 1,0,0 },{ 0,1,0 }, 32, ring_points, 0.003f);
    });

    auto m_unlit_red = getSystemBus()->resourceGetOrGenerate<vka::ecs::PBRMaterial>("mat:gizmo/unlit/red",
    [=]()
    {
        PBRMaterial mat;

        mat.baseColorFactor = {1,0,0,1};
        mat.emissiveFactor  = {1,0,0};
        return mat;
    });
    auto m_unlit_green = getSystemBus()->resourceGetOrGenerate<vka::ecs::PBRMaterial>("mat:gizmo/unlit/green",
    [=]()
    {
        PBRMaterial mat;
        mat.emissiveFactor  = {0,1,0};
        mat.baseColorFactor = {0,1,0,1};
        return mat;
    });
    auto m_unlit_blue = getSystemBus()->resourceGetOrGenerate<vka::ecs::PBRMaterial>("mat:gizmo/unlit/blue",
    [=]()
    {
        PBRMaterial mat;
        mat.emissiveFactor  = {0,0,1};
        mat.baseColorFactor = {0,0,1,1};
        return mat;
    });
    auto m_unlit_white = getSystemBus()->resourceGetOrGenerate<vka::ecs::PBRMaterial>("mat:gizmo/unlit/white",
    [=]()
    {
        PBRMaterial mat;
        mat.unlit = true;
        mat.emissiveFactor  = {1,1,1};
        mat.baseColorFactor = {1,1,1,1};
        return mat;
    });

    (void)m_unlit_white;
    auto E   = m_SystemBus->createEntity();
    E.emplaceComponent( CPosition() );
    E.emplaceComponent( CRotation() );
    E.emplaceComponent( CScale()    );

    (void)m_unlit_red;
    (void)m_unlit_green;
    (void)m_unlit_blue;
    (void)m_unlit_white;

    float cylinderRadius = (outer-inner)*0.5f;
    float torusRadius = glm::mix(inner,outer,0.5f);

    auto Ex = E.createChild();

    Ex.emplaceComponent( CMesh(ringId, m_unlit_red));
    Ex.emplaceComponent( CRotation() );
    Ex.emplaceComponent( CCollider( TorusCollider(torusRadius, cylinderRadius, 0, segments) ) );
    Ex.emplaceComponent( CGhostBody() );

    auto Ey = E.createChild();
    Ey.emplaceComponent( CMesh(ringId, m_unlit_green) );
    Ey.emplaceComponent( CRotation( glm::quat( glm::vec3(0,0,glm::half_pi<float>()) ) ) );
    Ey.emplaceComponent( CCollider( TorusCollider(torusRadius, cylinderRadius, 0, segments) ));
    Ey.emplaceComponent( CGhostBody() );

    auto Ez = E.createChild();
    Ez.emplaceComponent( CMesh(ringId, m_unlit_blue) );
    Ez.emplaceComponent( CRotation().lookat( {0,0,0}, {0,1,0}, {1,0,0} ) );
    Ez.emplaceComponent( CCollider( TorusCollider(torusRadius, cylinderRadius, 0, segments) ) );
    Ez.emplaceComponent( CGhostBody() );


    struct callbackData
    {
        int axis;
        entt::entity parent;
        PBRMaterial_ID defaultMat;
        PBRMaterial_ID hoverMat;


        int count=0;
        glm::vec3 rotationOffset;
        vka::plane3 rotationPlane;
        glm::quat quatOffset;
    };

    // a callback for when the mouse colliders with the collider
    auto callback = [systemBus]( ColliderTriggerEvent Tr,
                                 std::shared_ptr<callbackData> data)
    {
        auto axis           = data->axis;
        auto parent_entity  = data->parent;
        auto defaultMat     = data->defaultMat;
        auto hoverMat       = data->hoverMat;

        auto parentRotation = systemBus->registry.get<CRotation>(parent_entity);
        auto parentPosition = systemBus->registry.get<CPosition>(parent_entity);
        auto localAxis      = glm::mat3(1.0f)[axis];
        auto constraintAxis = parentRotation * localAxis;

        auto mouseRay = Tr.mouseRay;

        line3 L0( point3(parentPosition), constraintAxis);

        switch(Tr.type)
        {
            case ColliderEventType::UNKNOWN_EVENT:
                break;
            case ColliderEventType::MOUSE_ENTER:
            {
                systemBus->patchComponent<CMesh>(Tr.entity,
                                        [=](CMesh & M)
                {
                   M.primitives.front().material = hoverMat;
                });
            }
            break;
            case ColliderEventType::MOUSE_EXIT:
            {
                systemBus->patchComponent<CMesh>(Tr.entity,
                                        [=](CMesh & M)
                {
                   M.primitives.front().material = defaultMat;
                });
            }
            break;
            case ColliderEventType::MOUSE_PRESS:
            {
                vka::plane3 rPlane( point3(parentPosition), constraintAxis);

                auto pointOnPlane = intersection(rPlane, mouseRay);

                data->rotationPlane  = rPlane;
                data->rotationOffset = glm::normalize(pointOnPlane.asVec() - parentPosition);
                data->quatOffset     = parentRotation;
            }
            break;
            case ColliderEventType::MOUSE_RELEASE:
            {
            }
            break;
            case ColliderEventType::MOUSE_DRAG:
            {
                plane3 rotationPlane = data->rotationPlane;

                // find the point on the rotation plane where the mouse ray
                // intersects
                auto p = intersection(rotationPlane, Tr.mouseRay).asVec();

                auto currentOffset  = glm::normalize(p - parentPosition);
                auto rotationOffset = data->rotationOffset;

                auto dx = glm::dot( rotationOffset, currentOffset);
                auto dy = glm::dot( -glm::cross( rotationOffset, rotationPlane.n), currentOffset);

                auto rotationAngle = std::atan2( dy,dx);

                vka::Transform t;
                t.rotation = data->quatOffset;

                float snapIncrement = 5.0;
                if( snapIncrement < 0)
                {
                    t.rotateLocal( localAxis, rotationAngle );
                }
                else
                {
                   t.rotateLocal( localAxis, ( snapIncrement*glm::pi<float>()/180.0f) * glm::floor(rotationAngle / ( snapIncrement*glm::pi<float>()/180.0f) ) );
                }

                systemBus->registry.emplace_or_replace<CRotation>(parent_entity, t.rotation);
            }
            break;
        }
    };


    callbackData data;
    data.parent = E.entity;
    data.hoverMat = m_unlit_white;
    data.axis = 0;

    data.defaultMat = m_unlit_red;
    Ex.emplaceComponent( CColliderTrigger(std::bind( callback, std::placeholders::_1, std::make_shared<callbackData>(data) )));

    data.axis = 1;
    data.hoverMat = m_unlit_white;
    data.defaultMat = m_unlit_green;
    Ey.emplaceComponent( CColliderTrigger(std::bind( callback, std::placeholders::_1, std::make_shared<callbackData>(data) )));

    data.axis = 2;
    data.defaultMat = m_unlit_blue;
    Ez.emplaceComponent( CColliderTrigger(std::bind( callback, std::placeholders::_1, std::make_shared<callbackData>(data) )));
    //E.create<CControl>().attachObject( std::make_shared<ControlObjectGizmoRotationController>() );


    // here we will create a callback function that
    // will update the position component for the object
    // we want to manipulate.
    // the CUpdate system will trigger the
    // callback at the end of the control system phase
    // We will take the

    E.emplaceComponent( [](auto & rr)
    {
        auto cE = rr.template getVariable<entt::entity>("gizmo::cameraEntity", entt::null);
        auto tE = rr.template getVariable<entt::entity>("gizmo::targetEntity", entt::null);

        if( rr.registry.valid(cE) && rr.registry.valid(tE) )
        {
            // find the position of the target
            glm::vec3 pt = rr.registry.template get<vka::ecs::CPosition>(tE);

            // find the position of the camera
            glm::vec3 pc = rr.registry.template get<vka::ecs::CPosition>(cE);

            // set the rotation of the target to match the rotatin of the gizmo.
            glm::quat q  = rr.registry.template get<vka::ecs::CRotation>(rr.entity);
            rr.registry.template emplace_or_replace<vka::ecs::CRotation>(tE, q);

            // keep the gizmo's position at the halfway point between
            // the the camera and the target
            pt = glm::mix( pc, pt, 0.50f);
            rr.registry.template emplace_or_replace<vka::ecs::CPosition>(rr.entity, pt);
        }

        return true;
    });

    return  E.entity;
}



}
}

#endif
