#ifndef VKA_ECS_PREFAB_TRANSLATION_GIZMO
#define VKA_ECS_PREFAB_TRANSLATION_GIZMO

#include <vka/ecs/SystemBus.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CTransform.h>

#include <vka/ecs/Components/CUpdate.h>

// provided by physics system
#include <vka/ecs/Components/CColliderTrigger.h>
#include <vka/ecs/Components/CCollider.h>
#include <vka/ecs/Components/CRigidBody.h>

namespace vka {

namespace ecs {


/**
 * @brief createTranslationGizmo
 * @param systemBus
 * @return
 *
 * Create the translation gizmo and returns the main entity. The
 * entity returned is a hierarchy with multiple meshes
 *
 * You can use
 *
 */
inline entt::entity createTranslationGizmo(vka::ecs::SystemBus & systemBusR)
{
    auto systemBus = &systemBusR;
    const float arrowPosition = 0.6f;
    const float arrowSize     = 1.0f;
    const float arrowHead     = 0.2f;
    const float arrowWidth    = 0.1f;

    using namespace vka;
    using namespace ecs;
    auto getSystemBus = [systemBus]()
    {
      return systemBus;
    };

    {
        auto E   = getSystemBus()->createEntity();

        auto boxId = getSystemBus()->resourceGetOrGenerate<vka::ecs::HostMeshPrimitive>("prim:gizmo/translation/arrow",
        [=]()
        {
            auto H = vka::ecs::HostMeshPrimitive::Arrow( arrowHead,
                                                 arrowWidth,
                                                 arrowSize - arrowHead,
                                                 arrowWidth*0.5f,
                                                 2 ,16) ;
            H.translate(0.0f, 0.0f, -arrowSize*0.5f);
            return  H;
        });

        auto m_unlit_red = getSystemBus()->resourceGetOrGenerate<vka::ecs::PBRMaterial>("mat:gizmo/unlit/red",
        [=]()
        {
            PBRMaterial mat;

            mat.baseColorFactor = {1,0,0,1};
            mat.emissiveFactor  = {1,0,0};
            return mat;
        });
        auto m_unlit_green = getSystemBus()->resourceGetOrGenerate<vka::ecs::PBRMaterial>("mat:gizmo/unlit/green",
        [=]()
        {
            PBRMaterial mat;
            mat.emissiveFactor  = {0,1,0};
            mat.baseColorFactor = {0,1,0,1};
            return mat;
        });
        auto m_unlit_blue = getSystemBus()->resourceGetOrGenerate<vka::ecs::PBRMaterial>("mat:gizmo/unlit/blue",
        [=]()
        {
            PBRMaterial mat;
            mat.emissiveFactor  = {0,0,1};
            mat.baseColorFactor = {0,0,1,1};
            return mat;
        });
        auto m_unlit_white = getSystemBus()->resourceGetOrGenerate<vka::ecs::PBRMaterial>("mat:gizmo/unlit/white",
        [=]()
        {
            PBRMaterial mat;
            mat.unlit = true;
            mat.emissiveFactor  = {1,1,1};
            mat.baseColorFactor = {1,1,1,1};
            return mat;
        });
        (void)m_unlit_white;

        // Create each of the individual child arrows
        if(1)
        {


            struct callbackData
            {
                int axis;
                entt::entity parent;
                PBRMaterial_ID defaultMat;
                PBRMaterial_ID hoverMat;
            };

            // a callback for when the mouse colliders with the collider
            auto callback = [systemBus]( ColliderTriggerEvent Tr,
                                         std::shared_ptr<callbackData> data_p)
            {
                auto & data = *data_p;
                auto axis = data.axis;
                auto parent_entity = data.parent;
                auto defaultMat = data.defaultMat;
                auto hoverMat = data.hoverMat;

                auto parentRotation = systemBus->registry.get<CRotation>(parent_entity);
                auto parentPosition = systemBus->registry.get<CPosition>(parent_entity);
                auto constraintAxis = parentRotation * glm::mat3(1.0f)[axis];

                line3 L0( point3(parentPosition), constraintAxis);

                switch(Tr.type)
                {
                    case ColliderEventType::UNKNOWN_EVENT:
                        break;
                    case ColliderEventType::MOUSE_ENTER:
                    {
                        systemBus->patchComponent<CMesh>(Tr.entity,
                                                [=](CMesh & M)
                        {
                           M.primitives.front().material = hoverMat;
                        });
                    }
                    break;
                    case ColliderEventType::MOUSE_EXIT:
                    {
                        systemBus->patchComponent<CMesh>(Tr.entity,
                                                [=](CMesh & M)
                        {
                           M.primitives.front().material = defaultMat;
                        });
                    }
                    break;
                    case ColliderEventType::MOUSE_PRESS:
                    {
                        auto c      = vka::intersectingLine(L0, Tr.mouseRay).p.asVec();
                        auto dx     = parentPosition - c;
                        auto & main = systemBus->registry.get<CMain>(parent_entity);
                        main.m_variables["trGizmo::offset"] = dx;
                    }
                    break;
                    case ColliderEventType::MOUSE_RELEASE:
                    {
                    }
                    break;
                    case ColliderEventType::MOUSE_DRAG:
                    {
                        auto L1 = Tr.mouseRay;
                        auto L3 = vka::intersectingLine( L0, L1);

                        auto & main = systemBus->registry.get<CMain>(parent_entity);
                        auto offset = std::any_cast<glm::vec3>(main.m_variables.at("trGizmo::offset") );

                        systemBus->registry.emplace_or_replace<CPosition>(parent_entity, L3.p.asVec() + offset);
                    }
                    break;
                }
            };

            auto Ex =
            systemBus->constructChildEntity(
                             E.entity,
                             CMesh( boxId, m_unlit_red ),
                             CRotation(0,glm::half_pi<float>() ,0),
                             CPosition( arrowPosition,0,0),
                             CCollider(BoxCollider( arrowWidth*0.5f,arrowWidth*0.5f,arrowSize*0.5f), true),
                             CGhostBody()
                             );
            auto Ey =
            systemBus->constructChildEntity(
                             E.entity,
                             CMesh( boxId, m_unlit_green ),
                             CRotation( -glm::half_pi<float>() ,0, 0),
                             CPosition( 0,arrowPosition,0),
                             CCollider(BoxCollider( arrowWidth*0.5f,arrowWidth*0.5f,arrowSize*0.5f), true),
                             CGhostBody()
                             );

            auto Ez =
            systemBus->constructChildEntity(
                             E.entity,
                             CMesh( boxId, m_unlit_blue ),
                             CRotation(),
                             CPosition( 0, 0, arrowPosition),
                             CCollider(BoxCollider( arrowWidth*0.5f,arrowWidth*0.5f,arrowSize*0.5f), true),
                             CGhostBody()
                             );


            callbackData data;
            data.parent = E.entity;

            data.axis = 0;
            data.defaultMat = m_unlit_red;
            data.hoverMat = m_unlit_white;
            Ex.emplaceComponent( CColliderTrigger(std::bind( callback, std::placeholders::_1, std::make_shared<callbackData>(data) )));

            data.axis = 1;
            data.hoverMat = m_unlit_white;
            data.defaultMat = m_unlit_green;
            Ey.emplaceComponent( CColliderTrigger(std::bind( callback, std::placeholders::_1, std::make_shared<callbackData>(data) )));

            data.axis = 2;
            data.defaultMat = m_unlit_blue;
            data.hoverMat = m_unlit_white;
            Ez.emplaceComponent( CColliderTrigger(std::bind( callback, std::placeholders::_1, std::make_shared<callbackData>(data) )));

        }

        E.emplaceComponent( CPosition());
        E.emplaceComponent( CRotation());
        E.emplaceComponent( CScale() );


        // here we will create a callback function that
        // will update the position component for the object
        // we want to manipulate.
        // the CUpdate system will trigger the
        // callback at the end of the control system phase
        // We will take the
        E.emplaceComponent( CUpdate(
        [](auto & rr)
        {
            auto cE = rr.template getVariable<entt::entity>("gizmo::cameraEntity", entt::null);
            auto tE = rr.template getVariable<entt::entity>("gizmo::targetEntity", entt::null);

            if( rr.registry.valid(cE) && rr.registry.valid(tE) )
            {
                //rr.registry.template emplace_or_replace<vka::ecs::CPLerp>(rr.entity, vka::ecs::CPLerp{cE, tE, 0.5f} );

                // position of the the gizmo (this is the actual position)
                glm::vec3 p  = rr.registry.template get<vka::ecs::CPosition>(rr.entity);

                // find the position of the camera
                glm::vec3 pc = rr.registry.template get<vka::ecs::CPosition>(cE);

                // figure out where the extrapolated position of the
                // object.
                p = pc + (p-pc)*2.0f;

                // set the position of our object.
                rr.registry.template emplace_or_replace<vka::ecs::CPosition>(tE, p);

                auto pt = glm::mix( p, pc, 0.5f);
                rr.registry.template emplace_or_replace<vka::ecs::CPosition>(rr.entity, pt);
            }

            return true;
        }));


        return E.entity;
    }
}



}
}

#endif
