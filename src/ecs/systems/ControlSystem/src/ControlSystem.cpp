#include <fstream>

#include <vka/ecs/ControlSystem.h>
#include <vka/ecs/Components/CControl.h>
#include <vka/ecs/Components/CUpdate.h>

#include <vka/ecs/SubSystems/MouseLookControllerSystem.h>
#include <vka/ecs/SubSystems/WSADMovementControllerSystem.h>
#include <vka/ecs/SubSystems/PLERPController.h>
#include <vka/ecs/SubSystems/ColliderTriggerSystem.h>

namespace vka
{
namespace ecs
{

template<>
json serialize<CWSADMovement>(SystemBus const & S, CWSADMovement const & P)
{
    (void)S;
    json j;

    j["maxSpeed"] = to_json(P.maxSpeed);
    j["slowSpeed"] = to_json(P.slowSpeed);
    j["easing"] = to_json(P.easing);

    return j;
}

template<>
json serialize<CMouseLook>(SystemBus const & S, CMouseLook const & P)
{
    (void)S;
    json j;

    j["sensitivity"] = to_json(P.sensitivity);
    j["easing"] = P.easing;

    return j;
}



void ControlSystem::onConstructCControl(registry_type &r, entity_type e)
{
    (void)r;
    (void)e;
}

void ControlSystem::onUpdateCControl(registry_type &r, entity_type e)
{
    (void)r;
    (void)e;
}

void ControlSystem::onDestroyCControl(registry_type &r, entity_type e)
{
    (void)r;
    (void)e;
    auto & C = r.get<CControl>(e);
    for(auto & x : C.m_controlObjects)
    {
        x->destroy();
        _checkDestroy(x);
    }
    C.m_controlObjects.clear();
}


struct CUpdateSystem : public vka::ecs::SystemBaseInternal
{
    void onStart() override
    {

    }
    void onStop() override
    {
    }

    void onUpdate() override
    {
        using namespace vka;
        using namespace vka::ecs;


        for(auto e : view<CUpdate,CMain>() )
        {
            auto & C = get<CUpdate>(e);
            auto & CM = get<CMain>(e);

            CUpdateCallbackInfo cinfo{ getSystemBus()->registry, *getSystemBus(), CM.m_variables, entt::null};

            cinfo.entity = e;
            auto it =
            std::remove_if( C.callbacks.begin(), C.callbacks.end(),
                            [&cinfo](auto & x)
            {
                return !x(cinfo);
            });

            C.callbacks.erase(it, C.callbacks.end());
        }
    }
};

void ControlSystem::onConstruct()
{
    getSystemBus()->registerSerializer<CMouseLook>();
    getSystemBus()->registerSerializer<CWSADMovement>();
}

void ControlSystem::onStart()
{
    sink<vka::EvtInputMouseMotion>().connect<&ControlSystem::onMouseMove>(*this);
    sink<vka::EvtInputMouseButton>().connect<&ControlSystem::onMouseButton>(*this);
    sink<vka::EvtInputMouseWheel>() .connect<&ControlSystem::onMouseWheel>(*this);
    sink<vka::EvtInputKey>()        .connect<&ControlSystem::onKey>(*this);

    on_construct<CControl>().connect<  &ControlSystem::onConstructCControl>(*this);
    on_destroy<CControl>().connect<  &ControlSystem::onDestroyCControl>(*this);
    on_update<CControl>().connect<  &ControlSystem::onUpdateCControl>(*this);

    S_INFO("ControlSystem Connected Successfully");

    addPostSubSystem<MouseLookControllerSystem>();
    addPostSubSystem<WSADMovementControllerSystem>();
    addPostSubSystem<PLERPControllerSystem>();
    addPostSubSystem<ColliderTriggerSystem>();

    // this should always be the last one.
    addPostSubSystem<CUpdateSystem>();
}

void ControlSystem::onStop()
{
    sink<vka::EvtInputMouseMotion>().disconnect<&ControlSystem::onMouseMove>(*this);
    sink<vka::EvtInputMouseButton>().disconnect<&ControlSystem::onMouseButton>(*this);
    sink<vka::EvtInputMouseWheel>() .disconnect<&ControlSystem::onMouseWheel>(*this);
    sink<vka::EvtInputKey>()        .disconnect<&ControlSystem::onKey>(*this);

    on_construct<CControl>().disconnect<&ControlSystem::onConstructCControl>(*this);
    on_destroy<CControl>().disconnect<&ControlSystem::onDestroyCControl>(*this);
    on_update<CControl>().disconnect<  &ControlSystem::onUpdateCControl>(*this);
    S_INFO("ControlSystem Disconnected Successfully");
}

void ControlSystem::onUpdate()
{
    m_onTick.last    = m_onTick.now;
    m_onTick.now     = std::chrono::system_clock::now();
    m_onTick.delta   = m_onTick.now - m_onTick.last;
    m_onTick.delta_d = std::chrono::duration<double>(m_onTick.delta).count();
    m_onTick.count++;

    for(auto e : view<CControl>() )
    {
        auto & C = get<CControl>(e);
        for(auto & x : C.m_controlObjects )
        {
            if( x )  // valid pointer
            {
                if( x->m_state == ControlObjectState::STOPPED ) // object is currently stopped. we should start it
                {
                    x->m_entity = e;
                    x->m_systemBus = getSystemBus().get();

                    x->onStart(); // start it
                    x->m_state = ControlObjectState::DISABLED; // and place it in the disabled state
                }

                if( x->m_state == ControlObjectState::DISABLED && x->m_enabled == true) // is it currently disabled and it wants to enable?
                {
                    x->onEnabled(); // enable
                    x->m_state = ControlObjectState::ENABLED;        // place it in enabled state
                    // check if it requires any other input events
                    if( auto y = std::dynamic_pointer_cast<ControlObjectEventKey>(x))
                    {
                        m_keyEvents.insert(y);
                    }
                    if( auto y = std::dynamic_pointer_cast<ControlObjectEventMouse>(x))
                    {
                        m_mouseEvents.insert(y);
                    }
                }

                // run the on-tick event
                if( auto y = std::dynamic_pointer_cast<ControlObjectEventTick>(x))
                {
                    if( y->m_enable )
                        y->onTick(m_onTick);
                }

                _checkDestroy(x);
            }
        }
    }


}

bool ControlSystem::_checkDestroy(std::shared_ptr<ControlObjectBase> & x)
{
    if( x->m_state == ControlObjectState::ENABLED)
    {
        // destroy() was called, but disable() wasn't
        // so set the disable() flag
        if( x->m_toDestroy && x->m_enabled)
        {
            x->disable();
        }

        // disable set
        if( x->m_enabled == false)
        {
            x->onDisabled();
            x->m_state = ControlObjectState::DISABLED;
            auto y = std::dynamic_pointer_cast<ControlObjectEventMouse>(x);
            auto z = std::dynamic_pointer_cast<ControlObjectEventKey>(x);
            if( y ) m_mouseEvents.erase(y);
            if( z ) m_keyEvents.erase(z);
        }

    }

    if( x->m_state == ControlObjectState::DISABLED && x->m_toDestroy == true)
    {
        x->onDestroy();
        x->m_state = ControlObjectState::DESTROYED;

        x.reset(); // remove the pointer
    }

    return false;
}


void ControlSystem::onMouseMove(EvtInputMouseMotion m)
{
    getSystemBus()->variables.mouseScreenPosition       = glm::vec2(m.x,m.y);
    getSystemBus()->variables.mouseScreenPositionDelta += glm::vec2(m.xrel, m.yrel);

    for(auto x : m_mouseEvents)
    {
        //if( auto y = x.lock() )
        {
            if(x->m_enable)
            {
                x->onMouseMove(m);
            }
        }
    }
}

void ControlSystem::onMouseButton(EvtInputMouseButton m)
{
    getSystemBus()->variables.mouseButtonState[m.button] = m.state;

    for(auto x : m_mouseEvents)
    {
       // if( auto y = x.lock() )
        {
            if(x->m_enable)
            {
                x->onMouseButton(m);
            }
        }
    }
}

void ControlSystem::onMouseWheel(EvtInputMouseWheel m)
{
    (void)m;
    for(auto x : m_mouseEvents)
    {
        //if( auto y = x.lock() )
        {
            if(x->m_enable)
            {
                x->onMouseWheel(m);
            }
        }
    }
}

void ControlSystem::onKey(EvtInputKey m)
{
    (void)m;
    for(auto x : m_keyEvents)
    {
        //if( auto y = x.lock() )
        {
            if(x->m_enable)
            {
                x->onKey(m);
            }
        }
    }
}




void ControlSystem::clear()
{
    m_keyEvents.clear();
    m_mouseEvents.clear();
}



}
}
