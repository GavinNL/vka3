#include <iostream>

#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock
#include <sstream>
#include <regex>
#include "catch.hpp"

#include <fmt/format.h>
#include <vulkan/vulkan.hpp>
#include <vka/math/linalg.h>

#include <vka/ecs/CollisionShapeHelper.h>

#include <vka/math/geometry.h>


SCENARIO("")
{

    vka::box_t<float> B({0.5,0.5,0.5});

    REQUIRE( B({0,0,0}) < 0 );

    REQUIRE( B({1,0,0}) > 0 );
    REQUIRE( B({0,1,0}) > 0 );
    REQUIRE( B({0,0,1}) > 0 );

    REQUIRE( B({-1,0,0}) > 0 );
    REQUIRE( B({0,-1,0}) > 0 );
    REQUIRE( B({0,0,-1}) > 0 );

    REQUIRE( B({0,0,0.5f}) == Approx(0.0f) );
    REQUIRE( B({0.5f,0.5f,0.5f}) == Approx(0.0f) );
    REQUIRE( B({-0.5f,-0.5f,-0.5f}) == Approx(0.0f) );

}


SCENARIO("Collision Shape Helper")
{



    GIVEN("A sphere shape at {10,0,0}")
    {
        btSphereShape S(1.0f);
        btTransform T;
        T.setIdentity();
        T.setOrigin( {10,0,0});

        WHEN("We cast a ray from the origin to {10,10,10}")
        {
            vka::ecs::CollisionShapeHelper H;
            H.setRay( {0,0,0}, {10,0,0});

            THEN("THe ray will hit with a value of 0.9f")
            {
                auto h = H.rayTest(S, T);
                REQUIRE( h.w == Approx(0.9));

            }
        }
    }

    GIVEN("A sphere shape at {20,0,0}")
    {
        btSphereShape S(1.0f);
        btTransform T;
        T.setIdentity();
        T.setOrigin( {20,0,0});

        WHEN("We cast a ray from the origin to {10,10,10}")
        {
            vka::ecs::CollisionShapeHelper H;
            H.setRay( {0,0,0}, {10,0,0});

            THEN("The ray will hit with a value of greater than 1")
            {
                REQUIRE( H.rayTest(S, T).w > 1.0f);
            }
        }
    }

}
