#ifndef VKA_ECS_COLLISION_SHAPE_HELPER_H
#define VKA_ECS_COLLISION_SHAPE_HELPER_H

#include <btBulletCollisionCommon.h>
#include <vka/math/transform.h>
#include <optional>

namespace vka
{

namespace ecs
{

struct CollisionShapeHelper
{
    struct HitPoint
    {
        glm::vec3 position;
        glm::vec3 normal;
    };

    struct Callback : public btCollisionWorld::RayResultCallback
    {
        btScalar hitFraction = 2.0f;
        glm::vec3 normal;

        Callback() : btCollisionWorld::RayResultCallback()
        {

        }
        virtual btScalar addSingleResult( btCollisionWorld::LocalRayResult& rayResult, bool normalInWorldSpace) override
        {
            hitFraction = rayResult.m_hitFraction;
            normal = {rayResult.m_hitNormalLocal[0],rayResult.m_hitNormalLocal[1],rayResult.m_hitNormalLocal[2]};
            return rayResult.m_hitFraction;
            (void)normalInWorldSpace;
        }
    };

    btTransform rayFromTrans;
    btTransform rayToTrans;

    CollisionShapeHelper()
    {
        rayToTrans.setIdentity();
        rayFromTrans.setIdentity();
    }

    void setRay(glm::vec3 from, glm::vec3 to)
    {
        rayFromTrans.setOrigin( btVector3(from.x, from.y, from.z));
        rayToTrans.setOrigin( btVector3(to.x, to.y, to.z));
    }

    glm::vec4 rayTest(btCollisionShape const & shape,
                 btTransform const &T) const
    {

        Callback Cb;
        btCollisionObjectWrapper W(nullptr, &shape, nullptr, T, 10,15);
        btCollisionWorld::rayTestSingleInternal(rayFromTrans,
                                               rayToTrans,
                                               &W,
                                               Cb);
        //glm::mix(   *reinterpret_cast<glm::vec3 const *>( &rayFromTrans.getOrigin() ),
        //            *reinterpret_cast<glm::vec3 const *>( &rayToTrans.getOrigin()   ),
        //            Cb.hitFraction
        //            )
        return vec4(Cb.normal, Cb.hitFraction);
        //return Cb.hitFraction;
    }


    glm::vec4 rayTest(btCollisionShape const & shape,
                     vka::Transform const &tr) const
    {
        btTransform T(  btQuaternion(tr.rotation.x,tr.rotation.y,tr.rotation.z,tr.rotation.w),
                         btVector3(tr.position.x, tr.position.y, tr.position.z));

        Callback Cb;
        btCollisionObjectWrapper W(nullptr, &shape, nullptr, T, 10,15);
        btCollisionWorld::rayTestSingleInternal(rayFromTrans,
                                               rayToTrans,
                                               &W,
                                               Cb);
        return vec4(Cb.normal, Cb.hitFraction);
//        return Cb.hitFraction;
    }

    std::optional<HitPoint> castRay( btCollisionShape const & shape, vka::Transform const &tr)
    {
        auto v = rayTest(shape, tr);
        if( v.w > 1.0f) return {};

        HitPoint p{
             glm::mix(   *reinterpret_cast<glm::vec3 const *>( &rayFromTrans.getOrigin() ),
                         *reinterpret_cast<glm::vec3 const *>( &rayToTrans.getOrigin()   ),
                         v.w
                         ),
             glm::vec3( v.x, v.y, v.z)
        };
        return p;

    }
};

}

}


#endif
