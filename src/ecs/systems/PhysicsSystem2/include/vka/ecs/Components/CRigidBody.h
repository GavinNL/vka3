#ifndef VKA_ECS_RIGIDBODY_COMPONENT
#define VKA_ECS_RIGIDBODY_COMPONENT

#include <memory>

#include <vka/math/linalg.h>

#include <vka/ecs/ECS.h>
#include <vka/ecs/json.h>
#include <variant>

class btCollisionShape;
class btRigidBody;

namespace vka
{
namespace ecs
{

// an object eitehr have a static body or a rigid body component/
// not both. emplacing/replacing one of them will remove the other.
struct CStaticBody
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CStaticBody";

    int x;
};

struct CRigidBody
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CRigidBody";

    float mass         = 1.0f;
    float drag         = 0.f;
    float angularDrag  = 0.05f;

    bool isKinematic = false;  // if set to true, other physics objects will react to this,
                                 // use this for characters
    glm::vec3 angularFactor = {1,1,1};
    glm::vec3 linearFactor = {1,1,1};

    bool disableDeactivation = false;
};


/**
 * @brief The CGhostBody struct
 *
 * Ghost Bodies are used to check for events, when
 * an object enters the collider shape or leaves.
 */
struct CGhostBody
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CGhostBody";
};

}
}
#endif 
