#ifndef VKA_ECS_COLLIDER_COMPONENT
#define VKA_ECS_COLLIDER_COMPONENT

#include <memory>
#include <vka/math/linalg.h>
#include <vka/ecs/json.h>
#include <variant>


namespace vka
{
namespace ecs
{

struct CVelocity : glm::vec3
{
    CVelocity() : glm::vec3(0.0f){}
    CVelocity(glm::vec3 const & _x) : glm::vec3(_x){}
    CVelocity(float _x, float _y, float _z) : glm::vec3(_x,_y,_z){}
};

struct CAngularVelocity : glm::vec3
{
    CAngularVelocity() : glm::vec3(0.0f){}
    CAngularVelocity(glm::vec3 const & _x) : glm::vec3(_x){}
    CAngularVelocity(float _x, float _y, float _z) : glm::vec3(_x,_y,_z){}
};

struct BoxCollider : glm::vec3
{
    static constexpr auto _type = "collider";
    static constexpr auto _subType = "Box";

    BoxCollider(float _halfLength) : glm::vec3(_halfLength){}
    BoxCollider(float _x, float _y, float _z) : glm::vec3(_x,_y,_z){}
    BoxCollider() : glm::vec3(0.5f){}
};

struct SphereCollider
{
    static constexpr auto _type = "collider";
    static constexpr auto _subType = "Sphere";

    float radius = 1.0f;
    SphereCollider(){}
    SphereCollider(float r) : radius(r){}
};

struct CapsuleCollider
{
    static constexpr auto _type = "collider";
    static constexpr auto _subType = "Capsule";

    float height = 1.0f;
    float radius = 1.0f;
    int   axis   = 1; // 0 x, 1 y, 2 z

    CapsuleCollider(){}
    CapsuleCollider(float h, float r, int x) : height(h), radius(r), axis(x){}
};

struct TorusCollider
{
    static constexpr auto _type = "collider";
    static constexpr auto _subType = "Torus";

    float    centralRadius = 1.0f;
    float    tubeRadius    = 0.25f;
    uint32_t segments      = 8;
    int      axis          = 0;

    TorusCollider(){}
    TorusCollider(float R, float r, int _planeAxis=0, uint32_t _segments=8) : centralRadius(R), tubeRadius(r), segments(_segments), axis(_planeAxis)
    {

    }
};

struct ConvexHullCollider
{
    static constexpr auto _type = "collider";
    static constexpr auto _subType = "ConvexHull";

    std::vector<glm::vec3> points;
    ConvexHullCollider()
    {
    }
    ConvexHullCollider(std::vector<glm::vec3> _points) : points(_points)
    {

    }
};

struct CCollider
{
    static constexpr auto _type = "component";
    static constexpr auto _subType = "CCollider";

    using variant_type = std::variant<BoxCollider,SphereCollider,CapsuleCollider,TorusCollider,ConvexHullCollider>;
    variant_type colliderType;
    bool         inheritScale = false;
    CCollider()
    {
    }
    CCollider(variant_type v) : colliderType(v)
    {}

    CCollider(variant_type v, bool _inheritScale) : colliderType(v), inheritScale(_inheritScale)
    {}
};

}
}
#endif 
