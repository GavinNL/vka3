#ifndef VKA_ECS_PHYSICS_TAGS
#define VKA_ECS_PHYSICS_TAGS

#include <vka/ecs/ECS.h>


namespace vka
{
namespace ecs
{

// This tag will be placed on an entity
// when the mouse has intersected
// the collider shape
using tag_physics_MOUSE_INTERSECT   = entt::tag<"tag_physics_MOUSE_INTERSECT"_hs>;

}
}
#endif 
