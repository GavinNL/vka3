#ifndef VKA_ECS_PHYSICS_SYSTEM_H
#define VKA_ECS_PHYSICS_SYSTEM_H

#include <set>
#include <map>

#include <vka/ecs/SystemBase2.h>

#include <vka/ecs/ResourceObjects/HostPrimitive.h>
#include <vka/utils/Scene.h>


class btDefaultCollisionConfiguration     ;
class btCollisionDispatcher               ;
class btBroadphaseInterface               ;
class btSequentialImpulseConstraintSolver ;
class btDiscreteDynamicsWorld             ;

class btBvhTriangleMeshShape;
class btConvexHullShape;
class btCompoundShape;

namespace vka
{


namespace ecs
{


class PhysicsSystem2 : public SystemBaseInternal
{
public:
    std::shared_ptr<SystemBaseInternal> m_objectConstructorSystem;

    PhysicsSystem2();
    ~PhysicsSystem2();

    void onConstruct() override;
    void onStart() override;
    void onStop() override;
    void onUpdate() override;

    std::string name() const override
    {
        return "PhysicsSystem2";
    }

    void performFrustumCulling();

    void onConstructCVelocity( registry_type &r, entity_type e);
    void   onUpdateCVelocity(registry_type &r, entity_type e );
    void   onDestroyCVelocity(registry_type &r, entity_type e);


    void enableDebugDraw(bool enable);
    void toggleDebugDraw()
    {
        bool dd = !m_debugDrawEnabled;
        enableDebugDraw(dd);
    }
    /**
     * @brief debugDraw
     * @return
     *
     * Draws all the physics debug lines into the internal
     * buffers and returns the total number of lines drawn.
     *
     * Call getDebugLinePositions() and getDebugLineColors() to
     * get the array of positions and colours so that
     * they can be sent to the render system
     */
    uint32_t debugDraw();


protected:
    bool m_debugDrawEnabled=false;
    entt::entity m_debugDrawEntity = entt::null;

    ///-----initialization_start-----

    btDefaultCollisionConfiguration*     m_collisionConfiguration = nullptr;
    btCollisionDispatcher*               m_dispatcher             = nullptr;
    btBroadphaseInterface*               m_overlappingPairCache   = nullptr;
    btSequentialImpulseConstraintSolver* m_solver                 = nullptr;
    btDiscreteDynamicsWorld*             m_dynamicsWorld          = nullptr;

    ///-----initialization_end-----

    entt::observer m_worldTransformUpdated;
    entt::observer m_positionUpdatedObserver;
    entt::observer m_rotationUpdatedObserver;
private:
    void _updateTransforms();

public:
    void onBuildComponents(entt::entity e, const nlohmann::json &components) override;

public:
    void _handleColliderComponent(entt::entity e);
    void _handleGhostObject(entt::entity e);
    void _handlePhysicsObject(entt::entity e);

};




}
}

#endif

