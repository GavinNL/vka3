#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/PhysicsSystem2.h>
#include <vka/ecs/Components/CCollider.h>
#include <vka/ecs/Components/CRigidBody.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/Tags.h>
#include <vka/ecs/Components/PhysicsTags.h>

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include <BulletCollision/CollisionShapes/btBvhTriangleMeshShape.h>
#include <BulletCollision/CollisionShapes/btStridingMeshInterface.h>
#include <bullet/BulletCollision/CollisionShapes/btConvexHullShape.h>
#include <bullet/ConvexDecomposition/ConvexDecomposition.h>


#include <bullet/BulletCollision/NarrowPhaseCollision/btRaycastCallback.h>
#include <bullet/BulletCollision/Gimpact/btGImpactShape.h>


#include <vka/ecs/Components/CCamera.h>
#include <vka/ecs/Components/CTransform.h>

#include <easy/profiler.h>

namespace vka
{
namespace ecs
{

template<>
json serialize<CCollider>(SystemBus const & S, CCollider const & P)
{
    (void)S;
    json j;

    j["inheritScale"] = P.inheritScale;
    j["colliderType"] =
    std::visit([](auto&& arg) -> json
    {
        using T = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<T, BoxCollider>)
        {
            json c;
            c["type"] = "boxCollider";
            c["size"] = { arg.x, arg.y, arg.z};
            return c;
        }
        else if constexpr (std::is_same_v<T, SphereCollider>)
        {
            json c;
            c["type"] = "sphereCollider";
            c["radius"] = arg.radius;
            return c;
        }
        else if constexpr (std::is_same_v<T, CapsuleCollider>)
        {
            json c;
            c["type"] = "capsuleCollider";
            c["radius"] = arg.radius;
            c["height"] = arg.height;
            c["axis"] = arg.axis;
            return c;
        }
        else if constexpr (std::is_same_v<T, ConvexHullCollider>)
        {
            json c;
            c["type"] = "convexHullCollider";

            json pts = json::array();

            for(auto & p : arg.points)
            {
                pts.push_back( {p.x,p.y,p.z});
            }
            c["points"] = std::move(pts);
            return c;
        }
        else if constexpr (std::is_same_v<T, TorusCollider>)
        {
            json c;
            c["type"] = "torusCollider";
            c["centralRadius"] = arg.centralRadius;
            c["tubeRadius"] = arg.tubeRadius;
            c["segments"] = arg.segments;
            c["axis"] = arg.axis;
            return c;
        }
    }, P.colliderType);

    return j;
}

template<>
json serialize<CRigidBody>(SystemBus const & S, CRigidBody const & P)
{
    (void)S;
    json j;

    j["mass"]          = P.mass;//1.0f;
    j["drag"]          = P.drag;//0.f;
    j["angularDrag"]   = P.angularDrag;//0.05f;
    j["isKinematic"]   = P.isKinematic;//false;
    j["angularFactor"] = to_json(P.angularFactor);//{1,1,1};
    j["linearFactor"]  = to_json(P.linearFactor );//{1,1,1};

    return j;
}


using tag_physics2_rebuild_rigidbody     = entt::tag<"tag_physics2_rebuild_rigidbody"_hs>;
using tag_physics2_update_rigidbody     = entt::tag<"tag_physics2_update_rigidbody"_hs>;
using tag_physics2_transform_update     = entt::tag<"tag_physics2_transform_update"_hs>;
using tag_physics2_transform_modified_externally     = entt::tag<"tag_physics2_transform_modified_externally"_hs>;

using tag_physics2_dynamic_object   = entt::tag<"tag_physics2_dynamic_object"_hs>;
using tag_physics2_static_object    = entt::tag<"tag_physics2_static_object"_hs>;
using tag_physics2_kinematic_object = entt::tag<"tag_physics2_kinematic_object"_hs>;


struct ShapeDataPrivate
{
    std::shared_ptr<btCollisionShape> shape;

    ShapeDataPrivate(std::shared_ptr<btCollisionShape> s) : shape(s){}
};
struct GhostDataPrivate
{
    btGhostObject * body;
    std::shared_ptr<btCollisionShape> shape;
};
struct RigidBodyDataPrivate
{
    btRigidBody * body = nullptr;
    std::shared_ptr<btCollisionShape> shape;
};

struct PhysicsDataPrivate2
{
    btCollisionObject * rigidBody;

    std::shared_ptr<btCollisionShape> shape;
    std::shared_ptr<btCollisionShape> newShape;

    void setTransform(btTransform const &T)
    {
        if( auto sb = dynamic_cast<btGhostObject*>(rigidBody))
        {
            sb->setWorldTransform(T);
        }
        else
        {
            auto rb = dynamic_cast<btRigidBody*>(rigidBody);
            rb->setCenterOfMassTransform(T);
        }
    }

    void setVelocity( btVector3 const & v)
    {
        if( auto sb = dynamic_cast<btRigidBody*>(rigidBody))
        {
            sb->setLinearVelocity(v);
        }
    }
};


static glm::vec3 _convert(btVector3 const & v)
{
    return glm::vec3(v.x(),v.y(),v.z());
}
static btVector3 _convert(glm::vec3 const & v)
{
    return btVector3(v.x,v.y,v.z);
}

static glm::quat _convert(btQuaternion const & v)
{
    return glm::quat(v.w(), v.x(),v.y(),v.z());
}
static btQuaternion _convert(glm::quat const & v)
{
    return btQuaternion(v.x,v.y,v.z,v.w);
}
static vka::Transform _convert(btTransform const & tr)
{
    auto quat = tr.getRotation();
    return vka::Transform( _convert(tr.getOrigin()),
                           _convert(quat));
}
static btTransform _convert(vka::Transform const & t)
{
    glm::vec3 const & p = t.position;
    glm::quat const & q = t.rotation;

    btTransform startTransform;
    startTransform.setOrigin(   _convert(p) );
    startTransform.setRotation( _convert(q) );
    return startTransform;
}

///The btDefaultMotionState provides a common implementation to synchronize world transforms with offsets.
ATTRIBUTE_ALIGNED16(struct)	myMotionState2 : public btDefaultMotionState
{
    PhysicsSystem2::registry_type            *m_registry;
    PhysicsSystem2::entity_type               m_entity;

    BT_DECLARE_ALIGNED_ALLOCATOR();

    myMotionState2(const btTransform& startTrans = btTransform::getIdentity(), const btTransform& centerOfMassOffset = btTransform::getIdentity())
        : btDefaultMotionState(startTrans, centerOfMassOffset)

    {
    }

    ///synchronizes world transform from user to physics
    virtual void getWorldTransform(btTransform & centerOfMassWorldTrans) const override
    {
        // this function is called for kinematic objects only.

        glm::vec3 p = m_registry->get<CPosition>(m_entity); // worldTransform * glm::vec4(0,0,0,1);
        glm::quat q = m_registry->get<CRotation>(m_entity);

        btTransform startTransform;
        startTransform.setOrigin(   btVector3(p.x,p.y,p.z));
        startTransform.setRotation( btQuaternion(q.x,q.y,q.z,q.w));

        centerOfMassWorldTrans = startTransform * m_centerOfMassOffset.inverse();
    }

    ///synchronizes world transform from physics to user
    ///Bullet only calls the update of worldtransform for active objects
    virtual void	setWorldTransform(const btTransform& centerOfMassWorldTrans) override
    {
        btDefaultMotionState::setWorldTransform(centerOfMassWorldTrans);
        auto tr = _convert(centerOfMassWorldTrans);
        auto & p = tr.position;
        auto & q = tr.rotation;

        //auto & v = rb->getLinearVelocity();
        //auto & w = rb->getAngularVelocity();
        // set the position and rotational components
        // of the object.

        // Update the position and rotation components.
        // We are assuming that this entity is always a root entity.
        // we are calling emplace_or_replace so that the TransformSystem
        // can properly adjust the position of child nodes.
        m_registry->emplace_or_replace<CPosition>(m_entity, p);
        m_registry->emplace_or_replace<CRotation>(m_entity, q);

        //m_registry->emplace_or_replace<tag_physics2_transform_update>(m_entity);
    }

};

class DebugDraw : public btIDebugDraw
{

   int m_debugMode = 		DBG_DrawWireframe | DBG_DrawAabb  | DBG_DrawContactPoints | DBG_DrawConstraints | DBG_DrawConstraintLimits | DBG_DrawFrames;
   vka::ecs::HostMeshPrimitive * m_H = nullptr;
public:

   DebugDraw(vka::ecs::HostMeshPrimitive & S)
   {
        m_H = &S;
   }

   virtual ~DebugDraw() override
   {

   }

   void drawLine(const btVector3 &from, const btVector3 &to, const btVector3 &fromColor, const btVector3 &toColor) override
   {
       m_H->POSITION.push_back( glm::vec3(from[0], from[1], from[2] ) );
       m_H->POSITION.push_back( glm::vec3(to[0], to[1], to[2] ) );
       m_H->COLOR_0.push_back(  glm::u8vec4( fromColor[0]*255, fromColor[1]*255, fromColor[2]*255, 255 ) );
       m_H->COLOR_0.push_back(  glm::u8vec4( toColor[0]*255       , toColor[1]*255       , toColor[2]*255       , 255 ) );
   }

   void reportErrorWarning(const char* warningString) override
   {
       std::cout << warningString << std::endl;
   }

   virtual void   drawLine(const btVector3& from, const btVector3& to, const btVector3& color) override
   {
       drawLine(from, to, color, color);
   }

//   virtual void	drawSphere(btScalar radius, const btTransform& transform, const btVector3& color) override;

//
//   virtual void   drawTriangle(const btVector3& a, const btVector3& b, const btVector3& c, const btVector3& color, btScalar alpha);
//
   virtual void   drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color) override
   {
       btTransform b;
       b.setIdentity();
       b.setOrigin(PointOnB);
       drawBox( PointOnB-btVector3(distance,distance,distance), PointOnB+btVector3(distance,distance,distance), color );
       (void)&lifeTime;
       (void)&normalOnB;
   }

   virtual void   draw3dText(const btVector3& location, const char* textString) override
   {
        (void)location;
        (void)textString;
   }

   virtual void   setDebugMode(int debugMode) override
   {
       m_debugMode = debugMode;
   }

   virtual int      getDebugMode() const override { return m_debugMode; }
};


void PhysicsSystem2::enableDebugDraw(bool enable)
{
    m_debugDrawEnabled = enable;
    if( m_debugDrawEnabled == false)
    {
        if( m_debugDrawEntity != entt::null)
        {
            auto sb = getSystemBus();
            auto e = m_debugDrawEntity;
            sb->removeComponentLater<CMesh>(e);

            //sb->queueEntityCommand(
            //            [sb,e]()
            //            {
            //                sb->destroyEntity(e);
            //            }
            //            );
            //m_debugDrawEntity = entt::null;
        }
    }
}


uint32_t PhysicsSystem2::debugDraw()
{
#if 1
    // before we call the debuDraw method for bullet.
    // we need to get the HostMeshPrimitive where all the lines will be stored
    // this HostMeshPrimitive will store ALL the lines durng the btDebugDraw
    // it will be reconstructed every frame
    //auto i = m_systemBus->getResourceId<vka::ecs::HostMeshPrimitive>("btDebugDraw");

    size_t count=0;
    HostMeshPrimitive::DrawCall  dc;

    auto i = getSystemBus()->updateOrCreateResource<vka::ecs::HostMeshPrimitive>("btDebugDraw2",
    [this, &count, &dc](HostMeshPrimitive & H )
    {
        // so get the HostMeshPrimitive and reset its values.
        //auto & H = m_systemBus->getManagedResource<vka::ecs::HostMeshPrimitive>( i );
        H.POSITION.reset<float>(3);
        H.COLOR_0.reset<uint8_t>(4);

        // now call the debug Draw with the HostMeshPrimitive
        // which will push all the debug lines into the HostMeshPrimitive, H.
        DebugDraw D( H );
        m_dynamicsWorld->setDebugDrawer(&D);
        m_dynamicsWorld->debugDrawWorld();
        m_dynamicsWorld->setDebugDrawer(nullptr);
        count = H.POSITION.count();

        dc = H.getDrawCall();
        H.subMeshes.clear();
    });

    // always set it as a host-visible resource
    getSystemBus()->getResourceManager<vka::ecs::HostMeshPrimitive>().emplaceTag<tag_resource_host_visible>(i);

    {
        auto S = getSystemBus();
        {
            //====================================================================================================
            // Now create an un-lit material for the lines.
            //====================================================================================================
            PBRMaterial pbr;
            pbr.unlit = true;
            auto m = S->resourceGetOrEmplace<vka::ecs::PBRMaterial>("btDebugDrawUnlit", std::move(pbr) );
            //====================================================================================================


            //====================================================================================================
            // Finally create the entity as a simple MatieralPrimitiveComponent using
            // the HostMeshPrimitive and the Unlit material
            //====================================================================================================
            if( m_debugDrawEntity == entt::null)
                m_debugDrawEntity = getSystemBus()->createEntity("btDebugDraw2").entity;


            CMesh mm;
            mm.primitives.emplace_back( CPrimitive(i, m, vk::PrimitiveTopology::eLineList));

            emplace_or_replace<CMesh>(m_debugDrawEntity, std::move(mm));

            if(!has<CPosition>(m_debugDrawEntity))
                emplace_or_replace<vka::ecs::CPosition>(m_debugDrawEntity, glm::vec3(0,0,0));

            emplace_or_replace<tag_render_in_frustum>(m_debugDrawEntity);
            //====================================================================================================
        }
    }

    return static_cast<uint32_t>( count );
#endif
    return 0;

}




void PhysicsSystem2::onConstructCVelocity( registry_type &r, entity_type e)
{
    if( r.has<PhysicsDataPrivate2>(e))
    {
        auto & V = r.get<CVelocity>(e);
        r.get<PhysicsDataPrivate2>(e).setVelocity(btVector3(V.x,V.y,V.z));
    }
}

void PhysicsSystem2::onUpdateCVelocity(registry_type &r, entity_type e )
{
    if( r.has<PhysicsDataPrivate2>(e))
    {
        auto & V = r.get<CVelocity>(e);
        r.get<PhysicsDataPrivate2>(e).setVelocity(btVector3(V.x,V.y,V.z));
    }
}


static std::shared_ptr<btCollisionShape> _createCollisionShape(CCollider::variant_type const & colliderType)
{
    auto shape =
    std::visit([](auto&& arg) -> std::shared_ptr<btCollisionShape>
    {
        using T = std::decay_t<decltype(arg)>;

        std::shared_ptr<btCollisionShape> ret;
        if constexpr (std::is_same_v<T, BoxCollider>)
        {
            return std::shared_ptr<btCollisionShape>( new btBoxShape( btVector3( arg.x, arg.y, arg.z) ) );
        }
        else if constexpr (std::is_same_v<T, SphereCollider>)
        {
            return std::shared_ptr<btCollisionShape>( new btSphereShape( arg.radius ) );
        }
        else if constexpr (std::is_same_v<T, CapsuleCollider>)
        {
            auto ax = std::clamp(arg.axis,0,2);
            switch(ax)
            {
                case 0: return std::shared_ptr<btCollisionShape>( new btCapsuleShapeX(arg.radius, arg.height) );
                default:
                case 1: return std::shared_ptr<btCollisionShape>( new btCapsuleShape(arg.radius, arg.height ) );
                case 2: return std::shared_ptr<btCollisionShape>( new btCapsuleShapeZ(arg.radius, arg.height) );
            }
        }
        else if constexpr (std::is_same_v<T, ConvexHullCollider>)
        {
            auto sh = new btConvexHullShape();
            for(auto & p : arg.points)
            {
                sh->addPoint( btVector3(p.x,p.y,p.z));
            }
            sh->optimizeConvexHull();
            return std::shared_ptr<btCollisionShape>(sh);
        }
        else if constexpr (std::is_same_v<T, TorusCollider>)
        {
            double outer_radius = arg.centralRadius;
            double inner_radius = arg.tubeRadius;
            double subdivisions = static_cast<double>(arg.segments);
            double pi  = glm::pi<double>();

            btVector3 forward(btScalar(0.0),btScalar(1.0),btScalar(0.0));

            btVector3 side(btScalar(outer_radius),btScalar(0.0),btScalar(0.0));

            double gap = std::sqrt(2.0*inner_radius*inner_radius
                                     - 2.0*inner_radius*inner_radius*std::cos((2.0*pi)/subdivisions));

            auto * shapec = new btCylinderShapeZ(btVector3(btScalar(inner_radius),
                                                                      btScalar(inner_radius),
                                                                      btScalar((pi/subdivisions) + 0.5*gap)));

            btTransform t;
            btCompoundShape * torus_shape = new btCompoundShape();


            btTransform axisTransform;
            axisTransform.setIdentity();

            switch (arg.axis)
            {
                case 0:
                    axisTransform.setRotation( _convert(  glm::quat( glm::vec3(0,0,glm::half_pi<float>()) ) )  );
                break;
                case 1:
                    //axisTransform.setRotation( _convert(  glm::quat( glm::vec3(0,0,glm::half_pi<float>()) ) )  );
                break;
                case 2:
                    axisTransform.setRotation( _convert(  glm::quat( glm::vec3(glm::half_pi<float>(),0,0) ) )  );
                break;

            }



            for (int x = 0; x < (int)subdivisions; x++)
            {
                btScalar     angle = btScalar((x*2.0*pi)/subdivisions);
                btVector3 position = side.rotate(forward, angle);
                btQuaternion q(forward, angle);
                t.setIdentity();
                t.setOrigin(position);
                t.setRotation(q);
                torus_shape->addChildShape( axisTransform * t, shapec);
            }


            return std::shared_ptr<btCompoundShape>(torus_shape,
                                                     [shapec](btCompoundShape * c)
            {
                delete shapec;
                delete c;
            });
        }
    }, colliderType);

    return shape;

}

using checkCollisionObject_tag = entt::tag<"BuildRigidBody::checkCollisionObject_tag"_hs>;
//using collisionObjectUpdated_tag = entt::tag<"BuildRigidBody::collisionObjectUpdated_tag"_hs>;
using colliderUpdated_tag  = entt::tag<"BuildRigidBody::rigidbodyupdated_tag"_hs>;


struct PhysicsObjectConstructionSystem : public SystemBaseInternal
{
    btDynamicsWorld * m_dynamicsWorld = nullptr;

    entt::observer m_colliderUpdated;
    entt::observer m_worldTransformUpdated;

    void onStart() override
    {
         on_construct<CCollider>().connect<&entt::registry::emplace_or_replace<colliderUpdated_tag>>();
            on_update<CCollider>().connect<&entt::registry::emplace_or_replace<colliderUpdated_tag>>();

        on_construct<CRigidBody>().connect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();
           on_update<CRigidBody>().connect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();

       on_construct<CStaticBody>().connect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();
          on_update<CStaticBody>().connect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();

        on_construct<CGhostBody>().connect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();
           on_update<CGhostBody>().connect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();

        on_destroy<GhostDataPrivate>().connect< &PhysicsObjectConstructionSystem::_onDestroyGhostBodyPrivate>(*this);
        on_destroy<RigidBodyDataPrivate>().connect< &PhysicsObjectConstructionSystem::_onDestroyRigidBodyPrivate>(*this);

       m_worldTransformUpdated.connect( getSystemBus()->registry,
                                        entt::collector.update<CWorldTransform>()
                                                         .where<GhostDataPrivate>()
                                        );

        for(auto e : view<CCollider>())
        {
            emplace_or_replace<colliderUpdated_tag>(e);
        }
        for(auto e : view<CRigidBody>())
        {
            emplace_or_replace<checkCollisionObject_tag>(e);
        }
        for(auto e : view<CGhostBody>())
        {
            emplace_or_replace<checkCollisionObject_tag>(e);
        }
    }

    void _handleAllColliderUpdates()
    {
        for(auto e : view<colliderUpdated_tag,CCollider>() )
        {
            auto & C = get<CCollider>(e);
            auto shape = _createCollisionShape(C.colliderType);

            if( C.inheritScale && has<CScale>(e))
            {
               shape->setLocalScaling( _convert( get<CScale>(e)) );
            }

            // emplace or replace this so that
            // the next step can react to it
            emplace_or_replace<ShapeDataPrivate>(e, ShapeDataPrivate(shape));
            emplace_or_replace<checkCollisionObject_tag>(e);
        }
    }

    void _handleAllCollisionObjects()
    {
        for(auto e : view<checkCollisionObject_tag, CStaticBody, ShapeDataPrivate>() )
        {
            // it has CRigidBody but not the private data, so create the
            // private data.
            if( !has<RigidBodyDataPrivate>(e))
            {
                auto rb = _createRigidBody(e);

                RigidBodyDataPrivate rp;
                rp.body = rb;
                emplace<RigidBodyDataPrivate>(e, rp);
            }

            auto & R = get<RigidBodyDataPrivate>(e);
            auto & S = get<ShapeDataPrivate>(e);

            if( R.shape != S.shape )
            {
                btScalar mass = 0.0f;
                btVector3 inertia(0,0,0);

                R.body->setCollisionShape(S.shape.get());
                R.body->setMassProps(mass,inertia);
                R.shape = S.shape;

                m_dynamicsWorld->removeRigidBody(R.body);
                m_dynamicsWorld->addRigidBody(R.body);
            }
            remove_if_exists<checkCollisionObject_tag>(e);
        }


        for(auto e : view<checkCollisionObject_tag, CRigidBody, ShapeDataPrivate>() )
        {
            // it has CRigidBody but not the private data, so create the
            // private data.
            if( !has<RigidBodyDataPrivate>(e))
            {
                auto rb = _createRigidBody(e);

                RigidBodyDataPrivate rp;
                rp.body = rb;
                emplace<RigidBodyDataPrivate>(e, rp);
            }

            auto & R = get<RigidBodyDataPrivate>(e);
            auto & S = get<ShapeDataPrivate>(e);

            if( R.shape != S.shape )
            {
                btScalar mass = 0.0f;
                mass = get<CRigidBody>(e).mass;

                btVector3 inertia(0,0,0);
                S.shape->calculateLocalInertia(mass, inertia);
                R.body->setCollisionShape(S.shape.get());
                R.body->setMassProps(mass,inertia);
                R.shape = S.shape;

                m_dynamicsWorld->removeRigidBody(R.body);
                m_dynamicsWorld->addRigidBody(R.body);
            }
            remove_if_exists<checkCollisionObject_tag>(e);
        }

        for(auto e : view<checkCollisionObject_tag, CGhostBody, ShapeDataPrivate>() )
        {
            // it has CRigidBody but not the private data, so create the
            // private data.
            if( !has<GhostDataPrivate>(e))
            {
                auto rb = _createGhostObject(e);

                GhostDataPrivate rp;
                rp.body = rb;
                emplace<GhostDataPrivate>(e, rp);
            }

            auto & R = get<GhostDataPrivate>(e);
            auto & S = get<ShapeDataPrivate>(e);

            // the shape is different
            if( R.shape != S.shape )
            {
                R.body->setCollisionShape( S.shape.get() );
                R.shape = S.shape;
                m_dynamicsWorld->removeCollisionObject(R.body);
                m_dynamicsWorld->addCollisionObject(R.body);
            }

            remove_if_exists<checkCollisionObject_tag>(e);
        }
    }

    void _handleAllGhostTransforms()
    {
        for(auto e : m_worldTransformUpdated )
        {
            auto & C = get<GhostDataPrivate>(e);
            auto & R = get<CGlobalRotation>(e);
            auto & P = get<CGlobalPosition>(e);

            btTransform Tr( _convert(R) );
            Tr.setOrigin( _convert(P));
            C.body->setWorldTransform( Tr );
        }
        m_worldTransformUpdated.clear();
    }

    void onStop() override
    {
        on_destroy<GhostDataPrivate>().disconnect< &PhysicsObjectConstructionSystem::_onDestroyGhostBodyPrivate>(*this);
        on_destroy<RigidBodyDataPrivate>().disconnect< &PhysicsObjectConstructionSystem::_onDestroyRigidBodyPrivate>(*this);

        on_construct<CCollider>().disconnect<&entt::registry::emplace_or_replace<colliderUpdated_tag>>();
           on_update<CCollider>().disconnect<&entt::registry::emplace_or_replace<colliderUpdated_tag>>();

       on_construct<CRigidBody>().disconnect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();
          on_update<CRigidBody>().disconnect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();

      on_construct<CStaticBody>().disconnect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();
         on_update<CStaticBody>().disconnect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();

       on_construct<CGhostBody>().disconnect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();
          on_update<CGhostBody>().disconnect<&entt::registry::emplace_or_replace<checkCollisionObject_tag>>();;

    }

    void onUpdate() override
    {
        _handleAllGhostTransforms();
        _handleAllColliderUpdates();
        _handleAllCollisionObjects();
    }


    // create a rigidbody without a collision shape and
    btRigidBody* _createRigidBody(entity_type e)
    {
        myMotionState2* state = new myMotionState2( _convert( getSystemBus()->calculateWorldSpaceTransform(e) ) );
        state->m_entity   = e;
        state->m_registry = &getSystemBus()->registry;

        btScalar  mass=0.0f;
        if( has<CRigidBody>(e))
            mass = get<CRigidBody>(e).mass;

        btVector3 inertia(0,0,0);
        auto rigidBody = new btRigidBody( btRigidBody::btRigidBodyConstructionInfo(mass, state, nullptr, inertia ) );

        int entity_inte;
        std::memcpy( &entity_inte, &e, sizeof(entity_inte));
        rigidBody->setUserIndex3( entity_inte);

        return rigidBody;
    }

    btGhostObject* _createGhostObject(entity_type e)
    {
        auto g = new btGhostObject();
        g->setCollisionFlags(g->getCollisionFlags() | btCollisionObject::CF_NO_CONTACT_RESPONSE);

        int entity_inte;
        std::memcpy( &entity_inte, &e, sizeof(entity_inte));
        g->setUserIndex3( entity_inte);

        auto startT = _convert( getSystemBus()->calculateWorldSpaceTransform(e) );

        g->setWorldTransform(startT);

        return g;
    }

    void _onDestroyGhostBodyPrivate(registry_type & r, entity_type e)
    {
        auto & G = r.get<GhostDataPrivate>(e);
        if( G.body )
        {
            m_dynamicsWorld->removeCollisionObject(G.body);
            delete G.body;
            G.shape.reset();
            G.body = nullptr;
        }
    }
    void _onDestroyRigidBodyPrivate(registry_type & r, entity_type e)
    {
        auto & G = r.get<RigidBodyDataPrivate>(e);
        if( G.body )
        {
            m_dynamicsWorld->removeRigidBody(G.body);

            delete G.body->getMotionState();
            delete G.body;

            G.shape.reset();
            G.body = nullptr;
        }
    }

};


void PhysicsSystem2::onConstruct()
{
    getSystemBus()->registerSerializer<CRigidBody>();
    getSystemBus()->registerSerializer<CCollider>();
}

void PhysicsSystem2::onStart()
{
    auto sys = getSystemBus()->createSystem<PhysicsObjectConstructionSystem>();
    sys->m_dynamicsWorld = m_dynamicsWorld;
    m_objectConstructorSystem = sys;

    m_worldTransformUpdated.connect( getSystemBus()->registry,
                                     entt::collector.update<CWorldTransform>()
                                                      .where<PhysicsDataPrivate2>()
                                     );
    m_positionUpdatedObserver.connect(getSystemBus()->registry,
                                     entt::collector.update<CPosition>()
                                                      .where<RigidBodyDataPrivate>()
                                     );
    m_rotationUpdatedObserver.connect(getSystemBus()->registry,
                                     entt::collector.update<CRotation>()
                                                      .where<RigidBodyDataPrivate>()
                                     );

    S_INFO("PhysicsSystem Connected Successfully");
    m_objectConstructorSystem->start();
}

void PhysicsSystem2::onStop()
{
    m_objectConstructorSystem->stop();
    m_objectConstructorSystem.reset();

    clear<PhysicsDataPrivate2>();
    S_INFO("PhysicsSystem Disconnected Successfully");
}


void PhysicsSystem2::onUpdate()
{
    m_objectConstructorSystem->execute();

    // The position or rotation has been updated outside of
    // the physics system. so we need to flag the entities
    // and update their position in the physics system
    for(auto e : m_positionUpdatedObserver )
    {
        emplace_or_replace<tag_physics2_transform_modified_externally>(e);
    }
    for(auto e : m_rotationUpdatedObserver )
    {
        emplace_or_replace<tag_physics2_transform_modified_externally>(e);
    }


    EASY_BLOCK("Pre Transform RigidBody Update");
    // update the physics position of all entities that have been moved externally
    for(auto e : view<tag_physics2_transform_modified_externally, tag_is_root, RigidBodyDataPrivate>())
    {
        if( valid(e) )
        {
            auto & P = get<RigidBodyDataPrivate>(e);

            auto t = getSystemBus()->calculateWorldSpaceTransform(e);
            (void)P;
            (void)t;

            P.body->setCenterOfMassTransform( _convert(t));

            remove_if_exists<tag_physics2_transform_modified_externally>(e);
        }
    }
    EASY_END_BLOCK


    // Step the simulation
    // This will update all the rigidBody's internal
    EASY_BLOCK("Stepping Simulation");
    m_dynamicsWorld->stepSimulation( static_cast<btScalar>( DeltaTime() ), 1);
    EASY_END_BLOCK

    performFrustumCulling();

    if(m_debugDrawEnabled)
    {
        EASY_BLOCK("Debug Draw");
        debugDraw();
        EASY_END_BLOCK
    }

    EASY_BLOCK("Updating Transforms");
    _updateTransforms();
    EASY_END_BLOCK

    for(auto e : view<CGhostBody, GhostDataPrivate>())
    {
        #pragma message("To do: Figure out what to do about this. We need to somehow convey to the ghost object that entities have entered or exited")
        auto & G = get<GhostDataPrivate>(e);
        auto & overlappingPairs =G.body->getOverlappingPairs();

        (void)G;
        (void)overlappingPairs;
        //std::cout << overlappingPairs.size() << std::endl;
    }


    m_rotationUpdatedObserver.clear();
    m_positionUpdatedObserver.clear();


    getSystemBus()->variables.mouseEntityHit = entt::null;

    // Project the mouse ray into the physics scene
    // and figure out which collision object it hit
    {
        clear<tag_physics_MOUSE_INTERSECT>();


        auto p0 = getSystemBus()->variables.mouseRay.p;
        auto p1 = p0 + getSystemBus()->variables.mouseRay.v;

        btVector3 from(p0.x, p0.y, p0.z);//(-30, 1 + up, 0);
        btVector3 to  (p1.x, p1.y, p1.z);//(30, 1, 0);


        btCollisionWorld::ClosestRayResultCallback closestHitCallback(from, to);
        closestHitCallback.m_flags |= btTriangleRaycastCallback::kF_UseSubSimplexConvexCastRaytest;

        m_dynamicsWorld->rayTest(from, to, closestHitCallback);

        entt::entity e = entt::null;
        if( closestHitCallback.hasHit() )
        {
            auto rb = closestHitCallback.m_collisionObject;

            if( rb )
            {
                auto entity_i = rb->getUserIndex3();
                std::memcpy(&e, &entity_i, sizeof(e));
                getSystemBus()->variables.mouseWorldHitPosition = _convert(closestHitCallback.m_hitPointWorld);
                getSystemBus()->variables.mouseWorldHitNormal   = _convert(closestHitCallback.m_hitNormalWorld);
                getSystemBus()->variables.mouseEntityHit        = e;
                emplace<tag_physics_MOUSE_INTERSECT>(e);
            }
        }
    }
}


void PhysicsSystem2::_updateTransforms()
{
}

PhysicsSystem2::PhysicsSystem2()
{
    m_collisionConfiguration = new btDefaultCollisionConfiguration();

    ///use the default collision dispatcher. For parallel processing you can use a diffent dispatcher (see Extras/BulletMultiThreaded)
    m_dispatcher = new btCollisionDispatcher(m_collisionConfiguration);

    ///btDbvtBroadphase is a good general purpose broadphase. You can also try out btAxis3Sweep.
    m_overlappingPairCache = new btDbvtBroadphase();


    ///the default constraint solver. For parallel processing you can use a different solver (see Extras/BulletMultiThreaded)
    m_solver = new btSequentialImpulseConstraintSolver;

    m_dynamicsWorld = new btDiscreteDynamicsWorld(m_dispatcher, m_overlappingPairCache, m_solver, m_collisionConfiguration);

    m_dynamicsWorld->setGravity(btVector3(0, -9.8f, 0));

    m_dynamicsWorld->getPairCache()->setInternalGhostPairCallback( new btGhostPairCallback() );//->setOverlapFilterCallback(filterCallback);
}

PhysicsSystem2::~PhysicsSystem2()
{
    //remove the rigidbodies from the dynamics world that didn't get
    //deleted.
    int i;
    for (i = m_dynamicsWorld->getNumCollisionObjects() - 1; i >= 0; i--)
    {
        btCollisionObject* obj = m_dynamicsWorld->getCollisionObjectArray()[i];
        btRigidBody* body = btRigidBody::upcast(obj);
        if (body && body->getMotionState())
        {
            delete body->getMotionState();
        }
        m_dynamicsWorld->removeCollisionObject(obj);
        delete obj;
    }

    //delete dynamics world
    delete m_dynamicsWorld;

    //delete solver
    delete m_solver;

    //delete broadphase
    delete m_overlappingPairCache;

    //delete dispatcher
    delete m_dispatcher;

    delete m_collisionConfiguration;
}

#if 0
// Needed for occlusion culling only, passed to g_DBFC
// This is actually a small "Sofware Rasterizer", that draws to and performs queries on the occlusion buffer
struct OcclusionBuffer	{
    // 95% of the code is copied from CDTestFramework, but in this demo only a few of the appendOccluder/queryOccluder overloads are used
    struct WriteOCL
    {
        SIMD_FORCE_INLINE static bool Process(btScalar& q,btScalar v) { if(q<v) q=v;return(false); }
    };
    struct QueryOCL
    {
        SIMD_FORCE_INLINE static bool Process(btScalar& q,btScalar v) { return(q<=v); }
    };
    btAlignedObjectArray<btScalar>	buffer;
    int								sizes[2];
    btScalar						scales[2];
    btScalar						offsets[2];

    btScalar						wtrs[16];

    btVector3						eye;
    btVector3						neardist;
    btScalar						ocarea;
    btScalar						qrarea;
    uint32_t							texture;

    OcclusionBuffer()	{
        neardist=btVector3(2,2,2);
        ocarea=(btScalar)0;
        qrarea=(btScalar)0;
        texture = 0;
    }



    void		clear()
    {
        buffer.resize(0);
        buffer.resize(sizes[0]*sizes[1],0);
    }

    void reset()
    {
        if (texture>0) {
          //  glDeleteTextures(1,&texture);
            texture=0;
        }
    }
#if 0
    void		drawBuffer(	btScalar l,btScalar t,
                            btScalar r,btScalar b)
    {
        btAlignedObjectArray<GLubyte>	data;
        data.resize(buffer.size());
        // Very slow, but good quality
        btScalar bufferMax=data[0],bufferMin=data[0],bufferExtentFactor;
        for(int i=0;i<buffer.size();++i)	{
            const btScalar d = buffer[i];
            if (bufferMin > d)	bufferMin = d;
            else if (bufferMax < d) bufferMax = d;
        }
        bufferExtentFactor = (bufferMax==bufferMin ? 0 : btScalar(255.0) /(bufferMax - bufferMin));
        for(int i=0;i<data.size();++i)	{
            data[i] = (GLubyte)((buffer[i]-bufferMin)*bufferExtentFactor);
        }
        //
        glBindTexture(GL_TEXTURE_2D,texture);
        //glDisable(GL_BLEND);
        //glEnable(GL_TEXTURE_2D);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D,0,GL_LUMINANCE,sizes[0],sizes[1],0,GL_LUMINANCE,GL_UNSIGNED_BYTE,&data[0]);
        glBegin(GL_QUADS);
        glColor4ub(255,255,255,255);
        glNormal3f(0,0,1);
        glTexCoord2f(0,0);glVertex3f(l,b,0);
        glTexCoord2f(1,0);glVertex3f(r,b,0);
        glTexCoord2f(1,1);glVertex3f(r,t,0);
        glTexCoord2f(0,1);glVertex3f(l,t,0);
        glEnd();
        //glDisable(GL_TEXTURE_2D);
    }
#endif
    btVector4	transform(const btVector3& x) const
    {
        btVector4	t;
        t[0]	=	x[0]*wtrs[0]+x[1]*wtrs[4]+x[2]*wtrs[8]+wtrs[12];
        t[1]	=	x[0]*wtrs[1]+x[1]*wtrs[5]+x[2]*wtrs[9]+wtrs[13];
        t[2]	=	x[0]*wtrs[2]+x[1]*wtrs[6]+x[2]*wtrs[10]+wtrs[14];
        t[3]	=	x[0]*wtrs[3]+x[1]*wtrs[7]+x[2]*wtrs[11]+wtrs[15];
        return(t);
    }
    static bool	project(btVector4* p,int n)
    {
        for(int i=0;i<n;++i)
        {
            const btScalar	iw=1/p[i][3];
            p[i][2]=1/p[i][3];
            p[i][0]*=p[i][2];
            p[i][1]*=p[i][2];
            (void)iw;
        }
        return(true);
    }
    template <const int NP>
    static int	clip(const btVector4* pi,btVector4* po)
    {
        btScalar	s[ static_cast<size_t>(NP) ];
        int			m=0;
        for(int i=0;i<NP;++i)
        {
            s[i]=pi[i][2]+pi[i][3];
            if(s[i]<0) m+=1<<i;
        }
        if(m==((1<<NP)-1)) return(0);
        if(m!=0)
        {
            int n=0;
            for(int i=NP-1,j=0;j<NP;i=j++)
            {
                const btVector4&	a=pi[i];
                const btVector4&	b=pi[j];
                const btScalar		t=s[i]/(a[3]+a[2]-b[3]-b[2]);
                if((t>0)&&(t<1))
                {
                    po[n][0]	=	a[0]+(b[0]-a[0])*t;
                    po[n][1]	=	a[1]+(b[1]-a[1])*t;
                    po[n][2]	=	a[2]+(b[2]-a[2])*t;
                    po[n][3]	=	a[3]+(b[3]-a[3])*t;
                    ++n;
                }
                if(s[j]>0) po[n++]=b;
            }
            return(n);
        }
        for(int i=0;i<NP;++i) po[i]=pi[i];
        return(NP);
    }
    template <typename POLICY>
    inline bool	draw(	const btVector4& a,
                        const btVector4& b,
                        const btVector4& c,
                        const btScalar minarea)
    {
        const btScalar		a2=((b-a).cross(c-a))[2];
        if(a2>0)
        {
            if(a2<minarea)	return(true);
            const int		x[]={	(int)(a.x()*scales[0]+offsets[0]),
                                    (int)(b.x()*scales[0]+offsets[0]),
                                    (int)(c.x()*scales[0]+offsets[0])};
            const int		y[]={	(int)(a.y()*scales[1]+offsets[1]),
                                    (int)(b.y()*scales[1]+offsets[1]),
                                    (int)(c.y()*scales[1]+offsets[1])};
            const btScalar	z[]={	a.z(),b.z(),c.z()};
            const int		mix=btMax(0,btMin(x[0],btMin(x[1],x[2])));
            const int		mxx=btMin(sizes[0],1+btMax(x[0],btMax(x[1],x[2])));
            const int		miy=btMax(0,btMin(y[0],btMin(y[1],y[2])));
            const int		mxy=btMin(sizes[1],1+btMax(y[0],btMax(y[1],y[2])));
            const int		width=mxx-mix;
            const int		height=mxy-miy;
            if((width*height)>0)
            {
                const int		dx[]={	y[0]-y[1],
                                        y[1]-y[2],
                                        y[2]-y[0]};
                const int		dy[]={	x[1]-x[0]-dx[0]*width,
                                        x[2]-x[1]-dx[1]*width,
                                        x[0]-x[2]-dx[2]*width};
                const int		a=x[2]*y[0]+x[0]*y[1]-x[2]*y[1]-x[0]*y[2]+x[1]*y[2]-x[1]*y[0];
                const btScalar	ia=1/(btScalar)a;
                const btScalar	dzx=ia*(y[2]*(z[1]-z[0])+y[1]*(z[0]-z[2])+y[0]*(z[2]-z[1]));
                const btScalar	dzy=ia*(x[2]*(z[0]-z[1])+x[0]*(z[1]-z[2])+x[1]*(z[2]-z[0]))-(dzx*width);
                int				c[]={	miy*x[1]+mix*y[0]-x[1]*y[0]-mix*y[1]+x[0]*y[1]-miy*x[0],
                                        miy*x[2]+mix*y[1]-x[2]*y[1]-mix*y[2]+x[1]*y[2]-miy*x[1],
                                        miy*x[0]+mix*y[2]-x[0]*y[2]-mix*y[0]+x[2]*y[0]-miy*x[2]};
                btScalar		v=ia*((z[2]*c[0])+(z[0]*c[1])+(z[1]*c[2]));
                btScalar*		scan=&buffer[miy*sizes[1]];
                for(int iy=miy;iy<mxy;++iy)
                {
                    for(int ix=mix;ix<mxx;++ix)
                    {
                        if((c[0]>=0)&&(c[1]>=0)&&(c[2]>=0))
                        {
                            if(POLICY::Process(scan[ix],v)) return(true);
                        }
                        c[0]+=dx[0];c[1]+=dx[1];c[2]+=dx[2];v+=dzx;
                    }
                    c[0]+=dy[0];c[1]+=dy[1];c[2]+=dy[2];v+=dzy;
                    scan+=sizes[0];
                }
            }
        }
        return(false);
    }

    template <const int NP,typename POLICY>
    inline bool	clipDraw(	const btVector4* p,
                            btScalar minarea)
    {
        btVector4	o[NP*2];
        const int	n=clip<NP>(p,o);
        bool		earlyexit=false;
        project(o,n);
        for(int i=2;i<n;++i)
        {
            earlyexit|=draw<POLICY>(o[0],o[i-1],o[i],minarea);
        }
        return(earlyexit);
    }
    void		appendOccluder(	const btVector3& a,
                                const btVector3& b,
                                const btVector3& c)
    {
        const btVector4	p[]={transform(a),transform(b),transform(c)};
        clipDraw<3,WriteOCL>(p,ocarea);
    }
    void		appendOccluder(	const btVector3& a,
                                const btVector3& b,
                                const btVector3& c,
                                const btVector3& d)
    {
        const btVector4	p[]={transform(a),transform(b),transform(c),transform(d)};
        clipDraw<4,WriteOCL>(p,ocarea);
    }
    void		appendOccluder( const btVector3& c,
                                const btVector3& e)	// in this class 'e' seems to actually be a half extent...
    {
        const btVector4	x[]={	transform(btVector3(c[0]-e[0],c[1]-e[1],c[2]-e[2])),
                                transform(btVector3(c[0]+e[0],c[1]-e[1],c[2]-e[2])),
                                transform(btVector3(c[0]+e[0],c[1]+e[1],c[2]-e[2])),
                                transform(btVector3(c[0]-e[0],c[1]+e[1],c[2]-e[2])),
                                transform(btVector3(c[0]-e[0],c[1]-e[1],c[2]+e[2])),
                                transform(btVector3(c[0]+e[0],c[1]-e[1],c[2]+e[2])),
                                transform(btVector3(c[0]+e[0],c[1]+e[1],c[2]+e[2])),
                                transform(btVector3(c[0]-e[0],c[1]+e[1],c[2]+e[2]))};
        static const int	d[]={	1,0,3,2,
                                    4,5,6,7,
                                    4,7,3,0,
                                    6,5,1,2,
                                    7,6,2,3,
                                    5,4,0,1};
        for(size_t i=0;i<(sizeof(d)/sizeof(d[0]));)
        {
            const btVector4	p[]={	x[d[i++]],
                                    x[d[i++]],
                                    x[d[i++]],
                                    x[d[i++]]};
            clipDraw<4,WriteOCL>(p,ocarea);
        }
    }
    // New:
    void		appendOccluder( const btVector3& occluderInnerBoxCollisionShapeHalfExtent,
                                const btTransform& collisionObjectWorldTransform
                                )
    {
        const btVector3 c(collisionObjectWorldTransform.getOrigin());
        const btVector3& e = occluderInnerBoxCollisionShapeHalfExtent;
        const btMatrix3x3& basis = collisionObjectWorldTransform.getBasis();
        const btVector4	x[]={	transform(c+basis*btVector3(-e[0],-e[1],-e[2])),
                                transform(c+basis*btVector3(+e[0],-e[1],-e[2])),
                                transform(c+basis*btVector3(+e[0],+e[1],-e[2])),
                                transform(c+basis*btVector3(-e[0],+e[1],-e[2])),
                                transform(c+basis*btVector3(-e[0],-e[1],+e[2])),
                                transform(c+basis*btVector3(+e[0],-e[1],+e[2])),
                                transform(c+basis*btVector3(+e[0],+e[1],+e[2])),
                                transform(c+basis*btVector3(-e[0],+e[1],+e[2]))};
        static const int	d[]={	1,0,3,2,
                                    4,5,6,7,
                                    4,7,3,0,
                                    6,5,1,2,
                                    7,6,2,3,
                                    5,4,0,1};
        for(size_t i=0;i<(sizeof(d)/sizeof(d[0]));)
        {
            const btVector4	p[]={	x[d[i++]],
                                    x[d[i++]],
                                    x[d[i++]],
                                    x[d[i++]]};
            clipDraw<4,WriteOCL>(p,ocarea);
        }
    }
    inline bool	queryOccluder(	const btVector3& a,
                                const btVector3& b,
                                const btVector3& c)
    {
        const btVector4	p[]={transform(a),transform(b),transform(c)};
        return(clipDraw<3,QueryOCL>(p,qrarea));
    }
    inline bool	queryOccluder(	const btVector3& a,
                                const btVector3& b,
                                const btVector3& c,
                                const btVector3& d)
    {
        const btVector4	p[]={transform(a),transform(b),transform(c),transform(d)};
        return(clipDraw<4,QueryOCL>(p,qrarea));
    }
    inline bool	queryOccluder(	const btVector3& c,
                                const btVector3& e)
    {
        const btVector4	x[]={	transform(btVector3(c[0]-e[0],c[1]-e[1],c[2]-e[2])),
                                transform(btVector3(c[0]+e[0],c[1]-e[1],c[2]-e[2])),
                                transform(btVector3(c[0]+e[0],c[1]+e[1],c[2]-e[2])),
                                transform(btVector3(c[0]-e[0],c[1]+e[1],c[2]-e[2])),
                                transform(btVector3(c[0]-e[0],c[1]-e[1],c[2]+e[2])),
                                transform(btVector3(c[0]+e[0],c[1]-e[1],c[2]+e[2])),
                                transform(btVector3(c[0]+e[0],c[1]+e[1],c[2]+e[2])),
                                transform(btVector3(c[0]-e[0],c[1]+e[1],c[2]+e[2]))};
        for(int i=0;i<8;++i)
        {
            if((x[i][2]+x[i][3])<=0) return(true);
        }
        static const int	d[]={	1,0,3,2,
                                    4,5,6,7,
                                    4,7,3,0,
                                    6,5,1,2,
                                    7,6,2,3,
                                    5,4,0,1};
        for(size_t i=0;i<(sizeof(d)/sizeof(d[0]));)
        {
            const btVector4	p[]={	x[d[i++]],
                                    x[d[i++]],
                                    x[d[i++]],
                                    x[d[i++]]};
            if(clipDraw<4,QueryOCL>(p,qrarea)) return(true);
        }
        return(false);
    }

} g_occlusionBuffer;
#endif


struct	DbvtBroadphaseFrustumCulling : public btDbvt::ICollide
{
    btAlignedObjectArray<btCollisionObject*>* m_pCollisionObjectArray;
          int m_collisionFilterMask;
    btCollisionObject* m_additionalCollisionObjectToExclude;	// Unused in this demo
 //   OcclusionBuffer* m_ocb;
    void* m_ocb=nullptr;

    DbvtBroadphaseFrustumCulling(btAlignedObjectArray<btCollisionObject*>* _pArray=NULL)
        : 	m_pCollisionObjectArray(_pArray),
            m_collisionFilterMask(btBroadphaseProxy::AllFilter & ~btBroadphaseProxy::SensorTrigger),
            m_additionalCollisionObjectToExclude(NULL),
            m_ocb(NULL)
    {
    }

    //void	Process(const btDbvtNode* node,btScalar depth) override
    //{
    //    (void)depth;
    //    Process(node);
    //}
    using btDbvt::ICollide::Process;
    void Process(const btDbvtNode* leaf) override
    {
        btBroadphaseProxy*	proxy = static_cast < btBroadphaseProxy*> (leaf->data);
        btCollisionObject* co     = static_cast < btCollisionObject* > (proxy->m_clientObject);

//        if (m_ocb && IsOccluder(co) && co->getCollisionShape())
//        {
//            static btVector3 aabbMin;
//            static btVector3 aabbMax;
//            co->getCollisionShape()->getAabb(btTransform::getIdentity(),aabbMin,aabbMax);	// Actually here I should get the MINIMAL aabb that can be nested INSIDE the shape (ie. only btBoxShapes work)
//            m_ocb->appendOccluder((aabbMax-aabbMin)*btScalar(0.5f),co->getWorldTransform());	// Note that only a btVector3 (the inner box shape half extent) seems to be necessary here.
//        }

        if ((proxy->m_collisionFilterGroup & m_collisionFilterMask) != 0 && co != m_additionalCollisionObjectToExclude)
        {
            //m_pCollisionObjectArray->push_back( co );
        }
        m_pCollisionObjectArray->push_back( co );
    }

    inline bool IsOccluder(const btCollisionObject* o)
    {
        (void)o;
        return false;
        //return (o && (o->getCollisionFlags()&CF_OCCLUDER_OBJECT));
    }

}	g_DBFC;


void PhysicsSystem2::performFrustumCulling()
{
    EASY_FUNCTION(profiler::colors::Blue); // Magenta block with name "foo"
    btVector3 m_cameraPosition;
    btVector3 m_cameraTargetPosition; // what is it looking at?
    glm::mat4 m_modelviewMatrix;
    glm::mat4 m_projectionMatrix;


    btAlignedObjectArray<btCollisionObject*> m_objectsInFrustum;

    m_objectsInFrustum.resize(0);	// clear() is probably slower

    if (!m_dynamicsWorld)
        return;

    btDbvtBroadphase*	dbvtBroadphase = dynamic_cast <btDbvtBroadphase*> (m_dynamicsWorld->getBroadphase());


    // to fill out
    //float     m_frustumZFar;
    bool      m_frustumCullingEnabled = true;
    bool      m_occlusionCullingEnabled = false;

    auto _v = view<CCamera,CPosition,CRotation>();

    vka::Transform cameraTransform;
    vka::frustum F;
    for(auto a : _v)
    {
        auto & C = _v.get<CCamera>(a);
        auto & P = _v.get<CPosition>(a);
        auto & Q = _v.get<CRotation>(a);

        vka::Transform T;
        T.position = P;
        T.rotation = Q;

        m_modelviewMatrix  = T.getViewMatrix();
        m_projectionMatrix = C.getProjectionMatrix( getSystemBus()->variables.screenSize, true );

        cameraTransform.position = P;
        cameraTransform.rotation = Q;

        F = vka::frustum(m_projectionMatrix);
        F.transform( T.getMatrix() );
        break;
    }

    btTransform T;


    int m_DbvtCullingCollisionFilterMask = btBroadphaseProxy::AllFilter & ~btBroadphaseProxy::SensorTrigger;

    if (!dbvtBroadphase)
    {
        m_frustumCullingEnabled = m_occlusionCullingEnabled = false;
    }

    if (m_frustumCullingEnabled || m_occlusionCullingEnabled)
    {
        // Perform Dbvt Frustum Culling
        // Mapping stuff:---------------------------------------------------------------------------
        //const btVector3& eye = m_cameraPosition;//cameraT.getOrigin();//(GetCameraPos().x,GetCameraPos().y,GetCameraPos().z);
        //const btVector3	dir  = btVector3(m_cameraTargetPosition - m_cameraPosition).normalized();//cameraT.getBasis().getColumn(2);//(GetCameraDir().x,GetCameraDir().y,GetCameraDir().z);
        /*
        DemoApplication::
        float m_ele;
        float m_azi;
        btVector3 m_cameraPosition;
        btVector3 m_cameraTargetPosition;//look at
        float m_frustumZNear,m_frustumZFar
        */
   //     const float frustumFar = m_frustumZFar;

#define AVOID_GLGET_CALLS_EVERY_FRAME
        // At this point we must have the modelview matrix and the project matrix:
        #ifndef AVOID_GLGET_CALLS_EVERY_FRAME
            // SAFE AND VERY SLOW PART -----------------------
            // These are far too slow (easily more than 6 times the rest of the code in most cases) and can be easily avioded!
            // As it is (with these lines), a ghost object based frustum culling approach can even be faster!
            #ifdef BT_USE_DOUBLE_PRECISION
                glGetDoublev(GL_MODELVIEW_MATRIX, m_modelviewMatrix);
                glGetDoublev(GL_PROJECTION_MATRIX, m_projectionMatrix);
            #else //BT_USE_DOUBLE_PRECISION
                glGetFloatv(GL_MODELVIEW_MATRIX, m_modelviewMatrix);
                glGetFloatv(GL_PROJECTION_MATRIX, m_projectionMatrix);
            #endif //BT_USE_DOUBLE_PRECISION
            //-------------------------------------------------
        #else //AVOID_GLGET_CALLS_EVERY_FRAME
            // FAST WAY (a bit more difficult to understand)---

         //   GetLookAtMatrix(m_cameraPosition[0], m_cameraPosition[1], m_cameraPosition[2],
         //               m_cameraTargetPosition[0], m_cameraTargetPosition[1], m_cameraTargetPosition[2],
         //               m_cameraUp.getX(),m_cameraUp.getY(),m_cameraUp.getZ(),
         //               m_modelviewMatrix
         //               );
            // When a btTransform is directly used as a camera transform: cameraT.inverse().getOpenGLMatrix(m_modelviewMatrix) should work, and it's very fast (inverse = transpose)
            // We have already a perspective projection matrix (see the reshape method above). Note that ortho shouldn't work with this optimization.
        #endif //AVOID_GLGET_CALLS_EVERY_FRAME
        // End mapping stuff--------------------------------------------------------------------------

        // Storage stuff------------------
        btVector3 planes_n[5];
        btScalar  planes_o[5];
//        const btScalar farplane = frustumFar;
//        static const int nplanes=sizeof(planes_n)/sizeof(planes_n[0]);
        static const bool cullFarPlane = true;//false;	// This can be tweaked.
        int	acplanes= cullFarPlane?5:4;
       // btScalar mmp[16];	// mv*p
        // End storage stuff---------------

        // Frustum plane extraction--------
        {
            // This is shorter and cleaner than the approach used in the CDTestFramework
            // To better understand this approach please browse the web for a pdf named: "Fast Extraction of Viewing Frustum Planes from the World-View-Projection Matrix".

            //Matrix16Multiply(&m_modelviewMatrix[0],&m_projectionMatrix[0],mmp);
            auto mmp_m = m_modelviewMatrix * m_projectionMatrix;
            mmp_m = glm::inverse(mmp_m);
            float * mmp = &mmp_m[0][0];
            // Now we axtract the planes from mmp:

            // Extract the RIGHT clipping plane
            planes_n[0]	=	btVector3((btScalar)(mmp[ 3]-mmp[ 0]),(btScalar)(mmp[ 7]-mmp[ 4]),(btScalar)(mmp[11]-mmp[ 8]));
            // Extract the LEFT clipping plane
            planes_n[1]	=	btVector3((btScalar)(mmp[ 3]+mmp[ 0]),(btScalar)(mmp[ 7]+mmp[ 4]),(btScalar)(mmp[11]+mmp[ 8]));
            // Extract the TOP clipping plane
            planes_n[2]	=	btVector3((btScalar)(mmp[ 3]-mmp[ 1]),(btScalar)(mmp[ 7]-mmp[ 5]),(btScalar)(mmp[11]-mmp[9]));
            // Extract the BOTTOM clipping plane
            planes_n[3]	=	btVector3((btScalar)(mmp[ 3]+mmp[ 1]),(btScalar)(mmp[ 7]+mmp[ 5]),(btScalar)(mmp[11]+mmp[9]));
            // Extract the FAR clipping plane
            planes_n[4]	=	btVector3((btScalar)(mmp[ 3]-mmp[ 2]),(btScalar)(mmp[ 7]-mmp[ 6]),(btScalar)(mmp[11]-mmp[10]));
            //planes_n[4]	=	-dir;	//Should work well too... (don't know without normalizations... better stay coherent)

            // Extract the RIGHT clipping plane
            planes_o[0] = (btScalar)(mmp[15]-mmp[12]);
            // Extract the LEFT clipping plane
            planes_o[1] = (btScalar)(mmp[15]+mmp[12]);
            // Extract the TOP clipping plane
            planes_o[2] = (btScalar)(mmp[15]-mmp[13]);
            // Extract the BOTTOM clipping plane
            planes_o[3] = (btScalar)(mmp[15]+mmp[13]);
            // Extract the FAR clipping plane
            planes_o[4] = (btScalar)(mmp[15]-mmp[14]);

         //   planes_o[0]F.right.p.x,F.right.p.y,F.right.p.z
            /*
                    Compared to the pdf paper named: "Fast Extraction of Viewing Frustum Planes from the World-View-Projection Matrix":
                    Here, for example, the first coefficient of the RIGHT plane is (mmp[ 3]-mmp[ 0]), that in the paper is referenced as (m41-m11), since my matrices are stored in column major order and their index range is in [0,3].
                    Here, for example, the offset for the right plane is (mmp[15]-mmp[12]), and in the paper is (m44-m14).
                    Here the plane equations are in the form: planes_n[i].x()*x + planes_n[i].y()*y + planes_n[i].z()*z + planes_o[i] = 0
                    According to the paper, plane normalization isn't needed if we just want to test if a point is inside or outside the plane, so we don't normalize them.
                    Good paper, after all. The main drawback is that the matrix elements here have a (very) different convention. It should be handy to fully rewrite it with this convention...
            */
        }
        // End Frustum plane extraction----

        //=======================================================
        // OK, now the pure btDbvt code starts here:
        //=======================================================

        acplanes = 4;
        auto p0 = F.getRight();
        planes_n[0] = _convert(p0.n);
        planes_o[0] = p0.d;//glm::dot(F.right.v, cameraTransform.position);

        p0 = F.getLeft();
        planes_n[1] = _convert(p0.n);
        planes_o[1] = p0.d;//glm::dot(F.right.v, cameraTransform.position);

        p0 = F.getTop();
        planes_n[2] = _convert(p0.n);
        planes_o[2] = p0.d;//glm::dot(F.right.v, cameraTransform.position);

        p0 = F.getBottom();
        planes_n[3] = _convert(p0.n);
        planes_o[3] = p0.d;//glm::dot(F.right.v, cameraTransform.position);

        g_DBFC.m_pCollisionObjectArray = &m_objectsInFrustum;
        g_DBFC.m_collisionFilterMask   = m_DbvtCullingCollisionFilterMask;//btBroadphaseProxy::AllFilter & ~btBroadphaseProxy::SensorTrigger;	// This won't display sensors...
        g_DBFC.m_additionalCollisionObjectToExclude = NULL;//btCamera;

        g_DBFC.m_ocb = NULL;
        if (m_occlusionCullingEnabled)
        {
#if 0
            g_occlusionBuffer.initialize(mmp,m_occlusionBufferWidth,m_occlusionBufferHeight);
            g_occlusionBuffer.eye=eye;
            g_DBFC.m_ocb = &g_occlusionBuffer;
            btDbvt::collideOCL(dbvtBroadphase->m_sets[1].m_root,planes_n,planes_o,dir,acplanes,g_DBFC);
            btDbvt::collideOCL(dbvtBroadphase->m_sets[0].m_root,planes_n,planes_o,dir,acplanes,g_DBFC);
            // btDbvt::collideOCL(root,normals,offsets,sortaxis,count,icollide):
            // same of btDbvt::collideKDOP but with leaves sorted (min/max) along 'sortaxis'.
#endif
        }
        else	{
            g_DBFC.m_ocb = NULL;
            btDbvt::collideKDOP(dbvtBroadphase->m_sets[1].m_root,planes_n,planes_o,acplanes,g_DBFC);
            btDbvt::collideKDOP(dbvtBroadphase->m_sets[0].m_root,planes_n,planes_o,acplanes,g_DBFC);
            // btDbvt::collideKDOP(root,normals,offsets,count,icollide):
            // traverse the tree and call ICollide::Process(node) for all leaves located inside/on a set of planes.
        }

        auto count = m_objectsInFrustum.size();
        entt::entity e;
        for(int i=0;i<count;i++)
        {
            auto entity_i = m_objectsInFrustum[i]->getUserIndex3();
            std::memcpy(&e, &entity_i, sizeof(e));
            if( valid(e) )
                emplace_or_replace<tag_render_in_frustum>(e);
        }
        reportStat("physics_objects_in_frustum ",        count );
        //std::cout << "Objects in Frustum: " << m_objectsInFrustum.size() << std::endl;
        // End Perform Frustum Culling
    }

}



void PhysicsSystem2::onBuildComponents(entt::entity e, const nlohmann::json &components)
{
    if( components.count("CCollider") )
    {
        auto & J = components.at("CCollider");

        if( J.is_object() )
        {
            if( J.count("colliderType"))
            {
                auto & shJ = J.at("colliderType");

                if( shJ == "SphereCollider")
                {
                    auto r = J.at("radius").get<float>();
                    SphereCollider B;
                    B.radius = r;

                    emplace_or_replace<CCollider>(e, B);
                }
                else if( shJ == "BoxCollider")
                {
                    auto x = J.at("x").get<float>();
                    auto y = J.at("y").get<float>();
                    auto z = J.at("z").get<float>();

                    BoxCollider B;
                    B.x = x;
                    B.y = y;
                    B.z = z;

                    emplace_or_replace<CCollider>(e, B);
                }

            }
        }

    }
}


}

}






