#ifndef VKA_ECS_SDL_WINDOW_SYSTEM_H
#define VKA_ECS_SDL_WINDOW_SYSTEM_H

#include <vka/ecs/SystemBase2.h>
#include <vkw/SDLWidget.h>
#include <map>
#include <list>
#include <set>


namespace vka
{
namespace ecs
{

/**
 * @brief The RenderSystem3 class
 *
 * This is a re-imagined version of the rendersystem./
 *
 * This system will ONLY perform rendering. It will not perform
 * frustum culling, or managing of entities.
 *
 * This system only render MaterialPrimitiveComponents
 * and MeshComponents.
 */
class SDLWindowSystem : public vka::ecs::SystemBaseInternal
{
public:

    SDLWindowSystem()
    {

    }
    ~SDLWindowSystem()
    {

    }

    int mainLoop();

    //==========================================================================
    // From SystemBase
    //==========================================================================
    void onStart()    override;
    void onStop() override;
    void onUpdate() override;

    std::string name() const override
    {
        return "SDLWindowSystem";
    }
};



}
}

#endif


