#include <vka/ecs/SDLWindowSystem.h>
#include <vka/ecs/Events/InputEvents.h>
#include <vka/ecs/RenderSystem3.h>

#include <SDL2/SDL.h>
#include <vkw/SDLVulkanWindow.h>
#include <vkw/SDLWidget.h>

#include <vkff/framegraph.h>

#include <glslang/Public/ShaderLang.h>

namespace vka
{

namespace ecs
{


struct SDLApplication : public vkw::Application
{
    std::shared_ptr<vka::ecs::SystemBus> m_systemBus;

    auto getSystemBus()
    {
        return m_systemBus;
    }
    // Application interface
    void initResources() override;
    void releaseResources()  override;
    void initSwapChainResources()  override;
    void releaseSwapChainResources()  override;
    void render(vkw::Frame &frame)  override;
};

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;
    #define VKA_INFO(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_WARN(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_ERROR(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_DEBUG(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}





void SDLApplication::initResources()
{
    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    assert(rs);
    vka::ecs::RenderSystem3CreateInfo ii;
    ii.device         = getDevice();
    ii.physicalDevice = getPhysicalDevice();
    ii.instance       = getInstance();

    rs->init(ii);
}

void SDLApplication::releaseResources()
{
    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    assert(rs);
    rs->releaseResources();
}

void SDLApplication::initSwapChainResources()
{
    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    assert(rs);
    rs->initSwapchainResources(swapchainImageSize(),
                               concurrentFrameCount(),
                               getDefaultRenderPass());
}

void SDLApplication::releaseSwapChainResources()
{
    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    assert(rs);
    rs->releaseSwapchainResources();
}

void SDLApplication::render(vkw::Frame &frame)
{
    vkff::FrameGraphExecuteInfo info;
    info.commandBuffer        = frame.commandBuffer;
    info.swapchainImage       = frame.swapchainImage;
    info.swapchainExtent      = frame.swapchainSize;
    info.swapchainImageView   = frame.swapchainImageView;
    info.swapchainFrameIndex  = frame.swapchainIndex;
    info.swapchainFormat      = vk::Format(frame.swapchainFormat);
    info.swapchainRenderPass  = frame.renderPass;
    info.swapchainFramebuffer = frame.framebuffer;
    info.swapchainDepthFormat = vk::Format(frame.depthFormat);

    double dt = 0.016;//frameTime();
    getSystemBus()->step(dt);

    auto rs = getSystemBus()->getOrCreateSystem2<vka::ecs::RenderSystem3>();
    if( rs )
    {
        // if the rendersystem exists
        // then pass it to the rendersystem
        rs->render(info);
    }
    else
    {
        // otherwise do basic pass
        frame.beginRenderPass( frame.commandBuffer );
        frame.endRenderPass(frame.commandBuffer);
    }
    requestNextFrame();
}


void SDLWindowSystem::onStart()
{

}

void SDLWindowSystem::onStop()
{
}

void SDLWindowSystem::onUpdate()
{
}

int SDLWindowSystem::mainLoop()
{
    glslang::InitializeProcess();

    // This needs to be called first to initialize SDL
    SDL_Init(SDL_INIT_EVERYTHING);

    // set the initial properties of the
    // window. Also specify that we want
    // a depth stencil attachment
    vkw::SDLVulkanWidget::CreateInfo c;
    c.width       = 1024;
    c.height      = 768;
    c.windowTitle = "My Vulkan Application Window";
    c.depthFormat = VK_FORMAT_D32_SFLOAT_S8_UINT;
    c.callback    = &debugCallback;

    c.enabledFeatures.fillModeNonSolid = true;
    vkw::SDLVulkanWidget widget;
    widget.create(c);


    SDLApplication app;
    app.m_systemBus = getSystemBus();
    widget.exec(&app,
                      [&app,this](SDL_Event const & event)
    {
        using namespace vka;
        switch (event.type)
        {
            case SDL_QUIT : // User pressed the x button.
            {
                app.quit();
                break;
            }
            case SDL_DROPFILE:
            {
                if( event.drop.file )
                {
                    EvtInputFileDrop e;
                    e.path = event.drop.file;
                    SDL_free(event.drop.file);
                    getSystemBus()->triggerEvent(e);
                }
                break;
            }
            case SDL_KEYUP:
            {
                EvtInputKey E;
                E.down     = false;
                E.repeat   = event.key.repeat;
                E.keycode  = static_cast<vka::KeyCode>(event.key.keysym.sym);
                E.timestamp = std::chrono::system_clock::now();

                E.windowKeyCode  = static_cast<uint32_t>(event.key.keysym.sym);
                E.windowScanCode = static_cast<uint32_t>(event.key.keysym.scancode);
                E.windowEvent = &E;
                getSystemBus()->triggerEvent(E);
                break;
            }
            case SDL_KEYDOWN:
            {
                EvtInputKey E;
                E.down      = true;
                E.repeat    = event.key.repeat;
                E.keycode   = static_cast<vka::KeyCode>(event.key.keysym.sym);
                E.timestamp = std::chrono::system_clock::now();

                E.windowKeyCode  = static_cast<uint32_t>(event.key.keysym.sym);
                E.windowScanCode = static_cast<uint32_t>(event.key.keysym.scancode);
                E.windowEvent    = &E;
                getSystemBus()->triggerEvent(E);
                break;
            }
            case SDL_MOUSEBUTTONDOWN:
            {
                EvtInputMouseButton E;
                E.x      = event.button.x;
                E.y      = event.button.y;
                E.button = static_cast<MouseButton>(event.button.button);
                E.state  = event.button.state;
                E.clicks = event.button.clicks;

                getSystemBus()->triggerEvent(E);
                break;
            }
        case SDL_MOUSEBUTTONUP:
            {
                EvtInputMouseButton E;
                E.x      = event.button.x;
                E.y      = event.button.y;
                E.button = static_cast<MouseButton>(event.button.button);
                E.state  = event.button.state;
                E.clicks = event.button.clicks;

                getSystemBus()->triggerEvent(E);
                break;
            }
        case SDL_MOUSEMOTION:
            {
                // we have moved the mouse cursor

                EvtInputMouseMotion M;
                M.x = event.motion.x;
                M.y = event.motion.y;
                M.xrel = event.motion.xrel;
                M.yrel = event.motion.yrel;

                getSystemBus()->triggerEvent(M);
                break;
            }
        case SDL_MOUSEWHEEL:
            {
                // we have moved the mouse cursor

                EvtInputMouseWheel M;
                M.delta = event.wheel.y;
                M.x = event.wheel.x;
                M.y = event.wheel.y;

                getSystemBus()->triggerEvent(M);
                break;
            }
        }
    });

    widget.destroy();
    SDL_Quit();
    glslang::FinalizeProcess();
    return 0;
}

}
}



