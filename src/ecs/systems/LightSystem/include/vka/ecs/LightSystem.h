#ifndef VKA_ECS_LIGHTSYSTEM_H
#define VKA_ECS_LIGHTSYSTEM_H

#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/Components/CTransform.h>

namespace vka
{
namespace ecs
{

struct LightSystem : public vka::ecs::SystemBaseInternal
{

    ~LightSystem()
    {
        S_INFO("LightSystem destroyed");
    }

    void onStart() override;
    void onStop() override;
    void onUpdate() override;


    void clear() ;

    std::string name() const override
    {
        return "LightSystem";
    }

};



}
}


#endif
