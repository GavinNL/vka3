#include <sol/sol.hpp>
#include <vka/ecs/ScriptObjects/ScriptObjectLua.h>
#include <vka/ecs/Components/ScriptComponent.h>
#include <vka/ecs/SystemBus.h>

#include <vka/utils/filesystem.h>
#include <fstream>
#include <cstdint>

#define VKA_DEBUG(...)

std::string vka::ecs::ScriptObjectLua::Name() const
{
    return vka::fs::stem( vka::fs::filename(m_script.path) );
}

void vka::ecs::ScriptObjectLua::setVariable(const std::string &name, var_type v)
{
    if( m_sharedEnv )
    {
        auto & sharedEnv = *static_cast<sol::environment*>(m_sharedEnv.get());

        std::visit([&](auto&& arg)
        {
            //using T = std::decay_t<decltype(arg)>;
            sharedEnv[m_objectName][name] = arg;
        }, v);

    }
    else
    {
        _vars[name] = v;
    }
}

void vka::ecs::ScriptObjectLua::Start()
{
    auto scriptPath = ENGINE.m_systemBus->getPath(m_script);
    std::ifstream t(scriptPath);
    std::string luaCode((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());

    auto & SC = ENGINE.get<ScriptComponent>(entity_id());


    // For the LUA script objects. Each ScriptObjectLua on a single entity
    // should share teh same sol::environment. so we are going to loop
    // through all the current objects and check if any of them are ScriptObjectLua
    // if they are, copy the shared env.
    for(auto & x : SC.getScripts() )
    {
        auto s = SC.getScript(x);
        if( auto y = std::dynamic_pointer_cast<ScriptObjectLua>(s))
        {
            if( !y->m_sharedEnv ) continue;

            m_sharedEnv = y->m_sharedEnv;
            break;
        }
    }

    auto & luaState = *static_cast<sol::state*>(m_luaState);

    // if no shared enviornment was found, create the first one
    // and set all the values.
    if( !m_sharedEnv)
    {
        VKA_DEBUG("Creating LUA Shared Environment");

        auto raw = new sol::environment( luaState, sol::create,  luaState.globals() );// static_cast<sol::state*>(m_SolState.get())->globals());
        m_sharedEnv = std::shared_ptr<void>( raw, [](void *r)
        {
            delete static_cast<sol::environment*>(r);
        });
        //m_sharedEnv = std::make_shared<sol::environment>( luaState, sol::create,  luaState.globals() );// static_cast<sol::state*>(m_SolState.get())->globals());

        auto & sharedEnv = *static_cast<sol::environment*>(m_sharedEnv.get());
        sharedEnv["ENTITY"].get_or_create<sol::table>();
        sharedEnv["ENTITY"]["ID"] = static_cast<size_t>( entity_id() );
    }

    auto & sharedEnv = *static_cast<sol::environment*>(m_sharedEnv.get());

    // Run the actual LUA code, this should
    luaState.safe_script( luaCode, sharedEnv );
    VKA_DEBUG("Lua Code Executed: {}", scriptPath );

    auto scriptName = Name();

    // create a variable in the environment
    // caled:  scriptName_ = scriptName:()
    m_objectName = scriptName + "_";
    std::string luaScript = fmt::format("{} = {}:new()", m_objectName, scriptName);

    VKA_DEBUG("Lua Code: {}", luaScript);

    // Create the instance of the class using :new()
    luaState.safe_script(luaScript, sharedEnv);

    if( sharedEnv[m_objectName]["MouseMotion"].get_type() == sol::type::function )
    {
        VKA_DEBUG("Registering: {}:MouseMotion()", m_objectName);
        sink<EvtInputMouseMotion>().connect<&ScriptObjectLua::on_EvtInputMouseMotion>(this);
    }

    if( sharedEnv[m_objectName]["MouseButton"].get_type() == sol::type::function )
    {
        VKA_DEBUG("Registering: {}:MouseButton()", m_objectName);
        sink<EvtInputMouseButton>().connect<&ScriptObjectLua::on_EvtInputMouseButton>(this);
    }
    if( sharedEnv[m_objectName]["Key"].get_type() == sol::type::function )
    {
        VKA_DEBUG("Registering: {}:Key()", m_objectName);
        sink<EvtInputKey>().connect<&ScriptObjectLua::on_EvtInputKey>(this);
    }
    if( sharedEnv[m_objectName]["Loop"].get_type() == sol::type::function )
    {
        VKA_DEBUG("Registering: {}:Loop()", m_objectName);
        sink<EvtGameLoop>().connect<&ScriptObjectLua::on_EvtGameLoop>(this);
    }



    for(auto & v : _vars)
    {
        auto & a = v.second;

        std::visit([&](auto&& arg)
        {
            //using T = std::decay_t<decltype(arg)>;
            sharedEnv[m_objectName][v.first] = arg;
        }, a);
    }

    if( sharedEnv[m_objectName]["Start"].get_type() == sol::type::function )
    {
        VKA_DEBUG("Executing: {}:Start()", m_objectName);
        sharedEnv[m_objectName]["Start"]( sharedEnv[m_objectName] );
    }
}

void vka::ecs::ScriptObjectLua::on_EvtGameLoop(vka::ecs::EvtGameLoop m)
{
    auto & sharedEnv = *static_cast<sol::environment*>(m_sharedEnv.get());
    sharedEnv[m_objectName]["Loop"]( sharedEnv[m_objectName] );
    (void)&m;
}

void vka::ecs::ScriptObjectLua::on_EvtInputKey(vka::EvtInputKey m)
{
    auto & sharedEnv = *static_cast<sol::environment*>(m_sharedEnv.get());
    sharedEnv[m_objectName]["Key"]( sharedEnv[m_objectName]);
    (void)m;
}

void vka::ecs::ScriptObjectLua::on_EvtInputMouseButton(vka::EvtInputMouseButton m)
{
    auto & sharedEnv = *static_cast<sol::environment*>(m_sharedEnv.get());
    sharedEnv[m_objectName]["MouseButton"]( sharedEnv[m_objectName]);
    (void)m;
}

void vka::ecs::ScriptObjectLua::on_EvtInputMouseMotion(vka::EvtInputMouseMotion m)
{
    auto & sharedEnv = *static_cast<sol::environment*>(m_sharedEnv.get());
    sharedEnv[m_objectName]["MouseMotion"]( sharedEnv[m_objectName]);
    (void)m;
}

void vka::ecs::ScriptObjectLua::Update()
{
    auto & sharedEnv = *static_cast<sol::environment*>(m_sharedEnv.get());
    if( sharedEnv[m_objectName]["Update"].get_type() == sol::type::function )
    {
        sharedEnv[m_objectName]["Update"]( sharedEnv[m_objectName] );
    }
}

void vka::ecs::ScriptObjectLua::Stop()
{
    auto & sharedEnv = *static_cast<sol::environment*>(m_sharedEnv.get());
    if( sharedEnv[m_objectName]["Stop"].get_type() == sol::type::function )
    {
        VKA_DEBUG("Registering: {}:Stop()", m_objectName);
        sharedEnv[m_objectName]["Stop"]( sharedEnv[m_objectName] );
    }
}

