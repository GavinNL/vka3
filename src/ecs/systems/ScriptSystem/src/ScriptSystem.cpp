#include <fstream>

#include <sol/sol.hpp>

#include <vka/ecs/Components/ScriptComponent.h>
#include <vka/ecs/ScriptSystem.h>
#include <vka/ecs/Components/TransformComponent.h>
#include <vka/ecs/Components/CameraComponent.h>

#include <vka/ecs/ScriptObjects/ScriptObjectLua.h>

#include <vka/utils/filesystem.h>

namespace vka
{
namespace ecs
{

//==================================================================================
// Reference Objects for each Component that the SciptSystem will handle
// In the LUA script, we would use the funtion:
//
// e = ENGINE.createEntity()
// R = getRenderComponent(e)
//
//==================================================================================

struct ScriptRefPosition
{
    entt::entity      entity;
    entt::registry  * registry;

    ScriptRefPosition() = delete;
    ScriptRefPosition(entt::entity e, entt::registry * r) : entity(e), registry(r)
    {
    }

    void destroy()
    {
        registry->remove<PositionComponent>(entity);
    }

    void translate(const glm::vec3 & direction)
    {
        registry->patch<PositionComponent>(entity, [&](auto & T) { T += direction; } );
    }

    void setPosition(const glm::vec3 & p)
    {
        registry->emplace_or_replace<PositionComponent>(entity, p);
    }

    void set(const glm::vec3 & p)
    {
        registry->emplace_or_replace<PositionComponent>(entity, p);
    }
    vec3 getPosition()
    {
        return registry->get<PositionComponent>(entity);
    }

    static void registerLUA( sol::state * n )
    {
        n->new_usertype<ScriptRefPosition>(
                    "ScriptRefTransform"
                    // Constructor types
                   // , sol::constructors<PhysicsComponent()>()
                    // Member Variables
                    , "set"  , &ScriptRefPosition::set
                    , "translate"  , &ScriptRefPosition::translate

                    , "setPosition"  , &ScriptRefPosition::setPosition
                    , "getPosition"  , &ScriptRefPosition::getPosition

                    , "destroy"  , &ScriptRefPosition::destroy
                    // Public Methods
                    //, "destroy"    , &ScriptEntityRef::destroy
                  );
    }
};

struct ScriptRefRotation
{
    entt::entity      entity;
    entt::registry  * registry;

    ScriptRefRotation() = delete;
    ScriptRefRotation(entt::entity e, entt::registry * r) : entity(e), registry(r)
    {
    }

    void destroy()
    {
        registry->remove<RotationComponent>(entity);
    }

    void lookAt( const glm::vec3 & pos, const glm::vec3 & at, const glm::vec3 & up)
    {
        registry->patch<RotationComponent>(entity, [&](auto & T) { T.lookat(pos,at,up);; } );
    }

    void rotateLocal(const glm::vec3 & axis, float angle)
    {
        registry->patch<RotationComponent>(entity, [&](auto & T) { T.rotateLocal(axis,angle); } );
    }
    void rotateGlobal(const glm::vec3 & axis, float angle)
    {
        registry->patch<RotationComponent>(entity, [&](auto & T) { T.rotateGlobal(axis,angle); } );
    }
    void setRotation(const glm::vec3 & p)
    {
        registry->emplace_or_replace<RotationComponent>(entity, glm::quat(p) );
    }

    quat getRotation()
    {
        return registry->get<RotationComponent>(entity);
    }

    static void registerLUA( sol::state * n )
    {
        n->new_usertype<ScriptRefRotation>(
                    "ScriptRefRotation"
                    // Constructor types
                   // , sol::constructors<PhysicsComponent()>()
                    // Member Variables
                    , "rotateLocal"  , &ScriptRefRotation::rotateLocal
                    , "rotateGlobal" , &ScriptRefRotation::rotateGlobal

                    , "setRotation"  , &ScriptRefRotation::setRotation
                    , "getRotation"  , &ScriptRefRotation::getRotation

                    , "lookAt"   , &ScriptRefRotation::lookAt
                    , "destroy"  , &ScriptRefRotation::destroy
                    // Public Methods
                    //, "destroy"    , &ScriptEntityRef::destroy
                  );
    }
};


struct ScriptRefScript
{
    entt::entity      entity;
    entt::registry  * registry;

    ScriptRefScript() = delete;
    ScriptRefScript(entt::entity e, entt::registry * r) : entity(e), registry(r)
    {
    }


    void destroy()
    {
        registry->remove<ScriptComponent>(entity);
    }
    static void registerLUA( sol::state * n )
    {
        n->new_usertype<ScriptRefScript>(
                    "ScriptRefRender"
                    // Constructor types
                   // , sol::constructors<PhysicsComponent()>()
                    // Member Variables
                    // Public Methods
                    , "destroy"  , &ScriptRefScript::destroy
                  );
    }
};

struct ScriptRefEntity
{
    entt::entity      entity;
    entt::registry  * registry;

    ScriptRefEntity() = delete;
    ScriptRefEntity(entt::entity e, entt::registry * r) : entity(e), registry(r)
    {
    }
    void destroy()
    {
        registry->destroy(entity);
    }

    template<typename Cref, typename C>
    Cref create()
    {
        registry->emplace_or_replace<C>(entity);
        return Cref(entity, registry);
    }

    static void registerLUA( sol::state * n )
    {
        n->new_usertype<ScriptRefEntity>(
                    "EntityObject"
                    // Constructor types
                   // , sol::constructors<PhysicsComponent()>()
                    // Member Variables
                    , "id"      , &ScriptRefEntity::entity
                    // Public Methods
                    , "createRotationComponent" , &ScriptRefEntity::create<ScriptRefRotation, RotationComponent>
                    , "createScriptComponent"    , &ScriptRefEntity::create<ScriptRefScript, ScriptComponent>
                    , "destroy"   , &ScriptRefEntity::destroy
                  );
    }

};

//==================================================================================


void LuaScriptEnv::setVariable(const std::string & var, float v)
{
    (*static_cast<sol::environment*>(m_environment.get()))[var] = v;
}

ScriptComponent& ScriptComponent::addScript(const vka::uri & uri)
{
    addScriptObject( std::make_unique<vka::ecs::ScriptObjectLua>( uri ) );

    return *this;
}

void ScriptComponent::removeScript(const std::string &name)
{
    for(auto & x : m_scriptObjects)
    {
        if( x.first == name)
        {
            m_parent->_stopScriptObject( m_entity, x.second);
            break;
        }
    }
}

void ScriptComponent::removeAllScripts()
{
    for(auto & x : m_scriptObjects)
    {
        m_parent->_stopScriptObject( m_entity, x.second);
    }
}

ScriptComponent& ScriptComponent::addScriptObject(std::unique_ptr<ScriptObject> && obj)
{
    auto scriptName = obj->Name();

    if( m_scriptObjects.count(scriptName) )
    {
        throw std::runtime_error("Cannot add this script object. A script with name, {}, already exists for this entity");
        return *this;
    }
    std::shared_ptr<ScriptObject> s( obj.release() );

    m_scriptObjects[scriptName] = s;
    m_scriptNames.push_back(scriptName);

    m_parent->_startScriptObject(m_entity, s );

    return *this;
}


void ScriptSystem::_startScriptObject( entity_type e, std::shared_ptr<ScriptObject> ptr)
{
    ptr->_entity_id = e;
    m_scriptObjectsToInitialize.push_back(ptr);
}


void ScriptSystem::_stopScriptObject( entity_type e, std::shared_ptr<ScriptObject> ptr)
{
    m_scriptsToStop.push_back(ptr);
    (void)e;
}


void recursivelyCallDestroy(sol::object obj)
{
    if( obj.get_type() != sol::type::table) return;

    // this is a valid event class, so we need to make sure
    // we call the proper destructor
    if( obj.as<sol::table>()["_eventClass"].valid() )
    {
        // already been called
        if( obj.as<sol::table>()["_eventAbandon"].valid() ) return;


        // Call our destructor
        obj.as<sol::table>()["_eventAbandon"] = true;

        if( obj.as<sol::table>()["Destroy"].get_type() == sol::type::function)
        {
            obj.as<sol::table>()["Destroy"]( obj );
        }


        // loop through all the remaining
        for(auto & x : obj.as<sol::table>() )
        {
            if( x.second.get_type() == sol::type::table)
            {
                //std::cout << "Abandoning Table: " << x.first.as<std::string>() << std::endl;
                recursivelyCallDestroy(x.second);
            }
        }
    }
}



void ScriptSystem::removeAllScripts(entity_type e)
{
    //auto & S = get<ScriptComponent>(e);
    (void)e;
}

void ScriptSystem::removeScript(entity_type e, std::string scriptName)
{
   // auto & S = get<ScriptComponent>(e);

    // create a variable in the environment
    // caled:  scriptName_ = scriptName:()
   // std::string objName = scriptName + "_";
    (void)e;
    (void)scriptName;
}


void ScriptSystem::onConstructScriptComponent(registry_type &r, entity_type e)
{
    auto & S = r.get<ScriptComponent>(e);
    S.m_parent = this;
    S.m_entity = e;


    EvtComponentChange c;
    c.entity    = e;
    c.type      = EvtComponentChange::ADDED;
    c.component = "SCRIPT";
    getSystemBus()->triggerEvent(c);

    getSystemBus()->patch<ScriptComponent>(e);
}

void ScriptSystem::onReplaceScriptComponent(registry_type &r, entity_type e)
{
    (void)r;
    (void)e;
}

void ScriptSystem::onDestroyScriptComponent(registry_type &r, entity_type e)
{
    S_DEBUG("Destroying Script Component for Entity {}", e);
    auto & S = r.get<ScriptComponent>(e);

    //======================================
    for(auto & x : S.m_scriptObjects)
    {
        auto & s = x.second;
        if( s->_started)
        {
            if( !s->_stopped)
            {
                s->_stopped = true;
                s->Stop();
            }
        }
    }
    S.m_scriptObjects.clear();
    S.m_scriptNames.clear();
    //======================================
    EvtComponentChange c;
    c.entity = e;
    c.type = EvtComponentChange::REMOVED;
    c.component = "SCRIPT";
    getSystemBus()->triggerEvent(c);
}


void ScriptSystem::onComponentChange(vka::ecs::EvtComponentChange m)
{
    if( has<ScriptComponent>(m.entity) )
    {
        for(auto & x : get<ScriptComponent>(m.entity).m_scriptObjects)
        {
            if( x.second->_started)
            {
                x.second->Update();
            }
        }
    }
}

void ScriptSystem::onUpdate()
{
    //==========================================================================
    // This is temproary until we figure out a better way to do this.
    //   Loops through all the ScriptComponents that have been updated
    //   and checks their _luaScripts json object
    //   then creates luaScriptObjects for each of them and set
    //   and variables
    //==========================================================================
    for(auto e : scriptsUpdated)
    {
        auto & S = get<ScriptComponent>(e);
        for(auto & u : S._luaScripts )
        {
            auto luaS = std::make_unique<vka::ecs::ScriptObjectLua>( vka::uri(u.at("uri").get<std::string>()) );

            if( u.count("variables") )
            {
                auto & vj = u.at("variables");
                for(auto it = vj.begin(); it!=vj.end(); it++)
                {
                    auto k = it.key();
                    auto &val = it.value();

                    if( val.is_number() )
                    {
                        luaS->setVariable(k, val.get<float>() );
                    }
                    else if( val.is_string() )
                    {
                        luaS->setVariable(k, val.get<std::string>() );
                    }
                    else if( val.is_boolean() )
                    {
                        luaS->setVariable(k, val.get<bool>() );
                    }
                    else if( val.is_object() )
                    {
                        if( val.at("type") == "VEC3")
                        {
                            glm::vec3 vec = {val.at("x").get<float>(),val.at("y").get<float>(),val.at("z").get<float>()};
                            luaS->setVariable(k, vec );
                        }
                        if( val.at("type") == "VEC2")
                        {
                            glm::vec2 vec = {val.at("x").get<float>(),val.at("y").get<float>()};
                            luaS->setVariable(k, vec );
                        }
                        else if( val.at("type") == "VEC4")
                        {
                            glm::vec4 vec = {val.at("x").get<float>(),val.at("y").get<float>(),val.at("z").get<float>(), val.at("w").get<float>()};
                            luaS->setVariable(k, vec );
                        }
                        else if( val.at("type") == "QUAT")
                        {
                            glm::quat vec = glm::quat(val.at("w").get<float>(), val.at("x").get<float>(),val.at("y").get<float>(),val.at("z").get<float>());
                            luaS->setVariable(k, vec );
                        }
                    }
                }
            }
            S.addScriptObject( std::move(luaS) );

        }
        S._luaScripts.clear();
    }
    scriptsUpdated.clear();
    //==========================================================================

    {
        static uint64_t scriptIdCount=0;

        for(auto & _m : m_scriptObjectsToInitialize)
        {
            auto s = _m.lock();
            if( s )
            {
                scriptIdCount++;

                s->_started = true;
                s->_scriptId = scriptIdCount;
                s->ENGINE.m_registry = &getSystemBus()->registry;
                s->ENGINE.m_systemBus = getSystemBus().get();

                //auto & Sc = get<ScriptComponent>(s->_entity_id);

                if( auto l = std::dynamic_pointer_cast<ScriptObjectLua>(s) )
                {
                    S_INFO("Lua script Object found");
                    l->m_luaState = m_SolState.get();
                }

                s->Start();
            }
        }
        m_scriptObjectsToInitialize.clear();
    }
    //==============================================================
    auto & st = *static_cast<sol::state*>(m_SolState.get());


    st["FRAME"]["DELTA_TIME"]    = DeltaTime();
    st["INPUT"]["MOUSE"]["XREL"] = 0;
    st["INPUT"]["MOUSE"]["YREL"] = 0;


    //===============================================================
    // Gracefully stop and erase any scripts
    // that need to be stopped.
    //===============================================================
    for(auto & x : m_scriptsToStop)
    {
        // make sure the script has been started
        if( x->_started )
        {
            // make sure the script hasn't already been stopped
            if( !x->_stopped )
            {
                auto n = x->Name();
                x->_stopped = true;
                x->Stop();

                if( valid(x->entity_id()))
                {
                    auto & SC = get<ScriptComponent>(x->entity_id());
                    SC._eraseScript(n);
                }
            }
        }
    }
    m_scriptsToStop.clear();


    std::vector<entity_type> runningEntities;

    auto & r = getSystemBus()->registry;
    for(auto e : m_entitiesToDestroy)
    {
        if( r.valid(e) )
        {
            r.destroy(e);
            S_DEBUG("Destroying Scheduled Entity: {}", e);
        }
    }
    m_entitiesToDestroy.clear();
    m_entitiesToDestroy = std::move(runningEntities);

    //===============================================================
    // TEMPORARY
    //===============================================================
    #pragma message( "To Do: This should be replaced. EvtGameLoop should not be used to trigger the scripts")
    EvtGameLoop E;
    E.dt = DeltaTime();
    getSystemBus()->dispatcher.trigger(E);
    //===============================================================
}

void ScriptSystem::onMouseMove(EvtInputMouseMotion m)
{
    auto & st = *static_cast<sol::state*>(m_SolState.get());

    st["INPUT"]["MOUSE"]["X"]    = m.x;
    st["INPUT"]["MOUSE"]["Y"]    = m.y;
    st["INPUT"]["MOUSE"]["XREL"] = m.xrel;
    st["INPUT"]["MOUSE"]["YREL"] = m.yrel;

}

void ScriptSystem::onMouseButton(EvtInputMouseButton m)
{
    auto & st = *static_cast<sol::state*>(m_SolState.get());
    auto str = to_string(m.button);
    st["INPUT"]["MOUSE"][str] = m.state==1?true:false;
}

void ScriptSystem::onMouseWheel(EvtInputMouseWheel m)
{
    auto & st = *static_cast<sol::state*>(m_SolState.get());
    st["INPUT"]["MOUSE"]["WHEEL"] = m.delta;
}

void ScriptSystem::onKey(EvtInputKey m)
{
    auto & st = *static_cast<sol::state*>(m_SolState.get());
    auto str = to_string(m.keycode);
    str = str.substr(9);
    st["INPUT"]["KEY"][str] = m.down;
}



void ScriptSystem::onStart()
{
    this->initResources();


    scriptsUpdated.connect( getSystemBus()->registry, entt::collector.update<ScriptComponent>() );

    on_construct<ScriptComponent>().connect<&ScriptSystem::onConstructScriptComponent>(*this);
    on_update<  ScriptComponent>().connect<  &ScriptSystem::onReplaceScriptComponent>(*this);
    on_destroy<  ScriptComponent>().connect<  &ScriptSystem::onDestroyScriptComponent>(*this);

    sink<vka::ecs::EvtComponentChange>().connect<&ScriptSystem::onComponentChange>(*this);

    sink<vka::EvtInputMouseMotion>().connect<&ScriptSystem::onMouseMove>(*this);
    sink<vka::EvtInputMouseButton>().connect<&ScriptSystem::onMouseButton>(*this);
    sink<vka::EvtInputMouseWheel>() .connect<&ScriptSystem::onMouseWheel>(*this);
    sink<vka::EvtInputKey>()        .connect<&ScriptSystem::onKey>(*this);

    S_INFO("ScriptSystem Connected Successfully");
}

void ScriptSystem::onStop()
{
    on_construct<ScriptComponent>().disconnect<&ScriptSystem::onConstructScriptComponent>(*this);
    on_update<  ScriptComponent>().disconnect<  &ScriptSystem::onReplaceScriptComponent>(*this);
    on_destroy<  ScriptComponent>().disconnect<  &ScriptSystem::onDestroyScriptComponent>(*this);

    sink<vka::EvtInputMouseMotion>().disconnect<&ScriptSystem::onMouseMove>(*this);
    sink<vka::EvtInputMouseButton>().disconnect<&ScriptSystem::onMouseButton>(*this);
    sink<vka::EvtInputMouseWheel>() .disconnect<&ScriptSystem::onMouseWheel>(*this);
    sink<vka::EvtInputKey>()        .disconnect<&ScriptSystem::onKey>(*this);

    sink<vka::ecs::EvtComponentChange>().disconnect<&ScriptSystem::onComponentChange>(*this);
    S_INFO("ScriptSystem Disconnected Successfully");
}

void ScriptSystem::initResources()
{
    sol::state * n = new sol::state();

    n->open_libraries(sol::lib::base,
                      sol::lib::package,
                      sol::lib::string,
                      sol::lib::math
                      );


    for(auto & s : getSystemBus()->rootPaths->roots)
    {
        m_includePaths.push_back(s);
    }
    // Create the main Sol::State. We are using a
    // a void pointer because we do not want the header files
    // reference any lua/sol headers.
    m_SolState = std::shared_ptr<void>(n,
                                       [](void * S)
    {
       delete static_cast<sol::state*>(S);
    });


    {
        using type_name = glm::vec3;
        auto mult_overloads = sol::overload(
            [](const type_name& v1, const type_name& v2) -> type_name { return v1*v2; },
            [](const type_name& v1, float f) -> type_name { return v1*f; },
            [](float f, const type_name& v1) -> type_name { return f*v1; }
        );
        auto div_overloads = sol::overload(
            [](const type_name& v1, const type_name& v2) -> type_name { return v1/v2; },
            [](const type_name& v1, float f) -> type_name { return v1*f; }
        );
        /*sol::usertype<glm::vec3> vec3Type = */n->new_usertype<type_name>(
                    "vec3",
                    // Constructor types
                    sol::constructors< type_name(), type_name(float), type_name(float,float,float)>()
                    // Member Variables
                    , "x"  , &type_name::x
                    , "y"  , &type_name::y
                    , "z"  , &type_name::z
                    // Public Methods
                    ,sol::meta_function::multiplication, mult_overloads
                    ,sol::meta_function::division,    div_overloads
                    ,sol::meta_function::addition,    sol::resolve<type_name(const type_name&, const type_name&)>(glm::operator+)
                    ,sol::meta_function::subtraction, sol::resolve<type_name(const type_name&, const type_name&)>(glm::operator-)
                  );
    }

    {
        using type_name = glm::vec2;
        auto mult_overloads = sol::overload(
            [](const type_name& v1, const type_name& v2) -> type_name { return v1*v2; },
            [](const type_name& v1, float f) -> type_name { return v1*f; },
            [](float f, const type_name& v1) -> type_name { return f*v1; }
        );
        auto div_overloads = sol::overload(
            [](const type_name& v1, const type_name& v2) -> type_name { return v1/v2; },
            [](const type_name& v1, float f) -> type_name { return v1*f; }
        );
        /*sol::usertype<glm::vec3> vec3Type = */n->new_usertype<type_name>(
                    "vec2",
                    // Constructor types
                    sol::constructors< type_name(), type_name(float), type_name(float,float)>()
                    // Member Variables
                    , "x"  , &type_name::x
                    , "y"  , &type_name::y
                    // Public Methods
                    ,sol::meta_function::multiplication, mult_overloads
                    ,sol::meta_function::division,    div_overloads
                    ,sol::meta_function::addition,    sol::resolve<type_name(const type_name&, const type_name&)>(glm::operator+)
                    ,sol::meta_function::subtraction, sol::resolve<type_name(const type_name&, const type_name&)>(glm::operator-)
                  );
    }
    {
        using type_name = glm::vec4;
        auto mult_overloads = sol::overload(
            [](const type_name& v1, const type_name& v2) -> type_name { return v1*v2; },
            [](const type_name& v1, float f) -> type_name { return v1*f; },
            [](float f, const type_name& v1) -> type_name { return f*v1; }
        );
        auto div_overloads = sol::overload(
            [](const type_name& v1, const type_name& v2) -> type_name { return v1/v2; },
            [](const type_name& v1, float f) -> type_name { return v1*f; }
        );
        /*sol::usertype<glm::vec3> vec3Type = */n->new_usertype<type_name>(
                    "vec4",
                    // Constructor types
                    sol::constructors< type_name(), type_name(float), type_name(float,float, float, float)>()
                    // Member Variables
                    , "x"  , &type_name::x
                    , "y"  , &type_name::y
                    , "z"  , &type_name::z
                    , "w"  , &type_name::w
                    // Public Methods
                    // Public Methods
                    ,sol::meta_function::multiplication, mult_overloads
                    ,sol::meta_function::division,    div_overloads
                    ,sol::meta_function::addition,    sol::resolve<type_name(const type_name&, const type_name&)>(glm::operator+)
                    ,sol::meta_function::subtraction, sol::resolve<type_name(const type_name&, const type_name&)>(glm::operator-)
                  );
    }
    {
        using type_name = glm::quat;
        auto mult_overloads = sol::overload(
            [](const type_name& v1, const vec3& v2) -> vec3 { return v1*v2; },
            [](const type_name& v1, const type_name& v2) -> type_name { return v1*v2; },
            [](const type_name& v1, float f) -> type_name { return v1*f; },
            [](float f, const type_name& v1) -> type_name { return f*v1; }
        );
        auto div_overloads = sol::overload(
            [](const type_name& v1, float f) -> type_name { return v1*f; }
        );
        /*sol::usertype<glm::vec3> vec3Type = */n->new_usertype<type_name>(
                    "quat",
                    // Constructor types
                    sol::constructors< type_name(), type_name(float,float, float, float)>()
                    // Member Variables
                    , "x"  , &type_name::x
                    , "y"  , &type_name::y
                    , "z"  , &type_name::z
                    , "w"  , &type_name::w
                    // Public Methods
                    ,sol::meta_function::multiplication, mult_overloads
                    ,sol::meta_function::division,    div_overloads
                    ,sol::meta_function::addition,    sol::resolve<type_name(const type_name&, const type_name&)>(glm::operator+)
                    ,sol::meta_function::subtraction, sol::resolve<type_name(const type_name&, const type_name&)>(glm::operator-)
                  );
    }

    (*n)["Norm"] = [](glm::vec3 const& x) {return glm::length(x);};

    {
        using type_name = vka::transform;
        auto mult_overloads = sol::overload(
            [](const type_name& v1, const type_name& v2) -> type_name { return v1*v2; }
        );
        /*sol::usertype<glm::vec3> vec3Type = */n->new_usertype<type_name>(
                    "Transform",
                    // Constructor types
                    sol::constructors< type_name(), type_name(glm::vec3), type_name(glm::vec3, glm::quat)>()
                    // Member Variables
                    , "position"  , &type_name::m_position
                    , "rotation"  , &type_name::m_orientation
                    , "scale"     , &type_name::m_scale
                    // Public Methods
                    , "translateLocal"     ,  &type_name::translateLocal
                    , "translate"     ,  &type_name::translate
                    , "translateGlobal"     ,  &type_name::translate
                    , "rotateLocal"     , &type_name::rotateLocal
                    , "rotateGlobal"     , &type_name::rotateGlobal
                    , "setEuler"     , &type_name::set_euler
                    , "lookAt"     , &type_name::lookat
                    , "forward"     , &type_name::forward
                    , "back"     , &type_name::back
                    , "left"     , &type_name::left
                    , "right"     , &type_name::right
                    , "up"     , &type_name::up
                    , "down"     , &type_name::down
                    ,sol::meta_function::multiplication, mult_overloads
                  );
    }


    {
        using type_name = EvtInputMouseMotion;

        /*sol::usertype<glm::vec3> vec3Type = */n->new_usertype<type_name>(
                    "EvtInputMouseMotion"
                    // Constructor types
                    //,sol::constructors< type_name(), type_name(glm::vec3), type_name(glm::vec3, glm::quat)>()
                    // Member Variables
                    , "x"     , &type_name::x
                    , "y"     , &type_name::y
                    , "xrel"  , &type_name::xrel
                    , "yrel"  , &type_name::yrel
                    // Public Methods
                    //,sol::meta_function::multiplication, mult_overloads
                  );
    }


    // Register each of our Component reference
    // classes.
    ScriptRefEntity::registerLUA(n);
    ScriptRefScript::registerLUA(n);
    ScriptRefRotation::registerLUA(n);
    ScriptRefPosition::registerLUA(n);
    //ScriptRefRigidBody::registerLUA(n);

    // Get the main Lua State
    auto solStateRaw = static_cast<sol::state*>(m_SolState.get());
    auto & st = *static_cast<sol::state*>(m_SolState.get());

    std::string package_path = st["package"]["path"];

    for(auto & inc_path : m_includePaths)
    {
        package_path += ";" +  fs::join( inc_path, "?.lua");
    }
    S_INFO("Lua Package Path: {}", package_path);
    st["package"]["path"] = package_path.c_str();//(package_path + ";" VKA_CMAKE_SOURCE_DIR "/share/scripts/?.lua").c_str();

    st["INPUT"].get_or_create<sol::table>();
    st["INPUT"]["KEY"].get_or_create<sol::table>();
    st["INPUT"]["MOUSE"].get_or_create<sol::table>();
    st["INPUT"]["MOUSE"]["X"] = 0;
    st["INPUT"]["MOUSE"]["Y"] = 0;
    st["INPUT"]["MOUSE"]["XREL"] = 0;
    st["INPUT"]["MOUSE"]["YREL"] = 0;
    st["FRAME"].get_or_create<sol::table>();


    st["ENGINE"].get_or_create<sol::table>();

    st["ENGINE"]["findEntityByName"] = [&,solStateRaw](const std::string & i) -> sol::object
    {
        sol::state_view lua(*solStateRaw);

        auto f = getSystemBus()->findEntityByName(i);
        if( f == entt::null)
            return sol::make_object(lua, sol::nil);
        return sol::make_object(lua, static_cast<size_t>(f));
    };

    st["ENGINE"]["createEntity"] = [&]()
    {
        return static_cast<uint32_t>( getSystemBus()->createEntity());
    };
    st["ENGINE"]["getEntityObject"] = [&](size_t entityId)
    {
        auto id = static_cast<entity_type>(entityId);
        return ScriptRefEntity(id, &getSystemBus()->registry);
    };
    st["ENGINE"]["getRotationComponent"] = [&](size_t entityID)
    {
        auto id = static_cast<entity_type>(entityID);
        return ScriptRefRotation(id, &getSystemBus()->registry);
    };
    st["ENGINE"]["getPositionComponent"] = [&](size_t entityID)
    {
        auto id = static_cast<entity_type>(entityID);
        return ScriptRefPosition(id, &getSystemBus()->registry);
    };
    st["ENGINE"]["getScriptComponent"] = [&](size_t entityID)
    {
        auto id = static_cast<entity_type>(entityID);
        return ScriptRefScript(id, &getSystemBus()->registry);
    };

    st["ENGINE"]["scheduleDestroy"] = [&](size_t entityID)
    {
        m_entitiesToDestroy.push_back(static_cast<entity_type>(entityID));
    };

    st["ENGINE"]["hasComponent"] = [&](size_t entityID, std::string tr)
    {
        auto id = static_cast<entity_type>(entityID);
        if( tr=="CAMERA")
        {
            return has<CameraComponent>(id);
        }
        else if( tr=="RIGIDBODY")
        {
            return false;//return has<RigidBodyComponent>(id);
        }
        else if( tr=="SCRIPT")
        {
            return has<ScriptComponent>(id);
        }
        else if( tr=="ROTATION")
        {
            return has<RotationComponent>(id);
        }
        else if( tr=="POSITION")
        {
            return has<PositionComponent>(id);
        }
        else if( tr=="SCALE")
        {
            return has<ScaleComponent>(id);
        }
        return false;
    };

    S_INFO("ScriptSystem initResources");
}

void ScriptSystem::onBuildComponents(entt::entity e, const nlohmann::json &components)
{
    if( components.count("ScriptComponent") )
    {
        auto & S = emplace<ScriptComponent>(e);

        auto & Jc = components.at("ScriptComponent");
        for(auto Js : Jc.at("scripts"))
        {
            if( Js.is_string() )
            {
                auto ustr = Js.get<std::string>();
                vka::uri u(ustr);
                S.addScript(u);
            }
        }
    }
}

}
}
