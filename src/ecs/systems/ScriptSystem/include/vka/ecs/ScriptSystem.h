#ifndef VKA_ECS_SCRIPT_SYSTEM_H
#define VKA_ECS_SCRIPT_SYSTEM_H

#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/Components/CScript.h>
#include <vka/ecs/Events/InputEvents.h>

namespace vka
{
namespace ecs
{


/**
 * @brief The ScriptSystem class
 *
 * The ScriptSystem intercepts various events and
 * passes them to the ScriptComponent. The Event Component
 */
class ScriptSystem : public vka::ecs::SystemBaseInternal
{
    std::shared_ptr<void> m_SolState;
    struct ScriptRef
    {
        entity_type      entity;
        std::string scriptName;
    };



    // called on the gameloop time
    //std::list< ScriptRef > m_onUpdateEnv;

    std::map<std::string, std::list< ScriptRef > > m_ScriptReferences;

    entt::observer scriptsUpdated;
public:

    //==========================================================================
    // User functions
    //==========================================================================
    /**
     * @brief addScript
     * @param e
     * @param luaCode
     *
     * Adds a script to the entity. The Uri must a properly formatted uri
     * file:/home/user/myscript.lua
     *
     * The Lua Script MUST contain the following functions
     *
     * Start() - called when the script srats
     * Exit() - called when the script has been removed
     *
     * Each lua script must contain the following variable
     * somewhere in the global scope.
     *
     * NAME = "scriptName"
     *
     * You cannot have two scripts with the same name running
     * at the same time on the same entity.
     */
    void addScript(entity_type e, const vka::uri & uri);

    /**
     * @brief removeScript
     * @param e
     * @param scriptName
     *
     * Removes the script with the given name. This will
     * execute the script's Exit() function
     */
    void removeScript(entity_type e, std::string scriptName);

    void removeAllScripts(entity_type e);


    void addIncludePath( const std::string & path)
    {
        m_includePaths.push_back(path);
    }

    std::vector<std::string> m_includePaths;
    //==========================================================================


    //==========================================================================
    void onConstructScriptComponent( registry_type &r, entity_type e);

    void onReplaceScriptComponent(registry_type &r, entity_type e );

    void onDestroyScriptComponent(registry_type &r, entity_type e);
    //==========================================================================


    //==========================================================================
    // Application/Window Handling Events
    //   - these events will be translated into  Game Events.
    //==========================================================================
    void onComponentChange(vka::ecs::EvtComponentChange m);

    //void onGameLoop(vka::ecs::EvtGameLoop m);

    void onMouseMove(vka::EvtInputMouseMotion m);

    void onMouseButton(vka::EvtInputMouseButton m);

    void onMouseWheel(vka::EvtInputMouseWheel m);

    void onKey(vka::EvtInputKey m);

    void onStart() override;
    void onStop() override;

    std::string name() const override
    {
        return "ScriptSystem";
    }

    void onUpdate() override;

    void initResources();

    /**
     * @brief setVariable
     * @param e
     * @param varName
     * @param v
     *
     * Set a variable for a particular entity. The variable is
     * shared by all scripts associated with this entity
     */
    void setVariable(entity_type e, const std::string &varName, float v);

    /**
     * @brief setVariable
     * @param e
     * @param scriptName
     * @param varName
     * @param v
     *
     * Sets a variable for a particular running script within
     * the component
     */
    void setVariable(entity_type e, const std::string & scriptName, const std::string &varName, float v);


    void onBuildComponents(entity_type e, const nlohmann::json &components) override;
protected:
    void _startScriptObject(entity_type e, std::shared_ptr<ScriptObject> ptr);
    void _stopScriptObject(entity_type e, std::shared_ptr<ScriptObject> ptr);
    friend class ScriptHandler;


    std::vector<entity_type> m_entitiesToDestroy;
    std::vector< std::shared_ptr<ScriptObject> > m_scriptsToStop;



    std::vector< std::weak_ptr<ScriptObject> > m_scriptObjectsToInitialize;

    friend class ScriptComponent;

};


}
}

#endif
