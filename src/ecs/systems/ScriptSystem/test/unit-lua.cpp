#include <vka/math/geometry/bvhTree.h>
#include <sol/sol.hpp>
#include "catch.hpp"
#include <iostream>

class ScriptObject
{
public:
    explicit ScriptObject(sol::state *pLuaState, const std::string &luaClassName)
        :
          m_pLuaState(pLuaState),
          m_initialized(false)
    {
        if (m_pLuaState)
        {
            static std::size_t scriptNum = 0;
            m_scriptVarName = luaClassName + "_" + std::to_string(scriptNum++);
            bool isValidCreation = true;
            std::string luaScript = m_scriptVarName + " = " + luaClassName + ":new()";
            m_pLuaState->script(luaScript,
                                [&isValidCreation](lua_State* state, sol::protected_function_result res)
                                {
                                    (void)state;
                                    isValidCreation = false; return res;
                                });
            if (isValidCreation)
            {
                m_luaObjectData = (*m_pLuaState)[m_scriptVarName];
            }
            m_initialized = isValidCreation && m_luaObjectData.valid();
        }
    }
    void CallFunction(const std::string &fnName)
    {
        if (m_initialized)
        {
            m_luaObjectData[fnName](m_luaObjectData);
        }
    }


    template<typename... _Args>
    void CallFunction(const std::string &methodName, _Args&&... __args)
    {
        if (m_initialized)
        {
            m_luaObjectData[methodName](m_luaObjectData, std::forward<_Args>(__args)...);
        }
    }
    template<typename... _Args>
    void call(const std::string &methodName, _Args&&... __args)
    {
        if (m_initialized)
        {
            m_luaObjectData[methodName](m_luaObjectData, std::forward<_Args>(__args)...);
        }
    }

    ~ScriptObject()
    {
        if (m_initialized && m_pLuaState)
            m_pLuaState->do_string(m_scriptVarName + " = nil");
    }
private:
    std::string  m_scriptVarName; // The name of the lua object in the lua state
    sol::table   m_luaObjectData;  // A variable holding the data of the created lua object
    sol::state  *m_pLuaState;     // The lua state the lua object is created
    bool         m_initialized;          // whether the scriptObject is initialized
};





SCENARIO("Instantiate a Lua Class in C++ and call a method")
{
    std::string luaScript =
    R"foo(

    local class = require 'middleclass'

    CameraControllerClass = class('CameraControllerClass')

    function CameraControllerClass:initialize()
        print('CameraControllerClass Created! ')
        self.x = 1
        print('X = ' .. self.x)
    end

    function CameraControllerClass:update(dt)
        self.x = self.x + dt
        print(self.x)
    end

    function CameraControllerClass:set(y)
        self.x = y
        print(self.x)
    end

    )foo";


    sol::state state;
    state.open_libraries(sol::lib::base, sol::lib::package);
    std::string package_path = state["package"]["path"];
    std::cout << package_path << std::endl;
    state["package"]["path"] = (package_path + ";" CMAKE_CURRENT_SOURCE_DIR "/middleclass.lua").c_str();


    state.do_string( luaScript );

    ScriptObject myCameraController(&state, "CameraControllerClass");
    myCameraController.CallFunction("update", 1);
}


SCENARIO("Run a Lua script by calling a free function from C++")
{

    std::string luaScript =
    R"foo(

    function update(MyC)

        MyC:setInterval(5.3)

        MyC:stop()
    end

    x = 3
    function main()
        x = x+1
        print(x)
    end

    )foo";

  sol::state state;
  state.open_libraries(sol::lib::base, sol::lib::package);
  std::string package_path = state["package"]["path"];
  state["package"]["path"] = (package_path + ";" CMAKE_CURRENT_SOURCE_DIR "/middleclass.lua").c_str();

  // create the environment where the function will
  // reside
  sol::environment env = sol::environment(state , sol::create, state.globals());

  // Execute the script within the environment
  state.safe_script( luaScript, env);

  // Call the main function a bunch of time.
  env["main"]();
  env["main"]();
  env["main"]();


  // create a new environment
  sol::environment env2 = sol::environment(state , sol::create, state.globals());

  // Execute the script within the environment
  state.safe_script( luaScript, env2);

  env2["main"]();

}






struct EventRef
{
    uint32_t count=0;

    bool m_stop=false;
    EventRef()
    {
        auto c = inc(1);
        if( c > 1)
            throw  std::runtime_error("Cannot create more than 1");
    }
    ~EventRef()
    {
        inc(-1);
    }

    static int inc(int i)
    {
        static int c=0;
        c+=i;
        return c;
    }
    void setInterval( double seconds)
    {
        std::cout << "Interval set to " << seconds << " seconds" << std::endl;;
    }

    void stop()
    {
        std::cout << "Event stopped" << std::endl;
        m_stop=true;
    }
};






SCENARIO("Call the LUA script and pass it a REFERENCE to a C++ object")
{

    std::string luaScript =
    R"foo(

    function update(MyC)

        MyC:setInterval(5.3)

        MyC:stop()
    end


    )foo";

  sol::state state;
  state.open_libraries(sol::lib::base, sol::lib::package);
  std::string package_path = state["package"]["path"];
  state["package"]["path"] = (package_path + ";" CMAKE_CURRENT_SOURCE_DIR "/middleclass.lua").c_str();

  sol::usertype<EventRef> vec3v_type = state.new_usertype<EventRef>(
              "MyClass"
              // Constructor types
             // , sol::constructors<PhysicsComponent()>()
              // Member Variables
              , "count"  , &EventRef::count
              // Public Methods
              , "setInterval"  , &EventRef::setInterval
              , "stop"  , &EventRef::stop
            );

  // create the environment where the function will
  // reside
  sol::environment env = sol::environment(state , sol::create, state.globals());

  // Execute the script within the environment
  state.safe_script( luaScript, env);



  // The object will be passed to the script
  EventRef Obj;


  REQUIRE_NOTHROW( env["update"](Obj) );
  REQUIRE_NOTHROW( env["update"](Obj) );
  REQUIRE_NOTHROW( env["update"](Obj) );
  REQUIRE_NOTHROW( env["update"](Obj) );
  if( Obj.m_stop )
  {
      std::cout << "Script stopped" << std::endl;
  }
//  ScriptObject myTestClass2(&state, "TestClass");
//  myTestClass2.CallFunction("set", "hello");
}




struct LuaScriptEnv
{
    std::shared_ptr<sol::environment> m_environment;
    std::shared_ptr<sol::state>       m_state;

    LuaScriptEnv()
    {

    }
    ~LuaScriptEnv()
    {
        if(m_environment)
            m_environment->abandon();
    }

    template<typename... _Args>
    auto call(const std::string &methodName, _Args&&... __args)
    {
        return (*m_environment)[methodName](std::forward<_Args>(__args)...);
    }
    void exec(const std::string & luaScript)
    {
        // Execute the script within the environment
        m_state->safe_script( luaScript, *m_environment);
    }
};

struct ScriptSystem
{
    std::shared_ptr<sol::state> m_state;

    ScriptSystem()
    {
        m_state = std::make_shared<sol::state>();
    }

    LuaScriptEnv newEnv()
    {
        LuaScriptEnv L;
        L.m_environment = std::make_shared<sol::environment>(*m_state , sol::create, m_state->globals());
        L.m_state = m_state;
        return L;
    }
};

SCENARIO("ScriptSystem test")
{

    std::string luaScript =
    R"foo(

    function update(MyC)

        MyC:setInterval(5.3)

        MyC:stop()
    end


    )foo";

  ScriptSystem S;

  sol::usertype<EventRef> vec3v_type = S.m_state->new_usertype<EventRef>(
              "MyClass"
              // Constructor types
             // , sol::constructors<PhysicsComponent()>()
              // Member Variables
              , "count"  , &EventRef::count
              // Public Methods
              , "setInterval"  , &EventRef::setInterval
              , "stop"  , &EventRef::stop
            );

  // execute the lua code and return the environment
  // that it was executed in. This way you can
  // continue to call the functions which were
  // defined in the script.
  auto env = S.newEnv();
  env.exec(luaScript);

  // The object will be passed to the script
  EventRef Obj;

  env.call("update", Obj);

}


SCENARIO("Instantiate a Lua Class in C++ and call a method1212")
{
    std::string luaScript =
    R"foo(

    local class = require 'middleclass'

    CameraControllerClass = class('CameraControllerClass')

    -- This is the construtor, it will be called
    -- when the script has been added to the entity
    -- and has constructed the object
    function CameraControllerClass:initialize()
        print('CameraControllerClass Created! ')
        self.x = 1
        print('X = ' .. self.x)
    end

    function CameraControllerClass:update(dt)
        self.x = self.x + dt
        print(self.x)
    end

    function CameraControllerClass:set(y)
        self.x = y
        print(self.x)
    end


    myControllerClass = class('myControllerClass', CameraControllerClass)
    function myControllerClass:initialize()
        CameraControllerClass.initialize(self)
        print('myControllerClass Created! ')
    end
    )foo";


    sol::state state;
    state.open_libraries(sol::lib::base, sol::lib::package);
    std::string package_path = state["package"]["path"];
    //state["package"]["path"] = (package_path + ";" CMAKE_SOURCE_DIR "/middleclass.lua").c_str();
    state["package"]["path"] = (package_path + ";" VKA_CMAKE_SOURCE_DIR "/share/scripts/?.lua").c_str();
    package_path = state["package"]["path"];
    std::cout << "Current Path: " << package_path << std::endl;

    auto * env = new sol::environment( state, sol::create,  state.globals());// static_cast<sol::state*>(m_SolState.get())->globals());

    // Parse the script and run the first execution.
    // Any lua code not inside a function will be
    // executed at this time.
    state.safe_script(luaScript, *env);

    // Check that the methods of the class have been created
    REQUIRE( (*env)["CameraControllerClass"].valid() );
    REQUIRE( (*env)["CameraControllerClass"]["initialize"].get_type() == sol::type::function );
    REQUIRE( (*env)["CameraControllerClass"]["set"].get_type() == sol::type::function);
    REQUIRE( (*env)["CameraControllerClass"]["update"].get_type() == sol::type::function);

    std::string  m_scriptVarName = "CameraControllerClass_0 = CameraControllerClass:new()";
    state.safe_script(m_scriptVarName, *env);

    (*env)["CameraControllerClass_0"]["set"]( (*env)["CameraControllerClass_0"], 666);


    m_scriptVarName = "myControllerClass_0 = myControllerClass:new()";
    state.safe_script(m_scriptVarName, *env);
}


SCENARIO("Testing VkaScript")
{
    std::string luaScript =
    R"foo(

    local class = require 'middleclass'
    require 'Script'


    myControllerClass = class('myControllerClass', Script)

    function myControllerClass:initialize()
        Script.initialize(self)
        print('myControllerClass Created! ')
    end

    )foo";


    sol::state state;
    state.open_libraries(sol::lib::base, sol::lib::package);
    std::string package_path = state["package"]["path"];
    //state["package"]["path"] = (package_path + ";" CMAKE_SOURCE_DIR "/middleclass.lua").c_str();
    state["package"]["path"] = (package_path + ";" VKA_CMAKE_SOURCE_DIR "/share/scripts/?.lua").c_str();
    package_path = state["package"]["path"];
    std::cout << "Current Path: " << package_path << std::endl;

    auto * env = new sol::environment( state, sol::create,  state.globals());// static_cast<sol::state*>(m_SolState.get())->globals());

    // Parse the script and run the first execution.
    // Any lua code not inside a function will be
    // executed at this time.
    state.safe_script(luaScript, *env);


    std::string m_scriptVarName = "myControllerClass_0 = myControllerClass:new()";
    state.safe_script(m_scriptVarName, *env);
}

