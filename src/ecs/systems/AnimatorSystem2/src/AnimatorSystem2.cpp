#include <vka/ecs/AnimatorSystem2.h>
#include <vka/ecs/Components/CAnimator2.h>
#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CPose.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/Tags.h>

void vka::ecs::AnimatorSystem2::onConstructAnimatorComponent(registry_type &r, entity_type e)
{
    auto & R = r.get<vka::ecs::CAnimator2>(e);
    R.m_systemBus = getSystemBus();
    R.m_entity = e;

    EvtComponentChange c;
    c.entity    = e;
    c.type      = EvtComponentChange::ADDED;
    c.component = "ANIMATOR2";
    getSystemBus()->triggerEvent(c);
}

void vka::ecs::AnimatorSystem2::onReplaceAnimatorComponent(registry_type &r, entity_type e)
{
    (void)&r;
    (void)&e;
}

void vka::ecs::AnimatorSystem2::onDestroyAnimatorComponent(registry_type &r, entity_type e)
{
    EvtComponentChange c;
    c.entity = e;
    c.type = EvtComponentChange::REMOVED;
    c.component = "ANIMATOR2";
    getSystemBus()->triggerEvent(c);
    (void)r;
}




void vka::ecs::AnimatorSystem2::onStart()
{
    getSystemBus()->registry.on_construct<CAnimator2>().connect<  &AnimatorSystem2::onConstructAnimatorComponent>(*this);
    getSystemBus()->registry.on_update<  CAnimator2>().connect<  &AnimatorSystem2::onReplaceAnimatorComponent>(*this);
    getSystemBus()->registry.on_destroy<  CAnimator2>().connect<  &AnimatorSystem2::onDestroyAnimatorComponent>(*this);

    S_INFO("AnimatorSystem2 Connected Successfully");

}

void vka::ecs::AnimatorSystem2::onStop()
{
    //dispatcher().sink<vka::EvtInputMouseButton>().disconnect<&AnimatorSystem::onMousePress>(*this);

    getSystemBus()->registry.on_construct<CAnimator2>().disconnect  <&AnimatorSystem2::onConstructAnimatorComponent>(*this);
    getSystemBus()->registry.on_update<  CAnimator2>().disconnect<  &AnimatorSystem2::onReplaceAnimatorComponent>(*this);
    getSystemBus()->registry.on_destroy<  CAnimator2>().disconnect<  &AnimatorSystem2::onDestroyAnimatorComponent>(*this);

    S_INFO("AnimatorSystem2 Disconnected Successfully");
}

void vka::ecs::AnimatorSystem2::clear()
{
    // Remove the rigidbody components from
    // from all the entities that have them
    //
    // this functionality goes through the ECS
    {
        std::vector<entity_type> entities;
        auto _view = getSystemBus()->registry.view<CAnimator2>();
        for(auto & c : _view)
        {
            entities.push_back(c);
        }
        for(auto & c : entities)
        {
            getSystemBus()->registry.remove<CAnimator2>(c);
        }
    }

}



// recurively calculate the time for each node
float vka::ecs::AnimatorSystem2::_calculateTimes( vka::ecs::CAnimator2 & component, size_t r)
{
    auto & reg = getSystemBus()->registry;
    auto & Sc = reg.get<CScene>(component.m_entity);
    auto sceneId = Sc.sceneId;


    GLTFScene2 const & Scene = getSystemBus()->getManagedResource<GLTFScene2>(sceneId);

    // Loop through all the Input nodes and
    // calculate the total length of the animation for
    // them.
    auto & N = component.animationNode[r];
    float ret=0;
    std::visit([&](auto&& arg)
    {
        using T = std::decay_t<decltype(arg)>;

        if constexpr (std::is_same_v<T, vka::ecs::AnimatorNodeInput>)
        {
            auto & A = Scene.animations[ arg.animationIndex ];
            arg._length = A.end-A.start;
            ret=arg._length;
        }
        else
        {
            arg._length=0;
            for(auto j : arg.inputNodeIndex)
                arg._length = std::max( arg._length, _calculateTimes(component, j) );
            ret = arg._length;
        }
    }, N);
    return ret;

    //auto kfs1 = component.getOutput();
    //for(auto & k : kfs1)
    //{
    //    currentTransform.at( k.node ) = k.value;
    //}
}


void vka::ecs::AnimatorSystem2::onUpdate()
{
    double dt = DeltaTime();
    auto & r = getSystemBus()->registry;
    auto _view = view<CAnimator2,CScene, CPose>();
    for(const auto entity : _view)
    {
        auto & AC = _view.get<CAnimator2>(entity);
        auto & RC = _view.get<CScene>(entity);
        auto & PC = _view.get<CPose>(entity);
        AC.m_entity = entity;
        AC.m_systemBus = getSystemBus();
        if( PC.nodeSpaceTransforms.size() == 0 )
        {
            auto & MR = getSystemBus()->getManagedResource<GLTFScene2>(RC.sceneId);
            PC.nodeSpaceTransforms = MR.getNodeSpaceTransforms();
        }

        if( !AC._isCompiled)
        {
            if( !r.valid(AC.m_entity) )
                continue;
            AC._length = _calculateTimes(AC, AC.rootNode);
            AC._isCompiled=true;
        }

        {
            // increment the animation component by a time
            // equal to delta-t
            //AC.increment( static_cast<float>(dt) );
            AC.t += static_cast<float>(dt) * AC.playSpeed;
            AC.t = std::fmod( AC.t, AC._length);

            float s = AC.t / AC._length;

            // get the final output of the
            // animation
            auto kfs1 = AC.getOutput(s);

            for(auto & k : kfs1)
            {
                PC.nodeSpaceTransforms[ k.node ] = k.value;
            }

            r.emplace_or_replace<tag_animation_nodes_updated>(entity);
            updateLocalTransforms(entity);
        }
    }
}

vka::ecs::AnimatorSystem2::AnimatorSystem2()
{

}

vka::ecs::AnimatorSystem2::~AnimatorSystem2()
{

}


void vka::ecs::AnimatorSystem2::updateLocalTransforms(entt::entity e)
{
    auto & r = getSystemBus()->registry;
    auto & G = r.get<CScene>(e);
    auto & P = r.get<CPose>(e);

    auto const & S = getSystemBus()->getManagedResource<GLTFScene2>(G.sceneId);

    //  The model-space matrices for each node in the GLTF scene
    //  The G.nodeSpaceTransforms are used mostly for GLTF Animations
    //  then the AnimationController will modify this value
    //  we need to take these values and update the
    auto modelSpaceMatrices = S.getModelSpaceMatrices( P.nodeSpaceTransforms );

    // Loop through all the entities in the SceneComponent, G
    // and pull out the corresponding transform from the
    // Pose Component. Update the transformComponent for that
    // entity.
    size_t i=0;
    for(auto n : G.nodes)
    {
        if( r.valid(n) )
        {
            r.emplace_or_replace<vka::ecs::CPosition>(n, P.nodeSpaceTransforms[i].position);
            r.emplace_or_replace<vka::ecs::CRotation>(n, P.nodeSpaceTransforms[i].rotation);
            r.emplace_or_replace<vka::ecs::CScale>(   n, P.nodeSpaceTransforms[i].scale);

            if(  S.nodes[i].skinIndex.has_value() )
            {
                auto & M = r.get<vka::ecs::CSkeleton>( G.nodes[i] );
                // copy the skin matrices to the mes
                M.boneMatrices.clear();
                appendBoneMatrices(M.boneMatrices,
                                   modelSpaceMatrices,
                                   S.skins[ *S.nodes[i].skinIndex].inverseBindMatrices,
                                   S.skins[ *S.nodes[i].skinIndex].joints);
            }
        }
        ++i;
    }
}


void vka::ecs::AnimatorSystem2::appendBoneMatrices(std::vector<glm::mat4> &nodePlusBoneMatrices,
                                                   vk::ArrayProxy<const glm::mat4> modelSpaceBoneTransforms,
                                                   vk::ArrayProxy<const glm::mat4> inverseBindMatrices,
                                                   vk::ArrayProxy<const uint32_t> jointIndices)
{
    if( jointIndices.size() )
    {
        assert( jointIndices.size() == inverseBindMatrices.size() );

        if( modelSpaceBoneTransforms.size() )
        {
            // upload all bone matrices
            uint32_t b=0;
            for(auto j : jointIndices)
            {
                glm::mat4 bone = modelSpaceBoneTransforms.data()[j] * inverseBindMatrices.data()[b++];
                nodePlusBoneMatrices.push_back( bone );
            }
        }
    }
}


























namespace vka
{
namespace ecs
{

//============================================================
// AnimatorNodeInput
//============================================================
SceneBase::Animation::KeyFrameState AnimatorNodeInput::getKeyFrameState(CAnimator2 & C, float s)
{
    SceneBase::Animation::KeyFrameState p;
    auto & reg = C.m_systemBus->registry;
    if(!reg.has<CScene>(C.m_entity)) return p;

    auto sceneId = reg.get<CScene>(C.m_entity).sceneId;

    auto & S = C.m_systemBus->getManagedResource<vka::ecs::GLTFScene2>( sceneId );
    auto & a = S.animations.at(animationIndex);

    if( reverse) s=1.0f-s;
    auto length = a.end-a.start;
    auto   t    = s * length;

    a.getSceneKeyFrame(t, S.nodes, p);

    if( disableRootMotion )
    {
        for(auto & x : p )
        {
            if(  S.nodes[x.node].rootSkeleton.size()  )
            {
                x.value.position *= vec3(0,1,0);
            }
        }
    }

    return p;
}

//============================================================


//============================================================
// AnimatorNodeInput
//============================================================
SceneBase::Animation::KeyFrameState AnimatorNodeBlend::getKeyFrameState(CAnimator2 & C, float s)
{
    float bv = blendValue;
    if( !std::isfinite(blendValue))
        bv = 0.0f;

    if( blendValue <= 0.0f )
    {
        return C.node<AnimatorNode>( inputNodeIndex.front()  ).getKeyFrameState(C,s);
    }
    else if( blendValue >= static_cast<float>(inputNodeIndex.size()-1) )
    {
        return C.node<AnimatorNode>( inputNodeIndex.back()  ).getKeyFrameState(C,s);
    }
    else
    {
        auto  fb = std::floor(bv);
        size_t i = static_cast<size_t>( fb );
        auto  fr = blendValue - fb;

        auto p1 = C.node<AnimatorNode>( inputNodeIndex.at(i)    ).getKeyFrameState(C,s);
        auto p2 = C.node<AnimatorNode>( inputNodeIndex.at(i+1)  ).getKeyFrameState(C,s);

        SceneBase::Animation::KeyFrameState   p;// = allocateKeyFrameState();

        mix(p, p1, p2, fr);
        return p;
    }
}

//============================================================

#if 0

//============================================================
// AnimatorNodeOneShot
//============================================================
SceneBase::Animation::KeyFrameState AnimatorNodeOneShot::getKeyFrameState(float s)
{
    if( !triggered )
    {
        auto p = parentComponent->node<AnimatorNode>( inputNodeIndex.at(0)  ).getKeyFrameState(s);
        return p;
    }
    else
    {
        auto p = parentComponent->node<AnimatorNode>( inputNodeIndex.at(1)  ).getKeyFrameState();
        return p;
    }
}


float AnimatorNodeOneShot::getLength() const
{
    if( _L >= 0 )
        return _L;

    for(auto i : inputNodeIndex)
    {
        _L = parentComponent->node<AnimatorNode>(i).getLength();
        return _L;
    }
    _L=0;
    return _L;

}

bool AnimatorNodeOneShot::increment(float dt)
{
    time += dt * timeScale;
    bool f1 = time > getLength();
    time = std::fmod(time, getLength());

    for(auto & i : inputNodeIndex)
    {
        auto & N = parentComponent->node<AnimatorNode>(i);
        bool f = N.increment(dt*timeScale);
        if(triggered && std::distance(&inputNodeIndex.front(), &i)==1)
        {
            if(f)
            {
                triggered = false;

            }
        }
    }
    return f1;
}

void AnimatorNodeOneShot::setUnitTime(float s)
{
    auto L = getLength();
    time = glm::mix(0.f, L, s);
    time = std::fmod(time, L);
}

void AnimatorNodeOneShot::trigger()
{
    if(!triggered)
    {
        std::cout << "Triggered = true" << std::endl;
        triggered=true;
        if( inputNodeIndex.size()>2)
        {
            auto & N = parentComponent->node<AnimatorNode>( inputNodeIndex[1]);
            N.setUnitTime(0);
        }
    }
}
//============================================================
#endif
}
}
