#ifndef VKA_ECS_ANIMATOR_SYSTEM_2_H
#define VKA_ECS_ANIMATOR_SYSTEM_2_H

#include <set>
#include <map>

#include <vka/ecs/SystemBase2.h>
#include <vka/ecs/Components/CAnimator2.h>

namespace vka
{

namespace ecs
{

struct RenderComponent;

class AnimatorSystem2 : public SystemBaseInternal
{
public:
    AnimatorSystem2();
    ~AnimatorSystem2();

    /**
     * @brief clear
     *
     * Destroys all the Physics Objects from the system
     */
    void onStart() override;
    void onStop() override;
    void onUpdate() override;

    std::string name() const override
    {
        return "AnimatorSystem2";
    }

    void clear();
    void onConstructAnimatorComponent(registry_type &r, entity_type e);
    void onReplaceAnimatorComponent(  registry_type &r,   entity_type e);
    void onDestroyAnimatorComponent(  registry_type &r,   entity_type e);

    void onConstructCAnimator2  (registry_type &r,   entity_type e);
    void   onReplaceCAnimator2(  registry_type &r, entity_type e);
    void   onDestroyCAnimator2(  registry_type &r, entity_type e);

    // updates the local transforms of all entities
    void updateLocalTransforms(entt::entity e);

    void step(double dt, vk::ArrayProxy<entity_type> entities);

    vka::SceneBase::Animation::KeyFrameState allocateKeyFrameState()
    {
        if( m_KeyFrameQueue.size()==0)
        {
            m_KeyFrameQueue.push( vka::SceneBase::Animation::KeyFrameState() );
        }
        auto x = std::move( m_KeyFrameQueue.front() );
        m_KeyFrameQueue.pop();
        return x;
    }
    void freeKeyFrameState( vka::SceneBase::Animation::KeyFrameState && frame)
    {
        m_KeyFrameQueue.push( std::move(frame) );
    }
protected:
    float _calculateTimes(vka::ecs::CAnimator2 & component, size_t r);

    std::queue< vka::SceneBase::Animation::KeyFrameState > m_KeyFrameQueue;

    void appendBoneMatrices(std::vector<glm::mat4> &nodePlusBoneMatrices, vk::ArrayProxy<const glm::mat4> modelSpaceBoneTransforms, vk::ArrayProxy<const glm::mat4> inverseBindMatrices, vk::ArrayProxy<const uint32_t> jointIndices);
};




}
}

#endif

