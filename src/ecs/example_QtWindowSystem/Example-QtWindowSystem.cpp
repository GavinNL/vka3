#include <iostream>
#include <vka/ecs/Engine.h>
#include <fmt/format.h>

#include <vka/ecs/Components/CWSADMovement.h>
#include <vka/ecs/Components/CMouseLook.h>

#include <glslang/Public/ShaderLang.h>


#include <QMainWindow>
#include <QScrollArea>
#include <QApplication>
#include <QPlainTextEdit>
#include <QVulkanInstance>
#include <QLibraryInfo>
#include <QLoggingCategory>
#include <QPointer>
#include <QHBoxLayout>
#include <QPushButton>
#include <QDockWidget>

// callback function for validation layers
VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;
    #define VKA_INFO(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_WARN(...)  std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_ERROR(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    #define VKA_DEBUG(...) std::cout << fmt::format(__VA_ARGS__) << std::endl;
    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}

static QPointer<QPlainTextEdit> messageLogWidget;
static QtMessageHandler oldMessageHandler = nullptr;

static void messageHandler(QtMsgType msgType, const QMessageLogContext &logContext, const QString &text)
{
    if (!messageLogWidget.isNull())
        messageLogWidget->appendPlainText(text);
    if (oldMessageHandler)
        oldMessageHandler(msgType, logContext, text);
}

#include <vka/ecs/QtWindowSystem.h>
#include <vka/ecs/ui/vkaProjectTreeWidget.h> // from QtWindowSystem
#include <vka/ecs/ui/vkaComponentInspector.h>
#include <vka/ecs/ui/vkaResourceTable.h>

void init( std::shared_ptr<vka::ecs::SystemBus> sb)
{
    using namespace vka::ecs;

    if(1)
    {
        auto E = sb->createEntity("cameraEntity");
        auto & C = E.emplaceComponent( CCamera() );
        C.environment = sb->resourceCreate<vka::ecs::Environment>(vka::uri("rc:environments/green_hills_2.environment"));

        vka::Transform t;
        t.position = {-3,3,-3};
        t.lookat( {0,0,0}, {0,1,0});


        auto & RB = E.emplaceComponent( vka::ecs::CRigidBody());
        RB.isKinematic = true;
        RB.mass = 0;

        E.emplaceComponent( CPosition( t.position ));
        E.emplaceComponent( CRotation( t.rotation ));
        E.emplaceComponent( CMouseLook());
        E.emplaceComponent( CWSADMovement());
    }

    {
        vka::ecs::PBRMaterial unlit;
        unlit.unlit = true;
        auto unlit_id  = sb->resourceCreateAndEmplace<vka::ecs::PBRMaterial>("unlitMat", std::move(unlit) );
        auto grid_id   = sb->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("grid1", vka::ecs::HostMeshPrimitive::Grid(10,10) );
        auto meshId = grid_id;

        auto E      = sb->createEntity("gridEntity");

        E.emplaceComponent( CMesh(meshId, unlit_id, vk::PrimitiveTopology::eLineList) );
        E.emplaceComponent( CPosition() );
        E.emplaceComponent( CRotation() );

    }

    if(1)
    {
       auto sphere_id = sb->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("sphere1", vka::ecs::HostMeshPrimitive::Sphere(1.0f,32,32) );

       vka::ecs::PBRMaterial defaultMaterial;
       defaultMaterial.baseColorFactor = {0,1,0,1};
       defaultMaterial.roughnessFactor = 0;
       defaultMaterial.metallicFactor = 1.0;

       //auto default_material_id   = sb->createResource<vka::ecs::PBRMaterial>("defaultMaterial", std::move(defaultMaterial) );

       auto E = sb->createEntity("sphereEntity3");

       E.emplaceComponent( vka::ecs::CMesh(sphere_id, vka::ecs::PBRMaterial_ID()) );
       E.emplaceComponent( vka::ecs::CPosition(glm::vec3{0,2.5,0}) );
       E.emplaceComponent( vka::ecs::CRotation() );
       E.emplaceComponent( vka::ecs::CCollider( vka::ecs::BoxCollider() ) );
       E.emplaceComponent( vka::ecs::CGhostBody() );

    }

    if(1)
    {
        auto scene_id = sb->resourceGetOrCreate<vka::ecs::GLTFScene2>( vka::uri("rc:models/ybot_full.glb"));
        auto E  = sb->createEntity("sceneEntity");

        E.emplaceComponent( CScene(scene_id));
        E.emplaceComponent( CRotation());
        E.emplaceComponent( CPosition(glm::vec3(0,0,0)));
        E.emplaceComponent( CCollider( vka::ecs::SphereCollider(1.0f)));
        E.emplaceComponent( CGhostBody());
    }
    std::dynamic_pointer_cast<vka::ecs::PhysicsSystem2>(sb->getOrCreateSystem2<vka::ecs::PhysicsSystem2>())->toggleDebugDraw();
}



int main(int argc, char ** argv)
{
    (void)argc;
    (void)argv;

    glslang::InitializeProcess();

    // This is the main vka engine. It provides all the systems
    // except the window system. The window s ystem will be
    // configured by us.
    std::shared_ptr<vka::ecs::Engine>          m_enginePtr = std::make_shared<vka::ecs::Engine>();

    // grab the system bus so we can configure
    // everything
    auto systemBus = m_enginePtr->m_SystemBus;

    // add the paths to the resources
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    systemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");

    // initialize the engine. This will
    // add all the systems to the systembus
    // and configure them
    m_enginePtr->initializeSystems();






    {
        // Finally add the window system
        QApplication app(argc, argv);

        //=================================================================================
        // Before we set up the QtWindowSystem we need to set up
        // the vulkan instance we want to use.
        // Along with any validation layers.

        messageLogWidget = new QPlainTextEdit(QLatin1String(QLibraryInfo::build()) + QLatin1Char('\n'));
        messageLogWidget->setReadOnly(true);

        oldMessageHandler = qInstallMessageHandler(messageHandler);

        QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));

        QVulkanInstance inst;

        inst.setLayers(QByteArrayList() << "VK_LAYER_LUNARG_standard_validation");

        if (!inst.create())
            qFatal("Failed to create Vulkan instance: %d", inst.errorCode());

        //=================================================================================


        // Now that we have the instance created
        // we can create the QtWindowSystem and pass in
        // the QVulkanInstance we created.
        std::shared_ptr<vka::ecs::QtWindowSystem> qtWindowSystem = std::make_shared<vka::ecs::QtWindowSystem>(&inst);

        // create a wrapper for the vulkanWindow
        // we need this to be able to add our render area
        // as a widget for a qtWindow
        QWidget *wrapper = qtWindowSystem->getWrapper();
        (void)wrapper;


        // Now we can set up our Qt Window
        // and add our wrapper widget
        {
            QMainWindow * m_window = new QMainWindow();

            auto rtable    = new vkaResourceTable( systemBus, nullptr );
            auto tree      = new vkaProjectWidget( systemBus, m_window );
            auto inspector = new vkaComponenInspector( systemBus, m_window );

            rtable->show();
            // Set the Wrapper as teh central widget
            m_window->setCentralWidget(wrapper);


            {

                auto dockWidget_4 = new QDockWidget(m_window);
                     dockWidget_4->setObjectName(QString::fromUtf8("projectTreeDock"));
                     dockWidget_4->setFeatures(QDockWidget::DockWidgetMovable | QDockWidget::DockWidgetClosable | QDockWidget::DockWidgetFloatable);
                     dockWidget_4->setWindowTitle("Project Tree");

                         auto dockWidgetContents_4 = new QWidget(m_window);
                         dockWidgetContents_4->setLayout( new QVBoxLayout(dockWidgetContents_4 ));
                         dockWidgetContents_4->layout()->addWidget(tree);

                     dockWidget_4->setWidget(dockWidgetContents_4);
                m_window->addDockWidget(Qt::LeftDockWidgetArea, dockWidget_4);

            }


            if(1)
            {
                auto dockWidget_4 = new QDockWidget(m_window);
                dockWidget_4->setObjectName(QString::fromUtf8("projectComponentInspectorDock"));
                dockWidget_4->setFeatures(QDockWidget::DockWidgetMovable);

                dockWidget_4->setWindowTitle("Component Inspector");
                m_window->addDockWidget(Qt::RightDockWidgetArea, dockWidget_4);

                if(1)
                {
                    auto scroll    = new QScrollArea();
                    scroll->setWidgetResizable(true);
                    inspector->setSizePolicy(QSizePolicy(QSizePolicy::Maximum,QSizePolicy::Maximum));
                    scroll->setWidget(inspector);
                    dockWidget_4->setWidget( scroll);
                }
            }


            auto centralWidget = new QWidget();

            auto l = new QHBoxLayout();
            centralWidget->setLayout(l);


            tree->connect(tree, &vkaProjectWidget::entitySelected,
                          [inspector](entt::entity e)
            {
                inspector->showEntity(e);
            });
            tree->connect(tree, &vkaProjectWidget::resourceSelected,
                          [inspector](uint32_t e, QString type)
            {
                #define _handleRES(RTYPE) \
                if( type == RTYPE::_subType)\
                {\
                    auto id = vka::ecs::_ID<RTYPE>(e,false);\
                    inspector->showResource(id);\
                }
                _handleRES(vka::ecs::Environment)
                _handleRES(vka::ecs::PBRMaterial)
                _handleRES(vka::ecs::GLTFScene2)
                _handleRES(vka::ecs::HostMeshPrimitive)

                #undef _handleRES
            });

            m_window->resize(1024,768);
            m_window->show();

        }

        m_enginePtr->m_SystemBus->addSystemFront(qtWindowSystem);

        // Call our init function so that
        // we can configure it for our example
        init(systemBus);

        auto retCode = app.exec();

        systemBus->disconnectAllSystems();
        m_enginePtr.reset();

        glslang::FinalizeProcess();
        return retCode;
    }
    return 0;
}

