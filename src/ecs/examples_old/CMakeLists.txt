cmake_minimum_required(VERSION 3.10)


set(SAMPLE_MODEL_URI "https://github.com/KhronosGroup/glTF-Sample-Models/raw/master")

if(NOT EXISTS "${CMAKE_SOURCE_DIR}/share/models/download/DamagedHelmet.glb")

    file(DOWNLOAD "${SAMPLE_MODEL_URI}/2.0/DamagedHelmet/glTF-Binary/DamagedHelmet.glb" "${CMAKE_SOURCE_DIR}/share/models/download/DamagedHelmet.glb" )

endif()


# Find all files named unit-*.cpp
file(GLOB files "*.cpp")

foreach(file ${files})

    get_filename_component(file_basename ${file} NAME_WE)
    string(REGEX REPLACE "unit-([^$]+)\\.cpp" "\\1" exe_name ${file_basename})

    message("New File: ${file}    Exe name: ${exe_name}")

    add_executable( ${exe_name} ${file} )

    target_link_libraries( ${exe_name}
                                PUBLIC
                                    vka::ecs
                                    vka::ecsPhysicsSystem
                                    vka::ecsLightSystem
                                    vka::ecsControlSystem
                                    vka::ecsEventSystem
                                    vka::ecsScriptSystem
                                    vka::ecsAnimatorSystem2
                                    vka::ecsRenderSystem3
                                    vka::ecsSceneSystem
                                    vka::ecsTransformSystem
                                    vkw::vkw
                                    CONAN_PKG::sdl2

                                    vka_project_warnings)

endforeach()

add_subdirectory(Qt)

