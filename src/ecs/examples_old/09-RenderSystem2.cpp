#include <iostream>
#include <queue>
#include <tuple>

#include <vka/core.h>

#include <future>

#include <vka/logging.h>


#include <vka/math.h>

#include <vka/utils/GLSLCompiler.h>


#include <vka/ecs/RenderSystem2/RenderSystem2.h>
#include <vka/ecs/ControlSystem.h>
#include <vka/ecs/PhysicsSystem.h>
#include <vka/ecs/EventSystem.h>
#include <vka/ecs/ScriptSystem.h>
#include <vka/ecs/AnimatorSystem2.h>
#include <vka/utils/HostCubeImage.h>
#include <vka/ecs/Components/LightComponent.h>

#include <vka/ecs/Components/SceneComponent.h>
#include <vka/ecs/Components/PrimitiveMaterialComponent.h>

#include <vka/utils/Camera.h>
#include <vka/Controllers/FPSController.h>
#include <vka/Controllers/OrbitController.h>
//#include <vka/Controllers/GizmoController.h>
#include <vka/logging.h>
#include <vka/ecs/ScriptObjects/ScriptObjectGizmoRotation.h>
#include <vka/ecs/ScriptObjects/ScriptObjectGizmoTranslation.h>
#include <vka/ecs/ScriptObjects/ScriptObjectLua.h>


#define WIDTH  1024
#define HEIGHT 768


#ifndef VKA_NO_SDL_WIDGET
#include <vka/widgets/SDLWidget.h>
#endif

#include <entt/entt.hpp>

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;

    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}


class MyApp : public vka::Application
{
    // Application interface
public:

    std::shared_ptr<vka::ecs::SystemBus>      m_SystemBus;
    std::shared_ptr<vka::ecs::RenderSystem2>  m_RenderSystem;
    std::shared_ptr<vka::ecs::ControlSystem>  m_ControlSystem;
    std::shared_ptr<vka::ecs::PhysicsSystem>  m_PhysicsSystem;
    std::shared_ptr<vka::ecs::EventSystem>    m_EventSystem;
    std::shared_ptr<vka::ecs::ScriptSystem>   m_ScriptSystem;
    std::shared_ptr<vka::ecs::AnimatorSystem2> m_AnimatorSystem;

    void init()
    {
        
        auto box_id    = m_SystemBus->createResource<vka::HostTriPrimitive>("box1"   , vka::HostTriPrimitive::Box({1,1,1}) );
        auto sphere_id = m_SystemBus->createResource<vka::HostTriPrimitive>("sphere1", vka::HostTriPrimitive::Sphere(1.0f,40,40) );

        (void)box_id   ;// = m_SystemBus->createResource<vka::HostTriPrimitive>("box1"   , vka::HostTriPrimitive::Box({1,1,1}) );
        (void)sphere_id;// = m_SystemBus->createResource<vka::HostTriPrimitive>("sphere1", vka::HostTriPrimitive::Sphere(1.0f) );

        {
            vka::HostImage img;
            img.resize(256, 256);

            //// Generate an image by applying a function to each channel
            img.r = [](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
            img.g = [](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
            img.b = [](float u, float v){ return 0.5f+0.5f*std::sin( 3*2*3.14159f*std::hypot(u,v-1)) ;}; // full red texture;
            img.a = 255; // full red texture;

            m_SystemBus->createResource<vka::HostImage>("nullImage", std::move(img) );

        }


        if(1)
        {
            auto E = m_SystemBus->create();
            auto & C = E.create<vka::ecs::CameraComponent>();
            C.environment = m_SystemBus->createResource<vka::ecs::Environment>(vka::uri("rc:environments/default.environment"));

            E.create<vka::ecs::TransformComponent>(glm::vec3{-5,5,-5}).lookat({0,0,0}, {0,1,0});
            E.create<vka::ecs::ScriptComponent>()
                    // playerInput handler is reponsible for taking raw keyboard/mouse inputs and
                    // converting them into numerical "control values" eg: lookH=3.2
                    // the script stores the values in the Entity's shared variables LUA table: ENTITY.lookH
                    .addScript(vka::uri("file:" VKA_CMAKE_SOURCE_DIR "/share/scripts/example/playerInput.lua"))
                        //  .setVariable("playerInput", "LOOK_SENSITIVITY_V", 0.45)
                        //  .setVariable("playerInput", "LOOK_SENSITIVITY_H", 0.45)
                        //  .setVariable("playerInput", "FORWARD_SENSITIVITY",0.45)
                        //  .setVariable("playerInput", "SIDE_SENSITIVITY",   0.45)

        #if 0
                    // the cameraOrbitController keeps the entity pointed at a target and uses the
                    // ENTITY.lookH and ENTITY.lookV
                    .addScript(vka::uri("file:" VKA_CMAKE_SOURCE_DIR "/share/scripts/example/cameraOrbitController.lua"))
                        // Set which target we want to follow with the orbitController
                        .setVariable("cameraOrbitController", "targetEntity", "player")
                        .setVariable("cameraOrbitController", "targetOffset", glm::vec3(0,2,0))
        #else
                    //.addScript( vka::uri("file:" CMAKE_SOURCE_DIR "/share/scripts/example/playerInput.lua") )
                    // the playerController script is responsible for taking the numerical control values
                    // and modifying the entity's TRANSFORM component
                    .addScript( vka::uri("file:" VKA_CMAKE_SOURCE_DIR "/share/scripts/example/playerController.lua") );
        #endif
                    ;
        }

        if(1)
        {

            //auto scene_id = m_SystemBus->createResource<vka::ecs::GLTFScene>("SC1",  vka::uri("file:/home/gavin/Projects/vka3/share/models/ybot_full.glb"));
            auto scene_id = m_SystemBus->createResource<vka::ecs::GLTFScene>(vka::uri("rc:models/ybot_full2.glb"));

            auto E      = m_SystemBus->create();

            E.create<vka::ecs::NameComponent>("player");

            E.create<vka::ecs::SceneComponent>(scene_id);
            E.create<vka::ecs::TransformComponent>().m_position = {0,0,0};
            auto & A = E.create<vka::ecs::AnimatorComponent2>();
            //A.sceneId = scene_id;
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(0); // idle
            }

            //  ==== walk forward/left/right
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(1); // forward
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(2); // strafe left
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(3); // strafe right
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeBlend>();
                A.node<vka::ecs::AnimatorNodeBlend>(i1).inputNodeIndex = {2,1,3};
                A.node<vka::ecs::AnimatorNodeBlend>(i1).blendValue = 0.1f;
            }

            //  ==== run forward/left/right
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(4); // run-forward
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(5); // run-strafe left
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(6); // run-strafe right
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeBlend>();
                A.node<vka::ecs::AnimatorNodeBlend>(i1).inputNodeIndex = {6,5,7};
                A.node<vka::ecs::AnimatorNodeBlend>(i1).blendValue = 0.1f;
            }


            //  ==== back left/right
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(2); // back left
                A.node<vka::ecs::AnimatorNodeInput>(i1).reverse = true; // back left
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(3); // back right
                A.node<vka::ecs::AnimatorNodeInput>(i1).reverse = true; // back left
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeBlend>();
                A.node<vka::ecs::AnimatorNodeBlend>(i1).inputNodeIndex = {9,10};
                A.node<vka::ecs::AnimatorNodeBlend>(i1).blendValue = 0.5f;
            }


            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeBlend>();
                A.node<vka::ecs::AnimatorNodeBlend>(i1).inputNodeIndex = {11,4,0,8};
                A.node<vka::ecs::AnimatorNodeBlend>(i1).blendValue = 0.1f;
            }
            A.rootNode = 12;


            E.create<vka::ecs::EventComponent>().addEvent( vka::ecs::EventType::FRAME,
                                                           [&](vka::ecs::EventRef & R)
            {
                glm::vec2 x = m_SystemBus->variables.mouseScreenPosition / m_SystemBus->variables.screenSize;

                R.entity.get<vka::ecs::AnimatorComponent2>().node<vka::ecs::AnimatorNodeBlend>(4).blendValue  = x.x * 2.0f;
                R.entity.get<vka::ecs::AnimatorComponent2>().node<vka::ecs::AnimatorNodeBlend>(8).blendValue  = x.x * 2.0f;
                R.entity.get<vka::ecs::AnimatorComponent2>().node<vka::ecs::AnimatorNodeBlend>(11).blendValue = x.x;

                R.entity.get<vka::ecs::AnimatorComponent2>().node<vka::ecs::AnimatorNodeBlend>(12).blendValue = x.y * 3.0f ;

                R.entity.get<vka::ecs::AnimatorComponent2>().playSpeed = 3.0f;//x.y * 48.0f;
            });

            auto groundShape = std::make_shared<btBoxShape>( btVector3{ 1, 1.0, 1.0});
            E.create<vka::ecs::CollisionComponent>(groundShape);
            E.create<vka::ecs::MassComponent>(1.0f);

            if(0)
            {
                E.create<vka::ecs::ScriptComponent>()
                  .addScriptObject( std::make_unique<vka::ecs::ScriptObjectLua>(vka::uri("file:" VKA_CMAKE_SOURCE_DIR "/share/scripts/example/playerInput.lua")))
                  .addScriptObject( std::make_unique<vka::ecs::ScriptObjectLua>(vka::uri("file:" VKA_CMAKE_SOURCE_DIR "/share/scripts/example/thirdPersonCharacterController.lua")))
                        ;
            }
        }
        #if 1
//==========
        if(1)
        {
            // Create a very simple scene using
            // a single mesh primtii
            std::string s1 = "rc:/box";
            auto groundShape = std::make_shared<btBoxShape>( btVector3{ 1, 1.0, 1.0});
            auto wallShape   = std::make_shared<btBoxShape>( btVector3{ 1, 1.0, 1.0});

            glm::vec3 wallScale   = glm::vec3(5,2,0.1);
            glm::vec3 groundScale = glm::vec3(150,0.1,150);

            float wallY=wallScale.y;
            wallShape->setLocalScaling(   btVector3(wallScale.x,wallScale.y,wallScale.z));
            groundShape->setLocalScaling( btVector3(groundScale.x,groundScale.y,groundScale.z));

            vka::ecs::MaterialPrimitive renderable;
            renderable.primitive = box_id;

            vka::ecs::PBRMaterial material;
            material.baseColorFactor = {0.98f,0.98f,0.98f,1.0f};
            material.metallicFactor  = 0.0f;
            material.roughnessFactor = 0.9f;

            auto mId = m_SystemBus->createResource("myMaterial", std::move(material));
            {
                auto E = m_SystemBus->create();
                E.create<vka::ecs::MaterialPrimitiveComponent>(box_id, mId);
                E.create<vka::ecs::TransformComponent>().setPosition({0,-0.105,0}).setScale(groundScale);
                E.create<vka::ecs::CollisionComponent>(groundShape);
                E.create<vka::ecs::MassComponent>(0.0f);
            }
            if( 0)
            {
                {
                    auto E = m_SystemBus->create();
                    E.create<vka::ecs::MaterialPrimitiveComponent>(box_id, renderable.material);
                    E.create<vka::ecs::TransformComponent>().setScale(wallScale).setPosition({-0.0,  wallY, -5}).lookat( {0,wallY,0}, {0,1,0});
                    E.create<vka::ecs::CollisionComponent>(wallShape);
                    E.create<vka::ecs::MassComponent>(0.0f);
                }
                {
                    auto E = m_SystemBus->create();
                    E.create<vka::ecs::MaterialPrimitiveComponent>(box_id, renderable.material);
                    E.create<vka::ecs::TransformComponent>().setScale(wallScale).setPosition({-0.0,  wallY,  5}).lookat( {0,wallY,0}, {0,1,0});
                    E.create<vka::ecs::CollisionComponent>(wallShape);
                    E.create<vka::ecs::MassComponent>(0.0f);
                }
                {
                    auto E = m_SystemBus->create();
                    E.create<vka::ecs::MaterialPrimitiveComponent>(box_id, renderable.material);
                    E.create<vka::ecs::TransformComponent>().setScale(wallScale).setPosition({-5, wallY, 0}).lookat( {0,wallY,0}, {0,1,0});
                    E.create<vka::ecs::CollisionComponent>(wallShape);
                    E.create<vka::ecs::MassComponent>(0.0f);
                }
                {
                    auto E = m_SystemBus->create();
                    E.create<vka::ecs::MaterialPrimitiveComponent>(box_id, renderable.material);
                    E.create<vka::ecs::TransformComponent>().setScale(wallScale).setPosition({ 5, wallY, 0}).lookat( {0,wallY,0}, {0,1,0});
                    E.create<vka::ecs::CollisionComponent>(wallShape);
                    E.create<vka::ecs::MassComponent>(0.0f);
                }
            }
        }

            ///================
            ///
        for(int X=0; X < 11;X++)
        {
            float x = static_cast<float>(X);
            for(int Y=0;Y<11;Y++)
            {
                float y  = static_cast<float>(Y);

                auto E      = m_SystemBus->create();
                auto & R    = E.create<vka::ecs::MaterialPrimitiveComponent>();
                // auto & P    = E.create<vka::ecs::RigidBodyComponent>();

                R.primitive = m_SystemBus->getResourceManager<vka::HostTriPrimitive>().findResource("sphere1");

                vka::ecs::PBRMaterial material;
                material.baseColorTexture = m_SystemBus->getResourceManager<vka::HostImage>().findResource("nullImage");
                material.metallicFactor  = glm::clamp( x / 10.0f, 0.0f, 1.0f);
                material.roughnessFactor = glm::clamp( y / 10.0f, 0.0f, 1.0f);
                material.baseColorFactor = {1,1,1,1};
                //R.material.debugMode = vka::ecs::PBRDebugMode::NORMAL;
                E.create<vka::ecs::TransformComponent>().m_position = glm::vec3{1.75f*x,1.75f*y,0} + glm::vec3(10,2,10);
            }
        }

        {
            auto E      = m_SystemBus->create();
            //auto & R    = E.create<vka::ecs::MaterialPrimitiveComponent>();
            // auto & P    = E.create<vka::ecs::RigidBodyComponent>();

            //R.primitive = m_SystemBus->getResourceManager<vka::HostTriPrimitive>().findResource("sphere1");

           // R.material.baseColorTexture = m_SystemBus->getResourceManager<vka::HostImage>().findResource("nullImage");

            E.create<vka::ecs::LightComponent>().color = {1.0f,1.0f,1.f};
            E.create_and_update<vka::ecs::LightComponent>([](vka::ecs::LightComponent & L)
                        {
                            L.color = {1,1,1};
                            L.type = vka::ecs::LightType::SPOT;
                        }
                        );
            E.create<vka::ecs::TransformComponent>().m_position = glm::vec3{1.75f*5.f,1.75f*1.f,0} + glm::vec3(10,2,5);
            E.create_and_update<vka::ecs::ScriptComponent>([](vka::ecs::ScriptComponent & C)
            {
                auto & Rg = C.addScriptObject<vka::ecs::ScriptObjectGizmoRotation>();
                Rg.setVariable("gizmoScale", 0.2f);

                auto & Tg = C.addScriptObject<vka::ecs::ScriptObjectGizmoTranslation>();
                Tg.setVariable("localTransform", true);
            });

        }

        if(0)
        {
            auto boxShape = std::make_shared<btSphereShape>( 1.0f);

            auto E   = m_SystemBus->create();
            auto & R = E.create<vka::ecs::MaterialPrimitiveComponent>();

            vka::ecs::PBRMaterial mat;
            R.primitive = m_SystemBus->getResourceManager<vka::HostTriPrimitive>().findResource("sphere1");
            mat.baseColorTexture = m_SystemBus->getResourceManager<vka::HostImage>().findResource("nullImage");
            //R.material.metallicRoughnessTexture = m_SystemBus->getResourceManager<vka::HostImage>().findResource("nullImage");
            mat.baseColorFactor = {1,1,1,1};

            R.material = mat;
            E.create<vka::ecs::TransformComponent>().setPosition( {0,10,0});
            E.create<vka::ecs::CollisionComponent>(boxShape);
            E.create<vka::ecs::MassComponent>(1.0f);
        }



        //==================================================================
        // Sample Component
        //==================================================================
        if(1)
        {
          //  auto boxShape = std::make_shared<btBoxShape>( btVector3(1.0,1.0,1.0) );

            auto E   = m_SystemBus->create();

            vka::ecs::PBRMaterial material;
            auto & R = E.create<vka::ecs::MaterialPrimitiveComponent>();
                       R.primitive = m_SystemBus->getResourceManager<vka::HostTriPrimitive>().findResource("box1");
                     //  R.material.baseColorTexture = m_SystemBus->getResourceManager<vka::HostImage>().findResource("nullImage");;
                       //R.material.metallicRoughnessTexture = m_SystemBus->getResourceManager<vka::HostImage>().findResource("nullImage");;
                       material.baseColorFactor = {0.5,0.2,0.6,1};
                       material.metallicFactor = 0;
                       material.roughnessFactor = 1.0f;
                       material.unlit = true;

                       R.material = material;
                       E.create<vka::ecs::TransformComponent>().setPosition( {0,20,1.0});
            //E.create<vka::ecs::CollisionComponent>(boxShape);
            //E.create<vka::ecs::MassComponent>(1.0f);

            //E.create<vka::ecs::LightComponent>().color = {1.0f,1.0f,1.f};


            //E.create<vka::ecs::ScriptComponent>().addScriptObject<vka::ecs::ScriptObjectGizmoRotation>()
            //                                     .addScriptObject<vka::ecs::ScriptObjectGizmoTranslation>();

            E.create_and_update<vka::ecs::ScriptComponent>([](vka::ecs::ScriptComponent & C)
            {
                auto & Rg = C.addScriptObject<vka::ecs::ScriptObjectGizmoRotation>();
                Rg.setVariable("gizmoScale", 0.2f);
                Rg.setVariable("xAxisColorHover", glm::vec4(5.0f) );
                Rg.setVariable("yAxisColorHover", glm::vec4(5.0f) );
                Rg.setVariable("zAxisColorHover", glm::vec4(5.0f) );
                //C.addScriptObject<vka::ecs::ScriptObjectGizmoTranslation>()
                // .setVariable("localTransform", true);
            });
            //;
            E.create<vka::ecs::BoundingBoxComponent>();
            E.create<vka::ecs::EventComponent>()
             .addEvent(vka::ecs::EventType::FRAME,
                       [&](vka::ecs::EventRef & Rr)
                       {
                           if( Rr.count > 250 )
                           {
                               //Rr.entity.get<vka::ecs::ScriptComponent>().removeScript("ScriptObjectGizmoTranslation");
                               if(0)
                               {
                                   auto & I = m_SystemBus->getResourceManager<vka::HostImage>();
                                   auto id = I.findResource("nullImage");
                                   I.update( id,
                                             [](vka::HostImage & img)
                                   {
                                      img.r = 255;
                                      img.g = 0;
                                      img.b = 0;
                                   });
                               }
                               Rr.stop();
                           }
                       });

        }
        //==================================================================
#endif

        if(1)
        {
            auto E   = m_SystemBus->create();

            vka::HostTriPrimitive grid;
            grid.POSITION.reset<float>(3);

            float W = 10.0f;
            for(float x=-W; x <= W; x+=1.0f)
            {
                grid.POSITION.push_back( glm::vec3(-W,0,x) );
                grid.POSITION.push_back( glm::vec3( W,0,x) );
            }

            auto & R = E.create<vka::ecs::MaterialPrimitiveComponent>();
                       R.primitive = m_SystemBus->createResource<vka::HostTriPrimitive>("grid", std::move(grid));
                       R.primitiveTopology = vk::PrimitiveTopology::eLineList;
                       vka::ecs::PBRMaterial material;
                       material.baseColorFactor = {1.0,1.0,1.0,1};
                       material.metallicFactor  = 0;
                       material.roughnessFactor = 1.0f;
                       material.unlit = true;
                       R.material = material;
                       E.create<vka::ecs::TransformComponent>(glm::vec3(0,2,0));

                       auto ec = E.createChild();
                       ec.create<vka::ecs::MaterialPrimitiveComponent>() = R;
                       ec.create<vka::ecs::LocalTransformComponent>(vka::R90y);

                       E.create_and_update<vka::ecs::ScriptComponent>([](vka::ecs::ScriptComponent & C)
                       {
                           auto & Rg = C.addScriptObject<vka::ecs::ScriptObjectGizmoRotation>();
                           Rg.setVariable("gizmoScale", 0.2f);
                           Rg.setVariable("xAxisColorHover", glm::vec4(5.0f) );
                           Rg.setVariable("yAxisColorHover", glm::vec4(5.0f) );
                           Rg.setVariable("zAxisColorHover", glm::vec4(5.0f) );
                           C.addScriptObject<vka::ecs::ScriptObjectGizmoTranslation>()
                            .setVariable("localTransform", true);
                       });
        }

    }




    //===============================================================================================================================
    // Vulkan APP Specific code
    //===============================================================================================================================

    /**
     * @brief initResources
     *
     * This will be called right after system gets created.
     * It can be used to initialize any vulkan related resources
     * that are not related to the swapchain.
     */
    void initResources() override
    {

    //profiler::dumpBlocksToFile("test_profile.prof");
        glslang::InitializeProcess();

        addPath(VKA_CMAKE_SOURCE_DIR  "/share");
        addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
        addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
        addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
        addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");


        m_SystemBus    = std::make_shared<vka::ecs::SystemBus>();
        m_SystemBus->rootPaths = getRootPaths();

        m_RenderSystem  = std::make_shared<vka::ecs::RenderSystem2>();
        m_ControlSystem = std::make_shared<vka::ecs::ControlSystem>();
        m_PhysicsSystem = std::make_shared<vka::ecs::PhysicsSystem>();
        m_EventSystem   = std::make_shared<vka::ecs::EventSystem>();
        m_ScriptSystem  = std::make_shared<vka::ecs::ScriptSystem>();
        m_AnimatorSystem  = std::make_shared<vka::ecs::AnimatorSystem2>();

        m_ScriptSystem->addIncludePath(VKA_CMAKE_SOURCE_DIR "/share/scripts" );

        m_RenderSystem  ->initResources();

        m_RenderSystem  ->connect(m_SystemBus);
        m_ControlSystem ->connect(m_SystemBus);
        m_PhysicsSystem ->connect(m_SystemBus);
        m_EventSystem   ->connect(m_SystemBus);
        m_ScriptSystem  ->connect(m_SystemBus);
        m_AnimatorSystem->connect(m_SystemBus);
    }


    /**
     * @brief releaseResources
     *
     * This method must destroy all
     * non-swapchain related resources that were
     *
     */
    void releaseResources() override
    {
        m_RenderSystem->releaseResources();

        glslang::FinalizeProcess();
        //std::cout << "release non-graphics resources" << std::endl;
    }


    /**
     * @brief initSwapChainResources
     *
     * This is called right after the swapchain is created.
     * You can use this to initialize any swapchain related resources,
     * for example, render targets (because they change with swapchain size)
     */
    void initSwapChainResources() override
    {
        VKA_DEBUG("init swapchain releated resources");

        m_RenderSystem->initSwapchainResources(swapchainImageSize(),
                                               concurrentFrameCount(),
                                               getDefaultRenderPass());

        if( !m_init )
        {
            m_init= true;
            init();
        }
        startThreadPolling();
    }


    void releaseSwapChainResources() override
    {        
        m_RenderSystem->releaseSwapchainResources();

        VKA_DEBUG("release swapchain releated resources");
        stopThreadPolling();
    }




    /**
     * @brief render
     * @param frame
     *
     * This is the widget's main vulkan render function.
     * use this to actually record your command buffers.
     * submission of commandbuffers are automatically done
     * at the end
     */
    void render(vka::ApplicationFrame & frame) override
    {
        assert( frame.currentFrameBuffer );
        assert( frame.currentSwapchainImage );
        assert( frame.currentSwapchainImageView );
        vka::ecs::EvtGameLoop E;
        E.dt = std::chrono::duration<double>(frame.frameTime - frame.lastFrameTime).count();
        m_SystemBus->triggerEvent(E);
        m_PhysicsSystem->step(E.dt);

        // temporary for now.
        m_AnimatorSystem->step(E.dt, nullptr);

        m_PhysicsSystem->debugDraw();

        m_RenderSystem->render(frame);
    }

    void mouseMoveEvent(const vka::EvtInputMouseMotion *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }
    void mousePressEvent(const vka::EvtInputMouseButton *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }
    void mouseReleaseEvent(const vka::EvtInputMouseButton *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }

    void mouseWheelEvent(const vka::EvtInputMouseWheel *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }

    void keyPressEvent(const vka::EvtInputKey *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }
    void keyReleaseEvent(const vka::EvtInputKey *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }
    void fileDropEvent(const vka::EvtInputFileDrop *e) override
    {
        (void)e;
    }

    bool m_init=false;
    //=======================

    vka::line3 m_mouseProjection;
};

#ifndef VKA_NO_SDL_WIDGET

int main(int argc, char *argv[])
{
    //profiler::dumpBlocksToFile("test_profile.prof");
    (void)argc;
    (void)argv;
    // The basic usage of this

    // Create a vulkan window widget
    vka::SDLVulkanWidget2 vulkanWindow;

    // create the window and initialize
    // the vka::System
    vulkanWindow.create(1024, 768, debugCallback);

    // instantiate your application
    MyApp app;

    vulkanWindow.exec( &app );


    return 0;
}

#endif

