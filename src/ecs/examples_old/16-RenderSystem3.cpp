#include <iostream>
#include <queue>
#include <tuple>
#include <future>

#include <vka/core.h>
#include <vka/math.h>

#include <vka/ecs/TransformSystem.h>
#include <vka/ecs/ControlSystem.h>
#include <vka/ecs/PhysicsSystem.h>
#include <vka/ecs/EventSystem.h>
#include <vka/ecs/ScriptSystem.h>
#include <vka/ecs/AnimatorSystem2.h>
#include <vka/ecs/SceneSystem.h>

#include <vka/ecs/Engine.h>

#include <vka/ecs/Components/LightComponent.h>
#include <vka/ecs/Components/SceneComponent.h>
#include <vka/ecs/Components/PrimitiveMaterialComponent.h>

#include <vka/Controllers/FPSController.h>
#include <vka/Controllers/OrbitController.h>

#include <vka/ecs/ScriptObjects/ControlObjectGizmoRotation.h>
#include <vka/ecs/ScriptObjects/ControlObjectGizmoTranslation.h>
#include <vka/ecs/ScriptObjects/ScriptObjectLua.h>

#include <vka/logging.h>

#define WIDTH  1024
#define HEIGHT 768


#ifndef VKA_NO_SDL_WIDGET
#include <vka/widgets/SDLWidget3.h>
#endif

#include <entt/entt.hpp>

VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
    VkDebugReportFlagsEXT      flags,
    VkDebugReportObjectTypeEXT objectType,
    uint64_t                   object,
    size_t                     location,
    int32_t                    messageCode,
    const char*                pLayerPrefix,
    const char*                pMessage,
    void*                      pUserData
)
{
    (void)objectType;
    (void)object;
    (void)location;
    (void)messageCode;
    (void)pUserData;

    if( flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT ) {
        VKA_INFO("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[INFO]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_WARNING_BIT_EXT ) {
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
        //LOG("[WARN]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
    }
    else if( flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT ) {
        //LOG("[PERF]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_WARN("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_ERROR_BIT_EXT ) {
        //LOG("[ERROR]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_ERROR("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    else if( flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT ) {
        //LOG("[DEBUG]" << "[" << pLayerPrefix << "] : " << pMessage << " (" << messageCode << ")");
        VKA_DEBUG("**Validation** [{:s}]: {:s}",pLayerPrefix, pMessage);
    }
    return VK_FALSE;
}

class MyApp : public vka::Application
{
    // Application interface
public:

    vka::ecs::Engine m_engine;
    std::shared_ptr<vka::ecs::SystemBus>          m_SystemBus;

    ~MyApp()
    {
        std::cout << "My App destroyed: " << m_SystemBus.use_count() << std::endl;
    }
    void init()
    {
        m_SystemBus = m_engine.m_SystemBus;
        auto sphere_id = m_SystemBus->createResource<vka::HostTriPrimitive>("sphere1", vka::HostTriPrimitive::Sphere(1.0f,32,32) );

        (void)sphere_id;// = m_SystemBus->createResource<vka::HostTriPrimitive>("sphere1", vka::HostTriPrimitive::Sphere(1.0f) );



        if(1)
        {
            auto E = m_SystemBus->create();
            auto & C = E.create<vka::ecs::CameraComponent>();
            C.environment = m_SystemBus->createResource<vka::ecs::Environment>(vka::uri("rc:environments/papermill.environment"));

            E.create<vka::ecs::TransformComponent>(glm::vec3{0,0,-3}).lookat({0,0,0}, {0,1,0});
            E.create<vka::ecs::ScriptComponent>()
                    // playerInput handler is reponsible for taking raw keyboard/mouse inputs and
                    // converting them into numerical "control values" eg: lookH=3.2
                    // the script stores the values in the Entity's shared variables LUA table: ENTITY.lookH
                    .addScript(vka::uri("rc:scripts/example/playerInput.lua"))
                        //  .setVariable("playerInput", "LOOK_SENSITIVITY_V", 0.45)
                        //  .setVariable("playerInput", "LOOK_SENSITIVITY_H", 0.45)
                        //  .setVariable("playerInput", "FORWARD_SENSITIVITY",0.45)
                        //  .setVariable("playerInput", "SIDE_SENSITIVITY",   0.45)

        #if 0
                    // the cameraOrbitController keeps the entity pointed at a target and uses the
                    // ENTITY.lookH and ENTITY.lookV
                    .addScript(vka::uri("rc:scripts/example/cameraOrbitController.lua"))
                        // Set which target we want to follow with the orbitController
                        .setVariable("cameraOrbitController", "targetEntity", "player")
                        .setVariable("cameraOrbitController", "targetOffset", glm::vec3(0,0,0))
        #else
                    //.addScript( vka::uri("rc:scripts/example/playerInput.lua") )
                    // the playerController script is responsible for taking the numerical control values
                    // and modifying the entity's TRANSFORM component
                    .addScript( vka::uri("file:" VKA_CMAKE_SOURCE_DIR "/share/scripts/example/playerController.lua") );
        #endif
                    ;
        }


            ///================
            ///

            if(0)
            {
                auto E      = m_SystemBus->create("sphereEntity");

                auto chromeMatId = m_SystemBus->createResource<vka::ecs::PBRMaterial>(vka::uri("rc:materials/gold.material") );
                auto dc      = m_SystemBus->getManagedResource<vka::HostTriPrimitive>(sphere_id).getDrawCall();

                E.create<vka::ecs::MeshComponent>(sphere_id, chromeMatId, dc);

                E.create<vka::ecs::TransformComponent>();
                //E.create<vka::ecs::WorldTransformComponent>();
                auto sb = m_SystemBus;
                E.create<vka::ecs::EventComponent>().addEvent( vka::ecs::EventType::FRAME,
                                                               [sb](vka::ecs::EventRef & rr)
                {
                    rr.entity.patch<vka::ecs::TransformComponent>(
                                []( vka::ecs::TransformComponent & Tr)
                                {
                                    Tr.rotate( {0,1,0}, 0.001f);
                                }
                                );

                    auto xx = sb->variables.mouseScreenPosition / sb->variables.screenSize;

                    (void)xx;
                });


                //E.create<vka::ecs::ControlComponent>().attachObject<vka::ecs::ControlObjectGizmoTranslation>();
                //E.create<vka::ecs::ControlComponent>().attachObject<vka::ecs::ControlObjectGizmoRotation>();
            }



            if(0)
            {
                vka::ecs::PBRMaterial m;
                m.unlit = true;
                m.emissiveFactor  = {1.0f,1.0f,1.0f};
                m.baseColorFactor = {1.0f,1.0f,1.0f,1.0f};
                auto unlit = m_SystemBus->createResource<vka::ecs::PBRMaterial>("lightUnlit", std::move(m) );
                auto dc    = m_SystemBus->getManagedResource<vka::HostTriPrimitive>(sphere_id).getDrawCall();

                auto E      = m_SystemBus->create("lightEntity");
                E.create<vka::ecs::MeshComponent>().addMesh(sphere_id, unlit, dc);
                E.create<vka::ecs::TransformComponent>().m_scale = vka::vec3(0.2f);
                E.create<vka::ecs::LightComponent>().color = {1.0f,1.0f,1.f};
                E.create_and_update<vka::ecs::LightComponent>([](vka::ecs::LightComponent & L)
                            {
                                L.color = {1,1,1};
                                L.type = vka::ecs::LightType::POINT;
                            }
                            );
                E.create<vka::ecs::EventComponent>().addEvent( vka::ecs::EventType::FRAME,
                                                               [&](vka::ecs::EventRef & rr)
                {
                    rr.entity.patch<vka::ecs::TransformComponent>(
                                []( vka::ecs::TransformComponent & Tr)
                                {
                                    static float t = 0.0f;
                                    t+=0.016f;
                                    Tr.m_position = 2.0f * glm::vec3( std::cos(t), 0.0f, std::sin(t));
                                }
                                );

                });

            }
        //==================================================================

        if(1)
        {
            auto gltfID = m_SystemBus->createResource<vka::ecs::GLTFScene>( vka::uri("rc:models/download/DamagedHelmet.glb"));
            auto E      = m_SystemBus->create();
            auto S = E.create<vka::ecs::SceneComponent2>(gltfID);

            E.create<vka::ecs::TransformComponent>();

            E.create<vka::ecs::EventComponent>().addEvent( vka::ecs::EventType::FRAME,
                                                           [&](vka::ecs::EventRef & rr)
            {
                rr.entity.patch<vka::ecs::TransformComponent>(
                            []( vka::ecs::TransformComponent & Tr)
                            {
                                Tr.rotate( {0,1,0}, 0.001f);
                            });

                if( rr.count > 1000 )
                    rr.stop();
            });
           // E.create<vka::ecs::ControlComponent>().attachObject<vka::ecs::ControlObjectGizmoTranslation>();
        }

        if(0)
        {

            auto scene_id = m_SystemBus->createResource<vka::ecs::GLTFScene>(vka::uri("rc:models/ybot_full.glb"));

            auto E      = m_SystemBus->create();

            E.create<vka::ecs::NameComponent>("player");

            E.create<vka::ecs::SceneComponent2>(scene_id);
            E.create<vka::ecs::TransformComponent>().m_position = {0,0,0};
            auto & A = E.create<vka::ecs::AnimatorComponent2>();
            //A.sceneId = scene_id;
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(0); // idle
            }

            //  ==== walk forward/left/right
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(1); // forward
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(2); // strafe left
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(3); // strafe right
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeBlend>();
                A.node<vka::ecs::AnimatorNodeBlend>(i1).inputNodeIndex = {2,1,3};
                A.node<vka::ecs::AnimatorNodeBlend>(i1).blendValue = 0.1f;
            }

            //  ==== run forward/left/right
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(4); // run-forward
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(5); // run-strafe left
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(6); // run-strafe right
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeBlend>();
                A.node<vka::ecs::AnimatorNodeBlend>(i1).inputNodeIndex = {6,5,7};
                A.node<vka::ecs::AnimatorNodeBlend>(i1).blendValue = 0.1f;
            }


            //  ==== back left/right
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(2); // back left
                A.node<vka::ecs::AnimatorNodeInput>(i1).reverse = true; // back left
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeInput>();
                A.node<vka::ecs::AnimatorNodeInput>(i1).setAnimationIndex(3); // back right
                A.node<vka::ecs::AnimatorNodeInput>(i1).reverse = true; // back left
            }
            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeBlend>();
                A.node<vka::ecs::AnimatorNodeBlend>(i1).inputNodeIndex = {9,10};
                A.node<vka::ecs::AnimatorNodeBlend>(i1).blendValue = 0.5f;
            }


            {
                auto i1 = A.newNode<vka::ecs::AnimatorNodeBlend>();
                A.node<vka::ecs::AnimatorNodeBlend>(i1).inputNodeIndex = {11,4,0,8};
                A.node<vka::ecs::AnimatorNodeBlend>(i1).blendValue = 0.1f;
            }
            A.rootNode = 12;

            E.create<vka::ecs::EventComponent>().addEvent( vka::ecs::EventType::FRAME,
                                                           [&](vka::ecs::EventRef & R)
            {
                glm::vec2 x = m_SystemBus->variables.mouseScreenPosition / m_SystemBus->variables.screenSize;

                R.entity.get<vka::ecs::AnimatorComponent2>().node<vka::ecs::AnimatorNodeBlend>(4).blendValue  = x.x * 2.0f;
                R.entity.get<vka::ecs::AnimatorComponent2>().node<vka::ecs::AnimatorNodeBlend>(8).blendValue  = x.x * 2.0f;
                R.entity.get<vka::ecs::AnimatorComponent2>().node<vka::ecs::AnimatorNodeBlend>(11).blendValue = x.x;

                R.entity.get<vka::ecs::AnimatorComponent2>().node<vka::ecs::AnimatorNodeBlend>(12).blendValue = x.y * 3.0f ;

                R.entity.get<vka::ecs::AnimatorComponent2>().playSpeed = 3.0f;//x.y * 48.0f;
            });
        }

        //m_SystemBus->initShell("16-RenderSystem3.socket");
    }




    //===============================================================================================================================
    // Vulkan APP Specific code
    //===============================================================================================================================

    /**
     * @brief initResources
     *
     * This will be called right after system gets created.
     * It can be used to initialize any vulkan related resources
     * that are not related to the swapchain.
     */
    void initResources() override
    {
        m_SystemBus = m_engine.m_SystemBus;
    //profiler::dumpBlocksToFile("test_profile.prof");
        glslang::InitializeProcess();

        addPath(VKA_CMAKE_SOURCE_DIR  "/share");
        addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
        addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
        addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
        addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");

        m_SystemBus->rootPaths = getRootPaths();


        m_engine.m_ScriptSystem->addIncludePath(VKA_CMAKE_SOURCE_DIR "/share/scripts" );

        vka::ecs::RenderSystem3CreateInfo ii;
        ii.device         = m_device;
        ii.physicalDevice = m_physicalDevice;
        ii.instance       = m_instance;

        m_engine.m_RenderSystem->init(ii);

        // connect all the systems together
        m_engine.connectSystems();
    }


    /**
     * @brief releaseResources
     *
     * This method must destroy all
     * non-swapchain related resources that were
     *
     */
    void releaseResources() override
    {
        m_engine.clear();

        m_engine.disconnectSystems();
        m_engine.m_RenderSystem->releaseResources();

        glslang::FinalizeProcess();
        //std::cout << "release non-graphics resources" << std::endl;
    }


    /**
     * @brief initSwapChainResources
     *
     * This is called right after the swapchain is created.
     * You can use this to initialize any swapchain related resources,
     * for example, render targets (because they change with swapchain size)
     */
    void initSwapChainResources() override
    {
        m_engine.m_RenderSystem->initSwapchainResources(swapchainImageSize(),
                                               concurrentFrameCount(),
                                               getDefaultRenderPass());

        if( !m_init )
        {
            m_init= true;
            init();
        }
        startThreadPolling();
    }


    void releaseSwapChainResources() override
    {        
        m_engine.m_RenderSystem->releaseSwapchainResources();

        VKA_DEBUG("release swapchain releated resources");
        stopThreadPolling();
    }




    /**
     * @brief render
     * @param frame
     *
     * This is the widget's main vulkan render function.
     * use this to actually record your command buffers.
     * submission of commandbuffers are automatically done
     * at the end
     */
    void render(vka::ApplicationFrame & frame) override
    {
        assert( frame.currentFrameBuffer );
        assert( frame.currentSwapchainImage );
        assert( frame.currentSwapchainImageView );
        vka::ecs::EvtGameLoop E;
        E.dt = std::chrono::duration<double>(frame.frameTime - frame.lastFrameTime).count();
        m_SystemBus->triggerEvent(E);

        m_engine.step(E.dt);

        m_engine.m_RenderSystem->render(frame);
    }

    void mouseMoveEvent(const vka::EvtInputMouseMotion *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }
    void mousePressEvent(const vka::EvtInputMouseButton *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }
    void mouseReleaseEvent(const vka::EvtInputMouseButton *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }

    void mouseWheelEvent(const vka::EvtInputMouseWheel *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }

    void keyPressEvent(const vka::EvtInputKey *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }
    void keyReleaseEvent(const vka::EvtInputKey *e) override
    {
        m_SystemBus->triggerEvent(*e);
    }
    void fileDropEvent(const vka::EvtInputFileDrop *e) override
    {
        (void)e;
    }

    bool m_init=false;
    //=======================

    vka::line3 m_mouseProjection;
};

#if !defined VKA_NO_SDL_WIDGET
int main(int argc, char *argv[])
{
    (void)argc;
    (void)argv;
    // The basic usage of this

    // create a vulkan window widget
    vka::SDLVulkanWidget3 vulkanWindow;

    vka::SDLVulkanWidget3::CreateInfo c;
    c.width = 1024;
    c.height = 768;
    c.windowTitle = "title";
    c.depthFormat = VK_FORMAT_D32_SFLOAT_S8_UINT;
    c.enabledFeatures.fillModeNonSolid = true;

    // create the window and initialize
    // the vka::System
    vulkanWindow.create(c);


    vka::SystemCreateInfo2 sysCreateInfo;
    sysCreateInfo.device              = vulkanWindow.getDevice();
    sysCreateInfo.instance            = vulkanWindow.getInstance();
    sysCreateInfo.physicalDevice      = vulkanWindow.getPhysicalDevice();
    sysCreateInfo.queueGraphics       = vulkanWindow.getGraphicsQueue();

    // These values cannot be retrieved
   // sysCreateInfo.queuePresentFamily  = m_presentQueueFamily;
   // sysCreateInfo.queueGraphicsFamily = m_graphicsQueueFamily;
   // sysCreateInfo.surface             = m_surface;
   // sysCreateInfo.queuePresent        = m_device.getQueue( static_cast<uint32_t>(m_presentQueueFamily) , 0u );

    vka::System::createSystem(sysCreateInfo);

    // instantiate your application
    MyApp app;


    vulkanWindow.exec( &app );


    return 0;
}

#include <vkw/SDLVulkanWindow_USAGE.inl>
#include <vkw/SDLVulkanWindow_INIT.inl>

#endif

