#include <iostream>

#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock
#include <sstream>
#include <regex>
#include "catch.hpp"

#include <fmt/format.h>
#include <vulkan/vulkan.hpp>
#include <vka/math/linalg.h>

#include <vka/ecs/RenderSystem2/Renderer.h>

#include <vka/utils/GLSLCompiler.h>

SCENARIO("")
{
    using Renderer = vka::ecs::NewRenderer;

    glslang::InitializeProcess();

    auto compileShaderModule = [&](const std::string & src, EShLanguage language)
    {
        vka::GLSLCompiler compiler;
        try {
            auto spv   = compiler.compile(src, language  );
            return spv;
        } catch ( std::exception & e) {
            std::cout << compiler.getLog() << std::endl;
            std::cout << compiler.getDebugLog() << std::endl;
            std::cout << e.what() << std::endl;
            throw e;

        }
    };

    if(1)
    {
        std::string testVertexShader = R"foo(
//=========================================================================
// These share the same set/binding because it is backed by a single
// storage buffer. This essentially creates 2 different views into
// the buffer..
//
// Uniforms should obey the proper alignment, that is they must align
// to vec2 or to vec4
//
// All uniforms must start with u_
//=========================================================================
uniform mat4 u_modelMatrix;
uniform vec3 u_color;
uniform int  u_boneIndexOffset;


//=========================================================================
// Storage arrays are used to store multiple objects of the same type.
// There is a maximum of 4 storageArrays per shader stage.
// To access the storage data, you must access it as follows:
//=========================================================================
storageArray mat4 s_boneMatrices;
//=========================================================================


VSOut MAIN()
{
    VSOut OUT;

    vec4 locPos = s_boneMatrices[9] * vec4( in_POSITION, 1.0);

    OUT.glPosition = GLOBAL.PROJECTION_VIEW * locPos;
    OUT.position   = locPos.xyz;
    OUT.normal     = in_NORMAL;
    OUT.texCoord_0 = in_TEXCOORD_0;
    OUT.color      = vec3(1,1,1);

    return OUT;
}

)foo";


        Renderer::ShaderBuilder S1;


        //newShader = testFragmentShader;
        S1.setVertexShaderMain(testVertexShader);
        auto vf = S1.buildVertexShader();

        std::cout << "---\n" << vf << "\n-----\n";
        compileShaderModule( vf, EShLanguage::EShLangVertex);

    }

    if(1)
    {
        std::string testFragmentShader = R"foo(
//=========================================================================
// These share the same set/binding because it is backed by a single
// storage buffer. This essentially creates 2 different views into
// the buffer..
//
// Uniforms should obey the proper alignment, that is they must align
// to vec2 or to vec4
//
// All uniforms must start with u_
//=========================================================================
uniform vec4   u_baseColorFactor          ;
uniform float  u_metallicFactor           ;
uniform float  u_roughnessFactor          ;
uniform int    u_baseColorTexture         ;
uniform int    u_metallicRoughnessTexture ;
uniform int    u_normalTexture            ;
uniform int    u_occlusionTexture         ;
uniform int    u_emissiveTexture          ;
uniform int    u_unused                   ;


//=========================================================================
// Storage arrays are used to store multiple objects of the same type.
// There is a maximum of 4 storageArrays per shader stage.
// To access the storage data, you must access it as follows:
//=========================================================================
storageArray mat4 s_boneMatrices;
//=========================================================================

FSOut MAIN()
{
    FSOut OUT;

    OUT.position = f_POSITION;
    OUT.normal   = f_NORMAL;
    OUT.albedo   = vec3(1,0,0);
    OUT.metallic = 1.0f;
    OUT.roughness = 1.0f;

    return OUT;
}

)foo";


        Renderer::ShaderBuilder S1;


        //newShader = testFragmentShader;
        S1.setFragmentShaderMain(testFragmentShader);
        auto vf = S1.buildFragmentShader();

        std::cout << "---\n" << vf << "\n-----\n";
        compileShaderModule( vf, EShLanguage::EShLangFragment);

    }
    glslang::FinalizeProcess();
}
