#include <vka/ecs/Managers/ResourceManager5.h>
#include "catch.hpp"
#include <iostream>

using namespace vka;

struct MyResource
{
    int x=3;
};

struct OtherData
{
    int z=10;
};

SCENARIO("createResource")
{
    ecs::ResourceManager5 MM;
    ecs::ResourceManager5 const & MMc = MM;

    auto id = MM.resourceCreateAndEmplace( MyResource{5} );

    REQUIRE( MM.resourceGet(id).x == MMc.resourceGet(id).x);

    REQUIRE( MM.resourceIsValid(id) );

    REQUIRE( MM.resourceDestroy(id) );

    REQUIRE( !MM.resourceIsValid(id));
}

SCENARIO("lock resource")
{
    ecs::ResourceManager5 MM;

    auto id = MM.resourceCreateAndEmplace( MyResource{5} );

    REQUIRE( MM.resourceIsValid(id) );

    auto l = MM.resourceLock(id);

    REQUIRE( !MM.resourceDestroy(id) );

    l.reset();

    REQUIRE( MM.resourceDestroy(id) );

    REQUIRE( !MM.resourceIsValid(id));
}


SCENARIO("Emplacing additional")
{
    ecs::ResourceManager5 MM;

    auto id = MM.resourceCreateAndEmplace( MyResource{5} );

    REQUIRE( MM.resourceIsValid(id) );

    MM.resourceEmplaceData(id, OtherData() );

    REQUIRE( MM.resourceHasData<OtherData>(id));

    MM.resourceRemoveData<OtherData>(id);

    REQUIRE( !MM.resourceHasData<OtherData>(id));
}
