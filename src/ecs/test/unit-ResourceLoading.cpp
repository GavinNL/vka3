#include "catch.hpp"
#include <iostream>

#include <unordered_map>
#include <set>
#include <vka/ecs/ECS.h>
#include <vka/utils/uri.h>

#include <vka/utils/HostImageLoader.h>
#include <vka/ecs/Managers/ResourceManager4.h>

#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/SystemBus.h>

#include <vka/ecs/Loaders/CubeLoader.h>

#include <vka/ecs/Loaders/TextureLoader.h>
#include <vka/ecs/Loaders/MaterialLoader.h>
#include <vka/ecs/Loaders/GLTFLoader.h>
#include <vka/ecs/Loaders/CubeLoader.h>
#include <vka/ecs/Loaders/EnvironmentLoader.h>


using namespace vka::ecs;

SCENARIO("")
{
    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");


    auto gold_id = sb->resourceCreate<HostTexture2D>(vka::uri("rc:textures/ground/clay.png"));

    auto f = sb->resourceHostLoadBackground(gold_id);

    REQUIRE( f.isSet(ResourceFlagBits::eHostBackgroundLoading));

    while( !sb->resourceIsHostLoaded(gold_id) );


    sb->waitThreadPoolIdle();
}




SCENARIO("Loading Cubes")
{
    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");


    auto gold_id = sb->resourceCreate<HostTextureCube>(vka::uri("rc:environments/green_hills/irradiance.json"));

    auto f = sb->resourceHostLoadBackground(gold_id);

    REQUIRE( f.isSet(ResourceFlagBits::eHostBackgroundLoading));

    while( !sb->resourceIsHostLoaded(gold_id) );


    sb->waitThreadPoolIdle();
}



SCENARIO("Loading GLTF")
{
    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");


    auto gold_id = sb->resourceCreate<GLTFScene2>(vka::uri("rc:models/download/DamagedHelmet.glb"));

    auto f = sb->resourceHostLoadBackground(gold_id);

    REQUIRE( f.isSet(ResourceFlagBits::eHostBackgroundLoading));

    while( !sb->resourceIsHostLoaded(gold_id) );

    sb->waitThreadPoolIdle();
}


SCENARIO("Loading Material")
{
    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");


    auto gold_id = sb->resourceCreate<PBRMaterial>(vka::uri("rc:materials/paving_stone.material"));

    auto f = sb->resourceHostLoadBackground(gold_id);

    REQUIRE( f.isSet(ResourceFlagBits::eHostBackgroundLoading));

    while( !sb->resourceIsHostLoaded(gold_id) );

    sb->waitThreadPoolIdle();
}


SCENARIO("Loading Environment")
{
    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");


    auto gold_id = sb->resourceCreate<PBRMaterial>(vka::uri("rc:environments/green_hills.environment"));

    auto f = sb->resourceHostLoadBackground(gold_id);

    REQUIRE( f.isSet(ResourceFlagBits::eHostBackgroundLoading));

    while( !sb->resourceIsHostLoaded(gold_id) );

    sb->waitThreadPoolIdle();
}

