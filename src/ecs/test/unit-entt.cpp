
#include <iostream>
#include <vka/ecs/ECS.h>
#include <entt/entity/helper.hpp>
#include <entt/core/hashed_string.hpp>
#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock


#include "catch.hpp"
using namespace entt::literals;
struct MyComp
{
    int x;
};

struct MyComp2
{
    int y;
};

struct Project
{
    entt::registry m_registry;

    template<typename Comp>
    void onDestroy(entt::registry &r, entt::entity e)
    {
        (void)r;
        (void)e;
        onDestroy(r, e, m_registry.get<Comp>(e));
    }

    void onDestroy(entt::registry &r, entt::entity e, MyComp &C)
    {
        (void)r;
        (void)e;
        (void)C;
    }

    void onDestroy(entt::registry &r, entt::entity e, MyComp2 &C)
    {
        (void)r;
        (void)e;
        (void)C;
    }

    Project()
    {
        m_registry.on_destroy<MyComp>().connect<  &Project::onDestroy<MyComp>   >(*this);
        m_registry.on_destroy<MyComp2>().connect< &Project::onDestroy<MyComp2>  >(*this);
    }


};

//template<>
//inline void Project::onDestroy<MyComp>(entt::registry & r, entt::entity e )
//{
//    (void)r;
//    (void)e;
//}

//template<>
//inline void Project::onDestroy<MyComp2>(entt::registry & r, entt::entity e )
//{
//    (void)r;
//    (void)e;
//}

//Project::Project()


SCENARIO("enemy1")
{
    Project  Pr;
    entt::registry R;

    using enemy = entt::tag<"enemy"_hs>;

    std::vector<entt::entity> entities;
    for(uint32_t i=0;i<200000;i++)
    {
        entities.push_back( R.create() );
    }
    // obtain a time-based seed:
    //auto seed = std::chrono::system_clock::now().time_since_epoch().count();


    auto engine = std::default_random_engine();
    std::shuffle(entities.begin(), entities.end(), engine );

    std::vector<entt::entity> _out;
    _out.reserve(75000);

    REQUIRE( 0 == R.size<enemy>() );

    for(uint32_t i=0;i<75000;i++)
    {
        R.emplace_or_replace< enemy >( entities[i] );
    }

    size_t count = R.size<enemy>();

    REQUIRE( 75000 == count );

    for(auto e : R.view<enemy>() )
    {
        _out.push_back(e);
    }

    REQUIRE( 75000 == _out.size() );

    for(auto e : _out)
    {
        R.remove<enemy>(e);
    }

    REQUIRE( 0 == R.size<enemy>() );
}

SCENARIO("Testing replace")
{

    entt::registry R;
    std::vector<entt::entity> entities;

    for(size_t i=0;i<10;i++)
    {
    }

    struct System
    {
        void onDestroy(entt::registry & r, entt::entity e)
        {
            std::cout << "onDestroy called" << std::endl;
            (void)r;
            (void)e;
        }
        void onCreate(entt::registry & r, entt::entity e)
        {
            std::cout << "onCreate called" << std::endl;
            (void)r;
            (void)e;
        }
        void onUpdate(entt::registry & r, entt::entity e)
        {
            std::cout << "onUpdate called" << std::endl;
            auto & C = r.get<MyComp>(e);
            (void)r;
            (void)e;
            (void)C;
        }
    };


    System s;


    R.on_update<MyComp>().connect< &System::onUpdate >(s);
    R.on_construct<MyComp>().connect< &System::onCreate >(s);
    R.on_destroy<MyComp>().connect< &System::onDestroy >(s);


    auto e = R.create();
    auto e2 = R.create();


    R.emplace<MyComp>(e).x = 2;
    entities.push_back(e);

    REQUIRE( R.get<MyComp>( entities.front() ).x == 2);

    auto init = &R.get<MyComp>(entities.front() );
    R.emplace_or_replace<MyComp>(e).x = 3;
    R.emplace_or_replace<MyComp>(e2).x = 4;

    REQUIRE( R.get<MyComp>( entities.front() ).x == 3);
    auto final = &R.get<MyComp>(entities.front() );

    REQUIRE(init==final);
}

#if 0
SCENARIO("enemys")
{

    entt::registry R;

    using enemy = entt::integral_constant<4>;

    std::vector<entt::entity> entities;
    for(uint32_t i=0;i<200000;i++)
    {
        entities.push_back( R.create() );
    }
    // obtain a time-based seed:
    //auto seed = std::chrono::system_clock::now().time_since_epoch().count();


    auto engine = std::default_random_engine();

    BENCHMARK_ADVANCED("advanced")(Catch::Benchmark::Chronometer meter)
    {
        std::shuffle(entities.begin(), entities.end(), engine );

        std::vector<entt::entity> _out;
        _out.resize(200000);
        //std::cout << _out.size() << std::endl;
        meter.measure(
            [&]
            {
                auto b = _out.begin();

                REQUIRE( _out.size() == 200000);

                REQUIRE( 0 == std::distance(_out.begin(), b) );

                for(uint32_t i=0;i<75000;i++)
                {
                    R.emplace_or_replace< enemy >( entities[i] );
                }

                size_t count = R.size<enemy>();

                REQUIRE( 75000 == count );

                for(auto e : R.view<enemy>() )
                {
                    *b = e;
                    ++b;
                }

                REQUIRE( 75000 == std::distance(_out.begin(), b) );

                for(auto i=_out.begin();i!=b;i++)
                {
                    R.remove<enemy>(*i);
                }

                REQUIRE( 0 == R.size<enemy>() );
            });

    };

}
#endif
