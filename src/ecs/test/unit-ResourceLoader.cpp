#include "catch.hpp"
#include <iostream>

#include <unordered_map>
#include <set>
#include <vka/ecs/ECS.h>
#include <vka/utils/uri.h>

#include <vka/utils/HostImageLoader.h>
#include <vka/ecs/Managers/ResourceManager4.h>

#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/SystemBus.h>

#include <vka/ecs/Loaders/CubeLoader.h>

#include <vka/ecs/Loaders/TextureLoader.h>
#include <vka/ecs/Loaders/MaterialLoader.h>
#include <vka/ecs/Loaders/GLTFLoader.h>
#include <vka/ecs/Loaders/CubeLoader.h>
#include <vka/ecs/Loaders/EnvironmentLoader.h>


using namespace vka::ecs;


#define STB_IMAGE_IMPLEMENTATION
#define VKA_IMAGE_IMPLEMENTATION

#define VKAI_NO_STDIO
#define STBI_NO_STDIO


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wunused-function"

#include <vka/utils/vka_image.h>

#pragma GCC diagnostic pop
