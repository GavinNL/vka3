#include <iostream>

#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CMesh.h>

#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

#include <vka/ecs/json.h>
#define VKA_USE_DEPRECATED
#include "catch.hpp"
#include <vka/ecs/SystemBus.h>

using namespace vka::ecs;


SCENARIO("Test CMain")
{
    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");


    WHEN("We create an entity")
    {
        entt::entity parent = entt::null;
        entt::entity child  = entt::null;
        if( auto E = sb->createEntity()  )
        {
            parent = E.entity;
            if( auto C = E.createChild () )
            {
                C.create<vka::ecs::CPosition>();
                child = C.entity;
            }
        }

        REQUIRE( sb->registry.alive() == 2);

        WHEN("We destroy the parent entity")
        {
            sb->destroyEntity(parent);

            REQUIRE( !sb->registry.valid(parent) );
            REQUIRE( !sb->registry.valid(child) );
        }
    }
}


SCENARIO("Test RecurseFindChildren")
{
    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");


    auto GP = sb->createEntity();

    auto P1 = GP.createChild();
    auto P2 = GP.createChild();

    auto C1 = P1.createChild();
    auto C2 = P1.createChild();

    auto C3 = P2.createChild();
    auto C4 = P2.createChild();

    (void)C1;
    (void)C2;
    (void)C3;
    (void)C4;

    std::vector<entt::entity> ent;

    sb->_recurseGetChildren(GP.entity, ent);

    REQUIRE( ent.size() == 6);

    REQUIRE( ent[5] == C4.entity);
    REQUIRE( ent[4] == C3.entity);
    REQUIRE( ent[3] == C2.entity);
    REQUIRE( ent[2] == C1.entity);

    REQUIRE( ent[1] == P2.entity);
    REQUIRE( ent[0] == P1.entity);

    sb->destroyEntity(GP.entity);
}


void split4(const std::string& str, std::vector<std::string> & cont,
              const std::string& delims = " ")
{
    std::size_t current, previous = 0;
    current = str.find_first_of(delims);
    while (current != std::string::npos) {
        cont.push_back(str.substr(previous, current - previous));
        previous = current + 1;
        current = str.find_first_of(delims, previous);
    }
    cont.push_back(str.substr(previous, current - previous));
}
static void ls( SystemBus* sb, std::string absPath)
{
    if( vka::fs::is_directory(absPath))
    {
        absPath = vka::fs::parent_path(absPath);
    }
    auto fn = vka::fs::filename(absPath);
    auto en = vka::fs::filename(vka::fs::parent_path(absPath));

    std::vector<std::string> dirs;
    split4(absPath, dirs, "/");

    std::reverse( dirs.begin(), dirs.end());
    dirs.pop_back();
    for(auto & x : dirs)
    {

       // sb->registry.get<CMain>(e);
        std::cout << x << std::endl;
    }



    (void)sb;
}


SCENARIO("test")
{
    ls(nullptr, "/4/2/1/3/system");
}


#define STB_IMAGE_IMPLEMENTATION
#define VKA_IMAGE_IMPLEMENTATION

#define VKAI_NO_STDIO
#define STBI_NO_STDIO


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wunused-function"

#include <vka/utils/vka_image.h>

#pragma GCC diagnostic pop
