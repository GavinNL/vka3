#include <iostream>

#include <vka/ecs/Components/CTransform.h>
#include <vka/ecs/Components/CMesh.h>

#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

#include <vka/ecs/json.h>

#include "catch.hpp"
#include <vka/ecs/Flags.h>

using namespace vka::ecs;

enum class FlagBits : uint32_t
{
    eF1 = (1u<<0),
    eF2 = (1u<<1),
    eF3 = (1u<<2)
};



SCENARIO("test flags")
{
    Flags<FlagBits> F;

    REQUIRE(  F.isSet(FlagBits::eF1) == false);
    REQUIRE(  F.isSet(FlagBits::eF2) == false);
    REQUIRE(  F.isSet(FlagBits::eF3) == false);

    F.set(FlagBits::eF2);
    REQUIRE(  F.isSet(FlagBits::eF1) == false);
    REQUIRE(  F.isSet(FlagBits::eF2) == true);
    REQUIRE(  F.isSet(FlagBits::eF3) == false);

    F.set(FlagBits::eF1);
    REQUIRE(  F.isSet(FlagBits::eF1) == true);
    REQUIRE(  F.isSet(FlagBits::eF2) == true);
    REQUIRE(  F.isSet(FlagBits::eF3) == false);

    F.set(FlagBits::eF3);
    REQUIRE(  F.isSet(FlagBits::eF1) == true);
    REQUIRE(  F.isSet(FlagBits::eF2) == true);
    REQUIRE(  F.isSet(FlagBits::eF3) == true);

    F.clear(FlagBits::eF1);
    REQUIRE(  F.isSet(FlagBits::eF1) == false);
    REQUIRE(  F.isSet(FlagBits::eF2) == true);
    REQUIRE(  F.isSet(FlagBits::eF3) == true);

    F.clear(FlagBits::eF3);
    REQUIRE(  F.isSet(FlagBits::eF1) == false);
    REQUIRE(  F.isSet(FlagBits::eF2) == true);
    REQUIRE(  F.isSet(FlagBits::eF3) == false);

    F.clear(FlagBits::eF2);
    REQUIRE(  F.isSet(FlagBits::eF1) == false);
    REQUIRE(  F.isSet(FlagBits::eF2) == false);
    REQUIRE(  F.isSet(FlagBits::eF3) == false);
}
