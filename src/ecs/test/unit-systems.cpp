#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

#include "catch.hpp"
#include <vka/ecs/SystemBase2.h>

using namespace vka::ecs;


struct BuildCollidersSubSystem : public SystemBaseInternal
{

    void onStart() override
    {
        //on_construct<CCollider>().connect<&entt::registry::emplace_or_replace<update_collider>>();
        //on_update<CCollider>().connect<&entt::registry::emplace_or_replace<update_collider>>();
        std::cout <<  "BuildCollidersSubSystem::onStart" << std::endl;
    }
    void onStop() override
    {
        //on_construct<CCollider>().disconnect<&entt::registry::emplace_or_replace<update_collider>>();
        //on_update<CCollider>().disconnect<&entt::registry::emplace_or_replace<update_collider>>();
        std::cout <<  "BuildCollidersSubSystem::onStop" << std::endl;
    }

    void onUpdate() override
    {
        std::cout << "BuildCollidersSubSystem::onUpdate" << std::endl;
        //clear<update_collider>();
    }
};


struct BuildColliders : public SystemBaseInternal
{
    using update_collider = entt::tag<"BuildColliders::update_collider"_hs>;

    void onStart() override
    {
        getOrCreateSystem<BuildCollidersSubSystem>();
        //on_construct<CCollider>().connect<&entt::registry::emplace_or_replace<update_collider>>();
        //on_update<CCollider>().connect<&entt::registry::emplace_or_replace<update_collider>>();
    }
    void onStop() override
    {
        std::cout <<  "BuildColliders::onStop" << std::endl;
        //on_construct<CCollider>().disconnect<&entt::registry::emplace_or_replace<update_collider>>();
        //on_update<CCollider>().disconnect<&entt::registry::emplace_or_replace<update_collider>>();
    }

    void onUpdate() override
    {
        std::cout << "BuildColliders::onUpdate" << std::endl;
        //clear<update_collider>();
    }
};

SCENARIO("TEST")
{
    auto S = std::make_shared<SystemBus>();

    S->getOrCreateSystem2<BuildColliders>();

    S->step(0.016);

    S->disconnectAllSystems();
}
