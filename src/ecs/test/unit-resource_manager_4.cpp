#include <vka/ecs/Managers/ResourceManager4.h>
//#include <test_init_functions.hpp>
//#include <vka/Renderers/Uploaders.h>
#include "catch.hpp"
#include <iostream>

struct MyResource
{
    int x;
};


struct MyResourceManager
{
    using manager_type = vka::ecs::ResourceManager4<MyResource>;

    void init(manager_type & M )
    {
        M.registry().on_construct<MyResource>().connect< &MyResourceManager::onConstruct>(*this);
        M.registry().on_destroy<MyResource>().connect< &MyResourceManager::onDestroy>(*this);
    }

    void onConstruct(entt::registry & r, entt::entity e)
    {
        auto & R = r.get<MyResource>(e);
        std::cout << "Constructed: " << R.x << std::endl;
    }
    void onDestroy(entt::registry & r, entt::entity e)
    {
        auto & R = r.get<MyResource>(e);
        auto & N = r.get<manager_type::_nameComp>(e);

        std::cout << "Destroyed: " << R.x << "  " << N.name << std::endl;
        (void)r;
        (void)e;
    }
};


SCENARIO("Resource_t")
{
    MyResourceManager MM;

    vka::ecs::ResourceManager4<MyResource> M;
    MM.init(M);

    auto id = M.createResource("r1", MyResource{4});

    // must have unique names
    REQUIRE_THROWS( M.createResource("r1", MyResource{4}) );

    REQUIRE( M.size() == 1);
    M.destroyResource(id);

    M.createResource("r1", MyResource{1});
    M.createResource("r2", MyResource{2});
    M.createResource("r3", MyResource{3});
    M.createResource("r4", MyResource{4});
    M.createResource("r5", MyResource{5});

    M.destroyAll();

    REQUIRE( M.size() == 0 );
}



