#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

#define VKA_USE_DEPRECATED
#include "catch.hpp"
#include <vka/ecs/SystemBus.h>

using namespace vka::ecs;

SCENARIO("traverseChildren")
{
    std::shared_ptr<SystemBus> sb = std::make_shared<SystemBus>();

    struct Pos
    {
        float x = 0.0f;
    };

    auto P = sb->createEntity();
         P.emplace_or_replace<Pos>(Pos{0});
    auto P1 = P.createChild();
         P1.emplace_or_replace<Pos>(Pos{1});
      auto P11 = P1.createChild();
           P11.emplace_or_replace<Pos>(Pos{2});

      auto P12 = P1.createChild();
           P12.emplace_or_replace<Pos>(Pos{3});

    auto P2 = P.createChild();
      auto P21 = P1.createChild();
           P21.emplace_or_replace<Pos>(Pos{3});
      auto P22 = P1.createChild();
           P22.emplace_or_replace<Pos>(Pos{3});

    (void)P11;
    (void)P12;
    (void)P21;
    (void)P22;
    (void)P2;

    sb->traverseChildren(P.entity,
                         [](auto e, int i)
    {
        (void)e;
       std::cout << static_cast<uint32_t>(i) << std::endl;
       return i+1;
    }, 1);

    std::cout << "Traversing parents" << std::endl;
    sb->traverseParents(P22.entity,
                         [](auto e, int i)
    {
        (void)e;
       std::cout << static_cast<uint32_t>(i) << std::endl;
       return i+1;
    }, 1);


    auto & r = sb->registry;
    auto final =
    sb->traverseParents(P22.entity,
                         [&](auto e, float i)
    {
        (void)e;
        return r.get<Pos>(e).x + i;
    }, 0.0f);

    REQUIRE( final == Approx( 4.0f) );
}
