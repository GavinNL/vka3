#include <iostream>

#include <vka/ecs/Components/CMesh.h>
#include <iostream>     // std::cout
#include <algorithm>    // std::shuffle
#include <array>        // std::array
#include <random>       // std::default_random_engine
#include <chrono>       // std::chrono::system_clock

#include <vka/ecs/json.h>

#include "catch.hpp"
#include <vka/ecs/SystemBus.h>

using namespace vka::ecs;

SCENARIO("asdfasdf")
{

  //  using namespace vka::ecs;

//    MaterialPrimitiveComponent C;

    nlohmann::json J;
    J["vel"] = {1,2,3,4};
    J["quat"] = {1,2,3,4};


    {
        glm::vec3 v;
        from(J, "vel", v);

        REQUIRE( v[0] == Approx(1));
        REQUIRE( v[1] == Approx(2));
        REQUIRE( v[2] == Approx(3));
    }
    {
        glm::uvec3 v;
        from(J, "vel", v);

        REQUIRE( v[0] == 1u);
        REQUIRE( v[1] == 2u);
        REQUIRE( v[2] == 3u);
    }
    {
        glm::vec4 v;
        from(J, "vel", v);

        REQUIRE( v[0] == Approx(1));
        REQUIRE( v[1] == Approx(2));
        REQUIRE( v[2] == Approx(3));
        REQUIRE( v[3] == Approx(4));
    }
    {
        glm::uvec4 v;
        from(J, "vel", v);

        REQUIRE( v[0] == 1u);
        REQUIRE( v[1] == 2u);
        REQUIRE( v[2] == 3u);
        REQUIRE( v[3] == 4u);
    }
    {
        glm::quat v;
        from(J, "quat", v);

        REQUIRE( v[0] == Approx(1));
        REQUIRE( v[1] == Approx(2));
        REQUIRE( v[2] == Approx(3));
        REQUIRE( v[3] == Approx(4));
    }
}


SCENARIO("Test array of strings")
{
    auto json_str =
R"foo(
{
    "test" : [ "str1", "str2" ]
}
)foo";
    auto J = nlohmann::json::parse(json_str);

    auto v = J.at("test").get< std::vector< std::string > >();

    REQUIRE( v[0] == "str1");
    REQUIRE( v[1] == "str2");
}



SCENARIO("test serialization")
{
    auto sb = std::make_shared<vka::ecs::SystemBus>();

    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
    sb->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");

    auto E = sb->createEntity();


    E.createChild();
    E.createChild();
    auto C = E.createChild();
    (void)C;
}







// path = "#/object1/array1/3/object2/temp;
//
// returns a reference to the following
// J["object1"]["array1"][3]["object2"]["temp]
auto getIterator(vka::json & J, const std::string path)
{
    auto splitString = [](const std::string& str, std::vector<std::string> & cont,
                  const std::string& delims = " ")
    {
        std::size_t current, previous = 0;
        current = str.find_first_of(delims);
        while (current != std::string::npos) {
            cont.push_back(str.substr(previous, current - previous));
            previous = current + 1;
            current = str.find_first_of(delims, previous);
        }
        cont.push_back(str.substr(previous, current - previous));
    };

    std::vector<std::string> paths;
    splitString(path, paths, "/");
    if( paths.back().size() == 0)
        paths.pop_back();

    std::reverse( paths.begin(), paths.end() );

    if( paths.back() == "#")
        paths.pop_back();

    auto it = J.find( paths.back() );
    paths.pop_back();
    while( paths.size() )
    {
        auto & key = paths.back();
        std::cout << key << std::endl;

        if( it->is_object())
        {
            it = it->find(key);
        }
        else if ( it->is_array() )
        {
            auto  myint3 = std::stoll(key);

            it = it->begin() + myint3;
        }
        paths.pop_back();
    }
    return it;
}


SCENARIO("test iterators")
{
    vka::json J;

    J["array"][3] = 3;

    std::cout << J.dump(4) << std::endl;

    auto j = J["array"].begin() + 3;

    REQUIRE( j.value().get<uint32_t>() == 3 );
}


SCENARIO("test get reference")
{
    vka::json J = vka::json::parse(
                R"foo(
                {
                    "simpleObject" :
                    {
                        "str_value" : "hello",
                        "number_value" : 3,
                        "bool_value" : true,
                        "simpleArray" : [
                            3,
                            7,
                            8,
                            {
                                "int_value" : 7
                            }
                        ]
                    }
                }
                )foo"
                );


    {
        auto it = getIterator(J, "#/simpleObject");
        REQUIRE( it->is_object() );
    }
    {
        auto it = getIterator(J, "#/simpleObject/");
        REQUIRE( it->is_object() );
    }
    {
        auto it = getIterator(J, "#/simpleObject/str_value");
        REQUIRE( it->is_string() );
        REQUIRE( it->get<std::string>() == "hello" );
    }
    {
        auto it = getIterator(J, "#/simpleObject/bool_value");
        REQUIRE( it->is_boolean() );
        REQUIRE( it->get<bool>() == true );
    }
    {
        auto it = getIterator(J, "#/simpleObject/simpleArray");
        REQUIRE( it->is_array() );
        auto it0 = getIterator(J, "#/simpleObject/simpleArray/0");
        auto it1 = getIterator(J, "#/simpleObject/simpleArray/1");
        auto it2 = getIterator(J, "#/simpleObject/simpleArray/2");
        auto it3 = getIterator(J, "#/simpleObject/simpleArray/3");
        auto it4 = getIterator(J, "#/simpleObject/simpleArray/3/int_value");
        REQUIRE( it0->is_number() );
        REQUIRE( it1->is_number() );
        REQUIRE( it2->is_number() );
        REQUIRE( it3->is_object() );
        REQUIRE( it4->is_number() );
    }
}


SCENARIO("test Ref")
{
    vka::uri U( "ref://2/SceneComponent/position/0" );

    REQUIRE( U.scheme == "ref");
    REQUIRE( U.getAuthority() == "2");
    REQUIRE( U.path == "/SceneComponent/position/0");
    REQUIRE( U.host == "2");
}


#define STB_IMAGE_IMPLEMENTATION
#define VKA_IMAGE_IMPLEMENTATION

#define VKAI_NO_STDIO
#define STBI_NO_STDIO


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wunused-function"

#include <vka/utils/vka_image.h>

#pragma GCC diagnostic pop
