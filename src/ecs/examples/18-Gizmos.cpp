#include <iostream>
#include <queue>
#include <tuple>
#include <future>

#include <vka/math.h>
#define VKA_USE_DEPRECATED
#include <vka/ecs/Engine.h>
#include <glslang/Public/ShaderLang.h>

#include <vka/ecs/Components/CLight.h>
#include <vka/ecs/Components/CScene2.h>
#include <vka/ecs/Components/CMesh.h>
#include <vka/ecs/Components/CCollider.h>
#include <vka/ecs/Components/CRigidBody.h>
#include <vka/ecs/Components/CUpdate.h>

#include <vka/ecs/Components/CWSADMovement.h>
#include <vka/ecs/Components/CMouseLook.h>
#include <vka/ecs/Components/CPLerp.h>
#include <vka/ecs/Components/CColliderTrigger.h>

#include <vka/ecs/Prefab/RotationGizmo.h>
#include <vka/ecs/Prefab/TranslationGizmo.h>

#include <glm/gtx/io.hpp>
#include <glm/gtc/random.hpp>

#define WIDTH  1024
#define HEIGHT 768

#include "InputApplication.h"


class MyApplication : public InputApplication
{
    // Application interface
public:

    std::shared_ptr<vka::ecs::Engine>             m_enginePtr;
    std::shared_ptr<vka::ecs::RenderSystem3>      m_RenderSystem;

    ~MyApplication()
    {
    }
    void init()
    {
        auto m_SystemBus = m_enginePtr->m_SystemBus;

        vka::ecs::PBRMaterial unlit;
        unlit.unlit = true;

        auto sphere_id = m_SystemBus->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("sphere1", vka::ecs::HostMeshPrimitive::Sphere(1.0f,32,32) );
        auto box_id    = m_SystemBus->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("box1", vka::ecs::HostMeshPrimitive::Box( glm::vec3(0.5f)) );
        auto grid_id   = m_SystemBus->resourceCreateAndEmplace<vka::ecs::HostMeshPrimitive>("grid1", vka::ecs::HostMeshPrimitive::Grid(10,10) );
        auto unlit_id  = m_SystemBus->resourceCreateAndEmplace<vka::ecs::PBRMaterial>("unlitMat", std::move(unlit) );

        vka::ecs::PBRMaterial defaultMaterial;
        defaultMaterial.roughnessFactor = 0;
        defaultMaterial.metallicFactor = 0;
        auto default_material_id  = m_SystemBus->resourceCreateAndEmplace<vka::ecs::PBRMaterial>("defaultMaterial", std::move(defaultMaterial) );
        (void)sphere_id;// = m_SystemBus->createResource<vka::ecs::HostMeshPrimitive>("sphere1", vka::ecs::HostMeshPrimitive::Sphere(1.0f) );
        (void)grid_id;
        (void)box_id;
        (void)default_material_id;

        {
            auto meshId = grid_id;

            auto E      = m_SystemBus->createEntity("gridEntity");

            E.create<vka::ecs::CMesh>(meshId, unlit_id, vk::PrimitiveTopology::eLineList);

            E.create<vka::ecs::CPosition>();
            E.create<vka::ecs::CRotation>();

        }

        entt::entity cameraEntity;
        if(1)
        {
            auto E = m_SystemBus->createEntity("cameraEntity");
            cameraEntity = E.entity;
            auto & C = E.create<vka::ecs::CCamera>();
            C.environment = m_SystemBus->resourceCreate<vka::ecs::Environment>(vka::uri("rc:environments/green_hills_2.environment"));

            vka::Transform t;
            t.position = {-3,3,-3};
            t.lookat( {0,0,0}, {0,1,0});


            E.create<vka::ecs::CCollider>(vka::ecs::SphereCollider(1) );
            auto & RB = E.create<vka::ecs::CRigidBody>( );
            RB.isKinematic = true;
            RB.mass = 0;

            E.create<vka::ecs::CPosition>( t.position );
            E.create<vka::ecs::CRotation>( t.rotation );
            E.create<vka::ecs::CMouseLook>();
            E.create<vka::ecs::CWSADMovement>();
        }


        if(1)
        {
           auto E      = m_SystemBus->createEntity("sphereEntity");

           auto chromeMatId = m_SystemBus->resourceCreate<vka::ecs::PBRMaterial>(vka::uri("rc:materials/paving_stone.material") );

           E.create<vka::ecs::CMesh>(sphere_id, chromeMatId);
           E.create<vka::ecs::CPosition>(3,0,3);
           E.create<vka::ecs::CRotation>();
           E.create<vka::ecs::CScale>(glm::vec3(0.5f));

           auto sb = m_SystemBus;

        }

        {
            auto sph = m_SystemBus->findEntityByName("sphereEntity");

            // Create a translation gizmo that we can
            // move around.
            auto et = m_SystemBus->initPrefab("prefab:gizmo/translation");
            auto er = m_SystemBus->initPrefab("prefab:gizmo/rotation");

            m_SystemBus->setVariable(er, "gizmo::cameraEntity", cameraEntity);
            m_SystemBus->setVariable(er, "gizmo::targetEntity", sph);



            // use the CPLerp component to always
            // keep the gizmo component half way between the object we want
            // to manipulate and the camera.
            // this will make sure that the gizmo is always drawn in front
            // of the object.
            //m_SystemBus->registry.emplace<vka::ecs::CPLerp>(et, vka::ecs::CPLerp{cameraEntity, sph, 0.5f} );

            m_SystemBus->setVariable(et, "gizmo::cameraEntity", cameraEntity);
            m_SystemBus->setVariable(et, "gizmo::targetEntity", sph);

            m_SystemBus->registry.emplace_or_replace<vka::ecs::CPosition>( et, m_SystemBus->registry.get<vka::ecs::CPosition>(sph));
        }
    }




    //===============================================================================================================================
    // Vulkan APP Specific code
    //===============================================================================================================================

    /**
     * @brief initResources
     *
     * This will be called right after system gets created.
     * It can be used to initialize any vulkan related resources
     * that are not related to the swapchain.
     */
    void initResources() override
    {
        m_enginePtr = std::make_shared<vka::ecs::Engine>();
        auto m_SystemBus = m_enginePtr->m_SystemBus;

    //profiler::dumpBlocksToFile("test_profile.prof");
        glslang::InitializeProcess();

        m_SystemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share");
        m_SystemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders");
        m_SystemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/shaders/renderer");
        m_SystemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/models");
        m_SystemBus->addPath(VKA_CMAKE_SOURCE_DIR  "/share/scripts");

        // create all the systems and attach them
        m_enginePtr->initializeSystems();

        m_RenderSystem = m_enginePtr->m_SystemBus->getOrCreateSystem2<vka::ecs::RenderSystem3>();

        vka::ecs::RenderSystem3CreateInfo ii;
        ii.device         = m_device;
        ii.physicalDevice = m_physicalDevice;
        ii.instance       = m_instance;

        m_RenderSystem->init(ii);
    }


    /**
     * @brief releaseResources
     *
     * This method must destroy all
     * non-swapchain related resources that were
     *
     */
    void releaseResources() override
    {
        auto rs = m_enginePtr->m_SystemBus->getOrCreateSystem2<vka::ecs::RenderSystem3>();

        m_enginePtr->m_SystemBus->disconnectAllSystems();
        m_enginePtr.reset();

        rs->releaseResources();

        glslang::FinalizeProcess();
        //std::cout << "release non-graphics resources" << std::endl;
    }


    /**
     * @brief initSwapChainResources
     *
     * This is called right after the swapchain is created.
     * You can use this to initialize any swapchain related resources,
     * for example, render targets (because they change with swapchain size)
     */
    void initSwapChainResources() override
    {
        m_RenderSystem->initSwapchainResources(swapchainImageSize(),
                                      concurrentFrameCount(),
                                      getDefaultRenderPass());

        if( !m_init )
        {
            m_init= true;
            init();
        }
        //startThreadPolling();
    }


    void releaseSwapChainResources() override
    {        
        m_RenderSystem->releaseSwapchainResources();

      //  VKA_DEBUG("release swapchain releated resources");
    //    stopThreadPolling();
    }

    /**
     * @brief render
     * @param frame
     *
     * This is the widget's main vulkan render function.
     * use this to actually record your command buffers.
     * submission of commandbuffers are automatically done
     * at the end
     */
    void render(vkw::Frame & frame) override
    {
        vkff::FrameGraphExecuteInfo info;
        info.commandBuffer        = frame.commandBuffer;
        info.swapchainImage       = frame.swapchainImage;
        info.swapchainExtent      = frame.swapchainSize;
        info.swapchainImageView   = frame.swapchainImageView;
        info.swapchainFrameIndex  = frame.swapchainIndex;
        info.swapchainFormat      = vk::Format(frame.swapchainFormat);
        info.swapchainRenderPass  = frame.renderPass;
        info.swapchainFramebuffer = frame.framebuffer;
        info.swapchainDepthFormat = vk::Format(frame.depthFormat);

        double dt = 0.016;
//        double dt = std::chrono::duration<double>(frame.frameTime - frame.lastFrameTime).count();


        m_enginePtr->m_SystemBus->step(dt);


        auto rs = m_enginePtr->m_SystemBus->getOrCreateSystem2<vka::ecs::RenderSystem3>();
        if( rs )
        {
            // if the rendersystem exists
            // then pass it to the rendersystem
            rs->render(info);
        }
        else
        {
            // otherwise do basic pass
            frame.beginRenderPass( frame.commandBuffer );
            frame.endRenderPass(frame.commandBuffer);
        }
        requestNextFrame();
    }

    void mouseMoveEvent(const vka::EvtInputMouseMotion *e)
    {
        m_enginePtr->m_SystemBus->triggerEvent(*e);
    }
    void mousePressEvent(const vka::EvtInputMouseButton *e)
    {
        m_enginePtr->m_SystemBus->triggerEvent(*e);
    }
    void mouseReleaseEvent(const vka::EvtInputMouseButton *e)
    {
        m_enginePtr->m_SystemBus->triggerEvent(*e);
    }

    void mouseWheelEvent(const vka::EvtInputMouseWheel *e)
    {
        m_enginePtr->m_SystemBus->triggerEvent(*e);
    }

    void keyPressEvent(const vka::EvtInputKey *e)
    {
        if( e->keycode == vka::KeyCode::F11)
        {
            std::dynamic_pointer_cast<vka::ecs::PhysicsSystem2>(m_enginePtr->m_SystemBus->getOrCreateSystem2<vka::ecs::PhysicsSystem2>())->toggleDebugDraw();
        }
        m_enginePtr->m_SystemBus->triggerEvent(*e);
    }
    void keyReleaseEvent(const vka::EvtInputKey *e)
    {
        m_enginePtr->m_SystemBus->triggerEvent(*e);
    }
    void fileDropEvent(const vka::EvtInputFileDrop *e)
    {
        (void)e;
    }

    bool m_init=false;
    //=======================

    vka::line3 m_mouseProjection;
};

std::shared_ptr<InputApplication> getApp()
{
    return std::make_shared<MyApplication>();
}
