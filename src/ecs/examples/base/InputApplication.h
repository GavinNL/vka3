#include <iostream>
#include <queue>
#include <tuple>
#include <future>

#include <vkw/VulkanApplication.h>
#include <vka/ecs/Events/InputEvents.h>

class InputApplication : public vkw::Application
{
    // Application interface
public:

    virtual void mouseMoveEvent(const vka::EvtInputMouseMotion *e)
    {
        (void)e;
    }
    virtual void mousePressEvent(const vka::EvtInputMouseButton *e)
    {
        (void)e;
    }
    virtual void mouseReleaseEvent(const vka::EvtInputMouseButton *e)
    {
        (void)e;
    }

    virtual void mouseWheelEvent(const vka::EvtInputMouseWheel *e)
    {
        (void)e;
    }

    virtual void keyPressEvent(const vka::EvtInputKey *e)
    {
        (void)e;
    }
    virtual void keyReleaseEvent(const vka::EvtInputKey *e)
    {
        (void)e;
    }
    virtual void fileDropEvent(const vka::EvtInputFileDrop *e)
    {
        (void)e;
    }
};


