#pragma once
#ifndef VKA_HOST_IMAGE_LOADER_H
#define VKA_HOST_IMAGE_LOADER_H

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wunused-function"

#define VKAI_NO_STDIO
#define STBI_NO_STDIO
#include "vka_image.h"
#pragma GCC diagnostic pop

#include <fstream>
#include "HostImage.h"

namespace vka
{

HostImage loadHostImage(void const* memory, size_t s);

inline HostImage loadHostImage(const std::string & path)
{
    std::ifstream file(path, std::ios::binary | std::ios::ate);

    if( !file )
    {
        throw std::runtime_error("Error loading image: " + path);
    }
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<char> buffer( static_cast<size_t>(size) );
    if (file.read(buffer.data(), size))
    {
        return loadHostImage( buffer.data(), buffer.size() );
    }
    throw std::runtime_error("Error loading image");
}

inline HostImage loadHostImage(void const* memory, size_t s)
{
    int x;
    int y;
    int comp;
    auto raw = vkai_load_from_memory( static_cast<vkai_uc const*>(memory), static_cast<int>(s), &x, &y, &comp, 4);

    HostImage I;
    I.resize( static_cast<uint32_t>(x) , static_cast<uint32_t>(y) );
    I.copyFromBuffer(raw, static_cast<uint32_t>(x*y*4), static_cast<uint32_t>(x) , static_cast<uint32_t>(y));

    return I;
}


inline HostImage loadHostImage_NoRGB(const std::string & path)
{
    std::ifstream file(path, std::ios::binary | std::ios::ate);

    if( !file )
    {
        throw std::runtime_error("Error loading image: " + path);
    }
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);

    std::vector<char> buffer( static_cast<size_t>(size) );
    if (file.read(buffer.data(), size))
    {
        void * memory = buffer.data();
        auto s = buffer.size() ;

        int x;
        int y;
        int comp;

        vkai_info_from_memory(static_cast<vkai_uc const*>(memory), static_cast<int>(s), &x, &y, &comp);

        int desComp=comp;
        if(comp == 3)
            desComp=4;

        auto raw = vkai_load_from_memory( static_cast<vkai_uc const*>(memory), static_cast<int>(s), &x, &y, &comp, desComp);

        HostImage I;
        I.resize( static_cast<uint32_t>(x) , static_cast<uint32_t>(y), static_cast<uint32_t>(desComp) );
        I.copyFromBuffer(raw, static_cast<uint32_t>(x*y*desComp), static_cast<uint32_t>(x) , static_cast<uint32_t>(y), static_cast<uint32_t>(desComp));

        return I;
    }
    throw std::runtime_error("Error loading image");
}

}

#endif
