#ifndef VKA2_PRIMITIVE_DRAW_CALL_H
#define VKA2_PRIMITIVE_DRAW_CALL_H

#include <cstdint>
#include <vulkan/vulkan.hpp>

namespace vka
{

struct PrimitiveDrawCall
{
    uint32_t indexCount   = 0;
    uint32_t firstIndex   = 0;
     int32_t vertexOffset = 0;
    uint32_t vertexCount  = 0;
    vk::PrimitiveTopology topology = vk::PrimitiveTopology::eTriangleList;
};

}
#endif
