#ifndef VKA_HOST_MODEL_TYPES_H
#define VKA_HOST_MODEL_TYPES_H

#include <vector>
#include <string.h>
#include <map>

#include <cassert>

#include <vulkan/vulkan.hpp>

namespace vka
{

enum class AccessorType
{
    eUnknown,
    eVec1,
    eVec2,
    eVec3,
    eVec4,
    eMat2,
    eMat3,
    eMat4
};

enum class ComponentType
{
    eUnknown       = 0,
    eByte          = 5120,
    eUnsignedByte  = 5121,// (UNSIGNED_BYTE) normalized
    eShort         = 5122,
    eUnsignedShort = 5123,// (UNSIGNED_SHORT) normalized
    eInt           = 5124,
    eUnsignedInt   = 5125,
    eFloat         = 5126,// (FLOAT)
    eDouble        = 5130
};

enum class PrimitiveAttribute : uint8_t
{
    POSITION   = 0,
    NORMAL	   = 1,
    TANGENT	   = 2,
    TEXCOORD_0 = 3,
    TEXCOORD_1 = 4,
    COLOR_0	   = 5,
    JOINTS_0   = 6,
    WEIGHTS_0  = 7,
    __LAST,
    __UNDEFINED = 255,
};

inline std::string to_string(PrimitiveAttribute a)
{
    switch(a)
    {
        case PrimitiveAttribute::POSITION   : return std::string("POSITION" );
        case PrimitiveAttribute::NORMAL	    : return std::string("NORMAL" );
        case PrimitiveAttribute::TANGENT	: return std::string("TANGENT" );
        case PrimitiveAttribute::TEXCOORD_0 : return std::string("TEXCOORD_0" );
        case PrimitiveAttribute::TEXCOORD_1 : return std::string("TEXCOORD_1" );
        case PrimitiveAttribute::COLOR_0	: return std::string("COLOR_0" );
        case PrimitiveAttribute::JOINTS_0   : return std::string("JOINTS_0" );
        case PrimitiveAttribute::WEIGHTS_0  : return std::string("WEIGHTS_0" );
        case PrimitiveAttribute::__LAST     : return std::string("__LAST" );
        case PrimitiveAttribute::__UNDEFINED: return std::string("__UNDEFINED" );
    }
    return std::string("__UNDEFINED" );
}

inline vk::Format getVulkanFormat( ComponentType c, AccessorType a)
{
    uint32_t i = static_cast<uint32_t>(a)-1; // accessor type index
    uint32_t j = static_cast<uint32_t>(c) - 5120u;

    if( i > 3) return vk::Format::eUndefined;

    if(j==5130-5120)  // double
    {
        j = 7;
    }

    //                i  j
    const vk::Format f[][8] =
    {
        {vk::Format::eR8Sint      , vk::Format::eR8Uint      , vk::Format::eR16Sint         , vk::Format::eR16Uint         ,  vk::Format::eR32Sint         ,  vk::Format::eR32Uint         ,  vk::Format::eR32Sfloat         ,  vk::Format::eR64Sfloat},
        {vk::Format::eR8G8Sint    , vk::Format::eR8G8Uint    , vk::Format::eR16G16Sint      , vk::Format::eR16G16Uint      ,  vk::Format::eR32G32Sint      ,  vk::Format::eR32G32Uint      ,  vk::Format::eR32G32Sfloat      ,  vk::Format::eR64G64Sfloat},
        {vk::Format::eR8G8B8Sint  , vk::Format::eR8G8B8Uint  , vk::Format::eR16G16B16Sint   , vk::Format::eR16G16B16Uint   ,  vk::Format::eR32G32B32Sint   ,  vk::Format::eR32G32B32Uint   ,  vk::Format::eR32G32B32Sfloat   ,  vk::Format::eR64G64B64Sfloat},
        {vk::Format::eR8G8B8A8Sint, vk::Format::eR8G8B8A8Uint, vk::Format::eR16G16B16A16Sint, vk::Format::eR16G16B16A16Uint,  vk::Format::eR32G32B32A32Sint,  vk::Format::eR32G32B32A32Uint,  vk::Format::eR32G32B32A32Sfloat,  vk::Format::eR64G64B64A64Sfloat}
    };

    return f[i][j];
}

inline size_t componentSize(ComponentType c)
{
    switch( c )
    {
        case ComponentType::eByte          : return 1;//= 5120,
        case ComponentType::eUnsignedByte  : return 1;//= 5121,// (UNSIGNED_BYTE) normalized
        case ComponentType::eShort         : return 2;//= 5122,
        case ComponentType::eUnsignedShort : return 2;//= 5123,// (UNSIGNED_SHORT) normalized
        case ComponentType::eInt           : return 4;//= 5124,
        case ComponentType::eUnsignedInt   : return 4;//= 5125,
        case ComponentType::eFloat         : return 4;//= 5126,// (FLOAT)
        case ComponentType::eDouble        : return 8;//= 5130
        default:
            throw std::runtime_error("Incorrect component type");
    }
    throw std::runtime_error("Incorrect component type");
}


}

#endif
