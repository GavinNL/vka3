#ifndef VKA_IMAGE_COMPRESSOR_H
#define VKA_IMAGE_COMPRESSOR_H

#include <vector>
#include <cstring>

#include <vka/BasisConverter/BasisConverterBase.h>
#include <vka/utils/HostImage.h>

namespace vka
{


/**
 * @brief The BasisConverter class
 *
 * The BasisConverter class is used to convert HostImages
 * into to Basis format. The Basis format can then be
 * converted into compressed textures.
 *
 * You shoudl only have one BasisConverter instantiated because
 * it keeps an internal threadpool
 */
class ImageCompressor : public BasisConverter
{
public:
    ImageCompressor(uint32_t threadPoolThreadCount=1) : BasisConverter(threadPoolThreadCount) {}


    void pushImageLayer(vka::HostImage const & I)
    {
        BasisConverter::pushImageLayer( I.data(), I.size(), I.width(), I.height());
    }

};







}

#endif 


