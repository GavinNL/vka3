#pragma once
#ifndef VKA_FILESYSTEM_H
#define VKA_FILESYSTEM_H

#include<string>
#include<algorithm>
#include<stdexcept>

#ifdef _WIN32
   #include <io.h>
   #include<windows.h>
#else
   #include <unistd.h>
   #include <sys/stat.h>
#endif

#include <dirent.h>
#include <vector>

#if defined(__clang__)

#elif defined(__GNUC__) || defined(__GNUG__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#elif defined(_MSC_VER)

#endif



namespace vka
{
namespace fs
{
    inline void _unix_sep(std::string & path)
    {
        std::replace(path.begin(), path.end(), '\\', '/');
    }

    inline bool is_absolute(const std::string & path)
    {
        switch( path.size() )
        {
            case 0:
                return false;
            case 1:
                if( path.front() == '/') return true;
                return false;
            default:
                if( path[1]==':') return true;
                if( path[0]=='/') return true;
                return false;
        }
    }
    inline bool is_relative(const std::string & path)
    {
        return !is_absolute(path);
    }

    inline bool exists(const std::string & path)
    {
        #ifdef _WIN32
            return _access_s( path.c_str(), 0)==0;
        #else
            return access( path.c_str(), 0)==0;
        #endif
    }


    inline bool is_directory(const std::string & path)
    {
        struct stat s;
        if( stat(path.c_str(),&s) == 0 )
        {
            if( S_ISDIR(s.st_mode)  )
            {
                //it's a directory
                return true;
            }
            else if( S_ISREG(s.st_mode) )
            {
                //it's a file
                return false;
            }
            else
            {
                return false;
                //something else
            }
        }
        else
        {
            return false;
            //error
        }
    }

    inline std::string current_path()
    {

#if defined _WIN32
        char * cwd;
        if( (cwd = _getcwd( NULL, 0 )) != NULL )
        {
            std::string spath(cwd);
            _unix_sep(spath);
            return spath;
        }
        return "";
#else
        char path[FILENAME_MAX];
        getcwd(path, FILENAME_MAX);

        std::string spath(path);
        _unix_sep(spath);
        return spath;
#endif
    }
    /**
     * @brief parent
     * @param path
     * @return
     *
     * Returns the parent path.
     */
    inline std::string executable_path()
    {
        char path[FILENAME_MAX];

#if defined __linux__
        auto i = readlink("/proc/self/exe", path, FILENAME_MAX );
        path[i] = 0;
        return std::string(path);
#elif defined _WIN32
        auto i = GetModuleFileNameA( NULL, path, FILENAME_MAX);
        auto p = std::string( path );
        return join( current_path(), path);
#elif defined __APPLE__

        int ret;
        pid_t pid;
        char pathbuf[PROC_PIDPATHINFO_MAXSIZE];

        pid = getpid();
        ret = proc_pidpath (pid, pathbuf, sizeof(pathbuf));
        if ( ret <= 0 ) {
            return "";
            fprintf(stderr, "PID %d: proc_pidpath ();\n", pid);
            fprintf(stderr, "    %s\n", strerror(errno));
        } else {
            return pathbuf;
            //printf("proc %d: %s\n", pid, pathbuf);
        }
#endif
        return "";
    }

    inline std::string temp_directory_path()
    {
#if defined __linux__
        return "/tmp";
#elif defined _WIN32
        return "C:/Windows/TEMP";
#elif defined __APPLE__
        return "/tmp";
#endif
    }

    /**
     * @brief parent
     * @param path
     * @return
     *
     * Returns the parent path.
     */
    inline std::string parent_path(const std::string & path )
    {
        return path.substr(0, path.find_last_of( "/" ) );
    }

    /**
     * @brief filename
     * @param path
     * @return
     *
     * Returns the filename
     */
    inline std::string filename(const std::string & path )
    {
        return path.substr(path.find_last_of( "/" )+1 );
    }

    /**
     * @brief stem
     * @param path
     * @return
     *
     * Returns the stem of the path. ie: the filename without
     * the extension.
     */
    inline std::string stem(const std::string & path )
    {
        auto f = filename(path);
        return f.substr( 0, f.find_last_of( "." ) );
    }

    /**
     * @brief extension
     * @param path
     * @return
     *
     * Returns the extension of a path
     */
    inline std::string extension(const std::string & path )
    {
        auto f = filename(path);

        return f.substr( f.find_last_of( "." )+1 );
    }

    /**
     * @brief join
     * @param parent
     * @param child
     * @return
     *
     * Join a parent and child path together.
     */
    inline std::string join(const std::string & parent, const std::string & child)
    {
        auto s = parent+"/"+child;


        s.erase(std::unique(s.begin(), s.end(), [](const char & l, const char &r)
        {
            return l==r && l == '/';
        }), s.end());
        return s;
    }


    /**
     * @brief ls
     * @param path
     * @param recursive
     * @return
     *
     * Returns a list of all files in the path
     */
    inline std::vector<std::string> ls(const std::string & path, bool recursive=false)
    {
        std::vector<std::string> files;

        if( auto dir = opendir(path.c_str()) ; dir != nullptr)
        {
          struct dirent *ent;
          /* print all the files and directories within directory */
          while ( (ent = readdir (dir)) != NULL)
          {
              auto f = std::string(ent->d_name);
              if( f == "." || f == "..") continue;

              files.push_back( path + "/" + ent->d_name);

              try
              {
                  if( recursive && is_directory( files.back() ) )
                  {
                      auto sub = ls(files.back(), true);
                      files.insert( files.end(), sub.begin(), sub.end() );
                  }
              } catch (...) {

              }
          }
          closedir (dir);
        }
        else
        {
          /* could not open directory */
          throw std::runtime_error("Error opening directory: " + path);
        }

        return files;
    }

    inline bool rm(const std::string & path)
    {
        return std::remove( path.c_str() )==0;
    }

    inline bool create_directory(const std::string & path, std::int32_t chmod=0766)
    {
#if defined(_WIN32)
        return CreateDirectory ( path.c_str(), NULL) != 0;
#else
      auto success = (::mkdir(path.c_str(), static_cast<mode_t>(chmod) ) == 0);

      return success;
#endif
    }


}
}

#if defined(__clang__)

#elif defined(__GNUC__) || defined(__GNUG__)
#pragma GCC diagnostic pop
#elif defined(_MSC_VER)

#endif



#endif
