#pragma once
#ifndef VKA_URI_H
#define VKA_URI_H

#include<regex>
#include<string>
#include<algorithm>
#include<stdexcept>
#include<algorithm>
#include<locale>

namespace vka
{
struct uri
{
    std::string scheme;

    std::string user;
    std::string password;
    std::string host;
    std::string port;

    std::string path;
    std::string query;
    std::string fragment;

    struct data_uri
    {
        std::string          mediatype;
        std::string          encoding;
        std::vector<uint8_t> data;
    };


    uri()
    {}

    explicit uri(const std::string &str)
    {
        parse(str);
    }

    uri(const uri & _uri) :
        scheme  (_uri.scheme   ),
        user    (_uri.user     ),
        password(_uri.password ),
        host    (_uri.host     ),
        port    (_uri.port     ),
        path    (_uri.path     ),
        query   (_uri.query    ),
        fragment(_uri.fragment )
    {
    }

    uri(uri && _uri) :
        scheme  ( std::move(_uri.scheme   )),
        user    ( std::move(_uri.user     )),
        password( std::move(_uri.password )),
        host    ( std::move(_uri.host     )),
        port    ( std::move(_uri.port     )),
        path    ( std::move(_uri.path     )),
        query   ( std::move(_uri.query    )),
        fragment( std::move(_uri.fragment ))
    {
    }

    uri& operator=(uri && _uri)
    {
        if( this != &_uri )
        {
            scheme   = std::move(_uri.scheme   );
            user     = std::move(_uri.user     );
            password = std::move(_uri.password );
            host     = std::move(_uri.host     );
            port     = std::move(_uri.port     );
            path     = std::move(_uri.path     );
            query    = std::move(_uri.query    );
            fragment = std::move(_uri.fragment );
        }
        return *this;
    }

    uri& operator=(uri const & _uri)
    {
        if( this != &_uri )
        {
            scheme   = _uri.scheme  ;
            user     = _uri.user    ;
            password = _uri.password;
            host     = _uri.host    ;
            port     = _uri.port    ;
            path     = _uri.path    ;
            query    = _uri.query   ;
            fragment = _uri.fragment;
        }
        return *this;
    }

    // create a data URI using base64
    uri( void const* rawData, size_t byteSize)
    {
        scheme = "data";
        path = _toBase64(rawData,  static_cast<uint8_t const*>(rawData)+byteSize);
    }
    uri& operator=(const std::string & str)
    {
        parse(str);
        return *this;
    }

    std::string toString() const
    {
        std::string out;
       if( scheme.size()!=0 ) out += scheme + ":";
       auto auth = getAuthority();
       if(auth.size())
       {
           out += "//" + auth;
       }

       out += path;
       if(query.size())
       {
           out += "?" + query;
       }
       if( fragment.size())
       {
           out += "#" + fragment;
       }
       return out;
    }
    std::string getAuthority() const
    {
        std::string out;
        if( user.size() )
        {
            out += user;
            if(password.size())
                out += ":" + password;
            out += "@";
        }
        out += host;
        if( port.size() )
            out += ":" + port;
        return out;
    }
    void parse(const std::string & str)

    {
        // This parsing code was taken from Facebook's Folly library
        // the Boost regex was replaced with std::regex
        /*
         * Copyright (c) Facebook, Inc. and its affiliates.
         *
         * Licensed under the Apache License, Version 2.0 (the "License");
         * you may not use this file except in compliance with the License.
         * You may obtain a copy of the License at
         *
         *     http://www.apache.org/licenses/LICENSE-2.0
         *
         * Unless required by applicable law or agreed to in writing, software
         * distributed under the License is distributed on an "AS IS" BASIS,
         * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
         * See the License for the specific language governing permissions and
         * limitations under the License.
         */

      static const std::regex uriRegex(
          "([a-zA-Z][a-zA-Z0-9+.-]*):" // scheme:
          "([^?#]*)" // authority and path
          "(?:\\?([^#]*))?" // ?query
          "(?:#(.*))?"); // #fragment
      static const std::regex authorityAndPathRegex("//([^/]*)(/.*)?");

      std::smatch match;
      if ( !std::regex_match(str, match, uriRegex))
      {
          throw std::invalid_argument( "invalid URI: " + str);
      }

      scheme = match[1];
      std::transform(scheme.begin(), scheme.end(), scheme.begin(),
          [](unsigned char c){ return std::tolower(c); });

      std::string authorityAndPath(match[2].first, match[2].second);
      std::smatch authorityAndPathMatch;
      if (!std::regex_match(
              authorityAndPath,
              authorityAndPathMatch,
              authorityAndPathRegex)) {
        // Does not start with //, doesn't have authority

        path = authorityAndPath;
      }
      else
      {
        static const std::regex authorityRegex(
            "(?:([^@:]*)(?::([^@]*))?@)?" // username, password
            "(\\[[^\\]]*\\]|[^\\[:]*)" // host (IP-literal (e.g. '['+IPv6+']',
                                       // dotted-IPv4, or named host)
            "(?::(\\d*))?"); // port

        std::string authority = authorityAndPathMatch[1];
        std::smatch authorityMatch;
        if (!std::regex_match(
                authority,
                authorityMatch,
                authorityRegex)) {
          throw std::invalid_argument( "invalid URI authority");
        }

        port = std::string(authorityMatch[4].first, authorityMatch[4].second);


        user     = authorityMatch[1];
        password = authorityMatch[2];
        host     = authorityMatch[3];
        path     = authorityAndPathMatch[2];
      }

      query    = match[3];//, 3);
      fragment = match[4];//, 4);
    }


    //============================
    static data_uri extractDataURI(const std::string & path)
    {
        data_uri out;
        auto i = std::find( path.begin(), path.end(), ',');
        if( i == path.end() )
            return out;

        std::string media_encoding = std::string( path.begin(), i);

        std::string::value_type const * startOfBase64Data = &(*(i+1));
        std::string::value_type const * endOfBase64Data = &path[ path.size() ];
        out.data = _fromBase64( startOfBase64Data, endOfBase64Data);

        {
            auto j = std::find( media_encoding.begin(), media_encoding.end(), ';');
            if( j == media_encoding.end() )
            {
                out.mediatype = media_encoding;
            }
            else
            {
                out.mediatype = std::string( media_encoding.begin(), j);
                out.encoding  = std::string( j+1, media_encoding.end());
            }
        }
        return out;
    }

    static inline bool is_base64(unsigned char c) {
      return (isalnum(c) || (c == '+') || (c == '/'));
    }

    static inline std::vector<uint8_t> _fromBase64( void const* src, void const* src_end)
    {
        static const std::string base64_chars =
                     "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                     "abcdefghijklmnopqrstuvwxyz"
                     "0123456789+/";

        using char_type = std::string::value_type;
        auto bytes_to_encode     = static_cast<unsigned char const*>(src);
        auto bytes_to_encode_end = static_cast<unsigned char const*>(src_end);

        auto encoded_string = bytes_to_encode;
        auto in_len = bytes_to_encode_end - bytes_to_encode;
      //int in_len = encoded_string.size();
      int i = 0;
      int j = 0;
      int in_ = 0;
      unsigned char char_array_4[4], char_array_3[3];
      std::vector<uint8_t> ret;

      while (in_len-- && ( encoded_string[in_] != '=') && is_base64(encoded_string[in_]))
      {
        char_array_4[i++] = encoded_string[in_]; in_++;
        if (i ==4)
        {
          for (i = 0; i <4; i++)
            char_array_4[i] = static_cast<unsigned char>( base64_chars.find( static_cast<char_type>(char_array_4[i]) ) );

          char_array_3[0] = static_cast<unsigned char>(  ( char_array_4[0] << 2       ) + ((char_array_4[1] & 0x30) >> 4)  );
          char_array_3[1] = static_cast<unsigned char>(  ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2)  );
          char_array_3[2] = static_cast<unsigned char>(  ((char_array_4[2] & 0x3) << 6) +   char_array_4[3]                );

          for (i = 0; (i < 3); i++)
            ret.push_back( char_array_3[i] );
          i = 0;
        }
      }

      if (i)
      {
        for (j = 0; j < i; j++)
          char_array_4[j] = static_cast<unsigned char>( base64_chars.find(  static_cast<char_type>(char_array_4[j]) ) );

        char_array_3[0] = static_cast<unsigned char>(   (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4)         );
        char_array_3[1] = static_cast<unsigned char>(   ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2) );

        for (j = 0; (j < i - 1); j++)
            ret.push_back( char_array_3[j] );
      }

      return ret;
    }

    static std::string _toBase64( void const* src, void const* src_end)
    {
        static const std::string base64_chars =
                     "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                     "abcdefghijklmnopqrstuvwxyz"
                     "0123456789+/";

        auto bytes_to_encode     = static_cast<unsigned char const*>(src);
        auto bytes_to_encode_end = static_cast<unsigned char const*>(src_end);

        auto in_len = bytes_to_encode_end - bytes_to_encode;

      std::string ret = "text/plain;base64,";

      int i = 0;
      int j = 0;
      unsigned char char_array_3[3];
      unsigned char char_array_4[4];

      while (in_len--) {
        char_array_3[i++] = *(bytes_to_encode++);
        if (i == 3) {
          char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
          char_array_4[1] = static_cast<unsigned char>(  ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4)  );
          char_array_4[2] = static_cast<unsigned char>(  ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6)  );
          char_array_4[3] = char_array_3[2] & 0x3f;

          for(i = 0; (i <4) ; i++)
            ret += base64_chars[char_array_4[i]];
          i = 0;
        }
      }

      if (i)
      {
        for(j = i; j < 3; j++)
          char_array_3[j] = '\0';

        char_array_4[0] = ( char_array_3[0] & 0xfc) >> 2;
        char_array_4[1] = static_cast<unsigned char>( ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4) );
        char_array_4[2] = static_cast<unsigned char>( ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6) );

        for (j = 0; (j < i + 1); j++)
          ret += base64_chars[char_array_4[j]];

        while((i++ < 3))
          ret += '=';

      }

      return ret;

    }

};

}

#endif
