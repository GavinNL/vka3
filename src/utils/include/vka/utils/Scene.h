#ifndef VKA_RENDERER_SCENE_H
#define VKA_RENDERER_SCENE_H

#include<array>
#include<vector>

#include<vka/math/transform.h>
#include<vka/utils/HostImage.h>
#include<vka/utils/HostTriMesh.h>
#include<vka/utils/AnimationSampler.h>
#include<glm/gtx/spline.hpp>
#include<optional>

namespace vka
{

struct SceneBase
{
    template<typename T>
    struct PerNode_t
    {
        uint32_t node;
        T        value;

        PerNode_t(){}
        PerNode_t(PerNode_t const & X) : node(X.node), value(X.value){}
    };

    struct Node
    {
        std::string           name;
        std::vector<uint32_t> children;
        vka::Transform        transform;

        std::optional<uint32_t> meshIndex;
        std::optional<uint32_t> skinIndex;
        int32_t                parentIndex = -1;

        bool                   isRenderable=true; // this is set to true if this node or any of the child nodes contains a mesh
        std::vector<uint32_t>  rootSkeleton; // a list of skin indices where this node is the root.
    };

    struct DrawCall
    {
        uint32_t indexCount   = 0;
        uint32_t firstIndex   = 0;
         int32_t vertexOffset = 0;
        uint32_t vertexCount  = 0;

        DrawCall() :
            indexCount   (0),
            firstIndex   (0),
            vertexOffset (0),
            vertexCount  (0)
        {
        }



        DrawCall(DrawCall const & dc) :
            indexCount   (dc.indexCount  ),
            firstIndex   (dc.firstIndex  ),
            vertexOffset (dc.vertexOffset),
            vertexCount  (dc.vertexCount )
        {
        }
        DrawCall(HostTriPrimitive::DrawCall const & dc) :
            indexCount   (dc.indexCount  ),
            firstIndex   (dc.firstIndex  ),
            vertexOffset (dc.vertexOffset),
            vertexCount  (dc.vertexCount )
        {
        }

        DrawCall& operator=(DrawCall const & dc)
        {
            indexCount   = dc.indexCount  ;
            firstIndex   = dc.firstIndex  ;
            vertexOffset = dc.vertexOffset;
            vertexCount  = dc.vertexCount ;

            return *this;
        }
        DrawCall& operator=(HostTriPrimitive::DrawCall const & dc)
        {
            indexCount   = dc.indexCount  ;
            firstIndex   = dc.firstIndex  ;
            vertexOffset = dc.vertexOffset;
            vertexCount  = dc.vertexCount ;

            return *this;
        }

        operator HostTriPrimitive::DrawCall() const
        {
            HostTriPrimitive::DrawCall dc;
            dc.indexCount   = indexCount  ;
            dc.firstIndex   = firstIndex  ;
            dc.vertexOffset = vertexOffset;
            dc.vertexCount  = vertexCount ;
            return dc;
        }
    };

    struct Primitive   // a primitive is  GLTF object
    {
        std::string       name;
        uint32_t          rawPrimitiveIndex; // look this value up in the .rawPrimitives vector
        DrawCall          drawCall;

        Primitive( ) {}
        Primitive( uint32_t _rawPrimitiveIndex, DrawCall _drawCall) : rawPrimitiveIndex(_rawPrimitiveIndex), drawCall(_drawCall){}
    };

    struct MeshPrimitive
    {
        uint32_t primitiveIndex = std::numeric_limits<uint32_t>::max();
        uint32_t materialIndex  = std::numeric_limits<uint32_t>::max();
    };

    struct Mesh
    {
        std::string                name;
        std::vector<MeshPrimitive> meshPrimitives;
    };

    struct Material
    {
        std::string       name;
        glm::vec4   baseColorFactor          = {1,1,1,1};

        float       metallicFactor           = 1.0f;
        float       roughnessFactor          = 1.0f;

        int32_t     baseColorTexture         = -1;
        int32_t     metallicRoughnessTexture = -1;

        int32_t     normalTexture            = -1;
        int32_t     occlusionTexture         = -1;
        int32_t     emissiveTexture          = -1;

        glm::vec3   emissiveFactor           = {0,0,0};
    };

    struct Texture
    {
        uint32_t imageIndex;
        uint32_t samplerIndex; // unused
    };

    struct Animation
    {
        using linearVec3Sampler = vka::AnimationSampler_t<float, glm::vec3>;
        using linearQuatSampler = vka::AnimationSampler_t<float, glm::quat>;

        using cubicVec3Sampler  = vka::AnimationSampler_t<float, std::array<glm::vec3,3> >;
        using cubicQuatSampler  = vka::AnimationSampler_t<float, std::array<glm::quat,3> >;

        struct Channel
        {
            uint32_t node;
            std::array<int32_t,4> samplers = std::array<int32_t,4>{{-1,-1,-1,-1}};
        };

        using KeyFrameState = std::vector< PerNode_t<vka::Transform> >;

        struct Sampler
        {
            std::variant<
                            linearVec3Sampler,
                            linearQuatSampler,
                            cubicVec3Sampler,
                            cubicQuatSampler
                        > frames;
            uint8_t       interpolation=1; // 0- step, 1- linear, 2-cubic


            void sample(float timeVal, glm::vec3 * v3, glm::quat * q) const
            {
                std::visit([&](auto&& arg)
                {
                    using T = std::decay_t<decltype(arg)>;

                    if constexpr (std::is_same_v<T, Animation::linearVec3Sampler>)
                    {
                        auto i   = arg.find(timeVal);

                        if( interpolation == 0)
                        {
                            *v3 = i.factor < 0.99f ? i.frame1->value : i.frame2->value;
                        }
                        else
                        {
                            *v3 = glm::mix( i.frame1->value, i.frame2->value, i.factor);
                        }
                    }
                    else if constexpr (std::is_same_v<T, Animation::linearQuatSampler>)
                    {
                        auto i   = arg.find(timeVal);
                        if( interpolation == 0)
                        {
                            *q = i.factor < 0.99f ? i.frame1->value : i.frame2->value;
                        }
                        else
                        {
                            *q = glm::slerp( i.frame1->value, i.frame2->value, i.factor);
                        }
                    }
                    else if constexpr (std::is_same_v<T, Animation::cubicVec3Sampler>)
                    {
                        auto i   = arg.find(timeVal);
                        *v3 =
                        glm::hermite( i.frame1->value[1], i.frame1->value[0],
                                      i.frame2->value[1], i.frame1->value[2],
                                    i.factor);
                    }
                    else if constexpr (std::is_same_v<T, Animation::cubicQuatSampler>)
                    {
                        auto i   = arg.find(timeVal);

                        *q =
                        glm::hermite( i.frame1->value[1], i.frame1->value[0],
                                      i.frame2->value[1], i.frame2->value[0],
                                    i.factor);
                        *q = glm::normalize(*q);
                    }
                }, frames);
            }

        };

        /**
         * @brief getSceneKeyFrame
         * @param timeVal
         * @return
         *
         * Returns the Scene's KeyFrameState at timeVal.
         * The KeyFrameState lists all the transforms of
         * the nodes that have changed during the animation.
         *
         * You must pass in the base Scene for this to work because
         * if any of the position/rotation/scale values do not change
         * in the animation, the original value will be taken from the
         * scene's node list.
         */
        void getSceneKeyFrame(float timeVal , SceneBase const & S, KeyFrameState & p) const
        {
            p.resize( channels.size() );
            size_t j=0;
            for(auto & c : channels)
            {
                p[j].node = c.node;
                auto & tr = p[j++].value;
                tr = S.nodes[ c.node ].transform;
                if( c.samplers[0]!=-1)
                {
                    samplers[ static_cast<size_t>(c.samplers[0]) ].sample(timeVal, &tr.position, nullptr);
                }
                if( c.samplers[1]!=-1)
                {
                    samplers[ static_cast<size_t>(c.samplers[1]) ].sample(timeVal, nullptr, &tr.rotation);
                }
                if( c.samplers[2]!=-1)
                {
                    samplers[ static_cast<size_t>(c.samplers[2]) ].sample(timeVal, &tr.scale, nullptr);
                }

            }
        }
        void getSceneKeyFrame(float timeVal , std::vector<Node> const & N, KeyFrameState & p) const
        {
            p.resize( channels.size() );
            size_t j=0;
            for(auto & c : channels)
            {
                p[j].node = c.node;
                auto & tr = p[j++].value;
                tr = N[ c.node ].transform;
                if( c.samplers[0]!=-1)
                {
                    samplers[ static_cast<size_t>(c.samplers[0]) ].sample(timeVal, &tr.position, nullptr);
                }
                if( c.samplers[1]!=-1)
                {
                    samplers[ static_cast<size_t>(c.samplers[1]) ].sample(timeVal, nullptr, &tr.rotation);
                }
                if( c.samplers[2]!=-1)
                {
                    samplers[ static_cast<size_t>(c.samplers[2]) ].sample(timeVal, &tr.scale, nullptr);
                }

            }
        }

        KeyFrameState getSceneKeyFrame(float timeVal , SceneBase const & S) const
        {
            KeyFrameState p;
            getSceneKeyFrame(timeVal, S, p);
            return p;
        }
        /**
         * @brief getSceneKeyFrame
         * @param timeVal
         * @param currentNodeTransforms
         * @return
         *
         * Same as above, but instead of taking the original node transformation from
         * the Scene's Node list, it takes it from the arrayProxy.
         */
        KeyFrameState getSceneKeyFrame(float timeVal , vk::ArrayProxy<vka::Transform const> currentNodeTransforms) const
        {
            KeyFrameState p;
            p.resize( channels.size() );
            size_t j=0;
            for(auto & c : channels)
            {
                p[j].node = c.node;
                auto & tr = p[j++].value;
                tr = currentNodeTransforms.data()[c.node];
                if( c.samplers[0]!=-1)
                {
                    samplers[ static_cast<size_t>(c.samplers[0]) ].sample(timeVal, &tr.position, nullptr);
                }
                if( c.samplers[1]!=-1)
                {
                    samplers[ static_cast<size_t>(c.samplers[1]) ].sample(timeVal, nullptr, &tr.rotation);
                }
                if( c.samplers[2]!=-1)
                {
                    samplers[ static_cast<size_t>(c.samplers[2]) ].sample(timeVal, &tr.scale, nullptr);
                }
            }
            return p;
        }

        float getLength() const
        {
            return end-start;
        }
        //=========================================================
        std::string          name;
        float                start = 0.0f;
        float                end   = 0.0f;
        std::vector<Channel> channels;
        std::vector<Sampler> samplers;
    };

    struct Skin
    {
        std::vector< glm::mat4> inverseBindMatrices;
        std::vector< uint32_t > joints;
        uint32_t                rootNode;
    };

    struct Sc
    {
        std::string           name;
        std::vector<uint32_t> rootNodes;
    };

    std::string                            name;
    std::vector<Node>                      nodes;
    std::vector<Primitive>                 primitives;
    std::vector<Mesh>                      meshes;
    std::vector<Texture>                   textures;
    std::vector<Material>                  materials;
    std::vector<Animation>                 animations;
    std::vector<Skin>                      skins;
    std::vector< Sc >                      scenes; // root nodes per scene

    //=====================================================================================
    // Helper functions
    //=====================================================================================
    std::vector<vka::Transform> getNodeSpaceTransforms() const
    {
        std::vector<vka::Transform> out;
        for(auto & n : nodes)
            out.push_back(n.transform);
        return out;
    }

    /**
     * @brief getModelSpacePose
     * @param pose
     * @return
     * Convert the local pose transform, where eacn value in pose is a transform
     * in node-space, into a model space matrix for each node.
     */
    std::vector<glm::mat4> getModelSpaceMatrices(std::vector<vka::Transform> const& pose) const
    {
        std::vector<glm::mat4> out(pose.size());

        struct R
        {
            static void _recurse( SceneBase const & S,
                                  uint32_t  node,
                                  glm::mat4 nodeParentTransform,
                                  vk::ArrayProxy<vka::Transform const> nodeSpaceTransforms,
                                  vk::ArrayProxy<glm::mat4> nodeModelSpaceMatrices)
            {
                nodeParentTransform = nodeParentTransform * nodeSpaceTransforms.data()[node].getMatrix();
                nodeModelSpaceMatrices.data()[node] = nodeParentTransform;

                for(auto c : S.nodes[node].children)
                {
                    _recurse( S, c,
                              nodeParentTransform,
                              nodeSpaceTransforms,
                              nodeModelSpaceMatrices);
                }
            }
        };

        for(auto & s : scenes)
        {
            for(auto r : s.rootNodes)
            {
                R::_recurse( *this, r, glm::mat4(1.0f), pose, out);
            }
        }
        return out;
    }

    /**
     * @brief getModelSpaceTransform
     * @param index
     * @param pose
     * @return
     *
     * Return the model-space transform of a specific node.
     */
    vka::Transform getModelSpaceTransform(size_t index, std::vector<vka::Transform> const& pose) const
    {
        vka::Transform t = pose[index];

        while( nodes[index].parentIndex != -1)
        {
            index = static_cast<size_t>( nodes[index].parentIndex );
            t = pose[index] * t;
        }
        return t;
    }


    /**
     * @brief getAnimationPose
     * @param animationIndex
     * @param timeVal
     * @return
     *
     * Calculates the pose of animation, animationIndex at time timeVal.
     * This function will update the values in nodeSpaceTransforms
     */
    bool calculateAnimationPose(vk::ArrayProxy<vka::Transform> nodeSpaceTransforms,
                                uint32_t animationIndex,
                                float timeVal) const
    {
        auto & S = *this;
        if( animationIndex >= S.animations.size() ) return false;

        auto & A = S.animations.at(animationIndex);

        glm::vec3 v;
        glm::quat q;
        for(auto & c : A.channels)
        {
            vka::Transform & tr = nodeSpaceTransforms.data()[c.node];

            if( c.samplers[0]!=-1)
            {
                A.samplers[ static_cast<size_t>(c.samplers[0]) ].sample(timeVal, &v, nullptr);
                tr.position = v;
            }
            if( c.samplers[1]!=-1)
            {
                A.samplers[ static_cast<size_t>(c.samplers[1]) ].sample(timeVal, nullptr, &q);
                tr.rotation = q;
            }
            if( c.samplers[2]!=-1)
            {
                A.samplers[ static_cast<size_t>(c.samplers[2]) ].sample(timeVal, &v, nullptr);
                tr.scale = v;
            }
        }
        return true;
    }




    bool calculateAnimationPoseBlend(vk::ArrayProxy<vka::Transform> nodeSpaceTransforms,
                                     uint32_t animationIndex,
                                     float timeVal,
                                     float factor) const
    {
        auto & S = *this;
        if( animationIndex >= S.animations.size() ) return false;

        auto & A = S.animations.at(animationIndex);

        if( nodeSpaceTransforms.size() != S.nodes.size() )
        {
            return false;
        }
        vka::Transform * outTransform = nodeSpaceTransforms.data();

        glm::vec3 v;
        glm::quat q;
        for(auto & c : A.channels)
        {
            vka::Transform & tr = outTransform[c.node];

            if( c.samplers[0]!=-1)
            {
                A.samplers[ static_cast<size_t>(c.samplers[0]) ].sample(timeVal, &v, nullptr);
                tr.position = glm::mix( tr.position, v, factor);
            }
            if( c.samplers[1]!=-1)
            {
                A.samplers[ static_cast<size_t>(c.samplers[1]) ].sample(timeVal, nullptr, &q);
                tr.rotation = glm::slerp( tr.rotation, q, factor);
            }
            if( c.samplers[2]!=-1)
            {
                A.samplers[ static_cast<size_t>(c.samplers[2]) ].sample(timeVal, &v, nullptr);
                tr.scale = glm::mix( tr.scale, v, factor);
            }
        }
        return true;
    }


    /**
     * @brief setAnimationState
     * @param animationIndex
     * @param timeVal
     * @param loop
     *
     * Place the scene in the keyframe specified by the timeVal.
     */
    inline void setAnimationState(uint32_t animationIndex, float timeVal)
    {
        auto & S = *this;
        if( animationIndex >= S.animations.size() ) return;

        auto & A = S.animations.at(animationIndex);

        glm::vec3 v;
        glm::quat q;
        for(auto & c : A.channels)
        {
            vka::Transform & tr = nodes[c.node].transform;

            if( c.samplers[0]!=-1)
            {
                A.samplers[ static_cast<size_t>(c.samplers[0]) ].sample(timeVal, &v, nullptr);
                tr.position = v;
            }
            if( c.samplers[1]!=-1)
            {
                A.samplers[ static_cast<size_t>(c.samplers[1]) ].sample(timeVal, nullptr, &q);
                tr.rotation = q;
            }
            if( c.samplers[2]!=-1)
            {
                A.samplers[ static_cast<size_t>(c.samplers[2]) ].sample(timeVal, &v, nullptr);
                tr.scale = v;
            }
        }
    }

    /**
     * @brief finalize
     *
     * Takes the original HostScene which mimicks the GLTF format
     * and adds extra references
     */
    void finalize()
    {
        for(auto & n : nodes)
        {
            n.parentIndex = -1;
            n.isRenderable=false;
        }

        // set the parents of all nodes.
        for(auto & n : nodes)
        {
            for(auto  c : n.children)
            {
                nodes[c].parentIndex = static_cast<int32_t>(std::distance( &nodes.front(), &n));
            }
            if( n.name == "")
            {
                n.name = "node_" + std::to_string(  static_cast<int32_t>(std::distance( &nodes.front(), &n)) );
            }
        }

        for(auto & A : animations)
        {
            std::sort( A.channels.begin(), A.channels.end(),
                       [](auto & a, auto & b){ return a.node < b.node;});

            if( A.name == "")
            {
                A.name = "animation_" + std::to_string(  static_cast<int32_t>(std::distance( &animations.front(), &A)) );
            }
        }

        for(auto & s : skins)
        {
            nodes[s.rootNode].rootSkeleton.push_back( static_cast<uint32_t>(std::distance(&skins.front(), &s)) );
        }

        // loop through all the primitives and check if
        // the material exists, if it doesn't, create a default material for
        // the primitive.
        bool needDefaultMaterial=false;
        for(auto & m : meshes)
        {
            for(auto & p : m.meshPrimitives)
            {
                if( p.materialIndex >= materials.size() )
                {
                    p.materialIndex = static_cast<uint32_t>( materials.size() );
                    needDefaultMaterial = true;
                }
            }
            if( m.name == "")
            {
                m.name = "mesh_" + std::to_string(  static_cast<int32_t>(std::distance( &meshes.front(), &m)) );
            }

        }

        if( needDefaultMaterial)
        {
            materials.emplace_back();
            materials.back().name = "defaultMaterial";
            materials.back().baseColorFactor = {0.8,0.8,0.8,1.0};
            materials.back().metallicFactor  = 0.1f;
            materials.back().roughnessFactor = 1.0f;
        }

        for(auto & n : nodes)
        {
            if( n.meshIndex.has_value() )
            {
                _setIsRenderable(  static_cast<size_t>(std::distance( &nodes.front(), &n) ), true);
            }
        }

    }

    void _setIsRenderable(size_t nodeIndex, bool value)
    {
        auto & n = nodes[nodeIndex];
        n.isRenderable = value;
        if( n.parentIndex != -1)
            _setIsRenderable( static_cast<size_t>(n.parentIndex), value);
    }

    /**
     * @brief calculateSceneTransform
     * @param nodeIndex
     * @return
     *
     * Calculates the root-node transform of node index, i.
     *
     * the root-node transform is the transform of  node i
     * if it were in the space of the root-node (ie: the models-space transform of the node)
     */
    vka::Transform calculateSceneTransform(size_t i) const
    {
        auto &N = nodes.at(i);
        int32_t parentIndex           = N.parentIndex;
        vka::Transform childTransform = N.transform;

        while(parentIndex!=-1)
        {
            childTransform = nodes.at(static_cast<size_t>(parentIndex) ).transform * childTransform;
            parentIndex    = nodes.at(static_cast<size_t>(parentIndex) ).parentIndex;
        }
        return childTransform;
    }

    /**
     * @brief calculateSceneTransform
     * @param i
     * @param S
     * @param nodeTransforms
     * @return
     *
     * Calculates the root-node transform of node i in scene S, using the nodeTransforms array
     * for the node-space transforms
     */
    static vka::Transform calculateSceneTransform(size_t i, SceneBase const & S, vk::ArrayProxy<vka::Transform> nodeTransforms)
    {
        int32_t parentIndex           = S.nodes.at(i).parentIndex;
        vka::Transform childTransform = nodeTransforms.data()[i];

        while( parentIndex!=-1 )
        {
            childTransform = nodeTransforms.data()[ static_cast<size_t>(parentIndex) ] * childTransform;
            parentIndex    = S.nodes.at( static_cast<size_t>(parentIndex)).parentIndex;
        }
        return childTransform;
    }
};

/**
 * @brief mix
 * @param output
 * @param p1
 * @param p2
 * @param blendValue
 *
 * Mixes two KeyFrameStates into a new KeyFrameState. p1 and p2 must be sorted in incremental node order.
 */
inline void mix(vka::SceneBase::Animation::KeyFrameState & output, vka::SceneBase::Animation::KeyFrameState const & p1, vka::SceneBase::Animation::KeyFrameState const & p2, float blendValue)
{
    uint32_t i=0, j=0;
    uint32_t I=static_cast<uint32_t>(p1.size()), J=static_cast<uint32_t>(p2.size());
    output.clear();
    while(i<I && j<J)
    {
        if( p1[i].node == p2[j].node )
        {
            output.emplace_back( p1[i] );
            output.back().value = mix(p1[i].value, p2[j].value, blendValue);
            ++i;
            ++j;
        }
        else
        {
            if( p1[i].node < p2[j].node)
            {
                output.emplace_back(p1[i]);
                ++i;
            }
            else
            {
                output.emplace_back(p2[j]);
                ++j;
            }
        }
    }
}

struct HostScene : public SceneBase
{

    std::vector< vka::HostTriPrimitive   > rawPrimitives;
    std::vector< vka::HostImage          > images;

    /**
     * @brief optimizePrimitives
     *
     * Combines rawPrimitives of similar attributes
     */
    void optimizePrimitives()
    {
        if( rawPrimitives.size() == 0)
            return;

        std::vector< vka::HostTriPrimitive   > newPrimitives;

        for(auto & oldRawPrim : rawPrimitives)
        {
            if( newPrimitives.size() == 0)
            {
                newPrimitives.push_back( oldRawPrim);
                continue;
            }

            auto oldRawIndex = static_cast<uint32_t>( std::distance( &rawPrimitives.front(), &oldRawPrim ) );
            for(auto & newRawPrim : newPrimitives)
            {
                auto newRawIndex = static_cast<uint32_t>( std::distance(&newPrimitives.front() , &newRawPrim   ) );
                if( newRawPrim.isSimilar(oldRawPrim))
                {
                    // can be merged
                    auto newDrawCall = newRawPrim.append(oldRawPrim, false);

                    // update all primitives with the new index
                    for(auto & p : primitives)
                    {
                        if( p.rawPrimitiveIndex == oldRawIndex)
                        {
                            p.rawPrimitiveIndex     = newRawIndex;

                            p.drawCall.vertexOffset += newDrawCall.vertexOffset;
                            p.drawCall.firstIndex   += newDrawCall.firstIndex;
                        }
                    }
                }
            }
        }
        rawPrimitives = std::move(newPrimitives);
    }



    static vka::HostScene createGizmoScene()
    {
        auto rawPrimitive = vka::HostTriPrimitive::Box(1,1,1);
        auto sphere       = vka::HostTriPrimitive::Cylinder(1.0,0.1f);


        auto box_dc    = rawPrimitive.getDrawCall();
        auto sphere_dc = rawPrimitive.append(sphere,false);

        vka::HostScene m_scene;

        // place the raw primitive into the
        m_scene.rawPrimitives.resize(1);
        m_scene.rawPrimitives[0] = std::move(rawPrimitive);


        m_scene.primitives.resize(2);
        m_scene.primitives[0].drawCall          = box_dc;
        m_scene.primitives[0].rawPrimitiveIndex = 0;

        m_scene.primitives[1].drawCall          = sphere_dc;
        m_scene.primitives[1].rawPrimitiveIndex = 0;




        {
            vka::HostScene::Material m;
            m.baseColorFactor = {1,0,0,1}; m_scene.materials.push_back(m);
            m.baseColorFactor = {0,1,0,1}; m_scene.materials.push_back(m);
            m.baseColorFactor = {0,0,1,1}; m_scene.materials.push_back(m);
        }

        m_scene.meshes.resize(6);


        for(uint32_t i=0;i<6u;i+=2)
        {
            m_scene.meshes[i+0].meshPrimitives.resize(1);
            m_scene.meshes[i+0].meshPrimitives[0].primitiveIndex = 0;
            m_scene.meshes[i+0].meshPrimitives[0].materialIndex  = i/2;

            m_scene.meshes[i+1].meshPrimitives.resize(1);
            m_scene.meshes[i+1].meshPrimitives[0].primitiveIndex = 1;
            m_scene.meshes[i+1].meshPrimitives[0].materialIndex  = i/2;
        }

        m_scene.nodes.resize(11);

        m_scene.nodes[1].transform.position = {1.f,0.f,0.f};
        m_scene.nodes[2].transform.position = {0.f,1.f,0.f};
        m_scene.nodes[3].transform.position = {0.f,0.f,1.f};

        m_scene.nodes[1].transform.scale = {1.0f,0.2f,0.2f};
        m_scene.nodes[2].transform.scale = {0.2f,1.0f,0.2f};
        m_scene.nodes[3].transform.scale = {0.2f,0.2f,1.0f};


        {
            float r = 1.0f;
            m_scene.nodes[4].transform.position = {0.f,r,r};  m_scene.nodes[4].transform.rotateLocal( {0,1,0}, glm::half_pi<float>());
            m_scene.nodes[5].transform.position = {r,0.f,r};  m_scene.nodes[5].transform.rotateLocal( {1,0,0}, glm::half_pi<float>());
            m_scene.nodes[6].transform.position = {r,r,0.f};

            m_scene.nodes[4].transform.scale = {0.2f,0.2f,0.2f};
            m_scene.nodes[5].transform.scale = {0.2f,0.2f,0.2f};
            m_scene.nodes[6].transform.scale = {0.2f,0.2f,0.2f};
        }


        m_scene.nodes[0].children = {1,2,3,4,5,6};

        m_scene.nodes[1].meshIndex = 0;
        m_scene.nodes[2].meshIndex = 2;
        m_scene.nodes[3].meshIndex = 4;

        m_scene.nodes[4].meshIndex = 1;
        m_scene.nodes[5].meshIndex = 3;
        m_scene.nodes[6].meshIndex = 5;


        {
            auto & axisScene = m_scene.scenes.emplace_back();
            axisScene.rootNodes.push_back(0);
            axisScene.name = "AxisScene";
        }

        {
            float w = 0.25f;
            float d = 0.05f;
            float r = 0.5f;
            m_scene.nodes[7].children = {8,9,10};

            m_scene.nodes[8].transform.scale    = {d,w,w};
            m_scene.nodes[8].transform.position = {0.0, r , r};
            m_scene.nodes[8].meshIndex            = 0;

            m_scene.nodes[9].transform.scale    = {w,d,w};
            m_scene.nodes[9].transform.position = {r, 0.0 , r};
            m_scene.nodes[9].meshIndex            = 2;

            m_scene.nodes[10].transform.scale    = {w,w,d};
            m_scene.nodes[10].transform.position = {r, r , 0.0};
            m_scene.nodes[10].meshIndex            = 4;
        }

        {
            auto & AxisPlaneScene = m_scene.scenes.emplace_back();
            AxisPlaneScene.rootNodes.push_back(7);
            AxisPlaneScene.name = "AxisPlaneScene";
        }

        m_scene.optimizePrimitives();
        return m_scene;
    }


    /**
     * @brief pushPrimitive
     * @param P
     * @return
     *
     * Push a HostTriPrimitive into the set of raw primitives and
     * returns the HostScene::Primitive.  This method
     * will search for other primitives within the host scene that
     * have similar vertex attributes and appends it to that
     */
    uint32_t pushPrimitive(vka::HostTriPrimitive const & P)
    {
        Primitive out;
        if( rawPrimitives.size() == 0)
        {
            out.drawCall          = P.getDrawCall();
            out.rawPrimitiveIndex = static_cast<uint32_t>( rawPrimitives.size() );
            rawPrimitives.push_back( P );
            primitives.push_back(out);
            return static_cast<uint32_t>(primitives.size()-1);
        }
        else
        {
            for(auto & pr : rawPrimitives)
            {
                if( pr.isSimilar(P) )
                {
                    out.drawCall = pr.append(P, false);
                    out.rawPrimitiveIndex = static_cast<uint32_t>( std::distance(&pr, &rawPrimitives.front() ) );
                    primitives.push_back(out);
                    return static_cast<uint32_t>( std::distance( &rawPrimitives.front(), &pr) );
                }
            }

            out.drawCall          = P.getDrawCall();
            out.rawPrimitiveIndex = static_cast<uint32_t>( rawPrimitives.size( ));
            rawPrimitives.push_back( P );
            primitives.push_back(out);
            return static_cast<uint32_t>( primitives.size()-1 );
        }
    }

};







}


#endif

