#ifndef VKA_UGLTF_HELPERS_H
#define VKA_UGLTF_HELPERS_H

#include <ugltf/ugltf.h>
#include <vka/utils/HostImage.h>
#include <vka/utils/PrimitiveDrawCall.h>
#include <vka/utils/HostTriMesh.h>

#include <vka/utils/HostImageLoader.h>

namespace vka
{

inline vka::HostImage convertUGLTFImage( uGLTF::Image const &I)
{
    if( I.bufferView == std::numeric_limits<uint32_t>::max()
            &&
            I.m_imageData.size() == 0)
    {
        throw std::runtime_error("Image not loaded properly");
    }
    else if( I.bufferView != std::numeric_limits<uint32_t>::max() )
    {
        void const * rawData  = I.getBufferView().data();
        auto totalBytes = I.getBufferView().byteLength;

        return loadHostImage( rawData, totalBytes );
    }
    if( I.m_imageData.size() )
        return loadHostImage( I.m_imageData.data(), I.m_imageData.size() );

    throw std::runtime_error("Image not loaded properly");
}

inline vka::HostTriPrimitive convertUGLTFPrimitiveToHostTriPrimitive( uGLTF::Primitive const &P)
{
    vka::HostTriPrimitive H;


    static_assert( static_cast<uGLTF::PrimitiveAttribute>(vka::PrimitiveAttribute::POSITION  )  == uGLTF::PrimitiveAttribute::POSITION   , "" );
    static_assert( static_cast<uGLTF::PrimitiveAttribute>(vka::PrimitiveAttribute::NORMAL    )  == uGLTF::PrimitiveAttribute::NORMAL	 , "" );
    static_assert( static_cast<uGLTF::PrimitiveAttribute>(vka::PrimitiveAttribute::TANGENT   )  == uGLTF::PrimitiveAttribute::TANGENT	 , "" );
    static_assert( static_cast<uGLTF::PrimitiveAttribute>(vka::PrimitiveAttribute::TEXCOORD_0)  == uGLTF::PrimitiveAttribute::TEXCOORD_0 , "" );
    static_assert( static_cast<uGLTF::PrimitiveAttribute>(vka::PrimitiveAttribute::TEXCOORD_1)  == uGLTF::PrimitiveAttribute::TEXCOORD_1 , "" );
    static_assert( static_cast<uGLTF::PrimitiveAttribute>(vka::PrimitiveAttribute::COLOR_0   )  == uGLTF::PrimitiveAttribute::COLOR_0	 , "" );
    static_assert( static_cast<uGLTF::PrimitiveAttribute>(vka::PrimitiveAttribute::JOINTS_0  )  == uGLTF::PrimitiveAttribute::JOINTS_0   , "" );
    static_assert( static_cast<uGLTF::PrimitiveAttribute>(vka::PrimitiveAttribute::WEIGHTS_0 )  == uGLTF::PrimitiveAttribute::WEIGHTS_0  , "" );

    std::pair<uGLTF::PrimitiveAttribute, vka::VertexAttribute*> attributes[] = {
                                                 {uGLTF::PrimitiveAttribute::POSITION   , &H.POSITION   },
                                                 {uGLTF::PrimitiveAttribute::NORMAL     , &H.NORMAL     },
                                                 {uGLTF::PrimitiveAttribute::TANGENT    , &H.TANGENT    },
                                                 {uGLTF::PrimitiveAttribute::TEXCOORD_0 , &H.TEXCOORD_0 },
                                                 {uGLTF::PrimitiveAttribute::TEXCOORD_1 , &H.TEXCOORD_1 },
                                                 {uGLTF::PrimitiveAttribute::COLOR_0    , &H.COLOR_0    },
                                                 {uGLTF::PrimitiveAttribute::JOINTS_0   , &H.JOINTS_0   },
                                                 {uGLTF::PrimitiveAttribute::WEIGHTS_0  , &H.WEIGHTS_0  }
                                            };

    for(auto & xx : attributes)
    {
        auto gltfAttr = xx.first;

        if( P.has( gltfAttr ) )
        {
            auto & ATTRIBUTE = *xx.second;

            auto & acc = P.getAccessor( gltfAttr );

            ATTRIBUTE.reset( to_string(acc.type),  static_cast<int32_t>(acc.componentType)  );

            ATTRIBUTE.resize( acc.count );

            auto start = static_cast<uint8_t*>( ATTRIBUTE.data() );

            acc.memcpy_all( start );
        }

    }


    if( P.hasIndices() )
    {
        auto & a = P.getIndexAccessor();

        H.INDEX.reset( to_string(a.type), static_cast<int32_t>(a.componentType));

        H.INDEX.resize( a.count );

        a.memcpy_all( H.INDEX.data() );


        {

            if( H.INDEX.attributeSize() == 1)
            {
                VertexAttribute INDEX16;
                INDEX16.reset<uint16_t>(1);

                for(uint32_t i=0;i<H.INDEX.count();i++)
                {
                    INDEX16.push_back<uint16_t>( H.INDEX.get<uint8_t>(i) );
                }
                H.INDEX = INDEX16;
            }
        }
    }


    return H;
}



}


#endif
