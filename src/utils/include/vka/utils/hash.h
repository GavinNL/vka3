#ifndef VKA_HASH_H
#define VKA_HASH_H

#include <cstddef>
#include <functional>

namespace vka
{


inline size_t hashCombine(size_t seed, size_t h2)
{
    //std::hash<T> hasher;
    seed ^= h2 + 0x9e3779b9 + (seed<<6) + (seed>>2);
    return seed;
}

template<typename T>
inline size_t hashCombine_t(size_t seed, T const & v)
{
    std::hash<T> hasher;
    auto h2 = hasher(v);
    return hashCombine(seed, h2);
}

}

#endif
