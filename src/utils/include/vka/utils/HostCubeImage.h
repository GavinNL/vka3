#pragma once
#ifndef VKA_HOST_CUBE_IMAGE_H
#define VKA_HOST_CUBE_IMAGE_H

#include<iostream>
#include<string.h>
#include<cassert>
#include<type_traits>
#include<functional>
#include<cstdint>
#include<cmath>
#include "HostImage.h"

namespace vka
{

class HostCubeImage : public HostImageArray
{
public:
   // std::vector<HostImage> pX; //TexturePath, width, height, numberOfChannels, &textureData[0]);
   // std::vector<HostImage> nX; //TexturePath , width, height, numberOfChannels, &textureData[1]);
   // std::vector<HostImage> pY; //TexturePath   , width, height, numberOfChannels, &textureData[2]);
   // std::vector<HostImage> nY; //TexturePath , width, height, numberOfChannels, &textureData[3]);
   // std::vector<HostImage> pZ; //TexturePath, width, height, numberOfChannels, &textureData[4]);
   // std::vector<HostImage> nZ; //TexturePath , width, height, numberOfChannels, &textureData[5]);


    HostCubeImage()
    {
        resize(8);
    }

    void resize(uint32_t w)
    {
        HostImageArray::resize(w,w,6);
    }

    void fromLatLong(HostImage const & LatLong, uint32_t outputCubeLength)
    {
        (void)LatLong;
        resize( outputCubeLength );

        //float C = static_cast<float>(outputCubeLength);
        float W = static_cast<float>(LatLong.width());
        float H = static_cast<float>(LatLong.height());

        for(uint32_t l=0;l<6;l++)
        {
            auto & _layer = getLayer(l);

            for(uint32_t j=0;j<outputCubeLength;j++)
            {
                for(uint32_t i=0;i<outputCubeLength;i++)
                {
                    float u = _map(    i, 0, outputCubeLength-1, 0, 1);
                    float v = 1.f-_map(j, 0, outputCubeLength-1, 0, 1);

                    float x,y,z;
                    _convert_cube_uv_to_xyz(l, u,v,&x, &y, &z);

                    float rho  = std::sqrt(x*x+y*y+z*z);
                    float lon  = std::atan2(z,x);
                    float lat  = std::asin( y/rho );

                    lon = _map(lon, -3.141592653589f, 3.141592653589f, 0.f, W);
                    lat = _map(lat, 3.141592653589f/2.0f, -3.141592653589f/2.0f, 0, H);

                    for(uint32_t c=0;c<LatLong.getChannels();c++)
                    {
                        _layer.getLevel(0)(i,j,c) = LatLong( static_cast<uint32_t>(lon) , static_cast<uint32_t>(lat), c );
                    }

                }
            }
        }
    }
    static HostImage fromLatLong(HostImage const & LatLong, uint32_t outputCubeLength, uint32_t faceIndex)
    {
        (void)LatLong;
        HostImage out;
        out.resize( outputCubeLength, outputCubeLength );

        //float C = static_cast<float>(outputCubeLength);
        float W = static_cast<float>(LatLong.width());
        float H = static_cast<float>(LatLong.height());

        uint32_t l=faceIndex;
       // for(uint32_t l=0;l<6;l++)
        {
            for(uint32_t j=0;j<outputCubeLength;j++)
            {
                for(uint32_t i=0;i<outputCubeLength;i++)
                {
                    float u = _map(    i, 0, outputCubeLength-1, 0, 1);
                    float v = 1.f-_map(j, 0, outputCubeLength-1, 0, 1);

                    float x,y,z;
                    x=y=z=0.0f;
                    _convert_cube_uv_to_xyz(l, u,v,&x, &y, &z);


                    float rho  = std::sqrt(x*x+y*y+z*z);
                    float lon  = std::atan2(z,x);
                    float lat  = std::asin( y/rho );

                    lon = _map(lon, -3.141592653589f, 3.141592653589f, 0.f, W);
                    lat = _map(lat, 3.141592653589f/2.0f, -3.141592653589f/2.0f, 0, H);

                    for(uint32_t c=0;c<LatLong.getChannels();c++)
                        out(i,j,c) = LatLong( static_cast<uint32_t>(lon) , static_cast<uint32_t>(lat), c );
                    //std::cout << lat << " , " << lon << std::endl;
                }
            }
        }
        return out;
    }
    static HostImage fromLatLong(HostImage const * LatLongp, uint32_t outputCubeLength, uint32_t faceIndex)
    {
        auto & LatLong = *LatLongp;
        (void)LatLong;
        HostImage out;
        out.resize( outputCubeLength, outputCubeLength );

        //float C = static_cast<float>(outputCubeLength);
        float W = static_cast<float>(LatLong.width());
        float H = static_cast<float>(LatLong.height());

        uint32_t l=faceIndex;
       // for(uint32_t l=0;l<6;l++)
        {
            for(uint32_t j=0;j<outputCubeLength;j++)
            {
                for(uint32_t i=0;i<outputCubeLength;i++)
                {
                    float u = _map(    i, 0, outputCubeLength-1, 0, 1);
                    float v = 1.f-_map(j, 0, outputCubeLength-1, 0, 1);

                    float x,y,z;
                    x=y=z=0.0f;
                    _convert_cube_uv_to_xyz(l, u,v,&x, &y, &z);


                    float rho  = std::sqrt(x*x+y*y+z*z);
                    float lon  = std::atan2(z,x);
                    float lat  = std::asin( y/rho );

                    lon = _map(lon, -3.141592653589f, 3.141592653589f, 0.f, W);
                    lat = _map(lat, 3.141592653589f/2.0f, -3.141592653589f/2.0f, 0, H);

                    for(uint32_t c=0;c<LatLong.getChannels();c++)
                        out(i,j,c) = LatLong( static_cast<uint32_t>(lon) , static_cast<uint32_t>(lat), c );
                    //std::cout << lat << " , " << lon << std::endl;
                }
            }
        }
        return out;
    }
    static float _map(uint32_t i, uint32_t a, uint32_t b, float A, float B)
    {
        float t = static_cast<float>(i-a) / static_cast<float>(b-a);
        return t*B + (1.f - t) * A;
    }
    static float _map(float i, float a, float b, float A, float B)
    {
        float t = (i-a) / (b-a);
        return t*B + (1.f - t) * A;
    }

    static void _convert_cube_uv_to_xyz(uint32_t index, float u, float v, float *x, float *y, float *z)
    {
      // convert range 0 to 1 to -1 to 1
      float uc = 2.0f * u - 1.0f;
      float vc = 2.0f * v - 1.0f;
      switch (index)
      {
          case 0: *x =  1.0f; *y =    vc; *z =   -uc; break;	// POSITIVE X
          case 1: *x = -1.0f; *y =    vc; *z =    uc; break;	// NEGATIVE X
          case 2: *x =    uc; *y =  1.0f; *z =   -vc; break;	// POSITIVE Y
          case 3: *x =    uc; *y = -1.0f; *z =    vc; break;	// NEGATIVE Y
          case 4: *x =    uc; *y =    vc; *z =  1.0f; break;	// POSITIVE Z
          case 5: *x =   -uc; *y =    vc; *z = -1.0f; break;	// NEGATIVE Z
      }
    }

    template<typename Callable_t>
    void apply( uint32_t mipLevel, Callable_t C)
    {
        auto outputCubeLength = getWidth();

        for(uint32_t l=0;l<6;l++)
        {
            auto & face = getLayer(l);
            auto & mipL = face.getLevel(mipLevel);

            for(uint32_t j=0;j<outputCubeLength;j++)
            {
                for(uint32_t i=0;i<outputCubeLength;i++)
                {
                    float u = _map(    i, 0, outputCubeLength-1, 0, 1);
                    float v = 1.f-_map(j, 0, outputCubeLength-1, 0, 1);

                    float x,y,z;
                    _convert_cube_uv_to_xyz(l, u,v,&x, &y, &z);

                    auto out = C(x,y,z);

                    mipL(i,j,0) = static_cast<uint8_t>(out.r * 255);
                    mipL(i,j,1) = static_cast<uint8_t>(out.g * 255);
                    mipL(i,j,2) = static_cast<uint8_t>(out.b * 255);
                    mipL(i,j,3) = 255;
                }
            }
        }
    }
};

}
#endif
