#pragma once
#ifndef VKA_HOST_IMAGE_H
#define VKA_HOST_IMAGE_H

#include<iostream>
#include<string.h>
#include<cassert>
#include<type_traits>
#include<functional>
#include<cstdint>
#include<cmath>
#include "HostImage3.h"

namespace vka
{

#if 0
struct pixel4
{
    uint8_t r;
    uint8_t g;
    uint8_t b;
    uint8_t a;

    pixel4()
    {

    }
    pixel4(uint8_t R,uint8_t G,uint8_t B,uint8_t A) : r(R), g(G), b(B), a(A)
    {

    }
    pixel4(uint32_t c)
    {
        memcpy( &r, &c, sizeof(uint32_t));
    }
    uint8_t& operator[](size_t i)
    {
        return (&r)[i];
    }
    uint8_t const& operator[](size_t i) const
    {
        return (&r)[i];
    }
};


/**
 * @brief The channel1f struct
 *
 * Essentially a 1D image of floating point values.
 * Mostly used for intermediate stages;
 */
struct channel1f
{
      std::vector<float> data;

      channel1f(uint32_t w, uint32_t h) : _width(w), _height(h)
      {
          data.resize(w*h);
      }
      float & operator()(uint32_t u, uint32_t v)
      {
          return data[v*_width+u];
      }
      float const & operator()(uint32_t u, uint32_t v) const
      {
          return data[v*_width+u];
      }

      uint32_t width() const
      {
          return _width;
      }

      uint32_t height() const
      {
          return _height;
      }


private:
      uint32_t _width;
      uint32_t _height;
};


template<uint32_t offset>
struct channel_t
{
      using channel_type = channel_t<offset>;

      uint8_t & operator()(uint32_t u, uint32_t v)
      {
          return static_cast<uint8_t*>(static_cast<void*>(&ptr[v*width+u]))[offset];
      }
      uint8_t const & operator()(uint32_t u, uint32_t v) const
      {
          return static_cast<uint8_t*>(static_cast<void*>(&ptr[v*width+u]))[offset];
      }
      template<uint32_t N>
      channel_t<offset>& operator=( channel_t<N> const & other)
      {
          if( width  !=  other.width ||
              height !=  other.height)
          {
              throw std::logic_error("Channels are of different size");
          }

          auto *b = &(*this)(0,0);
          auto *e = b+(width*height*4);

          auto *b2 = &other(0u,0u);

          while(b!=e)
          {
              *b = *b2;
              b+=4; b2+=4;
          }
          return *this;
      }

      channel_t<offset>& operator=( uint8_t val)
      {
          auto *b = &(*this)(0,0);
          auto *e = b+(width*height*4);

          while(b!=e)
          {
              *b = val;
              b+=4;
          }
          return *this;
      }

      channel_t<offset>& operator=( int val )
      {
          return this->operator=( static_cast<uint8_t>(val) );
      }

      channel_t<offset>& operator=( float val )
      {
          return this->operator=( static_cast<uint8_t>(val*255.0f) );
      }

      channel_t<offset>& operator=( channel1f && val )
      {
          //std::cout << "Moving OneChannel" << std::endl;
          auto *b = &(*this)(0,0);
          auto *e = b+(width*height*4);
          auto  v = val.data.begin();

          while(b!=e)
          {
              *b = 255 * *v++;
              b+=4;
          }
          return *this;
      }

      channel_t<offset>& operator=( channel1f const &val )
      {
          //std::cout << "Copying OneChannel" << std::endl;
          auto *b = &(*this)(0,0);
          auto *e = b+(width*height*4);
          auto  v = val.data.begin();

          while(b!=e)
          {
              *b = 255.0f * *v++;
              b+=4;
          }
          return *this;
      }

      template<uint32_t N>
      channel1f operator+( channel_t<N> const & other) const
      {
          channel1f R(width,height);

          auto *b1 = &(*this)(0u,0u);
          auto *b2 = &other(0u,0u);

          float sc = 1.0f / 255.0f;
          for(auto & b : R.data)
          {
              b = static_cast<float>( *b1 + *b2 ) * sc;
              b1+=4; b2+=4;
          }

          return R;
      }
      template<uint32_t N>
      channel1f operator*( channel_t<N> const & other) const
      {
          channel1f R(width,height);

          auto *b1 = &(*this)(0u,0u);
          auto *b2 = &other(0u,0u);

          float sc = 1.0f / (255.0f*255.0f);

          for(auto & b : R.data)
          {
              float n = static_cast<float>(*b1 * *b2);
              b = n * sc;
              b1+=4; b2+=4;
          }

          return R;
      }

      template<typename Callable_t>
      void apply( Callable_t C)
      {
          float sw = 1.0f / float(width);
          float sh = 1.0f / float(height);

          for(uint32_t v = 0; v < height; ++v)
          for(uint32_t u = 0; u < width; ++u)
          {
              float x = static_cast<float>(u) * sw;
              float y = static_cast<float>(v) * sh;

              (*this)(u,v) =  static_cast<uint8_t>( C(x,y) * 255);
          }
      }

      template<typename Callable_t>
      channel_t<offset>& operator=( Callable_t C )
      {
          apply(C);
          return *this;
      }

    private:
      uint32_t  width;
      uint32_t height;
      uint32_t   *ptr;

      friend class HostImage;
};

template<uint32_t N>
channel1f operator * (channel_t<N> const & a, float b)
{
    channel1f D(a.width,a.height);

    auto *Y = &a(0,0);

    float sc = 1.0f/255.0f;

    for(auto & v : D.data)
    {
        v = sc * *Y * b;
        Y+=4;
    }
    return D;
}

template<uint32_t N>
channel1f operator * (float b, channel_t<N> const & a)
{
    return operator*(a,b);
}



template<uint32_t N>
channel1f operator + (channel_t<N> const & a, float b)
{
    //std::cout << "construct OneChannel+float" << std::endl;
    channel1f D(a.width,a.height);

    auto *Y = &a(0,0);

    float sc = 1.0f/255.0f;

    for(auto & v : D.data)
    {
        v = *Y*sc + b;
        Y+=4;
    }
    return D;
}

template<uint32_t N>
channel1f operator + (float b, channel_t<N> const & a )
{
    return operator+(a,b);
}


inline channel1f&& operator + (channel1f && D, channel1f && E)
{
    //std::cout << "move OneChannel+OneChannel" << std::endl;
    auto b = E.data.begin();
    for(auto & v : D.data)
    {
        v += *b++;
    }
    return std::move(D);
}

inline channel1f&& operator + (channel1f && D, float b)
{
    for(auto & v : D.data)
    {
        v += b;
    }
    return std::move(D);
}

inline channel1f&& operator - (channel1f && D, float b)
{
    return operator+( std::move(D),-b);
}



template<uint32_t N>
channel1f operator - (channel_t<N> const & a, float b)
{
    return operator+(a,-b);
}

template<uint32_t N>
channel1f operator - (float b, channel_t<N> const & a )
{
    //std::cout << "Construct OneChannel+float" << std::endl;
    channel1f D(a.width,a.height);

    auto *Y = &a(0,0);

    float sc = 1.0f / 255.0f;

    for(auto & v : D.data)
    {
        v = b - *Y*sc;
        Y+=4;
    }
    return D;
}

class HostImage
{
public:


    using value_type = uint32_t;

    HostImage()
    {
        r.width = r.height = 0;
        r.ptr = nullptr;
    }
    ~HostImage()
    {
        clear();
    }

    void clear()
    {
        r.width = r.height = 0;
        if(r.ptr) delete [] r.ptr;
    }

    HostImage(const HostImage & other) : HostImage()
    {

        resize(other.width(), other.height());

        auto x = other.begin();
        for(auto & v : *this)
        {
            v = *x++;
        }
    }

    HostImage(HostImage && other)
    {
        r.ptr = other.r.ptr;
        r.width = other.r.width;
        r.height = other.r.height;
        other.r.ptr = nullptr;
    }

    HostImage& operator=(const HostImage & other)
    {
        if( &other != this)
        {
            resize(other.width(), other.height());

            auto x = other.begin();
            for(auto & v : *this)
            {
                v = *x++;
            }
        }
        return *this;
    }
    HostImage& operator=(HostImage && other)
    {
        if( &other != this)
        {
            r.ptr       = other.r.ptr;
            r.width     = other.r.width;
            r.height    = other.r.height;
            other.r.ptr = nullptr;
        }
        return *this;
    }
    void resize(uint32_t w, uint32_t h)
    {
        if(r.width*r.height != w*h)
        {
            if( r.ptr )
            {
                delete [] r.ptr;
                r.ptr = nullptr;
            }
            r.ptr   = new uint32_t[w*h];
        }
        if( r.ptr != nullptr )
            r.ptr   = new uint32_t[w*h];

        r.width  = w;
        r.height = h;
    }

    void copyFromBuffer(void const * src, uint32_t totalBytes, uint32_t width, uint32_t height)
    {
          assert(totalBytes%sizeof(uint32_t)==0);

          resize(width,height);
          memcpy(data(), src, totalBytes);
    }

    value_type & operator()(uint32_t u, uint32_t v)
    {
        return r.ptr[v*r.width+u];
    }
    value_type const & operator()(uint32_t u, uint32_t v) const
    {
        return r.ptr[v*r.width+u];
    }

    /**
     * @brief sample
     * @param u
     * @param v
     * @return
     *
     * Samples a 2x2 block of pixels and returns the average. really only used for
     * the nextMipMap() method.
     */
    value_type sample(uint32_t u, uint32_t v) const
    {
        auto & R = *this;

        pixel4 c1 = R(u,v);
        pixel4 c2 = R(u,v+1);
        pixel4 c3 = R(u+1,v);
        pixel4 c4 = R(u+1,v+1);

        pixel4 out(0);
        out.r = static_cast<uint8_t>( (static_cast<uint32_t>(c1.r) + static_cast<uint32_t>(c2.r) + static_cast<uint32_t>(c3.r) + static_cast<uint32_t>(c4.r) ) / 4u);
        out.g = static_cast<uint8_t>( (static_cast<uint32_t>(c1.g) + static_cast<uint32_t>(c2.g) + static_cast<uint32_t>(c3.g) + static_cast<uint32_t>(c4.g) ) / 4u);
        out.b = static_cast<uint8_t>( (static_cast<uint32_t>(c1.b) + static_cast<uint32_t>(c2.b) + static_cast<uint32_t>(c3.b) + static_cast<uint32_t>(c4.b) ) / 4u);
        out.a = static_cast<uint8_t>( (static_cast<uint32_t>(c1.a) + static_cast<uint32_t>(c2.a) + static_cast<uint32_t>(c3.a) + static_cast<uint32_t>(c4.a) ) / 4u);

        uint32_t uu;
        memcpy(&uu, &out, sizeof(v));
        return uu;
    }

    /**
     * @brief nextMipMap
     * @return
     *
     * Returns the next mipmap level of the image. The next mipmap level has
     * width and height which is half the original.
     */
    HostImage nextMipMap() const
    {
        HostImage out;
        out.resize( width()/2, height()/2);

        for(uint32_t j=0;j<out.height();j++)
        {
            for(uint32_t i=0;i<out.width();i++)
            {
                out(i,j) = sample( i*2, j*2 );
            }
        }
        return out;
    }

    void const* data() const
    {
        return r.ptr;
    }
    void * data()
    {
        return r.ptr;
    }
    size_t size() const
    {
        return width()*height()*sizeof(value_type);
    }

    uint32_t width() const
    {
        return r.width;
    }
    uint32_t height() const
    {
        return r.height;
    }

    value_type* begin()
    {
        return r.ptr;
    }

    value_type* end()
    {
        return begin() + width()*height();
    }
    value_type const* begin() const
    {
        return r.ptr;
    }

    value_type const * end() const
    {
        return begin() + width()*height();
    }
public:
//#if defined VKA_HASH_H
//    size_t hash;

//    void setHash()
//    {
//        hash = computeHash();
//    }

    size_t hash() const
    {
        auto     w = width();
        auto     h = height();
        uint32_t c = 4;

        auto hashCo = [](size_t _seed, size_t h2)
        {
            _seed ^= h2 + 0x9e3779b9 + (_seed<<6) + (_seed>>2);
            return _seed;
        };

        std::hash<uint32_t> Hu;

        auto seed = Hu(w);
        seed = hashCo(seed, Hu(h) );
        seed = hashCo(seed, Hu(c) );

        auto * begin = static_cast<uint32_t const*>(data());
        for(uint32_t i=0;i<w*h;i++)
        {
            seed = hashCo(seed, Hu(*begin++));
        }
        return seed;
    }
//#endif

    union
    {
        channel_t<0> r;
        channel_t<1> g;
        channel_t<2> b;
        channel_t<3> a;
    };


    /**
     * @brief X
     * @param width
     * @param height
     * @return
     *
     * Returns a oneD_Channel where the value of the channel increases
     * linearly in the u direction
     */
    static channel1f X(uint32_t width, uint32_t height)
    {
        channel1f D(width,height);

        float sc = 1.0f / float(width);
        for( uint32_t v=0; v< height; ++v)
        {
            for( uint32_t u=0; u<width; ++u)
            {
                float x = static_cast<float>(u) * sc;
                D(u,v) = x;
            }
        }
        return D;
    }

    /**
     * @brief Y
     * @param width
     * @param height
     * @return
     *
     * Returns a oneD_Channel where the value of the channel increases
     * linearly in the v direction
     */
    static channel1f Y(uint32_t width, uint32_t height)
    {
        channel1f D(width,height);

        float sc = 1.0f / float(height);
        for( uint32_t v=0; v< height; ++v)
        {
            float y = static_cast<float>(v) * sc;
            for( uint32_t u=0; u<width; ++u)
            {
                D(u,v) = y;
            }
        }
        return D;
    }
};

inline pixel4 mix( pixel4 const & a, pixel4 const & b, float t)
{
    return
    {
        static_cast<uint8_t>( (float(a.r) * (1.0f-t) + float(b.r)*t)),
        static_cast<uint8_t>( (float(a.g) * (1.0f-t) + float(b.g)*t)),
        static_cast<uint8_t>( (float(a.b) * (1.0f-t) + float(b.b)*t)),
        static_cast<uint8_t>( (float(a.a) * (1.0f-t) + float(b.a)*t))
    };
}

inline pixel4 mix( pixel4 const & a, pixel4 const & b, pixel4 const t)
{
    const float sc = 1.0f/255.0f;
    const float tr = t.r * sc;
    const float tg = t.g * sc;
    const float tb = t.b * sc;
    const float ta = t.a * sc;
    return pixel4
    {
        static_cast<uint8_t>( (float(a.r) * (1.0f-tr) + float(b.r)*tr)),
        static_cast<uint8_t>( (float(a.g) * (1.0f-tg) + float(b.g)*tg)),
        static_cast<uint8_t>( (float(a.b) * (1.0f-tb) + float(b.b)*tb)),
        static_cast<uint8_t>( (float(a.a) * (1.0f-ta) + float(b.a)*ta))
    };
}
template<uint32_t N, uint32_t M>
inline channel1f mix( channel_t<N> const a, channel_t<M> const b, float t)
{
    channel1f D( a.width, a.height);

    auto * A = &a(0,0);
    auto * B = &b(0,0);

    float sc = 1.0f/255.0f;
    for(auto & v : D.data)
    {

        float x1 = static_cast<float>(*A);
        float x2 = static_cast<float>(*B);
        v   = ( (1.0f - t) * x1 + t * x2 ) * sc;

        A+=4; B+=4;
    }
    return D;
}

template<uint32_t N, uint32_t M, uint32_t P>
inline channel1f mix( channel_t<N> const a, channel_t<M> const b, channel_t<P> const _t)
{
    channel1f D( a.width, a.height);

    auto * A = &a(0,0);
    auto * B = &b(0,0);
    auto * T = &_t(0,0);

    float sc = 1.0f/255.0f;
    for(auto & v : D.data)
    {
        float t = *T * sc;

        v  = ( (1.0f - t) * (*A) + t * (*B) ) * sc;

        A+=4; B+=4; T+=4;
    }
    return D;
}


inline HostImage mix( HostImage const a, HostImage const b, HostImage const _t)
{
    HostImage D;
    D.resize(a.width(), a.height());

    pixel4 const * A = static_cast<pixel4 const*>(a.data());
    pixel4 const * B = static_cast<pixel4 const*>(b.data());
    pixel4 const * T = static_cast<pixel4 const*>(_t.data());

    pixel4 * C = static_cast<pixel4*>(D.data());

    auto end = C + D.width()*D.height();

    while(C!=end)
    {
        *C++ = mix(*A++, *B++, *T++);
    }

    return D;
}

inline HostImage mix( HostImage const a, HostImage const b, float _t)
{
    HostImage D;
    D.resize(a.width(), a.height());

    pixel4 const * A = static_cast<pixel4 const*>(a.data());
    pixel4 const * B = static_cast<pixel4 const*>(b.data());

    pixel4 * C = static_cast<pixel4*>(D.data());

    auto end = C + D.width()*D.height();

    while( C != end)
    {
        *C++ = mix(*A++, *B++, _t);
    }

    return D;
}


}


namespace std
{
template<>
struct hash<vka::HostImage>
{
    static inline size_t hashCombine(size_t seed, size_t h2)
    {
        //std::hash<T> hasher;
        seed ^= h2 + 0x9e3779b9 + (seed<<6) + (seed>>2);
        return seed;
    }

    std::size_t operator()(vka::HostImage const & img) const noexcept
    {
        auto     w = img.width();
        auto     h = img.height();
        uint32_t c = 4;

        std::hash<uint32_t> Hu;

        auto seed = Hu(w);
        seed = hashCombine(seed, Hu(h) );
        seed = hashCombine(seed, Hu(c) );

        auto * begin = static_cast<uint32_t const*>(img.data());
        for(uint32_t i=0;i<w*h;i++)
        {
            seed = hashCombine(seed, Hu(*begin++));
        }
        return seed;
    }
};

#endif

}
#endif
