#pragma once
#ifndef VKA_RESOURCE_PATH
#define VKA_RESOURCE_PATH

#include<string>
#include<algorithm>
#include<memory>
#include<vector>
#include<stdexcept>

#include "filesystem.h"

namespace vka
{

/**
 * @brief The Resources class
 *
 * The resources container allows you to add root paths to it. for example
 *
 * Resources r;
 * r.addPath("/usr/bin");
 * r.addPath("/usr/local/bin");
 * r.addPath("/bin");
 *
 * assert( r.get("bash") == "/bin/bash");
 *
 */
class Resources
{

public:
    std::vector< std::string > roots;


    /**
     * @brief addPath
     * @param path
     *
     * Add a path to the set of resource paths.
     */
    void addPath(const std::string path)
    {
        roots.push_back(path);
    }

    /**
     * @brief clearPaths
     *
     * Clear all the paths.
     */
    void clearPaths()
    {
        roots.clear();
    }


    /**
     * @brief get
     * @param file_name
     * @return
     *
     * Gets a resource relative to the paths set in the
     * environment variable.
     *
     * The env variable (which is defined using the setEnvVar( ) method )
     * is checked for a list of : separated paths, similar to how the PATH
     * variable set.
     *
     * It first checks all the paths in the env variable, if it is not found
     * it will check the current working directory. If the file is not found
     * it will throw an error.
     */
    std::string get(const std::string & file_name) const
    {
        for(auto p1 : roots )
        {
            auto abs_path = vka::fs::join(p1, file_name);
            if( vka::fs::exists(abs_path) )
            {
                return abs_path;
            }
        }
        throw std::runtime_error("Could not find resource: " + file_name);
    }


    static std::vector<std::string> tokenize(const std::string& str, const std::string& delimiters = ":")
    {
        using namespace std;
        vector<string> tokens;

        // Skip delimiters at beginning.
        string::size_type lastPos = str.find_first_not_of(delimiters, 0);

        // Find first non-delimiter.
        string::size_type pos = str.find_first_of(delimiters, lastPos);

        while (string::npos != pos || string::npos != lastPos)
        {
            // Found a token, add it to the vector.
            tokens.push_back(str.substr(lastPos, pos - lastPos));

            // Skip delimiters.
            lastPos = str.find_first_not_of(delimiters, pos);

            // Find next non-delimiter.
            pos = str.find_first_of(delimiters, lastPos);
        }
        return tokens;
    }
};


}


#endif
