#ifndef VKA_ANIMATION_SAMPLER_H
#define VKA_ANIMATION_SAMPLER_H

#include <vka/math/transform.h>
#include <iostream>
#include <variant>
#include <vector>
#include <algorithm>
#include <tuple>

namespace vka
{


/**
 * @brief The AnimationSampler_t struct
 *
 * Animation sampler for interpolation
 *
 * The AnimationSampler basically keeps track of two things
 * a key value (usually a floating point time value) and
 * the value type that is meant to be sampled (for example, vec3).
 *
 * Insert values into the AnimationSampler by using
 * insert()
 *
 * Then, you can find the interpolant by calling
 * find( )  at some some time-value.
 *
 * It will return an interpolation that
 * gives you the interator between two frames, and
 * a factor which is the interpolatino factor
 * between the two frames.
 *
 */
template<typename key_t, typename T>
struct AnimationSampler_t
{
    using key_type          = key_t;
    using value_type        = T;

    struct frame_value_type
    {
        key_type   time;
        value_type value;
    };

    using iterator          = typename std::vector<frame_value_type>::iterator;
    using const_iterator    = typename std::vector<frame_value_type>::const_iterator;

    struct interpolant
    {
        const_iterator   frame1;
        const_iterator   frame2;
        key_type         factor;
    };

    /**
     * @brief insert
     * @param t
     * @param v
     *
     * Insert a value into the sampler and returns
     * an interator to where it was inserted
     */
    auto insert(key_type t, value_type v)
    {
        auto it =
        std::lower_bound( m_data.begin(), m_data.end(), t, [](auto & a, auto & b){ return a.time < b; } );

        return m_data.insert( it, frame_value_type{t,v} );
    }

    auto begin()      { return m_data.begin(); }
    auto end()        { return m_data.end();   }

    auto begin() const { return m_data.begin(); }
    auto end()   const { return m_data.end();   }

    auto size() const { return m_data.size(); }

    auto & at(size_t i) const
    {
        return m_data.at(i);
    }
    frame_value_type const& operator[](size_t i) const
    {
        return m_data[i];
    }


    interpolant find(key_type t) const
    {
        if( t > m_data.back().time )
        {
            return interpolant{ m_data.end()-1, m_data.end()-1, static_cast<key_type>(0.0)};
        }
        else if( t < m_data.front().time )
        {
            return interpolant{ m_data.begin(), m_data.begin(), static_cast<key_type>(0.0f)};
        }
        auto it2 =
        std::upper_bound( m_data.begin(), m_data.end(), t,
                   [](auto & a, auto & b){ return a < b.time; } );

        auto it1 = it2-1;

        if( it2->time < t)
        {
            return interpolant{it2-1, it2, static_cast<key_type>(1.0) };
        }
        auto t0 = (t - it1->time) / (it2->time - it1->time);

        return interpolant{it1,it2,t0};
    }

private:

    std::vector<frame_value_type> m_data;
};


}


#endif

