#ifndef VKA2_HOST_TRI_MESH_H
#define VKA2_HOST_TRI_MESH_H

#include <array>
#include <vector>
#include <string.h>
#include <map>
#include <variant>

#include <cassert>
#include "ModelTypes.h"
#include "VulkanHelperFunctions.h"
#include "PrimitiveDrawCall.h"
#include <ugltf/ugltf.h>
#include <vka/math/linalg.h>
#include <vka/utils/hash.h>

namespace vka
{


struct VertexAttribute
{
protected:
    std::vector< uint8_t > m_data;
    uint16_t               m_gltfComponentType=0;
    uint16_t               m_componentCount=0;

public:

    template<typename From_t, typename To_t>
    VertexAttribute convert() const
    {
        auto b = &m_data.front();
        auto e = b + m_data.size();

        VertexAttribute newV;
        newV.reset<To_t>(m_componentCount);

        while(b!=e)
        {
            From_t from;

            std::memcpy(&from, b, sizeof(From_t));

            To_t to = static_cast<To_t>(from);

            newV.push_back( &to, sizeof(to));

            b += sizeof(From_t);
        }
        return newV;
    }

    template<typename To_t>
    VertexAttribute convert() const
    {
        switch( m_gltfComponentType )
        {
            case /*BYTE          */ 5120:
            return convert<int8_t, To_t>();
            case /*UNSIGNED_BYTE */ 5121:
            return convert<int8_t, To_t>();
            case /*SHORT         */ 5122:
            return convert<int8_t, To_t>();
            case /*UNSIGNED_SHORT*/ 5123:
            return convert<int8_t, To_t>();
            case /*INT           */ 5124:
            return convert<int8_t, To_t>();
            case /*UNSIGNED_INT  */ 5125:
            return convert<int8_t, To_t>();
            case /*FLOAT         */ 5126:
            return convert<int8_t, To_t>();
            case /*DOUBLE        */ 5130:
            return convert<int8_t, To_t>();
        }
        throw std::runtime_error("unknown type");
    }

    template<typename T, typename Callable_t>
    void forEach( Callable_t && C)
    {
        auto b = &m_data.front();
        auto e = b + m_data.size();

        auto ar = attributeSize();

        while(b!=e)
        {
            std::array<T, 4> from = {};

            std::memcpy(&from, b, ar );

            C(from);

            b += attributeSize();
        }
    }

    size_t hash() const
    {
        std::hash<size_t> Hs;
        std::hash<uint8_t> Hb;

        auto seed  =  Hs( static_cast<size_t>(m_gltfComponentType) );

        seed = hashCombine(seed,  Hs( static_cast<size_t>(m_componentCount) ) );

        for(auto & b : m_data)
        {
            seed = hashCombine(seed, Hb(b) );
        }

        return seed;
    }

    template<typename ComponentType>
    static int32_t getComponentType()
    {
        if(      std::is_same<  int8_t, ComponentType >::value ) return /*BYTE          */ 5120;
        else if( std::is_same< uint8_t, ComponentType >::value ) return /*UNSIGNED_BYTE */ 5121;
        else if( std::is_same< int16_t, ComponentType >::value ) return /*SHORT         */ 5122;
        else if( std::is_same<uint16_t, ComponentType >::value ) return /*UNSIGNED_SHORT*/ 5123;
        else if( std::is_same< int32_t, ComponentType >::value ) return /*INT           */ 5124;
        else if( std::is_same<uint32_t, ComponentType >::value ) return /*UNSIGNED_INT  */ 5125;
        else if( std::is_same< float  , ComponentType >::value ) return /*FLOAT         */ 5126;
        else if( std::is_same< double , ComponentType >::value ) return /*DOUBLE        */ 5130;
        else
        {
            throw std::runtime_error("In valid type");
        }
    }

    template<typename ComponentType>
    void reset(uint16_t componentCount)
    {
        int32_t _type = getComponentType<ComponentType>();
        reset(componentCount, _type);
    }

    void reset(uint16_t compCount, int32_t componentType)
    {
        if(compCount > 4) throw std::runtime_error("compCount must be a value between 1-4");

        switch( componentType )
        {
             case /*BYTE          */ 5120: break;
             case /*UNSIGNED_BYTE */ 5121: break;
             case /*SHORT         */ 5122: break;
             case /*UNSIGNED_SHORT*/ 5123: break;
             case /*INT           */ 5124: break;
             case /*UNSIGNED_INT  */ 5125: break;
             case /*FLOAT         */ 5126: break;
             case /*DOUBLE        */ 5130: break;
             default:
                throw std::runtime_error("Incorrect componentType, must be one of the following: 5120(byte), 5121(ubyte), 5122(short), 5123(ushort), 5124(int), 5125(uint), 5126(float), 5130(double)");
        }

        m_gltfComponentType  = static_cast<uint16_t>(componentType);
        m_componentCount     = compCount;
        m_data.clear();
    }

    void reset(const std::string accessorType, int32_t componentType)
    {
        uint16_t newCompCount=0;
        if( accessorType == "SCALAR") newCompCount = 1;
        if( accessorType == "VEC2"  ) newCompCount = 2;
        if( accessorType == "VEC3"  ) newCompCount = 3;
        if( accessorType == "VEC4"  ) newCompCount = 4;
        reset(newCompCount, componentType);
    }

    /**
     * @brief count
     * @return
     *
     * Returns the total number of attributes
     */
    size_t count() const
    {
        if( attributeSize()==0) return 0;
        return byteLength() / attributeSize();
    }

    /**
     * @brief components
     * @return
     * Returns the number of components per vertex attribute,
     * will be between 1-4.
     */
    size_t components() const
    {
        return m_componentCount;
    }

    /**
     * @brief attributeSize
     * @return
     *
     * Returns the byte size of the vertex attribute.
     * eg: vec3 -> componentSize() * components() = 4 * 3 = 12
     */
    size_t attributeSize() const
    {
        return componentSize() * components();
    }
    /**
     * @brief componentSize
     * @return
     *
     * Returns number of bytes of the attribute component.
     */
    size_t componentSize() const
    {
        switch( m_gltfComponentType )
        {
             case /*BYTE          */ 5120: return 1;
             case /*UNSIGNED_BYTE */ 5121: return 1;
             case /*SHORT         */ 5122: return 2;
             case /*UNSIGNED_SHORT*/ 5123: return 2;
             case /*INT           */ 5124: return 4;
             case /*UNSIGNED_INT  */ 5125: return 4;
             case /*FLOAT         */ 5126: return 4;
             case /*DOUBLE        */ 5130: return 8;
             default: return 0;
        }
    }

    /**
     * @brief byteLength
     * @return
     *
     * Returns the total number of bytes
     */
    size_t byteLength() const
    {
        return m_data.size();
    }

    int32_t componentType() const
    {
        return m_gltfComponentType;
    }

    template<typename T>
    void push_back( T const & x)
    {
        uint8_t _x[ sizeof(T)];
        std::memcpy(_x, &x, sizeof(x));
        m_data.insert( m_data.end(),
                       &_x[0],
                       &_x[0] + sizeof(_x));
    }

    template<typename T>
    void set( uint32_t vertexIndex, T const & x)
    {
        auto stride = attributeSize();
        if( sizeof(x) != stride)
        {
            throw std::runtime_error("Size of template is not the same as the size of the vertex attribute");
        }
        std::memcpy( &m_data.at(vertexIndex*stride), &x, sizeof(x));
    }

    template<typename T>
    T get( uint32_t vertexIndex) const
    {
        auto stride = attributeSize();
        if( sizeof(T) != stride)
        {
            throw std::runtime_error("Size of template is not the same as the size of the vertex attribute");
        }
        T x;
        std::memcpy( &x, &m_data.at(vertexIndex*stride), sizeof(x));
        return x;
    }

    template<typename T>
    T getElement( uint32_t elementIndex) const
    {
        auto stride = componentSize();
        if( sizeof(T) != stride)
        {
            throw std::runtime_error("Size of template is not the same as the size of the component");
        }
        T x;
        std::memcpy( &x, &m_data.at(elementIndex*stride), sizeof(x));
        return x;
    }

    template<typename T>
    void setElement( uint32_t elementIndex, T const & x)
    {
        auto stride = componentSize();
        if( sizeof(x) != stride)
        {
            throw std::runtime_error("Size of template is not the same as the size of the component");
        }
        std::memcpy( &m_data.at(elementIndex*stride), &x, sizeof(x));
    }

    void resize(size_t vertexCount)
    {
        m_data.resize(vertexCount * attributeSize() );
    }
    void push_back( const void* data, size_t numBytes)
    {
        auto i = m_data.size();
        m_data.resize( m_data.size() + numBytes);

        std::memcpy( &m_data[i], data, numBytes);
    }

    /**
     * @brief data
     * @return
     *
     * Return a pointer to the start of the buffer
     */
    void const* data() const
    {
        return m_data.data();
    }

    void * data()
    {
        return m_data.data();
    }

    /**
     * @brief data
     * @param vertexOffset
     * @return
     *
     * Return a pointer to the start of a particular vertex.
     */
    void const * data(size_t vertexOffset) const
    {
        uint8_t const * c = static_cast<uint8_t const *>(data()) + vertexOffset*attributeSize();
        return c;
    }

    void * data(size_t vertexOffset)
    {
        uint8_t * c = static_cast<uint8_t*>(data()) + vertexOffset*attributeSize();
        return c;
    }

    /**
     * @brief append
     * @param M
     *
     * Appends the data from attribute M to the end
     * of this current data. DOES NOT CHECK
     * if the data is compatible.
     */
    void append(VertexAttribute const & M)
    {
        auto * v=M.m_data.data();
        auto s = M.byteLength();
        if(s==0) return;
        m_data.insert( m_data.end(),
                       v,
                       v+s);
    }


    /**
     * @brief memcpy
     * @param dst
     * @return
     *
     * Copies the data from this attribute into
     * the memory location provided
     */
    size_t memcpy(void * dst) const
    {
        std::memcpy(dst, m_data.data(), m_data.size());
        return m_data.size();
    }
};


/**
 * @brief The HostTriPrimitive struct
 *
 * A HostTriPrimitive is a strict
 * triangular mesh with predefined
 * vertex attributes. The vertex attributes
 * are the standard set which is required by a GLTF
 * primitive.
 *
 * It is meant to hold a single triangular mesh.
 */
struct HostTriPrimitive
{
    struct DrawCall
    {
        uint32_t indexCount   = 0;
        uint32_t firstIndex   = 0;
         int32_t vertexOffset = 0;
        uint32_t vertexCount  = 0;
    };

    VertexAttribute POSITION  ;
    VertexAttribute NORMAL    ;
    VertexAttribute TANGENT   ;
    VertexAttribute TEXCOORD_0;
    VertexAttribute TEXCOORD_1;
    VertexAttribute COLOR_0   ;
    VertexAttribute JOINTS_0  ;
    VertexAttribute WEIGHTS_0 ;

    VertexAttribute INDEX;

    // a vector of drawcalls
    std::vector<DrawCall> subMeshes;

    size_t addSubMesh(DrawCall const C)
    {
        subMeshes.push_back(C);
        return subMeshes.size()-1;
    }

    size_t hash() const
    {
        auto seed  =  POSITION.hash();
        seed = hashCombine(seed, NORMAL.hash() );
        seed = hashCombine(seed, TANGENT.hash() );
        seed = hashCombine(seed, TEXCOORD_0.hash() );
        seed = hashCombine(seed, TEXCOORD_1.hash() );
        seed = hashCombine(seed, COLOR_0.hash() );
        seed = hashCombine(seed, JOINTS_0.hash() );
        seed = hashCombine(seed, WEIGHTS_0.hash() );
        seed = hashCombine(seed, INDEX.hash() );

        return seed;
    }
    /**
     * @brief vertexCount
     * @return
     *
     * Returns the vertex count of the mesh.
     * Returns the the min
     */
    size_t vertexCount() const
    {
        size_t count = std::numeric_limits<size_t>::max();
        count = std::min( count, POSITION  .count()==0 ? count : POSITION  .count() );
        count = std::min( count, NORMAL    .count()==0 ? count : NORMAL    .count() );
        count = std::min( count, TANGENT   .count()==0 ? count : TANGENT   .count() );
        count = std::min( count, TEXCOORD_0.count()==0 ? count : TEXCOORD_0.count() );
        count = std::min( count, TEXCOORD_1.count()==0 ? count : TEXCOORD_1.count() );
        count = std::min( count, COLOR_0   .count()==0 ? count : COLOR_0   .count() );
        count = std::min( count, JOINTS_0  .count()==0 ? count : JOINTS_0  .count() );
        count = std::min( count, WEIGHTS_0 .count()==0 ? count : WEIGHTS_0 .count() );
        return count==std::numeric_limits<size_t>::max() ? 0 : count;
    }

    size_t indexCount() const
    {
        return INDEX.count();
    }

    /**
     * @brief getDrawCall
     * @return
     *
     * Returns the drawcall of the mesh.
     */
    DrawCall getDrawCall() const
    {
        DrawCall x;
        x.firstIndex  = 0;
        x.indexCount  = static_cast<uint32_t>(indexCount());
        x.vertexCount = static_cast<uint32_t>(vertexCount());
        x.vertexOffset= 0;
        return x;
    }

    bool isSimilar( const HostTriPrimitive & M) const
    {
        VertexAttribute const * A = &POSITION;
        VertexAttribute const * B = &M.POSITION;

        auto s = (&INDEX -  &POSITION)  + 1;
        for(int i=0;i<s;i++)
        {
            if( A[i].components()    != B[i].components() )    return false;
            if( A[i].componentType() != B[i].componentType() ) return false;
        }
        return true;
    }

    /**
     * @brief append
     * @param M
     * @return
     *
     * Appends another mesh to the back of this and
     * returns the drawcall of the new mesh within
     * the larger mesh. Any subsequent calls
     * to append() of the combined mesh will
     * result in a drawcall that draws both meshs
     *
     * If renumberIndices is set to true, it will
     * add the currentVertex count to each of the indices
     */
    DrawCall append( HostTriPrimitive const & M, bool renumberIndices)
    {
        if( !isSimilar(M) )
            throw std::runtime_error("Cannot combine these meshs, they contain different types of vertex attributes");

        VertexAttribute       * A = &POSITION;
        VertexAttribute const * B = &M.POSITION;

        auto subM = M.subMeshes;

        auto N = vertexCount();

        auto dc = getDrawCall();

        // append all the attributes
        auto s = (&INDEX -  &POSITION);
        for(int i=0;i<s;i++)
        {
            A[i].append( B[i] );
        }

        if(renumberIndices)
        {
            auto count = M.INDEX.count();

            if( M.INDEX.componentSize() == 2)
            {
                for(uint32_t i=0;i<count;i++)
                {
                    uint32_t a  = static_cast<uint32_t>(N);
                             a += M.INDEX.get<uint16_t>(i);
                    INDEX.push_back<uint16_t>( static_cast<uint16_t>(a) );
                }
            }
            else if( M.INDEX.componentSize() == 4)
            {
                for(uint32_t i=0;i<count;i++)
                {
                    uint32_t a  = static_cast<uint32_t>(N);
                             a += M.INDEX.get<uint32_t>(i);

                    INDEX.push_back<uint32_t>( a );
                }
            }
        }
        else
        {
            INDEX.append(M.INDEX);
        }

        auto dc_new = M.getDrawCall();

        dc_new.firstIndex   = dc.indexCount;
        if(renumberIndices)
        {
            dc_new.vertexOffset = 0;
        }
        else
        {
            dc_new.vertexOffset = static_cast<int32_t>(dc.vertexCount);
        }

        for(auto & ss : subM)
        {
            ss.firstIndex    = dc.indexCount;
            ss.vertexOffset  = static_cast<int32_t>(dc.vertexCount);
        }
        return dc_new;
    }

    /**
     * @brief requiredByteSize
     * @param alignments
     * @return
     *
     * Returns the total contigious number of bytes
     * required to hold the entire mesh.
     *
     * alignment is the alignment of the vertex attributes
     * it should be a common alignment, likely 16
     * since most GL/vulkan attributes will work with
     * 16 byte alignment.
     */
    size_t requiredByteSize( size_t alignment) const
    {
        #define ROUND_UP(N, S) ((((N) + (S) - 1) / (S)) * (S))
        size_t offset = 0;

        VertexAttribute const * A = &POSITION;

        auto s = (&INDEX -  &POSITION)  + 1;
        for(int i=0;i<s;i++)
        {
            offset += A[i].byteLength();
            offset  = ROUND_UP(offset, alignment);
        }

        #undef ROUND_UP

        return offset;
    }

    /**
     * @brief getOffets
     * @param alignment
     * @return
     *
     * Return the offets of all attributes including the INDEX buffer.
     *
     * The offets would be the byte offsets of each attribute if they were
     * all stored in a single contiguious piece of memory;
     *
     * The attribute will be placed in order of their appearance:
     * +----------+--------+---------+-----------+------------+---------+----------+-----------+--------+
     * | position | normal | tangent | texcoord0 | texcoord 1 | color_0 | joints_0 | weights_0 | index  |
     * +----------+--------+---------+-----------+------------+---------+----------+-----------+--------+
     *
     * The offset will always be aligned to the input alignment.
     *
     * If the attribute does not exist, the offset for that attribute will be set to std::numeric_limits<vk::DeviceSize>::max()
     */
    std::array< vk::DeviceSize, 9> getOffets(size_t alignment) const
    {
        static_assert( offsetof(HostTriPrimitive, POSITION  ) == 0 * sizeof(VertexAttribute), "" );
        static_assert( offsetof(HostTriPrimitive, NORMAL    ) == 1 * sizeof(VertexAttribute), "" );
        static_assert( offsetof(HostTriPrimitive, TANGENT   ) == 2 * sizeof(VertexAttribute), "" );
        static_assert( offsetof(HostTriPrimitive, TEXCOORD_0) == 3 * sizeof(VertexAttribute), "" );
        static_assert( offsetof(HostTriPrimitive, TEXCOORD_1) == 4 * sizeof(VertexAttribute), "" );
        static_assert( offsetof(HostTriPrimitive, COLOR_0   ) == 5 * sizeof(VertexAttribute), "" );
        static_assert( offsetof(HostTriPrimitive, JOINTS_0  ) == 6 * sizeof(VertexAttribute), "" );
        static_assert( offsetof(HostTriPrimitive, WEIGHTS_0 ) == 7 * sizeof(VertexAttribute), "" );
        static_assert( offsetof(HostTriPrimitive, INDEX     ) == 8 * sizeof(VertexAttribute), "" );


        std::array< vk::DeviceSize, 9> offsets;


        #define ROUND_UP(N, S) ((((N) + (S) - 1) / (S)) * (S))
        vk::DeviceSize offset = 0;

        VertexAttribute const * A = &POSITION;

        auto s = static_cast<size_t>( (&INDEX -  &POSITION)  + 1 );
        for(size_t i=0;i<s;i++)
        {
            offsets[i] = A[i].byteLength() == 0 ? std::numeric_limits<vk::DeviceSize>::max() : offset;

            offset += A[i].byteLength();
            offset  = ROUND_UP(offset, alignment);
        }

        #undef ROUND_UP

        return offsets;

    }

    /**
     * @brief copyData
     * @param dst
     * @param alignment
     * @return
     *
     * Copies all the vertex and index attributes into the
     * continuious data.
     */
    size_t copyData(void * dst, std::array< size_t, 9> const & offsets) const
    {
        size_t byteSize=0;
        uint8_t * dstC = static_cast<uint8_t*>(dst);

        VertexAttribute const * A = &POSITION;

        auto s = static_cast<size_t>( (&INDEX -  &POSITION)  + 1 );
        for(size_t i=0;i<s;i++)
        {
            if( offsets[i] != std::numeric_limits<size_t>::max() )
            {
                std::memcpy( &dstC[ offsets[i] ], A[i].data(), A[i].byteLength() );
                byteSize += A[i].byteLength();
            }
        }
        return byteSize;
    }






    //====================================================================
    // Mesh Modifications
    //====================================================================
    void translate(float x, float y, float z)
    {
        auto size = POSITION.count() * POSITION.components();

        for(uint32_t i=0;i<size;i+=3)
        {
            POSITION.setElement(i+0, POSITION.getElement<float>(i+0)+x );
            POSITION.setElement(i+1, POSITION.getElement<float>(i+1)+y );
            POSITION.setElement(i+2, POSITION.getElement<float>(i+2)+z );
        }
    }

    // flips all the normals
    void flipNormals()
    {
        auto size = NORMAL.count() * NORMAL.components();

        auto compType = NORMAL.componentType();

        if( compType == 5126)
        {
            for(uint32_t i=0;i<size;i++)
                NORMAL.setElement(i, -NORMAL.getElement<float>(i) );
        }
        else if( compType == 5130)
        {
            for(uint32_t i=0;i<size;i++)
                NORMAL.setElement(i, -NORMAL.getElement<double>(i) );
        }
        else if( compType == 5124)
        {
            for(uint32_t i=0;i<size;i++)
                NORMAL.setElement(i, -NORMAL.getElement<int32_t>(i) );
        }
        else if( compType == 5120)
        {
            for(uint32_t i=0;i<size;i++)
                NORMAL.setElement(i, -NORMAL.getElement<int8_t>(i) );
        }
        else
        {
            throw std::runtime_error("Cannot flip normals. The NORMAL attribute is set as an UNSIGNED type");
        }
    }

    // reverses the winding order of the indices
    void reverseWindingOrder()
    {
        VertexAttribute INDEX2;
        if( INDEX.attributeSize() == 2) // uint16
        {
            INDEX2.reset<uint16_t>(1);

            auto size = INDEX.count();

            for(uint32_t i=0;i<size;i+=3)
            {
                auto a = INDEX.get<uint16_t>(i);
                auto b = INDEX.get<uint16_t>(i+1);
                auto c = INDEX.get<uint16_t>(i+2);

                INDEX2.push_back(c);
                INDEX2.push_back(b);
                INDEX2.push_back(a);
            }
        }
        else if( INDEX.attributeSize() == 4) // uint32_t
        {
            INDEX2.reset<uint32_t>(1);

            auto size = INDEX.count();

            for(uint32_t i=0;i<size;i+=3)
            {
                auto a = INDEX.get<uint32_t>(i);
                auto b = INDEX.get<uint32_t>(i+1);
                auto c = INDEX.get<uint32_t>(i+2);

                INDEX2.push_back(c);
                INDEX2.push_back(b);
                INDEX2.push_back(a);
            }
        }

        INDEX = INDEX2;
    }
    //====================================================================


    void calculateNormals()
    {
        std::vector<glm::vec3> normals;
        normals.resize( POSITION.count() );
        if( INDEX.attributeSize() == 4 )
        {
            uint32_t indices = static_cast<uint32_t>( INDEX.count() );

            for(uint32_t i=0;i<indices;i+=3)
            {
                auto a = INDEX.get<uint32_t>(i);
                auto b = INDEX.get<uint32_t>(i+1);
                auto c = INDEX.get<uint32_t>(i+2);

                auto p1 = POSITION.get<glm::vec3>(a);
                auto p2 = POSITION.get<glm::vec3>(b);
                auto p3 = POSITION.get<glm::vec3>(c);

                auto v1 = p2-p1;
                auto v2 = p3-p1;
                auto n = glm::cross(v1,v2);

                normals[a] += n;
                normals[b] += n;
                normals[c] += n;
            }
        }
        else if( INDEX.attributeSize() == 2 )
        {
            uint32_t indices =  static_cast<uint32_t>( INDEX.count() );

            for(uint32_t i=0;i<indices;i+=3)
            {
                auto a = INDEX.get<uint16_t>(i);
                auto b = INDEX.get<uint16_t>(i+1);
                auto c = INDEX.get<uint16_t>(i+2);

                auto p1 = POSITION.get<glm::vec3>(a);
                auto p2 = POSITION.get<glm::vec3>(b);
                auto p3 = POSITION.get<glm::vec3>(c);

                auto v1 = p2-p1;
                auto v2 = p3-p1;
                auto n = glm::cross(v1,v2);

                normals[a] += n;
                normals[b] += n;
                normals[c] += n;
            }
        }
        else
        {
            throw std::runtime_error("Cannot flip normals. The NORMAL attribute is set as an UNSIGNED type");
        }
        for(auto & n : normals)
        {
            n = glm::normalize(n);
        }
        NORMAL.reset<float>(3);
        NORMAL.push_back( normals.data(), normals.size() * sizeof(glm::vec3));
    }




    //====================================================================
    // Sample Primitives
    //====================================================================
    static HostTriPrimitive Grid(int length, int width, int dl=1, int dw=1, int majorL=5, int majorW=5, float lscale=1.0f, float wscale=1.0f)
    {

        using _vec3 = std::array<float,3>;
        using _uvec4 = std::array<uint8_t,4>;

        HostTriPrimitive M;

        M.POSITION.reset<float>(3);
        M.COLOR_0.reset<uint8_t>(4);

        auto & P = M.POSITION;
        auto & C = M.COLOR_0;

        _uvec4 majorColor{0,255,0,255};
        _uvec4 minorColor{127,127,127,255};
        _uvec4 borderColor{255,255,255,255};

        for(int x=-length;x<=length;x+=dl)
        {
            _vec3 p0{ static_cast<float>(x)*lscale, 0.0f, static_cast<float>(-width)*wscale };
            _vec3 p1{ static_cast<float>(x)*lscale, 0.0f, static_cast<float>( width)*wscale };

            P.push_back(p0);
            P.push_back(p1);

            if( x == -length || x==length)
            {
                C.push_back(borderColor);
                C.push_back(borderColor);
            }
            else if( x % majorL==0)
            {
                C.push_back(majorColor);
                C.push_back(majorColor);
            }
            else
            {
                C.push_back(minorColor);
                C.push_back(minorColor);
            }
        }

        for(int x=-width;x<=width;x+=dw)
        {
            _vec3 p0{ static_cast<float>( length)*lscale, 0.0, static_cast<float>(x)*wscale };
            _vec3 p1{ static_cast<float>(-length)*lscale, 0.0, static_cast<float>(x)*wscale };

            P.push_back(p0);
            P.push_back(p1);

            if( x == -length || x==length)
            {
                C.push_back(borderColor);
                C.push_back(borderColor);
            }
            else if( x % majorW==0)
            {
                C.push_back(majorColor);
                C.push_back(majorColor);
            }
            else
            {
                C.push_back(minorColor);
                C.push_back(minorColor);
            }
        }

        return M;
    }


    static HostTriPrimitive Box(glm::vec3 const & halfExtents)
    {
        return Box( halfExtents.x*2, halfExtents.y*2, halfExtents.z*2);
    }
    static HostTriPrimitive Box(float dx , float dy , float dz )
    {
        using _vec2 = std::array<float,2>;
        using _vec3 = std::array<float,3>;

        HostTriPrimitive M;

        M.POSITION.reset<float>(3);
        M.NORMAL.reset<float>(3);
        M.TEXCOORD_0.reset<float>(2);

        M.INDEX.reset<uint16_t>(1);

        auto & P = M.POSITION;
        auto & N = M.NORMAL;
        auto & U = M.TEXCOORD_0;
        auto & I = M.INDEX;


    //       |       Position                           |   UV         |     Normal    |
            P.push_back( _vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,0.0f}) ; N.push_back( _vec3{0.0f,  0.0f,  1.0f}) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,0.0f}) ; N.push_back( _vec3{0.0f,  0.0f,  1.0f}) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,1.0f}) ; N.push_back( _vec3{0.0f,  0.0f,  1.0f}) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,0.0f}) ; N.push_back( _vec3{0.0f,  0.0f,  1.0f}) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,1.0f}) ; N.push_back( _vec3{0.0f,  0.0f,  1.0f}) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,1.0f}) ; N.push_back( _vec3{0.0f,  0.0f,  1.0f}) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,1.0f}) ; N.push_back( _vec3{0.0f,  0.0f, -1.0f}) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,1.0f}) ; N.push_back( _vec3{0.0f,  0.0f, -1.0f}) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,0.0f}) ; N.push_back( _vec3{0.0f,  0.0f, -1.0f}) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,1.0f}) ; N.push_back( _vec3{0.0f,  0.0f, -1.0f}) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,0.0f}) ; N.push_back( _vec3{0.0f,  0.0f, -1.0f}) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,0.0f}) ; N.push_back( _vec3{0.0f,  0.0f, -1.0f}) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,0.0f}) ; N.push_back( _vec3{-1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,0.0f}) ; N.push_back( _vec3{-1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,1.0f}) ; N.push_back( _vec3{-1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,0.0f}) ; N.push_back( _vec3{-1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,1.0f}) ; N.push_back( _vec3{-1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,1.0f}) ; N.push_back( _vec3{-1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,1.0f}) ; N.push_back( _vec3{1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,1.0f}) ; N.push_back( _vec3{1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,0.0f}) ; N.push_back( _vec3{1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,1.0f}) ; N.push_back( _vec3{1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,0.0f}) ; N.push_back( _vec3{1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,0.0f}) ; N.push_back( _vec3{1.0f, 0.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,0.0f}) ; N.push_back( _vec3{0.0f,-1.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,0.0f}) ; N.push_back( _vec3{0.0f,-1.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,1.0f}) ; N.push_back( _vec3{0.0f,-1.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,0.0f}) ; N.push_back( _vec3{0.0f,-1.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,1.0f}) ; N.push_back( _vec3{0.0f,-1.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,0.0f - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,1.0f}) ; N.push_back( _vec3{0.0f,-1.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,1.0f}) ; N.push_back( _vec3{0.0f, 1.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,1.0f}) ; N.push_back( _vec3{0.0f, 1.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,0.0f}) ; N.push_back( _vec3{0.0f, 1.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,dz   -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,1.0f}) ; N.push_back( _vec3{0.0f, 1.0f,  0.0f }) ;
            P.push_back( _vec3{dx   - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{1.0f,0.0f}) ; N.push_back( _vec3{0.0f, 1.0f,  0.0f }) ;
            P.push_back( _vec3{0.0f - 0.5f*dx  ,dy   - 0.5f*dy  ,0.0f -0.5f*dz} ) ;  U.push_back( _vec2{0.0f,0.0f}) ; N.push_back( _vec3{0.0f, 1.0f,  0.0f }) ;

        //=========================
        // Edges of the triangle : postion delta


        //=========================
        for( uint16_t j=0;j<36;j++)
            I.push_back( j );


        return M;
    }

    static HostTriPrimitive TestSphere(float radius , uint32_t rings=20, uint32_t sectors=20)
    {
        auto H = Sphere(radius, rings, sectors);
        auto vc = H.vertexCount();

        H.JOINTS_0.reset<uint16_t>(4);
        H.WEIGHTS_0.reset<float>(4);
        for(uint32_t i=0;i<vc;i++)
        {
            glm::u16vec4 x( 0x0, 0x0, 0x01, 0x0);
            H.JOINTS_0.push_back(x)    ;

            glm::vec4 y( 0,0,1,1);
            H.WEIGHTS_0.push_back(y)    ;
        }
        return H;
    }
    static HostTriPrimitive Sphere(float radius , uint32_t rings=20, uint32_t sectors=20)
    {
        using _vec2 = std::array<float,2>;
        using _vec3 = std::array<float,3>;

        HostTriPrimitive M;

        M.POSITION.reset<float>(3);
        M.NORMAL.reset<float>(3);
        M.TEXCOORD_0.reset<float>(2);

        M.INDEX.reset<uint16_t>(1);

        auto & P = M.POSITION;
        auto & N = M.NORMAL;
        auto & U = M.TEXCOORD_0;
        auto & I = M.INDEX;


        float const R = 1.0f / static_cast<float>(rings-1);
        float const S = 1.0f / static_cast<float>(sectors-1);
        unsigned int r, s;


        for(r = 0; r < rings; r++)
        {
            auto rf = static_cast<float>(r);
            for(s = 0; s < sectors; s++)
            {
                auto sf = static_cast<float>(s);

                float const y = std::sin( -3.141592653589f*0.5f + 3.141592653589f * rf * R );
                float const x = std::cos(2*3.141592653589f * sf * S) * std::sin( 3.141592653589f * rf * R );
                float const z = std::sin(2*3.141592653589f * sf * S) * std::sin( 3.141592653589f * rf * R );

                P.push_back( _vec3{ radius*x ,radius*y ,radius*z} );
                U.push_back( _vec2{sf*S, rf*R} );
                N.push_back( _vec3{x,y,z} );
            }
        }


        for(r = 0 ; r < rings   - 1 ; r++)
        {
            for(s = 0 ; s < sectors - 1 ; s++)
            {
                I.push_back(  static_cast<uint16_t>( (r+1) * sectors + s) ); //0
                I.push_back(  static_cast<uint16_t>( (r+1) * sectors + (s+1) ) ); //1
                I.push_back(  static_cast<uint16_t>(  r * sectors + (s+1) )); //2
                I.push_back(  static_cast<uint16_t>( (r+1) * sectors + s )); //0
                I.push_back(  static_cast<uint16_t>(  r * sectors + (s+1) )); //2
                I.push_back(  static_cast<uint16_t>(   r * sectors + s )); //3
            }
        }

        return M;
    }
    static HostTriPrimitive Cylinder(float R=1.0f, float H=3.0f, uint32_t rSegments=16)
    {
        using _vec2 = std::array<float,2>;
        using _vec3 = std::array<float,3>;

        HostTriPrimitive M;

        M.POSITION.reset<float>(3);
        M.NORMAL.reset<float>(3);
        M.TEXCOORD_0.reset<float>(2);
        M.INDEX.reset<uint16_t>(1);

        auto & P = M.POSITION;  //[ vka2::PrimitiveAttribute::POSITION ];
        auto & N = M.NORMAL;    //[ vka2::PrimitiveAttribute::NORMAL ];
        auto & U = M.TEXCOORD_0;//[ vka2::PrimitiveAttribute::TEXCOORD_0 ];
        auto & I = M.INDEX;


        float dt = 2.0f * 3.141592653589f / static_cast<float>(rSegments);

        float t = 0;
        if(1)
        {
           for(uint32_t r=0 ; r<rSegments; r++)
           {
               vec3 p{  R*std::cos(t) ,  R * std::sin(t),  0 };
               t += dt;
               P.push_back(p);
               N.push_back( _vec3{ std::cos(t), std::sin(t), 0.f } );
               U.push_back( _vec2{ t, 0} );
           }

           for(uint32_t r=0 ; r<rSegments; r++)
           {
               vec3 p{  R*std::cos(t) ,  R * std::sin(t),  H };
               t += dt;
               P.push_back(p);
               N.push_back( _vec3{ std::cos(t), std::sin(t), 0.f } );
               U.push_back( _vec2{ t, 1} );
           }


            for(uint32_t i=0 ; i < rSegments; ++i)
            {
                const uint32_t a = (i + 0) % rSegments;
                const uint32_t b = (i + 1) % rSegments;
                const uint32_t c = b + rSegments;

                const uint32_t d = a + rSegments;

                I.push_back( static_cast<uint16_t>(a) );
                I.push_back( static_cast<uint16_t>(b) );
                I.push_back( static_cast<uint16_t>(c) );

                I.push_back( static_cast<uint16_t>(a) );
                I.push_back( static_cast<uint16_t>(c) );
                I.push_back( static_cast<uint16_t>(d) );

                //std::cout << a << ", " << b << ", " << c << std::endl;
                //std::cout << a << ", " << c << ", " << d << std::endl;
            }
        }


        if(1)
        { // top cap

            HostTriPrimitive M2;

            M2.POSITION.reset<float>(3);
            M2.NORMAL.reset<float>(3);
            M2.TEXCOORD_0.reset<float>(2);
            M2.INDEX.reset<uint16_t>(1);

            auto & P2 = M2.POSITION;  //[ vka2::PrimitiveAttribute::POSITION ];
            auto & N2 = M2.NORMAL;    //[ vka2::PrimitiveAttribute::NORMAL ];
            auto & U2 = M2.TEXCOORD_0;//[ vka2::PrimitiveAttribute::TEXCOORD_0 ];
            auto & I2 = M2.INDEX;

            t = 0;
            P2.push_back( vec3{ 0.f, 0.f, H});
            N2.push_back( vec3{ 0.f, 0.f, 1.f } );
            U2.push_back( vec2{ 0.5f, 0.5f } );

            for(uint32_t r=0 ; r < rSegments; r++)
            {
                vec3 p{  R * std::cos(t) ,  R * std::sin(t),  H };
                t += dt;
                P2.push_back(p);
                N2.push_back( vec3{ 0.f, 0.f, 1.f } );
                U2.push_back( vec2{ 0.5f+std::cos(t), 0.5f+std::sin(t)} );

                const uint16_t A = 0;
                const uint16_t B = static_cast<uint16_t>(r+1);
                const uint16_t C = static_cast<uint16_t>( (r+1)%rSegments+1 );

                I2.push_back( static_cast<uint16_t>(A) );
                I2.push_back( static_cast<uint16_t>(B) );
                I2.push_back( static_cast<uint16_t>(C) );

                //std::cout << A << ", " << B << ", " << C << std::endl;
            }

            M.append(M2,true);

            // bottom cap.

           auto M3 = M2;
           {
               auto count = M3.POSITION.count();
               for(uint32_t i=0;i<count;i++)
               {
                   M3.POSITION.setElement<float>(3*i+2, 0.0f);
               }
           }
           M3.flipNormals();
           M3.reverseWindingOrder();
           M.append(M3,true);
        }

        return M;

    }

    static HostTriPrimitive Arrow(float headLength, float headWidth,
                                  float bodyLength, float bodyWidth,
                                  int axis, //1 - x axis, 2-y axis, 3 -z axis
                                  int revolutionSegements)
    {
        float arrowLength = bodyLength + headLength;
        float arrowWidth = bodyWidth + headWidth;

        std::vector<glm::vec2> end_cap            = { { 0, 0     }, { 0, bodyWidth } /*, { 0.30f, 0.05f },{ 1, 0.05f },{ 1, 0.10f },{ 1.2f, 0 }*/ };
        std::vector<glm::vec2> shaft              = { { 0, bodyWidth }, { bodyLength, bodyWidth }  };
        std::vector<glm::vec2> arrow_cap          = { { bodyLength, bodyWidth }, { bodyLength, arrowWidth }  };
        std::vector<glm::vec2> arrow_points       = { { bodyLength, arrowWidth },{ arrowLength, 0.00 } };
        //std::vector<glm::vec2> arrow_points       = { { 0.25f, 0 }, { 0.25f, 0.02f } /*, { 0.30f, 0.05f },{ 1, 0.05f },{ 1, 0.10f },{ 1.2f, 0 }*/ };

        glm::vec3 ax1 = {0,0,1};
        glm::vec3 ax2 = {1,0,0};
        switch (axis) {
            case 0:
            ax1 = {1,0,0};
            ax2 = {1,0,1};
            break;
            case 1:
            ax1 = {0,1,0};
            ax2 = {1,0,0};
            break;
            case 2:
            ax1 = {0,0,1};
            ax2 = {1,0,0};
        default:
            break;
        }
        auto H =       vka::HostTriPrimitive::revolvePolygon( ax1, ax2, revolutionSegements, end_cap);
             H.append( vka::HostTriPrimitive::revolvePolygon( ax1, ax2, revolutionSegements, shaft), true);
             H.append( vka::HostTriPrimitive::revolvePolygon( ax1, ax2, revolutionSegements, arrow_cap), true);
             H.append( vka::HostTriPrimitive::revolvePolygon( ax1, ax2, revolutionSegements, arrow_points), true);

        H.calculateNormals();

        return H;
    }

    static HostTriPrimitive revolvePolygon(glm::vec3 axis,
                                           glm::vec3 perpendicularAxis,
                                           int slices,
                                           std::vector<vec2> const & polygonPoints,
                                           float eps = 0.0f)
    {
        auto & points = polygonPoints;
        auto tau = glm::pi<float>() * 2.0f;
        HostTriPrimitive mesh;

        mesh.POSITION.reset<float>(3);
        mesh.NORMAL.reset<float>(3);
        mesh.INDEX.reset<uint16_t>(1);

        auto & Pos = mesh.POSITION;
        auto & Nor = mesh.NORMAL;

        auto & Ind = mesh.INDEX;

        float slices_f = static_cast<float>(slices);

        axis = glm::normalize(axis);
        auto arm1 = glm::normalize(perpendicularAxis);
        auto arm2 = glm::cross(axis, arm1);

        for (int i = 0; i <= slices; ++i)
        {
            const float angle  = ( static_cast<float>(i % slices) * tau / slices_f) + (tau/8.f), c = std::cos(angle), s = std::sin(angle);
//            const float3x2 mat = { axis, arm1 * c + arm2 * s };

            glm::mat2x3 mat( axis, arm1 * c + arm2 * s);


            for (auto & p : points)
            {
                auto x = mat * p + eps;

                Pos.push_back(x);
                Nor.push_back(x); // will need to be updated manually.
            }

            if (i > 0)
            {
                using uint_type = uint32_t;
                using index_type = uint16_t;
                uint_type k = static_cast<uint_type>(i);
                for (uint_type j = 1; j < points.size(); ++j)
                {
                    uint_type i0 = static_cast<uint_type>(k - 1u) * static_cast<uint_type>(points.size()) + static_cast<uint_type>(j - 1u);
                    uint_type i1 = static_cast<uint_type>(k - 0u) * static_cast<uint_type>(points.size()) + static_cast<uint_type>(j - 1u);
                    uint_type i2 = static_cast<uint_type>(k - 0u) * static_cast<uint_type>(points.size()) + static_cast<uint_type>(j - 0u);
                    uint_type i3 = static_cast<uint_type>(k - 1u) * static_cast<uint_type>(points.size()) + static_cast<uint_type>(j - 0u);


                    index_type j0 = static_cast<index_type>(i0);
                    index_type j1 = static_cast<index_type>(i1);
                    index_type j2 = static_cast<index_type>(i2);
                    index_type j3 = static_cast<index_type>(i3);

                    Ind.push_back(j0);
                    Ind.push_back(j1);
                    Ind.push_back(j2);

                    Ind.push_back(j0);
                    Ind.push_back(j2);
                    Ind.push_back(j3);
                }
            }
        }

        mesh.compute_normals2();

        return mesh;
    }

    void compute_normals2()
    {
        static const float NORMAL_EPSILON = 0.0001f;

        auto vertex_count = vertexCount();

        std::vector<uint32_t> uniqueVertIndices(vertex_count, 0);

        std::vector<glm::vec3> position(vertex_count);
        std::memcpy(&position[0], POSITION.data(), sizeof(glm::vec3)*vertex_count );
        std::vector<glm::vec3> normal( vertex_count, glm::vec3(0.0f));

        for (uint32_t i = 0; i < uniqueVertIndices.size(); ++i)
        {
            if (uniqueVertIndices[i] == 0)
            {
                uniqueVertIndices[i] = i + 1;
                const auto v0 = position[i];
                for (auto j = i + 1; j < vertex_count; ++j)
                {
                    const auto v1 = position[j];
                    auto dv = v1-v0;
                    if ( glm::dot(dv,dv) < NORMAL_EPSILON)
                    {
                        uniqueVertIndices[j] = uniqueVertIndices[i];
                    }
                }
            }
        }

        std::vector<uint16_t> _indices( indexCount(), 0);
        std::memcpy( &_indices[0], INDEX.data(), sizeof(uint16_t)*indexCount() );

        uint32_t idx0, idx1, idx2;
        //for (auto & t : mesh.triangles)
        for(uint32_t j=0; j<_indices.size(); j+=3)
        {
            auto tx = _indices[j];
            auto ty = _indices[j+1];
            auto tz = _indices[j+2];

            idx0 = uniqueVertIndices[tx] - 1;
            idx1 = uniqueVertIndices[ty] - 1;
            idx2 = uniqueVertIndices[tz] - 1;

            //geometry_vertex & v0 = mesh.vertices[idx0], &v1 = mesh.vertices[idx1], &v2 = mesh.vertices[idx2];

            auto & v0 = position[idx0];
            auto & v1 = position[idx1];
            auto & v2 = position[idx2];

            const auto n = glm::cross(v1 - v0, v2 - v0);

            normal[idx0] += n;
            normal[idx1] += n;
            normal[idx2] += n;

        }


        for (uint32_t i = 0; i < vertex_count; ++i)
        {
            normal[i]               = normal[ uniqueVertIndices[i]-1];
            //mesh.vertices[i].normal = mesh.vertices[uniqueVertIndices[i] - 1].normal;
        }


        NORMAL.reset<float>(3);
        for(auto & n : normal)
        {
            n = glm::normalize(n);
            NORMAL.push_back(n);
        }


    }

};

}

#if 0

namespace std
{

template<>
struct hash<vka::HostMeshPrimitiveAttribute>
{
    static inline size_t hashCombine(size_t seed, size_t h2)
    {
        //std::hash<T> hasher;
        seed ^= h2 + 0x9e3779b9 + (seed<<6) + (seed>>2);
        return seed;
    }

    std::size_t operator()(vka::HostMeshPrimitiveAttribute const & img) const noexcept
    {
        std::hash<size_t> Hs;
        std::hash<uint8_t> Hb;

        auto seed  =  Hs( static_cast<size_t>(img.type) );

        seed = hashCombine(seed,  Hs( static_cast<size_t>(img.componentType) ) );

        for(auto & b : img.data)
        {
            seed = hashCombine(seed, Hb(b));
        }

        return seed;
    }
};

template<>
struct hash<vka::HostMeshPrimitive>
{
    static inline size_t hashCombine(size_t seed, size_t h2)
    {
        //std::hash<T> hasher;
        seed ^= h2 + 0x9e3779b9 + (seed<<6) + (seed>>2);
        return seed;
    }

    std::size_t operator()(vka::HostMeshPrimitive const & img) const noexcept
    {
        std::hash<vka::HostMeshPrimitiveAttribute> Ha;
        std::hash<uint32_t> Hu;
        std::hash<int32_t> Hi;

        size_t seed = Ha(img.indices);
        for(auto const & a : img.attributes)
        {
            seed = hashCombine(seed, a);
        }

        for(auto & dc : img.drawCalls)
        {
            seed = hashCombine(seed, Hu(dc.indexCount  ) );
            seed = hashCombine(seed, Hu(dc.firstIndex  ) );
            seed = hashCombine(seed, Hi(dc.vertexOffset) );
            seed = hashCombine(seed, Hu(dc.vertexCount ) );
        }
        return seed;
    }
};

}

#endif
#endif

