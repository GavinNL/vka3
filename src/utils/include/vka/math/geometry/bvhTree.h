/*
  Copyright (c) 2009 Erin Catto http://www.box2d.org
  Copyright (c) 2016-2018 Lester Hedges <lester.hedges+aabbcc@gmail.com>

  This software is provided 'as-is', without any express or implied
  warranty. In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.

  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.

  3. This notice may not be removed or altered from any source distribution.

  This code was adapted from parts of the Box2D Physics Engine,
  http://www.box2d.org

  ----------------------------------------------------------------

  This Code was adapated from https://github.com/lohedges/aabbcc
*/



#pragma once
#ifndef VKA_BVH_TREE_H
#define VKA_BVH_TREE_H

#include <cassert>
#include <vector>
#include <exception>
#include <stdexcept>
#include <unordered_map>
#include <map>

#include "aabb.h"

namespace vka
{

template<typename _T, size_t _dim>
class AABB_t : public aabb_t<_T, _dim>
{
public:
    using value_type = _T;
    using vec_type   = typename aabb_t<_T, _dim>::vec_type;
    using aabb_type  = AABB_t<_T,_dim>;
    using base_type  = aabb_t<_T, _dim>;

    /// The position of the AABB centre.
    vec_type centre;

    /// The AABB's surface area.
    value_type surfaceArea;

    AABB_t()
    {
    }

    constexpr size_t dimensions() const
    {
       return _dim;
    }

    AABB_t(const vec_type& lowerBound_, const vec_type& upperBound_) :
        aabb_t<_T,_dim>(lowerBound_,  upperBound_)
    {
        surfaceArea = base_type::computeSurfaceArea();
        centre      = base_type::computeCentre();
    }

    double getSurfaceArea() const
    {
        return static_cast<double>(surfaceArea);
    }

    void merge(const aabb_type & aabb1, const aabb_type & aabb2)
    {
        int32_t dim = static_cast<int32_t>( dimensions() );
        for (int32_t i=0;i<dim;i++)
        {
            this->lowerBound[i] = std::min(aabb1.lowerBound[i], aabb2.lowerBound[i]);
            this->upperBound[i] = std::max(aabb1.upperBound[i], aabb2.upperBound[i]);
        }

        surfaceArea = base_type::computeSurfaceArea();
        centre      = base_type::computeCentre();
    }

    bool contains(const aabb_type & aabb) const
    {
        int32_t dim = static_cast<int32_t>( dimensions() );
        for (int32_t i=0;i<dim;i++)
        {
            if (aabb.lowerBound[i] < this->lowerBound[i]) return false;
            if (aabb.upperBound[i] > this->upperBound[i]) return false;
        }

        return true;
    }

    bool overlaps(const aabb_type & aabb, bool touchIsOverlap) const
    {
        return base_type::overlaps(aabb, touchIsOverlap);
    }

};

#define NULL_NODE 0xFFFFFFFF

template<typename aabb_t>
struct Node
{
    using aabb_type  = aabb_t;

    /// The fattened axis-aligned bounding box.
    aabb_type aabb;

    /// Index of the parent node.
    uint32_t parent = NULL_NODE;

    /// Index of the next node.
    uint32_t next = NULL_NODE;

    /// Index of the left-hand child.
    uint32_t left = NULL_NODE;

    /// Index of the right-hand child.
    uint32_t right = NULL_NODE;

    /// Height of the node. This is 0 for a leaf and -1 for a free node.
    int height;

    /// The index of the particle that the node contains (leaf nodes only).
    uint32_t particle;

    Node()
    {
    }

    bool isLeaf() const
    {
        return (left == NULL_NODE);
    }
};


template<typename _T, size_t _dim=3>
class BVHTree
{
public:
    using value_type = _T;
    using aabb_type  = AABB_t< value_type, _dim >;
    using vec_type   = typename aabb_type::vec_type;
    using node_type  = Node<aabb_type>;
    using bvec_type  = glm::vec<static_cast<int>(_dim), bool, glm::defaultp>;// array_type;
private:


    /// The index of the root node.
    uint32_t root;

    /// The dynamic tree.
    std::vector<node_type> nodes;

    /// The current number of nodes in the tree.
    uint32_t nodeCount;

    /// The current node capacity.
    uint32_t nodeCapacity;

    /// The position of node at the top of the free list.
    uint32_t freeList;

    /// Whether the system is periodic along at least one axis.
    bool isPeriodic;

    /// The skin thickness of the fattened AABBs, as a fraction of the AABB base length.
    double skinThickness;

    /// Whether the system is periodic along each axis.
    bvec_type periodicity;

    /// The size of the system in each _dim.
    vec_type boxSize;

    /// The position of the negative minimum image.
    vec_type negMinImage;

    /// The position of the positive minimum image.
    vec_type posMinImage;

    /// A map between particle and node indices.
    std::unordered_map<uint32_t, uint32_t> particleMap;

    /// Does touching count as overlapping in tree queries?
    bool touchIsOverlap;
public:
    BVHTree(
               double skinThickness_,
               uint32_t nParticles,
               bool touchIsOverlap_) :
        isPeriodic(false), skinThickness(skinThickness_),
        touchIsOverlap(touchIsOverlap_)
    {

        periodicity = bvec_type(false);

        // Initialise the tree.
        root = NULL_NODE;
        nodeCount = 0;
        nodeCapacity = nParticles;
        nodes.resize(nodeCapacity);

        // Build a linked list for the list of free nodes.
        for (uint32_t i=0;i<nodeCapacity-1;i++)
        {
            nodes[i].next = i + 1;
            nodes[i].height = -1;
        }
        nodes[nodeCapacity-1].next = NULL_NODE;
        nodes[nodeCapacity-1].height = -1;

        // Assign the index of the first free node.
        freeList = 0;
    }

    BVHTree(
               double skinThickness_,
               const bvec_type& periodicity_,
               const vec_type& boxSize_,
               uint32_t nParticles,
               bool touchIsOverlap_) :
        skinThickness(skinThickness_),
        periodicity(periodicity_), boxSize(boxSize_),
        touchIsOverlap(touchIsOverlap_)
    {


        // Initialise the tree.
        root = NULL_NODE;
        touchIsOverlap = true;
        nodeCount = 0;
        nodeCapacity = nParticles;
        nodes.resize(nodeCapacity);

        // Build a linked list for the list of free nodes.
        for (uint32_t i=0;i<nodeCapacity-1;i++)
        {
            nodes[i].next = i + 1;
            nodes[i].height = -1;
        }
        nodes[nodeCapacity-1].next = NULL_NODE;
        nodes[nodeCapacity-1].height = -1;

        // Assign the index of the first free node.
        freeList = 0;

        // Check periodicity.
        isPeriodic = false;

        for (int32_t i=0;i<static_cast<int32_t>(_dim);i++)
        {
            posMinImage[i] =  0.5f*boxSize[i];
            negMinImage[i] = -0.5f*boxSize[i];

            if (periodicity[i])
                isPeriodic = true;
        }
    }

    void setPeriodicity(const bvec_type& periodicity_)
    {
        periodicity = periodicity_;
    }

    void setBoxSize(const vec_type& boxSize_)
    {
        boxSize = boxSize_;
    }

    uint32_t allocateNode()
    {
        // Exand the node pool as needed.
        if (freeList == NULL_NODE)
        {
            assert(nodeCount == nodeCapacity);

            // The free list is empty. Rebuild a bigger pool.
            nodeCapacity *= 2;
            nodes.resize(nodeCapacity);

            // Build a linked list for the list of free nodes.
            for (uint32_t i=nodeCount;i<nodeCapacity-1;i++)
            {
                nodes[i].next = i + 1;
                nodes[i].height = -1;
            }
            nodes[nodeCapacity-1].next = NULL_NODE;
            nodes[nodeCapacity-1].height = -1;

            // Assign the index of the first free node.
            freeList = nodeCount;
        }

        // Peel a node off the free list.
        uint32_t node = freeList;
        freeList = nodes[node].next;
        nodes[node].parent = NULL_NODE;
        nodes[node].left = NULL_NODE;
        nodes[node].right = NULL_NODE;
        nodes[node].height = 0;
        nodeCount++;

        return node;
    }

    void freeNode(uint32_t node)
    {
        assert(node < nodeCapacity);
        assert(0 < nodeCount);

        nodes[node].next = freeList;
        nodes[node].height = -1;
        freeList = node;
        nodeCount--;
    }

    void insertParticle(uint32_t particle, vec_type const & position, value_type radius)
    {
        // Make sure the particle doesn't already exist.
        if (particleMap.count(particle) != 0)
        {
            throw std::invalid_argument("[ERROR]: Particle already exists in tree!");
        }



        // Allocate a new node for the particle.
        uint32_t node = allocateNode();

        // AABB size in each _dim.
        vec_type size;

        // Compute the AABB limits.
        for (int32_t i=0;i<static_cast<int32_t>(_dim);i++)
        {
            nodes[node].aabb.lowerBound[i] = position[i] - radius;
            nodes[node].aabb.upperBound[i] = position[i] + radius;
            size[i] = nodes[node].aabb.upperBound[i] - nodes[node].aabb.lowerBound[i];
        }

        // Fatten the AABB.
        for (int32_t i=0;i<static_cast<int32_t>(_dim);i++)
        {
            nodes[node].aabb.lowerBound[i] -= static_cast<float>(skinThickness) * size[i];
            nodes[node].aabb.upperBound[i] += static_cast<float>(skinThickness) * size[i];
        }
        nodes[node].aabb.surfaceArea = nodes[node].aabb.computeSurfaceArea();
        nodes[node].aabb.centre      = nodes[node].aabb.computeCentre();

        // Zero the height.
        nodes[node].height = 0;

        // Insert a new leaf into the tree.
        insertLeaf(node);

        // Add the new particle to the map.
        particleMap.insert(std::unordered_map<uint32_t, uint32_t>::value_type(particle, node));

        // Store the particle index.
        nodes[node].particle = particle;
    }

    void insertParticle(uint32_t particle, vec_type const & lowerBound, vec_type const & upperBound)
    {
        // Make sure the particle doesn't already exist.
        if (particleMap.count(particle) != 0)
        {
            throw std::invalid_argument("[ERROR]: Particle already exists in tree!");
        }

        // Allocate a new node for the particle.
        uint32_t nodeIndex = allocateNode();

        // AABB size in each _dim.
        vec_type size(_dim);

        // Compute the AABB limits.
        for (int32_t i=0;i<static_cast<int32_t>(_dim);i++)
        {
            // Validate the bound.
            if (lowerBound[i] > upperBound[i])
            {
                throw std::invalid_argument("[ERROR]: AABB lower bound is greater than the upper bound!");
            }

            nodes[nodeIndex].aabb.lowerBound[i] = lowerBound[i];
            nodes[nodeIndex].aabb.upperBound[i] = upperBound[i];
            size[i] = upperBound[i] - lowerBound[i];
        }

        // Fatten the AABB.
        for (int32_t i=0;i<static_cast<int32_t>(_dim);i++)
        {
            nodes[nodeIndex].aabb.lowerBound[i] -= static_cast<typename vec_type::value_type>(skinThickness) * size[i];
            nodes[nodeIndex].aabb.upperBound[i] += static_cast<typename vec_type::value_type>(skinThickness) * size[i];
        }
        nodes[nodeIndex].aabb.surfaceArea = nodes[nodeIndex].aabb.computeSurfaceArea();
        nodes[nodeIndex].aabb.centre = nodes[nodeIndex].aabb.computeCentre();

        // Zero the height.
        nodes[nodeIndex].height = 0;

        // Insert a new leaf into the tree.
        insertLeaf(nodeIndex);

        // Add the new particle to the map.
        particleMap.insert( std::unordered_map<uint32_t, uint32_t>::value_type(particle, nodeIndex) );

        // Store the particle index.
        nodes[nodeIndex].particle = particle;
    }

    uint32_t nParticles()
    {
        return static_cast<uint32_t>( particleMap.size() );
    }

    bool containsParticle(uint32_t particle) const
    {
        // Find the particle.
        auto it = particleMap.find(particle);
        return it != particleMap.end();
    }

    void removeParticle(uint32_t particle)
    {
        // Find the particle.
        auto it = particleMap.find(particle);

        // The particle doesn't exist.
        if (it == particleMap.end())
        {
            throw std::invalid_argument("[ERROR]: Invalid particle index!");
        }

        // Extract the node index.
        uint32_t node = it->second;

        // Erase the particle from the map.
        particleMap.erase(it);

        assert(node < nodeCapacity);
        assert(nodes[node].isLeaf());

        removeLeaf(node);
        freeNode(node);
    }

    void removeAll()
    {
        // Iterator pointing to the start of the particle map.
        auto it = particleMap.begin();

        // Iterate over the map.
        while (it != particleMap.end())
        {
            // Extract the node index.
            uint32_t node = it->second;

            assert(node < nodeCapacity);
            assert(nodes[node].isLeaf());

            removeLeaf(node);
            freeNode(node);

            it++;
        }

        // Clear the particle map.
        particleMap.clear();
    }

    bool updateParticle(uint32_t particle, vec_type const& position, double radius,
                              bool alwaysReinsert)
    {
        // AABB bounds vectors.
        vec_type lowerBound(_dim);
        vec_type upperBound(_dim);

        // Compute the AABB limits.
        for (uint32_t i=0;i<_dim;i++)
        {
            lowerBound[i] = position[i] - radius;
            upperBound[i] = position[i] + radius;
        }

        // Update the particle.
        return updateParticle(particle, lowerBound, upperBound, alwaysReinsert);
    }

    bool updateParticle(uint32_t particle, vec_type const & lowerBound,
                              vec_type const & upperBound, bool alwaysReinsert)
    {
        // Find the particle.
        auto  it = particleMap.find(particle);

        // The particle doesn't exist.
        if (it == particleMap.end())
        {
            throw std::invalid_argument("[ERROR]: Invalid particle index!");
        }

        // Extract the node index.
        uint32_t node = it->second;

        assert(node < nodeCapacity);
        assert(nodes[node].isLeaf());

        // AABB size in each _dim.
        vec_type size(_dim);

        // Compute the AABB limits.
        auto _dim_i = static_cast<int32_t>(_dim);
        for (int32_t i=0; i < _dim_i ;i++)
        {
            // Validate the bound.
            if (lowerBound[i] > upperBound[i])
            {
                throw std::invalid_argument("[ERROR]: AABB lower bound is greater than the upper bound!");
            }

            size[i] = upperBound[i] - lowerBound[i];
        }

        // Create the new AABB.
        aabb_type aabb(lowerBound, upperBound);

        // No need to update if the particle is still within its fattened AABB.
        if (!alwaysReinsert && nodes[node].aabb.contains(aabb)) return false;

        // Remove the current leaf.
        removeLeaf(node);

        // Fatten the new AABB.
        for (int32_t i=0;i<_dim_i;i++)
        {
            aabb.lowerBound[i] -= static_cast<value_type>(skinThickness) * size[i];
            aabb.upperBound[i] += static_cast<value_type>(skinThickness) * size[i];
        }

        // Assign the new AABB.
        nodes[node].aabb = aabb;

        // Update the surface area and centroid.
        nodes[node].aabb.surfaceArea = nodes[node].aabb.computeSurfaceArea();
        nodes[node].aabb.centre = nodes[node].aabb.computeCentre();

        // Insert a new leaf node.
        insertLeaf(node);

        return true;
    }

    std::vector<uint32_t> query(uint32_t particle)
    {
        // Make sure that this is a valid particle.
        if (particleMap.count(particle) == 0)
        {
            throw std::invalid_argument("[ERROR]: Invalid particle index!");
        }

        // Test overlap of particle AABB against all other particles.
        return query(particle, nodes[particleMap.find(particle)->second].aabb);
    }

    /**
     * @brief query
     * @param aabb
     * @param intersects
     * @param C
     *
     * Tests whether a custom shape intersects with any of the particles.
     *
     * the intersects is a calllable funciton that takes the
     *
     * bool intersects( Shape_t const & shape, aabb_type const & aabb)
     *
     * and must return true if shape and aabb intersect with each other.
     *
     * The callback,C is executed for each particle that intersects with shape.
     */
    template<typename Shape_t, typename IntersectionFunction_t, typename Callable_t>
    void query(const Shape_t & shape, IntersectionFunction_t && intersects, Callable_t && C )
    {
          query(std::numeric_limits<uint32_t>::max(), shape, intersects, C);
    }


    /**
     * @brief query
     * @param aabb
     * @param C
     *
     * Queries the tree for all particles which intersect
     * the AABB. Calls the callback function for each particle.
     *
     * the callback function must have the following prototype:
     *
     * void callback(uint32_t particleIndex);
     */
    template<typename Callable_t>
    void query(const aabb_type & aabb , Callable_t && C)
    {
        query(std::numeric_limits<uint32_t>::max(), aabb, C );
    }

    template<typename Shape_t, typename IntersectionFunction_t, typename Callable_t>
    void query(uint32_t particle, const Shape_t & aabb, IntersectionFunction_t && intersects, Callable_t && C )
    {
        std::vector<uint32_t> stack;
        stack.reserve(256);
        stack.push_back(root);

       // std::vector<uint32_t> particles;

        while (stack.size() > 0)
        {
            uint32_t node = stack.back();
            stack.pop_back();

            if (node == NULL_NODE) continue;

            // Copy the AABB.
            auto nodeAABB = nodes[node].aabb;

            // Test for overlap between the AABBs.
            if ( intersects( aabb, nodeAABB) )
            {
                // Check that we're at a leaf node.
                if (nodes[node].isLeaf())
                {
                    // Can't interact with itself.
                    if (nodes[node].particle != particle)
                    {
                        C(nodes[node].particle);
                    }
                }
                else
                {
                    stack.push_back(nodes[node].left);
                    stack.push_back(nodes[node].right);
                }
            }
        }
    }



    template<typename Callable_t>
    void query(uint32_t particle, const aabb_type & aabb, Callable_t && C)
    {
        std::vector<uint32_t> stack;
        stack.reserve(256);
        stack.push_back(root);

       // std::vector<uint32_t> particles;

        while (stack.size() > 0)
        {
            uint32_t node = stack.back();
            stack.pop_back();

            // Copy the AABB.
            auto nodeAABB = nodes[node].aabb;

            if (node == NULL_NODE) continue;

            if (isPeriodic)
            {
                vec_type separation;
                vec_type shift;

                if constexpr( std::is_same<vec_type, std::vector<value_type> >::value )
                {
                    separation.resize(_dim);
                    shift.resize(_dim);
                }

                int32_t dim = static_cast<int32_t>( aabb.dimensions() );
                for (int32_t i=0;i<dim;i++)
                    separation[i] = nodeAABB.centre[i] - aabb.centre[i];

                bool isShifted = minimumImage(separation, shift);

                // Shift the AABB.
                if (isShifted)
                {
                    for (int32_t i=0;i<dim;i++)
                    {
                        nodeAABB.lowerBound[i] += shift[i];
                        nodeAABB.upperBound[i] += shift[i];
                    }
                }
            }

            // Test for overlap between the AABBs.
            if (aabb.overlaps(nodeAABB, touchIsOverlap))
            {
                // Check that we're at a leaf node.
                if (nodes[node].isLeaf())
                {
                    // Can't interact with itself.
                    if (nodes[node].particle != particle)
                    {
                        C(nodes[node].particle);
                    }
                }
                else
                {
                    stack.push_back(nodes[node].left);
                    stack.push_back(nodes[node].right);
                }
            }
        }
    }

    template<typename Callable_t>
    void traverse(Callable_t && C ) const
    {
        std::vector<uint32_t> stack;
        stack.reserve(256);
        stack.push_back(root);

       // std::vector<uint32_t> particles;

        while (stack.size() > 0)
        {
            uint32_t node = stack.back();
            stack.pop_back();

            // Copy the AABB.
            if (node == NULL_NODE) continue;

            C(nodes[node].aabb);

            stack.push_back(nodes[node].left);
            stack.push_back(nodes[node].right);
        }
    }


    std::vector<uint32_t> query(uint32_t particle, const aabb_type & aabb )
    {
        std::vector<uint32_t> particles;
        query(particle, aabb, [&](auto N){particles.push_back(N);} );
        return particles;
    }

    std::vector<uint32_t> query(const aabb_type & aabb)
    {
        // Make sure the tree isn't empty.
        if (particleMap.size() == 0)
        {
            return std::vector<uint32_t>();
        }

        // Test overlap of AABB against all particles.
        return query(std::numeric_limits<uint32_t>::max(), aabb);
    }

    const aabb_type & getAABB(uint32_t particle)
    {
        return nodes[particleMap[particle]].aabb;
    }

    void insertLeaf(uint32_t leaf)
    {
        if (root == NULL_NODE)
        {
            root = leaf;
            nodes[root].parent = NULL_NODE;
            return;
        }

        // Find the best sibling for the node.

        auto leafAABB = nodes[leaf].aabb;
        uint32_t index = root;

        while (!nodes[index].isLeaf())
        {
            // Extract the children of the node.
            uint32_t left  = nodes[index].left;
            uint32_t right = nodes[index].right;

            double surfaceArea = nodes[index].aabb.getSurfaceArea();

            aabb_type combinedAABB;
            combinedAABB.merge(nodes[index].aabb, leafAABB);
            double combinedSurfaceArea = combinedAABB.getSurfaceArea();

            // Cost of creating a new parent for this node and the new leaf.
            double cost = 2.0 * combinedSurfaceArea;

            // Minimum cost of pushing the leaf further down the tree.
            double inheritanceCost = 2.0 * (combinedSurfaceArea - surfaceArea);

            // Cost of descending to the left.
            double costLeft;
            if (nodes[left].isLeaf())
            {
                aabb_type aabb;
                aabb.merge(leafAABB, nodes[left].aabb);
                costLeft = aabb.getSurfaceArea() + inheritanceCost;
            }
            else
            {
                aabb_type aabb;
                aabb.merge(leafAABB, nodes[left].aabb);
                double oldArea = nodes[left].aabb.getSurfaceArea();
                double newArea = aabb.getSurfaceArea();
                costLeft = (newArea - oldArea) + inheritanceCost;
            }

            // Cost of descending to the right.
            double costRight;
            if (nodes[right].isLeaf())
            {
                aabb_type aabb;
                aabb.merge(leafAABB, nodes[right].aabb);
                costRight = aabb.getSurfaceArea() + inheritanceCost;
            }
            else
            {
                aabb_type aabb;
                aabb.merge(leafAABB, nodes[right].aabb);
                double oldArea = nodes[right].aabb.getSurfaceArea();
                double newArea = aabb.getSurfaceArea();
                costRight = (newArea - oldArea) + inheritanceCost;
            }

            // Descend according to the minimum cost.
            if ((cost < costLeft) && (cost < costRight)) break;

            // Descend.
            if (costLeft < costRight) index = left;
            else                      index = right;
        }

        uint32_t sibling = index;

        // Create a new parent.
        uint32_t oldParent = nodes[sibling].parent;
        uint32_t newParent = allocateNode();
        nodes[newParent].parent = oldParent;
        nodes[newParent].aabb.merge(leafAABB, nodes[sibling].aabb);
        nodes[newParent].height = nodes[sibling].height + 1;

        // The sibling was not the root.
        if (oldParent != NULL_NODE)
        {
            if (nodes[oldParent].left == sibling) nodes[oldParent].left = newParent;
            else                                  nodes[oldParent].right = newParent;

            nodes[newParent].left = sibling;
            nodes[newParent].right = leaf;
            nodes[sibling].parent = newParent;
            nodes[leaf].parent = newParent;
        }
        // The sibling was the root.
        else
        {
            nodes[newParent].left = sibling;
            nodes[newParent].right = leaf;
            nodes[sibling].parent = newParent;
            nodes[leaf].parent = newParent;
            root = newParent;
        }

        // Walk back up the tree fixing heights and AABBs.
        index = nodes[leaf].parent;
        while (index != NULL_NODE)
        {
            index = balance(index);

            uint32_t left = nodes[index].left;
            uint32_t right = nodes[index].right;

            assert(left != NULL_NODE);
            assert(right != NULL_NODE);

            nodes[index].height = 1 + std::max(nodes[left].height, nodes[right].height);
            nodes[index].aabb.merge(nodes[left].aabb, nodes[right].aabb);

            index = nodes[index].parent;
        }
    }

    void removeLeaf(uint32_t leaf)
    {
        if (leaf == root)
        {
            root = NULL_NODE;
            return;
        }

        uint32_t parent = nodes[leaf].parent;
        uint32_t grandParent = nodes[parent].parent;
        uint32_t sibling;

        if (nodes[parent].left == leaf) sibling = nodes[parent].right;
        else                            sibling = nodes[parent].left;

        // Destroy the parent and connect the sibling to the grandparent.
        if (grandParent != NULL_NODE)
        {
            if (nodes[grandParent].left == parent) nodes[grandParent].left = sibling;
            else                                   nodes[grandParent].right = sibling;

            nodes[sibling].parent = grandParent;
            freeNode(parent);

            // Adjust ancestor bounds.
            uint32_t index = grandParent;
            while (index != NULL_NODE)
            {
                index = balance(index);

                uint32_t left = nodes[index].left;
                uint32_t right = nodes[index].right;

                nodes[index].aabb.merge(nodes[left].aabb, nodes[right].aabb);
                nodes[index].height = 1 + std::max(nodes[left].height, nodes[right].height);

                index = nodes[index].parent;
            }
        }
        else
        {
            root = sibling;
            nodes[sibling].parent = NULL_NODE;
            freeNode(parent);
        }
    }

    uint32_t balance(uint32_t node)
    {
        assert(node != NULL_NODE);

        if (nodes[node].isLeaf() || (nodes[node].height < 2))
            return node;

        uint32_t left = nodes[node].left;
        uint32_t right = nodes[node].right;

        assert(left < nodeCapacity);
        assert(right < nodeCapacity);

        int currentBalance = nodes[right].height - nodes[left].height;

        // Rotate right branch up.
        if (currentBalance > 1)
        {
            uint32_t rightLeft = nodes[right].left;
            uint32_t rightRight = nodes[right].right;

            assert(rightLeft < nodeCapacity);
            assert(rightRight < nodeCapacity);

            // Swap node and its right-hand child.
            nodes[right].left = node;
            nodes[right].parent = nodes[node].parent;
            nodes[node].parent = right;

            // The node's old parent should now point to its right-hand child.
            if (nodes[right].parent != NULL_NODE)
            {
                if (nodes[nodes[right].parent].left == node) nodes[nodes[right].parent].left = right;
                else
                {
                    assert(nodes[nodes[right].parent].right == node);
                    nodes[nodes[right].parent].right = right;
                }
            }
            else root = right;

            // Rotate.
            if (nodes[rightLeft].height > nodes[rightRight].height)
            {
                nodes[right].right = rightLeft;
                nodes[node].right = rightRight;
                nodes[rightRight].parent = node;
                nodes[node].aabb.merge(nodes[left].aabb, nodes[rightRight].aabb);
                nodes[right].aabb.merge(nodes[node].aabb, nodes[rightLeft].aabb);

                nodes[node].height = 1 + std::max(nodes[left].height, nodes[rightRight].height);
                nodes[right].height = 1 + std::max(nodes[node].height, nodes[rightLeft].height);
            }
            else
            {
                nodes[right].right = rightRight;
                nodes[node].right = rightLeft;
                nodes[rightLeft].parent = node;
                nodes[node].aabb.merge(nodes[left].aabb, nodes[rightLeft].aabb);
                nodes[right].aabb.merge(nodes[node].aabb, nodes[rightRight].aabb);

                nodes[node].height = 1 + std::max(nodes[left].height, nodes[rightLeft].height);
                nodes[right].height = 1 + std::max(nodes[node].height, nodes[rightRight].height);
            }

            return right;
        }

        // Rotate left branch up.
        if (currentBalance < -1)
        {
            uint32_t leftLeft = nodes[left].left;
            uint32_t leftRight = nodes[left].right;

            assert(leftLeft < nodeCapacity);
            assert(leftRight < nodeCapacity);

            // Swap node and its left-hand child.
            nodes[left].left = node;
            nodes[left].parent = nodes[node].parent;
            nodes[node].parent = left;

            // The node's old parent should now point to its left-hand child.
            if (nodes[left].parent != NULL_NODE)
            {
                if (nodes[nodes[left].parent].left == node) nodes[nodes[left].parent].left = left;
                else
                {
                    assert(nodes[nodes[left].parent].right == node);
                    nodes[nodes[left].parent].right = left;
                }
            }
            else root = left;

            // Rotate.
            if (nodes[leftLeft].height > nodes[leftRight].height)
            {
                nodes[left].right = leftLeft;
                nodes[node].left = leftRight;
                nodes[leftRight].parent = node;
                nodes[node].aabb.merge(nodes[right].aabb, nodes[leftRight].aabb);
                nodes[left].aabb.merge(nodes[node].aabb, nodes[leftLeft].aabb);

                nodes[node].height = 1 + std::max(nodes[right].height, nodes[leftRight].height);
                nodes[left].height = 1 + std::max(nodes[node].height, nodes[leftLeft].height);
            }
            else
            {
                nodes[left].right = leftRight;
                nodes[node].left = leftLeft;
                nodes[leftLeft].parent = node;
                nodes[node].aabb.merge(nodes[right].aabb, nodes[leftLeft].aabb);
                nodes[left].aabb.merge(nodes[node].aabb, nodes[leftRight].aabb);

                nodes[node].height = 1 + std::max(nodes[right].height, nodes[leftLeft].height);
                nodes[left].height = 1 + std::max(nodes[node].height, nodes[leftRight].height);
            }

            return left;
        }

        return node;
    }

    uint32_t computeHeight() const
    {
        return computeHeight(root);
    }

    uint32_t computeHeight(uint32_t node) const
    {
        assert(node < nodeCapacity);

        if (nodes[node].isLeaf()) return 0;

        uint32_t height1 = computeHeight(nodes[node].left);
        uint32_t height2 = computeHeight(nodes[node].right);

        return 1 + std::max(height1, height2);
    }

    uint32_t getHeight() const
    {
        if (root == NULL_NODE) return 0;
        return nodes[root].height;
    }

    uint32_t getNodeCount() const
    {
        return nodeCount;
    }

    uint32_t computeMaximumBalance() const
    {
        uint32_t maxBalance = 0;
        for (uint32_t i=0; i<nodeCapacity; i++)
        {
            if (nodes[i].height <= 1)
                continue;

            assert(nodes[i].isLeaf() == false);

            uint32_t balance = std::abs(nodes[nodes[i].left].height - nodes[nodes[i].right].height);
            maxBalance = std::max(maxBalance, balance);
        }

        return maxBalance;
    }

    double computeSurfaceAreaRatio() const
    {
        if (root == NULL_NODE) return 0.0;

        double rootArea = nodes[root].aabb.computeSurfaceArea();
        double totalArea = 0.0;

        for (uint32_t i=0; i<nodeCapacity;i++)
        {
            if (nodes[i].height < 0) continue;

            totalArea += nodes[i].aabb.computeSurfaceArea();
        }

        return totalArea / rootArea;
    }

    void validate() const
    {
#ifndef NDEBUG
        validateStructure(root);
        validateMetrics(root);

        uint32_t freeCount = 0;
        uint32_t freeIndex = freeList;

        while (freeIndex != NULL_NODE)
        {
            assert(freeIndex < nodeCapacity);
            freeIndex = nodes[freeIndex].next;
            freeCount++;
        }

        assert(getHeight() == computeHeight());
        assert((nodeCount + freeCount) == nodeCapacity);
#endif
    }

    void rebuild()
    {
        std::vector<uint32_t> nodeIndices(nodeCount);
        uint32_t count = 0;

        for (uint32_t i=0;i<nodeCapacity;i++)
        {
            // Free node.
            if (nodes[i].height < 0) continue;

            if (nodes[i].isLeaf())
            {
                nodes[i].parent = NULL_NODE;
                nodeIndices[count] = i;
                count++;
            }
            else freeNode(i);
        }

        while (count > 1)
        {
            double minCost = std::numeric_limits<double>::max();
            int iMin = -1, jMin = -1;

            for (uint32_t i=0;i<count;i++)
            {
                auto aabbi = nodes[nodeIndices[i]].aabb;

                for (uint32_t j=i+1;j<count;j++)
                {
                    auto aabbj = nodes[nodeIndices[j]].aabb;
                    aabb_type aabb;
                    aabb.merge(aabbi, aabbj);
                    double cost = aabb.getSurfaceArea();

                    if (cost < minCost)
                    {
                        iMin = static_cast<int>(i);
                        jMin = static_cast<int>(j);
                        minCost = cost;
                    }
                }
            }

            uint32_t index1 = nodeIndices[static_cast<size_t>(iMin)];
            uint32_t index2 = nodeIndices[static_cast<size_t>(jMin)];

            uint32_t parent = allocateNode();
            nodes[parent].left = index1;
            nodes[parent].right = index2;
            nodes[parent].height = 1 + std::max(nodes[index1].height, nodes[index2].height);
            nodes[parent].aabb.merge(nodes[index1].aabb, nodes[index2].aabb);
            nodes[parent].parent = NULL_NODE;

            nodes[index1].parent = parent;
            nodes[index2].parent = parent;

            nodeIndices[static_cast<size_t>(jMin) ] = nodeIndices[count-1];
            nodeIndices[static_cast<size_t>(iMin) ] = parent;
            count--;
        }

        root = nodeIndices[0];

        validate();
    }

    void validateStructure(uint32_t node) const
    {
        if (node == NULL_NODE) return;

        if (node == root) assert(nodes[node].parent == NULL_NODE);

        uint32_t left = nodes[node].left;
        uint32_t right = nodes[node].right;

        if (nodes[node].isLeaf())
        {
            assert(left == NULL_NODE);
            assert(right == NULL_NODE);
            assert(nodes[node].height == 0);
            return;
        }

        assert(left < nodeCapacity);
        assert(right < nodeCapacity);

        assert(nodes[left].parent == node);
        assert(nodes[right].parent == node);

        validateStructure(left);
        validateStructure(right);
    }

    void validateMetrics(uint32_t node) const
    {
        if (node == NULL_NODE) return;

        uint32_t left = nodes[node].left;
        uint32_t right = nodes[node].right;

        if (nodes[node].isLeaf())
        {
            assert(left == NULL_NODE);
            assert(right == NULL_NODE);
            assert(nodes[node].height == 0);
            return;
        }

        assert(left < nodeCapacity);
        assert(right < nodeCapacity);

        int height1 = nodes[left].height;
        int height2 = nodes[right].height;
        int height = 1 + std::max(height1, height2);
        (void)height; // Unused variable in Release build
        assert(nodes[node].height == height);

        aabb_type aabb;
        aabb.merge(nodes[left].aabb, nodes[right].aabb);

        for (uint32_t i=0;i<_dim;i++)
        {
            assert(aabb.lowerBound[i] == nodes[node].aabb.lowerBound[i]);
            assert(aabb.upperBound[i] == nodes[node].aabb.upperBound[i]);
        }

        validateMetrics(left);
        validateMetrics(right);
    }

    void periodicBoundaries(vec_type& position)
    {
        const int32_t dim = static_cast<int32_t>( _dim );
        for (int32_t i=0;i<dim;i++)
        {
            if (position[i] < 0)
            {
                position[i] += boxSize[i];
            }
            else
            {
                if (position[i] >= boxSize[i])
                {
                    position[i] -= boxSize[i];
                }
            }
        }
    }

    bool minimumImage(vec_type& separation, vec_type& shift)
    {
        bool isShifted = false;
        const int32_t dim = static_cast<int32_t>( _dim );
        for (int32_t i=0;i<dim;i++)
        {
            if (separation[i] < negMinImage[i])
            {
                separation[i] += periodicity[i]*boxSize[i];
                shift[i]       = periodicity[i]*boxSize[i];
                isShifted      = true;
            }
            else
            {
                if (separation[i] >= posMinImage[i])
                {
                    separation[i] -=  static_cast<float>(periodicity[i])*boxSize[i];
                    shift[i]       = -static_cast<float>(periodicity[i])*boxSize[i];
                    isShifted      = true;
                }
            }
        }

        return isShifted;
    }
};
}

#endif
