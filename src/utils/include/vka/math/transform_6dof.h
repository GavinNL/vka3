#ifndef VKA_TRANSFORM6DOF_SEQUENCE_H
#define VKA_TRANSFORM6DOF_SEQUENCE_H

#include <vector>
#include <algorithm>

#include <vka/math/linalg.h>

namespace vka
{


/**
 * @brief The transform6dof struct
 *
 * A transformation structure for 6 degrees of freedom (position and rotation, no scaling)
 */
struct transform6dof
{
    using value_type = glm::vec3::value_type;

    glm::vec3 origin;
    glm::quat rotation;

    transform6dof()
    {
    }

    explicit transform6dof(glm::vec3 const & p ) : origin(p), rotation( {1,0,0,0} )
    {
    }

    transform6dof(glm::vec3 const & p, glm::quat const & r) : origin(p), rotation(r)
    {
    }

    transform6dof(transform6dof const & t) : origin(t.origin), rotation(t.rotation)
    {

    }
    /**
     * @brief reverse
     * @return
     *
     * Reverses the orientation of the transformation so that it is pointing
     * in the opposite direction.
     */
    glm::quat reverse() const
    {
        return glm::quat(rotation.w, -rotation.x,  -rotation.y, -rotation.z);
    }

    transform6dof inverse() const
    {
        return {
            -origin,
            reverse()
        };
    }

    inline glm::mat4 get_matrix_rotate_then_translate() const
    {
        return glm::translate(  glm::mat4_cast(rotation), origin);
    }
    inline glm::mat4 get_matrix_translate_then_rotate() const
    {
        return glm::translate(origin) * get_rotation_matrix();
    }
    inline glm::mat4 get_rotation_matrix() const
    {
        return glm::mat4_cast(rotation);
    }

    inline glm::mat4 getMatrix() const
    {
        return get_matrix_translate_then_rotate();
#if defined USE_ANGLE_AXIS
        const float angle    = glm::angle(m_orientation);
        const glm::vec3 axis = glm::axis(m_orientation);

        return glm::scale( glm::rotate( glm::translate(  glm::mat4(1.0f), m_position), angle, axis), m_scale);
#else
       // return glm::translate(  glm::mat4_cast(rotation), origin);
#endif
    }

    static transform6dof identity()
    {
        return transform6dof{ {0,0,0}, {1,0,0,0} };
    }
    void make_identity()
    {
        origin = {0,0,0};
        rotation ={1,0,0,0};
    }

    static transform6dof mix( transform6dof const & in1, transform6dof const & in2, value_type t)
    {
        return transform6dof{
                           glm::mix( in1.origin, in2.origin, t),
                           glm::slerp( in1.rotation, in2.rotation, t)
                            };
                           ///(1.0f-t)*in1.scale    + t*in2.scale );
    }

    static transform6dof mult (const transform6dof & ps, const transform6dof & ls)
    {
        return transform6dof(ps.origin  + ps.rotation * ls.origin,
                             ps.rotation  * ls.rotation);

    }

    static glm::vec3 mult (const transform6dof & ps, const glm::vec3 & ls)
    {
        return ps.origin + ps.rotation * ls;
    }
};


inline transform6dof operator*(const transform6dof & ps, const transform6dof & ls)
{
    return transform6dof::mult(ps,ls);
}
inline glm::vec3 operator*(const transform6dof & ps, const glm::vec3 & ls)
{
    return transform6dof::mult(ps,ls);
}

inline transform6dof mix( transform6dof const & in1, transform6dof const & in2, float t)
{
    return transform6dof{
                       glm::mix( in1.origin, in2.origin, t),
                       glm::slerp( in1.rotation, in2.rotation, t)
                        };
                       ///(1.0f-t)*in1.scale    + t*in2.scale );
}


}


#endif
