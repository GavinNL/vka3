#ifndef VKA_LINALG_H
#define VKA_LINALG_H

#pragma once

#ifndef GLM_FORCE_CTOR_INIT
#define GLM_FORCE_CTOR_INIT
#endif
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

namespace vka
{
    using vec2 = glm::vec2;
    using vec3 = glm::vec3;
    using vec4 = glm::vec4;
    using quat = glm::quat;

    using mat3 = glm::mat3;
    using mat4 = glm::mat4;

    template<typename T>
    inline T mapRange(T in1, T in2, T out1, T out2, T t)
    {
        auto x = (t-in1) / (in2-in1);
        return glm::mix( out1, out2, x);
    }
}
#endif

