#ifndef VKA_MATH_H
#define VKA_MATH_H

#include "linalg.h"

namespace vka
{

inline
glm::vec3 barycentric(glm::vec3 const & p,
                      glm::vec3 const & a,
                      glm::vec3 const & b,
                      glm::vec3 const & c)
{
    glm::vec3 v0 = b - a, v1 = c - a, v2 = p - a;
    auto d00 = glm::dot(v0, v0);
    auto d01 = glm::dot(v0, v1);
    auto d11 = glm::dot(v1, v1);
    auto d20 = glm::dot(v2, v0);
    auto d21 = glm::dot(v2, v1);
    auto one_over_denom = 1.0f / (d00 * d11 - d01 * d01);
    auto v = (d11 * d20 - d01 * d21) * one_over_denom;
    auto w = (d00 * d21 - d01 * d20) * one_over_denom;
    return glm::vec3( v, w, 1.0f-v-w);
}

/**
 * @brief mapRange
 * @param value
 * @param start1
 * @param stop1
 * @param start2
 * @param stop2
 * @return
 *
 * Maps value which lies in the range [start1, stop1], into a new value
 * which lies in the range [start2, stop2]
 */
inline float mapRange(float value,
          float start1, float stop1,
          float start2, float stop2)
{

    const float t = (value-start1) / (stop1-start1);
    return glm::mix(start2, stop2, t);
    //float outgoing =
    //  start2 + (stop2 - start2) * ((value - start1) / (stop1 - start1));
    //return outgoing;
}

/**
 * @brief loopRange
 * @param value
 * @param min
 * @param max
 * @return
 *
 * Given a value and a range, returns the value as if it
 * was looped between the range..
 *
 * for example, loopRange(8, 2,9), is the 8th value starting
 * from 2,3,4,5,6,7,8,2,3,4,5,6,7,8
 */
inline float loopRange(float value, float min, float max)
{
    float range = max-min;
    value = value-min;

    float i = value - std::floor(value/range)*range;
    return i+min;
}

}

#endif
