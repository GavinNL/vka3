#include <vka/utils/HostTriMesh.h>


#include "catch.hpp"


SCENARIO("VertexAttribute")
{
    vka::VertexAttribute A;

    struct TestCase_t
    {
        vka::AccessorType  a;
        vka::ComponentType c;
        vk::Format         f;
        size_t             s;
    };

    TestCase_t TestCases[] = {

        {vka::AccessorType::eVec1, vka::ComponentType::eFloat, vk::Format::eR32Sfloat         , 4},
        {vka::AccessorType::eVec2, vka::ComponentType::eFloat, vk::Format::eR32G32Sfloat      , 8},
        {vka::AccessorType::eVec3, vka::ComponentType::eFloat, vk::Format::eR32G32B32Sfloat   , 12},
        {vka::AccessorType::eVec4, vka::ComponentType::eFloat, vk::Format::eR32G32B32A32Sfloat, 16},

        {vka::AccessorType::eVec1, vka::ComponentType::eInt, vk::Format::eR32Sint         , 4},
        {vka::AccessorType::eVec2, vka::ComponentType::eInt, vk::Format::eR32G32Sint      , 8},
        {vka::AccessorType::eVec3, vka::ComponentType::eInt, vk::Format::eR32G32B32Sint   , 12},
        {vka::AccessorType::eVec4, vka::ComponentType::eInt, vk::Format::eR32G32B32A32Sint, 16},

        {vka::AccessorType::eVec1, vka::ComponentType::eUnsignedInt, vk::Format::eR32Uint         , 4},
        {vka::AccessorType::eVec2, vka::ComponentType::eUnsignedInt, vk::Format::eR32G32Uint      , 8},
        {vka::AccessorType::eVec3, vka::ComponentType::eUnsignedInt, vk::Format::eR32G32B32Uint   , 12},
        {vka::AccessorType::eVec4, vka::ComponentType::eUnsignedInt, vk::Format::eR32G32B32A32Uint, 16},

        {vka::AccessorType::eVec1, vka::ComponentType::eUnsignedShort, vk::Format::eR16Uint         , 2},
        {vka::AccessorType::eVec2, vka::ComponentType::eUnsignedShort, vk::Format::eR16G16Uint      , 4},
        {vka::AccessorType::eVec3, vka::ComponentType::eUnsignedShort, vk::Format::eR16G16B16Uint   , 6},
        {vka::AccessorType::eVec4, vka::ComponentType::eUnsignedShort, vk::Format::eR16G16B16A16Uint, 8},

        {vka::AccessorType::eVec1, vka::ComponentType::eShort, vk::Format::eR16Sint         , 2},
        {vka::AccessorType::eVec2, vka::ComponentType::eShort, vk::Format::eR16G16Sint      , 4},
        {vka::AccessorType::eVec3, vka::ComponentType::eShort, vk::Format::eR16G16B16Sint   , 6},
        {vka::AccessorType::eVec4, vka::ComponentType::eShort, vk::Format::eR16G16B16A16Sint, 8},

        {vka::AccessorType::eVec1, vka::ComponentType::eByte, vk::Format::eR8Sint      , 1},
        {vka::AccessorType::eVec2, vka::ComponentType::eByte, vk::Format::eR8G8Sint    , 2},
        {vka::AccessorType::eVec3, vka::ComponentType::eByte, vk::Format::eR8G8B8Sint  , 3},
        {vka::AccessorType::eVec4, vka::ComponentType::eByte, vk::Format::eR8G8B8A8Sint, 4},

        {vka::AccessorType::eVec1, vka::ComponentType::eUnsignedByte, vk::Format::eR8Uint      , 1},
        {vka::AccessorType::eVec2, vka::ComponentType::eUnsignedByte, vk::Format::eR8G8Uint    , 2},
        {vka::AccessorType::eVec3, vka::ComponentType::eUnsignedByte, vk::Format::eR8G8B8Uint  , 3},
        {vka::AccessorType::eVec4, vka::ComponentType::eUnsignedByte, vk::Format::eR8G8B8A8Uint, 4}
    };

    for(auto & TC : TestCases)
    {
        A.reset( static_cast<uint16_t>(TC.a), static_cast<int32_t>(TC.c) );

        REQUIRE( A.attributeSize() == TC.s);
        REQUIRE( A.components()  == static_cast<size_t>(TC.a));
    }

}



SCENARIO("VertexAttribute 2")
{
    vka::VertexAttribute A;

    A.reset<uint16_t>(1);
    REQUIRE( A.componentSize() == 2);
    REQUIRE( A.attributeSize() == 2);

    A.reset< int16_t>(1);
    REQUIRE( A.componentSize() == 2);
    REQUIRE( A.attributeSize() == 2);

    A.reset<uint32_t>(1);
    REQUIRE( A.componentSize() == 4);
    REQUIRE( A.attributeSize() == 4);

    A.reset< int32_t>(1);
    REQUIRE( A.componentSize() == 4);
    REQUIRE( A.attributeSize() == 4);


    A.reset<uint16_t>(2);
    REQUIRE( A.componentSize() == 2);
    REQUIRE( A.attributeSize()    == 4);
    A.reset< int16_t>(2);
    REQUIRE( A.componentSize() == 2);
    REQUIRE( A.attributeSize()    == 4);
    A.reset<uint32_t>(2);
    REQUIRE( A.componentSize() == 4);
    REQUIRE( A.attributeSize()    == 8);
    A.reset< int32_t>(2);
    REQUIRE( A.componentSize() == 4);
    REQUIRE( A.attributeSize()    == 8);

}




SCENARIO("VertexAttribute, appending data.")
{
    vka::VertexAttribute A;

    A.reset<uint32_t>(3);

    A.push_back( static_cast<uint32_t>(0x12345678) );
    REQUIRE( A.count() == 0);
    A.push_back( static_cast<uint32_t>(0x87654321) );
    REQUIRE( A.count() == 0);
    A.push_back( static_cast<uint32_t>(0xABCDEFAB) );
    REQUIRE( A.count() == 1);

    uint32_t x[3];
    std::memcpy( x, A.data(), sizeof(uint32_t)*3);

    REQUIRE( x[0] == 0x12345678);
    REQUIRE( x[1] == 0x87654321);
    REQUIRE( x[2] == 0xABCDEFAB);
}


SCENARIO("VertexAttribute, Setting vertex attributes at indices")
{
    vka::VertexAttribute A;

    A.reset<uint32_t>(3);

    A.resize(3);

    REQUIRE( A.count() == 3 );

    std::array<uint32_t, 3> v{0xaabbccdd,0x11223344,0x44332211};
    A.set(1, v);

    auto v1 = A.get<std::array<uint32_t,3>>(1);
    REQUIRE( v1[0] == 0xaabbccdd);
    REQUIRE( v1[1] == 0x11223344);
    REQUIRE( v1[2] == 0x44332211);
}


SCENARIO("HostTriPrimitive, appending data.")
{
    vka::HostTriPrimitive M;

    auto & A = M.POSITION;

    A.reset<uint32_t>(3);

    for(int i=0;i<10;i++)
    {
        A.push_back( static_cast<uint32_t>(0x12345678) );
        A.push_back( static_cast<uint32_t>(0x87654321) );
        A.push_back( static_cast<uint32_t>(0xABCDEFAB) );
    }

    REQUIRE(M.vertexCount() == 10);

    auto dc = M.getDrawCall();

    REQUIRE( dc.indexCount == 0);
    REQUIRE( dc.vertexCount == 10);
}

SCENARIO("HostTriPrimitive, isSimilar")
{
    vka::HostTriPrimitive M;

    M.POSITION.reset<float>(3);
    M.COLOR_0.reset<uint8_t>(4);

    THEN("")
    {
        vka::HostTriPrimitive M2;

        M2.POSITION.reset<float>(3);
        M2.COLOR_0.reset<uint8_t>(4);

        REQUIRE( M.isSimilar(M2) );
    }
    THEN("")
    {
        vka::HostTriPrimitive M2;

        M2.POSITION.reset<float>(3);
        M2.COLOR_0.reset<uint8_t>(3);

        REQUIRE( !M.isSimilar(M2) );
    }
}



SCENARIO("Reverse Winding Order")
{
    vka::HostTriPrimitive M;

    M.POSITION.reset<uint32_t>(1);
    M.INDEX.reset<uint32_t>(1);
    M.POSITION.push_back( uint32_t(0x11111111));
    M.POSITION.push_back( uint32_t(0x22222222));
    M.POSITION.push_back( uint32_t(0x33333333));
    M.POSITION.push_back( uint32_t(0x11111111));
    M.POSITION.push_back( uint32_t(0x22222222));
    M.POSITION.push_back( uint32_t(0x33333333));

    M.INDEX.push_back( uint32_t(0) );
    M.INDEX.push_back( uint32_t(1) );
    M.INDEX.push_back( uint32_t(2) );
    M.INDEX.push_back( uint32_t(3) );
    M.INDEX.push_back( uint32_t(4) );
    M.INDEX.push_back( uint32_t(5) );

    WHEN("We reverse the winding order")
    {
        M.reverseWindingOrder();

        REQUIRE( M.INDEX.get<uint32_t>(0) == uint32_t(2) );
        REQUIRE( M.INDEX.get<uint32_t>(1) == uint32_t(1) );
        REQUIRE( M.INDEX.get<uint32_t>(2) == uint32_t(0) );
        REQUIRE( M.INDEX.get<uint32_t>(3) == uint32_t(5) );
        REQUIRE( M.INDEX.get<uint32_t>(4) == uint32_t(4) );
        REQUIRE( M.INDEX.get<uint32_t>(5) == uint32_t(3) );
    }
}

SCENARIO("Flip Normals")
{
    vka::HostTriPrimitive M;

    M.NORMAL.reset<int32_t>(1);

    M.NORMAL.push_back( int32_t(1));
    M.NORMAL.push_back( int32_t(2));
    M.NORMAL.push_back( int32_t(3));
    M.NORMAL.push_back( int32_t(4));
    M.NORMAL.push_back( int32_t(5));
    M.NORMAL.push_back( int32_t(6));

    WHEN("We flip the normals")
    {
        M.flipNormals();

        REQUIRE( M.NORMAL.get<int32_t>(0) == -1 );
        REQUIRE( M.NORMAL.get<int32_t>(1) == -2 );
        REQUIRE( M.NORMAL.get<int32_t>(2) == -3 );
        REQUIRE( M.NORMAL.get<int32_t>(3) == -4 );
        REQUIRE( M.NORMAL.get<int32_t>(4) == -5 );
        REQUIRE( M.NORMAL.get<int32_t>(5) == -6 );
    }
}


SCENARIO("HostTriPrimitive, append")
{
    GIVEN("A mesh with 3 vertices and 1 triangle")
    {
        vka::HostTriPrimitive M;

        M.POSITION.reset<uint32_t>(1);
        M.INDEX.reset<uint32_t>(1);

        M.POSITION.push_back( uint32_t(0x11111111));
        M.POSITION.push_back( uint32_t(0x22222222));
        M.POSITION.push_back( uint32_t(0x33333333));

        M.INDEX.push_back( uint32_t(0) );
        M.INDEX.push_back( uint32_t(1) );
        M.INDEX.push_back( uint32_t(2) );

        WHEN("we append with a mesh that has 6 vertices and 2 triangles")
        {
            vka::HostTriPrimitive M2;

            M2.POSITION.reset<uint32_t>(1);
            M2.INDEX.reset<uint32_t>(1);
            M2.POSITION.push_back( uint32_t(0x11111111));
            M2.POSITION.push_back( uint32_t(0x22222222));
            M2.POSITION.push_back( uint32_t(0x33333333));
            M2.POSITION.push_back( uint32_t(0x11111111));
            M2.POSITION.push_back( uint32_t(0x22222222));
            M2.POSITION.push_back( uint32_t(0x33333333));

            M2.INDEX.push_back( uint32_t(0) );
            M2.INDEX.push_back( uint32_t(1) );
            M2.INDEX.push_back( uint32_t(2) );
            M2.INDEX.push_back( uint32_t(3) );
            M2.INDEX.push_back( uint32_t(4) );
            M2.INDEX.push_back( uint32_t(5) );

            auto dc1 = M.getDrawCall();

            THEN("The original draw call has 3 indices and 3 vertices with offets of 0")
            {

                REQUIRE( dc1.indexCount   == 3);
                REQUIRE( dc1.firstIndex   == 0);
                REQUIRE( dc1.vertexCount  == 3);
                REQUIRE( dc1.vertexOffset == 0);
            }

            WHEN("We append without updating the indices")
            {
                auto dc2 = M.append(M2, false);
                auto dct = M.getDrawCall();

                THEN("The returned drawcall is contains 6 vertices and 2 triangles with index/vertex offsets of 3")
                {
                    REQUIRE( dc2.indexCount   == 6); // 2 triangles
                    REQUIRE( dc2.firstIndex   == 3);
                    REQUIRE( dc2.vertexCount  == 6);
                    REQUIRE( dc2.vertexOffset == 3);

                    THEN("The indices will be updated")
                    {
                        REQUIRE( M.INDEX.get<uint32_t>(0) == uint32_t(0) );
                        REQUIRE( M.INDEX.get<uint32_t>(1) == uint32_t(1) );
                        REQUIRE( M.INDEX.get<uint32_t>(2) == uint32_t(2) );


                        REQUIRE( M.INDEX.get<uint32_t>(3) == uint32_t(0) );
                        REQUIRE( M.INDEX.get<uint32_t>(4) == uint32_t(1) );
                        REQUIRE( M.INDEX.get<uint32_t>(5) == uint32_t(2) );
                        REQUIRE( M.INDEX.get<uint32_t>(6) == uint32_t(3) );
                        REQUIRE( M.INDEX.get<uint32_t>(7) == uint32_t(4) );
                        REQUIRE( M.INDEX.get<uint32_t>(8) == uint32_t(5) );
                    }

                }
                THEN("The drawcall to the combined mesh draws all 3 triangles")
                {
                    REQUIRE( dct.indexCount   == 9);
                    REQUIRE( dct.firstIndex   == 0);
                    REQUIRE( dct.vertexCount  == 9);
                    REQUIRE( dct.vertexOffset == 0);
                }
            }
            WHEN("We append WITH updating the indices")
            {
                auto dc2 = M.append(M2, true);
                auto dct = M.getDrawCall();

                REQUIRE( dc2.indexCount   == 6); // 2 triangles
                REQUIRE( dc2.vertexCount  == 6);

                THEN("The firstIndex will be 3 since there were 3 indices before the appending")
                {
                    REQUIRE( dc2.firstIndex   == 3);
                }
                THEN("The Vertex offset will be zero")
                {
                    REQUIRE( dc2.vertexOffset == 0);

                    THEN("The indices will be updated")
                    {
                        REQUIRE( M.INDEX.get<uint32_t>(0) == uint32_t(0) );
                        REQUIRE( M.INDEX.get<uint32_t>(1) == uint32_t(1) );
                        REQUIRE( M.INDEX.get<uint32_t>(2) == uint32_t(2) );


                        REQUIRE( M.INDEX.get<uint32_t>(3) == uint32_t(3) );
                        REQUIRE( M.INDEX.get<uint32_t>(4) == uint32_t(4) );
                        REQUIRE( M.INDEX.get<uint32_t>(5) == uint32_t(5) );
                        REQUIRE( M.INDEX.get<uint32_t>(6) == uint32_t(6) );
                        REQUIRE( M.INDEX.get<uint32_t>(7) == uint32_t(7) );
                        REQUIRE( M.INDEX.get<uint32_t>(8) == uint32_t(8) );
                    }
                }
                THEN("The drawcall to the combined mesh draws all 3 triangles")
                {
                    REQUIRE( dct.indexCount   == 9);
                    REQUIRE( dct.firstIndex   == 0);
                    REQUIRE( dct.vertexCount  == 9);
                    REQUIRE( dct.vertexOffset == 0);
                }
            }
        }
    }
}






SCENARIO("HostTriPrimitive, alignments")
{
    GIVEN("A mesh with a single vertex of position and color attributes")
    {
        uint32_t alignment=16;
        vka::HostTriPrimitive M;

        M.POSITION.reset<uint32_t>(3);
        M.COLOR_0.reset<uint32_t>(1);

        M.POSITION.push_back( uint32_t(0x11111111));
        M.POSITION.push_back( uint32_t(0x22222222));
        M.POSITION.push_back( uint32_t(0x33333333));

        M.COLOR_0.push_back( uint32_t(0xaabbccdd));

        THEN(" The position byte length is 12 because it has 3 components of uint32_t")
        {
            REQUIRE(M.POSITION.byteLength() == 12);
        }
        THEN("The color byte length is 4 because it has 1 component of uint32_t")
        {
            REQUIRE(M.COLOR_0.byteLength()  == 4);
        }
        THEN("The total required bytes is 32 because each at attribute is aligned to a 16 byte boundary")
        {
            REQUIRE(M.requiredByteSize(alignment)    == 32);
        }
        WHEN("We get the offsets")
        {
            auto offsets = M.getOffets(alignment);

            THEN("The offsets are always at 16  byte boundaries")
            {
                REQUIRE( offsets[0] == 0);
                REQUIRE( offsets[1] == std::numeric_limits<size_t>::max() );
                REQUIRE( offsets[2] == std::numeric_limits<size_t>::max() );
                REQUIRE( offsets[3] == std::numeric_limits<size_t>::max() );
                REQUIRE( offsets[4] == std::numeric_limits<size_t>::max() );
                REQUIRE( offsets[5] == 16);
                REQUIRE( offsets[6] == std::numeric_limits<size_t>::max() );
                REQUIRE( offsets[7] == std::numeric_limits<size_t>::max() );
                REQUIRE( offsets[8] == std::numeric_limits<size_t>::max() );

            }
        }
        WHEN("We copy the data to a contiguious buffer")
        {
            std::vector<uint32_t> rawData;

            // allocate extra space
            rawData.resize( M.requiredByteSize(alignment) / sizeof(uint32_t) + 1);

            auto offsets = M.getOffets(alignment);
            M.copyData( rawData.data() , offsets);

            REQUIRE( rawData[ offsets[0]/sizeof(uint32_t) + 0  ] == 0x11111111);
            REQUIRE( rawData[ offsets[0]/sizeof(uint32_t) + 1  ] == 0x22222222);
            REQUIRE( rawData[ offsets[0]/sizeof(uint32_t) + 2  ] == 0x33333333);

            REQUIRE( rawData[ offsets[5]/sizeof(uint32_t) ] == 0xaabbccdd);
        }
    }

}


SCENARIO("HostTriPrimitive, all attributes")
{
    uint32_t alignment = 1000;
    GIVEN("A mesh with a single vertex of position and color attributes")
    {
        vka::HostTriPrimitive M;

        M.POSITION  .reset<uint32_t>(2);
        M.NORMAL    .reset<uint32_t>(2);
        M.TANGENT   .reset<uint32_t>(2);
        M.TEXCOORD_0.reset<uint32_t>(2);
        M.TEXCOORD_1.reset<uint32_t>(2);
        M.COLOR_0   .reset<uint32_t>(2);
        M.JOINTS_0  .reset<uint32_t>(2);
        M.WEIGHTS_0 .reset<uint32_t>(2);


        M.POSITION  .push_back( glm::u32vec2(0x00000001, 0xA) );
        M.NORMAL    .push_back( glm::u32vec2(0x00000002, 0xB) );
        M.TANGENT   .push_back( glm::u32vec2(0x00000003, 0xC) );
        M.TEXCOORD_0.push_back( glm::u32vec2(0x00000004, 0xD) );
        M.TEXCOORD_1.push_back( glm::u32vec2(0x00000005, 0xE) );
        M.COLOR_0   .push_back( glm::u32vec2(0x00000006, 0xF) );
        M.JOINTS_0  .push_back( glm::u32vec2(0x00000007, 0x1) );
        M.WEIGHTS_0 .push_back( glm::u32vec2(0x00000008, 0x2) );

        WHEN("We get the offsets")
        {
            auto offsets = M.getOffets(alignment);

            THEN("The offsets are always at 16  byte boundaries")
            {
                REQUIRE( offsets[0] == alignment * 0 );//0);
                REQUIRE( offsets[1] == alignment * 1 );//16 );
                REQUIRE( offsets[2] == alignment * 2 );//32 );
                REQUIRE( offsets[3] == alignment * 3 );//48 );
                REQUIRE( offsets[4] == alignment * 4 );//64 );
                REQUIRE( offsets[5] == alignment * 5 );//80);
                REQUIRE( offsets[6] == alignment * 6 );//96 );
                REQUIRE( offsets[7] == alignment * 7 );//112 );
                REQUIRE( offsets[8] == std::numeric_limits<size_t>::max() );
            }
        }
        WHEN("We copy the data to a contiguious buffer")
        {
            std::vector<uint32_t> rawData;

            // allocate extra space
            rawData.resize( M.requiredByteSize(alignment) / sizeof(uint32_t) + 1);

            auto offsets = M.getOffets(alignment);
            M.copyData( rawData.data(), offsets );

            REQUIRE( offsets[0] == alignment * 0 );//0);
            REQUIRE( offsets[1] == alignment * 1 );//16 );
            REQUIRE( offsets[2] == alignment * 2 );//32 );
            REQUIRE( offsets[3] == alignment * 3 );//48 );
            REQUIRE( offsets[4] == alignment * 4 );//64 );
            REQUIRE( offsets[5] == alignment * 5 );//80);
            REQUIRE( offsets[6] == alignment * 6 );//96 );
            REQUIRE( offsets[7] == alignment * 7 );//112 );
            REQUIRE( offsets[8] == std::numeric_limits<size_t>::max() );

            REQUIRE( rawData[ offsets[0] / sizeof(uint32_t) ] == 0x00000001) ;
            REQUIRE( rawData[ offsets[1] / sizeof(uint32_t) ] == 0x00000002) ;
            REQUIRE( rawData[ offsets[2] / sizeof(uint32_t) ] == 0x00000003) ;
            REQUIRE( rawData[ offsets[3] / sizeof(uint32_t) ] == 0x00000004) ;
            REQUIRE( rawData[ offsets[4] / sizeof(uint32_t) ] == 0x00000005) ;
            REQUIRE( rawData[ offsets[5] / sizeof(uint32_t) ] == 0x00000006) ;
            REQUIRE( rawData[ offsets[6] / sizeof(uint32_t) ] == 0x00000007) ;
            REQUIRE( rawData[ offsets[7] / sizeof(uint32_t) ] == 0x00000008) ;

            REQUIRE( rawData[ offsets[0] / sizeof(uint32_t) + 1 ] == 0x0000000a) ;
            REQUIRE( rawData[ offsets[1] / sizeof(uint32_t) + 1 ] == 0x0000000b) ;
            REQUIRE( rawData[ offsets[2] / sizeof(uint32_t) + 1 ] == 0x0000000c) ;
            REQUIRE( rawData[ offsets[3] / sizeof(uint32_t) + 1 ] == 0x0000000d) ;
            REQUIRE( rawData[ offsets[4] / sizeof(uint32_t) + 1 ] == 0x0000000e) ;
            REQUIRE( rawData[ offsets[5] / sizeof(uint32_t) + 1 ] == 0x0000000f) ;
            REQUIRE( rawData[ offsets[6] / sizeof(uint32_t) + 1 ] == 0x00000001) ;
            REQUIRE( rawData[ offsets[7] / sizeof(uint32_t) + 1 ] == 0x00000002) ;
        }
    }

}

#if 0

SCENARIO("HostTriPrimitive, alignments sphere")
{
    GIVEN("A mesh with a single vertex of position and color attributes")
    {
        vka::HostTriPrimitive M = vka::HostTriPrimitive::Sphere(1);

        REQUIRE( M.POSITION.count()   == 400);
        REQUIRE( M.NORMAL.count()     == 400);
        REQUIRE( M.TEXCOORD_0.count() == 400);

        REQUIRE( M.POSITION.count() == M.vertexCount() );

        WHEN("We get the offests")
        {
             auto offsets = M.getOffets(1000);

             REQUIRE( offsets[0] == 0);
             REQUIRE( M.requiredByteSize(1000) >= offsets[8]+M.INDEX.byteLength());
        }

        WHEN("We add new attributes")
        {
            auto vc = M.vertexCount();

            M.JOINTS_0.reset<uint16_t>( 4);
            M.WEIGHTS_0.reset<float>( 4);

            for(uint32_t i=0;i<vc;i++)
            {
                glm::u16vec4 x( 0xFFFF, 0x0, 0x0, 0x0);
                M.JOINTS_0.push_back(x)    ;

                glm::vec4 y( 1,1,1,1);
                M.WEIGHTS_0.push_back(y)    ;
            }

            REQUIRE( M.vertexCount() == 400);
            REQUIRE( M.JOINTS_0.count() == 400);
            REQUIRE( M.WEIGHTS_0.count() == 400);
            THEN("asdf")
            {

                auto offsets = M.getOffets(1000);
                REQUIRE( offsets[0] == 0);
                REQUIRE( M.requiredByteSize(1000) >= offsets[8]+M.INDEX.byteLength());



                THEN("")
                {
                    std::vector<uint16_t> raw( M.requiredByteSize(1000)/2 );

                    auto cOffsets = M.copyData( raw.data(), 1000);

                    REQUIRE(cOffsets == offsets);

                    uint16_t * joints_0 = static_cast<uint16_t*>(M.JOINTS_0.data());
                    uint16_t * joints = &raw[ cOffsets[6] ];

                    auto vertexCount = M.vertexCount();
                    for(uint32_t i=0;i<vertexCount*4;i++)
                    {
                        REQUIRE( *joints_0 == *joints);
                        joints++;
                        joints_0++;
                    }
                }
            }
        }

    }

}
#endif
