#include "catch.hpp"
#include <iostream>
#include <fstream>

#include <vka/utils/AnimationSampler.h>


SCENARIO("Animation Sampler")
{
    GIVEN("An animation float sampler")
    {
        vka::AnimationSampler_t<float, float> f;

        WHEN("We insert values into the sampler")
        {
            f.insert(1.f, 5.f);
            f.insert(7, 5);
            f.insert(3, 5);

            THEN("The order is always sorted by time")
            {
                REQUIRE( f.size() == 3);
                REQUIRE( f[0].time == Approx(1.0f));
                REQUIRE( f[1].time == Approx(3.0f));
                REQUIRE( f[2].time == Approx(7.0f));
            }
            WHEN("We search for a key that is larger than the last value")
            {
                auto interp2 = f.find(10.0f);

                THEN("We get an interpolant that has both frame iterators equal to the last value and the factor=0")
                {
                    REQUIRE( interp2.frame1->time   == Approx(7.0f));
                    REQUIRE( interp2.frame2->time   == Approx(7.0f));
                    REQUIRE( interp2.factor         == Approx(0.0f));
                }
            }
            WHEN("We search for a key that is larger than the last value")
            {
                auto interp2 = f.find(-1.1f);

                THEN("We get an interpolant that has both frame iterators equal to the last value and the factor=0")
                {
                    REQUIRE( interp2.frame1->time   == Approx(1.0f));
                    REQUIRE( interp2.frame2->time   == Approx(1.0f));
                    REQUIRE( interp2.factor         == Approx(0.0f));
                }
            }
            WHEN("We search for a value that is inbetween two frames and the factor is 0.5")
            {
                auto iterp = f.find( 2.0f );
                REQUIRE( iterp.frame1->time == Approx(1.0f));
                REQUIRE( iterp.frame2->time == Approx(3.0f));
                REQUIRE( iterp.factor       == Approx(0.5f));

            }
        }


    }
}






