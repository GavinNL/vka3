#include <vka/math/geometry/bvhTree.h>
#include <vka/utils/Scene.h>
#include <ugltf/ugltf.h>

#include "catch.hpp"
#include <iostream>
#include <fstream>

#include <vka/utils/HostTriMesh.h>
#include <vka/utils/uGLTF_Helpers.h>

#include <vka/utils/SceneLoader.h>

SCENARIO("Convert the BoxAnimated gltf file into a Primitives")
{
    uGLTF::GLTFModel M;
    std::ifstream in(VKA_CMAKE_SOURCE_DIR "/share/models/BoxAnimated.gltf");
    M.load(in);


    auto p = vka::convertUGLTFPrimitiveToHostTriPrimitive( M.meshes[0].primitives[0] );

    auto dc = p.getDrawCall();
    REQUIRE( dc.indexCount   == 186);
    REQUIRE( dc.firstIndex   == 0);
    REQUIRE( dc.vertexOffset == 0);
    REQUIRE( dc.vertexCount  == 96);
}

SCENARIO("Convert the BoxAnimated gltf file into a vka::Scene")
{
    uGLTF::GLTFModel M;
    std::ifstream in(VKA_CMAKE_SOURCE_DIR "/share/models/BoxAnimated.gltf");
    M.load(in);

    vka::HostScene S = vka::fromUGLTF(M,false);

    auto & hostPrimitives = S.rawPrimitives;

    REQUIRE( S.nodes.size() == 4);

    REQUIRE(S.nodes[0].children.at(0) == 1);
    REQUIRE(S.nodes[1].children.at(0) == 2);

    REQUIRE(S.nodes[2].meshIndex == 0);
    REQUIRE(S.nodes[3].meshIndex == 1);

    REQUIRE( S.meshes.size()       == 2 );
    REQUIRE( hostPrimitives.size() == 2 );

    REQUIRE( S.meshes[0].meshPrimitives.size() == 1 );
    REQUIRE( S.meshes[1].meshPrimitives.size() == 1 );

    REQUIRE( S.meshes[0].meshPrimitives[0].primitiveIndex == 0);
    REQUIRE( S.meshes[1].meshPrimitives[0].primitiveIndex == 1);

    REQUIRE( S.primitives[ S.meshes[0].meshPrimitives[0].primitiveIndex].drawCall.indexCount   == 186);
    REQUIRE( S.primitives[ S.meshes[0].meshPrimitives[0].primitiveIndex].drawCall.firstIndex   == 0);
    REQUIRE( S.primitives[ S.meshes[0].meshPrimitives[0].primitiveIndex].drawCall.vertexOffset == 0);
    REQUIRE( S.primitives[ S.meshes[0].meshPrimitives[0].primitiveIndex].drawCall.vertexCount  == 96);

    REQUIRE( S.primitives[ S.meshes[1].meshPrimitives[0].primitiveIndex].drawCall.indexCount   == 576);
    REQUIRE( S.primitives[ S.meshes[1].meshPrimitives[0].primitiveIndex].drawCall.firstIndex   == 0);
    REQUIRE( S.primitives[ S.meshes[1].meshPrimitives[0].primitiveIndex].drawCall.vertexOffset == 0);
    REQUIRE( S.primitives[ S.meshes[1].meshPrimitives[0].primitiveIndex].drawCall.vertexCount  == 224);


    REQUIRE( S.materials.size() == 2);

    REQUIRE( S.materials[0].baseColorFactor[0] == Approx( 0.800000011920929  ) );
    REQUIRE( S.materials[0].baseColorFactor[1] == Approx( 0.4159420132637024 ) );
    REQUIRE( S.materials[0].baseColorFactor[2] == Approx( 0.7952920198440552 ) );
    REQUIRE( S.materials[0].baseColorFactor[3] == Approx( 1.0                ) );

    REQUIRE( S.materials[0].metallicFactor == Approx( 0.0                ) );




    REQUIRE( S.materials[1].baseColorFactor[0] == Approx( 0.301604002714157 ) );
    REQUIRE( S.materials[1].baseColorFactor[1] == Approx( 0.533541977405548 ) );
    REQUIRE( S.materials[1].baseColorFactor[2] == Approx( 0.800000011920929 ) );
    REQUIRE( S.materials[1].baseColorFactor[3] == Approx( 1.0               ) );

    REQUIRE( S.materials[1].metallicFactor == Approx( 0.0                ) );

    WHEN("We optimize the scene")
    {
        REQUIRE( S.rawPrimitives.size() == 2 );

        REQUIRE( S.primitives[ 0 ].rawPrimitiveIndex     == 0);
        REQUIRE( S.primitives[ 0 ].drawCall.indexCount   == 186);
        REQUIRE( S.primitives[ 0 ].drawCall.firstIndex   == 0);
        REQUIRE( S.primitives[ 0 ].drawCall.vertexOffset == 0);
        REQUIRE( S.primitives[ 0 ].drawCall.vertexCount  == 96);

        REQUIRE( S.primitives[ 1 ].rawPrimitiveIndex     == 1);
        REQUIRE( S.primitives[ 1 ].drawCall.indexCount   == 576);
        REQUIRE( S.primitives[ 1 ].drawCall.firstIndex   == 0);
        REQUIRE( S.primitives[ 1 ].drawCall.vertexOffset == 0);
        REQUIRE( S.primitives[ 1 ].drawCall.vertexCount  == 224);


        THEN("Then ")
        {
            S.optimizePrimitives();
            REQUIRE( S.rawPrimitives.size() == 1 );

            REQUIRE( S.primitives[ 0 ].rawPrimitiveIndex     == 0);
            REQUIRE( S.primitives[ 0 ].drawCall.indexCount   == 186);
            REQUIRE( S.primitives[ 0 ].drawCall.firstIndex   == 0);
            REQUIRE( S.primitives[ 0 ].drawCall.vertexOffset == 0);
            REQUIRE( S.primitives[ 0 ].drawCall.vertexCount  == 96);

            REQUIRE( S.primitives[ 1 ].rawPrimitiveIndex     == 0);
            REQUIRE( S.primitives[ 1 ].drawCall.indexCount   == 576);
            REQUIRE( S.primitives[ 1 ].drawCall.firstIndex   == 186);
            REQUIRE( S.primitives[ 1 ].drawCall.vertexOffset == 96);
            REQUIRE( S.primitives[ 1 ].drawCall.vertexCount  == 224);
        }

    }

}





SCENARIO("Convert the BoxTextured gltf file into a vka::Scene")
{
    uGLTF::GLTFModel M;
    std::ifstream in(VKA_CMAKE_SOURCE_DIR "/share/models/BoxTextured.gltf");
    M.load(in);
    vka::HostScene S = vka::fromUGLTF(M);




    REQUIRE( S.materials.size() == 1);

    REQUIRE( S.materials[0].metallicFactor == Approx(0.0f) );
    REQUIRE( S.materials[0].baseColorTexture == 0 );


    REQUIRE( S.textures.size() == 1);
    REQUIRE( S.textures[0].imageIndex   == 0);
    REQUIRE( S.textures[0].samplerIndex == 0);


    REQUIRE( S.images.size() == 1);
}


SCENARIO("Appending Scenes")
{
    vka::HostScene S;

    S.pushPrimitive( vka::HostTriPrimitive::Box(1,1,1) );

    REQUIRE( S.primitives.size() == 1);
    REQUIRE( S.rawPrimitives.size() == 1);

    REQUIRE( S.primitives[0].drawCall.firstIndex   == 0);
    REQUIRE( S.primitives[0].drawCall.vertexOffset == 0);
    REQUIRE( S.primitives[0].drawCall.indexCount   == 36);
    REQUIRE( S.primitives[0].drawCall.vertexCount  == 36);
    WHEN("We add another box")
    {
        S.pushPrimitive( vka::HostTriPrimitive::Sphere(1) );

        REQUIRE( S.primitives.size()    == 2);

        REQUIRE( S.primitives[1].drawCall.firstIndex   == 36);
        REQUIRE( S.primitives[1].drawCall.vertexOffset == 36);
        REQUIRE( S.primitives[1].drawCall.indexCount   == 2166);
        REQUIRE( S.primitives[1].drawCall.vertexCount  == 400);

        REQUIRE( S.rawPrimitives.size() == 1);
    }
}


SCENARIO("Calculating Node Transforms")
{
    vka::HostScene S;

    S.nodes.resize(3);

    S.nodes[0].children.push_back(1);
    S.nodes[1].children.push_back(2);

    S.nodes[1].transform.position = {1,0,0};
    S.nodes[2].transform.position = {0,1,0};

    S.finalize();

    auto tr = S.calculateSceneTransform(2);
    REQUIRE( tr.position.x == Approx(1));
    REQUIRE( tr.position.y == Approx(1));

}

#define STB_IMAGE_IMPLEMENTATION
#define VKA_IMAGE_IMPLEMENTATION

#define VKAI_NO_STDIO
#define STBI_NO_STDIO


#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wduplicated-branches"
#pragma GCC diagnostic ignored "-Wconversion"
#pragma GCC diagnostic ignored "-Wdouble-promotion"
#pragma GCC diagnostic ignored "-Wsign-conversion"
#pragma GCC diagnostic ignored "-Wtype-limits"
#pragma GCC diagnostic ignored "-Wunused-function"

#include <vka/utils/vka_image.h>

#pragma GCC diagnostic pop





