#include <vka/utils/HostImage.h>


#include "catch.hpp"

#include <vka/utils/ImageCompressor.h>


///==============================
///
#if 0

#include "../src/basisu/basisu_comp.h"
#include "../src/basisu/transcoder/basisu_transcoder.h"

#define MAGIC 0xDEADBEE1

basist::etc1_global_selector_codebook g_pGlobal_codebook(basist::g_global_selector_cb_size, basist::g_global_selector_cb);

struct basis_file
{
  uint32_t m_magic = 0;
  basist::basisu_transcoder m_transcoder;
  std::vector<uint8_t> m_file;
  std::vector<uint8_t> m_dst;

  basis_file(const std::vector<uint8_t> & jsBuffer)
    :  m_transcoder(&g_pGlobal_codebook)
  {
    m_file = jsBuffer;
    if (!m_transcoder.validate_header(m_file.data(), m_file.size())) {
      m_file.clear();
    }

    // Initialized after validation
    m_magic = MAGIC;
  }

  void close()
  {
    assert(m_magic == MAGIC);
    m_file.clear();
  }

  uint32_t getHasAlpha() {
    assert(m_magic == MAGIC);
    if (m_magic != MAGIC)
      return 0;

    basist::basisu_image_level_info li;
    if (!m_transcoder.get_image_level_info(m_file.data(), m_file.size(), li, 0, 0))
      return 0;

    return li.m_alpha_flag;
  }

  uint32_t getNumImages() {
    assert(m_magic == MAGIC);
    if (m_magic != MAGIC)
      return 0;

    return m_transcoder.get_total_images(m_file.data(), m_file.size());
  }

  uint32_t getNumLevels(uint32_t image_index) {
    assert(m_magic == MAGIC);
    if (m_magic != MAGIC)
      return 0;

    basist::basisu_image_info ii;
    if (!m_transcoder.get_image_info(m_file.data(), m_file.size(), ii, image_index))
      return 0;

    return ii.m_total_levels;
  }

  uint32_t getImageWidth(uint32_t image_index, uint32_t level_index) {
    assert(m_magic == MAGIC);
    if (m_magic != MAGIC)
      return 0;

    uint32_t orig_width, orig_height, total_blocks;
    if (!m_transcoder.get_image_level_desc(m_file.data(), m_file.size(), image_index, level_index, orig_width, orig_height, total_blocks))
      return 0;

    return orig_width;
  }

  uint32_t getImageHeight(uint32_t image_index, uint32_t level_index) {
    assert(m_magic == MAGIC);
    if (m_magic != MAGIC)
      return 0;

    uint32_t orig_width, orig_height, total_blocks;
    if (!m_transcoder.get_image_level_desc(m_file.data(), m_file.size(), image_index, level_index, orig_width, orig_height, total_blocks))
      return 0;

    return orig_height;
  }

  uint32_t getImageTranscodedSizeInBytes(uint32_t image_index, uint32_t level_index, uint32_t format) {
    assert(m_magic == MAGIC);
    if (m_magic != MAGIC)
      return 0;

    if (format >= (int)basist::transcoder_texture_format::cTFTotalTextureFormats)
      return 0;

     uint32_t orig_width, orig_height, total_blocks;
     if (!m_transcoder.get_image_level_desc(m_file.data(), m_file.size(), image_index, level_index, orig_width, orig_height, total_blocks))
         return 0;

     const basist::transcoder_texture_format transcoder_format = static_cast<basist::transcoder_texture_format>(format);

     if (basis_transcoder_format_is_uncompressed(transcoder_format))
     {
         // Uncompressed formats are just plain raster images.
         const uint32_t bytes_per_pixel = basis_get_uncompressed_bytes_per_pixel(transcoder_format);
         const uint32_t bytes_per_line = orig_width * bytes_per_pixel;
         const uint32_t bytes_per_slice = bytes_per_line * orig_height;
         return bytes_per_slice;
     }
     else
     {
         // Compressed formats are 2D arrays of blocks.
         const uint32_t bytes_per_block = basis_get_bytes_per_block_or_pixel(transcoder_format);

         if (transcoder_format == basist::transcoder_texture_format::cTFPVRTC1_4_RGB || transcoder_format == basist::transcoder_texture_format::cTFPVRTC1_4_RGBA)
         {
             // For PVRTC1, Basis only writes (or requires) total_blocks * bytes_per_block. But GL requires extra padding for very small textures:
              // https://www.khronos.org/registry/OpenGL/extensions/IMG/IMG_texture_compression_pvrtc.txt
             const uint32_t width = (orig_width + 3) & ~3;
             const uint32_t height = (orig_height + 3) & ~3;
             const uint32_t size_in_bytes = (std::max(8U, width) * std::max(8U, height) * 4 + 7) / 8;
             return size_in_bytes;
         }

         return total_blocks * bytes_per_block;
     }
  }

  bool isUASTC() {
    assert(m_magic == MAGIC);
    if (m_magic != MAGIC)
      return false;

    return m_transcoder.get_tex_format(m_file.data(), m_file.size()) == basist::basis_tex_format::cUASTC4x4;
  }

  uint32_t startTranscoding() {
    assert(m_magic == MAGIC);
    if (m_magic != MAGIC)
      return 0;

    return m_transcoder.start_transcoding(m_file.data(), m_file.size());
  }

  uint32_t transcodeImage(//const emscripten::val& dst,
                          uint32_t image_index,
                          uint32_t level_index,
                          basist::transcoder_texture_format format,
                          uint32_t unused,
                          uint32_t get_alpha_for_opaque_formats)
  {
     (void)unused;

      assert(m_magic == MAGIC);
      if (m_magic != MAGIC)
          return 0;

     const basist::transcoder_texture_format transcoder_format = static_cast<basist::transcoder_texture_format>(format);

     uint32_t orig_width, orig_height, total_blocks;
      if (!m_transcoder.get_image_level_desc(m_file.data(), m_file.size(), image_index, level_index, orig_width, orig_height, total_blocks))
          return 0;

      std::vector<uint8_t> dst_data;

      uint32_t flags = get_alpha_for_opaque_formats ? basist::cDecodeFlagsTranscodeAlphaDataToOpaqueFormats : 0;

      uint32_t status;

      if (basis_transcoder_format_is_uncompressed(transcoder_format))
      {
          const uint32_t bytes_per_pixel = basis_get_uncompressed_bytes_per_pixel(transcoder_format);
          const uint32_t bytes_per_line = orig_width * bytes_per_pixel;
          const uint32_t bytes_per_slice = bytes_per_line * orig_height;

          dst_data.resize(bytes_per_slice);

          status = m_transcoder.transcode_image_level(
              m_file.data(), m_file.size(), image_index, level_index,
              dst_data.data(), orig_width * orig_height,
              transcoder_format,
              flags,
              orig_width,
              nullptr,
              orig_height);
      }
      else
      {
          uint32_t bytes_per_block = basis_get_bytes_per_block_or_pixel(transcoder_format);

          uint32_t required_size = total_blocks * bytes_per_block;

          if (transcoder_format == basist::transcoder_texture_format::cTFPVRTC1_4_RGB || transcoder_format == basist::transcoder_texture_format::cTFPVRTC1_4_RGBA)
          {
              // For PVRTC1, Basis only writes (or requires) total_blocks * bytes_per_block. But GL requires extra padding for very small textures:
              // https://www.khronos.org/registry/OpenGL/extensions/IMG/IMG_texture_compression_pvrtc.txt
              // The transcoder will clear the extra bytes followed the used blocks to 0.
              const uint32_t width = (orig_width + 3) & ~3;
              const uint32_t height = (orig_height + 3) & ~3;
              required_size = (std::max(8U, width) * std::max(8U, height) * 4 + 7) / 8;
              assert(required_size >= total_blocks * bytes_per_block);
          }

          dst_data.resize(required_size);

          status = m_transcoder.transcode_image_level(
              m_file.data(), m_file.size(), image_index, level_index,
              dst_data.data(), dst_data.size() / bytes_per_block,
              static_cast<basist::transcoder_texture_format>(format),
              flags);
      }

//      emscripten::val memory = emscripten::val::module_property("HEAP8")["buffer"];
//      emscripten::val memoryView = emscripten::val::global("Uint8Array").new_(memory, reinterpret_cast<uintptr_t>(dst_data.data()), dst_data.size());

//      dst.call<void>("set", memoryView);

      m_dst = std::move(dst_data);
      return status;
  }
};

///==============================
SCENARIO("")
{
    basisu::basisu_encoder_init();

    basisu::basisu_encoder_init();
    basisu::basisu_encoder_init();
    basisu::basisu_encoder_init();
    basisu::basisu_encoder_init();

    basisu::job_pool jobPool(1);

    vka::HostImage img;
    img.resize(1024, 1024);

    //// Generate an image by applying a function to each channel
    img.r = [](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
    img.g = 0;//[](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
    img.b = [](float u, float v){ return 0.5f+0.5f*std::sin( 3*2*3.14159f*std::hypot(u,v-1)) ;}; // full red texture;
    img.a = 255; // full red texture;


    WHEN("we create a basisu image")
    {
        static_assert( sizeof(basisu::color_rgba) == sizeof( vka::HostImage::value_type), "");

        basisu::image Bimg;

        Bimg.resize( img.width(), img.height() );

        std::memcpy( Bimg.get_pixels().data(), img.data(), img.size() );
        auto & pixels = Bimg.get_pixels();
        REQUIRE( pixels.size() == img.width() * img.height() );

        THEN("")
        {
            basisu::basis_compressor_params params;

            params.m_source_images.push_back( Bimg );

            basisu::basis_compressor comp;

            params.m_read_source_images = false;
            params.m_debug              = true;
            params.m_uastc              = true;
            params.m_mip_gen            = false;
            params.m_pJob_pool          = &jobPool;

            REQUIRE( comp.init(params) );
            auto err = comp.process();

            REQUIRE( err == basisu::basis_compressor::cECSuccess);

            REQUIRE(comp.get_basis_file_size() > 0 );
            REQUIRE(comp.get_basis_file_size() < img.size() );


            THEN("We can transcode to BC1")
            {
                basis_file B( comp.get_output_basis_file() );

                auto status =
                B.transcodeImage(//const emscripten::val& dst,
                                        0,
                                        0,
                                        basist::transcoder_texture_format::cTFBC3,
                                        0,
                                         1);

                REQUIRE( status == 0 );

                REQUIRE( B.m_dst.size() > 0 );
            }

        }
    }


    //params.m_source_images
}
#endif


#if 1

SCENARIO("Uinsg the BasisConverter calss")
{

    vka::HostImage img;
    img.resize(1024, 1024);

    //// Generate an image by applying a function to each channel
    img.r = [](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
    img.g = 0;//[](float u, float v){ return 0.5f+0.5f*std::cos( 3*2*3.14159f*std::hypot(u,v))   ;}; // full red texture;
    img.b = [](float u, float v){ return 0.5f+0.5f*std::sin( 3*2*3.14159f*std::hypot(u,v-1)) ;}; // full red texture;
    img.a = 255; // full red texture;


    WHEN("we create a basisu image")
    {
        vka::ImageCompressor conv;

        conv.pushImageLayer(img);
        conv.pushImageLayer(img);
        auto B = conv.process(true);


        REQUIRE( B.size() > 0 );
        REQUIRE( B.size() < 2*img.size());

        auto comp = B.transcodeImage(0,0, vk::Format::eBc3UnormBlock,1);

        REQUIRE( comp.m_data.size() > 0);
        REQUIRE( comp.m_data.size() < 2*img.size());
    }


    //params.m_source_images
}

#endif
